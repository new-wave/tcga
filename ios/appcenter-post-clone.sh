#!/usr/bin/env bash
#Place this script in project/ios/

echo "Uninstalling all CocoaPods versions"
#sudo gem uninstall cocoapods --all --executables

#COCOAPODS_VER=`sed -n -e 's/^COCOAPODS: \([0-9.]*\)/\1/p' Podfile.lock`
# fail if any command fails
set -e
# debug log
set -x
cd ..

rm -fR flutter
git clone -b beta https://github.com/flutter/flutter.git
cd flutter
git checkout 9b2d32b605630f28625709ebd9d78ab3016b2bf6
cd ..
export PATH=`pwd`/flutter/bin:$PATH
##echo "Installing CocoaPods version $COCOAPODS_VER"
#sudo gem install cocoapods -v $COCOAPODS_VER
sudo gem uninstall cocoapods -ax
sudo gem install cocoapods --pre

pod setup
flutter doctor
flutter pub get

echo "Installed flutter to `pwd`/flutter"

flutter build ios --release --no-codesign
import 'package:hive/hive.dart';

import '_hive_type_id.dart';

part 'language.g.dart';

@HiveType(typeId: HiveTypeId.language)
class Language extends HiveObject {
  @HiveField(0)
  String code;
  @HiveField(1)
  String name;
  @HiveField(2)
  bool isPrimary;

  Language({
    this.code,
    this.name,
    this.isPrimary,
  });
}

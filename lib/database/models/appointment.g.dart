// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'appointment.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class ParticipantAdapter extends TypeAdapter<Participant> {
  @override
  final int typeId = 13;

  @override
  Participant read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Participant(
      isOrganizer: fields[5] as bool,
      reference: fields[2] as String,
      isRequired: fields[4] as int,
      role: fields[1] as int,
      status: fields[3] as int,
      type: fields[0] as int,
    );
  }

  @override
  void write(BinaryWriter writer, Participant obj) {
    writer
      ..writeByte(6)
      ..writeByte(0)
      ..write(obj.type)
      ..writeByte(1)
      ..write(obj.role)
      ..writeByte(2)
      ..write(obj.reference)
      ..writeByte(3)
      ..write(obj.status)
      ..writeByte(4)
      ..write(obj.isRequired)
      ..writeByte(5)
      ..write(obj.isOrganizer);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ParticipantAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class AppointmentAdapter extends TypeAdapter<Appointment> {
  @override
  final int typeId = 11;

  @override
  Appointment read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Appointment(
      id: fields[0] as dynamic,
      actualEndTime: fields[18] as DateTime,
      actualStartTime: fields[17] as DateTime,
      appointmentStyle: fields[30] as int,
      cancellationReasonCode: fields[28] as String,
      capacity: fields[14] as int,
      color: fields[22] as Color,
      description: fields[9] as String,
      endTime: fields[16] as DateTime,
      isAllDay: fields[23] as bool,
      participants: (fields[13] as List)?.cast<Participant>(),
      patientInstructions: fields[34] as String,
      priority: fields[33] as int,
      reasonReference: fields[32] as String,
      reasonReferenceType: fields[31] as AppointmentReasonReferenceType,
      recurrenceExceptionDates: (fields[21] as List)?.cast<DateTime>(),
      recurrenceId: fields[20] as String,
      recurrenceRule: fields[19] as String,
      reference: fields[11] as String,
      reminderMinutesBeforeStart: fields[25] as int,
      reminderOption: fields[24] as int,
      sensitivity: fields[12] as SensitivityType,
      serviceTypeCode: fields[29] as String,
      startTime: fields[15] as DateTime,
      status: fields[27] as int,
      subject: fields[8] as String,
      timeSlots: (fields[36] as List)?.cast<TimeSlot>(),
      timeZone: fields[35] as String,
      type: fields[10] as int,
      venue: fields[26] as int,
    )
      ..version = fields[1] as int
      ..createdOn = fields[2] as DateTime
      ..updatedOn = fields[3] as DateTime
      ..deletedOn = fields[4] as DateTime
      ..syncedOn = fields[5] as DateTime
      ..isDeleted = fields[6] as bool
      ..managingOrganization = fields[7] as String
      ..createdBy = fields[99] as String;
  }

  @override
  void write(BinaryWriter writer, Appointment obj) {
    writer
      ..writeByte(38)
      ..writeByte(8)
      ..write(obj.subject)
      ..writeByte(9)
      ..write(obj.description)
      ..writeByte(10)
      ..write(obj.type)
      ..writeByte(11)
      ..write(obj.reference)
      ..writeByte(12)
      ..write(obj.sensitivity)
      ..writeByte(13)
      ..write(obj.participants)
      ..writeByte(14)
      ..write(obj.capacity)
      ..writeByte(15)
      ..write(obj.startTime)
      ..writeByte(16)
      ..write(obj.endTime)
      ..writeByte(17)
      ..write(obj.actualStartTime)
      ..writeByte(18)
      ..write(obj.actualEndTime)
      ..writeByte(19)
      ..write(obj.recurrenceRule)
      ..writeByte(20)
      ..write(obj.recurrenceId)
      ..writeByte(21)
      ..write(obj.recurrenceExceptionDates)
      ..writeByte(22)
      ..write(obj.color)
      ..writeByte(23)
      ..write(obj.isAllDay)
      ..writeByte(24)
      ..write(obj.reminderOption)
      ..writeByte(25)
      ..write(obj.reminderMinutesBeforeStart)
      ..writeByte(26)
      ..write(obj.venue)
      ..writeByte(27)
      ..write(obj.status)
      ..writeByte(28)
      ..write(obj.cancellationReasonCode)
      ..writeByte(29)
      ..write(obj.serviceTypeCode)
      ..writeByte(30)
      ..write(obj.appointmentStyle)
      ..writeByte(31)
      ..write(obj.reasonReferenceType)
      ..writeByte(32)
      ..write(obj.reasonReference)
      ..writeByte(33)
      ..write(obj.priority)
      ..writeByte(34)
      ..write(obj.patientInstructions)
      ..writeByte(35)
      ..write(obj.timeZone)
      ..writeByte(36)
      ..write(obj.timeSlots)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.version)
      ..writeByte(2)
      ..write(obj.createdOn)
      ..writeByte(3)
      ..write(obj.updatedOn)
      ..writeByte(4)
      ..write(obj.deletedOn)
      ..writeByte(5)
      ..write(obj.syncedOn)
      ..writeByte(6)
      ..write(obj.isDeleted)
      ..writeByte(7)
      ..write(obj.managingOrganization)
      ..writeByte(99)
      ..write(obj.createdBy);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is AppointmentAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class TimeSlotAdapter extends TypeAdapter<TimeSlot> {
  @override
  final int typeId = 14;

  @override
  TimeSlot read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return TimeSlot(
      durationInMinute: fields[0] as int,
      startAt: fields[1] as DateTime,
    );
  }

  @override
  void write(BinaryWriter writer, TimeSlot obj) {
    writer
      ..writeByte(2)
      ..writeByte(0)
      ..write(obj.durationInMinute)
      ..writeByte(1)
      ..write(obj.startAt);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is TimeSlotAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

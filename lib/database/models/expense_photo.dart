import 'dart:io';

import 'package:hive/hive.dart';

import '_hive_type_id.dart';

part 'expense_photo.g.dart';

@HiveType(
  typeId: HiveTypeId.expensePhoto,
)
class ExpensePhoto extends HiveObject {
  @HiveField(0)
  String url;

  ExpensePhoto({
    this.url,
  });
}

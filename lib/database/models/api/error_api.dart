import 'package:json_annotation/json_annotation.dart';

part 'error_api.g.dart';

@JsonSerializable(nullable: true)
class ErrorAPI {
  @JsonKey(name: 'type')
  final String type;
  @JsonKey(name: 'title')
  final String title;
  @JsonKey(name: 'status')
  final int status;
  @JsonKey(name: 'detail')
  final String detail;
  @JsonKey(name: 'instance')
  final String instance;
  ErrorAPI({
    this.type,
    this.title,
    this.status,
    this.detail,
    this.instance,
  });

  factory ErrorAPI.fromJson(Map<String, dynamic> json) =>
      _$ErrorAPIFromJson(json);
  Map<String, dynamic> toJson() => _$ErrorAPIToJson(this);
}

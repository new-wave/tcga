// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'registration_api.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Registration _$RegistrationFromJson(Map<String, dynamic> json) {
  return Registration(
    links: (json['links'] as List)
        ?.map(
            (e) => e == null ? null : Links.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    email: json['email'] as String,
    mobileNumber: json['mobileNumber'] as String,
    registrationCode: json['registrationCode'] as String,
    password: json['password'] as String,
    userAgent: json['userAgent'] as String,
  );
}

Map<String, dynamic> _$RegistrationToJson(Registration instance) =>
    <String, dynamic>{
      'links': instance.links,
      'email': instance.email,
      'mobileNumber': instance.mobileNumber,
      'registrationCode': instance.registrationCode,
      'password': instance.password,
      'userAgent': instance.userAgent,
    };

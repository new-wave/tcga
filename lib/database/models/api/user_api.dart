import 'package:json_annotation/json_annotation.dart';

part 'user_api.g.dart';

@JsonSerializable(nullable: true)
class UserAPI {
  @JsonKey(name: 'links')
  final List<Links> links;
  @JsonKey(name: 'id', defaultValue: '')
  final String id;
  @JsonKey(name: 'externalId', defaultValue: '')
  final String externalId;
  @JsonKey(name: 'role', defaultValue: '')
  final String role;
  @JsonKey(name: 'email', defaultValue: '')
  final String email;
  @JsonKey(name: 'mobileNumber', defaultValue: '')
  final String mobileNumber;
  @JsonKey(name: 'firstName', defaultValue: '')
  final String firstName;
  @JsonKey(name: 'lastName', defaultValue: '')
  final String lastName;
  @JsonKey(name: 'userAgent', defaultValue: '')
  final String userAgent;
  @JsonKey(name: 'recaptchaResponse', defaultValue: '')
  final String recaptchaResponse;
  UserAPI({
    this.links,
    this.id,
    this.externalId,
    this.role,
    this.email,
    this.mobileNumber,
    this.firstName,
    this.lastName,
    this.userAgent,
    this.recaptchaResponse,
  });

  factory UserAPI.fromJson(Map<String, dynamic> json) =>
      _$UserAPIFromJson(json);
  Map<String, dynamic> toJson() => _$UserAPIToJson(this);
}

@JsonSerializable(nullable: true)
class Links {
  @JsonKey(name: 'href', defaultValue: '')
  final String href;
  @JsonKey(name: 'rel', defaultValue: '')
  final String rel;
  @JsonKey(name: 'method', defaultValue: '')
  final String method;
  Links(
    this.href,
    this.rel,
    this.method,
  );
  factory Links.fromJson(Map<String, dynamic> json) => _$LinksFromJson(json);
  Map<String, dynamic> toJson() => _$LinksToJson(this);
}

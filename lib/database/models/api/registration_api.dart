import 'package:json_annotation/json_annotation.dart';
import 'user_api.dart';

part 'registration_api.g.dart';

@JsonSerializable(nullable: true)
class Registration {
  @JsonKey(name: 'links')
  final List<Links> links;
  @JsonKey(name: 'email')
  final String email;
  @JsonKey(name: 'mobileNumber')
  final String mobileNumber;
  @JsonKey(name: 'registrationCode')
  final String registrationCode;
  @JsonKey(name: 'password')
  final String password;
  @JsonKey(name: 'userAgent')
  final String userAgent;
  Registration({
    this.links,
    this.email,
    this.mobileNumber,
    this.registrationCode,
    this.password,
    this.userAgent,
  });

  factory Registration.fromJson(Map<String, dynamic> json) =>
      _$RegistrationFromJson(json);
  Map<String, dynamic> toJson() => _$RegistrationToJson(this);
}

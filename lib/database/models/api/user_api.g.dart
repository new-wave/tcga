// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_api.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserAPI _$UserAPIFromJson(Map<String, dynamic> json) {
  return UserAPI(
    links: (json['links'] as List)
        ?.map(
            (e) => e == null ? null : Links.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    id: json['id'] as String ?? '',
    externalId: json['externalId'] as String ?? '',
    role: json['role'] as String ?? '',
    email: json['email'] as String ?? '',
    mobileNumber: json['mobileNumber'] as String ?? '',
    firstName: json['firstName'] as String ?? '',
    lastName: json['lastName'] as String ?? '',
    userAgent: json['userAgent'] as String ?? '',
    recaptchaResponse: json['recaptchaResponse'] as String ?? '',
  );
}

Map<String, dynamic> _$UserAPIToJson(UserAPI instance) => <String, dynamic>{
      'links': instance.links,
      'id': instance.id,
      'externalId': instance.externalId,
      'role': instance.role,
      'email': instance.email,
      'mobileNumber': instance.mobileNumber,
      'firstName': instance.firstName,
      'lastName': instance.lastName,
      'userAgent': instance.userAgent,
      'recaptchaResponse': instance.recaptchaResponse,
    };

Links _$LinksFromJson(Map<String, dynamic> json) {
  return Links(
    json['href'] as String ?? '',
    json['rel'] as String ?? '',
    json['method'] as String ?? '',
  );
}

Map<String, dynamic> _$LinksToJson(Links instance) => <String, dynamic>{
      'href': instance.href,
      'rel': instance.rel,
      'method': instance.method,
    };

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'error_api.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ErrorAPI _$ErrorAPIFromJson(Map<String, dynamic> json) {
  return ErrorAPI(
    type: json['type'] as String,
    title: json['title'] as String,
    status: json['status'] as int,
    detail: json['detail'] as String,
    instance: json['instance'] as String,
  );
}

Map<String, dynamic> _$ErrorAPIToJson(ErrorAPI instance) => <String, dynamic>{
      'type': instance.type,
      'title': instance.title,
      'status': instance.status,
      'detail': instance.detail,
      'instance': instance.instance,
    };

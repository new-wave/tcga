import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import '_hive_type_id.dart';
import 'base_entity.dart';

part 'service_type.g.dart';

@HiveType(
  typeId: HiveTypeId.serviceType,
)
class ServiceType extends QueryOnlyEntity {
  ServiceType({
    this.codeService,
    this.system,
    this.display,
  }) : super(
          code: codeService,
        );

  @HiveField(1)
  String codeService;
  @HiveField(2)
  String system;
  @HiveField(3)
  String display;

  factory ServiceType.fromJson(Map<String, dynamic> json) => ServiceType(
        codeService: json["Code"],
        system: json["System"],
        display: json["Display"],
      );

  Map<String, dynamic> toJson() => {
        "Code": codeService,
        "System": system,
        "Display": display,
      };
}

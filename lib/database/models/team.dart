import 'package:hive/hive.dart';

import 'base_entity.dart';
import '_hive_type_id.dart';

part 'team.g.dart';

@HiveType(typeId: HiveTypeId.team)
class Team extends BaseEntity {
  @HiveField(8)
  String name;
  @HiveField(9)
  int type;
  @HiveField(10)
  String ownerId;
  @HiveField(11)
  int ownerType;
  @HiveField(12)
  List<TeamMember> members;

  Team({
    id,
    this.name,
    this.type,
    this.ownerId,
    this.ownerType,
    this.members,
  }) : super(
          id: id,
        );
}

@HiveType(typeId: HiveTypeId.teamMember)
class TeamMember extends HiveObject {
  @HiveField(0)
  String personId;
  @HiveField(1)
  int role;
  @HiveField(2)
  bool isPrimary;
  @HiveField(3)
  bool isActive;

  TeamMember({
    this.personId,
    this.role,
    this.isPrimary,
    this.isActive = true,
  });
}

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'medication.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class MedicationAdapter extends TypeAdapter<Medication> {
  @override
  final int typeId = 8;

  @override
  Medication read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Medication(
      id: fields[0] as dynamic,
      code: fields[10] as String,
      isSelfReported: fields[11] as bool,
      name: fields[9] as String,
      personId: fields[8] as String,
      route: fields[13] as int,
      status: fields[12] as int,
      time: fields[14] as DateTime,
      recordedDate: fields[15] as DateTime,
    )
      ..version = fields[1] as int
      ..createdOn = fields[2] as DateTime
      ..updatedOn = fields[3] as DateTime
      ..deletedOn = fields[4] as DateTime
      ..syncedOn = fields[5] as DateTime
      ..isDeleted = fields[6] as bool
      ..managingOrganization = fields[7] as String
      ..createdBy = fields[99] as String;
  }

  @override
  void write(BinaryWriter writer, Medication obj) {
    writer
      ..writeByte(17)
      ..writeByte(8)
      ..write(obj.personId)
      ..writeByte(9)
      ..write(obj.name)
      ..writeByte(10)
      ..write(obj.code)
      ..writeByte(11)
      ..write(obj.isSelfReported)
      ..writeByte(12)
      ..write(obj.status)
      ..writeByte(13)
      ..write(obj.route)
      ..writeByte(14)
      ..write(obj.time)
      ..writeByte(15)
      ..write(obj.recordedDate)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.version)
      ..writeByte(2)
      ..write(obj.createdOn)
      ..writeByte(3)
      ..write(obj.updatedOn)
      ..writeByte(4)
      ..write(obj.deletedOn)
      ..writeByte(5)
      ..write(obj.syncedOn)
      ..writeByte(6)
      ..write(obj.isDeleted)
      ..writeByte(7)
      ..write(obj.managingOrganization)
      ..writeByte(99)
      ..write(obj.createdBy);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is MedicationAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

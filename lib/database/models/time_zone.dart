import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import '_hive_type_id.dart';
import 'base_entity.dart';

part 'time_zone.g.dart';

@HiveType(
  typeId: HiveTypeId.timeZone,
)
class TimeZone extends QueryOnlyEntity {
  TimeZone({
    this.timezone,
    this.region,
    String utcOffset,
  }) : super(
          code: utcOffset,
        );

  @HiveField(1)
  String timezone;
  @HiveField(2)
  String region;

  factory TimeZone.fromJson(Map<String, dynamic> json) => TimeZone(
        timezone: json["Timezone"],
        region: json["Region"],
        utcOffset: json["UTCOffset"],
      );

  Map<String, dynamic> toJson() => {
        "Timezone": timezone,
        "Region": region,
        "UTCOffset": code,
      };
}

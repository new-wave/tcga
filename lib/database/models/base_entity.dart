import 'package:hive/hive.dart';

abstract class BaseEntity extends HiveObject {
  @HiveField(0)
  String id;
  @HiveField(1)
  int version;
  @HiveField(2)
  DateTime createdOn;
  @HiveField(3)
  DateTime updatedOn;
  @HiveField(4)
  DateTime deletedOn;
  @HiveField(5)
  DateTime syncedOn;
  @HiveField(6)
  bool isDeleted;
  @HiveField(7)
  String managingOrganization;
  @HiveField(99)
  String createdBy;

  BaseEntity({
    this.id,
    this.createdOn,
    this.deletedOn,
    this.isDeleted,
    this.managingOrganization,
    this.syncedOn,
    this.updatedOn,
    this.version,
    this.createdBy,
  });
}

abstract class QueryOnlyEntity extends HiveObject {
  @HiveField(0)
  String code;

  QueryOnlyEntity({
    this.code,
  });
}

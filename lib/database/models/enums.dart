enum AddressType {
  Home,
  Work,
  MailingOnly,
}

enum RouteInfoScreenType {
  TripDetail,
  ResultRoutes,
}

enum AllergyIntoleranceType {
  Allergy,
  Intolerance,
}

enum ChronicityType {
  Acute,
  Chronic,
}

enum ConditionType {
  Medical,
  Behavioral,
}

enum ConditionStatus {
  Active,
  Inactive,
}

enum AllergyCategory {
  Food,
  Medication,
  Environmental,
  Biologic,
}

enum AllergyCriticality {
  Low,
  High,
  UnableToAssess,
}

enum ParticipantType {
  Admitter, // The practitioner who is responsible for admitting a patient to a patient encounter.
  Attender, // The practitioner that has responsibility for overseeing a patient's care during a patient encounter.
  CallbackContact, // A person or organization who should be contacted for follow-up questions about the act in place of the author.
  Consultant, // An advisor participating in the service by performing evaluations and making recommendations.
  Discharger, // The practitioner who is responsible for the discharge of a patient from a patient encounter.
  Escort, // Only with Transportation services. A person who escorts the patient.
  Referrer, // A person having referred the subject of the service to the performer (referring physician). Typically, a referring physician will receive a report.
  SecondaryPerformer, // A person assisting in an act through his substantial presence and involvement This includes: assistants, technicians, associates, or whatever the job titles may be.
  PrimaryPerformer, // The principal or primary performer of the act.
  Participation, // Indicates that the target of the participation is involved in some manner in the act, but does not qualify how.
  Translator, // A translator who is facilitating communication with the patient during the encounter.
  Emergency, // A person to be contacted in case of an emergency during the encounter.
}

enum EntryWidgetType {
  Normal,
  Email,
  Password,
}

enum RecurrenceEventEditorType {
  This,
  ThisAndFollowing,
  All,
}

enum ActorRole {
  CareRecipient,
  CareGiver,
  Person,
}

enum ParticipantStatus {
  Accepted,
  Declined,
  Tentative,
  NeedsAction,
}

enum ParticipantRequiredType {
  Required,
  Optional,
  InformationOnly,
}

enum AppointmentStatus {
  Pending,
  Scheduled,
  Open,
  Completed,
  Stopped,
  Taken,
  OnHold,
  Discontinued,
}

enum AppointmentType {
  MedicationAdministration,
  ActivityDailyLiving,
  Meeting,
}

enum AppointmentReasonReferenceType {
  Condition,
  Procedure,
  Observation,
  ImmunizationRecommendation,
}

enum AppointmentPriority {
  High,
  Medium,
  Low,
}

enum AppointmentStyleType {
  Checkup,
  Emergency,
  FollowUp,
  Routine,
  Walkin,
}

enum ReminderOption {
  None,
  Email,
  PushNotification,
  All,
}

enum SensitivityType {
  Normal,
  Personal,
  Private,
  Confidential,
}

enum VenueType {
  Phone,
  Video,
  InPerson,
  Unknown,
}

enum ImmunizationStatus {
  Completed,
  EnteredInError,
  NotDone,
}

enum MedicationStatus {
  Active,
  Inactive,
  EnteredInError,
}

enum MedicationRoute {
  Oral,
  Cutaneous,
  Inhalation,
  Intramuscular,
  Intravenous,
  Nasal,
  Ocular,
  Otic,
  Rectal,
  Subcutaneous,
  Sublingual,
  Topical,
  Transdermal,
  Vaginal,
}

enum NoteType {
  AllergyIntolerance,
  Contact,
  Condition,
  Immunization,
  Medication,
  General,
  Appointment,
}

enum NotePriority {
  High,
  Medium,
  Low,
}

enum MaritalStatusType {
//  Annulled,
  Divorced,
//  Interlocutory,
  LegallySeparated,
  Married,
  Polygamous,
//  NeverMarried,
  DomesticPartner,
  Unmarried,
  Widowed,
  Unknown,
}

enum PrefixType {
  Mr,
  Ms,
  Mrs,
  Dr,
}

enum SuffixType {
  MD,
  Second,
  Third,
}

enum AdministrativeGenderType {
  Male,
  Female,
  Other,
  Unknown,
}

enum TeamRole {
  CareGiver,
  CareRecipient,
  CaseManager,
  General,
  EmergencyContact,
  PowerOfAttorney,
  Provider,
  Facility,
  PCP,
  InsuranceCompany,
  FederalAgency,
  SocialWorker,
  Employer,
  FamilyMember,
  Spouse,
  Child,
  Sibling,
  Parent,
  Neighbor,
  Roommate,
  Friend,
  DomesticPartner,
  InLaw,
  Other,
}

enum TeamType {
  Contacts, // builtin type used for CareGiver Contacts and Caregiver Contacts
  EmergencyContacts, // builtin type used for CareGiver Contacts and Care Recipient Emergency Contacts
  Family,
  CareTeam,
  Other,
}

enum TeamOwnerType {
  Recipient,
  Caregiver,
}

class ValueSetType {
  static const String careRecipient = "carerecipients";
  static const String condition = "conditions";
  static const String medication = "medications";
  static const String allergyIntolerance = "allergies";
  static const String immunization = "immunizations";
  static const String route = "routes";
  static const String serviceType = "service_types";
  static const String specialty = "specialties";
  static const String credential = "qualifications";
  static const String contactType = "contact_types";
  static const String timeZones = "time_zones";
}

enum ExpenseCategory {
  Food,
  Parking,
  Transportation,
  Other,
}

enum ExpenseStatus {
  Pending,
  Invoiced,
  Paid,
}

enum TimeFilterOption {
  Today,
  Yesterday,
  ThisWeek,
  LastWeek,
  ThisMonth,
  LastMonth,
}

enum ExpenseTrackingOption {
  Pending,
  Invoiced,
  Paid,
}

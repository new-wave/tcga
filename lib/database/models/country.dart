import 'dart:convert';

import 'package:hive/hive.dart';
import 'package:tcga_flutter/database/models/base_entity.dart';

import '_hive_type_id.dart';
part 'country.g.dart';

@HiveType(
  typeId: HiveTypeId.country,
)
class Country extends QueryOnlyEntity {
  @HiveField(1)
  String isoCode;
  @HiveField(2)
  String flag;
  @HiveField(3)
  String uniIsoCode;
  @HiveField(4)
  String name;
  @HiveField(5)
  String dialCode;
  @HiveField(6)
  List<String> mask;

  Country({
    this.isoCode,
    this.flag,
    this.uniIsoCode,
    this.name,
    this.dialCode,
    this.mask,
  }) : super(
          code: isoCode,
        );

  factory Country.fromJson(Map<String, dynamic> parsedJson) {
    return Country(
        isoCode: parsedJson['IsoCode'],
        flag: parsedJson['Flag'],
        uniIsoCode: parsedJson['UniIsoCode'],
        name: parsedJson['Name'],
        dialCode: parsedJson['DialCode'],
        mask: (parsedJson['Mask'] as List)?.map((e) => e as String)?.toList());
  }
}

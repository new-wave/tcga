// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'immunization.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class ImmunizationAdapter extends TypeAdapter<Immunization> {
  @override
  final int typeId = 6;

  @override
  Immunization read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Immunization(
      id: fields[0] as dynamic,
      doseNumber: fields[14] as int,
      doseNumberString: fields[15] as String,
      isSelfReported: fields[11] as bool,
      name: fields[9] as String,
      note: fields[19] as String,
      occurrenceDateTime: fields[16] as DateTime,
      occurrenceString: fields[17] as String,
      recordedDate: fields[18] as DateTime,
      status: fields[12] as ImmunizationStatus,
      personId: fields[8] as String,
      statusReason: fields[13] as String,
      vaccineCode: fields[10] as String,
    )
      ..version = fields[1] as int
      ..createdOn = fields[2] as DateTime
      ..updatedOn = fields[3] as DateTime
      ..deletedOn = fields[4] as DateTime
      ..syncedOn = fields[5] as DateTime
      ..isDeleted = fields[6] as bool
      ..managingOrganization = fields[7] as String
      ..createdBy = fields[99] as String;
  }

  @override
  void write(BinaryWriter writer, Immunization obj) {
    writer
      ..writeByte(21)
      ..writeByte(8)
      ..write(obj.personId)
      ..writeByte(9)
      ..write(obj.name)
      ..writeByte(10)
      ..write(obj.vaccineCode)
      ..writeByte(11)
      ..write(obj.isSelfReported)
      ..writeByte(12)
      ..write(obj.status)
      ..writeByte(13)
      ..write(obj.statusReason)
      ..writeByte(14)
      ..write(obj.doseNumber)
      ..writeByte(15)
      ..write(obj.doseNumberString)
      ..writeByte(16)
      ..write(obj.occurrenceDateTime)
      ..writeByte(17)
      ..write(obj.occurrenceString)
      ..writeByte(18)
      ..write(obj.recordedDate)
      ..writeByte(19)
      ..write(obj.note)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.version)
      ..writeByte(2)
      ..write(obj.createdOn)
      ..writeByte(3)
      ..write(obj.updatedOn)
      ..writeByte(4)
      ..write(obj.deletedOn)
      ..writeByte(5)
      ..write(obj.syncedOn)
      ..writeByte(6)
      ..write(obj.isDeleted)
      ..writeByte(7)
      ..write(obj.managingOrganization)
      ..writeByte(99)
      ..write(obj.createdBy);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ImmunizationAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'expense.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class ExpenseAdapter extends TypeAdapter<Expense> {
  @override
  final int typeId = 3;

  @override
  Expense read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Expense(
      id: fields[0] as dynamic,
      title: fields[8] as String,
      category: fields[9] as int,
      amount: fields[10] as double,
      currencyCode: fields[11] as String,
      personId: fields[12] as String,
      status: fields[13] as int,
      recordedOn: fields[14] as DateTime,
      photos: (fields[15] as List)?.cast<ExpensePhoto>(),
    )
      ..version = fields[1] as int
      ..createdOn = fields[2] as DateTime
      ..updatedOn = fields[3] as DateTime
      ..deletedOn = fields[4] as DateTime
      ..syncedOn = fields[5] as DateTime
      ..isDeleted = fields[6] as bool
      ..managingOrganization = fields[7] as String
      ..createdBy = fields[99] as String;
  }

  @override
  void write(BinaryWriter writer, Expense obj) {
    writer
      ..writeByte(17)
      ..writeByte(8)
      ..write(obj.title)
      ..writeByte(9)
      ..write(obj.category)
      ..writeByte(10)
      ..write(obj.amount)
      ..writeByte(11)
      ..write(obj.currencyCode)
      ..writeByte(12)
      ..write(obj.personId)
      ..writeByte(13)
      ..write(obj.status)
      ..writeByte(14)
      ..write(obj.recordedOn)
      ..writeByte(15)
      ..write(obj.photos)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.version)
      ..writeByte(2)
      ..write(obj.createdOn)
      ..writeByte(3)
      ..write(obj.updatedOn)
      ..writeByte(4)
      ..write(obj.deletedOn)
      ..writeByte(5)
      ..write(obj.syncedOn)
      ..writeByte(6)
      ..write(obj.isDeleted)
      ..writeByte(7)
      ..write(obj.managingOrganization)
      ..writeByte(99)
      ..write(obj.createdBy);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ExpenseAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

import 'package:hive/hive.dart';

import '_hive_type_id.dart';
import 'base_entity.dart';
import 'expense_photo.dart';

part 'expense.g.dart';

@HiveType(typeId: HiveTypeId.expense)
class Expense extends BaseEntity {
  @HiveField(8)
  String title;
  @HiveField(9)
  int category;
  @HiveField(10)
  double amount;
  @HiveField(11)
  String currencyCode;
  @HiveField(12)
  String personId;
  @HiveField(13)
  int status;
  @HiveField(14)
  DateTime recordedOn;
  @HiveField(15)
  List<ExpensePhoto> photos;

  Expense({
    id,
    this.title,
    this.category,
    this.amount,
    this.currencyCode,
    this.personId,
    this.status,
    this.recordedOn,
    this.photos,
  }) : super(
          id: id,
        );
}

import 'dart:core';

import 'package:hive/hive.dart';

import '_hive_type_id.dart';
import 'base_entity.dart';
import 'enums.dart';

part 'note.g.dart';

@HiveType(typeId: HiveTypeId.note)
class Note extends BaseEntity {
  @HiveField(8)
  int type;
  @HiveField(9)
  String reference; // medication.id, condition.id, allergyintolerance.id, etc.

  @HiveField(10)
  String subject;
  @HiveField(11)
  String content;
  @HiveField(12)
  String createdBy; // user.id
  @HiveField(13)
  String personId;
  @HiveField(14)
  bool isPatientInstruction;
  @HiveField(15)
  bool isConfidential;
  @HiveField(16)
  int priority;
  @HiveField(17)
  bool noteClass;

  Note({
    id,
    this.createdBy,
    this.isConfidential,
    this.isPatientInstruction,
    this.content,
    this.priority,
    this.reference,
    this.subject,
    this.type,
    this.personId,
    this.noteClass,
  }) : super(
          id: id,
        );
}

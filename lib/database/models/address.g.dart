// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'address.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class AddressAdapter extends TypeAdapter<Address> {
  @override
  final int typeId = 15;

  @override
  Address read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Address(
      id: fields[8] as String,
      contactId: fields[9] as String,
      title: fields[10] as String,
      addressContent: fields[11] as String,
      city: fields[12] as String,
      county: fields[13] as String,
      country: fields[14] as String,
      stateProvince: fields[15] as String,
      zipPostalCode: fields[16] as String,
      isPrimary: fields[17] as bool,
      lat: fields[18] as double,
      lng: fields[19] as double,
    )
      ..version = fields[1] as int
      ..createdOn = fields[2] as DateTime
      ..updatedOn = fields[3] as DateTime
      ..deletedOn = fields[4] as DateTime
      ..syncedOn = fields[5] as DateTime
      ..isDeleted = fields[6] as bool
      ..managingOrganization = fields[7] as String
      ..createdBy = fields[99] as String;
  }

  @override
  void write(BinaryWriter writer, Address obj) {
    writer
      ..writeByte(20)
      ..writeByte(8)
      ..write(obj.id)
      ..writeByte(9)
      ..write(obj.contactId)
      ..writeByte(10)
      ..write(obj.title)
      ..writeByte(11)
      ..write(obj.addressContent)
      ..writeByte(12)
      ..write(obj.city)
      ..writeByte(13)
      ..write(obj.county)
      ..writeByte(14)
      ..write(obj.country)
      ..writeByte(15)
      ..write(obj.stateProvince)
      ..writeByte(16)
      ..write(obj.zipPostalCode)
      ..writeByte(17)
      ..write(obj.isPrimary)
      ..writeByte(18)
      ..write(obj.lat)
      ..writeByte(19)
      ..write(obj.lng)
      ..writeByte(1)
      ..write(obj.version)
      ..writeByte(2)
      ..write(obj.createdOn)
      ..writeByte(3)
      ..write(obj.updatedOn)
      ..writeByte(4)
      ..write(obj.deletedOn)
      ..writeByte(5)
      ..write(obj.syncedOn)
      ..writeByte(6)
      ..write(obj.isDeleted)
      ..writeByte(7)
      ..write(obj.managingOrganization)
      ..writeByte(99)
      ..write(obj.createdBy);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is AddressAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

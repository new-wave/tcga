// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class UserModelAdapter extends TypeAdapter<UserModel> {
  @override
  final int typeId = 2;

  @override
  UserModel read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return UserModel(
      id: fields[0] as dynamic,
      emailAddress: fields[8] as String,
      isHealthcareProfessional: fields[11] as bool,
      personId: fields[12] as String,
      password: fields[9] as String,
      salt: fields[10] as String,
      lastAccessedOn: fields[13] as DateTime,
      passwordSalt: fields[14] as String,
      lastName: fields[16] as String,
      firstName: fields[15] as String,
    )
      ..version = fields[1] as int
      ..createdOn = fields[2] as DateTime
      ..updatedOn = fields[3] as DateTime
      ..deletedOn = fields[4] as DateTime
      ..syncedOn = fields[5] as DateTime
      ..isDeleted = fields[6] as bool
      ..managingOrganization = fields[7] as String
      ..createdBy = fields[99] as String;
  }

  @override
  void write(BinaryWriter writer, UserModel obj) {
    writer
      ..writeByte(18)
      ..writeByte(8)
      ..write(obj.emailAddress)
      ..writeByte(9)
      ..write(obj.password)
      ..writeByte(10)
      ..write(obj.salt)
      ..writeByte(11)
      ..write(obj.isHealthcareProfessional)
      ..writeByte(12)
      ..write(obj.personId)
      ..writeByte(13)
      ..write(obj.lastAccessedOn)
      ..writeByte(14)
      ..write(obj.passwordSalt)
      ..writeByte(15)
      ..write(obj.firstName)
      ..writeByte(16)
      ..write(obj.lastName)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.version)
      ..writeByte(2)
      ..write(obj.createdOn)
      ..writeByte(3)
      ..write(obj.updatedOn)
      ..writeByte(4)
      ..write(obj.deletedOn)
      ..writeByte(5)
      ..write(obj.syncedOn)
      ..writeByte(6)
      ..write(obj.isDeleted)
      ..writeByte(7)
      ..write(obj.managingOrganization)
      ..writeByte(99)
      ..write(obj.createdBy);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is UserModelAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

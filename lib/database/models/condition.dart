import 'dart:core';

import 'package:hive/hive.dart';

import '_hive_type_id.dart';
import 'base_entity.dart';

part 'condition.g.dart';

@HiveType(typeId: HiveTypeId.condition)
class Condition extends BaseEntity {
  @HiveField(8)
  String personId;
  @HiveField(9)
  int type;
  @HiveField(10)
  int status;
  @HiveField(11)
  String name;
  @HiveField(12)
  String code;
  @HiveField(13)
  bool isSelfReported;
  @HiveField(14)
  int chronicity;
  @HiveField(15)
  DateTime onsetDateTime;
  @HiveField(16)
  DateTime abatementDateTime;
  @HiveField(17)
  DateTime recordedDate;

  @HiveField(18)
  bool isPrimary;

  Condition({
    id,
    this.abatementDateTime,
    this.chronicity,
    this.code,
    this.isPrimary,
    this.isSelfReported,
    this.name,
    this.onsetDateTime,
    this.personId,
    this.recordedDate,
    this.status,
    this.type,
  }) : super(
          id: id,
        );
}

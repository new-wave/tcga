// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'expense_photo.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class ExpensePhotoAdapter extends TypeAdapter<ExpensePhoto> {
  @override
  final int typeId = 20;

  @override
  ExpensePhoto read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return ExpensePhoto(
      url: fields[0] as String,
    );
  }

  @override
  void write(BinaryWriter writer, ExpensePhoto obj) {
    writer
      ..writeByte(1)
      ..writeByte(0)
      ..write(obj.url);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ExpensePhotoAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

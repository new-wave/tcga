// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'value_set.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class ValueSetAdapter extends TypeAdapter<ValueSet> {
  @override
  final int typeId = 18;

  @override
  ValueSet read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return ValueSet(
      code: fields[0] as dynamic,
      name: fields[1] as String,
      system: fields[2] as String,
      display: fields[3] as String,
    );
  }

  @override
  void write(BinaryWriter writer, ValueSet obj) {
    writer
      ..writeByte(4)
      ..writeByte(1)
      ..write(obj.name)
      ..writeByte(2)
      ..write(obj.system)
      ..writeByte(3)
      ..write(obj.display)
      ..writeByte(0)
      ..write(obj.code);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ValueSetAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

import 'dart:core';

import 'package:flutter/material.dart';
import 'package:hive/hive.dart';

import '_hive_type_id.dart';
import 'base_entity.dart';
import 'enums.dart';

part 'appointment.g.dart';

@HiveType(typeId: HiveTypeId.appointmentParticipant)
class Participant extends HiveObject {
  @HiveField(0)
  int type; //ParticipantType
  @HiveField(1)
  int role; //ActorRole
  @HiveField(2)
  String reference; // ResourceId(s) in Syncfusion, Person.Id in LiteDB

  @HiveField(3)
  int status; //ParticipantStatus
  @HiveField(4)
  int isRequired; //ParticipantRequiredType
  @HiveField(5)
  bool isOrganizer;
  Participant({
    this.isOrganizer,
    this.reference,
    this.isRequired,
    this.role,
    this.status,
    this.type,
  });
}

// Derived from: https://help.syncfusion.com/cr/cref_files/xamarin/Syncfusion.SfSchedule.XForms~Syncfusion.SfSchedule.XForms.ScheduleAppointment_members.html
// Notes are recorded in Notes collection
@HiveType(typeId: HiveTypeId.appointment)
class Appointment extends BaseEntity {
  @HiveField(8)
  String subject; // Short Description for the calendar view
  @HiveField(9)
  String description; // Description for the event details view
  @HiveField(10)
  int type; // Identity the CareGiverApp Type
  @HiveField(11)
  String
      reference; // If Type = Medication, Medication.id for a Medication Schedule
  @HiveField(12)
  SensitivityType sensitivity;
  @HiveField(13)
  List<Participant> participants;
  @HiveField(14)
  int capacity;
  @HiveField(15)
  DateTime startTime; // 3:00pm
  @HiveField(16)
  DateTime endTime;
  @HiveField(17)
  DateTime actualStartTime;
  @HiveField(18)
  DateTime actualEndTime;
  @HiveField(19)
  String recurrenceRule; // FREQ=DAILY, FREQ=MONTHLY
  @HiveField(20)
  String recurrenceId;
  @HiveField(21)
  List<DateTime> recurrenceExceptionDates;
  @HiveField(22)
  Color
      color; // Xamarin.Forms Hex value of the color to use for the appointment
  @HiveField(23)
  bool isAllDay;
  @HiveField(24)
  int reminderOption;
  @HiveField(25)
  int reminderMinutesBeforeStart;
  @HiveField(26)
  int venue;

  // FUTURE - physical address or online meeting location information
  //  Location Location ;

  // FHIR related
  // ===================================================================================
  // Note:  Any Code properties come from loaded ValueSets
  @HiveField(27)
  int status;

  //https://www.hl7.org/fhir/valueset-appointment-cancellation-reason.html
  @HiveField(28)
  String
      cancellationReasonCode; // The coded reason for the appointment being cancelled

  //https://www.hl7.org/fhir/valueset-service-category.html
  // DERIVED from service type
  // String ServiceCategoryCode ;    //A broad categorization of the service that is to be performed during this appointment

  //https://www.hl7.org/fhir/valueset-service-type.html
  @HiveField(29)
  String
      serviceTypeCode; // The specific service that is to be performed during this appointment

  //https://www.hl7.org/fhir/valueset-c80-practice-codes.html
  // DERIVED from service type
  // String SpecialtyCode ;         // The specialty of a practitioner that would be required to perform the service requested in this appointment

  //https://www.hl7.org/fhir/v2/0276/index.html
  @HiveField(30)
  int appointmentStyle; // The style of appointment or patient that has been booked in the slot (not service type)

  //https://www.hl7.org/fhir/valueset-encounter-reason.html
  // EXCLUDED from MVP, too medical/technical
  // String ReasonCode ;            // Coded reason this appointment is scheduled
  @HiveField(31)
  AppointmentReasonReferenceType
      reasonReferenceType; // Reason the appointment is to take place (resource)
  @HiveField(32)
  String reasonReference; // ""
  @HiveField(33)
  int priority; // Used to make informed decisions if needing to re-prioritize
  @HiveField(34)
  String
      patientInstructions; // Detailed information and instructions for the patient
  @HiveField(35)
  String timeZone;
  @HiveField(36)
  List<TimeSlot> timeSlots;

  Appointment({
    id,
    this.actualEndTime,
    this.actualStartTime,
    this.appointmentStyle,
    this.cancellationReasonCode,
    this.capacity,
    this.color,
    this.description,
    this.endTime,
    this.isAllDay,
    this.participants,
    this.patientInstructions,
    this.priority,
    this.reasonReference,
    this.reasonReferenceType,
    this.recurrenceExceptionDates,
    this.recurrenceId,
    this.recurrenceRule,
    this.reference,
    this.reminderMinutesBeforeStart,
    this.reminderOption,
    this.sensitivity,
    this.serviceTypeCode,
    this.startTime,
    this.status,
    this.subject,
    this.timeSlots,
    this.timeZone,
    this.type,
    this.venue,
  }) : super(
          id: id,
        );
}

@HiveType(typeId: HiveTypeId.appointmentTimeSlot)
class TimeSlot extends HiveObject {
  @HiveField(0)
  int durationInMinute;

  @HiveField(1)
  DateTime startAt;

  TimeSlot({
    this.durationInMinute,
    this.startAt,
  });
}

import 'package:hive/hive.dart';

import '_hive_type_id.dart';

part 'geo_point.g.dart';

@HiveType(typeId: HiveTypeId.geoPoint)
class GeoPoint extends HiveObject {
  @HiveField(0)
  final double latitude;
  @HiveField(1)
  final double longitude;

  GeoPoint({
    this.latitude,
    this.longitude,
  });
}

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'condition.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class ConditionAdapter extends TypeAdapter<Condition> {
  @override
  final int typeId = 7;

  @override
  Condition read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Condition(
      id: fields[0] as dynamic,
      abatementDateTime: fields[16] as DateTime,
      chronicity: fields[14] as int,
      code: fields[12] as String,
      isPrimary: fields[18] as bool,
      isSelfReported: fields[13] as bool,
      name: fields[11] as String,
      onsetDateTime: fields[15] as DateTime,
      personId: fields[8] as String,
      recordedDate: fields[17] as DateTime,
      status: fields[10] as int,
      type: fields[9] as int,
    )
      ..version = fields[1] as int
      ..createdOn = fields[2] as DateTime
      ..updatedOn = fields[3] as DateTime
      ..deletedOn = fields[4] as DateTime
      ..syncedOn = fields[5] as DateTime
      ..isDeleted = fields[6] as bool
      ..managingOrganization = fields[7] as String
      ..createdBy = fields[99] as String;
  }

  @override
  void write(BinaryWriter writer, Condition obj) {
    writer
      ..writeByte(20)
      ..writeByte(8)
      ..write(obj.personId)
      ..writeByte(9)
      ..write(obj.type)
      ..writeByte(10)
      ..write(obj.status)
      ..writeByte(11)
      ..write(obj.name)
      ..writeByte(12)
      ..write(obj.code)
      ..writeByte(13)
      ..write(obj.isSelfReported)
      ..writeByte(14)
      ..write(obj.chronicity)
      ..writeByte(15)
      ..write(obj.onsetDateTime)
      ..writeByte(16)
      ..write(obj.abatementDateTime)
      ..writeByte(17)
      ..write(obj.recordedDate)
      ..writeByte(18)
      ..write(obj.isPrimary)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.version)
      ..writeByte(2)
      ..write(obj.createdOn)
      ..writeByte(3)
      ..write(obj.updatedOn)
      ..writeByte(4)
      ..write(obj.deletedOn)
      ..writeByte(5)
      ..write(obj.syncedOn)
      ..writeByte(6)
      ..write(obj.isDeleted)
      ..writeByte(7)
      ..write(obj.managingOrganization)
      ..writeByte(99)
      ..write(obj.createdBy);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ConditionAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

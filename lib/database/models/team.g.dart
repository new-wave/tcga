// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'team.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class TeamAdapter extends TypeAdapter<Team> {
  @override
  final int typeId = 4;

  @override
  Team read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Team(
      id: fields[0] as dynamic,
      name: fields[8] as String,
      type: fields[9] as int,
      ownerId: fields[10] as String,
      ownerType: fields[11] as int,
      members: (fields[12] as List)?.cast<TeamMember>(),
    )
      ..version = fields[1] as int
      ..createdOn = fields[2] as DateTime
      ..updatedOn = fields[3] as DateTime
      ..deletedOn = fields[4] as DateTime
      ..syncedOn = fields[5] as DateTime
      ..isDeleted = fields[6] as bool
      ..managingOrganization = fields[7] as String
      ..createdBy = fields[99] as String;
  }

  @override
  void write(BinaryWriter writer, Team obj) {
    writer
      ..writeByte(14)
      ..writeByte(8)
      ..write(obj.name)
      ..writeByte(9)
      ..write(obj.type)
      ..writeByte(10)
      ..write(obj.ownerId)
      ..writeByte(11)
      ..write(obj.ownerType)
      ..writeByte(12)
      ..write(obj.members)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.version)
      ..writeByte(2)
      ..write(obj.createdOn)
      ..writeByte(3)
      ..write(obj.updatedOn)
      ..writeByte(4)
      ..write(obj.deletedOn)
      ..writeByte(5)
      ..write(obj.syncedOn)
      ..writeByte(6)
      ..write(obj.isDeleted)
      ..writeByte(7)
      ..write(obj.managingOrganization)
      ..writeByte(99)
      ..write(obj.createdBy);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is TeamAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class TeamMemberAdapter extends TypeAdapter<TeamMember> {
  @override
  final int typeId = 5;

  @override
  TeamMember read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return TeamMember(
      personId: fields[0] as String,
      role: fields[1] as int,
      isPrimary: fields[2] as bool,
      isActive: fields[3] as bool,
    );
  }

  @override
  void write(BinaryWriter writer, TeamMember obj) {
    writer
      ..writeByte(4)
      ..writeByte(0)
      ..write(obj.personId)
      ..writeByte(1)
      ..write(obj.role)
      ..writeByte(2)
      ..write(obj.isPrimary)
      ..writeByte(3)
      ..write(obj.isActive);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is TeamMemberAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'medication_code.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class MedicationCodeAdapter extends TypeAdapter<MedicationCode> {
  @override
  final int typeId = 17;

  @override
  MedicationCode read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return MedicationCode(
      code: fields[0] as dynamic,
      proprietaryName: fields[1] as String,
      genericName: fields[2] as String,
      system: fields[3] as String,
      dosageForm: fields[4] as String,
      labelerName: fields[5] as String,
      substanceName: fields[6] as String,
      activeNumeratorStrength: fields[7] as String,
      activeIngredientUnit: fields[8] as String,
      route: fields[9] as String,
    );
  }

  @override
  void write(BinaryWriter writer, MedicationCode obj) {
    writer
      ..writeByte(10)
      ..writeByte(1)
      ..write(obj.proprietaryName)
      ..writeByte(2)
      ..write(obj.genericName)
      ..writeByte(3)
      ..write(obj.system)
      ..writeByte(4)
      ..write(obj.dosageForm)
      ..writeByte(5)
      ..write(obj.labelerName)
      ..writeByte(6)
      ..write(obj.substanceName)
      ..writeByte(7)
      ..write(obj.activeNumeratorStrength)
      ..writeByte(8)
      ..write(obj.activeIngredientUnit)
      ..writeByte(9)
      ..write(obj.route)
      ..writeByte(0)
      ..write(obj.code);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is MedicationCodeAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

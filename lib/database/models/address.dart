import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:hive/hive.dart';
import 'package:uuid/uuid.dart';

import '_hive_type_id.dart';
import 'base_entity.dart';
import 'geo_point.dart';

part 'address.g.dart';

@HiveType(typeId: HiveTypeId.address)
class Address extends BaseEntity {
  @HiveField(8)
  String id;
  @HiveField(9)
  String contactId;
  @HiveField(10)
  String title;
  @HiveField(11)
  String addressContent;
  @HiveField(12)
  String city;
  @HiveField(13)
  String county;
  @HiveField(14)
  String country;
  @HiveField(15)
  String stateProvince;
  @HiveField(16)
  String zipPostalCode;
  @HiveField(17)
  bool isPrimary;
  @HiveField(18)
  double lat;
  @HiveField(19)
  double lng;

  Address({
    this.id,
    this.contactId,
    this.title,
    this.addressContent,
    this.city,
    this.county,
    this.country,
    this.stateProvince,
    this.zipPostalCode,
    this.isPrimary,
    this.lat,
    this.lng,
  }) {
    id ??= Uuid().v4();
  }

  bool contains(String keyword) {
    bool isOk = false;
    if (addressContent != null) {
      if (addressContent.toLowerCase().contains(keyword.trim())) {
        isOk = true;
        return isOk;
      }
    }

    if (title != null) {
      if (title.toLowerCase().contains(keyword.trim())) {
        isOk = true;
        return isOk;
      }
    }

    if (city != null) {
      if (city.toLowerCase().contains(keyword.trim())) {
        isOk = true;
        return isOk;
      }
    }

    if (stateProvince != null) {
      if (stateProvince.toLowerCase().contains(keyword.trim())) {
        isOk = true;
        return isOk;
      }
    }

    if (country != null) {
      if (country.toLowerCase().contains(keyword.trim())) {
        isOk = true;
        return isOk;
      }
    }
    return isOk;
  }
}

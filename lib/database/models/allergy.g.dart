// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'allergy.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class AllergyAdapter extends TypeAdapter<Allergy> {
  @override
  final int typeId = 9;

  @override
  Allergy read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Allergy(
      id: fields[0] as dynamic,
      category: fields[13] as int,
      code: fields[10] as String,
      criticality: fields[14] as int,
      isSelfReported: fields[11] as bool,
      lastOccurrence: fields[16] as DateTime,
      name: fields[9] as String,
      personId: fields[8] as String,
      lastOccuredDate: fields[15] as DateTime,
      type: fields[12] as int,
    )
      ..version = fields[1] as int
      ..createdOn = fields[2] as DateTime
      ..updatedOn = fields[3] as DateTime
      ..deletedOn = fields[4] as DateTime
      ..syncedOn = fields[5] as DateTime
      ..isDeleted = fields[6] as bool
      ..managingOrganization = fields[7] as String
      ..createdBy = fields[99] as String;
  }

  @override
  void write(BinaryWriter writer, Allergy obj) {
    writer
      ..writeByte(18)
      ..writeByte(8)
      ..write(obj.personId)
      ..writeByte(9)
      ..write(obj.name)
      ..writeByte(10)
      ..write(obj.code)
      ..writeByte(11)
      ..write(obj.isSelfReported)
      ..writeByte(12)
      ..write(obj.type)
      ..writeByte(13)
      ..write(obj.category)
      ..writeByte(14)
      ..write(obj.criticality)
      ..writeByte(15)
      ..write(obj.lastOccuredDate)
      ..writeByte(16)
      ..write(obj.lastOccurrence)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.version)
      ..writeByte(2)
      ..write(obj.createdOn)
      ..writeByte(3)
      ..write(obj.updatedOn)
      ..writeByte(4)
      ..write(obj.deletedOn)
      ..writeByte(5)
      ..write(obj.syncedOn)
      ..writeByte(6)
      ..write(obj.isDeleted)
      ..writeByte(7)
      ..write(obj.managingOrganization)
      ..writeByte(99)
      ..write(obj.createdBy);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is AllergyAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

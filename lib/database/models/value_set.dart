import 'dart:convert';

import 'package:hive/hive.dart';

import '_hive_type_id.dart';
import 'base_entity.dart';

part 'value_set.g.dart';

ValueSet valueSetFromJson(String str) => ValueSet.fromJson(json.decode(str));

String valueSetToJson(ValueSet data) => json.encode(data.toJson());

@HiveType(typeId: HiveTypeId.valueSet)
class ValueSet extends QueryOnlyEntity {
  ValueSet({
    code,
    this.name,
    this.system,
    this.display,
  }) : super(
          code: code,
        );

  @HiveField(1)
  String name;
  @HiveField(2)
  String system;
  @HiveField(3)
  String display;

  factory ValueSet.fromJson(Map<String, dynamic> json) => ValueSet(
        name: json["Name"],
        code: json["Code"],
        system: json["System"],
        display: json["Display"],
      );

  Map<String, dynamic> toJson() => {
        "Name": name,
        "Code": code,
        "System": system,
        "Display": display,
      };
}

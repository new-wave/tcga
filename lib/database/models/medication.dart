import 'dart:core';

import 'package:hive/hive.dart';

import '_hive_type_id.dart';
import 'base_entity.dart';
import 'enums.dart';

part 'medication.g.dart';

@HiveType(typeId: HiveTypeId.medication)
class Medication extends BaseEntity {
  @HiveField(8)
  String personId;
  @HiveField(9)
  String name;
  @HiveField(10)
  String code;
  @HiveField(11)
  bool isSelfReported;
  @HiveField(12)
  int status;
  @HiveField(13)
  int route;
  @HiveField(14)
  DateTime time;
  @HiveField(15)
  DateTime recordedDate;

  Medication({
    id,
    this.code,
    this.isSelfReported,
    this.name,
    this.personId,
    this.route,
    this.status,
    this.time,
    this.recordedDate,
  }) : super(
          id: id,
        );
}

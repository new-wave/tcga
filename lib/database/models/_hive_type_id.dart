class HiveTypeId {
  static const int person = 1;
  static const int user = 2;
  static const int expense = 3;
  static const int team = 4;
  static const int teamMember = 5;

  static const int immunization = 6;
  static const int condition = 7;
  static const int medication = 8;
  static const int allergy = 9;
  static const int note = 10;

  static const int appointment = 11;
  static const int appointmentParticipant = 13;
  static const int appointmentTimeSlot = 14;

  static const int address = 15;
  static const int geoPoint = 16;

  static const int medicationCode = 17;
  static const int valueSet = 18;

  static const int language = 19;
  static const int expensePhoto = 20;

  static const int timeZone = 21;

  static const int trip = 22;

  static const int address_trip = 23;

  static const int country = 24;

  static const int serviceType = 25;
}

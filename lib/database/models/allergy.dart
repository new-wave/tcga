import 'package:hive/hive.dart';

import '_hive_type_id.dart';
import 'base_entity.dart';
import 'enums.dart';

part 'allergy.g.dart';

@HiveType(typeId: HiveTypeId.allergy)
class Allergy extends BaseEntity {
  @HiveField(8)
  String personId;

  @HiveField(9)
  String name;
  @HiveField(10)
  String code;
  @HiveField(11)
  bool isSelfReported;
  @HiveField(12)
  int type;
  @HiveField(13)
  int category;
  @HiveField(14)
  int criticality;
  @HiveField(15)
  DateTime lastOccuredDate;
  @HiveField(16)
  DateTime lastOccurrence;

  Allergy({
    id,
    this.category,
    this.code,
    this.criticality,
    this.isSelfReported,
    this.lastOccurrence,
    this.name,
    this.personId,
    this.lastOccuredDate,
    this.type,
  }) : super(
          id: id,
        );
}

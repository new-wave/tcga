// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'note.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class NoteAdapter extends TypeAdapter<Note> {
  @override
  final int typeId = 10;

  @override
  Note read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Note(
      id: fields[0] as dynamic,
      createdBy: fields[12] as String,
      isConfidential: fields[15] as bool,
      isPatientInstruction: fields[14] as bool,
      content: fields[11] as String,
      priority: fields[16] as int,
      reference: fields[9] as String,
      subject: fields[10] as String,
      type: fields[8] as int,
      personId: fields[13] as String,
      noteClass: fields[17] as bool,
    )
      ..version = fields[1] as int
      ..createdOn = fields[2] as DateTime
      ..updatedOn = fields[3] as DateTime
      ..deletedOn = fields[4] as DateTime
      ..syncedOn = fields[5] as DateTime
      ..isDeleted = fields[6] as bool
      ..managingOrganization = fields[7] as String;
  }

  @override
  void write(BinaryWriter writer, Note obj) {
    writer
      ..writeByte(18)
      ..writeByte(8)
      ..write(obj.type)
      ..writeByte(9)
      ..write(obj.reference)
      ..writeByte(10)
      ..write(obj.subject)
      ..writeByte(11)
      ..write(obj.content)
      ..writeByte(12)
      ..write(obj.createdBy)
      ..writeByte(13)
      ..write(obj.personId)
      ..writeByte(14)
      ..write(obj.isPatientInstruction)
      ..writeByte(15)
      ..write(obj.isConfidential)
      ..writeByte(16)
      ..write(obj.priority)
      ..writeByte(17)
      ..write(obj.noteClass)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.version)
      ..writeByte(2)
      ..write(obj.createdOn)
      ..writeByte(3)
      ..write(obj.updatedOn)
      ..writeByte(4)
      ..write(obj.deletedOn)
      ..writeByte(5)
      ..write(obj.syncedOn)
      ..writeByte(6)
      ..write(obj.isDeleted)
      ..writeByte(7)
      ..write(obj.managingOrganization);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is NoteAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

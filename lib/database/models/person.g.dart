// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'person.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class PersonAdapter extends TypeAdapter<Person> {
  @override
  final int typeId = 1;

  @override
  Person read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Person(
      id: fields[0] as dynamic,
      firstName: fields[9] as String,
      middleName: fields[10] as String,
      lastName: fields[11] as String,
      organization: fields[8] as String,
      prefix: fields[12] as int,
      suffix: fields[13] as int,
      gender: fields[14] as int,
      birthDate: fields[15] as DateTime,
      maritalStatus: fields[16] as int,
      photo: fields[19] as String,
      website: fields[20] as String,
      distance: fields[21] as String,
      mobilePhone: fields[17] as String,
      email: fields[18] as String,
      addresses: (fields[22] as List)?.cast<Address>(),
      languages: (fields[23] as List)?.cast<Language>(),
      credentials: (fields[24] as List)?.cast<ValueSet>(),
      specialties: (fields[25] as List)?.cast<ValueSet>(),
      programs: (fields[26] as List)?.cast<String>(),
      countryCode: fields[27] as String,
    )
      ..version = fields[1] as int
      ..createdOn = fields[2] as DateTime
      ..updatedOn = fields[3] as DateTime
      ..deletedOn = fields[4] as DateTime
      ..syncedOn = fields[5] as DateTime
      ..isDeleted = fields[6] as bool
      ..managingOrganization = fields[7] as String
      ..createdBy = fields[99] as String;
  }

  @override
  void write(BinaryWriter writer, Person obj) {
    writer
      ..writeByte(29)
      ..writeByte(8)
      ..write(obj.organization)
      ..writeByte(9)
      ..write(obj.firstName)
      ..writeByte(10)
      ..write(obj.middleName)
      ..writeByte(11)
      ..write(obj.lastName)
      ..writeByte(12)
      ..write(obj.prefix)
      ..writeByte(13)
      ..write(obj.suffix)
      ..writeByte(14)
      ..write(obj.gender)
      ..writeByte(15)
      ..write(obj.birthDate)
      ..writeByte(16)
      ..write(obj.maritalStatus)
      ..writeByte(17)
      ..write(obj.mobilePhone)
      ..writeByte(18)
      ..write(obj.email)
      ..writeByte(19)
      ..write(obj.photo)
      ..writeByte(20)
      ..write(obj.website)
      ..writeByte(21)
      ..write(obj.distance)
      ..writeByte(22)
      ..write(obj.addresses)
      ..writeByte(23)
      ..write(obj.languages)
      ..writeByte(24)
      ..write(obj.credentials)
      ..writeByte(25)
      ..write(obj.specialties)
      ..writeByte(26)
      ..write(obj.programs)
      ..writeByte(27)
      ..write(obj.countryCode)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.version)
      ..writeByte(2)
      ..write(obj.createdOn)
      ..writeByte(3)
      ..write(obj.updatedOn)
      ..writeByte(4)
      ..write(obj.deletedOn)
      ..writeByte(5)
      ..write(obj.syncedOn)
      ..writeByte(6)
      ..write(obj.isDeleted)
      ..writeByte(7)
      ..write(obj.managingOrganization)
      ..writeByte(99)
      ..write(obj.createdBy);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is PersonAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

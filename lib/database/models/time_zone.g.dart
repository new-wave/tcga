// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'time_zone.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class TimeZoneAdapter extends TypeAdapter<TimeZone> {
  @override
  final int typeId = 21;

  @override
  TimeZone read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return TimeZone(
      timezone: fields[1] as String,
      region: fields[2] as String,
    )..code = fields[0] as String;
  }

  @override
  void write(BinaryWriter writer, TimeZone obj) {
    writer
      ..writeByte(3)
      ..writeByte(1)
      ..write(obj.timezone)
      ..writeByte(2)
      ..write(obj.region)
      ..writeByte(0)
      ..write(obj.code);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is TimeZoneAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

import 'package:hive/hive.dart';

import '_hive_type_id.dart';
import 'base_entity.dart';
import 'enums.dart';

part 'immunization.g.dart';

@HiveType(typeId: HiveTypeId.immunization)
class Immunization extends BaseEntity {
  @HiveField(8)
  String personId;
  @HiveField(9)
  String name;
  @HiveField(10)
  String vaccineCode;
  @HiveField(11)
  bool isSelfReported;
  @HiveField(12)
  ImmunizationStatus status;
  @HiveField(13)
  String statusReason;
  @HiveField(14)
  int doseNumber;
  @HiveField(15)
  String doseNumberString;
  @HiveField(16)
  DateTime occurrenceDateTime;
  @HiveField(17)
  String occurrenceString;
  @HiveField(18)
  DateTime recordedDate;
  @HiveField(19)
  String note;

  Immunization({
    id,
    this.doseNumber,
    this.doseNumberString,
    this.isSelfReported,
    this.name,
    this.note,
    this.occurrenceDateTime,
    this.occurrenceString,
    this.recordedDate,
    this.status,
    this.personId,
    this.statusReason,
    this.vaccineCode,
  }) : super(
          id: id,
        );
}

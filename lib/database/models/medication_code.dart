import 'dart:convert';

import 'package:hive/hive.dart';

import '_hive_type_id.dart';
import 'base_entity.dart';

part 'medication_code.g.dart';

MedicationCode medicationCodeFromJson(String str) =>
    MedicationCode.fromJson(json.decode(str));

String medicationCodeToJson(MedicationCode data) => json.encode(data.toJson());

@HiveType(typeId: HiveTypeId.medicationCode)
class MedicationCode extends QueryOnlyEntity {
  MedicationCode({
    code,
    this.proprietaryName,
    this.genericName,
    this.system,
    this.dosageForm,
    this.labelerName,
    this.substanceName,
    this.activeNumeratorStrength,
    this.activeIngredientUnit,
    this.route,
  }) : super(
          code: code,
        );

  @HiveField(1)
  String proprietaryName;
  @HiveField(2)
  String genericName;
  @HiveField(3)
  String system;
  @HiveField(4)
  String dosageForm;
  @HiveField(5)
  String labelerName;
  @HiveField(6)
  String substanceName;
  @HiveField(7)
  String activeNumeratorStrength;
  @HiveField(8)
  String activeIngredientUnit;
  @HiveField(9)
  String route;

  factory MedicationCode.fromJson(Map<String, dynamic> json) => MedicationCode(
        code: json["Code"],
        proprietaryName: json["ProprietaryName"],
        genericName: json["GenericName"],
        system: json["System"],
        dosageForm: json["DosageForm"],
        labelerName: json["LabelerName"],
        substanceName: json["SubstanceName"],
        activeNumeratorStrength: json["ActiveNumeratorStrength"],
        activeIngredientUnit: json["ActiveIngredientUnit"],
        route: json["Route"],
      );

  Map<String, dynamic> toJson() => {
        "Code": code,
        "ProprietaryName": proprietaryName,
        "GenericName": genericName,
        "System": system,
        "DosageForm": dosageForm,
        "LabelerName": labelerName,
        "SubstanceName": substanceName,
        "ActiveNumeratorStrength": activeNumeratorStrength,
        "ActiveIngredientUnit": activeIngredientUnit,
        "Route": route,
      };
}

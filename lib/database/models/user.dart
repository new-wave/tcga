import 'package:hive/hive.dart';

import 'base_entity.dart';
import '_hive_type_id.dart';

part 'user.g.dart';

@HiveType(typeId: HiveTypeId.user)
class UserModel extends BaseEntity {
  @HiveField(8)
  String emailAddress;

  @HiveField(9)
  String password;
  @HiveField(10)
  String salt;

  @HiveField(11)
  bool isHealthcareProfessional;

  @HiveField(12)
  String personId;

  @HiveField(13)
  DateTime lastAccessedOn;

  @HiveField(14)
  String passwordSalt;

  @HiveField(15)
  String firstName;

  @HiveField(16)
  String lastName;

  UserModel({
    id,
    this.emailAddress,
    this.isHealthcareProfessional,
    this.personId,
    this.password,
    this.salt,
    this.lastAccessedOn,
    this.passwordSalt,
    this.lastName,
    this.firstName,
  });
}

import 'package:hive/hive.dart';

import 'address.dart';
import 'base_entity.dart';
import '_hive_type_id.dart';

part 'trip.g.dart';

@HiveType(typeId: HiveTypeId.trip)
class Trip extends BaseEntity {
  @HiveField(8)
  Address fromAddress;
  @HiveField(9)
  Address toAddress;
  @HiveField(10)
  double miles;
  @HiveField(11)
  String duration;
  @HiveField(12)
  String description;

  Trip({
    id,
    this.fromAddress,
    this.toAddress,
    this.miles,
    this.duration,
    this.description,
  }) : super(
          id: id,
        );
  
  bool contains(String keyword){
    bool isOk = false;
    if (fromAddress != null) {
      if (fromAddress.addressContent != null) {
        if (fromAddress.addressContent.toLowerCase().contains(keyword.trim())) {
          isOk = true;
          return isOk;
        }
      }

      if (fromAddress.title != null) {
        if (fromAddress.title.toLowerCase().contains(keyword.trim())) {
          isOk = true;
          return isOk;
        }
      }

      if (fromAddress.city != null) {
        if (fromAddress.city.toLowerCase().contains(keyword.trim())) {
          isOk = true;
          return isOk;
        }
      }

      if (fromAddress.stateProvince != null) {
        if (fromAddress.stateProvince.toLowerCase().contains(keyword.trim())) {
          isOk = true;
          return isOk;
        }
      }

      if (fromAddress.country != null) {
        if (fromAddress.country.toLowerCase().contains(keyword.trim())) {
          isOk = true;
          return isOk;
        }
      }
    }
    if (toAddress != null) {
      if (toAddress.addressContent != null) {
        if (toAddress.addressContent.toLowerCase().contains(keyword.trim())) {
          isOk = true;
          return isOk;
        }
      }

      if (toAddress.title != null) {
        if (toAddress.title.toLowerCase().contains(keyword.trim())) {
          isOk = true;
          return isOk;
        }
      }

      if (toAddress.city != null) {
        if (toAddress.city.toLowerCase().contains(keyword.trim())) {
          isOk = true;
          return isOk;
        }
      }

      if (toAddress.stateProvince != null) {
        if (toAddress.stateProvince.toLowerCase().contains(keyword.trim())) {
          isOk = true;
          return isOk;
        }
      }

      if (toAddress.country != null) {
        if (toAddress.country.toLowerCase().contains(keyword.trim())) {
          isOk = true;
          return isOk;
        }
      }
    }
    return isOk;
  }
}

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'country.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class CountryAdapter extends TypeAdapter<Country> {
  @override
  final int typeId = 24;

  @override
  Country read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Country(
      isoCode: fields[1] as String,
      flag: fields[2] as String,
      uniIsoCode: fields[3] as String,
      name: fields[4] as String,
      dialCode: fields[5] as String,
      mask: (fields[6] as List)?.cast<String>(),
    )..code = fields[0] as String;
  }

  @override
  void write(BinaryWriter writer, Country obj) {
    writer
      ..writeByte(7)
      ..writeByte(1)
      ..write(obj.isoCode)
      ..writeByte(2)
      ..write(obj.flag)
      ..writeByte(3)
      ..write(obj.uniIsoCode)
      ..writeByte(4)
      ..write(obj.name)
      ..writeByte(5)
      ..write(obj.dialCode)
      ..writeByte(6)
      ..write(obj.mask)
      ..writeByte(0)
      ..write(obj.code);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CountryAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

import 'package:json_annotation/json_annotation.dart';
import 'distance_duration.dart';
part 'element_matrix.g.dart';

@JsonSerializable(nullable: true)
class ElementMatrix {
  @JsonKey(name: 'distance')
  DistanceDuration distance;
  @JsonKey(name: 'duration')
  DistanceDuration duration;
  String status;

  ElementMatrix(this.status, this.distance, this.duration);

  factory ElementMatrix.fromJson(Map<String, dynamic> json) =>
      _$ElementMatrixFromJson(json);
  Map<String, dynamic> toJson() => _$ElementMatrixToJson(this);

}

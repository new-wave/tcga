// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'geocode_result.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GeocodeResult _$GeocodeResultFromJson(Map<String, dynamic> json) {
  return GeocodeResult(
    json['geometry'] == null
        ? null
        : Geometry.fromJson(json['geometry'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$GeocodeResultToJson(GeocodeResult instance) =>
    <String, dynamic>{
      'geometry': instance.geometry,
    };

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'element_matrix.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ElementMatrix _$ElementMatrixFromJson(Map<String, dynamic> json) {
  return ElementMatrix(
    json['status'] as String,
    json['distance'] == null
        ? null
        : DistanceDuration.fromJson(json['distance'] as Map<String, dynamic>),
    json['duration'] == null
        ? null
        : DistanceDuration.fromJson(json['duration'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$ElementMatrixToJson(ElementMatrix instance) =>
    <String, dynamic>{
      'distance': instance.distance,
      'duration': instance.duration,
      'status': instance.status,
    };

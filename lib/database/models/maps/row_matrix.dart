import 'package:json_annotation/json_annotation.dart';
import 'package:tcga_flutter/database/models/maps/element_matrix.dart';

part 'row_matrix.g.dart';

@JsonSerializable(nullable: true)
class RowMatrix {
  @JsonKey(name: 'elements')
  List<ElementMatrix> elements;

  RowMatrix(this.elements);

  factory RowMatrix.fromJson(Map<String, dynamic> json) =>
      _$RowMatrixFromJson(json);

  Map<String, dynamic> toJson() => _$RowMatrixToJson(this);
}

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'direction.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Direction _$DirectionFromJson(Map<String, dynamic> json) {
  return Direction(
    (json['routes'] as List)
        ?.map((e) =>
            e == null ? null : Routes.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    json['status'] as String,
  );
}

Map<String, dynamic> _$DirectionToJson(Direction instance) => <String, dynamic>{
      'routes': instance.routes,
      'status': instance.status,
    };

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'distance_matrix.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DistanceMatrix _$DistanceMatrixFromJson(Map<String, dynamic> json) {
  return DistanceMatrix(
    (json['rows'] as List)
        ?.map((e) =>
            e == null ? null : RowMatrix.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    json['status'] as String,
  );
}

Map<String, dynamic> _$DistanceMatrixToJson(DistanceMatrix instance) =>
    <String, dynamic>{
      'rows': instance.rows,
      'status': instance.status,
    };

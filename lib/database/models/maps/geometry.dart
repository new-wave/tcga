import 'package:json_annotation/json_annotation.dart';
import 'package:tcga_flutter/database/models/maps/location.dart';
part 'geometry.g.dart';

@JsonSerializable(nullable: true)
class Geometry {
  @JsonKey(name: 'location')
  Location location;

  Geometry(this.location);

  factory Geometry.fromJson(Map<String, dynamic> json) => _$GeometryFromJson(json);
  Map<String, dynamic> toJson() => _$GeometryToJson(this);
}
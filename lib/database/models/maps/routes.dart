import 'package:json_annotation/json_annotation.dart';
import 'package:tcga_flutter/database/models/maps/element_matrix.dart';

import 'overview_polyline.dart';

part 'routes.g.dart';

@JsonSerializable(nullable: true)
class Routes {
  @JsonKey(name: 'overview_polyline')
  OverviewPolyline overviewPolyline;

  Routes(this.overviewPolyline);

  factory Routes.fromJson(Map<String, dynamic> json) =>
      _$RoutesFromJson(json);

  Map<String, dynamic> toJson() => _$RoutesToJson(this);
}

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'row_matrix.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RowMatrix _$RowMatrixFromJson(Map<String, dynamic> json) {
  return RowMatrix(
    (json['elements'] as List)
        ?.map((e) => e == null
            ? null
            : ElementMatrix.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$RowMatrixToJson(RowMatrix instance) => <String, dynamic>{
      'elements': instance.elements,
    };

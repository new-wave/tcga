// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'routes.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Routes _$RoutesFromJson(Map<String, dynamic> json) {
  return Routes(
    json['overview_polyline'] == null
        ? null
        : OverviewPolyline.fromJson(
            json['overview_polyline'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$RoutesToJson(Routes instance) => <String, dynamic>{
      'overview_polyline': instance.overviewPolyline,
    };

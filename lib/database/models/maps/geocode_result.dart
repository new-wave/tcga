import 'package:json_annotation/json_annotation.dart';
import 'package:tcga_flutter/database/models/maps/geometry.dart';

part 'geocode_result.g.dart';

@JsonSerializable(nullable: true)
class GeocodeResult {
  @JsonKey(name: 'geometry')
  Geometry geometry;

  GeocodeResult(this.geometry);

  factory GeocodeResult.fromJson(Map<String, dynamic> json) =>
      _$GeocodeResultFromJson(json);

  Map<String, dynamic> toJson() => _$GeocodeResultToJson(this);
}

import 'package:json_annotation/json_annotation.dart';
part 'distance_duration.g.dart';

@JsonSerializable(nullable: true)
class DistanceDuration {
  @JsonKey(name: 'value')
  double value;
  @JsonKey(name: 'text')
  String text;

  DistanceDuration(this.value, this.text);

  factory DistanceDuration.fromJson(Map<String, dynamic> json) => _$DistanceDurationFromJson(json);
  Map<String, dynamic> toJson() => _$DistanceDurationToJson(this);
}
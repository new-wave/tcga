import 'package:json_annotation/json_annotation.dart';
import 'package:tcga_flutter/database/models/maps/row_matrix.dart';
import 'distance_duration.dart';
part 'distance_matrix.g.dart';

@JsonSerializable(nullable: true)
class DistanceMatrix {
  @JsonKey(name: 'rows')
  List<RowMatrix> rows;
  @JsonKey(name: 'status')
  String status;

  DistanceMatrix(this.rows, this.status);

  factory DistanceMatrix.fromJson(Map<String, dynamic> json) =>
      _$DistanceMatrixFromJson(json);
  Map<String, dynamic> toJson() => _$DistanceMatrixToJson(this);

}

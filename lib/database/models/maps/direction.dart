import 'package:json_annotation/json_annotation.dart';
import 'package:tcga_flutter/database/models/maps/routes.dart';
import 'package:tcga_flutter/database/models/maps/row_matrix.dart';
import 'distance_duration.dart';
part 'direction.g.dart';

@JsonSerializable(nullable: true)
class Direction {
  @JsonKey(name: 'routes')
  List<Routes> routes;
  @JsonKey(name: 'status')
  String status;

  Direction(this.routes, this.status);

  factory Direction.fromJson(Map<String, dynamic> json) =>
      _$DirectionFromJson(json);
  Map<String, dynamic> toJson() => _$DirectionToJson(this);

}

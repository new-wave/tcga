// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'distance_duration.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DistanceDuration _$DistanceDurationFromJson(Map<String, dynamic> json) {
  return DistanceDuration(
    (json['value'] as num)?.toDouble(),
    json['text'] as String,
  );
}

Map<String, dynamic> _$DistanceDurationToJson(DistanceDuration instance) =>
    <String, dynamic>{
      'value': instance.value,
      'text': instance.text,
    };

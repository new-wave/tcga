import 'package:json_annotation/json_annotation.dart';
import 'geocode_result.dart';
part 'geocode.g.dart';

@JsonSerializable(nullable: true)
class Geocode {
  @JsonKey(name: 'results')
  List<GeocodeResult> results;
  @JsonKey(name: 'status')
  String status;

  Geocode(this.results, this.status);

  factory Geocode.fromJson(Map<String, dynamic> json) =>
      _$GeocodeFromJson(json);
  Map<String, dynamic> toJson() => _$GeocodeToJson(this);

}

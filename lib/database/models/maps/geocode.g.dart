// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'geocode.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Geocode _$GeocodeFromJson(Map<String, dynamic> json) {
  return Geocode(
    (json['results'] as List)
        ?.map((e) => e == null
            ? null
            : GeocodeResult.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    json['status'] as String,
  );
}

Map<String, dynamic> _$GeocodeToJson(Geocode instance) => <String, dynamic>{
      'results': instance.results,
      'status': instance.status,
    };

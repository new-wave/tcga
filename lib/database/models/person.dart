import 'package:hive/hive.dart';
import 'package:tcga_flutter/database/models/address.dart';
import 'package:tcga_flutter/database/models/language.dart';
import 'package:tcga_flutter/database/models/value_set.dart';

import 'base_entity.dart';
import '_hive_type_id.dart';

part 'person.g.dart';

@HiveType(typeId: HiveTypeId.person)
class Person extends BaseEntity {
  @HiveField(8)
  String organization;
  @HiveField(9)
  String firstName;
  @HiveField(10)
  String middleName;
  @HiveField(11)
  String lastName;
  @HiveField(12)
  int prefix;
  @HiveField(13)
  int suffix;
  @HiveField(14)
  int gender;
  @HiveField(15)
  DateTime birthDate;
  @HiveField(16)
  int maritalStatus;
  @HiveField(17)
  String mobilePhone;
  @HiveField(18)
  String email;

  @HiveField(19)
  String photo;
  @HiveField(20)
  String website;
  @HiveField(21)
  String distance;
  @HiveField(22)
  List<Address> addresses;
  @HiveField(23)
  List<Language> languages;
  @HiveField(24)
  List<ValueSet> credentials;
  @HiveField(25)
  List<ValueSet> specialties;
  @HiveField(26)
  List<String> programs;
  @HiveField(27)
  String countryCode;

  Person({
    id,
    this.firstName,
    this.middleName,
    this.lastName,
    this.organization,
    this.prefix,
    this.suffix,
    this.gender,
    this.birthDate,
    this.maritalStatus,
    this.photo,
    this.website,
    this.distance,
    this.mobilePhone,
    this.email,
    this.addresses,
    this.languages,
    this.credentials,
    this.specialties,
    this.programs,
    this.countryCode,
  }) : super(
          id: id,
        );
}

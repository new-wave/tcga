import 'package:hive/hive.dart';
import 'package:tcga_flutter/database/models.dart';
import 'package:tcga_flutter/database/models/service_type.dart';
import 'package:tcga_flutter/database/models/trip.dart';

import 'models/country.dart';

registerHiveAdapters() {
  Hive.registerAdapter(AddressAdapter());
  Hive.registerAdapter(LanguageAdapter());
  Hive.registerAdapter(PersonAdapter());
  Hive.registerAdapter(UserModelAdapter());
  Hive.registerAdapter(TeamAdapter());
  Hive.registerAdapter(TeamMemberAdapter());
  Hive.registerAdapter(AppointmentAdapter());
  Hive.registerAdapter(MedicationCodeAdapter());
  Hive.registerAdapter(ValueSetAdapter());
  Hive.registerAdapter(TimeSlotAdapter());
  Hive.registerAdapter(ParticipantAdapter());
  Hive.registerAdapter(ConditionAdapter());
  Hive.registerAdapter(MedicationAdapter());
  Hive.registerAdapter(AllergyAdapter());
  Hive.registerAdapter(ImmunizationAdapter());
  Hive.registerAdapter(NoteAdapter());
  Hive.registerAdapter(ExpenseAdapter());
  Hive.registerAdapter(ExpensePhotoAdapter());
  Hive.registerAdapter(TimeZoneAdapter());
  Hive.registerAdapter(TripAdapter());
  Hive.registerAdapter(ServiceTypeAdapter());
  Hive.registerAdapter(CountryAdapter());
}

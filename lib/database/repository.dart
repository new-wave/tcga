import 'package:hive/hive.dart';
import 'package:uuid/uuid.dart';

import 'models/base_entity.dart';

Type typeOf<T>() => T;

final _uuid = Uuid();

class Repository<T extends BaseEntity> {
  Future<List<T>> query({
    Function(T) filter,
    bool includeDeletedItems = false,
    int pageSize = 100,
  }) async {
    final box = await _getBox();

    var query = box.values;

    if (includeDeletedItems != true) {
      query = query.where((element) => !element.isDeleted);
    }

    if (filter != null) {
      query = query.where(filter);
    }
    return query.take(pageSize).toList();
  }

  Future<T> get(String id) async {
    final box = await _getBox();
    return box.get(id);
  }

  Future<String> create(T entity) async {
    entity.id = _generateId();
    entity.isDeleted = false;
    entity.deletedOn = null;
    entity.updatedOn = null;
    entity.createdOn = DateTime.now();

    final box = await _getBox();
    await box.put(entity.id, entity);
    return entity.id;
  }

  Future<bool> update(T entity) async {
    final box = await _getBox();
    final existingEntity = box.get(entity.id);

    if (existingEntity == null || existingEntity.isDeleted) {
      return false;
    }

    entity.isDeleted = false;
    entity.deletedOn = null;
    entity.createdBy = existingEntity.createdBy;
    entity.createdOn = existingEntity.createdOn;
    entity.updatedOn = DateTime.now();

    await box.put(entity.id, entity);

    return true;
  }

  Future<bool> delete(String id) async {
    final box = await _getBox();
    final entity = box.get(id);

    if (entity == null || entity.isDeleted) {
      return true;
    }

    entity.isDeleted = true;
    entity.deletedOn = DateTime.now();

    await box.put(entity.id, entity);

    return true;
  }

  Future<Box<T>> _getBox() async {
    final boxName = T.toString().toLowerCase();

    if (Hive.isBoxOpen(boxName)) {
      return Hive.box<T>(boxName);
    }

    return await Hive.openBox<T>(boxName);
  }

  String _generateId() {
    return _uuid.v4();
  }
}

class QueryOnlyRepository<T extends QueryOnlyEntity> {
  Future<List<T>> query({
    Function(T) filter,
    bool includeDeletedItems = false,
    int pageSize = 400,
  }) async {
    final box = await _getBox();

    var query = box.values;

    if (filter != null) {
      query = query.where(filter);
    }
    return query.toList();
  }

  Future<T> get(String id) async {
    final box = await _getBox();
    return box.get(id);
  }

  Future<String> create(T entity) async {
    final box = await _getBox();
    await box.put(entity.code, entity);
    return entity.code;
  }

  Future<Box<T>> _getBox() async {
    final boxName = T.toString().toLowerCase();

    if (Hive.isBoxOpen(boxName)) {
      return Hive.box<T>(boxName);
    }

    return await Hive.openBox<T>(boxName);
  }
}

import 'package:dio/dio.dart';

class BaseApiModel<T> {
  DioError _error;
  T data;

  setException(DioError error) {
    _error = error;
  }

  setData(T data) {
    this.data = data;
  }

  getException() {
    return _error;
  }
}

import 'package:dio/dio.dart';
import 'package:retrofit/retrofit.dart';
import 'package:tcga_flutter/database/models/api/registration_api.dart';

import '../database/models/api/user_api.dart';

part 'retrofit_client.g.dart';

@RestApi(baseUrl: 'https://api.thecaregiver.app')
abstract class RetrofitClient {
  factory RetrofitClient(Dio dio, {String baseUrl}) = _RetrofitClient;

  @POST('/User')
  Future<UserAPI> createUser(@Body() UserAPI userAPI);
  @POST('/User/{id}/Activation')
  Future activateUser(@Path() String id, @Body() Registration userAPI);
}

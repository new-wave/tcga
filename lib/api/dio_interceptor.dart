import 'package:dio/dio.dart';
import 'package:logger/logger.dart';

class DioInterceptor extends InterceptorsWrapper {
  var log = Logger(
    printer: PrettyPrinter(
        methodCount: 2,
        // number of method calls to be displayed
        errorMethodCount: 8,
        // number of method calls if stacktrace is provided
        lineLength: 120,
        // width of the output
        colors: true,
        // Colorful log messages
        printEmojis: true,
        // Print an emoji for each log message
        printTime: false // Should each log print contain a timestamp
        ),
  );
  @override
  Future onRequest(RequestOptions options) async {
    log.d("onRequest: ${options.uri} \n"
        "data=${options.data}\n"
        "method=${options.method}\n"
        "headers=${options.headers}\n"
        "queryParameters=${options.queryParameters}");
    return super.onRequest(options);
  }

  @override
  Future onResponse(Response response) async {
    log.d("onResponse: $response");
    return super.onResponse(response);
  }

  @override
  Future onError(DioError err) async {
    log.d("onError: $err\n"
        "Response: ${err.response}");
    return super.onError(err);
  }
}

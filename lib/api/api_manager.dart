import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:tcga_flutter/api/abs_api_manager.dart';
import 'package:tcga_flutter/api/retrofit_client.dart';
import 'package:tcga_flutter/database/models/api/error_api.dart';
import 'package:tcga_flutter/database/models/api/registration_api.dart';
import 'package:tcga_flutter/database/models/api/user_api.dart';
import 'package:tcga_flutter/api/base_api_model.dart';
import '../widget/error_api_wiget.dart' as w;
import 'dio_interceptor.dart';

class APIManager implements IAPIManager {
  // ignore: non_constant_identifier_names
  static RetrofitClient ApiClient({Dio dio}) {
    final Dio _dio =
        dio ?? new Dio(BaseOptions(sendTimeout: 30000, receiveTimeout: 30000));
    _dio.interceptors.add(DioInterceptor());
    RetrofitClient _client = RetrofitClient(_dio);
    return _client;
  }

  @override
  Future<BaseApiModel<UserAPI>> createUser(UserAPI user) async {
    UserAPI userAPI;
    try {
      EasyLoading.show(maskType: EasyLoadingMaskType.black);
      userAPI = await ApiClient().createUser(user);
    } catch (e) {
      EasyLoading.dismiss();
      if (e is DioError) {
        var error = ErrorAPI.fromJson(jsonDecode(e.response.data));
        EasyLoading.show(
            dismissOnTap: true,
            indicator: w.ErrorWidget(
              error: error.title,
            ),
            maskType: EasyLoadingMaskType.black);
        return BaseApiModel()..setException(e);
      }
    }
    EasyLoading.dismiss();
    return BaseApiModel()..data = userAPI;
  }

  @override
  Future<bool> activateUser(String id, Registration registration) async {
    try {
      EasyLoading.show(maskType: EasyLoadingMaskType.black);
      final _ = await ApiClient().activateUser(id, registration);
    } catch (e) {
      EasyLoading.dismiss();

      if (e is DioError) {
        var error = ErrorAPI.fromJson(jsonDecode(e.response.data));
        EasyLoading.show(
            dismissOnTap: true,
            indicator: w.ErrorWidget(
              error: error.title,
            ),
            maskType: EasyLoadingMaskType.black);
        return false;
      }
    }
    EasyLoading.dismiss();
    return true;
  }
}

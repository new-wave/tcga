// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'retrofit_client.dart';

// **************************************************************************
// RetrofitGenerator
// **************************************************************************

class _RetrofitClient implements RetrofitClient {
  _RetrofitClient(this._dio, {this.baseUrl}) {
    ArgumentError.checkNotNull(_dio, '_dio');
    baseUrl ??= 'https://api.thecaregiver.app';
  }

  final Dio _dio;

  String baseUrl;

  @override
  Future<UserAPI> createUser(userAPI) async {
    ArgumentError.checkNotNull(userAPI, 'userAPI');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(userAPI?.toJson() ?? <String, dynamic>{});
    final _result = await _dio.request<Map<String, dynamic>>('/User',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'POST',
            headers: <String, dynamic>{},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = UserAPI.fromJson(_result.data);
    return value;
  }

  @override
  Future<dynamic> activateUser(id, userAPI) async {
    ArgumentError.checkNotNull(id, 'id');
    ArgumentError.checkNotNull(userAPI, 'userAPI');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(userAPI?.toJson() ?? <String, dynamic>{});
    final _result = await _dio.request('/User/$id/Activation',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'POST',
            headers: <String, dynamic>{},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = _result.data;
    return value;
  }
}

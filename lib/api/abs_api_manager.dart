import '../database/models/api/registration_api.dart';

import '../api/base_api_model.dart';
import '../database/models/api/user_api.dart';

abstract class IAPIManager {
  Future<BaseApiModel<UserAPI>> createUser(UserAPI user);
  Future<bool> activateUser(String id, Registration user);
}

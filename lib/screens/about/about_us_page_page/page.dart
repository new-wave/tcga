import 'package:fish_redux/fish_redux.dart';

import 'effect.dart';
import 'reducer.dart';
import 'state.dart';
import 'view.dart';

class AboutUsPagePage extends Page<AboutUsPageState, Map<String, dynamic>> {
  AboutUsPagePage()
      : super(
            initState: initState,
            effect: buildEffect(),
            reducer: buildReducer(),
            view: buildView,
            dependencies: Dependencies<AboutUsPageState>(
                adapter: null,
                slots: <String, Dependent<AboutUsPageState>>{
                }),
            middleware: <Middleware<AboutUsPageState>>[
            ],);

}

import 'package:fish_redux/fish_redux.dart';
import 'action.dart';
import 'state.dart';

Effect<AboutUsPageState> buildEffect() {
  return combineEffects(<Object, Effect<AboutUsPageState>>{
    AboutUsPageAction.action: _onAction,
  });
}

void _onAction(Action action, Context<AboutUsPageState> ctx) {
}

import 'package:fish_redux/fish_redux.dart';

import 'action.dart';
import 'state.dart';

Reducer<AboutUsPageState> buildReducer() {
  return asReducer(
    <Object, Reducer<AboutUsPageState>>{
      AboutUsPageAction.action: _onAction,
    },
  );
}

AboutUsPageState _onAction(AboutUsPageState state, Action action) {
  final AboutUsPageState newState = state.clone();
  return newState;
}

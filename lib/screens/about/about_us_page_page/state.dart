import 'package:fish_redux/fish_redux.dart';

class AboutUsPageState implements Cloneable<AboutUsPageState> {

  @override
  AboutUsPageState clone() {
    return AboutUsPageState();
  }
}

AboutUsPageState initState(Map<String, dynamic> args) {
  return AboutUsPageState();
}

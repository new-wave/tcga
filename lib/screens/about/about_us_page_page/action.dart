import 'package:fish_redux/fish_redux.dart';

//TODO replace with your own action
enum AboutUsPageAction { action }

class AboutUsPageActionCreator {
  static Action onAction() {
    return const Action(AboutUsPageAction.action);
  }
}

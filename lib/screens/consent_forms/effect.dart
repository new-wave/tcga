import 'package:fish_redux/fish_redux.dart';

import 'action.dart';
import 'manager.dart';
import 'state.dart';

final _manager = ConsentFormsManager();

Effect<ConsentFormsState> buildEffect() {
  return combineEffects(<Object, Effect<ConsentFormsState>>{
    Lifecycle.initState: _init,
    ConsentFormsAction.action: _onAction,
  });
}

void _onAction(Action action, Context<ConsentFormsState> ctx) {}

void _init(Action action, Context<ConsentFormsState> ctx) async {
  final file = await _manager
      .getFileFromAsset("assets/Export-Import-Form-Data-Updated.pdf");
  String path = file.path;

  ctx.dispatch(
    ConsentFormsActionCreator.initialize(
      path,
    ),
  );
}

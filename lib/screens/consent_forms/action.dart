import 'package:fish_redux/fish_redux.dart';

//TODO replace with your own action
enum ConsentFormsAction { action, initialize }

class ConsentFormsActionCreator {
  static Action onAction() {
    return const Action(ConsentFormsAction.action);
  }

  static Action initialize(String path) {
    return Action(
      ConsentFormsAction.initialize,
      payload: path,
    );
  }
}

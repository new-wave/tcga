import 'package:fish_redux/fish_redux.dart';

class ConsentFormsState implements Cloneable<ConsentFormsState> {
  String assetPDFPath;

  @override
  ConsentFormsState clone() {
    return ConsentFormsState()..assetPDFPath = assetPDFPath;
  }
}

ConsentFormsState initState(Map<String, dynamic> args) {
  return ConsentFormsState()..assetPDFPath;
}

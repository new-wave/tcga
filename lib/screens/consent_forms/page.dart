import 'package:fish_redux/fish_redux.dart';

import 'effect.dart';
import 'reducer.dart';
import 'state.dart';
import 'view.dart';

class ConsentFormsPage extends Page<ConsentFormsState, Map<String, dynamic>> {
  ConsentFormsPage()
      : super(
            initState: initState,
            effect: buildEffect(),
            reducer: buildReducer(),
            view: buildView,
            dependencies: Dependencies<ConsentFormsState>(
                adapter: null,
                slots: <String, Dependent<ConsentFormsState>>{
                }),
            middleware: <Middleware<ConsentFormsState>>[
            ],);

}

import 'package:fish_redux/fish_redux.dart';

import 'action.dart';
import 'state.dart';

Reducer<ConsentFormsState> buildReducer() {
  return asReducer(
    <Object, Reducer<ConsentFormsState>>{
      ConsentFormsAction.action: _onAction,
      ConsentFormsAction.initialize: _init,
    },
  );
}

ConsentFormsState _onAction(ConsentFormsState state, Action action) {
  final ConsentFormsState newState = state.clone();
  return newState;
}

ConsentFormsState _init(ConsentFormsState state, Action action) {
  final ConsentFormsState newState = state.clone();
  String path = action.payload;
  newState.assetPDFPath = path;
  return newState;
}

import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_pdfview/flutter_pdfview.dart';
import 'state.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';

Widget buildView(
    ConsentFormsState state, Dispatch dispatch, ViewService viewService) {
  return Scaffold(
    appBar: _appBar(viewService, dispatch),
    body: Column(
      children: <Widget>[
        // Container(
        //   height: 48,
        //   color: colorBackground,
        // ),
        state.assetPDFPath != null
            ? Expanded(
                child: Container(
                  color: Colors.white,
                  child: PDFView(
                    filePath: state
                        .assetPDFPath, //== null ? 'assets/abc' : state.assetPDFPath,
                    autoSpacing: true,
                    enableSwipe: true,
                    pageSnap: true,
                    swipeHorizontal: false,
                    nightMode: true,
                    onError: (e) {
                      print(e);
                    },
                  ),
                ),
              )
            : CircularProgressIndicator()
      ],
    ),
  );
}

AppBar _appBar(ViewService viewService, Dispatch dispatch) {
  return AppBar(
    elevation: 3,
    backgroundColor: Colors.white,
    leading: IconButton(
      iconSize: 24,
      icon: SvgPicture.asset('assets/icons/ic_arrow_left_circle.svg'),
      onPressed: () {
        Navigator.pop(viewService.context);
      },
    ),
    title: Text(AppLocalizations.of(
      viewService.context,
    ).text('consense_forms.appbar.title')),
  );
}

import 'dart:io';

import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';

class ConsentFormsManager {
  static final _singleton = ConsentFormsManager._internal();

  factory ConsentFormsManager() {
    return _singleton;
  }

  ConsentFormsManager._internal();

  Future<File> getFileFromAsset(String asset) async {
    try {
      var data = await rootBundle.load(asset);
      var bytes = data.buffer.asUint8List();
      var dir = await getApplicationDocumentsDirectory();
      File file = File("${dir.path}/myFile.pdf");
      File assetFile = await file.writeAsBytes(bytes);

      return assetFile;
    } catch (e) {
      throw Exception("Error opening asset file");
    }
  }
}

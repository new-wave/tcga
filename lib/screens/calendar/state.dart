import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:tcga_flutter/database/models/enums.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';
import 'package:tcga_flutter/screens/calendar/timeslot_picker_page/timeslot_item_component/state.dart';

class AppointmentItemState implements Cloneable<AppointmentItemState>{
  AppointmentItemState({
    this.id,
    this.recipientId,
    this.subject,
    this.start,
    this.end,
    this.selectedDay,
    this.isAllDay,
    this.appointmentType,
    this.color,
    this.recurrenceRule,
    this.participant,
    this.exceptionDates,
    this.timeSlots,
    this.timeZone,
  });

  String recurrenceRule;

  Color color;

  String id;

  String recipientId;

  AppointmentType appointmentType;

  String subject;

  DateTime selectedDay;

  DateTime start;

  DateTime end;

  bool isAllDay;

  String participant;

  List<DateTime> exceptionDates;

  List<TimeslotItemState> timeSlots;

  String timeZone;

  bool isMedication() {
    return appointmentType == AppointmentType.MedicationAdministration;
  }

  @override
  AppointmentItemState clone() {
    return AppointmentItemState()
      ..id = id
      ..recipientId = recipientId
      ..subject = subject
      ..start = start
      ..end = end
      ..selectedDay = selectedDay
      ..isAllDay = isAllDay
      ..appointmentType = appointmentType
      ..color = color
      ..recurrenceRule = recurrenceRule
      ..participant = participant
      ..exceptionDates = exceptionDates
      ..timeSlots = timeSlots
      ..timeZone = timeZone;
  }
}

class MeetingDataSource extends CalendarDataSource {
  MeetingDataSource(List<AppointmentItemState> source) {
    appointments = source;
  }

  @override
  List<DateTime> getRecurrenceExceptionDates(int index) {
    return appointments[index].exceptionDates;
  }

  @override
  String getRecurrenceRule(int index) {
    return appointments[index].recurrenceRule;
  }

  @override
  DateTime getStartTime(int index) {
    return appointments[index].start;
  }

  @override
  DateTime getEndTime(int index) {
    return appointments[index].end;
  }

  @override
  String getSubject(int index) {
    return appointments[index].subject;
  }

  @override
  Color getColor(int index) {
    return appointments[index].isMedication() ? Colors.orange : Colors.blue;
  }

  @override
  bool isAllDay(int index) {
    return appointments[index].isAllDay;
  }
}

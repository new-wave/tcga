import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/cupertino.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';
import '../../../utils/utils.dart';

class MakeRecurrencePickerState
    implements Cloneable<MakeRecurrencePickerState> {
  static final Map<RecurrenceType, String> recurrenceTypes = {
    RecurrenceType.daily: 'day',
    RecurrenceType.weekly: 'week',
    RecurrenceType.monthly: 'month',
    RecurrenceType.yearly: 'year',
  };

  RecurrenceType recurrenceType;
  int interval;
  TextEditingController intervalEditingController;

  RecurrenceRange recurrenceRange;

  TextEditingController endDateEditingController;
  int recurrenceCount;
  TextEditingController recurrenceCountEditingController;

  DateTime startDate;
  DateTime endDate;

  DateTime selectedStartDate;
  DateTime selectedEndDate;

  @override
  MakeRecurrencePickerState clone() {
    return MakeRecurrencePickerState()
      ..intervalEditingController = intervalEditingController
      ..recurrenceType = recurrenceType
      ..interval = interval
      ..recurrenceCount = recurrenceCount
      ..recurrenceRange = recurrenceRange
      ..endDateEditingController = endDateEditingController
      ..recurrenceCountEditingController = recurrenceCountEditingController
      ..startDate = startDate
      ..endDate = endDate
      ..selectedStartDate = selectedStartDate
      ..selectedEndDate = selectedEndDate;
  }
}

MakeRecurrencePickerState initState(Map<String, dynamic> args) {
  final recurrenceProperties =
      args['recurrence-properties'] as RecurrenceProperties;
  final selectedStartTime = args['start-date'] as DateTime;
  final selectedEndTime = args['end-date'] as DateTime;

  return MakeRecurrencePickerState()
    ..interval = recurrenceProperties?.interval ?? 1
    ..intervalEditingController = TextEditingController(
      text: (recurrenceProperties?.interval ?? 1).toString(),
    )
    ..recurrenceType =
        recurrenceProperties?.recurrenceType ?? RecurrenceType.daily
    ..recurrenceRange =
        recurrenceProperties?.recurrenceRange ?? RecurrenceRange.noEndDate
    ..recurrenceCount = recurrenceProperties?.recurrenceCount ?? 1
    ..endDateEditingController = TextEditingController(
      text:
          (recurrenceProperties?.endDate ?? DateTime.now()).toShortDateString(),
    )
    ..recurrenceCountEditingController = TextEditingController(
      text: (recurrenceProperties?.recurrenceCount ?? 1).toString(),
    )
    ..startDate = selectedStartTime ?? DateTime.now()
    ..endDate = recurrenceProperties?.endDate ?? DateTime.now()
    ..selectedStartDate = selectedStartTime ?? DateTime.now()
    ..selectedEndDate = selectedEndTime ?? DateTime.now();
}

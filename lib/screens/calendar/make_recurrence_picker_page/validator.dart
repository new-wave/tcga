import 'package:syncfusion_flutter_calendar/calendar.dart';

import 'state.dart';

class Validator {
  static bool isValid(MakeRecurrencePickerState state) {
    final interval = int.tryParse(
      state.intervalEditingController.text,
    );
    if (interval == null || interval < 1) return false;

    final rangeCount = int.tryParse(
      state.recurrenceCountEditingController.text,
    );
    if (state.recurrenceRange == RecurrenceRange.count &&
        (rangeCount == null || rangeCount < 1)) return false;

    return true;
  }
}

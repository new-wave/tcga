import 'package:fish_redux/fish_redux.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';
import '../../../utils/utils.dart';
import 'action.dart';
import 'state.dart';

Reducer<MakeRecurrencePickerState> buildReducer() {
  return asReducer(
    <Object, Reducer<MakeRecurrencePickerState>>{
      MakeRecurrencePickerAction.handleRepeatEveryTime: _handleRepeatEveryTime,
      MakeRecurrencePickerAction.handleEndDateSelected: _handleEndDateSelected,
      MakeRecurrencePickerAction.handleRecurrenceRangeSelected:
          _handleRecurrenceRangeSelected,
    },
  );
}

MakeRecurrencePickerState _handleRepeatEveryTime(
    MakeRecurrencePickerState state, Action action) {
  final newState = state.clone();

  final index = action.payload as int;
  newState.recurrenceType = RecurrenceType.values[index];
  return newState;
}

MakeRecurrencePickerState _handleRecurrenceRangeSelected(
    MakeRecurrencePickerState state, Action action) {
  final newState = state.clone();

  newState.recurrenceRange = action.payload;

  return newState;
}

MakeRecurrencePickerState _handleEndDateSelected(
    MakeRecurrencePickerState state, Action action) {
  final newState = state.clone();
  final value = action.payload as DateTime;

  if (value == null) return state;

  newState.endDate = value;

  return newState;
}

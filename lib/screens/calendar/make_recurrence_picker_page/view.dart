import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/config/style.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/widget/button_widget.dart';
import 'package:tcga_flutter/widget/datetime_select_controller.dart';
import 'package:tcga_flutter/widget/picker_widget.dart';
import 'package:tcga_flutter/widget/import.dart';

import 'action.dart';
import 'state.dart';
import 'validator.dart';

Widget buildView(
  MakeRecurrencePickerState state,
  Dispatch dispatch,
  ViewService viewService,
) {
  return GestureDetector(
    onTap: () {
      dispatch(MakeRecurrencePickerActionCreator.unfocus());
    },
    child: Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.vertical(top: Radius.circular(15.0)),
        color: Colors.white,
      ),
      child: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(spacingMd),
            child: Row(
              children: [
                GestureDetector(
                  onTap: () {
                    dispatch(
                      MakeRecurrencePickerActionCreator.clear(),
                    );
                  },
                  child: Text(
                    AppLocalizations.of(viewService.context)
                        .text('common.clear'),
                    style: Style.labelClear,
                  ),
                ),
                Spacer(
                  flex: 5,
                ),
                Text(
                  AppLocalizations.of(viewService.context)
                      .text('calendar.editor.make_recurrence_picker.header'),
                  style: Style.labelTitleItem,
                ),
                Spacer(
                  flex: 4,
                ),
                InkWell(
                  onTap: () {
                    Navigator.of(viewService.context).pop();
                  },
                  child: SvgPicture.asset(
                    "assets/icons/ic_close.svg",
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: spacingMd),
            child: Row(
              children: [
                Text(
                  AppLocalizations.of(viewService.context).text(
                      'calendar.editor.make_recurrence_picker.repeat_every'),
                  style: Style.labelNormal,
                ),
                Spacer(),
                Expanded(
                  flex: 3,
                  child: Container(
                    height: 36,
                    width: 65,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(4),
                      color: colorBackground,
                    ),
                    padding: EdgeInsets.symmetric(horizontal: 3, vertical: 6),
                    child: TextFormField(
                      style: Style.labelNormal,
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        hintStyle: Style.labelNormal,
                        contentPadding: const EdgeInsets.all(12),
                      ),
                      keyboardType: TextInputType.number,
                      controller: state.intervalEditingController,
                      maxLength: 2,
                      buildCounter: (context,
                              {currentLength, isFocused, maxLength}) =>
                          currentLength = null,
                    ),
                  ),
                ),
                Spacer(),
                Expanded(
                  flex: 6,
                  child: PickerWidget(
                    initialIndex: state.recurrenceType != null
                        ? MakeRecurrencePickerState.recurrenceTypes.keys
                            .toList()
                            .indexOf(
                              state.recurrenceType,
                            )
                        : null,
                    items: MakeRecurrencePickerState.recurrenceTypes.values
                        .toList(),
                    title: 'Repeat Frequency Type',
                    indexSelected: (index) {
                      dispatch(MakeRecurrencePickerActionCreator
                          .handleRepeatEveryTime(index));
                    },
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Padding(
                padding: const EdgeInsets.only(left: spacingMd),
                child: Text(
                  AppLocalizations.of(viewService.context)
                      .text('calendar.editor.make_recurrence_picker.end_time'),
                  textAlign: TextAlign.start,
                  style: Style.labelNormal,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(right: spacingMd),
                child: GestureDetector(
                  onTap: () {
                    dispatch(MakeRecurrencePickerActionCreator
                        .handleRecurrenceRangeSelected(
                            RecurrenceRange.endDate));
                  },
                  child: Row(
                    children: [
                      Expanded(
                        flex: 4,
                        child: Row(
                          children: [
                            Radio(
                              value: RecurrenceRange.endDate,
                              groupValue: state.recurrenceRange,
                              onChanged: (value) {
                                dispatch(MakeRecurrencePickerActionCreator
                                    .handleRecurrenceRangeSelected(value));
                              },
                            ),
                            Container(
                              width: 67,
                              child: Text(
                                AppLocalizations.of(viewService.context).text(
                                    'calendar.editor.make_recurrence_picker.at_date'),
                                style: Style.labelNormal,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        width: 16,
                      ),
                      Expanded(
                        flex: 6,
                        child: Container(
                          margin: EdgeInsets.only(
                            top: spacingSm,
                          ),
                          child: Focus(
                            onFocusChange: (hasFocus) {
                              if (hasFocus) {
                                dispatch(MakeRecurrencePickerActionCreator
                                    .handleRecurrenceRangeSelected(
                                        RecurrenceRange.endDate));
                              }
                            },
                            child: DatePickerWidget(
                              initial: state.endDate,
                              max: state.endDate.add(new Duration(days: 730)),
                              min: state.endDate
                                  .subtract(new Duration(days: 730)),
                              dateSelected: (date) => dispatch(
                                MakeRecurrencePickerActionCreator
                                    .handleEndDateSelected(
                                  date,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(right: spacingMd),
                child: GestureDetector(
                  onTap: () {
                    dispatch(MakeRecurrencePickerActionCreator
                        .handleRecurrenceRangeSelected(RecurrenceRange.count));
                  },
                  child: Row(
                    children: [
                      Expanded(
                        flex: 4,
                        child: Row(
                          children: [
                            Radio(
                              value: RecurrenceRange.count,
                              groupValue: state.recurrenceRange,
                              onChanged: (value) {
                                dispatch(MakeRecurrencePickerActionCreator
                                    .handleRecurrenceRangeSelected(value));
                              },
                            ),
                            Text(
                              AppLocalizations.of(viewService.context).text(
                                  'calendar.editor.make_recurrence_picker.after'),
                              style: Style.labelNormal,
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        width: 16,
                      ),
                      Expanded(
                        flex: 6,
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              height: 36,
                              width: 65,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(4),
                                color: colorBackground,
                              ),
                              margin: EdgeInsets.only(
                                top: spacingSm,
                              ),
                              padding: EdgeInsets.symmetric(
                                horizontal: 6,
                                vertical: 6,
                              ),
                              child: Focus(
                                onFocusChange: (hasFocus) {
                                  if (hasFocus) {
                                    dispatch(MakeRecurrencePickerActionCreator
                                        .handleRecurrenceRangeSelected(
                                            RecurrenceRange.count));
                                  }
                                },
                                child: TextFormField(
                                  style: Style.labelNormal,
                                  decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintStyle: Style.labelNormal,
                                    contentPadding: const EdgeInsets.all(12),
                                  ),
                                  keyboardType: TextInputType.number,
                                  maxLength: 2,
                                  buildCounter: (context,
                                          {currentLength,
                                          isFocused,
                                          maxLength}) =>
                                      currentLength = null,
                                  controller:
                                      state.recurrenceCountEditingController,
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Container(
                              width: 100,
                              margin: EdgeInsets.only(
                                top: 4,
                              ),
                              child: Text(
                                AppLocalizations.of(viewService.context).text(
                                    'calendar.editor.make_recurrence_picker.recurrence_time'),
                                style: Style.labelNormal,
                                maxLines: 1,
                                softWrap: true,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                            Spacer(),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              GestureDetector(
                onTap: () {
                  dispatch(MakeRecurrencePickerActionCreator
                      .handleRecurrenceRangeSelected(
                          RecurrenceRange.noEndDate));
                },
                child: Row(
                  children: [
                    Radio(
                      value: RecurrenceRange.noEndDate,
                      groupValue: state.recurrenceRange,
                      onChanged: (value) {
                        dispatch(MakeRecurrencePickerActionCreator
                            .handleRecurrenceRangeSelected(value));
                      },
                    ),
                    Text(
                      AppLocalizations.of(viewService.context).text(
                          'calendar.editor.make_recurrence_picker.no_end_date'),
                      style: Style.labelNormal,
                    ),
                  ],
                ),
              ),
            ],
          ),
          Container(
            child: Builder(
              builder: (context) {
                return ButtonWidget(
                  onPress: () {
                    final isValid = Validator.isValid(state);

                    if (!isValid) {
                      final snackbar = SnackBar(
                        content: Container(
                          child: Text(
                            AppLocalizations.of(viewService.context).text(
                                'common.warning.please_fill_in_valid_details'),
                          ),
                        ),
                        backgroundColor: colorIndicatorWarning,
                      );

                      Scaffold.of(context).showSnackBar(snackbar);

                      return;
                    }
                    dispatch(
                      MakeRecurrencePickerActionCreator.confirm(),
                    );
                  },
                  colorButton: colorPrimary,
                  textButton: AppLocalizations.of(viewService.context)
                      .text('common.confirm'),
                );
              },
            ),
          ),
        ],
      ),
    ),
  );
}

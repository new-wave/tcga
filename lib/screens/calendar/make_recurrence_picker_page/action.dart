import 'package:fish_redux/fish_redux.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';

//TODO replace with your own action
enum MakeRecurrencePickerAction {
  handleRepeatEveryTime,
  handleEndDateSelected,
  unfocus,
  selectTypeEndTime,
  handleRecurrenceRangeSelected,
  confirm,
  clear,
}

class MakeRecurrencePickerActionCreator {
  static Action unfocus() {
    return Action(
      MakeRecurrencePickerAction.unfocus,
    );
  }

  static Action handleRepeatEveryTime(int typeDate) {
    return Action(
      MakeRecurrencePickerAction.handleRepeatEveryTime,
      payload: typeDate,
    );
  }

  static Action handleEndDateSelected(DateTime time) {
    return Action(
      MakeRecurrencePickerAction.handleEndDateSelected,
      payload: time,
    );
  }

  static Action selectTypeEndTime(String value) {
    return Action(
      MakeRecurrencePickerAction.selectTypeEndTime,
      payload: value,
    );
  }

  static Action handleRecurrenceRangeSelected(RecurrenceRange value) {
    return Action(
      MakeRecurrencePickerAction.handleRecurrenceRangeSelected,
      payload: value,
    );
  }

  static Action confirm() {
    return Action(
      MakeRecurrencePickerAction.confirm,
    );
  }

  static Action clear() {
    return Action(
      MakeRecurrencePickerAction.clear,
    );
  }
}

import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart' hide Action, Page;
import 'package:syncfusion_flutter_calendar/calendar.dart';
import 'package:tcga_flutter/database/models/enums.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/utils/confirm_dialog.dart';
import 'package:tcga_flutter/utils/show_picker.dart';
import '../../../utils/utils.dart';

import 'action.dart';
import 'state.dart';

Effect<MakeRecurrencePickerState> buildEffect() {
  return combineEffects(<Object, Effect<MakeRecurrencePickerState>>{
    MakeRecurrencePickerAction.unfocus: _unfocus,
    MakeRecurrencePickerAction.confirm: _confirm,
    MakeRecurrencePickerAction.clear: _clear,
  });
}

void _unfocus(Action action, Context<MakeRecurrencePickerState> ctx) {
  FocusScopeNode currentFocus = FocusScope.of(ctx.context);
  if (!currentFocus.hasPrimaryFocus) {
    currentFocus.unfocus();
  }
}

void _clear(
  Action action,
  Context<MakeRecurrencePickerState> ctx,
) async {
  ctx.goBack(
    'clear',
  );
}

void _confirm(
  Action action,
  Context<MakeRecurrencePickerState> ctx,
) async {
  int interval = 1;
  switch (ctx.state.recurrenceType) {
    case RecurrenceType.daily:
      interval = int.parse(ctx.state.intervalEditingController?.text) ?? 1;
      break;
    case RecurrenceType.weekly:
      interval = int.parse(ctx.state.intervalEditingController?.text) * 7 ?? 7;
      break;
    case RecurrenceType.monthly:
      DateTime startDate = ctx.state.startDate;
      DateTime dateAfterRecurrence = startDate
          .addMonth(int.parse(ctx.state.intervalEditingController?.text));
      interval = dateAfterRecurrence.difference(startDate).inDays;
      break;
    case RecurrenceType.yearly:
      DateTime startDate = ctx.state.startDate;
      DateTime dateAfterRecurrence = startDate
          .addYear(int.parse(ctx.state.intervalEditingController?.text));
      interval = dateAfterRecurrence.difference(startDate).inDays;
      break;
  }

  if (ctx.state.selectedEndDate
          .difference(ctx.state.selectedStartDate)
          .inHours >
      interval * 24) {
    var _ = await ctx.confirm(
        title: "Error",
        message:
            "Repeated Day can't less than difference time between Start Date and End Date",
        isPositiveButtonVisible: false,
        negativeActionTitle: AppLocalizations.instance.text('common.cancel'),
        positiveActionTitle: AppLocalizations.instance.text('common.ok'));
    return;
  }

  final recurrenceProperties = RecurrenceProperties(
    recurrenceType: ctx.state.recurrenceType,
    endDate: ctx.state.recurrenceRange == RecurrenceRange.endDate
        ? ctx.state.endDate
        : null,
    recurrenceCount: ctx.state.recurrenceRange == RecurrenceRange.count
        ? int.parse(ctx.state.recurrenceCountEditingController.text)
        : null,
    interval: int.parse(ctx.state.intervalEditingController?.text) ?? 1,
    startDate: ctx.state.startDate,
    month: (ctx.state.recurrenceType == RecurrenceType.monthly ||
            ctx.state.recurrenceType == RecurrenceType.yearly)
        ? ctx.state.startDate.month
        : null,
    dayOfMonth: (ctx.state.recurrenceType == RecurrenceType.monthly ||
            ctx.state.recurrenceType == RecurrenceType.yearly)
        ? ctx.state.startDate.day
        : null,
    dayOfWeek: ctx.state.recurrenceType == RecurrenceType.yearly
        ? ctx.state.startDate.day
        : null,
    weekDays: ctx.state.recurrenceType == RecurrenceType.monthly ||
            ctx.state.recurrenceType == RecurrenceType.weekly
        ? [WeekDays.values[ctx.state.startDate.weekday % 7]]
        : [],
    recurrenceRange: ctx.state.recurrenceRange,
  );

  ctx.goBack(
    recurrenceProperties,
  );
}

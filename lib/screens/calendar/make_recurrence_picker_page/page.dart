import 'package:fish_redux/fish_redux.dart';

import 'effect.dart';
import 'reducer.dart';
import 'state.dart';
import 'view.dart';

class MakeRecurrencePickerPage extends Page<MakeRecurrencePickerState, Map<String, dynamic>> {
  MakeRecurrencePickerPage()
      : super(
            initState: initState,
            effect: buildEffect(),
            reducer: buildReducer(),
            view: buildView,
            dependencies: Dependencies<MakeRecurrencePickerState>(
                adapter: null,
                slots: <String, Dependent<MakeRecurrencePickerState>>{
                }),
            middleware: <Middleware<MakeRecurrencePickerState>>[
            ],);

}

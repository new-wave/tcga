import 'package:fish_redux/fish_redux.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';
import '../state.dart';

class TimeslotsState implements Cloneable<TimeslotsState> {
  int duration;
  List<String> durations;
  List<String> startAt;
  DateTime dateTime;
  MeetingDataSource calendarDataSource;

  List<Appointment> appointments;

  @override
  TimeslotsState clone() {
    return TimeslotsState()
      ..appointments = appointments
      ..duration = duration
      ..durations = durations
      ..startAt = startAt
      ..dateTime = dateTime
      ..calendarDataSource = calendarDataSource;
  }
}

TimeslotsState initState(Map<String, dynamic> args) {
  return TimeslotsState()
    ..dateTime = null
    ..durations = []
    ..startAt = []
    ..duration = 11
    ..appointments = [];
}

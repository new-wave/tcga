import 'package:fish_redux/fish_redux.dart';

import 'effect.dart';
import 'reducer.dart';
import 'state.dart';
import 'view.dart';

class TimeslotItemComponent extends Component<TimeslotItemState> {
  TimeslotItemComponent()
      : super(
            effect: buildEffect(),
            reducer: buildReducer(),
            view: buildView,
            dependencies: Dependencies<TimeslotItemState>(
                adapter: null,
                slots: <String, Dependent<TimeslotItemState>>{
                }),);

}

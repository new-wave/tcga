import 'package:fish_redux/fish_redux.dart';

class TimeslotItemState implements Cloneable<TimeslotItemState> {
  int durationInMinute;
  DateTime startAt;

  TimeslotItemState({
    this.startAt,
    this.durationInMinute,
  });

  @override
  TimeslotItemState clone() {
    return TimeslotItemState()
      ..durationInMinute = durationInMinute
      ..startAt = startAt;
  }
}

TimeslotItemState initState(Map<String, dynamic> args) {
  return TimeslotItemState();
}

import 'package:fish_redux/fish_redux.dart';
import 'action.dart';
import 'state.dart';

Effect<TimeslotItemState> buildEffect() {
  return combineEffects(<Object, Effect<TimeslotItemState>>{
    TimeslotItemAction.action: _onAction,
  });
}

void _onAction(Action action, Context<TimeslotItemState> ctx) {
}

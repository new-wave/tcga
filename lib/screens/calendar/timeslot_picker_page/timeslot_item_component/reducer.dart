import 'package:fish_redux/fish_redux.dart';

import 'action.dart';
import 'state.dart';

Reducer<TimeslotItemState> buildReducer() {
  return asReducer(
    <Object, Reducer<TimeslotItemState>>{
      TimeslotItemAction.action: _onAction,
    },
  );
}

TimeslotItemState _onAction(TimeslotItemState state, Action action) {
  final TimeslotItemState newState = state.clone();
  return newState;
}

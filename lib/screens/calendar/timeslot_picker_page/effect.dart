import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart' hide Action;
import '../../../utils/utils.dart';
import 'action.dart';
import 'state.dart';
import 'timeslot_item_component/state.dart';

Effect<TimeslotsState> buildEffect() {
  return combineEffects(<Object, Effect<TimeslotsState>>{
    TimeslotsAction.handleStartAt: _handleStartAt,
    TimeslotsAction.handleSelectDuration: _handleSelectDuration,
    TimeslotsAction.confirm: _confirm,
  });
}

void _handleSelectDuration(Action action, Context<TimeslotsState> ctx) async {
  int index = await showDialog(
    context: ctx.context,
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text('Duration'),
        content: Container(
          width: double.maxFinite,
          child: ListView.builder(
            shrinkWrap: true,
            itemCount: ctx.state.durations.length,
            itemBuilder: (context, index) {
              return ListTile(
                onTap: () {
                  Navigator.pop(context, index);
                },
                title: Text('${ctx.state.durations[index]}'),
              );
            },
          ),
        ),
      );
    },
  );
  if (index != null)
    ctx.dispatch(TimeslotsActionCreator.selectedDuration(index));
}

void _confirm(Action action, Context<TimeslotsState> ctx) {
  TimeslotItemState timeSlot = new TimeslotItemState();
  timeSlot.startAt = ctx.state.dateTime;
  timeSlot.durationInMinute = ctx.state.duration;
  ctx.goBack(timeSlot);
}

Future<void> _handleStartAt(Action action, Context<TimeslotsState> ctx) async {
  var index = await showDialog(
    context: ctx.context,
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text('Start Time'),
        content: Container(
          width: double.maxFinite,
          child: ListView.builder(
            shrinkWrap: true,
            itemCount: ctx.state.startAt.length,
            itemBuilder: (context, index) {
              return ListTile(
                onTap: () {
                  ctx.goBack(index);
                },
                title: Text('${ctx.state.startAt[index]}'),
              );
            },
          ),
        ),
      );
    },
  );
  if (index != null)
    ctx.dispatch(TimeslotsActionCreator.selectedStartAt(index));
}

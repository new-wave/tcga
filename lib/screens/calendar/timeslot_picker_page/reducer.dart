import 'package:fish_redux/fish_redux.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';
import 'package:tcga_flutter/utils/format_date.dart';
import 'action.dart';
import 'state.dart';

Reducer<TimeslotsState> buildReducer() {
  return asReducer(
    <Object, Reducer<TimeslotsState>>{
      TimeslotsAction.action: _onAction,
      TimeslotsAction.handleSelectTimeSlot: _handleSelectTimeSlot,
      TimeslotsAction.selectedStartAt: _selectedStartAt,
      TimeslotsAction.selectedDuration: _selectedDuration,
    },
  );
}

TimeslotsState _selectedDuration(TimeslotsState state, Action action) {
  final TimeslotsState newState = state.clone();
  final index = action.payload as int;
  newState.duration = index;
  return newState;
}

TimeslotsState _selectedStartAt(TimeslotsState state, Action action) {
  final TimeslotsState newState = state.clone();
  final index = action.payload as int;
  DateTime dateTime = state.dateTime;
  dateTime = DateTime(dateTime.year, dateTime.month, dateTime.day, dateTime.hour, 0);
  newState.dateTime = dateTime.add(new Duration(minutes: index * 15));
  newState.appointments.clear();
  newState.appointments.add(Appointment(
    startTime: newState.dateTime,
    endTime: newState.dateTime.add(Duration(hours: 1)),
    subject: '',
    //   color: AppColors.,
    startTimeZone: '',
    endTimeZone: '',
  ));
  return newState;
}

TimeslotsState _onAction(TimeslotsState state, Action action) {
  final TimeslotsState newState = state.clone();
  return newState;
}

TimeslotsState _handleSelectTimeSlot(TimeslotsState state, Action action) {
  final TimeslotsState newState = state.clone();
  final dateTime = action.payload as DateTime;
  newState.dateTime =
      DateTime(dateTime.year, dateTime.month, dateTime.day, dateTime.hour, 0);
  newState.appointments.clear();
  newState.appointments.add(
    Appointment(
      startTime: dateTime,
      endTime: dateTime.add(Duration(hours: 1)),
      subject: '',
      //    color: Colors.blue,
      startTimeZone: '',
      endTimeZone: '',
    ),
  );
  newState.duration = 11;
  newState.startAt = [];
  var date =
      DateTime(dateTime.year, dateTime.month, dateTime.day, dateTime.hour, 0);
  for (int i = 0; i < 4; ++i) {
    newState.startAt
        .insert(i, '${dateFormatHM(date.add(new Duration(minutes: 15 * i)))}');
  }
  newState.durations = [];
  for (int i = 1; i < 25; ++i) {
    newState.durations.add('${i * 5} mins');
  }
  return newState;
}

import 'package:fish_redux/fish_redux.dart';

enum TimeslotsAction {
  action,
  handleSelectTimeSlot,
  handleStartAt,
  selectedStartAt,
  handleSelectDuration,
  selectedDuration,
  confirm,
}

class TimeslotsActionCreator {
  static Action onAction() {
    return const Action(TimeslotsAction.action);
  }

  static Action handleSelectTimeSlot(DateTime dateTime) {
    return Action(TimeslotsAction.handleSelectTimeSlot, payload: dateTime);
  }

  static Action handleStartAt() {
    return const Action(
      TimeslotsAction.handleStartAt,
    );
  }

  static Action selectedStartAt(int i) {
    return Action(
      TimeslotsAction.selectedStartAt,
      payload: i,
    );
  }

  static Action handleSelectDuration() {
    return const Action(
      TimeslotsAction.handleSelectDuration,
    );
  }

  static Action selectedDuration(int i) {
    return Action(
      TimeslotsAction.selectedDuration,
      payload: i,
    );
  }

  static Action confirm() {
    return const Action(
      TimeslotsAction.confirm,
    );
  }
}

import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/config/style.dart';
import 'package:tcga_flutter/screens/calendar/timeslot_picker_page/validator.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/utils/format_date.dart';
import 'package:tcga_flutter/widget/import.dart';
import '../../../utils/datetime_extensions.dart';
import 'action.dart';
import 'state.dart';

Widget buildView(
    TimeslotsState state, Dispatch dispatch, ViewService viewService) {
  double x = 0;

  return Container(
    height: MediaQuery.of(viewService.context).size.height - 100,
    padding: EdgeInsets.only(top: spacingMd),
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(spacingMd),
      color: Colors.white,
    ),
    child: Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Row(
            children: [
              Container(
                margin: EdgeInsets.only(left: spacingMd),
                child: Icon(
                  Icons.close_outlined,
                  color: Colors.transparent,
                ),
              ),
              Expanded(
                child: Text(
                  AppLocalizations.of(viewService.context)
                      .text('calendar.editor.time_slot_picker.header'),
                  style: Style.labelTitleDialog,
                  textAlign: TextAlign.center,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                ),
              ),
              InkWell(
                onTap: () {
                  Navigator.pop(viewService.context);
                },
                child: Container(
                    margin: EdgeInsets.only(right: spacingMd),
                    alignment: Alignment.centerRight,
                    child: Icon(
                      Icons.close_outlined,
                    )),
              )
            ],
          ),
          Container(
            margin: EdgeInsets.only(top: spacingMd),
            child: Text(
              '${DateTime.now().toLongDateTimeString()}',
              textAlign: TextAlign.center,
              style: TextStyle(
                color: colorPrimary,
                fontSize: spacingMd,
              ),
            ),
          ),
          Expanded(
            child: GestureDetector(
              onPanDown: (details) {
                final RenderBox box = viewService.context.findRenderObject();
                final Offset localOffset =
                    box.globalToLocal(details.globalPosition);
                x = localOffset.dx;
              },
              child: SfCalendar(
                view: CalendarView.day,
                firstDayOfWeek: 1,
                headerHeight: 0,
                timeSlotViewSettings: TimeSlotViewSettings(
                  timeInterval: Duration(hours: 1),
                  timeIntervalHeight: 64,
                  timeFormat: 'HH:mm',
                  startHour: 8,
                  endHour: 18,
                ),
                onTap: (details) {
                  if (details.targetElement == CalendarElement.calendarCell) {
                    dispatch(TimeslotsActionCreator.handleSelectTimeSlot(
                        details.date));
                  } else if (details.targetElement ==
                      CalendarElement.appointment) {
                    if (x > 50) {
                      if (x <
                          (MediaQuery.of(viewService.context).size.width + 50) /
                              2) {
                        dispatch(TimeslotsActionCreator.handleStartAt());
                      } else {
                        dispatch(TimeslotsActionCreator.handleSelectDuration());
                      }
                    }
                  }
                },
                dataSource: _AppointmentDataSource(state.appointments),
                appointmentBuilder: (context, calendarAppointmentDetails) {
                  final Appointment appointment =
                      calendarAppointmentDetails.appointments.first;
                  List<String> listDurations = state.durations;
                  DateTime dateTime = appointment.startTime;
                  return Material(
                    shape: RoundedRectangleBorder(
                      side: BorderSide(
                        color: appointment.color,
                        width: 0.75,
                      ),
                      borderRadius: BorderRadius.circular(4),
                    ),
                    color: appointment.color,
                    child: Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Expanded(
                            child: GestureDetector(
                              onTap: () {
                                dispatch(
                                    TimeslotsActionCreator.handleStartAt());
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(5),
                                ),
                                margin: EdgeInsets.symmetric(
                                    horizontal: spacingXs,
                                    vertical: spacingXxs),
                                padding: EdgeInsets.symmetric(
                                    horizontal: spacingSm, vertical: spacingXs),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceAround,
                                      children: [
                                        Text(
                                          AppLocalizations.of(
                                                  viewService.context)
                                              .text(
                                                  'calendar.editor.time_slot_picker.start_at'),
                                          style: TextStyle(
                                              fontSize: 10,
                                              color: colorTextSecondary),
                                        ),
                                        Text(
                                          '${dateFormatHM(dateTime)}',
                                          style: textStyleSecondaryTabBar,
                                        ),
                                      ],
                                    ),
                                    SvgPicture.asset(
                                      'assets/icons/ic_alarm_clock.svg',
                                      color: colorPrimary,
                                      fit: BoxFit.none,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          Expanded(
                            child: GestureDetector(
                              onTap: () {
                                dispatch(TimeslotsActionCreator
                                    .handleSelectDuration());
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(5),
                                ),
                                margin: EdgeInsets.symmetric(
                                    horizontal: spacingXs,
                                    vertical: spacingXxs),
                                padding: EdgeInsets.symmetric(
                                    horizontal: spacingSm, vertical: spacingXs),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceAround,
                                      children: [
                                        Text(
                                          AppLocalizations.of(
                                                  viewService.context)
                                              .text(
                                                  'calendar.editor.time_slot_picker.duration'),
                                          style: TextStyle(
                                              fontSize: 10,
                                              color: colorTextSecondary),
                                        ),
                                        Text(
                                          listDurations.isNotEmpty
                                              ? '${listDurations[state.duration]}'
                                              : '',
                                          style: textStyleSecondaryTabBar,
                                        ),
                                      ],
                                    ),
                                    SvgPicture.asset(
                                      'assets/icons/ic_duration.svg',
                                      fit: BoxFit.none,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                },
              ),
            ),
          ),
          Container(
            child: Builder(
              builder: (context) {
                return ButtonWidget(
                  onPress: () {
                    final isValid = Validator.isValid(state);
                    if (!isValid) {
                      final snackbar = SnackBar(
                        content: Container(
                          child: Text(
                            AppLocalizations.of(viewService.context)
                                .text('common.warning.timeslot_picker'),
                          ),
                        ),
                        backgroundColor: colorIndicatorWarning,
                      );
                      Scaffold.of(context).showSnackBar(snackbar);
                      return;
                    }
                    dispatch(TimeslotsActionCreator.confirm());
                  },
                  textButton: AppLocalizations.of(viewService.context)
                      .text('common.confirm'),
                  colorButton: colorPrimary,
                );
              },
            ),
          ),
        ],
      ),
    ),
  );
}

class _AppointmentDataSource extends CalendarDataSource {
  _AppointmentDataSource(List<Appointment> source) {
    appointments = source;
  }
}

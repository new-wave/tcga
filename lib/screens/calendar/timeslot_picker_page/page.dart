import 'package:fish_redux/fish_redux.dart';

import 'effect.dart';
import 'reducer.dart';
import 'state.dart';
import 'view.dart';

class TimeslotsPage extends Page<TimeslotsState, Map<String, dynamic>> {
  TimeslotsPage()
      : super(
            initState: initState,
            effect: buildEffect(),
            reducer: buildReducer(),
            view: buildView,
            dependencies: Dependencies<TimeslotsState>(
                adapter: null,
                slots: <String, Dependent<TimeslotsState>>{
                }),
            middleware: <Middleware<TimeslotsState>>[
            ],);

}

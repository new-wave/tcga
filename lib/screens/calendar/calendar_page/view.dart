import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/database/models/enums.dart';
import 'package:tcga_flutter/screens/calendar/state.dart';
import 'package:tcga_flutter/screens/Drawer/drawer_page.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/utils/format_date.dart';

import 'action.dart';
import 'state.dart';

Widget buildView(
    CalendarState state, Dispatch dispatch, ViewService viewService) {
  return Builder(builder: (context) {
    return Container(
      child: Calendar(
        monthViewSettings: state.monthViewSettings,
        dispatch: dispatch,
        state: state,
        groupedByDayEvents: state.groupedByDayEvents,
        chosenDayEvents: state.chosenDayEvents,
        events: state.items,
        controller: state.controller,
        onAdd: () {
          // dispatch(CalendarActionCreator.create());
        },
        eventSelected: (e, date) => dispatch(
          CalendarActionCreator.edit(e, date),
        ),
        daySelected: (e) => dispatch(
          CalendarActionCreator.handleDaySelected(e),
        ),
        initialSelectedDay: state.chosenDay,
        viewService: viewService,
      ),
    );
  });
}

class Calendar extends StatefulWidget {
  final Function onAdd;
  final Function(AppointmentItemState, DateTime) eventSelected;
  final Function(DateTime) daySelected;
  final Map<DateTime, List<AppointmentItemState>> groupedByDayEvents;
  final List<AppointmentItemState> events;
  final List<AppointmentItemState> chosenDayEvents;
  final Dispatch dispatch;
  final CalendarState state;
  final DateTime initialSelectedDay;
  final ViewService viewService;

  final CalendarController controller;
  final MonthViewSettings monthViewSettings;

  Calendar({
    Key key,
    this.dispatch,
    this.state,
    this.onAdd,
    this.daySelected,
    this.eventSelected,
    this.groupedByDayEvents,
    this.chosenDayEvents,
    this.events,
    this.initialSelectedDay,
    this.viewService,
    this.controller,
    this.monthViewSettings,
  });

  @override
  _CalendarState createState() => _CalendarState();
}

class _CalendarState extends State<Calendar> with TickerProviderStateMixin {
  TabController tabController;

  @override
  void initState() {
    tabController = new TabController(length: 3, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: _appBar(widget.viewService, widget.dispatch, widget.state),
          drawer: DrawerPage(),
          body: DefaultTabController(
            length: 3,
            initialIndex: 0,
            child: Scaffold(
              body: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  TabBar(
                    onTap: (index) {
                      widget.dispatch(
                          CalendarActionCreator.handleCalendarView(index));
                    },
                    indicatorColor: colorPrimary,
                    indicatorWeight: 3,
                    unselectedLabelColor: colorTextSecondary,
                    labelColor: colorPrimary,
                    labelStyle: textStylePrimaryTabBar,
                    unselectedLabelStyle: textStyleSecondaryTabBar,
                    controller: tabController,
                    tabs: [
                      Tab(
                        text: AppLocalizations.instance.text('calendar.day'),
                      ),
                      Tab(
                        text: AppLocalizations.instance.text('calendar.week'),
                      ),
                      Tab(
                        text: AppLocalizations.instance.text('calendar.month'),
                      ),
                    ],
                  ),
                  Container(
                    height: 1,
                    color: Color(0xffE0E0E0),
                  ),
                  Expanded(
                    child: SfCalendar(
                      controller: widget.controller,
                      monthViewSettings: widget.monthViewSettings,
                      scheduleViewSettings:
                          ScheduleViewSettings(appointmentItemHeight: 150),
                      showDatePickerButton: true,
                      dataSource: MeetingDataSource(widget.events),
                      onTap: (calendarTapDetails) {
                        if (calendarTapDetails?.appointments != null &&
                            calendarTapDetails.appointments.length > 0) {
                          final AppointmentItemState appointment =
                              calendarTapDetails?.appointments?.first;
                          if (widget.controller.view == CalendarView.day &&
                              appointment != null) {
                            widget.eventSelected(
                                appointment, calendarTapDetails.date);
                          } else {
                            if (calendarTapDetails?.targetElement ==
                                    CalendarElement.appointment &&
                                appointment != null) {
                              widget.eventSelected(
                                  appointment, calendarTapDetails.date);
                            }
                          }
                        }
                      },
                      appointmentBuilder:
                          (context, calendarAppointmentDetails) {
                        final AppointmentItemState appointment =
                            calendarAppointmentDetails?.appointments?.first;
                        if (widget.controller.view == CalendarView.day) {
                          return Container(
                            padding: calendarAppointmentDetails.bounds.width >
                                    100
                                ? EdgeInsets.symmetric(horizontal: spacingMd)
                                : EdgeInsets.symmetric(horizontal: 0),
                            decoration: BoxDecoration(
                              color: appointment.isMedication()
                                  ? Color(0xffF2994A)
                                  : Color(0xff27AE60),
                              shape: BoxShape.rectangle,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(4.0)),
                            ),
                            child: calendarAppointmentDetails.bounds.height <
                                        30 ||
                                    calendarAppointmentDetails.bounds.width < 30
                                ? Container()
                                : Container(
                                    margin: EdgeInsets.only(top: spacingXxs),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        appointment.isAllDay ||
                                                calendarAppointmentDetails
                                                        .bounds.top ==
                                                    0
                                            ? new Flexible(
                                                child: Column(
                                                  children: [
                                                    Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .start,
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        new Flexible(
                                                          child: Text(
                                                            '${appointment.subject}',
                                                            overflow:
                                                                TextOverflow
                                                                    .ellipsis,
                                                            softWrap: true,
                                                            maxLines: 1,
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .white,
                                                                fontSize: 12),
                                                          ),
                                                        ),
                                                        SizedBox(
                                                          width: 20,
                                                        ),
                                                        Expanded(
                                                          child: Row(
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .start,
                                                            children: <Widget>[
                                                              SvgPicture.asset(
                                                                  "assets/icons/ic_user.svg"),
                                                              SizedBox(
                                                                width: 5,
                                                              ),
                                                              Expanded(
                                                                child: Text(
                                                                  '${appointment.participant}',
                                                                  overflow:
                                                                      TextOverflow
                                                                          .ellipsis,
                                                                  softWrap:
                                                                      true,
                                                                  maxLines: 1,
                                                                  style:
                                                                      TextStyle(
                                                                    fontSize:
                                                                        12,
                                                                    color: Colors
                                                                        .white,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w400,
                                                                  ),
                                                                ),
                                                              )
                                                            ],
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                              )
                                            : Expanded(
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Text(
                                                      '${appointment.subject}',
                                                      overflow:
                                                          TextOverflow.ellipsis,
                                                      style: TextStyle(
                                                          color: Colors.white,
                                                          fontSize: 12),
                                                    ),
                                                    Row(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceBetween,
                                                      children: <Widget>[
                                                        SvgPicture.asset(
                                                            "assets/icons/ic_user.svg"),
                                                        SizedBox(
                                                          width: 5,
                                                        ),
                                                        Expanded(
                                                          child: Text(
                                                            '${appointment.participant}',
                                                            overflow:
                                                                TextOverflow
                                                                    .ellipsis,
                                                            style: TextStyle(
                                                              fontSize: 12,
                                                              color:
                                                                  Colors.white,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w400,
                                                            ),
                                                          ),
                                                        )
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                              ),
                                        calendarAppointmentDetails
                                                    .bounds.width >
                                                100
                                            ? Container(
                                                margin:
                                                    EdgeInsets.only(left: 5),
                                                alignment: Alignment.topRight,
                                                child:
                                                    appointment.isMedication()
                                                        ? SvgPicture.asset(
                                                            "assets/icons/ic_drugs.svg",
                                                            color: Colors.white,
                                                            fit: BoxFit.none,
                                                          )
                                                        : SvgPicture.asset(
                                                            "assets/icons/teams.svg",
                                                            color: Colors.white,
                                                            fit: BoxFit.none,
                                                          ),
                                              )
                                            : Container(),
                                      ],
                                    ),
                                  ),
                          );
                        } else if (calendarAppointmentDetails
                            .isMoreAppointmentRegion) {
                          return Container(
                            width: calendarAppointmentDetails.bounds.width,
                            height: calendarAppointmentDetails.bounds.height,
                            child: Text('+More'),
                          );
                        } else {
                          return appointmentWidget(
                              appointment,
                              calendarAppointmentDetails.date,
                              widget.eventSelected,
                              context);
                        }
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

AppBar _appBar(
    ViewService viewService, Dispatch dispatch, CalendarState state) {
  return AppBar(
    elevation: 3,
    leading: state.recipientId != null
        ? IconButton(
            icon: SvgPicture.asset(
              "assets/icons/ic_back.svg",
              width: 24,
              height: 24,
            ),
            onPressed: () {
              Navigator.pop(viewService.context);
            })
        : null,
    iconTheme: IconThemeData(color: Colors.black),
    title:
        Text(AppLocalizations.of(viewService.context).text('calendar.header')),
    centerTitle: false,
    actions: [
      PopupMenuButton(
        child: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 8,
            vertical: 8,
          ),
          child: SvgPicture.asset(
            'assets/icons/ic_button_add.svg',
          ),
        ),
        itemBuilder: (_) => List.generate(
          CalendarState.appointmentTypes.length,
          (index) => new PopupMenuItem(
            child: SizedBox(
              width: 200,
              height: 40,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    height: 30,
                    width: 30,
                    decoration: BoxDecoration(
                      color: CalendarState.appointmentTypes.keys
                                  .elementAt(index) ==
                              AppointmentType.MedicationAdministration
                          ? colorIndicatorWarning
                          : colorIndicatorPositive,
                      borderRadius: BorderRadius.circular(15),
                    ),
                    child: SizedBox(
                      height: 24,
                      width: 24,
                      child: SvgPicture.asset(
                        CalendarState.appointmentTypes.keys.elementAt(index) ==
                                AppointmentType.MedicationAdministration
                            ? 'assets/icons/ic_drugs.svg'
                            : 'assets/icons/calendar.svg',
                        height: 20,
                        width: 20,
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 16,
                  ),
                  Expanded(
                    child: Text(
                      CalendarState.appointmentTypes.values.elementAt(
                        index,
                      ),
                      style: textStylePrimary,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                ],
              ),
            ),
            value: CalendarState.appointmentTypes.keys.elementAt(
              index,
            ),
          ),
        ),
        onSelected: (value) => dispatch(
          CalendarActionCreator.create(
            value,
          ),
        ),
      )
    ],
  );
}

Widget appointmentWidget(
    AppointmentItemState appointment,
    DateTime date,
    Function(AppointmentItemState, DateTime) eventSelected,
    BuildContext context) {
  return Container(
    padding: EdgeInsets.fromLTRB(3, 3, 21, 4),
    margin: EdgeInsets.only(
      left: 14,
      right: 18,
    ),
    decoration: BoxDecoration(
      color: appointment.color,
      borderRadius: BorderRadius.circular(5),
    ),
    child: Row(
      children: <Widget>[
        Container(
          alignment: Alignment.center,
          width: 69,
          padding: EdgeInsets.only(left: 10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(2),
            color: Colors.white,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                '${dateFormatHM(appointment.start)}',
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w500,
                  color: colorTextPrimary,
                ),
              ),
            ],
          ),
        ),
        SizedBox(
          width: 20,
        ),
        Expanded(
          child: Container(
            margin: EdgeInsets.symmetric(vertical: spacingSSm),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                  child: Text(
                    '${appointment.subject}',
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                    softWrap: true,
                    style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: Colors.white,
                    ),
                  ),
                ),
                Spacer(),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SvgPicture.asset("assets/icons/ic_user.svg"),
                    SizedBox(
                      width: 5,
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.3,
                      child: Text(
                        '${appointment.participant}',
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          fontSize: 12,
                          color: Colors.white,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
        ),
        appointment.isMedication()
            ? SvgPicture.asset(
                "assets/icons/ic_drugs.svg",
                color: Colors.white,
              )
            : SvgPicture.asset(
                "assets/icons/teams.svg",
                color: Colors.white,
              ),
      ],
    ),
  );
}

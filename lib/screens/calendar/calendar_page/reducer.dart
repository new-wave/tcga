import 'dart:math';

import 'package:fish_redux/fish_redux.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';
import 'package:tcga_flutter/screens/calendar/state.dart';
import 'package:tcga_flutter/screens/calendar/timeslot_picker_page/timeslot_item_component/state.dart';
import 'action.dart';
import 'state.dart';
import '../../../utils/utils.dart';

//final f = new DateFormat('d/MM/y');
Reducer<CalendarState> buildReducer() {
  return asReducer(
    <Object, Reducer<CalendarState>>{
      CalendarAction.initialize: _initialize,
      CalendarAction.handleDaySelected: _handleDaySelected,
      CalendarAction.handleAppointmentCreated: _handleEventCreated,
      CalendarAction.handleAppointmentEdited: _handleEventEdited,
      CalendarAction.handleAppointmentDeleted: _handleEventDeleted,
      CalendarAction.handleCalendarView: _handleCalendarView,
    },
  );
}

CalendarState _initialize(CalendarState state, Action action) {
  final newState = state.clone();
  List<AppointmentItemState> items = action.payload;
  newState.items = createNewAppointmentByTimeSlot(items, false);
  return newState;
}

List<AppointmentItemState> createNewAppointmentByTimeSlot(
    List<AppointmentItemState> items, bool isEdit) {
  List<AppointmentItemState> newAppointmentItems = [];
  for (AppointmentItemState appointment in items) {
    if (appointment.timeSlots != null && appointment.timeSlots.isNotEmpty) {
      for (TimeslotItemState timeSlot in appointment.timeSlots) {
        DateTime startTime = DateTime(
          appointment?.start?.year,
          appointment?.start?.month,
          appointment?.start?.day,
          timeSlot.startAt?.hour,
          timeSlot.startAt?.minute,
          timeSlot.startAt?.second,
          Random().nextInt(1000),
        );

        startTime = startTime.getCurrentTimeZone(appointment.timeZone);

        DateTime timeSlotEndTime = timeSlot.startAt.add(
          Duration(minutes: timeSlot.durationInMinute * 5 + 5),
        );
        DateTime endTime = DateTime(
          appointment?.start?.year,
          appointment?.start?.month,
          appointment?.start?.day,
          timeSlotEndTime.hour,
          timeSlotEndTime.minute,
          timeSlotEndTime.second,
        );
        endTime =
            endTime.subtract(Duration(milliseconds: Random().nextInt(1000)));
        endTime = endTime.getCurrentTimeZone(appointment.timeZone);

        newAppointmentItems.add(appointment.clone()
          ..start = startTime
          ..end = endTime);
      }
    } else {
      if (isEdit) {
        appointment.start = appointment.start
            .add(Duration(milliseconds: Random().nextInt(1000)));
      }
      newAppointmentItems.add(appointment);
    }
  }
  return newAppointmentItems;
}

List<AppointmentItemState> initAppointment(List<AppointmentItemState> items,
    AppointmentItemState newItem, bool isEdit) {
  if (isEdit) {
    items.removeWhere((element) => element.id == newItem?.id);
  }
  List<AppointmentItemState> editedItems =
      createNewAppointmentByTimeSlot([newItem], isEdit);
  for (AppointmentItemState editedItem in editedItems) {
    items.add(editedItem);
  }
  return items;
}

CalendarState _handleDaySelected(CalendarState state, Action action) {
  return state.clone()..chosenDay = action.payload;
}

CalendarState _handleEventCreated(CalendarState state, Action action) {
  final newState = state.clone();
  newState.items = initAppointment(state.items, action.payload, false);
  return newState;
}

CalendarState _handleEventEdited(CalendarState state, Action action) {
  final newState = state.clone();
  newState.items = initAppointment(state.items, action.payload, true);
  return newState;
}

CalendarState _handleEventDeleted(CalendarState state, Action action) {
  final newState = state.clone();
  String id = action.payload as String;
  newState.items.removeWhere((element) => element.id == id);
  return newState;
}

CalendarState _handleCalendarView(CalendarState state, Action action) {
  final newState = state.clone();

  final index = action.payload;

  if (index == -1) return state;
  if (index == 0) {
    newState.controller.view = CalendarView.day;
  } else if (index == 1) {
    newState.controller.view = CalendarView.month;
    newState.monthViewSettings = MonthViewSettings(
        numberOfWeeksInView: 1,
        agendaItemHeight: 60,
        agendaViewHeight: 440,
        showAgenda: true,
        appointmentDisplayMode: MonthAppointmentDisplayMode.indicator,
        navigationDirection: MonthNavigationDirection.horizontal);
  } else {
    newState.controller.view = CalendarView.month;
    newState.monthViewSettings = MonthViewSettings(
        appointmentDisplayMode: MonthAppointmentDisplayMode.indicator,
        navigationDirection: MonthNavigationDirection.horizontal);
  }
  return newState;
}

import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/contacts/manager.dart';

import '../../../route_name.dart';
import '../../../utils/utils.dart';
import '../manager.dart';
import '../state.dart';
import 'action.dart';
import 'state.dart';

final _manager = AppointmentManager();
final _contactManager = ContactManager();

Effect<CalendarState> buildEffect() {
  return combineEffects(<Object, Effect<CalendarState>>{
    Lifecycle.initState: _init,
    CalendarAction.create: _create,
    CalendarAction.edit: _edit,
  });
}

void _init(Action action, Context<CalendarState> ctx) async {
  final items = await _manager.query(recipientId: ctx.state?.recipientId);
  for (var item in items) {
    var contactItemState = (await _contactManager.get(
      item.participant,
    ));
    item.participant = contactItemState?.fullName;
    item.recipientId = contactItemState?.id;
  }
  ctx.dispatch(
    CalendarActionCreator.initialize(
      items,
    ),
  );
}

void _create(Action action, Context<CalendarState> ctx) async {
  final result = await ctx.navigateTo(
    RouteName.appointmentEditor,
    arguments: {
      'appointment-type': action.payload,
    },
  );

  if (result is AppointmentItemState) {
    var contactItemState = (await _contactManager.get(
      result.participant,
    ));
    result.participant = contactItemState?.fullName;
    result.recipientId = contactItemState?.id;
    ctx.dispatch(
      CalendarActionCreator.handleAppointmentCreated(
        result,
      ),
    );
  }
}

void _edit(Action action, Context<CalendarState> ctx) async {
  final result = await ctx.navigateTo(
    RouteName.appointmentEditor,
    arguments: {
      'item': action.payload,
    },
  );

  if (result is String) {
    ctx.dispatch(
      CalendarActionCreator.handleAppointmentDeleted(
        result,
      ),
    );
  }

  if (result is AppointmentItemState) {
    var contactItemState = (await _contactManager.get(
      result.participant,
    ));
    result.participant = contactItemState?.fullName;
    result.recipientId = contactItemState?.id;
    ctx.dispatch(
      CalendarActionCreator.handleAppointmentEdited(
        result,
      ),
    );
  }

  if (result is List<AppointmentItemState>) {
    if (result.length > 0) {
      var contactItemState0 = (await _contactManager.get(
        result[0].participant,
      ));
      result[0].participant = contactItemState0?.fullName;
      result[0].recipientId = contactItemState0?.id;
      ctx.dispatch(
        CalendarActionCreator.handleAppointmentEdited(
          result[0],
        ),
      );
    }

    if (result.length > 1) {
      var contactItemState1 = (await _contactManager.get(
        result[1].participant,
      ));
      result[1].participant = contactItemState1?.fullName;
      result[1].recipientId = contactItemState1?.id;
      ctx.dispatch(
        CalendarActionCreator.handleAppointmentCreated(
          result[1],
        ),
      );
    }
  }
}

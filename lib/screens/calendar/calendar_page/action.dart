import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/database/models/enums.dart';

import '../../../utils/utils.dart';

import '../state.dart';

enum CalendarAction {
  initialize,
  create,
  handleAppointmentCreated,
  edit,
  handleAppointmentEdited,
  handleAppointmentDeleted,
  handleDaySelected,
  handleCalendarView,
}

class CalendarActionCreator {
  static Action initialize(List<AppointmentItemState> items) {
    return Action(
      CalendarAction.initialize,
      payload: items,
    );
  }

  static Action create(AppointmentType type) {
    return Action(
      CalendarAction.create,
      payload: type,
    );
  }

  static Action edit(AppointmentItemState item, DateTime dateTime) {
    item.selectedDay = dateTime;
    return Action(
      CalendarAction.edit,
      payload: item,
    );
  }

  static Action handleAppointmentCreated(AppointmentItemState item) {
    return Action(
      CalendarAction.handleAppointmentCreated,
      payload: item,
    );
  }

  static Action handleAppointmentEdited(AppointmentItemState item) {
    return Action(
      CalendarAction.handleAppointmentEdited,
      payload: item,
    );
  }

  static Action handleAppointmentDeleted(String id) {
    return Action(
      CalendarAction.handleAppointmentDeleted,
      payload: id,
    );
  }

  static Action handleDaySelected(DateTime day) {
    return Action(
      CalendarAction.handleDaySelected,
      payload: day.today(),
    );
  }

  static Action handleCalendarView(int index) {
    return Action(
      CalendarAction.handleCalendarView,
      payload: index,
    );
  }
}

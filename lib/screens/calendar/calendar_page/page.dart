import 'package:fish_redux/fish_redux.dart';
import 'effect.dart';
import 'state.dart';
import 'view.dart';
import 'reducer.dart';

class CalendarPage extends Page<CalendarState, Map<String, dynamic>> {
  CalendarPage()
      : super(
          initState: initState,
          effect: buildEffect(),
          reducer: buildReducer(),
          view: buildView,
        );
}

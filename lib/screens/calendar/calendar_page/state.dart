import 'package:collection/collection.dart';
import 'package:fish_redux/fish_redux.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';
import 'package:tcga_flutter/database/models/enums.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';

import '../../../utils/utils.dart';

import '../state.dart';

class CalendarState implements Cloneable<CalendarState> {
  static final appointmentTypes = {
    AppointmentType.MedicationAdministration: AppLocalizations.instance
        .text('calendar.popup_menu.medication_schedule'),
    AppointmentType.ActivityDailyLiving:
        AppLocalizations.instance.text('calendar.popup_menu.appointment'),
    // AppointmentType.Meeting: 'Meeting',
  };

  CalendarController controller;
  MonthViewSettings monthViewSettings;
  List<AppointmentItemState> items;
  DateTime chosenDay;
  String recipientId;

  Map<DateTime, List<AppointmentItemState>> get groupedByDayEvents =>
      groupBy(this.items, (x) => x.start);

  List<AppointmentItemState> get chosenDayEvents =>
      groupedByDayEvents.containsKey(chosenDay)
          ? groupedByDayEvents[chosenDay]
          : [];

  @override
  CalendarState clone() {
    return CalendarState()
      ..items = [...items]
      ..chosenDay = chosenDay
      ..controller = controller
      ..monthViewSettings = monthViewSettings
      ..recipientId = recipientId;
  }
}

CalendarState initState(Map<String, dynamic> args) {
  String recipientId;
  if (args != null) {
    recipientId = args.containsKey('recipient-id')
        ? args['recipient-id'] as String
        : null;
  }

  return CalendarState()
    ..items = []
    ..chosenDay = DateTime.now().today()
    ..controller = CalendarController()
    ..monthViewSettings = MonthViewSettings()
    ..recipientId = recipientId;
}

import 'dart:math';

import 'package:flutter_native_timezone/flutter_native_timezone.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart' hide Appointment;
import 'package:tcga_flutter/database/models.dart';
import 'package:tcga_flutter/database/models/enums.dart';
import 'package:tcga_flutter/database/repository.dart';
import 'package:tcga_flutter/screens/calendar/timeslot_picker_page/timeslot_item_component/state.dart';
import 'package:tcga_flutter/screens/contacts/manager.dart';
import 'package:tcga_flutter/screens/contacts/state.dart';
import 'package:tcga_flutter/screens/recipients/manager.dart';
import 'package:tcga_flutter/screens/valuesets/manager.dart';
import 'package:tcga_flutter/screens/valuesets/valueset_picker_page/valueset_item_component/state.dart';
import 'package:tcga_flutter/utils/datetime_extensions.dart';
import 'package:timezone/timezone.dart';
import 'package:flutter/services.dart';
import "../../utils/string_extensions.dart";
import 'appointment_editor/state.dart';
import 'state.dart';
import 'mapper.dart';

class AppointmentManager {
  static final _singleton = AppointmentManager._internal();

  factory AppointmentManager() {
    return _singleton;
  }

  AppointmentManager._internal();

  final _appointmentRepository = Repository<Appointment>();
  final _valuesetManager = ValueSetManager();
  final _recipientManager = RecipientManager();
  final _contactManager = ContactManager();

  Future<List<AppointmentItemState>> query({String recipientId}) async {
    final filter = (Appointment appointment) {
      bool isOk = true;
      if (recipientId != null) {
        String participantId;
        if (appointment.participants != null &&
            appointment.participants.isNotEmpty) {
          participantId = appointment.participants.first.reference;
        }
        isOk = recipientId == participantId;
        if (!isOk) return isOk;
      }
      return isOk;
    };
    final items = await _appointmentRepository.query(filter: filter);

    return items.map((x) => x.toState()).toList();
  }

  Future<AppointmentEditorState> fill(AppointmentEditorState state) async {
    final newState = state.clone();
    Appointment entity;
    ValuesetItemState timeZone;
    if (state.onEditMode) {
      entity = await _appointmentRepository.get(state.id);
      timeZone = await _valuesetManager.getValuesetId(
        ValueSetType.timeZones,
        entity.timeZone,
      );
    } else {
      String currentTimeZone;
      try {
        currentTimeZone = await FlutterNativeTimezone.getLocalTimezone();
      } catch (e) {}
      if (currentTimeZone != null) {
        timeZone = await _valuesetManager.getValuesetId(
            ValueSetType.timeZones, currentTimeZone);
      }
    }

    newState.timeZone = timeZone;
    newState.timeZoneEditingController.text = timeZone.display;
    if (!state.onEditMode) {
      return newState;
    }

    newState.appointmentPriority = entity.priority != null
        ? AppointmentPriority.values[entity.priority]
        : null;
    newState.appointmentPriorityInit = entity.priority != null
        ? AppointmentPriority.values[entity.priority]
        : null;
    newState.remindBefore = entity.reminderMinutesBeforeStart;
    newState.remindBeforeInit = entity.reminderMinutesBeforeStart;

    newState.startTime = entity.startTime.getTimeZone(entity.timeZone);
    newState.endTime = entity.endTime.getTimeZone(entity.timeZone);
    newState.isAllDay = entity.isAllDay;

    newState.startTimeInit = entity.startTime.getTimeZone(entity.timeZone);
    newState.endTimeInit = entity.endTime.getTimeZone(entity.timeZone);
    newState.isAllDayInit = entity.isAllDay;
    newState.timeZoneInit = timeZone;
    newState.timeZoneEditingControllerInit.text = timeZone.display;

    if (entity.recurrenceRule != null) {
      newState.recurrencePropertiesInit = SfCalendar.parseRRule(
        entity.recurrenceRule,
        newState.startTime,
      );
      newState.recurrenceProperties = SfCalendar.parseRRule(
        entity.recurrenceRule,
        newState.startTime,
      );
    }

    if(entity.venue != null) {
      newState.venueType = VenueType.values[entity.venue];
      newState.venueTypeInit = VenueType.values[entity.venue];
    }

    newState?.selectedDate =
        newState?.startTime?.changeDay(newState?.selectedDate);
    DateTime selectedStartTime, selectedEndTime;
    int daysDifference =
        newState?.endTime?.difference(newState?.startTime)?.inDays;
    if (daysDifference == 0) {
      selectedStartTime =
          newState?.startTime?.changeDay(newState?.selectedDate);
      selectedEndTime = newState?.endTime?.changeDay(newState?.selectedDate);
    } else {
      int selectedDayDifference =
          newState?.selectedDate?.difference(newState?.startTime)?.inDays;

      int intervalDay = 1;
      int interval = newState?.recurrenceProperties?.interval;
      switch (newState?.recurrenceProperties?.recurrenceType) {
        case RecurrenceType.daily:
          intervalDay = interval ?? 1;
          break;
        case RecurrenceType.weekly:
          intervalDay = interval * 7 ?? 7;
          break;
        case RecurrenceType.monthly:
          DateTime startDate = newState?.selectedStartTime;
          DateTime dateAfterRecurrence = startDate.addMonth(interval);
          interval = dateAfterRecurrence.difference(startDate).inDays;
          break;
        case RecurrenceType.yearly:
          DateTime startDate = newState?.selectedEndTime;
          DateTime dateAfterRecurrence = startDate.addYear(interval);
          interval = dateAfterRecurrence.difference(startDate).inDays;
          break;
      }

      if (intervalDay != 0) {
        int days = selectedDayDifference % intervalDay;
        selectedStartTime = newState?.startTime
            ?.add(Duration(days: selectedDayDifference - days));
        selectedEndTime = newState?.endTime
            ?.add(Duration(days: selectedDayDifference - days));
      } else {
        selectedStartTime = newState?.startTime;
        selectedEndTime = newState?.endTime;
      }
    }
    newState.selectedStartTime = selectedStartTime;
    newState.selectedEndTime = selectedEndTime;

    if (state.appointmentType == AppointmentType.MedicationAdministration) {
      final medication = await _valuesetManager.getValuesetId(
        ValueSetType.medication,
        entity.reference,
      );
      newState.medication = medication;
      newState.medicationInit = medication;
      newState.medicationEditingController.text = medication.display;

      final recipient = await _recipientManager.get(
        entity.participants.first.reference,
      );
      newState.recipient = recipient;
      newState.recipientInit = recipient;
      final timeslots = entity.timeSlots
          .map(
            (x) => TimeslotItemState(
              startAt: x.startAt,
              durationInMinute: x.durationInMinute,
            ),
          )
          .toList();
      timeslots.forEach((element) {
        newState.timeslots.add(element);
        newState.timeslotsInit.add(element);
      });

      newState.appointmentStatus = entity.status != null
          ? AppointmentStatus.values[entity.status]
          : null;
      newState.appointmentStatusInit = entity.status != null
          ? AppointmentStatus.values[entity.status]
          : null;
    } else {
      final serviceType = await _valuesetManager.getValuesetId(
        ValueSetType.serviceType,
        entity.serviceTypeCode,
      );
      newState.serviceType = serviceType;
      newState.serviceTypeInit = serviceType;
      newState.serviceTypeEditingController.text = serviceType.display;

      final attendees = List<ContactItemState>();
      final attendeesInit = List<ContactItemState>();

      for (var item in entity.participants) {
        final contact = await _contactManager.get(
          item.reference,
        );
        attendees.add(contact);
        attendeesInit.add(contact);
      }

      newState.attendees = attendees;
      newState.attendeesInit = attendeesInit;

      newState.appointmentStyleType =
          AppointmentStyleType.values[entity.appointmentStyle];
      newState.appointmentStyleTypeInit =
          AppointmentStyleType.values[entity.appointmentStyle];
    }

    return newState;
  }

  Future<AppointmentItemState> createEvent(AppointmentEditorState state) async {
    final newState = state.clone();
    newState.startTime = newState.selectedStartTime;
    newState.endTime = newState.selectedEndTime;
    final entity = newState.toEntity();
    entity.id = await _appointmentRepository.create(entity);
    return entity.toState();
  }

  Future<AppointmentItemState> updateAllEvents(
      AppointmentEditorState state) async {
    final newState = state.clone();
    newState.startTime =
        newState.selectedStartTime.changeDay(newState.startTime);
    newState.endTime = newState.selectedEndTime.changeDay(newState.endTime);

    final entity = newState.toEntity();
    final _ = await _appointmentRepository.update(entity);

    return entity.toState();
  }

  Future<List<AppointmentItemState>> updateThisEvent(
      AppointmentEditorState state) async {
    List<AppointmentItemState> states = [];

    AppointmentEditorState stateThis = state.clone();
    stateThis.recurrenceProperties = null;
    stateThis.recurrencePropertiesInit = null;
    stateThis.startTime = stateThis.selectedStartTime;
    stateThis.endTime = stateThis.selectedEndTime;

    // stateThis.timeslots.map((e) => e.startAt = DateTime(
    //   stateThis.startTime?.year,
    //   stateThis.startTime?.month,
    //   stateThis.startTime?.day,
    //   e.startAt?.hour,
    //   e.startAt?.minute,
    //   e.startAt?.second,
    //   Random().nextInt(1000),
    // )).toList();

    if (state.recurrenceProperties != null) {
      AppointmentEditorState stateOther = state.clone();
      stateOther.subjectEditingController =
          stateOther.subjectEditingControllerInit;
      stateOther.medication = stateOther.medicationInit;
      stateOther.recipient = stateOther.recipientInit;
      stateOther.appointmentStatus = stateOther.appointmentStatusInit;
      stateOther.appointmentPriority = stateOther.appointmentPriorityInit;
      stateOther.remindBefore = stateOther.remindBeforeInit;
      stateOther.appointmentType = stateOther.appointmentTypeInit;
      stateOther.appointmentStyleType = stateOther.appointmentStyleTypeInit;
      stateOther.venueType = stateOther.venueTypeInit;
      stateOther.serviceType = stateOther.serviceTypeInit;
      stateOther.attendees = stateOther.attendeesInit;
      stateOther.isAllDay = stateOther.isAllDayInit;
      stateOther.startTime = stateOther.startTimeInit;
      stateOther.endTime = stateOther.endTimeInit;
      stateOther.timeZone = stateOther.timeZoneInit;
      stateOther.timeslots = stateOther.timeslotsInit;
      stateOther.timeZoneEditingController =
          stateOther.timeZoneEditingControllerInit;

      List<DateTime> exceptionDates = stateOther.exceptionDates ?? [];
      int daysDifference =
          stateThis.endTime.difference(stateThis.startTime).inDays;
      for (int i = 0; i <= daysDifference; i++) {
        exceptionDates.add(stateThis.startTime.add(Duration(days: i)));
      }
      stateOther.exceptionDates = exceptionDates;
      final entityOther = stateOther.toEntity();
      final _ = await _appointmentRepository.update(entityOther);
      states.add(entityOther.toState());
    }

    final entityThis = stateThis.toEntity();
    final resultEntityThis = state.recurrenceProperties != null
        ? await _appointmentRepository.create(entityThis)
        : await _appointmentRepository.update(entityThis);
    states.add(entityThis.toState());
    return states;
  }

  Future<List<AppointmentItemState>> updateThisAndFollowingEvents(
      AppointmentEditorState state) async {
    List<AppointmentItemState> states = [];
    AppointmentEditorState stateThisAndFollowing = state.clone();
    stateThisAndFollowing.startTime = stateThisAndFollowing.selectedStartTime;
    stateThisAndFollowing.endTime = stateThisAndFollowing.selectedEndTime;

    if (state.recurrencePropertiesInit != null) {
      AppointmentEditorState stateOther = state.clone();
      stateOther.subjectEditingController =
          stateThisAndFollowing.subjectEditingControllerInit;
      stateOther.medication = stateOther.medicationInit;
      stateOther.recipient = stateOther.recipientInit;
      stateOther.appointmentStatus = stateOther.appointmentStatusInit;
      stateOther.appointmentPriority = stateOther.appointmentPriorityInit;
      stateOther.appointmentType = stateOther.appointmentTypeInit;
      stateOther.serviceType = stateOther.serviceTypeInit;
      stateOther.remindBefore = stateOther.remindBeforeInit;
      stateOther.appointmentStyleType = stateOther.appointmentStyleTypeInit;
      stateOther.venueType = stateOther.venueTypeInit;
      stateOther.attendees = stateOther.attendeesInit;
      stateOther.isAllDay = stateOther.isAllDayInit;
      stateOther.startTime = stateOther.startTimeInit;
      stateOther.endTime = stateOther.endTimeInit;
      stateOther.timeslots = stateOther.timeslotsInit;
      stateOther.timeZone = stateOther.timeZoneInit;
      stateOther.timeZoneEditingController =
          stateOther.timeZoneEditingControllerInit;
      stateOther.recurrenceProperties = stateOther.recurrencePropertiesInit;
      stateOther.recurrenceProperties.endDate = DateTime(
          stateOther.selectedStartTime.year,
          stateOther.selectedStartTime.month,
          stateOther.selectedStartTime.day -
              stateOther.recurrenceProperties.interval);
      final entityOther = stateOther.toEntity();
      final _ = await _appointmentRepository.update(entityOther);
      states.add(entityOther.toState());
    }

    final entityThis = stateThisAndFollowing.toEntity();
    final resultEntityThis = state.recurrencePropertiesInit != null
        ? await _appointmentRepository.create(entityThis)
        : await _appointmentRepository.update(entityThis);
    states.add(entityThis.toState());
    return states;
  }

  Future<bool> delete(String id) async {
    return await _appointmentRepository.delete(id);
  }
}

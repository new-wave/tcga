import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/calendar/appointment_editor/appointment_time_component/component.dart';
import 'package:tcga_flutter/screens/recipients/recipient_picker_component/component.dart';

import 'attendees_picker_component/component.dart';
import 'state.dart';
import 'timeslots_compoment/component.dart';
import 'view.dart';
import 'effect.dart';
import 'reducer.dart';

class AppointmentEditorPage
    extends Page<AppointmentEditorState, Map<String, dynamic>> {
  AppointmentEditorPage()
      : super(
          initState: initState,
          view: buildView,
          effect: buildEffect(),
          reducer: buildReducer(),
          dependencies: Dependencies<AppointmentEditorState>(
              adapter: null,
              slots: <String, Dependent<AppointmentEditorState>>{
                'recipient-picker': RecipientPickerStateConnector() +
                    RecipientPickerComponent(),
                'attendees-picker': AttendeesPickerStateConnector() +
                    AttendeesPickerComponent(),
                'appointment-time': NoneConn<AppointmentEditorState>() +
                    AppointmentTimeComponent(),
                'timeslots-picker': TimeslotsPickerStateConnector() +
                    TimeslotsPickerComponent(),
              }),
        );
}

import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/database/models/enums.dart';

import 'action.dart';
import 'state.dart';

Reducer<ConfirmEditorState> buildReducer() {
  return asReducer(
    <Object, Reducer<ConfirmEditorState>>{
      ConfirmEditorAction.handleChooseOption: _handleChooseOption,
    },
  );
}

ConfirmEditorState _handleChooseOption(
    ConfirmEditorState state,
    Action action,
    ) {
  final ConfirmEditorState newState = state.clone();
  RecurrenceEventEditorType index = action.payload;
  newState.selectedOption = index;
  return newState;
}

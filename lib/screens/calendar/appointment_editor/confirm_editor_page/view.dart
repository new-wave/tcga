import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/config/style.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/widget/button_widget.dart';
import 'package:tcga_flutter/widget/entry_widget.dart';

import 'action.dart';
import 'state.dart';

Widget buildView(
  ConfirmEditorState state,
  Dispatch dispatch,
  ViewService viewService,
) {
  return Center(
    child: Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.vertical(top: Radius.circular(15.0)),
        color: Colors.white,
      ),
      width: MediaQuery.of(viewService.context).size.width - 64,
      height: 230,
      child: GestureDetector(
        onTap: () {
          dispatch(ConfirmEditorActionCreator.unFocus());
        },
        child: Material(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(
                height: 16.0,
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: spacingMd),
                child: Row(
                  children: [
                    Text(
                      AppLocalizations.of(viewService.context)
                          .text('calendar.editor.saving_recurrence_event'),
                      textAlign: TextAlign.start,
                      style: Style.labelNormal,
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 4.0,
              ),
              Spacer(),
              ListView.builder(
                shrinkWrap: true,
                padding: const EdgeInsets.all(8),
                itemCount: state.options.length,
                itemBuilder: (BuildContext context, int index) {
                  return Row(
                    children: [
                      Radio(
                        value: state.options[index],
                        groupValue: state.selectedOption,
                        onChanged: (value) {
                          dispatch(
                              ConfirmEditorActionCreator.handleChooseOption(
                                  value));
                        },
                      ),
                      Text(
                        state.optionTexts[index],
                        style: Style.labelNormal,
                      ),
                    ],
                  );
                },
              ),
              Spacer(),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  GestureDetector(
                    onTap: () {
                      Navigator.of(viewService.context).pop();
                    },
                    child: Text(
                      AppLocalizations.instance.text('common.cancel'),
                    ),
                  ),
                  SizedBox(
                    width: 16.0,
                  ),
                  GestureDetector(
                    onTap: () {
                      dispatch(ConfirmEditorActionCreator.save());
                    },
                    child: Text(
                      AppLocalizations.instance.text('common.save'),
                    ),
                  ),
                  SizedBox(
                    width: 16.0,
                  ),
                ],
              ),
              SizedBox(
                height: 16.0,
              ),
            ],
          ),
        ),
      ),
    ),
  );
}

import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart' hide Action, Page;
import '../../../../utils/utils.dart';
import 'action.dart';
import 'state.dart';

Effect<ConfirmEditorState> buildEffect() {
  return combineEffects(<Object, Effect<ConfirmEditorState>>{
    ConfirmEditorAction.save: _save,
    ConfirmEditorAction.unFocus: _unFocus,
  });
}

void _save(
  Action action,
  Context<ConfirmEditorState> ctx,
) async {
  ctx.goBack({
    'selectedOption': ctx.state.selectedOption,
  });
}

void _unFocus(Action action, Context<ConfirmEditorState> ctx) {
  FocusScopeNode currentFocus = FocusScope.of(ctx.context);
  if (!currentFocus.hasPrimaryFocus) {
    currentFocus.unfocus();
  }
}

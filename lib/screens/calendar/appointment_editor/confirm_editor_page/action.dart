import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/database/models/enums.dart';

//TODO replace with your own action
enum ConfirmEditorAction {
  handleChooseOption,
  save,
  unFocus,
}

class ConfirmEditorActionCreator {
  static Action handleChooseOption(RecurrenceEventEditorType value) {
    return Action(ConfirmEditorAction.handleChooseOption,
        payload: value);
  }

  static Action save() {
    return Action(
      ConfirmEditorAction.save,
    );
  }

  static Action unFocus() {
    return Action(
      ConfirmEditorAction.unFocus,
    );
  }
}

import 'package:fish_redux/fish_redux.dart';

import 'effect.dart';
import 'reducer.dart';
import 'state.dart';
import 'view.dart';

class ConfirmEditorPage extends Page<ConfirmEditorState, Map<String, dynamic>> {
  ConfirmEditorPage()
      : super(
    initState: initState,
    effect: buildEffect(),
    reducer: buildReducer(),
    view: buildView,
    dependencies: Dependencies<ConfirmEditorState>(
        adapter: null,
        slots: <String, Dependent<ConfirmEditorState>>{
        }),
    middleware: <Middleware<ConfirmEditorState>>[
    ],);

}

import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart' hide Action, Pahe;
import 'package:tcga_flutter/database/models/enums.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';

List<String> optionStrings = [
  AppLocalizations.instance.text('calendar.editor.this_event'),
  AppLocalizations.instance.text('calendar.editor.this_following_events'),
  AppLocalizations.instance.text('calendar.editor.all_events')
];

class ConfirmEditorState implements Cloneable<ConfirmEditorState> {
  RecurrenceEventEditorType selectedOption;
  List<RecurrenceEventEditorType> options;
  List<String> optionTexts;

  @override
  ConfirmEditorState clone() {
    return ConfirmEditorState()
      ..selectedOption = selectedOption
      ..options = options
      ..optionTexts = optionTexts;
  }
}

ConfirmEditorState initState(Map<String, dynamic> args) {
  final selectedOption = args['selectedOption'] as RecurrenceEventEditorType;
  final options = args['options'] as List<RecurrenceEventEditorType>;

  List<String> optionTexts = [];
  for (int i = 0; i < options.length; i++) {
    if (options[i] == RecurrenceEventEditorType.This) {
      optionTexts.add(AppLocalizations.instance.text('calendar.editor.this_event'));
    } else if (options[i] == RecurrenceEventEditorType.ThisAndFollowing) {
      optionTexts.add(AppLocalizations.instance.text('calendar.editor.this_following_events'));
    } else if (options[i] == RecurrenceEventEditorType.All) {
      optionTexts.add(AppLocalizations.instance.text('calendar.editor.all_events'));
    }
  }

  return ConfirmEditorState()
    ..selectedOption = selectedOption ?? RecurrenceEventEditorType.This
    ..options = options ?? []
    ..optionTexts = optionTexts;
}

import 'package:fish_redux/fish_redux.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';
import 'package:tcga_flutter/database/models/enums.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/utils/show_picker.dart';

import '../../../route_name.dart';
import '../../../utils/utils.dart';
import 'validator.dart';
import '../manager.dart';
import 'action.dart';
import 'state.dart';

final _manager = AppointmentManager();

Effect<AppointmentEditorState> buildEffect() {
  return combineEffects(<Object, Effect<AppointmentEditorState>>{
    Lifecycle.initState: _init,
    AppointmentEditorAction.selectServiceType: _selectServiceType,
    AppointmentEditorAction.selectMedication: _selectMedication,
    AppointmentEditorAction.selectMakeRecurrence: _selectMakeRecurrence,
    AppointmentEditorAction.save: _save,
    AppointmentEditorAction.delete: _delete,
    AppointmentEditorAction.selectTimeZone: _selectTimeZone,
    AppointmentEditorAction.selectReminderOption: _selectReminderOption,
    AppointmentEditorAction.selectTime: _selectTime,
  });
}

void _init(
  Action action,
  Context<AppointmentEditorState> ctx,
) async {
  final state = await _manager.fill(ctx.state);

  ctx.dispatch(
    AppointmentEditorActionCreator.initialize(
      state,
    ),
  );
}

void _delete(Action action, Context<AppointmentEditorState> ctx) async {
  final confirmed = await ctx.confirm(
    title: AppLocalizations.instance.text('common.warning.delete.title'),
    message: AppLocalizations.instance.text('common.warning.delete.desc'),
    negativeActionTitle: AppLocalizations.instance.text('common.cancel'),
    positiveActionTitle: AppLocalizations.instance.text('common.sure'),
  );
  if (!confirmed) return;

  final result = await _manager.delete(ctx.state.id);
  if (result == null) return;
  ctx.goBack(ctx.state.id);
}

void _save(
  Action action,
  Context<AppointmentEditorState> ctx,
) async {
  final state = ctx.state;

  bool isValid = Validator.isValid(state);
  if (!isValid) {
    ctx.dispatch(AppointmentEditorActionCreator.checkField());
    return;
  }

  if (state.onEditMode) {
    if (state.recurrenceProperties != null) {
      List<RecurrenceEventEditorType> options = [];
      options.add(RecurrenceEventEditorType.This);
      options.add(RecurrenceEventEditorType.ThisAndFollowing);
      options.add(RecurrenceEventEditorType.All);

      if(state.subjectEditingController?.text != state.subjectEditingControllerInit?.text){
        state.isEditedForm = true;
      }

      if (state.isEditedForm) {
        final item = await showCustomDialog(
          ctx.context,
          RouteName.appointmentSelectionSave,
          args: {
            'selectedOption': state.optionSavingRecurrence,
            'options': options,
          },
        );

        if (item == null) return;
        RecurrenceEventEditorType option =
            item['selectedOption'] as RecurrenceEventEditorType ??
                RecurrenceEventEditorType.All;
        state.optionSavingRecurrence = option;
      }
    }
  }

  if (state.optionSavingRecurrence == RecurrenceEventEditorType.This) {
    final results = await _manager.updateThisEvent(state);
    if (results == null) return;
    ctx.goBack(results);
  } else if (state.optionSavingRecurrence ==
      RecurrenceEventEditorType.ThisAndFollowing) {
    final results = await _manager.updateThisAndFollowingEvents(state);
    if (results == null) return;
    ctx.goBack(results);
  } else {
    final result = state.onEditMode
        ? await _manager.updateAllEvents(state)
        : await _manager.createEvent(state);
    if (result == null) return;
    ctx.goBack(result);
  }
}

void _selectServiceType(
  Action action,
  Context<AppointmentEditorState> ctx,
) async {
  final item = await ctx.pickValuesetItem(
    ValueSetType.serviceType,
    confirmRequired: true,
    selectedItems: [
      if (ctx.state.serviceType != null) ctx.state.serviceType,
    ],
  );

  if (item == null) return;

  ctx.dispatch(
    AppointmentEditorActionCreator.handleServiceTypeSelected(
      item,
    ),
  );
}

void _selectTimeZone(
  Action action,
  Context<AppointmentEditorState> ctx,
) async {
  final item = await ctx.pickValuesetItem(
    ValueSetType.timeZones,
    confirmRequired: true,
    selectedItems: [
      if (ctx.state.timeZone != null) ctx.state.timeZone,
    ],
  );

  if (item == null) return;

  ctx.dispatch(
    AppointmentEditorActionCreator.handleTimeZoneSelected(
      item,
    ),
  );
}

void _selectMedication(
  Action action,
  Context<AppointmentEditorState> ctx,
) async {
  final item = await ctx.pickValuesetItem(
    ValueSetType.medication,
    confirmRequired: true,
    selectedItems: [
      if (ctx.state.medication != null) ctx.state.medication,
    ],
  );

  if (item == null) return;

  ctx.dispatch(
    AppointmentEditorActionCreator.handleMedicationSelected(
      item,
    ),
  );
}

void _selectReminderOption(
  Action action,
  Context<AppointmentEditorState> ctx,
) async {
  //final item = await selectReminder(ctx.context, ctx.state.remindBefore);

  final item = await showModal2(
    ctx.context,
    RouteName.appointmentAlertPicker,
    args: {
      'reminder-before': ctx.state.remindBefore,
    },
  );

  if (item == null) return;

  ctx.dispatch(
    AppointmentEditorActionCreator.handleReminderOptionSelected(
      item,
    ),
  );
}

void _selectTime(
  Action action,
  Context<AppointmentEditorState> ctx,
) async {
  final result = await showModal2(
    ctx.context,
    RouteName.appointmentTimePicker,
    args: {
      'start-time': ctx.state.selectedStartTime,
      'end-time': ctx.state.selectedEndTime,
      'timezone': ctx.state.timeZone,
      'all-day': ctx.state.isAllDay,
    },
  );

  if (result == null) return;

  ctx.dispatch(
    AppointmentEditorActionCreator.handleAppointTimeSelected(
      result,
    ),
  );
}

void _selectMakeRecurrence(
  Action action,
  Context<AppointmentEditorState> ctx,
) async {
  final result = await showModal2(
    ctx.context,
    RouteName.makeRecurrencePicker,
    args: {
      'recurrence-properties': ctx.state.recurrenceProperties,
      'start-date': ctx.state.selectedStartTime,
      'end-date': ctx.state.selectedEndTime,
    },
  );

  if (result == null) return;

  if (result is RecurrenceProperties) {
    ctx.dispatch(
      AppointmentEditorActionCreator.handleMakeRecurrenced(
        result,
      ),
    );
  } else {
    ctx.dispatch(
      AppointmentEditorActionCreator.handleMakeRecurrenced(
        null,
      ),
    );
  }
}

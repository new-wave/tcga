import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mdi/mdi.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/config/style.dart';
import 'package:tcga_flutter/database/models/enums.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/widget/import.dart';

import '../../../config/palette.dart';
import '../../../utils/enum_to_string.dart';
import '../../../widget/import.dart';
import 'action.dart';
import 'state.dart';
import 'validator.dart';

Widget buildView(
  AppointmentEditorState state,
  Dispatch dispatch,
  ViewService viewService,
) {
  return Builder(
    builder: (context) {
      return Padding(
        padding: EdgeInsets.only(
          bottom: MediaQuery.of(context).viewInsets.bottom,
        ),
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: Colors.white,
          appBar: _appBar(
            viewService,
            state,
            dispatch,
          ),
          body: Container(
            padding: EdgeInsets.all(spacingMd),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  EntryWidget(
                    controller: state.subjectEditingController,
                    title: AppLocalizations.of(viewService.context)
                        .text('calendar.editor.placeholder_type_the_subject'),
                    //type subject
                    keyboardType: TextInputType.text,
                    warningText: AppLocalizations.instance
                        .text('common.validator.subject'),
                    isRequired: true,
                    isCheckField: state.isCheckField ?? false,
                  ),
                  _verticalSpacer(),
                  if (state.medicationOnly)
                    PickerWidget(
                      title: AppLocalizations.of(viewService.context)
                          .text('calendar.editor.placeholder_medication'),
                      //medication
                      suffixIcon: SvgPicture.asset(
                        "assets/icons/ic_drugs.svg",
                        color: colorPrimary,
                      ),
                      tap: () => dispatch(
                        AppointmentEditorActionCreator.selectMedication(),
                      ),
                      controller: state.medicationEditingController,
                      warningText: AppLocalizations.instance
                          .text('common.validator.medication'),
                      isRequired: true,
                      isCheckField: state.isCheckField ?? false,
                    ),
                  if (state.medicationOnly) _verticalSpacer(),
                  if (state.medicationOnly)
                    viewService.buildComponent(
                      'recipient-picker',
                    ),
                  if (state.medicationOnly) _verticalSpacer(),
                  if (state.medicationOnly)
                    viewService.buildComponent('timeslots-picker'),
                  if (state.medicationOnly) _verticalSpacer(),
                  if (state.adlOnly)
                    PickerWidget(
                      title: AppLocalizations.of(viewService.context)
                          .text('calendar.editor.placeholder_service_type'),
                      //acti/service
                      suffixIcon: SvgPicture.asset(
                        "assets/icons/ic_list_service.svg",
                      ),
                      tap: () => dispatch(
                        AppointmentEditorActionCreator.selectServiceType(),
                      ),
                      controller: state.serviceTypeEditingController,
                      warningText: AppLocalizations.instance
                          .text('common.validator.service_type'),
                      isRequired: true,
                      isCheckField: state.isCheckField ?? false,
                    ),
                  if (state.adlOnly) _verticalSpacer(),
                  if (state.adlOnly)
                    PickerWidget(
                      items: convertEnumToList(AppointmentStyleType.values),
                      initialIndex: state.appointmentStyleType != null
                          ? AppointmentEditorState.appointmentStyleTypes.keys
                              .toList()
                              .indexOf(state.appointmentStyleType)
                          : null,
                      title: AppLocalizations.of(viewService.context)
                          .text('calendar.editor.placeholder_appointment_type'),
                      //apoi type
                      suffixIcon: SvgPicture.asset(
                        "assets/icons/ic_medical_case.svg",
                      ),
                      indexSelected: (index) => dispatch(
                        AppointmentEditorActionCreator
                            .handleAppointmentStyleTypeSelected(
                          index,
                        ),
                      ),
                      warningText: AppLocalizations.instance
                          .text('common.validator.appointment_type'),
                      isRequired: true,
                      isCheckField: state.isCheckField ?? false,
                    ),
                  if (state.adlOnly) _verticalSpacer(),
                  if (state.adlOnly)
                    viewService.buildComponent('attendees-picker'),
                  if (state.adlOnly)
                    Row(
                      children: [
                        Checkbox(
                            value: state.shouldSendInviationEmail,
                            onChanged: (value) {
                              dispatch(AppointmentEditorActionCreator
                                  .handleShouldSendInvitationEmailChanged(
                                      value));
                            }),
                        Expanded(
                          child: Text(
                            AppLocalizations.of(viewService.context).text(
                                'calendar.editor.sent_an_invitation_to_attendee_via_email'),
                            //sent an...
                            style: Style.labelNormal,
                          ),
                        ),
                      ],
                    ),
                  viewService.buildComponent('appointment-time'),
                  _verticalSpacer(),
                  if (state.medicationOnly)
                    PickerWidget(
                      warningText: AppLocalizations.instance
                          .text('common.validator.status'),
                      isRequired: true,
                      isCheckField: state.isCheckField ?? false,
                      items: AppointmentEditorState.appoitmentStatuses.values
                          .toList(),
                      initialIndex: state.appointmentStatus != null
                          ? AppointmentEditorState.appoitmentStatuses.keys
                              .toList()
                              .indexOf(state.appointmentStatus)
                          : null,
                      title: AppLocalizations.of(viewService.context)
                          .text('calendar.editor.placeholder_status'),
                      //status
                      suffixIcon: Container(
                        alignment: Alignment.center,
                        height: 24,
                        width: 24,
                        padding: const EdgeInsets.all(2),
                        decoration: BoxDecoration(
                          border: Border.all(color: colorPrimary, width: 2),
                          borderRadius: BorderRadius.circular(12),
                        ),
                        child: Icon(
                          Mdi.exclamationThick,
                          size: 16,
                        ),
                      ),
                      indexSelected: (index) => dispatch(
                        AppointmentEditorActionCreator
                            .handleAppointmentStatusSelected(
                          index,
                        ),
                      ),
                    ),
                  if (state.medicationOnly) _verticalSpacer(),
                  PickerWidget(
                    controller: state.remindBeforeEditingController,
                    tap: () => dispatch(
                      AppointmentEditorActionCreator.selectReminderOption(),
                    ),
                    title: AppLocalizations.of(viewService.context).text(
                        'calendar.editor.placeholder_alert_me'), //alert me
                    suffixIcon: SvgPicture.asset('assets/icons/ic_bell.svg'),
                  ),
                  _verticalSpacer(),
                  PickerWidget(
                    suffixIcon: SvgPicture.asset('assets/icons/ic_rotate.svg'),
                    title: AppLocalizations.of(viewService.context)
                        .text('calendar.editor.placeholder_make_recurrence'),
                    //make..
                    tap: () {
                      dispatch(AppointmentEditorActionCreator
                          .selectMakeRecurrence());
                    },
                    controller: state.makeRecurrenceEditingController,
                  ),
                  _verticalSpacer(),
                  if (state.adlOnly)
                    PickerWidget(
                      items: AppointmentEditorState.venueTypes.values.toList(),
                      initialIndex: state.venueType != null
                          ? AppointmentEditorState.venueTypes.keys
                              .toList()
                              .indexOf(
                                state.venueType,
                              )
                          : null,
                      indexSelected: (index) => dispatch(
                        AppointmentEditorActionCreator.handleVenueTypeSelected(
                          index,
                        ),
                      ),
                      title: AppLocalizations.of(viewService.context)
                          .text('calendar.editor.placeholder_venue'), //venue
                    ),
                  if (state.adlOnly) _verticalSpacer(),
                  PickerWidget(
                    items: AppointmentEditorState.appointmentPriorities.values
                        .toList(),
                    initialIndex: state.appointmentPriority != null
                        ? AppointmentEditorState.appointmentPriorities.keys
                            .toList()
                            .indexOf(
                              state.appointmentPriority,
                            )
                        : null,
                    indexSelected: (index) => dispatch(
                      AppointmentEditorActionCreator
                          .handleAppointmentPrioritySelected(
                        index,
                      ),
                    ),
                    title: AppLocalizations.of(viewService.context).text(
                        'calendar.editor.placeholder_priority'), // priority
                  ),
                ],
              ),
            ),
          ),
          bottomNavigationBar: Builder(builder: (context) {
            return ButtonWidget(
              colorButton: colorPrimary,
              textButton:
                  AppLocalizations.of(viewService.context).text('common.save'),
              onPress: () {
                dispatch(
                  AppointmentEditorActionCreator.save(),
                );
              },
            );
          }),
        ),
      );
    },
  );
}

SizedBox _verticalSpacer() {
  return SizedBox(
    height: 16,
  );
}

AppBar _appBar(
    ViewService viewService, AppointmentEditorState state, Dispatch dispatch) {
  return AppBar(
    iconTheme: IconThemeData(color: Colors.black),
    elevation: 3,
    backgroundColor: Color(0xFFFFFFFF),
    leading: IconButton(
        icon: SvgPicture.asset(
          "assets/icons/ic_back.svg",
          width: 24,
          height: 24,
        ),
        onPressed: () {
          Navigator.pop(viewService.context);
        }),
    centerTitle: false,
    title: Text(
      state.appointmentType == AppointmentType.MedicationAdministration
          ? state.onEditMode
              ? AppLocalizations.of(viewService.context)
                  .text('calendar.editor.header.medication.edit')
              : AppLocalizations.of(viewService.context)
                  .text('calendar.editor.header.medication.create')
          : state.onEditMode
              ? AppLocalizations.of(viewService.context)
                  .text('calendar.editor.header.appointment.edit')
              : AppLocalizations.of(viewService.context)
                  .text('calendar.editor.header.appointment.create'),
      style: TextStyle(
          color: Color(0xff4F4F4F), fontSize: 20, fontWeight: FontWeight.w400),
    ),
    actions: [
      if (state.onEditMode)
        IconButton(
          icon: SvgPicture.asset(
            "assets/icons/ic_trash.svg",
            width: 24,
            height: 24,
          ),
          onPressed: () => dispatch(
            AppointmentEditorActionCreator.delete(),
          ),
        ),
    ],
  );
}

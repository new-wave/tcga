import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mdi/mdi.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/screens/calendar/appointment_editor/action.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/widget/picker_widget.dart';
import 'package:tcga_flutter/utils/utils.dart';

import '../state.dart';

Widget buildView(
    AppointmentEditorState state, Dispatch dispatch, ViewService viewService) {
  return Container(
    decoration: BoxDecoration(
      color: colorBackground,
      borderRadius: BorderRadius.circular(5),
    ),
    child: Column(
      children: [
        Row(
          children: [
            Expanded(
              child: Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 16,
                ),
                child: Text(AppLocalizations.of(viewService.context)
                    .text('calendar.editor.choose_all_day')),
              ),
            ),
            Checkbox(
              value: state.isAllDay,
              onChanged: (value) => dispatch(
                AppointmentEditorActionCreator.handleIsAllDayChanged(value),
              ),
            ),
          ],
        ),
        Divider(
          color: Colors.grey,
          height: 1,
        ),
        GestureDetector(
          onTap: () => dispatch(
            AppointmentEditorActionCreator.selectTime(),
          ),
          child: Padding(
            padding: const EdgeInsets.only(
              left: 16,
              top: 16,
              right: 12,
              bottom: 4,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(AppLocalizations.of(viewService.context)
                    .text('calendar.editor.start_time_picker')),
                Text(
                  state.selectedStartTime.toFullDateTimeString(),
                ),
              ],
            ),
          ),
        ),
        GestureDetector(
          onTap: () => dispatch(
            AppointmentEditorActionCreator.selectTime(),
          ),
          child: Padding(
            padding: const EdgeInsets.only(
              left: 16,
              top: 4,
              right: 12,
              bottom: 16,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(AppLocalizations.of(viewService.context)
                    .text('calendar.editor.end_time_picker')),
                Text(
                  state.selectedEndTime.toFullDateTimeString(),
                ),
              ],
            ),
          ),
        ),
        Divider(
          color: Colors.grey,
          height: 1,
        ),
        Padding(
          padding: const EdgeInsets.only(top: 4.0, bottom: 16.0),
          child: PickerWidget(
            controller: state.timeZoneEditingController,
            title: AppLocalizations.of(viewService.context)
                .text('calendar.editor.time_zone_time_picker'),
            tap: () => dispatch(
              AppointmentEditorActionCreator.selectTimeZone(),
            ),
            suffixIcon: Icon(Mdi.earth),
          ),
        ),
      ],
    ),
  );
}

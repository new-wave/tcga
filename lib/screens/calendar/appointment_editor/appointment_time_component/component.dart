import 'package:fish_redux/fish_redux.dart';

import '../state.dart';
import 'view.dart';

class AppointmentTimeComponent extends Component<AppointmentEditorState> {
  AppointmentTimeComponent()
      : super(
          view: buildView,
          dependencies: Dependencies<AppointmentEditorState>(
            adapter: null,
            slots: <String, Dependent<AppointmentEditorState>>{},
          ),
        );
}

import 'package:fish_redux/fish_redux.dart';

import 'effect.dart';
import 'reducer.dart';
import 'state.dart';
import 'view.dart';

class AttendeesPickerComponent extends Component<AttendeesPickerState> {
  AttendeesPickerComponent()
      : super(
            effect: buildEffect(),
            reducer: buildReducer(),
            view: buildView,
            dependencies: Dependencies<AttendeesPickerState>(
                adapter: null,
                slots: <String, Dependent<AttendeesPickerState>>{
                }),);

}

import 'package:fish_redux/fish_redux.dart';

import 'action.dart';
import 'state.dart';

Reducer<AttendeesPickerState> buildReducer() {
  return asReducer(
    <Object, Reducer<AttendeesPickerState>>{
      AttendeesPickerAction.handleAttendeesSelected: _handleAttendeesSelected,
      AttendeesPickerAction.handleAttendeeRemoved: _handleAttendeeRemoved,
    },
  );
}

AttendeesPickerState _handleAttendeesSelected(
  AttendeesPickerState state,
  Action action,
) {
  return state.clone()
    ..items = action.payload
    ..isEditedForm = true;
}

AttendeesPickerState _handleAttendeeRemoved(
  AttendeesPickerState state,
  Action action,
) {
  final index = state.items.indexWhere((x) => x.id == action.payload.id);
  final newState = state.clone();
  newState.items.removeAt(index);
  newState.isEditedForm = true;
  return newState;
}

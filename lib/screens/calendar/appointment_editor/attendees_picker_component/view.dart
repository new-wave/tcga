import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/widget/import.dart';

import 'action.dart';
import 'state.dart';

Widget buildView(
    AttendeesPickerState state, Dispatch dispatch, ViewService viewService) {
  return InkWell(
    onTap: () => dispatch(
      AttendeesPickerActionCreator.selectAttendees(),
    ),
    child: ListForms(
      label: state.isRequired ? AppLocalizations.of(viewService.context)
          .text('calendar.editor.attendee_picker') : AppLocalizations.of(viewService.context)
          .text('calendar.editor.attendee'),
      careRecipientState: state.items,
      onPressed: (attendee) {
        dispatch(AttendeesPickerActionCreator.handleAttendeeRemoved(attendee));
      },
    ),
  );
}

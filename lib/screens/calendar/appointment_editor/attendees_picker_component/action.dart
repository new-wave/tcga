import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/contacts/state.dart';

enum AttendeesPickerAction {
  selectAttendees,
  handleAttendeesSelected,
  removeAttendee,
  handleAttendeeRemoved,
}

class AttendeesPickerActionCreator {
  static Action selectAttendees() {
    return const Action(
      AttendeesPickerAction.selectAttendees,
    );
  }

  static Action removeAttendee(ContactItemState attendee) {
    return Action(
      AttendeesPickerAction.removeAttendee,
      payload: attendee,
    );
  }

  static Action handleAttendeeRemoved(ContactItemState attendee) {
    return Action(
      AttendeesPickerAction.handleAttendeeRemoved,
      payload: attendee,
    );
  }

  static Action handleAttendeesSelected(List<ContactItemState> items) {
    return Action(
      AttendeesPickerAction.handleAttendeesSelected,
      payload: items,
    );
  }
}

import 'package:fish_redux/fish_redux.dart';

import '.././../../../route_name.dart';
import '.././../../../utils/utils.dart';

import 'action.dart';
import 'state.dart';

Effect<AttendeesPickerState> buildEffect() {
  return combineEffects(<Object, Effect<AttendeesPickerState>>{
    AttendeesPickerAction.selectAttendees: _selectAttendees,
  });
}

void _selectAttendees(
  Action action,
  Context<AttendeesPickerState> ctx,
) async {
  final items = await ctx.navigateTo(RouteName.contactsPicker, arguments: {
    'selected-items': [
      ...ctx.state.items,
    ],
    'multi-selection': true,
  });

  if (items == null) return;

  ctx.dispatch(
    AttendeesPickerActionCreator.handleAttendeesSelected(
      items,
    ),
  );
}

import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/contacts/state.dart';

class AttendeesPickerState implements Cloneable<AttendeesPickerState> {
  List<ContactItemState> items;
  bool isRequired;
  bool isEditedForm;

  AttendeesPickerState({
    this.items,
    this.isRequired = true,
    this.isEditedForm = false,
  });

  @override
  AttendeesPickerState clone() {
    return AttendeesPickerState()
      ..items = [...items]
      ..isRequired = isRequired
      ..isEditedForm = isEditedForm;
  }
}

AttendeesPickerState initState(Map<String, dynamic> args) {
  return AttendeesPickerState()
    ..items = args?.containsKey('items') == true ? args['items'] : []
    ..isRequired =
        args?.containsKey('isRequired') == true ? args['isRequired'] : true;
}

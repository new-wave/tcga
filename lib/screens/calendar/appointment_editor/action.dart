import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/src/material/time.dart';

import 'package:tcga_flutter/screens/valuesets/valueset_picker_page/valueset_item_component/state.dart';

import 'state.dart';

enum AppointmentEditorAction {
  initialize,
  save,
  delete,
  selectMedication,
  handleMedicationSelected,
  selectServiceType,
  handleServiceTypeSelected,
  handleAppointmentStyleTypeSelected,
  handleShouldSendInvitationEmailChanged,
  handleIsAllDayChanged,
  handleStartTimeSelected,
  handleEndTimeSelected,
  selectTimeZone,
  handleTimeZoneSelected,
  selectReminderOption,
  handleReminderOptionSelected,
  selectMakeRecurrence,
  handleMakeRecurrenced,
  handleAppointmentPrioritySelected,
  handleVenueTypeSelected,
  handleAppointmentStatusSelected,
  selectTime,
  handleAppointmentTimeSelected,
  checkField,
}

class AppointmentEditorActionCreator {
  static Action initialize(AppointmentEditorState item) {
    return Action(
      AppointmentEditorAction.initialize,
      payload: item,
    );
  }

  static Action checkField() {
    return const Action(
      AppointmentEditorAction.checkField,
    );
  }

  static Action selectServiceType() {
    return const Action(
      AppointmentEditorAction.selectServiceType,
    );
  }

  static Action handleServiceTypeSelected(ValuesetItemState item) {
    return Action(
      AppointmentEditorAction.handleServiceTypeSelected,
      payload: item,
    );
  }

  static Action handleAppointmentStyleTypeSelected(int index) {
    return Action(
      AppointmentEditorAction.handleAppointmentStyleTypeSelected,
      payload: index,
    );
  }

  static Action save() {
    return Action(
      AppointmentEditorAction.save,
    );
  }

  static Action handleShouldSendInvitationEmailChanged(bool value) {
    return Action(
      AppointmentEditorAction.handleShouldSendInvitationEmailChanged,
      payload: value,
    );
  }

  static Action handleIsAllDayChanged(bool value) {
    return Action(
      AppointmentEditorAction.handleIsAllDayChanged,
      payload: value,
    );
  }

  static Action delete() {
    return const Action(
      AppointmentEditorAction.delete,
    );
  }

  static Action handleAppointmentPrioritySelected(int index) {
    return Action(
      AppointmentEditorAction.handleAppointmentPrioritySelected,
      payload: index,
    );
  }

  static Action handleVenueTypeSelected(int index) {
    return Action(
      AppointmentEditorAction.handleVenueTypeSelected,
      payload: index,
    );
  }

  static Action selectMedication() {
    return const Action(
      AppointmentEditorAction.selectMedication,
    );
  }

  static Action handleMedicationSelected(ValuesetItemState item) {
    return Action(
      AppointmentEditorAction.handleMedicationSelected,
      payload: item,
    );
  }

  static Action handleAppointmentStatusSelected(int index) {
    return Action(
      AppointmentEditorAction.handleAppointmentStatusSelected,
      payload: index,
    );
  }

  static Action selectTimeZone() {
    return const Action(
      AppointmentEditorAction.selectTimeZone,
    );
  }

  static Action handleTimeZoneSelected(ValuesetItemState item) {
    return Action(
      AppointmentEditorAction.handleTimeZoneSelected,
      payload: item,
    );
  }

  static Action selectTime() {
    return const Action(
      AppointmentEditorAction.selectTime,
    );
  }

  static Action selectMakeRecurrence() {
    return const Action(
      AppointmentEditorAction.selectMakeRecurrence,
    );
  }

  static Action handleAppointTimeSelected(result) {
    return Action(
      AppointmentEditorAction.handleAppointmentTimeSelected,
      payload: result,
    );
  }

  static Action handleReminderOptionSelected(result) {
    return Action(
      AppointmentEditorAction.handleReminderOptionSelected,
      payload: result,
    );
  }

  static Action selectReminderOption() {
    return const Action(
      AppointmentEditorAction.selectReminderOption,
    );
  }

  static Action handleMakeRecurrenced(result) {
    return Action(
      AppointmentEditorAction.handleMakeRecurrenced,
      payload: result,
    );
  }
}

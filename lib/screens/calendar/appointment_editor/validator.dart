import 'package:tcga_flutter/database/models/enums.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';

import 'state.dart';

class Validator {
  static bool isValid(AppointmentEditorState state) {
    if (state.subjectEditingController.text == null ||
        state.subjectEditingController.text.trim().isEmpty) return false;

    if (!state.isAllDay &&
        state.endTime.difference(state.startTime).isNegative) {
      return false;
    }

    if (state.appointmentType == AppointmentType.MedicationAdministration) {
      if (state.medication == null) return false;

      if (state.timeslots.length == 0) return false;

      if (state.appointmentStatus == null) return false;
    } else {
   //   if (state.attendees.length == 0) return false;

      if (state.serviceType == null) return false;

      if (state.appointmentStyleType == null) return false;
    }

    return true;
  }
}

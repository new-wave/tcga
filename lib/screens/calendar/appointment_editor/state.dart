import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_native_timezone/flutter_native_timezone.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';
import 'package:tcga_flutter/database/models/enums.dart';
import 'package:tcga_flutter/screens/contacts/state.dart';
import 'package:tcga_flutter/screens/recipients/recipient_picker_component/state.dart';
import 'package:tcga_flutter/screens/recipients/state.dart';
import 'package:tcga_flutter/screens/valuesets/valueset_picker_page/valueset_item_component/state.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:timezone/timezone.dart';

import '../timeslot_picker_page/timeslot_item_component/state.dart';
import '../state.dart';
import 'attendees_picker_component/state.dart';
import 'timeslots_compoment/state.dart';
import '../../../utils/utils.dart';

class AppointmentEditorState implements Cloneable<AppointmentEditorState> {
  static final appointmentStyleTypes = {
    AppointmentStyleType.Checkup:
        AppLocalizations.instance.text('enums.appointment_style_type.checkup'),
    AppointmentStyleType.Emergency: AppLocalizations.instance
        .text('enums.appointment_style_type.emergency'),
    AppointmentStyleType.FollowUp:
        AppLocalizations.instance.text('enums.appointment_style_type.followUp'),
    AppointmentStyleType.Routine:
        AppLocalizations.instance.text('enums.appointment_style_type.routine'),
    AppointmentStyleType.Walkin:
        AppLocalizations.instance.text('enums.appointment_style_type.walkin'),
  };
  static final appointmentPriorities = {
    AppointmentPriority.High:
        AppLocalizations.instance.text('enums.appointment_priority.high'),
    AppointmentPriority.Medium:
        AppLocalizations.instance.text('enums.appointment_priority.medium'),
    AppointmentPriority.Low:
        AppLocalizations.instance.text('enums.appointment_priority.low'),
  };

  static final venueTypes = const {
    VenueType.Phone: 'Phone',
    VenueType.Video: 'Video',
    VenueType.InPerson: 'InPerson',
    VenueType.Unknown: 'Unknown',
  };

  static final appoitmentStatuses = {
    AppointmentStatus.Pending:
        AppLocalizations.instance.text('enums.appointment_status.pending'),
    AppointmentStatus.Scheduled:
    AppLocalizations.instance.text('enums.appointment_status.scheduled'),
    AppointmentStatus.Open:
    AppLocalizations.instance.text('enums.appointment_status.open'),
    AppointmentStatus.Completed:
        AppLocalizations.instance.text('enums.appointment_status.completed'),
    AppointmentStatus.Stopped:
        AppLocalizations.instance.text('enums.appointment_status.stopped'),
    AppointmentStatus.Taken: AppLocalizations.instance
        .text('enums.appointment_status.taken'),
    AppointmentStatus.OnHold:
    AppLocalizations.instance.text('enums.appointment_status.on_hold'),
    AppointmentStatus.Discontinued:
        AppLocalizations.instance.text('enums.appointment_status.discontinued'),
  };

  String id;
  AppointmentType appointmentType;
  AppointmentType appointmentTypeInit;
  TextEditingController subjectEditingController;
  TextEditingController subjectEditingControllerInit;

  ValuesetItemState serviceType;
  ValuesetItemState serviceTypeInit;
  TextEditingController serviceTypeEditingController;

  AppointmentStyleType appointmentStyleType;
  AppointmentStyleType appointmentStyleTypeInit;

  AppointmentStatus appointmentStatus;
  AppointmentStatus appointmentStatusInit;

  RecurrenceProperties recurrencePropertiesInit;
  RecurrenceProperties recurrenceProperties;

  bool isCheckField;

  bool isEditedForm;

  get onEditMode => id != null;

  // String get title =>
  //     appointmentType == AppointmentType.MedicationAdministration
  //         ? onEditMode ? 'Edit Medication Schedule' : 'New Medication Schedule'
  //         : onEditMode ? 'Edit Activity Of Daily' : 'New Activity Of Daily';

  List<ContactItemState> attendees;
  List<ContactItemState> attendeesInit;
  bool shouldSendInviationEmail;
  bool shouldSendInviationEmailInit;

  ValuesetItemState medication;
  ValuesetItemState medicationInit;
  TextEditingController medicationEditingController;

  bool get medicationOnly =>
      appointmentType == AppointmentType.MedicationAdministration;

  bool get adlOnly => appointmentType == AppointmentType.ActivityDailyLiving;

  RecipientItemState recipient;
  RecipientItemState recipientInit;
  List<TimeslotItemState> timeslots;
  List<TimeslotItemState> timeslotsInit;

  DateTime selectedDate;

  DateTime selectedStartTime;
  DateTime selectedEndTime;

  List<DateTime> exceptionDates;

  bool isAllDay;
  DateTime startTime;
  DateTime endTime;
  ValuesetItemState timeZone;
  TextEditingController timeZoneEditingController;
  String devicetimezone;

  bool isAllDayInit;
  DateTime startTimeInit;
  DateTime endTimeInit;
  ValuesetItemState timeZoneInit;
  TextEditingController timeZoneEditingControllerInit;

  int remindBefore;
  int remindBeforeInit;

  RecurrenceEventEditorType optionSavingRecurrence;
  bool isChangeDate;
  bool isChangeTime;
  bool isChangeTimeSlot;
  bool isChangeRecurrence;

  TextEditingController remindBeforeEditingController;

  TextEditingController makeRecurrenceEditingController;

  AppointmentPriority appointmentPriority;
  AppointmentPriority appointmentPriorityInit;

  VenueType venueType;
  VenueType venueTypeInit;

  bool isRequiredAttendee = true;
  bool isRequiredRecipient = false;

  @override
  AppointmentEditorState clone() {
    List<TimeslotItemState> timeSlotItems = [];
    for (var timeSlot in timeslots) {
      timeSlotItems.add(timeSlot);
    }

    List<TimeslotItemState> timeSlotInitItems = [];
    for (var timeSlot in timeslotsInit) {
      timeSlotInitItems.add(timeSlot);
    }

    return AppointmentEditorState()
      ..id = id
      ..isEditedForm = isEditedForm
      ..appointmentType = appointmentType
      ..appointmentTypeInit = appointmentTypeInit
      ..subjectEditingControllerInit = subjectEditingControllerInit
      ..subjectEditingController = subjectEditingController
      ..serviceType = serviceType
      ..serviceTypeInit = serviceTypeInit
      ..serviceTypeEditingController = serviceTypeEditingController
      ..appointmentStyleType = appointmentStyleType
      ..appointmentStyleTypeInit = appointmentStyleTypeInit
      ..attendees = attendees
      ..attendeesInit = attendeesInit
      ..shouldSendInviationEmail = shouldSendInviationEmail
      ..shouldSendInviationEmailInit = shouldSendInviationEmailInit
      ..selectedDate = selectedDate
      ..selectedStartTime = selectedStartTime
      ..selectedEndTime = selectedEndTime
      ..exceptionDates = exceptionDates
      ..isCheckField = isCheckField
      ..isAllDay = isAllDay
      ..startTime = startTime
      ..endTime = endTime
      ..timeZone = timeZone
      ..timeZoneEditingController = timeZoneEditingController
      ..isAllDayInit = isAllDayInit
      ..startTimeInit = startTimeInit
      ..endTimeInit = endTimeInit
      ..timeZoneInit = timeZoneInit
      ..timeZoneEditingControllerInit = timeZoneEditingControllerInit
      ..remindBefore = remindBefore
      ..remindBeforeInit = remindBeforeInit
      ..remindBeforeEditingController = remindBeforeEditingController
      ..recurrencePropertiesInit = recurrencePropertiesInit
      ..recurrenceProperties = recurrenceProperties
      ..makeRecurrenceEditingController = makeRecurrenceEditingController
      ..appointmentPriority = appointmentPriority
      ..appointmentPriorityInit = appointmentPriorityInit
      ..appointmentStatus = appointmentStatus
      ..appointmentStatusInit = appointmentStatusInit
      ..venueType = venueType
      ..venueTypeInit = venueTypeInit
      ..medication = medication
      ..medicationInit = medicationInit
      ..medicationEditingController = medicationEditingController
      ..recipient = recipient
      ..recipientInit = recipientInit
      ..timeslots = timeSlotItems
      ..timeslotsInit = timeSlotInitItems
      ..optionSavingRecurrence = optionSavingRecurrence
      ..isChangeDate = isChangeDate
      ..isChangeTime = isChangeTime
      ..isChangeTimeSlot = isChangeTimeSlot
      ..isChangeRecurrence = isChangeRecurrence
      ..isRequiredAttendee = isRequiredAttendee
      ..isRequiredRecipient = isRequiredRecipient;
  }
}

AppointmentEditorState initState(Map<String, dynamic> args) {
  final item =
      args.containsKey('item') ? args['item'] as AppointmentItemState : null;
  final appointmentType = args.containsKey('appointment-type')
      ? args['appointment-type'] as AppointmentType
      : AppointmentType.ActivityDailyLiving;
  return AppointmentEditorState()
    ..id = item?.id
    ..appointmentType = item?.appointmentType ?? appointmentType
    ..appointmentTypeInit = item?.appointmentType ?? appointmentType
    ..subjectEditingController = TextEditingController(
      text: item?.subject ?? '',
    )
    ..isEditedForm = false
    ..subjectEditingControllerInit = TextEditingController(
      text: item?.subject ?? '',
    )
    ..serviceTypeEditingController = TextEditingController(
      text: '',
    )
    ..medicationEditingController = TextEditingController(
      text: '',
    )
    ..timeZoneEditingController = TextEditingController(
      text: '',
    )
    ..remindBeforeEditingController = TextEditingController(
      text: '',
    )
    ..makeRecurrenceEditingController = TextEditingController(
      text: '',
    )
    ..isRequiredAttendee =
        appointmentType == AppointmentType.ActivityDailyLiving ? false : true
    ..isRequiredRecipient = false
    ..isCheckField = false
    ..attendees = []
    ..attendeesInit = []
    ..timeslots = []
    ..timeslotsInit = []
    ..shouldSendInviationEmail = false
    ..shouldSendInviationEmailInit = false
    ..exceptionDates = item?.exceptionDates ?? []
    ..selectedDate = item?.selectedDay ?? DateTime.now()
    ..selectedStartTime = item?.start ?? DateTime.now()
    ..selectedEndTime = item?.end ??
        DateTime.now().subtract(
          Duration(
            minutes: -60,
          ),
        )
    ..isAllDay = item?.isAllDay ?? false
    ..startTime = item?.start ?? DateTime.now()
    ..endTime = item?.end ??
        DateTime.now().subtract(
          Duration(
            minutes: -60,
          ),
        )
    ..isAllDayInit = item?.isAllDay ?? false
    ..startTimeInit = item?.start ?? DateTime.now()
    ..endTimeInit = item?.end ??
        DateTime.now().subtract(
          Duration(
            minutes: -60,
          ),
        )
    ..timeZoneEditingControllerInit = TextEditingController(
      text: '',
    )
    ..optionSavingRecurrence = RecurrenceEventEditorType.All
    ..isChangeDate = false
    ..isChangeTime = false
    ..isChangeTimeSlot = false
    ..isChangeRecurrence = false;
}

class RecipientPickerStateConnector
    extends ConnOp<AppointmentEditorState, RecipientPickerState> {
  @override
  RecipientPickerState get(AppointmentEditorState state) =>
      RecipientPickerState(
        item: state.recipient,
        isRequired: state.isRequiredRecipient,
        isEditedForm: state.isEditedForm,
      );

  @override
  void set(AppointmentEditorState state, RecipientPickerState subState) {
    state.recipient = subState.item;
    state.isRequiredRecipient = subState.isRequired;
    state.isEditedForm = subState.isEditedForm;
  }
}

class AttendeesPickerStateConnector
    extends ConnOp<AppointmentEditorState, AttendeesPickerState> {
  @override
  AttendeesPickerState get(AppointmentEditorState state) =>
      AttendeesPickerState(
        items: [...state.attendees],
        isRequired: state.isRequiredAttendee,
        isEditedForm: state.isEditedForm,
      );

  @override
  void set(AppointmentEditorState state, AttendeesPickerState subState) {
    state.isRequiredAttendee = subState.isRequired;
    state.attendees = [...subState.items];
    state.isEditedForm = subState.isEditedForm;
  }
}

class TimeslotsPickerStateConnector
    extends ConnOp<AppointmentEditorState, TimeslotsPickerState> {
  @override
  TimeslotsPickerState get(AppointmentEditorState state) =>
      TimeslotsPickerState(
        items: [...state.timeslots],
        isChangeTimeSlot: state.isChangeTimeSlot,
        isCheckField: state.isCheckField,
        isEditedForm: state.isEditedForm,
      );

  @override
  void set(AppointmentEditorState state, TimeslotsPickerState subState) {
    state.timeslots = [...subState.items];
    state.isChangeTimeSlot = subState.isChangeTimeSlot;
    state.isCheckField = subState.isCheckField;
    state.isEditedForm = subState.isEditedForm;
  }
}

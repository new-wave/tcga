import 'package:fish_redux/fish_redux.dart';
import '../../../../route_name.dart';
import 'package:tcga_flutter/utils/show_picker.dart';
import '.././../../../utils/utils.dart';
import 'action.dart';
import 'state.dart';

Effect<TimeslotsPickerState> buildEffect() {
  return combineEffects(<Object, Effect<TimeslotsPickerState>>{
    TimeslotsPickerAction.selectTimeSlot: _selectTimeSlot,
  });
}

Future<void> _selectTimeSlot(
    Action action, Context<TimeslotsPickerState> ctx) async {
  final items = await showModal2(
    ctx.context,
    RouteName.appointmentTimeSlotPicker,
  );

  if (items == null) return;
  ctx.dispatch(TimeslotsPickerActionCreator.comfirm(items));
}

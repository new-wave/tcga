import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/calendar/timeslot_picker_page/timeslot_item_component/state.dart';

class TimeslotsPickerState implements Cloneable<TimeslotsPickerState> {
  List<TimeslotItemState> items;
  bool isChangeTimeSlot;
  bool isCheckField;

  bool isEditedForm;

  TimeslotsPickerState({
    this.items,
    this.isChangeTimeSlot = false,
    this.isCheckField = false,
    this.isEditedForm = false,
  });

  @override
  TimeslotsPickerState clone() {
    return TimeslotsPickerState()
      ..items = items
      ..isChangeTimeSlot = isChangeTimeSlot
      ..isCheckField = isCheckField
      ..isEditedForm = isEditedForm;
  }
}

TimeslotsPickerState initState(Map<String, dynamic> args) {
  return TimeslotsPickerState()
    ..items = []
    ..isChangeTimeSlot = false
    ..isCheckField = false;
}

import 'package:fish_redux/fish_redux.dart';
import '../../timeslot_picker_page/timeslot_item_component/state.dart';

//TODO replace with your own action
enum TimeslotsPickerAction {
  action,
  removeTimeSlot,
  selectTimeSlot,
  comfirm,
}

class TimeslotsPickerActionCreator {
  static Action onAction() {
    return const Action(TimeslotsPickerAction.action);
  }

  static Action removeTimeSlot(TimeslotItemState timeslotItemState) {
    return Action(TimeslotsPickerAction.removeTimeSlot,
        payload: timeslotItemState);
  }

  static Action selectTimeSlot() {
    return const Action(TimeslotsPickerAction.selectTimeSlot);
  }

  static Action comfirm(TimeslotItemState timeslotItemState) {
    return Action(TimeslotsPickerAction.comfirm, payload: timeslotItemState);
  }
}

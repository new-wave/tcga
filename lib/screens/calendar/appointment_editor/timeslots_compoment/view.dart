import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/widget/time_slot_forms_widget.dart';
import 'action.dart';
import 'state.dart';

Widget buildView(
    TimeslotsPickerState state, Dispatch dispatch, ViewService viewService) {
  return Container(
    padding: const EdgeInsets.only(right: 8),
    decoration: BoxDecoration(
      color: colorBackground,
      borderRadius: BorderRadius.circular(5),
    ),
    child: InkWell(
      onTap: () => dispatch(TimeslotsPickerActionCreator.selectTimeSlot()),
      child: TimeForms(
        label: AppLocalizations.of(viewService.context)
            .text('calendar.editor.time_slot'),
        list: state.items,
        removeSelectTime: (item) {
          dispatch(TimeslotsPickerActionCreator.removeTimeSlot(item));
        },
        warningText: AppLocalizations.instance.text('common.validator.time_slot'),
        isRequired: true,
        isCheckField: state.isCheckField ?? false,
      ),
    ),
  );
}

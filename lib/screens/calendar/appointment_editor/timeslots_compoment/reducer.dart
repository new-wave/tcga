import 'package:fish_redux/fish_redux.dart';
import '../../timeslot_picker_page/timeslot_item_component/state.dart';

import 'action.dart';
import 'state.dart';

Reducer<TimeslotsPickerState> buildReducer() {
  return asReducer(
    <Object, Reducer<TimeslotsPickerState>>{
      TimeslotsPickerAction.action: _onAction,
      TimeslotsPickerAction.removeTimeSlot: _removeTimeSlot,
      TimeslotsPickerAction.comfirm: _comfirm,
    },
  );
}

TimeslotsPickerState _onAction(TimeslotsPickerState state, Action action) {
  final TimeslotsPickerState newState = state.clone();
  return newState;
}

TimeslotsPickerState _removeTimeSlot(
    TimeslotsPickerState state, Action action) {
  final item = action.payload as TimeslotItemState;
  final TimeslotsPickerState newState = state.clone();
  newState.items.remove(item);
  newState.isChangeTimeSlot = true;
  newState.isEditedForm = true;
  return newState;
}

TimeslotsPickerState _comfirm(TimeslotsPickerState state, Action action) {
  final TimeslotsPickerState newState = state.clone();
  final item = action.payload as TimeslotItemState;
  newState.items.add(item);
  newState.isChangeTimeSlot = true;
  newState.isEditedForm = true;
  return newState;
}

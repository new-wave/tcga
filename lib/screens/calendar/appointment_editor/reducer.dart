import 'package:fish_redux/fish_redux.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';
import 'package:tcga_flutter/screens/valuesets/valueset_picker_page/valueset_item_component/state.dart';
import '../../../utils/utils.dart';
import 'action.dart';
import 'state.dart';

Reducer<AppointmentEditorState> buildReducer() {
  return asReducer(
    <Object, Reducer<AppointmentEditorState>>{
      AppointmentEditorAction.initialize: _initialize,
      AppointmentEditorAction.handleServiceTypeSelected:
          _handleServiceTypeSelected,
      AppointmentEditorAction.handleAppointmentStyleTypeSelected:
          _handleAppointmentStyleTypeSelected,
      AppointmentEditorAction.handleShouldSendInvitationEmailChanged:
          _handleShouldSendInvitationEmailChanged,
      AppointmentEditorAction.handleIsAllDayChanged: _handleIsAllDayChanged,
      AppointmentEditorAction.handleStartTimeSelected: _handleStartTimeSelected,
      AppointmentEditorAction.handleEndTimeSelected: _handleEndTimeSelected,
      AppointmentEditorAction.handleTimeZoneSelected: _handleTimeZoneSelected,
      AppointmentEditorAction.handleReminderOptionSelected:
          _handleReminderOptionSelected,
      AppointmentEditorAction.handleMakeRecurrenced: _handleMakeRecurrenced,
      AppointmentEditorAction.handleAppointmentPrioritySelected:
          _handleAppointmentPrioritySelected,
      AppointmentEditorAction.handleVenueTypeSelected: _handleVenueTypeSelected,
      AppointmentEditorAction.handleMedicationSelected:
          _handleMedicationSelected,
      AppointmentEditorAction.handleAppointmentTimeSelected:
          _handleAppointmentTimeSelected,
      AppointmentEditorAction.handleAppointmentStatusSelected:
          _handleAppointmentStatusSelected,
      AppointmentEditorAction.checkField: _checkFieldReducer,
    },
  );
}

AppointmentEditorState _initialize(
  AppointmentEditorState state,
  Action action,
) {
  final AppointmentEditorState newState = action.payload.clone();

  if (newState.recurrenceProperties != null) {
    newState.makeRecurrenceEditingController.text = _getRecurrenceDisplayString(
      newState.recurrenceProperties,
    );
  }

  if (newState.remindBefore != null) {
    newState.remindBeforeEditingController.text = _getAlertDisplayString(
      newState.remindBefore,
    );
  }

  return newState;
}

AppointmentEditorState _checkFieldReducer(
    AppointmentEditorState state, Action action) {
  final AppointmentEditorState newState = state.clone();
  newState.isCheckField = true;
  newState.isEditedForm = true;
  return newState;
}

AppointmentEditorState _handleServiceTypeSelected(
  AppointmentEditorState state,
  Action action,
) {
  return state.clone()
    ..serviceType = action.payload
    ..isEditedForm = true
    ..serviceTypeEditingController.text = action.payload.display;
}

AppointmentEditorState _handleAppointmentStyleTypeSelected(
  AppointmentEditorState state,
  Action action,
) {
  final newState = state.clone();
  int index = action.payload;
  newState.appointmentStyleType =
      AppointmentEditorState.appointmentStyleTypes.keys.elementAt(
    index,
  );
  newState.isEditedForm = true;
  return newState;
}

AppointmentEditorState _handleShouldSendInvitationEmailChanged(
  AppointmentEditorState state,
  Action action,
) {
  return state.clone()
    ..isEditedForm = true
    ..shouldSendInviationEmail = !state.shouldSendInviationEmail;
}

AppointmentEditorState _handleIsAllDayChanged(
  AppointmentEditorState state,
  Action action,
) {
  return state.clone()
    ..isEditedForm = true
    ..isAllDay = !state.isAllDay
    ..isChangeTime = true;
}

AppointmentEditorState _handleStartTimeSelected(
  AppointmentEditorState state,
  Action action,
) {
  return state.clone()
    ..isEditedForm = true
    ..startTime = action.payload
    ..isChangeTime = true;
}

AppointmentEditorState _handleEndTimeSelected(
  AppointmentEditorState state,
  Action action,
) {
  return state.clone()
    ..isEditedForm = true
    ..endTime = action.payload
    ..isChangeTime = true;
}

AppointmentEditorState _handleTimeZoneSelected(
  AppointmentEditorState state,
  Action action,
) {
  final newState = state.clone()
    ..isEditedForm = true
    ..timeZone = action.payload
    ..isChangeTime = true;

  newState.timeZoneEditingController.text = '${newState.timeZone.display}';

  return newState;
}

AppointmentEditorState _handleReminderOptionSelected(
  AppointmentEditorState state,
  Action action,
) {
  // final value = action.payload as int;
  //
  // final AppointmentEditorState newState = state.clone();

  final args = action.payload;

  final remindBefore = args['reminder-before'] ?? null;
  final newState = state.clone();
  if (remindBefore != null) {
    final text = _getAlertDisplayString(remindBefore);
    newState.remindBeforeEditingController.text = text;
    newState.remindBefore = remindBefore;
  } else {
    newState.remindBeforeEditingController.text = '';
    newState.remindBefore = null;
  }
  newState.isEditedForm = true;
  return newState;
}

String _getAlertDisplayString(int remindBefore) {
  String text = 'Alert';

  final days = remindBefore ~/ 1440;
  final hours = (remindBefore % 1440) ~/ 60;
  final minutes = remindBefore % 60;

  if (days > 0) {
    text += days == 1 ? ' 1 day' : ' $days days';
  }

  if (hours > 0) {
    text += hours == 1 ? ' 1 hour' : ' $hours hours';
  }

  if (minutes > 0) {
    text += minutes == 1 ? ' 1 min' : ' $minutes mins';
  }
  return text + ' before start';
}

AppointmentEditorState _handleMakeRecurrenced(
  AppointmentEditorState state,
  Action action,
) {
  final newState = state.clone();

  if (action.payload != null) {
    final recurrenceProperties = action.payload as RecurrenceProperties;

    // TODO ?? why needs this?
    String recurrenceDisplayStringInit = _getRecurrenceDisplayString(
      newState.recurrencePropertiesInit,
    );

    recurrenceProperties.withStartDate(newState.startTime);
    newState.recurrenceProperties = recurrenceProperties;

    String recurrenceDisplayString = _getRecurrenceDisplayString(
      recurrenceProperties,
    );

    newState.isChangeRecurrence = false;
    if (recurrenceDisplayStringInit != recurrenceDisplayString) {
      newState.isChangeRecurrence = true;
    }
    newState.isEditedForm = true;
    newState.recurrenceProperties = recurrenceProperties;
    newState.makeRecurrenceEditingController.text = recurrenceDisplayString;
  } else {
    newState.isChangeRecurrence = true;
    newState.isEditedForm = true;
    newState.recurrenceProperties = null;
    newState.makeRecurrenceEditingController.text = '';
  }
  return newState;
}

String _getRecurrenceDisplayString(RecurrenceProperties recurrenceProperties) {
  if (recurrenceProperties != null) {
    final recurrenceType = recurrenceProperties.recurrenceType
        .toString()
        .split('.')
        .last
        .toLowerCase();

    switch (recurrenceProperties.recurrenceRange) {
      case RecurrenceRange.count:
        return 'Repeat every ${recurrenceProperties.interval} $recurrenceType end after ${recurrenceProperties.recurrenceCount} times.';
        break;
      case RecurrenceRange.endDate:
        return 'Repeat every ${recurrenceProperties.interval} $recurrenceType end on ${recurrenceProperties.endDate.toShortDateString()}.';
      case RecurrenceRange.noEndDate:
        return 'Repeat every ${recurrenceProperties.interval} $recurrenceType no end date.';
    }
  }
  return '';
}

AppointmentEditorState _handleAppointmentPrioritySelected(
  AppointmentEditorState state,
  Action action,
) {
  final newState = state.clone();
  int index = action.payload;
  newState.appointmentPriority = index >= 0
      ? AppointmentEditorState.appointmentPriorities.keys.elementAt(
          index,
        )
      : null;
  newState.isEditedForm = true;
  return newState;
}

AppointmentEditorState _handleVenueTypeSelected(
  AppointmentEditorState state,
  Action action,
) {
  final newState = state.clone();
  int index = action.payload;
  newState.venueType = index >= 0
      ? AppointmentEditorState.venueTypes.keys.elementAt(
          index,
        )
      : null;
  newState.isEditedForm = true;
  return newState;
}

AppointmentEditorState _handleAppointmentStatusSelected(
  AppointmentEditorState state,
  Action action,
) {
  return state.clone()
  ..isEditedForm = true
    ..appointmentStatus =
        AppointmentEditorState.appoitmentStatuses.keys.elementAt(
      action.payload,
    );
}

AppointmentEditorState _handleMedicationSelected(
  AppointmentEditorState state,
  Action action,
) {
  final newState = state.clone()..medication = action.payload;
  newState.medication = action.payload;
  newState.medicationEditingController.text = newState.medication.display;
  newState.isEditedForm = true;
  return newState;
}

AppointmentEditorState _handleAppointmentTimeSelected(
  AppointmentEditorState state,
  Action action,
) {
  final args = action.payload;
  final allDay = args['all-day'] ?? false;
  final startTime = args['start-time'] ?? DateTime.now();
  final endTime = args['end-time'] ??
      startTime.subtract(
        Duration(hours: -1),
      );
  final timeZone = args['timezone'] as ValuesetItemState;

  final newState = state.clone();

  newState.isChangeDate = false;
  if (newState.selectedStartTime.isSameDay(startTime) ||
      newState.selectedEndTime.isSameDay(endTime)) {
    newState.isChangeDate = false;
  }

  if (newState.isAllDay != allDay ||
      newState.selectedStartTime.microsecondsSinceEpoch !=
          startTime.microsecondsSinceEpoch ||
      newState.selectedEndTime.microsecondsSinceEpoch !=
          endTime.microsecondsSinceEpoch) {
    newState.isChangeTime = true;
    newState.isEditedForm = true;
  }
  newState.isAllDay = allDay;
  newState.selectedStartTime = startTime;
  newState.selectedEndTime = endTime;
  newState.timeZone = timeZone;
  newState.timeZoneEditingController.text = '${newState.timeZone.display}';

  if (newState.recurrenceProperties != null) {
    newState.recurrenceProperties.withStartDate(newState.startTime);
  }

  return newState;
}

extension RecurrencePropertiesExension on RecurrenceProperties {
  withStartDate(DateTime startDate) {
    this.startDate = startDate;
    this.month =
        recurrenceType == RecurrenceType.yearly ? startDate.month : null;
    dayOfMonth = recurrenceType == RecurrenceType.monthly ||
            recurrenceType == RecurrenceType.yearly
        ? startDate.day
        : null;
    weekDays = recurrenceType == RecurrenceType.weekly
        ? [WeekDays.values[startDate.weekday % 7]]
        : [];
  }
}

import 'state.dart';

class Validator {
  static bool isValid(AppointmentTimePickerState state) {
    if (!state.isAllDay &&
        state.endTime.difference(state.startTime).isNegative) {
      return false;
    }

    return true;
  }
}

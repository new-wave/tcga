import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart' hide Action;
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/database/models/enums.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';

import '../../../utils/utils.dart';

import 'action.dart';
import 'state.dart';
import 'validator.dart';

Effect<AppointmentTimePickerState> buildEffect() {
  return combineEffects(<Object, Effect<AppointmentTimePickerState>>{
    AppointmentTimePickerAction.selectStartTime: _selectStartTime,
    AppointmentTimePickerAction.selectEndTime: _selectEndTime,
    AppointmentTimePickerAction.selectTimeZone: _selectTimeZone,
    AppointmentTimePickerAction.confirm: _confirm,
  });
}

Future _selectStartTime(
  Action action,
  Context<AppointmentTimePickerState> ctx,
) async {
  final value = await ctx.pickTime(
    TimeOfDay(
      hour: ctx.state.startTime.hour,
      minute: ctx.state.startTime.minute,
    ),
  );

  if (value == null) return;

  ctx.dispatch(
    AppointmentTimePickerActionCreator.handleStartTimeSelected(
      value,
    ),
  );
}

Future _selectEndTime(
  Action action,
  Context<AppointmentTimePickerState> ctx,
) async {
  final value = await ctx.pickTime(
    TimeOfDay(
      hour: ctx.state.endTime.hour,
      minute: ctx.state.endTime.minute,
    ),
  );

  if (value == null) return;

  ctx.dispatch(
    AppointmentTimePickerActionCreator.handleEndTimeSelected(
      value,
    ),
  );
}

void _selectTimeZone(
  Action action,
  Context<AppointmentTimePickerState> ctx,
) async {
  final item = await ctx.pickValuesetItem(
    ValueSetType.timeZones,
    confirmRequired: true,
    selectedItems: [
      if (ctx.state.timeZone != null) ctx.state.timeZone,
    ],
  );

  if (item == null) return;

  ctx.dispatch(
    AppointmentTimePickerActionCreator.handleTimeZoneSelected(
      item,
    ),
  );
}

void _confirm(
  Action action,
  Context<AppointmentTimePickerState> ctx,
) async {
  final state = ctx.state;

  if (!Validator.isValid(state)) {
    final snackBar = SnackBar(
      content: Text(
        AppLocalizations.of(ctx.context)
            .text('common.warning.please_fill_in_valid_details'),
      ),
      backgroundColor: colorIndicatorWarning,
    );
    ctx.state.scaffoldKey.currentState.showSnackBar(snackBar);
    return;
  }

  ctx.goBack({
    'start-time': ctx.state.startTime,
    'end-time': ctx.state.endTime,
    'timezone': ctx.state.timeZone,
    'all-day': ctx.state.isAllDay,
  });
}

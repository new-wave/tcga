import 'package:fish_redux/fish_redux.dart';

import 'effect.dart';
import 'reducer.dart';
import 'state.dart';
import 'view.dart';

class AppointmentTimePickerPage extends Page<AppointmentTimePickerState, Map<String, dynamic>> {
  AppointmentTimePickerPage()
      : super(
            initState: initState,
            effect: buildEffect(),
            reducer: buildReducer(),
            view: buildView,
            dependencies: Dependencies<AppointmentTimePickerState>(
                adapter: null,
                slots: <String, Dependent<AppointmentTimePickerState>>{
                }),
            middleware: <Middleware<AppointmentTimePickerState>>[
            ],);
}

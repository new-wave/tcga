import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../../utils/utils.dart';
import 'package:tcga_flutter/screens/valuesets/valueset_picker_page/valueset_item_component/state.dart';

class AppointmentTimePickerState
    implements Cloneable<AppointmentTimePickerState> {
  bool isAllDay;
  DateTime startTime;
  TextEditingController startTimeEditingController;
  DateTime endTime;
  TextEditingController endTimeEditingController;
  final scaffoldKey = GlobalKey<ScaffoldState>();
  ValuesetItemState timeZone;
  TextEditingController timeZoneEditingController;

  @override
  AppointmentTimePickerState clone() {
    return AppointmentTimePickerState()
      ..isAllDay = isAllDay
      ..startTime = startTime
      ..startTimeEditingController = startTimeEditingController
      ..endTime = endTime
      ..endTimeEditingController = endTimeEditingController
      ..timeZone = timeZone
      ..timeZoneEditingController = timeZoneEditingController;
  }
}

AppointmentTimePickerState initState(Map<String, dynamic> args) {
  final allDay = args['all-day'] as bool ?? false;
  final startTime = args['start-time'] as DateTime ?? DateTime.now();
  final endTime = args['end-time'] as DateTime ??
      startTime.subtract(
        Duration(hours: -1),
      );
  final timeZone = args['timezone'] as ValuesetItemState;

  return AppointmentTimePickerState()
    ..isAllDay = allDay
    ..startTime = startTime
    ..endTime = endTime
    ..timeZone = timeZone
    ..timeZoneEditingController = TextEditingController(
      text: timeZone?.display,
    )
    ..startTimeEditingController = TextEditingController(
      text: startTime.toShortTimeString(),
    )
    ..endTimeEditingController = TextEditingController(
      text: endTime.toShortTimeString(),
    );
}

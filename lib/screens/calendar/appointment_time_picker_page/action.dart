import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart' hide Action;
import 'package:tcga_flutter/screens/valuesets/valueset_picker_page/valueset_item_component/state.dart';

enum AppointmentTimePickerAction {
  handleStartDateSelected,
  selectStartTime,
  handleStartTimeSelected,
  handleEndDateSelected,
  selectEndTime,
  handleEndTimeSelected,
  selectTimeZone,
  handleTimeZoneSelected,
  handleIsAllDayChanged,
  confirm,
}

class AppointmentTimePickerActionCreator {
  static Action handleStartDateSelected(DateTime value) {
    return Action(
      AppointmentTimePickerAction.handleStartDateSelected,
      payload: value,
    );
  }

  static Action selectStartTime() {
    return const Action(
      AppointmentTimePickerAction.selectStartTime,
    );
  }

  static Action handleStartTimeSelected(TimeOfDay value) {
    return Action(
      AppointmentTimePickerAction.handleStartTimeSelected,
      payload: value,
    );
  }

  static Action handleEndDateSelected(DateTime value) {
    return Action(
      AppointmentTimePickerAction.handleEndDateSelected,
      payload: value,
    );
  }

  static Action selectEndTime() {
    return const Action(
      AppointmentTimePickerAction.selectEndTime,
    );
  }

  static Action handleEndTimeSelected(TimeOfDay value) {
    return Action(
      AppointmentTimePickerAction.handleEndTimeSelected,
      payload: value,
    );
  }

  static Action handleTimeZoneSelected(ValuesetItemState item) {
    return Action(
      AppointmentTimePickerAction.handleTimeZoneSelected,
      payload: item,
    );
  }

  static Action selectTimeZone() {
    return const Action(
      AppointmentTimePickerAction.selectTimeZone,
    );
  }

  static Action handleIsAllDayChanged(bool value) {
    return Action(
      AppointmentTimePickerAction.handleIsAllDayChanged,
      payload: value,
    );
  }

  static Action confirm() {
    return const Action(
      AppointmentTimePickerAction.confirm,
    );
  }
}

import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mdi/mdi.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/config/style.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/widget/import.dart';

import 'action.dart';
import 'state.dart';
import 'validator.dart';

Widget buildView(AppointmentTimePickerState state, Dispatch dispatch,
    ViewService viewService) {
  return Container(
    padding: EdgeInsets.only(
      top: 16,
    ),
    decoration: BoxDecoration(
      borderRadius: BorderRadius.vertical(top: Radius.circular(15.0)),
      color: Colors.white,
    ),
    height: 380,
    child: Scaffold(
      key: state.scaffoldKey,
      body: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 16,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Row(
              children: [
                Spacer(
                  flex: 5,
                ),
                Text(
                  AppLocalizations.of(viewService.context)
                      .text('calendar.editor.time_picker.header'),
                  style: Style.labelTitleItem,
                ),
                Spacer(
                  flex: 4,
                ),
                InkWell(
                  onTap: () async => Navigator.pop(viewService.context),
                  child: SvgPicture.asset(
                    "assets/icons/ic_close.svg",
                  ),
                ),
              ],
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Text(
                  AppLocalizations.of(viewService.context)
                      .text('calendar.editor.time_picker.start_time'),
                  textAlign: TextAlign.start,
                  style: Style.labelNormal,
                ),
                Row(
                  children: [
                    Expanded(
                      child: Container(
                        margin: EdgeInsets.only(
                          top: spacingSm,
                        ),
                        child: Container(
                          child: DatePickerWidget(
                            initial: state.startTime,
                            max: state.startTime.add(new Duration(days: 730)),
                            min: state.startTime
                                .subtract(new Duration(days: 730)),
                            dateSelected: (date) => dispatch(
                              AppointmentTimePickerActionCreator
                                  .handleStartDateSelected(
                                date,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 16,
                    ),
                    Container(
                      width: 144,
                      margin: EdgeInsets.only(
                        top: spacingSm,
                      ),
                      child: PickerWidget(
                        tap: () => dispatch(
                          AppointmentTimePickerActionCreator.selectStartTime(),
                        ),
                        controller: state.startTimeEditingController,
                        suffixIcon: SvgPicture.asset(
                          'assets/icons/ic_alarm_clock.svg',
                          color: colorPrimary,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
            Spacer(),
            Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Text(
                  AppLocalizations.of(viewService.context)
                      .text('calendar.editor.time_picker.end_time'),
                  textAlign: TextAlign.start,
                  style: Style.labelNormal,
                ),
                Row(
                  children: [
                    Expanded(
                      child: Container(
                        margin: EdgeInsets.only(
                          top: spacingSm,
                        ),
                        child: Container(
                          child: DatePickerWidget(
                            initial: state.endTime,
                            max: state.endTime.add(new Duration(days: 730)),
                            min:
                                state.endTime.subtract(new Duration(days: 730)),
                            dateSelected: (date) => dispatch(
                              AppointmentTimePickerActionCreator
                                  .handleEndDateSelected(
                                date,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 16,
                    ),
                    Container(
                      width: 144,
                      margin: EdgeInsets.only(
                        top: spacingSm,
                      ),
                      child: PickerWidget(
                        tap: () => dispatch(
                          AppointmentTimePickerActionCreator.selectEndTime(),
                        ),
                        controller: state.endTimeEditingController,
                        suffixIcon: SvgPicture.asset(
                          'assets/icons/ic_alarm_clock.svg',
                          color: colorPrimary,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
            Spacer(),
            PickerWidget(
              controller: state.timeZoneEditingController,
              title: AppLocalizations.of(viewService.context)
                  .text('calendar.editor.time_zone_time_picker'),
              tap: () => dispatch(
                AppointmentTimePickerActionCreator.selectTimeZone(),
              ),
              suffixIcon: Icon(Mdi.earth),
            ),
            Divider(
              color: Colors.white,
              height: spacingSm,
            ),
            Container(
              child: Row(
                children: [
                  Checkbox(
                    value: state.isAllDay,
                    onChanged: (value) => dispatch(
                      AppointmentTimePickerActionCreator.handleIsAllDayChanged(
                        value,
                      ),
                    ),
                  ),
                  Text(
                    AppLocalizations.of(viewService.context)
                        .text('calendar.editor.time_picker.all_day_event'),
                    style: Style.labelNormal,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: Builder(
        builder: (context) => Container(
          decoration: BoxDecoration(
              color: colorPrimary, border: Border.all(color: colorPrimary)),
          child: ButtonWidget(
            colorButton: colorPrimary,
            textButton:
                AppLocalizations.of(viewService.context).text('common.save'),
            onPress: () {
              dispatch(
                AppointmentTimePickerActionCreator.confirm(),
              );
            },
          ),
        ),
      ),
    ),
  );
}

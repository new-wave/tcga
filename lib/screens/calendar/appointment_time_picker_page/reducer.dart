import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart' hide Action;

import '../../../utils/utils.dart';

import 'action.dart';
import 'state.dart';

Reducer<AppointmentTimePickerState> buildReducer() {
  return asReducer(
    <Object, Reducer<AppointmentTimePickerState>>{
      AppointmentTimePickerAction.handleStartDateSelected:
          _handleStartDateSelected,
      AppointmentTimePickerAction.handleStartTimeSelected:
          _handleStartTimeSelected,
      AppointmentTimePickerAction.handleEndDateSelected: _handleEndDateSelected,
      AppointmentTimePickerAction.handleEndTimeSelected: _handleEndTimeSelected,
      AppointmentTimePickerAction.handleTimeZoneSelected:
          _handleTimeZoneSelected,
      AppointmentTimePickerAction.handleIsAllDayChanged: _handleIsAllDayChanged,
    },
  );
}

AppointmentTimePickerState _handleStartDateSelected(
  AppointmentTimePickerState state,
  Action action,
) {
  final newState = state.clone();
  final value = action.payload as DateTime;

  if (value == null) return state;

  DateTime startTime = DateTime(
      value.year,
      value.month,
      value.day,
      newState.startTime.hour,
      newState.startTime.minute,
      newState.startTime.second);

  DateTime endTime = DateTime(
      value.year,
      value.month,
      value.day,
      newState.endTime.hour,
      newState.endTime.minute,
      newState.endTime.second);

  newState.startTime = startTime;
  newState.endTime = endTime;
  return newState;
}

AppointmentTimePickerState _handleStartTimeSelected(
  AppointmentTimePickerState state,
  Action action,
) {
  final newState = state.clone();
  final value = action.payload as TimeOfDay;

  if (value == null) return state;

  newState.startTime = DateTime(
    newState.startTime.year,
    newState.startTime.month,
    newState.startTime.day,
    value.hour,
    value.minute,
    0,
  );

  newState.endTime = DateTime(
    newState.endTime.year,
    newState.endTime.month,
    newState.endTime.day,
    value.hour + 1,
    value.minute,
    0,
  );

  newState.startTimeEditingController.text =
      newState.startTime.toShortTimeString();
  newState.endTimeEditingController.text = newState.endTime.toShortTimeString();

  return newState;
}

AppointmentTimePickerState _handleEndDateSelected(
  AppointmentTimePickerState state,
  Action action,
) {
  final newState = state.clone();
  final value = action.payload as DateTime;

  if (value == null) return state;

  DateTime dateTime = DateTime(
      value.year,
      value.month,
      value.day,
      newState.endTime.hour,
      newState.endTime.minute,
      newState.endTime.second);
  newState.endTime = dateTime;

  return newState;
}

AppointmentTimePickerState _handleEndTimeSelected(
  AppointmentTimePickerState state,
  Action action,
) {
  final newState = state.clone();
  final value = action.payload as TimeOfDay;

  if (value == null) return state;

  newState.endTime = DateTime(
    newState.endTime.year,
    newState.endTime.month,
    newState.endTime.day,
    value.hour,
    value.minute,
    0,
  );
  newState.endTimeEditingController.text = newState.endTime.toShortTimeString();

  return newState;
}

AppointmentTimePickerState _handleTimeZoneSelected(
  AppointmentTimePickerState state,
  Action action,
) {
  final newState = state.clone()..timeZone = action.payload;

  newState.timeZoneEditingController.text = '${newState.timeZone.display}';

  return newState;
}

AppointmentTimePickerState _handleIsAllDayChanged(
  AppointmentTimePickerState state,
  Action action,
) {
  return state.clone()..isAllDay = !state.isAllDay;
}

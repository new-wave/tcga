import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart' hide Appointment;
import 'package:tcga_flutter/database/models.dart';
import 'package:tcga_flutter/database/models/enums.dart';
import 'package:tcga_flutter/screens/calendar/timeslot_picker_page/timeslot_item_component/state.dart';
import '../../utils/utils.dart';
import 'appointment_editor/state.dart';
import 'state.dart';

extension EntityToStateMapper on Appointment {
  AppointmentItemState toState() => AppointmentItemState(
        id: this.id,
        recipientId: (this.participants != null && this.participants.isNotEmpty)
            ? this.participants[0].reference
            : null,
        subject: this.subject,
        timeZone: this.timeZone,
        appointmentType: AppointmentType.values[this.type],
        start: this.startTime,
        end: this.endTime,
        isAllDay: this.isAllDay,
        exceptionDates: this.recurrenceExceptionDates,
        color: AppointmentType.values[this.type].index == 0
            ? Color(0xffF2994A)
            : Color(0xff27AE60),
        recurrenceRule: _ensureRecurrenceRule(
          this.recurrenceRule,
          this.startTime,
        ),
        participant: (this.participants != null && this.participants.isNotEmpty)
            ? this.participants[0].reference
            : '',
        timeSlots: this
            .timeSlots
            ?.map(
              (e) => TimeslotItemState(
                startAt: e.startAt,
                durationInMinute: e.durationInMinute,
              ),
            )
            ?.toList(growable: false),
      );
}

String _ensureRecurrenceRule(String rule, DateTime startTime) {
  try {
    final _ = SfCalendar.parseRRule(rule, startTime);

    return rule;
  } catch (e) {
    return null;
  }
}

extension StateToEntityMapper on AppointmentEditorState {
  Appointment toEntity() => Appointment(
        id: this.id,
        type: this.appointmentType.index,
        subject: this.subjectEditingController.text,
        startTime: this.startTime.getCurrentTimeZone(this.timeZone.code),
        endTime: this.endTime.getCurrentTimeZone(this.timeZone.code),
        timeZone: this.timeZone.code,
        priority: this.appointmentPriority?.index,
        venue: this.venueType?.index,
        isAllDay: this.isAllDay,
        recurrenceExceptionDates: this.exceptionDates,
        recurrenceRule: this.recurrenceProperties != null
            ? SfCalendar.generateRRule(
                this.recurrenceProperties,
                this.startTime,
                this.endTime,
              )
            : null,
        reminderMinutesBeforeStart: this.remindBefore,
        participants:
            this.appointmentType == AppointmentType.MedicationAdministration
                ? [
                    Participant(
                      reference: recipient?.id ?? '',
                      type: ParticipantType.Attender.index,
                    )
                  ]
                : this
                        .attendees
                        ?.map(
                          (x) => Participant(
                            reference: x.id,
                            type: ParticipantType.Attender.index,
                          ),
                        )
                        ?.toList() ??
                    [],
        serviceTypeCode: this.serviceType?.code,
        appointmentStyle: this.appointmentStyleType?.index,
        reference: this.medication?.code,
        status: this.appointmentStatus?.index,
        timeSlots: this
            .timeslots
            ?.map((x) => TimeSlot(
                  startAt: x.startAt,
                  durationInMinute: x.durationInMinute,
                ))
            ?.toList(),
      );
}

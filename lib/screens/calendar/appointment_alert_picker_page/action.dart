import 'package:fish_redux/fish_redux.dart';

//TODO replace with your own action
enum AppointmentAlertPickerAction {
  handleChooseTime,
  confirm,
  clear,
  unFocus,
}

class AppointmentAlertPickerActionCreator {
  static Action handleChooseTime(String value) {
    return Action(AppointmentAlertPickerAction.handleChooseTime,
        payload: value);
  }

  static Action confirm() {
    return Action(
      AppointmentAlertPickerAction.confirm,
    );
  }

  static Action clear() {
    return Action(
      AppointmentAlertPickerAction.clear,
    );
  }

  static Action unFocus() {
    return Action(
      AppointmentAlertPickerAction.unFocus,
    );
  }
}

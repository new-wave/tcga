import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/config/style.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/widget/button_widget.dart';
import 'package:tcga_flutter/widget/entry_widget.dart';

import 'action.dart';
import 'state.dart';

Widget buildView(
  AppointmentAlertPickerState state,
  Dispatch dispatch,
  ViewService viewService,
) {
  return GestureDetector(
    onTap: () {
      dispatch(AppointmentAlertPickerActionCreator.unFocus());
    },
    child: Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.vertical(top: Radius.circular(15.0)),
        color: Colors.white,
      ),
      height: 380,
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.all(spacingMd),
            child: Row(
              children: [
                GestureDetector(
                  onTap: (){
                    dispatch(
                      AppointmentAlertPickerActionCreator.clear(),
                    );
                  },
                  child: Text(
                    AppLocalizations.of(viewService.context)
                        .text('common.clear'),
                    style: Style.labelClear,
                  ),
                ),
                Spacer(
                  flex: 5,
                ),
                Text(
                  AppLocalizations.of(viewService.context)
                      .text('calendar.editor.alert_me_picker.header'),
                  style: Style.labelTitleItem,
                ),
                Spacer(
                  flex: 4,
                ),
                InkWell(
                  onTap: () {
                    Navigator.of(viewService.context).pop();
                  },
                  child: SvgPicture.asset(
                    "assets/icons/ic_close.svg",
                  ),
                ),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: spacingMd, vertical: spacingSm),
            child: Row(
              children: [
                Text(
                  AppLocalizations.of(viewService.context)
                      .text('calendar.editor.alert_me_picker.remind_me_before'),
                  textAlign: TextAlign.start,
                  style: Style.labelNormal,
                ),
              ],
            ),
          ),
          Column(
            children: [
              GestureDetector(
                onTap: () {
                  dispatch(AppointmentAlertPickerActionCreator.handleChooseTime(
                      '1'));
                },
                child: Row(
                  children: [
                    Radio(
                      value: '1',
                      groupValue: state.value,
                      onChanged: (value) {
                        dispatch(AppointmentAlertPickerActionCreator
                            .handleChooseTime(value));
                      },
                    ),
                    Text(
                      AppLocalizations.of(viewService.context).text(
                          'calendar.editor.alert_me_picker.thirty_minutes'),
                      style: Style.labelNormal,
                    ),
                  ],
                ),
              ),
              GestureDetector(
                onTap: () {
                  dispatch(AppointmentAlertPickerActionCreator.handleChooseTime(
                      '2'));
                },
                child: Row(
                  children: [
                    Radio(
                      value: '2',
                      groupValue: state.value,
                      onChanged: (value) {
                        dispatch(AppointmentAlertPickerActionCreator
                            .handleChooseTime(value));
                      },
                    ),
                    Text(
                      AppLocalizations.of(viewService.context)
                          .text('calendar.editor.alert_me_picker.one_hour'),
                      style: Style.labelNormal,
                    ),
                  ],
                ),
              ),
              GestureDetector(
                onTap: () {
                  dispatch(AppointmentAlertPickerActionCreator.handleChooseTime(
                      '3'));
                },
                child: Row(
                  children: [
                    Radio(
                      value: '3',
                      groupValue: state.value,
                      onChanged: (value) {
                        dispatch(AppointmentAlertPickerActionCreator
                            .handleChooseTime(value));
                      },
                    ),
                    Text(
                      AppLocalizations.of(viewService.context)
                          .text('calendar.editor.alert_me_picker.one_day'),
                      style: Style.labelNormal,
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(right: spacingMd),
                child: GestureDetector(
                  onTap: () {
                    dispatch(
                        AppointmentAlertPickerActionCreator.handleChooseTime(
                            '4'));
                  },
                  child: Row(
                    children: [
                      Radio(
                        value: '4',
                        groupValue: state.value,
                        onChanged: (value) {
                          dispatch(AppointmentAlertPickerActionCreator
                              .handleChooseTime(value));
                        },
                      ),
                      Text(
                        AppLocalizations.of(viewService.context)
                            .text('calendar.editor.alert_me_picker.Other'),
                        style: Style.labelNormal,
                      ),
                      SizedBox(
                        width: 8,
                      ),
                      // if (!options.containsKey(selectedValue))
                      Flexible(
                        child: Focus(
                          onFocusChange: (hasFocus) {
                            if (hasFocus) {
                              dispatch(AppointmentAlertPickerActionCreator
                                  .handleChooseTime('4'));
                            }
                          },
                          child: EntryWidget(
                            controller: state.dayEditingController,
                            title: AppLocalizations.of(viewService.context)
                                .text('calendar.editor.alert_me_picker.day'),
                            keyboardType: TextInputType.number,
                            maxLength: 2,
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 8,
                      ),
                      // if (!options.containsKey(selectedValue))
                      Flexible(
                        child: Focus(
                          onFocusChange: (hasFocus) {
                            if (hasFocus) {
                              dispatch(AppointmentAlertPickerActionCreator
                                  .handleChooseTime('4'));
                            }
                          },
                          child: EntryWidget(
                            controller: state.hourEditingController,
                            title: AppLocalizations.of(viewService.context)
                                .text('calendar.editor.alert_me_picker.hour'),
                            keyboardType: TextInputType.number,
                            maxLength: 2,
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 8,
                      ),
                      // if (!options.containsKey(selectedValue))
                      Flexible(
                        child: Focus(
                          onFocusChange: (hasFocus) {
                            if (hasFocus) {
                              dispatch(AppointmentAlertPickerActionCreator
                                  .handleChooseTime('4'));
                            }
                          },
                          child: EntryWidget(
                            controller: state.minuteEditingController,
                            title: AppLocalizations.of(viewService.context)
                                .text('calendar.editor.alert_me_picker.minute'),
                            keyboardType: TextInputType.number,
                            maxLength: 2,
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 8,
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: spacingXs),
                        child: SvgPicture.asset(
                          'assets/icons/ic_alarm_clock.svg',
                          color: colorPrimary,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
          Spacer(),
          Container(
            child: ButtonWidget(
              onPress: () {
                dispatch(AppointmentAlertPickerActionCreator.confirm());
              },
              colorButton: colorPrimary,
              textButton: AppLocalizations.of(viewService.context)
                  .text('common.confirm'),
            ),
          ),
        ],
      ),
    ),
  );
}

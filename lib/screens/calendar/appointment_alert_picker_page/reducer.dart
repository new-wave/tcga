import 'package:fish_redux/fish_redux.dart';

import 'action.dart';
import 'state.dart';

Reducer<AppointmentAlertPickerState> buildReducer() {
  return asReducer(
    <Object, Reducer<AppointmentAlertPickerState>>{
      AppointmentAlertPickerAction.handleChooseTime: _handleChooseTime,
    },
  );
}

AppointmentAlertPickerState _handleChooseTime(
  AppointmentAlertPickerState state,
  Action action,
) {
  final AppointmentAlertPickerState newState = state.clone();
  final index = action.payload as String;
  newState.value = index;
  return newState;
}

import 'package:fish_redux/fish_redux.dart';

import 'effect.dart';
import 'reducer.dart';
import 'state.dart';
import 'view.dart';

class AppointmentAlertPickerPage extends Page<AppointmentAlertPickerState, Map<String, dynamic>> {
  AppointmentAlertPickerPage()
      : super(
            initState: initState,
            effect: buildEffect(),
            reducer: buildReducer(),
            view: buildView,
            dependencies: Dependencies<AppointmentAlertPickerState>(
                adapter: null,
                slots: <String, Dependent<AppointmentAlertPickerState>>{
                }),
            middleware: <Middleware<AppointmentAlertPickerState>>[
            ],);

}

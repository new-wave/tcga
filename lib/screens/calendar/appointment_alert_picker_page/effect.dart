import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart' hide Action, Page;
import '../../../utils/utils.dart';
import 'action.dart';
import 'state.dart';

Effect<AppointmentAlertPickerState> buildEffect() {
  return combineEffects(<Object, Effect<AppointmentAlertPickerState>>{
    AppointmentAlertPickerAction.confirm: _confirm,
    AppointmentAlertPickerAction.clear: _clear,
    AppointmentAlertPickerAction.unFocus: _unFocus,
  });
}

void _clear(
  Action action,
  Context<AppointmentAlertPickerState> ctx,
) async {
  ctx.goBack({
    'reminder-before': null,
  });
}

void _confirm(
  Action action,
  Context<AppointmentAlertPickerState> ctx,
) async {
  if (ctx.state.value == '4') {
    final day = ctx.state.dayEditingController.text == null ||
            ctx.state.dayEditingController.text.isEmpty
        ? 0
        : int.tryParse(ctx.state.dayEditingController.text);
    final hour = ctx.state.hourEditingController.text == null ||
            ctx.state.hourEditingController.text.isEmpty
        ? 0
        : int.tryParse(ctx.state.hourEditingController.text);
    final min = ctx.state.minuteEditingController.text == null ||
            ctx.state.minuteEditingController.text.isEmpty
        ? 0
        : int.tryParse(ctx.state.minuteEditingController.text);

    if (day != null && hour != null && min != null) {
      ctx.state.remindBefore = day * 1440 + hour * 60 + min;
    }
  } else {
    if (ctx.state.value == "1") ctx.state.remindBefore = 30;
    if (ctx.state.value == "2") ctx.state.remindBefore = 60;
    if (ctx.state.value == "3") ctx.state.remindBefore = 1440;
  }

  ctx.goBack({
    'reminder-before': ctx.state.remindBefore,
  });
}

void _unFocus(Action action, Context<AppointmentAlertPickerState> ctx) {
  FocusScopeNode currentFocus = FocusScope.of(ctx.context);
  if (!currentFocus.hasPrimaryFocus) {
    currentFocus.unfocus();
  }
}

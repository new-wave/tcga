import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart' hide Action, Pahe;

class AppointmentAlertPickerState
    implements Cloneable<AppointmentAlertPickerState> {
  String value;
  int remindBefore;
  TextEditingController dayEditingController;
  TextEditingController hourEditingController;
  TextEditingController minuteEditingController;

  @override
  AppointmentAlertPickerState clone() {
    return AppointmentAlertPickerState()
      ..value = value
      ..remindBefore = remindBefore
      ..dayEditingController = dayEditingController
      ..hourEditingController = hourEditingController
      ..minuteEditingController = minuteEditingController;
  }
}

AppointmentAlertPickerState initState(Map<String, dynamic> args) {
  final reminderMinutesBeforeStart = args['reminder-before'] as int;
  int day;
  int hour;
  int minute;

  String value = '1';
  if (reminderMinutesBeforeStart != null && reminderMinutesBeforeStart != 0) {
    if (reminderMinutesBeforeStart == 30) {
      value = '1';
    } else if (reminderMinutesBeforeStart == 60) {
      value = '2';
    } else if (reminderMinutesBeforeStart == 1440) {
      value = '3';
    } else {
      value = '4';
      hour = reminderMinutesBeforeStart ~/ 60;
      day = hour ~/ 24;
      hour = hour - day * 24;
      minute = reminderMinutesBeforeStart - ((day * 24 + hour) * 60);
    }
  }

  return AppointmentAlertPickerState()
    ..value = value ?? '1'
    ..remindBefore = reminderMinutesBeforeStart ?? 30
    ..dayEditingController =
        TextEditingController(text: day != null ? day.toString() : '')
    ..hourEditingController =
        TextEditingController(text: hour != null ? hour.toString() : '')
    ..minuteEditingController =
        TextEditingController(text: minute != null ? minute.toString() : '');
}

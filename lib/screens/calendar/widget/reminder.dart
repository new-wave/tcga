import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/config/style.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/widget/button_widget.dart';
import 'package:tcga_flutter/widget/entry_widget.dart';

Future<int> selectReminder(
    BuildContext context, int reminderMinutesBeforeStart) async {
  return showModalBottomSheet(
    context: context,
    backgroundColor: Colors.transparent,
    builder: (BuildContext build) {
      return __Reminder(
        reminderMinutesBeforeStart: reminderMinutesBeforeStart,
      );
    },
  );
}

class __Reminder extends StatefulWidget {
  int reminderMinutesBeforeStart;

  __Reminder({
    Key key,
    this.reminderMinutesBeforeStart,
  }) : super(key: key);

  @override
  __ReminderState createState() => __ReminderState();
}

class __ReminderState extends State<__Reminder> {
  static final options = {
    '30 minutes': 30,
    '1 hour': 60,
    '1 day': 1440,
  };

  TextEditingController dayEditingController;
  TextEditingController hourEditingController;
  TextEditingController minEditingController;

  String selectedValue;

  @override
  void initState() {
    if (widget.reminderMinutesBeforeStart != null) {
      selectedValue = widget.reminderMinutesBeforeStart == 30
          ? '30 minutes'
          : widget.reminderMinutesBeforeStart == 60
              ? '1 hour'
              : widget.reminderMinutesBeforeStart == 1440 ? '1 day' : 'Other';
      dayEditingController = TextEditingController(
        text: (widget.reminderMinutesBeforeStart ~/ 1440).toString(),
      );
      hourEditingController = TextEditingController(
        text: ((widget.reminderMinutesBeforeStart % 1440) ~/ 60).toString(),
      );
      minEditingController = TextEditingController(
        text: (widget.reminderMinutesBeforeStart % 60).toString(),
      );
    } else {
      selectedValue = null;
      dayEditingController = TextEditingController();
      hourEditingController = TextEditingController();
      minEditingController = TextEditingController();
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);
        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.unfocus();
        }
      },
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.vertical(top: Radius.circular(15.0)),
          color: Colors.white,
        ),
        height: 380,
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.all(spacingMd),
              child: Row(
                children: [
                  Spacer(
                    flex: 5,
                  ),
                  Text(
                    'Reminder',
                    style: Style.labelTitleItem,
                  ),
                  Spacer(
                    flex: 4,
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: SvgPicture.asset(
                      "assets/icons/ic_close.svg",
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: spacingMd),
              child: Row(
                children: [
                  Text(
                    'Remin me before',
                    textAlign: TextAlign.start,
                    style: Style.labelNormal,
                  ),
                ],
              ),
            ),
            Spacer(),
            Column(
              children: [
                Row(
                  children: [
                    Radio(
                      value: '30 minutes',
                      groupValue: selectedValue,
                      onChanged: (value) {
                        setState(() {
                          selectedValue = value;
                        });
                      },
                    ),
                    Text(
                      '30 minutes',
                      style: Style.labelNormal,
                    ),
                  ],
                ),
                Row(
                  children: [
                    Radio(
                      value: '1 hour',
                      groupValue: selectedValue,
                      onChanged: (value) {
                        setState(() {
                          selectedValue = value;
                        });
                      },
                    ),
                    Text(
                      '1 hour',
                      style: Style.labelNormal,
                    ),
                  ],
                ),
                Row(
                  children: [
                    Radio(
                      value: '1 day',
                      groupValue: selectedValue,
                      onChanged: (value) {
                        setState(() {
                          selectedValue = value;
                        });
                      },
                    ),
                    Text(
                      '1 day',
                      style: Style.labelNormal,
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(right: spacingMd),
                  child: Row(
                    children: [
                      Radio(
                        value: 'Other',
                        groupValue: selectedValue,
                        onChanged: (value) {
                          setState(() {
                            selectedValue = value;
                          });
                        },
                      ),
                      Text(
                        'Other',
                        style: Style.labelNormal,
                      ),
                      Spacer(),
                      SizedBox(
                        width: 16,
                      ),
                      if (!options.containsKey(selectedValue))
                        SizedBox(
                          width: 64,
                          child: EntryWidget(
                            controller: dayEditingController,
                            title: 'day',
                            keyboardType: TextInputType.number,
                            maxLength: 2,
                          ),
                        ),
                      SizedBox(
                        width: 16,
                      ),
                      if (!options.containsKey(selectedValue))
                        SizedBox(
                          width: 64,
                          child: EntryWidget(
                            controller: hourEditingController,
                            title: 'hour',
                            keyboardType: TextInputType.number,
                            maxLength: 2,
                          ),
                        ),
                      SizedBox(
                        width: 16,
                      ),
                      if (!options.containsKey(selectedValue))
                        SizedBox(
                          width: 64,
                          child: EntryWidget(
                            controller: minEditingController,
                            title: 'min',
                            keyboardType: TextInputType.number,
                            maxLength: 2,
                          ),
                        ),
                      SizedBox(
                        width: 16,
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: spacingXs),
                        child: SvgPicture.asset(
                          'assets/icons/ic_alarm_clock.svg',
                          color: colorPrimary,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Spacer(),
            Container(
              //color: Colors.transparent,
              margin: EdgeInsets.all(spacingMd),
              child: ButtonWidget(
                onPress: () {
                  int timeOfDay;
                  if (selectedValue != null) {
                    if (options.containsKey(selectedValue)) {
                      timeOfDay = options[selectedValue];
                    } else {
                      final day = dayEditingController.text == null ||
                              dayEditingController.text.isEmpty
                          ? 0
                          : int.tryParse(dayEditingController.text);
                      final hour = hourEditingController.text == null ||
                              hourEditingController.text.isEmpty
                          ? 0
                          : int.tryParse(hourEditingController.text);
                      final min = minEditingController.text == null ||
                              minEditingController.text.isEmpty
                          ? 0
                          : int.tryParse(minEditingController.text);

                      if (day != null && hour != null && min != null) {
                        timeOfDay = day * 1440 + hour * 60 + min;
                      }
                    }
                  }

                  Navigator.of(context).pop(timeOfDay);
                },
                colorButton: colorPrimary,
                textButton: AppLocalizations.of(context).text('common.confirm'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

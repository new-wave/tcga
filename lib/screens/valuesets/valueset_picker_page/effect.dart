import 'package:flutter/material.dart' hide Action;

import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/valuesets/valueset_picker_page/valueset_item_component/state.dart';

import '../../../app.dart';
import '../manager.dart';
import 'action.dart';
import 'state.dart';

int page = 0;

Effect<ValuesetsState> buildEffect() {
  return combineEffects(<Object, Effect<ValuesetsState>>{
    Lifecycle.initState: _init,
    ValueSetsPickerAction.loadMore: _loadMore,
    ValueSetsPickerAction.search: _search,
    ValueSetsPickerAction.confirm: _confirm,
    ValueSetsPickerAction.select: _select,
    ValueSetsPickerAction.selectTab: _selectTab,
  });
}

Future _init(Action action, Context<ValuesetsState> ctx) async {
  final valuesetManager = ValueSetManager();
  page = 0;
  var items = await valuesetManager.query(
    type: ctx.state.type,
  );

  ScrollController scrollController = ctx.state.scrollController;
  if(scrollController == null){
    scrollController = ScrollController();
  }
  // ignore: invalid_use_of_protected_member
  if (!scrollController.hasListeners) {
    scrollController.addListener(() async {
      if (scrollController.offset >=
          scrollController.position.maxScrollExtent &&
          !scrollController.position.outOfRange) {
        //reach the bottom
        ctx.dispatch(
          ValuesetsActionCreator.loadMore(),
        );
      }
      if (scrollController.offset <=
          scrollController.position.minScrollExtent &&
          !scrollController.position.outOfRange) {
        //reach the top
      }
    });
  }

  ctx.dispatch(ValuesetsActionCreator.initialize(items, scrollController));
}

Future _search(Action action, Context<ValuesetsState> ctx) async {
  final valuesetManager = ValueSetManager();

  var items = await valuesetManager.query(
    type: ctx.state.type,
    keyword: action.payload as String,
  );
  ctx.dispatch(ValuesetsActionCreator.initialize(items, ctx.state.scrollController));
}

Future _loadMore(Action action, Context<ValuesetsState> ctx) async {
  page++;
  List<ValuesetItemState> allItems = ctx.state.originItems ?? [];
  if (page * pageSize < allItems.length) {
    ctx.dispatch(
      ValuesetsActionCreator.setLoading(true),
    );
    await Future.delayed(const Duration(milliseconds: 300));
    ctx.dispatch(
      ValuesetsActionCreator.handleLoadMore(page),
    );
  }
}

void _selectTab(Action action, Context<ValuesetsState> ctx) {
  ctx.dispatch(
    ValuesetsActionCreator.handleTabSelected(action.payload),
  );
}

void _select(Action action, Context<ValuesetsState> ctx) {
  if (ctx.state.confirmRequired) {
    ctx.dispatch(ValuesetsActionCreator.handleItemSelected(action.payload));

    return;
  }

  Navigator.of(ctx.context).pop(
    action.payload,
  );
}

void _confirm(Action action, Context<ValuesetsState> ctx) {
  //if (ctx.state.selectedItems.length == 0) return;

  Navigator.of(ctx.context).pop(
    ctx.state.multiSelection
        ? ctx.state.selectedItems
        : ctx.state.selectedItems.first,
  );
}

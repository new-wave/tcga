import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/database/models/enums.dart';
import 'package:flutter/material.dart' hide Action;
import '../../../app.dart';
import 'valueset_item_component/state.dart';

import 'action.dart';
import 'state.dart';

Reducer<ValuesetsState> buildReducer() {
  return asReducer(
    <Object, Reducer<ValuesetsState>>{
      ValueSetsPickerAction.initialize: _initialize,
      ValueSetsPickerAction.handleLoadMore: _handleLoadMore,
      ValueSetsPickerAction.setLoading: _setLoading,
      ValueSetsPickerAction.handleItemSelected: _handleItemSelected,
      ValueSetsPickerAction.handleTabSelected: _handleTabSelected,
    },
  );
}

ValuesetsState _initialize(ValuesetsState state, Action action) {
  final newState = state.clone();
  final items = action.payload[0] as List<ValuesetItemState> ?? [];
  final scrollController =
      action.payload[1] as ScrollController ?? ScrollController();
  newState.scrollController = scrollController;

  for (var item in items) {
    item.isSelected = newState.selectedItems.any((x) => x.code == item.code);
  }

  if (newState.type == ValueSetType.serviceType.toString()) {
    var adls =
        items.where((element) => element.system.contains('adls')).toList();
    adls.sort((a, b) => a.display.compareTo(b.display));
    newState.adls = adls;

    var services = items
        .where((element) => element.system.contains('service-type'))
        .toList();
    services.sort((a, b) => a.display.compareTo(b.display));
    newState.serviceTypes = services;

    if (newState.currentPage == 1) {
      newState.items = adls;
    } else if (newState.currentPage == 2) {
      newState.items = services;
    } else {
      items.sort((a, b) => a.display.compareTo(b.display));
      newState.items = items;
    }
  } else {
//    items.sort((a, b) => a.display.compareTo(b.display));
    newState.items = items
        .getRange(0, items.length > pageSize ? pageSize : items.length)
        .toList(growable: false);
  }
  newState.originItems = items;
  return newState;
}

ValuesetsState _handleLoadMore(ValuesetsState state, Action action) {
  final newState = state.clone();
  List<ValuesetItemState> values = [];
  for (var value in state.items) {
    values.add(value);
  }
  final page = action.payload ?? 0;

  int nextRange = page * pageSize + pageSize;
  final items = state.originItems
      .getRange(
          page * pageSize,
          nextRange < state.originItems.length
              ? nextRange
              : state.originItems.length)
      .toList();

  for (var item in items) {
    item.isSelected = newState.selectedItems.any((x) => x.code == item.code);
  }
  values.addAll(items);
  newState.isLoading = false;
  newState.items = values;
  return newState;
}

ValuesetsState _setLoading(ValuesetsState state, Action action) {
  final newState = state.clone();
  newState.isLoading = action.payload;
  return newState;
}

ValuesetsState _handleTabSelected(ValuesetsState state, Action action) {
  final newState = state.clone();
  int index = action.payload ?? 0;
  newState.currentPage = index;
  if (index == 1) {
    newState.items = newState.adls;
  } else if (index == 2) {
    newState.items = newState.serviceTypes;
  } else {
    newState.items = newState.originItems;
  }
  return newState;
}

ValuesetsState _handleItemSelected(ValuesetsState state, Action action) {
  final newState = state.clone();
  final selectedItem = action.payload as ValuesetItemState;
  final selectedIndex = state.items.indexOf(selectedItem);

  if (!state.multiSelection) {
    if (!selectedItem.isSelected) {
      for (var item in newState.items) {
        if (item.isSelected) {
          item.isSelected = false;
        }
      }
    }

    newState.selectedItems = [
      selectedItem,
    ];
    selectedItem.isSelected = true;
  } else {
    selectedItem.isSelected = !selectedItem.isSelected;

    if (selectedItem.isSelected) {
      final index = newState.selectedItems.indexWhere(
        (item) => item.code == selectedItem.code,
      );

      if (index == -1) {
        newState.selectedItems.add(selectedItem);
      }
    } else {
      final index = newState.selectedItems.indexWhere(
        (item) => item.code == selectedItem.code,
      );

      if (index > -1) {
        newState.selectedItems.removeAt(index);
      }
    }
  }

  newState.items[selectedIndex] = selectedItem;
  return newState;
}

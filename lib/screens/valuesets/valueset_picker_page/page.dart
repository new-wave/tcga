import 'package:fish_redux/fish_redux.dart';

import 'valuesets_adapter/adapter.dart';

import 'effect.dart';
import 'reducer.dart';
import 'state.dart';
import 'view.dart';

class ValuesetPickerPage extends Page<ValuesetsState, Map<String, dynamic>> {
  ValuesetPickerPage()
      : super(
          initState: initState,
          effect: buildEffect(),
          reducer: buildReducer(),
          view: buildView,
          dependencies: Dependencies<ValuesetsState>(
              adapter: NoneConn<ValuesetsState>() + ValuesetsAdapter(),
              slots: <String, Dependent<ValuesetsState>>{}),
          middleware: <Middleware<ValuesetsState>>[],
        );
}

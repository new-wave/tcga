import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mdi/mdi.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/screens/valuesets/valuesets_component/action.dart';

import '../action.dart';
import 'state.dart';

Widget buildView(
    ValuesetItemState state, Dispatch dispatch, ViewService viewService) {
  return state.isSelectable
      ? InkWell(
          onTap: () {
            dispatch(
              ValuesetsActionCreator.select(state),
            );
          },
          child: _buildContent(state, dispatch),
        )
      : _buildContent(state, dispatch);
}

Column _buildContent(ValuesetItemState state, Dispatch dispatch) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      Row(
        children: [
          Expanded(
            child: Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 16,
                vertical: 6,
              ),
              child: Text(
                state.display,
                style: textStylePrimary,
                maxLines: 2,
              ),
            ),
          ),
          if (state.isSelected)
            Icon(
              Mdi.checkboxMarkedCircle,
              color: colorIndicatorPositive,
            ),
          // if (!state.isSelectable)
          //   GestureDetector(
          //     onTap: () => dispatch(
          //       ValuesetsComponentActionCreator.delete(
          //         state,
          //       ),
          //     ),
          //     child: Container(
          //       child: SvgPicture.asset(
          //         'assets/icons/ic_trash.svg',
          //         height: 24,
          //         width: 24,
          //       ),
          //     ),
          //   ),
          SizedBox(
            width: 16,
          ),
        ],
      )
    ],
  );
}

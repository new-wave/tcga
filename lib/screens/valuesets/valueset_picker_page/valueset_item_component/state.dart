import 'package:fish_redux/fish_redux.dart';

class ValuesetItemState implements Cloneable<ValuesetItemState> {
  String display;
  String code;
  String system;
  bool isSelected;
  bool isSelectable;

  ValuesetItemState({
    this.code,
    this.display,
    this.isSelected = false,
    this.isSelectable = true,
    this.system = '',
  });

  @override
  ValuesetItemState clone() {
    return ValuesetItemState()
      ..display = display
      ..code = code
      ..system = system
      ..isSelected = isSelected
      ..isSelectable = isSelectable;
  }
}

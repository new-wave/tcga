import 'package:fish_redux/fish_redux.dart';

import 'state.dart';
import 'view.dart';

class ValuesetItemComponent extends Component<ValuesetItemState> {
  ValuesetItemComponent()
      : super(
          view: buildView,
          dependencies: Dependencies<ValuesetItemState>(
              adapter: null, slots: <String, Dependent<ValuesetItemState>>{}),
        );
}

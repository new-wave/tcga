import 'dart:math' as math;

import 'package:flutter/material.dart';

import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/database/models/enums.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';

import 'package:tcga_flutter/widget/import.dart';

import 'action.dart';
import 'state.dart';

Widget buildView(
    ValuesetsState state, Dispatch dispatch, ViewService viewService) {
  final adapter = viewService.buildAdapter();

  return Container(
    decoration: BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.circular(
        15,
      ),
    ),
    child: Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: true,
        title: Text(AppLocalizations.of(viewService.context)
            .text('value_set.picker.select')),
        actions: [
          InkWell(
            onTap: () {
              Navigator.of(viewService.context).pop();
            },
            child: SizedBox(
              height: 48,
              width: 48,
              child: Padding(
                padding: const EdgeInsets.all(12),
                child: SvgPicture.asset('assets/icons/ic_close.svg'),
              ),
            ),
          )
        ],
      ),
      body: Stack(
        children: [
          Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                padding: const EdgeInsets.all(12),
                child: EntryWidget(
                  title: AppLocalizations.of(viewService.context)
                      .text('value_set.picker.placeholder_enter_to_search'),
                  keyboardType: TextInputType.text,
                  suffixIcon: SvgPicture.asset('assets/icons/ic_search.svg'),
                  onTextChanged: (text) {
                    dispatch(
                      ValuesetsActionCreator.search(text),
                    );
                  },
                ),
              ),
              state.type == ValueSetType.serviceType.toString()
                  ? Expanded(
                      child: Container(
                        height: MediaQuery.of(viewService.context).size.height -
                            200,
                        child: DefaultTabController(
                          length: 3,
                          initialIndex: 0,
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              TabBar(
                                onTap: (index) {
                                  dispatch(
                                    ValuesetsActionCreator.selectTab(index),
                                  );
                                },
                                indicatorColor: colorPrimary,
                                indicatorWeight: 3,
                                unselectedLabelColor: colorTextSecondary,
                                labelColor: colorPrimary,
                                labelStyle: textStylePrimaryTabBar,
                                unselectedLabelStyle: textStyleSecondaryTabBar,
                                tabs: [
                                  Tab(
                                    text: AppLocalizations.instance
                                        .text('calendar.service_type.all'),
                                  ),
                                  Tab(
                                    text: AppLocalizations.instance.text(
                                        'calendar.service_type.activities'),
                                  ),
                                  Tab(
                                    text: AppLocalizations.instance
                                        .text('calendar.service_type.services'),
                                  ),
                                ],
                              ),
                              Container(
                                height: 1,
                                color: Color(0xffE0E0E0),
                              ),
                              Expanded(
                                child: ListView.separated(
                                  padding: const EdgeInsets.symmetric(
                                    vertical: 16,
                                  ),
                                  separatorBuilder: (context, index) =>
                                      Divider(),
                                  itemBuilder: adapter.itemBuilder,
                                  itemCount: adapter.itemCount,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    )
                  : Expanded(
                      child: ListView.separated(
                        controller: state.scrollController,
                        shrinkWrap: true,
                        separatorBuilder: (context, index) => Divider(),
                        itemCount: adapter.itemCount,
                        itemBuilder: (BuildContext context, int index) {
                          return adapter.itemBuilder(
                            context,
                            index,
                          );
                        },
                      ),
                    ),
            ],
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: state.isLoading
                ? Container(
                    width: 24.0,
                    height: 24.0,
                    margin: EdgeInsets.all(8.0),
                    child: Center(
                      child: CircularProgressIndicator(),
                    ),
                  )
                : Container(),
          ),
        ],
      ),
      bottomNavigationBar: state.confirmRequired
          ? ButtonWidget(
              textButton: AppLocalizations.of(viewService.context)
                  .text('common.confirm'),
              colorButton: colorPrimary,
              onPress: () {
                dispatch(ValuesetsActionCreator.confirm());
              },
            )
          : SizedBox.shrink(),
    ),
  );
}

import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';

import 'valueset_item_component/state.dart';

class ValuesetsState extends MutableSource
    implements Cloneable<ValuesetsState> {
  List<ValuesetItemState> originItems;
  List<ValuesetItemState> adls;
  List<ValuesetItemState> serviceTypes;
  List<ValuesetItemState> items;
  List<ValuesetItemState> selectedItems;
  int currentPage;
  ScrollController scrollController;
  String type;
  bool confirmRequired;
  bool multiSelection;
  bool isLoading = false;

  @override
  ValuesetsState clone() {
    return ValuesetsState()
      ..items = items
      ..originItems = originItems
      ..adls = adls
      ..serviceTypes = serviceTypes
      ..selectedItems = selectedItems
      ..type = type
      ..currentPage = currentPage
      ..confirmRequired = confirmRequired
      ..multiSelection = multiSelection
      ..isLoading = isLoading
      ..scrollController = scrollController;
  }

  @override
  Object getItemData(int index) {
    return items[index];
  }

  @override
  String getItemType(int index) {
    return 'valueset-item';
  }

  @override
  int get itemCount => items?.length ?? 0;

  @override
  void setItemData(int index, Object data) {
    items[index] = data;
  }
}

ValuesetsState initState(Map<String, dynamic> args) {
  return ValuesetsState()
    ..multiSelection = args.containsKey(
      'multi-selection',
    )
    ..confirmRequired = args.containsKey(
          'multi-selection',
        ) ||
        args.containsKey(
          'confirm-required',
        )
    ..selectedItems = args.containsKey('selected-items')
        ? args['selected-items'] as List<ValuesetItemState> ?? []
        : []
    ..type = args.containsKey('type') ? args['type'] as String ?? '' : ''
    ..isLoading = false
    ..originItems = []
    ..currentPage = 0
    ..scrollController = ScrollController()
    ..adls = []
    ..serviceTypes = []
    ..currentPage = 0;
}

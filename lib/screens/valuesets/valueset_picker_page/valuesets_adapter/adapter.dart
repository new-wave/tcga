import 'package:fish_redux/fish_redux.dart';

import '../valueset_item_component/component.dart';
import '../state.dart';

class ValuesetsAdapter extends SourceFlowAdapter<ValuesetsState> {
  ValuesetsAdapter()
      : super(
          pool: <String, Component<Object>>{
            'valueset-item': ValuesetItemComponent(),
          },
        );
}

import 'package:fish_redux/fish_redux.dart';
import 'valueset_item_component/state.dart';
import 'package:flutter/material.dart' hide Action;

enum ValueSetsPickerAction {
  initialize,
  confirm,
  search,
  select,
  handleItemSelected,
  setLoading,
  loadMore,
  handleLoadMore,
  setScrollListener,
  selectTab,
  handleTabSelected,
}

class ValuesetsActionCreator {
  static Action initialize(
    List<ValuesetItemState> items,
    ScrollController scrollController,
  ) {
    return Action(
      ValueSetsPickerAction.initialize,
      payload: [items, scrollController],
    );
  }

  static Action loadMore() {
    return Action(
      ValueSetsPickerAction.loadMore,
    );
  }

  static Action handleLoadMore(int page) {
    return Action(
      ValueSetsPickerAction.handleLoadMore,
      payload: page,
    );
  }

  static Action setLoading(bool isLoading) {
    return Action(
        ValueSetsPickerAction.setLoading,
      payload: isLoading,
    );
  }

  static Action selectTab(int index) {
    return Action(
      ValueSetsPickerAction.selectTab,
      payload: index,
    );
  }

  static Action search(String text) {
    return Action(
      ValueSetsPickerAction.search,
      payload: text,
    );
  }

  static Action confirm() {
    return const Action(
      ValueSetsPickerAction.confirm,
    );
  }

  static Action select(ValuesetItemState item) {
    return Action(
      ValueSetsPickerAction.select,
      payload: item,
    );
  }

  static Action handleItemSelected(ValuesetItemState item) {
    return Action(
      ValueSetsPickerAction.handleItemSelected,
      payload: item,
    );
  }

  static Action handleTabSelected(int index) {
    return Action(
      ValueSetsPickerAction.handleTabSelected,
      payload: index,
    );
  }
}

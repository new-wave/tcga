import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:tcga_flutter/database/models/country.dart';
import 'package:tcga_flutter/database/models/service_type.dart';
import 'package:timezone/timezone.dart' as tz;
import 'package:tcga_flutter/utils/utils.dart';

import '../../database/models/enums.dart';
import '../../database/models.dart';
import '../../database/repository.dart';

import 'valueset_picker_page/valueset_item_component/state.dart';

class ValueSetManager {
  static final _singleton = ValueSetManager._internal();

  factory ValueSetManager() {
    return _singleton;
  }

  ValueSetManager._internal();

  final QueryOnlyRepository<MedicationCode> _medicationCodeRepository =
      QueryOnlyRepository();
  final QueryOnlyRepository<ValueSet> _valueSetRepository =
      QueryOnlyRepository();
  final QueryOnlyRepository<TimeZone> _timeZoneRepository =
      QueryOnlyRepository();
  final QueryOnlyRepository<ServiceType> _serviceTypeRepository =
      QueryOnlyRepository();
  final QueryOnlyRepository<Country> _countryRepository = QueryOnlyRepository();

  Future importDataFromJson() async {
    // await _importMedicationCodes();
    // await _importValueSets();
    // await _importTimeZones();
    // await _importServiceTypes();
    // await _importCountries();
  }

  Future _importMedicationCodes() async {
    try {
      final countriesJson = await rootBundle.loadString(
        'assets/json/medicationcodes.json',
      );
      final jsonItems = json.decode(countriesJson) as List;
      final items = jsonItems
          .map(
            (e) => MedicationCode.fromJson(e),
          )
          .toList(
            growable: false,
          );

      for (var item in items) {
        await _medicationCodeRepository.create(item);
      }
      print('medication done');
    } catch (e) {
      return;
    }
  }

  Future _importValueSets() async {
    try {
      final countriesJson = await rootBundle.loadString(
        'assets/json/valueset.json',
      );
      final jsonItems = json.decode(countriesJson) as List;
      final items = jsonItems
          .map(
            (e) => ValueSet.fromJson(e),
          )
          .toList(
            growable: false,
          );

      for (var item in items) {
        await _valueSetRepository.create(item);
      }
      print('valueset done');
    } catch (e) {
      return;
    }
  }

  Future _importTimeZones() async {
    try {
      final jsonString = await rootBundle.loadString(
        'assets/json/timezones.json',
      );
      final jsonItems = json.decode(jsonString) as List;
      final items = jsonItems
          .map(
            (e) => TimeZone.fromJson(e),
          )
          .toList(
            growable: false,
          );

      for (var item in items) {
        await _timeZoneRepository.create(item);
      }
      print('valueset done');
    } catch (e) {
      return;
    }
  }

  Future _importServiceTypes() async {
    try {
      final jsonString = await rootBundle.loadString(
        'assets/json/service_types.json',
      );
      final jsonItems = json.decode(jsonString) as List;
      final items = jsonItems
          .map(
            (e) => ServiceType.fromJson(e),
          )
          .toList(
            growable: false,
          );

      for (var item in items) {
        await _serviceTypeRepository.create(item);
      }
      print('importServiceTypes done');
    } catch (e) {
      return;
    }
  }

  Future _importCountries() async {
    try {
      final jsonString = await rootBundle.loadString(
        'assets/json/countries.json',
      );
      final jsonItems = json.decode(jsonString) as List;
      final items = jsonItems
          .map(
            (e) => Country.fromJson(e),
          )
          .toList(
            growable: false,
          );

      for (var item in items) {
        await _countryRepository.create(item);
      }
      print('importCountries done');
    } catch (e) {
      return;
    }
  }

  Future copyDbFiles({bool forceCopy = false}) async {
    final appDirectory = await getApplicationDocumentsDirectory();
    final boxFileNames = [
      'medicationcode.hive',
      'valueset.hive',
      'timezone.hive',
      'servicetype.hive',
      'country.hive',
    ];

    for (var boxName in boxFileNames) {
      final destPath = join(appDirectory.path, boxName);
      final shouldSkip = !forceCopy && await File(destPath).exists();

      if (shouldSkip) {
        continue;
      }

      final data = await rootBundle.load('assets/db/$boxName');
      final bytes = data.buffer.asUint8List(
        data.offsetInBytes,
        data.lengthInBytes,
      );
      await File(destPath).writeAsBytes(bytes);
    }
  }

  Future<List<ValuesetItemState>> query({
    @required String type,
    String keyword,
    int pageSize = 100,
  }) async {
    if (type == ValueSetType.medication) {
      return await _queryMedicationCodes(
        keyword: keyword,
        pageSize: pageSize,
      );
    }

    if (type == ValueSetType.timeZones) {
      return timeZoneList(
        keyword: keyword,
        pageSize: pageSize,
      );
    }

    if (type == ValueSetType.serviceType) {
      return await _queryServiceType(
        keyword: keyword,
        pageSize: pageSize,
      );
    }

    final keywordUpperCase = (keyword ?? '').trim().toUpperCase();
    final filter = keywordUpperCase.isEmpty
        ? (ValueSet item) => item.name == type
        : (ValueSet item) =>
            item.name == type &&
            item.display.toUpperCase().contains(keywordUpperCase);

    final items = await _valueSetRepository.query(
      filter: filter,
      pageSize: pageSize,
    );

    return items
        .map((item) => ValuesetItemState(
              code: item.code,
              display: item.display,
            ))
        .toList();
  }

  Future<List<ValuesetItemState>> _queryMedicationCodes({
    String keyword,
    int pageSize = 100,
  }) async {
    final keywordUpperCase = (keyword ?? '').trim().toUpperCase();
    final filter = keywordUpperCase.isEmpty
        ? null
        : (MedicationCode item) =>
            item.genericName.toUpperCase().contains(keywordUpperCase);

    final items = await _medicationCodeRepository.query(
      filter: filter,
      pageSize: pageSize,
    );

    return items
        .map((item) => ValuesetItemState(
              code: item.code,
              display: item.genericName,
            ))
        .toList();
  }

  Future<List<ValuesetItemState>> _queryServiceType({
    String keyword,
    int pageSize = 100,
  }) async {
    final keywordUpperCase = (keyword ?? '').trim().toUpperCase();
    final filter = keywordUpperCase.isEmpty
        ? null
        : (ServiceType item) =>
            item.display.toUpperCase().contains(keywordUpperCase);

    final items = await _serviceTypeRepository.query(
      filter: filter,
      pageSize: 200,
    );

    return items
        .map((item) => ValuesetItemState(
              code: item.code,
              display: item.display,
              system: item.system,
            ))
        .toList();
  }

  List<ValuesetItemState> timeZoneList({
    String keyword,
    int pageSize = 100,
  }) {
    final keywordUpperCase = (keyword ?? '').trim().toUpperCase();
    var timeZones = tz.timeZoneDatabase.locations.values.toList().where(
        (element) => (element.name
                .replaceAll('_', ' ')
                .toUpperCase()
                .contains(keywordUpperCase) ||
            element.name
                .replaceAll('_', '')
                .toUpperCase()
                .contains(keywordUpperCase)));
    List<ValuesetItemState> listTimeZones = [];
    timeZones.forEach((element) {
      int offsert = element.currentTimeZone.offset ~/ 60000;
      TimeOfDay timeOfDay = offsert.toTimeOfDay();
      listTimeZones.add(
        ValuesetItemState(
          code: '${element.name}',
          display:
              '${element.name.replaceAll('_', ' ')}\n${timeOfDay.getTimeZone()}',
        ),
      );
    });
    return listTimeZones;
  }

  Future<ValuesetItemState> getValuesetId(String type, String id) async {
    if (type == ValueSetType.medication) {
      final filter = (MedicationCode item) => item.code == id;
      var item = await _medicationCodeRepository.query(filter: filter);
      return ValuesetItemState(
        code: item.first.code,
        display: item.first.genericName,
      );
    } else if (type == ValueSetType.timeZones) {
      final locationName = id;
      final currentTimeZone = tz.getLocation(locationName);
      if (currentTimeZone != null) {
        int offsert = currentTimeZone.currentTimeZone.offset ~/ 60000;
        TimeOfDay timeOfDay = offsert.toTimeOfDay();
        return ValuesetItemState(
          code: '${currentTimeZone.name}',
          display:
              '${currentTimeZone.name.replaceAll('_', ' ')}\n${timeOfDay.getTimeZone()}',
        );
      }
    } else if (type == ValueSetType.serviceType) {
      final filter = (ValueSet item) => item.code == id;
      var item = await _valueSetRepository.query(filter: filter);
      return ValuesetItemState(
        code: item.first.code,
        display: item.first.display,
      );
    }

    return null;
  }
}

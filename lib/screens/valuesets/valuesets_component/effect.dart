import 'package:fish_redux/fish_redux.dart';

import 'package:tcga_flutter/route_name.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/utils/utils.dart';

import '../valueset_picker_page/valueset_item_component/state.dart';

import 'action.dart';
import 'state.dart';

Effect<ValuesetsState> buildEffect() {
  return combineEffects(<Object, Effect<ValuesetsState>>{
    ValueSetsAction.select: _select,
    ValueSetsAction.delete: _delete,
  });
}

Future _select(
  Action action,
  Context<ValuesetsState> ctx,
) async {
  var selectedItems = await ctx.navigateTo(
    RouteName.valuesetPicker,
    arguments: {
      'selected-items': ctx.state.items,
      'multi-selection': true,
      'type': ctx.state.type,
    },
  ) as List<ValuesetItemState>;

  if (selectedItems != null) {
    ctx.dispatch(
      ValuesetsComponentActionCreator.handleItemsSelected(
        ctx.state.type,
        selectedItems,
      ),
    );
  }
}

Future _delete(Action action, Context<ValuesetsState> ctx) async {
  final item = action.payload as ValuesetItemState;
  if (item == null) return;

  final confirmed = await ctx.confirm(
    title: AppLocalizations.instance.text('common.warning.delete.title'),
    message: AppLocalizations.instance.text('common.warning.delete.desc'),
    positiveActionTitle: AppLocalizations.instance.text('common.sure'),
    negativeActionTitle: AppLocalizations.instance.text('common.cancel'),
  );
  if (!confirmed) return;

  ctx.dispatch(
    ValuesetsComponentActionCreator.handleItemsDeleted(
      item,
    ),
  );
}

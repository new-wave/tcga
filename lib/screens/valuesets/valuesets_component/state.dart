import 'package:fish_redux/fish_redux.dart';
import '../valueset_picker_page/state.dart' as picker;

import '../valueset_picker_page/valueset_item_component/state.dart';

class ValuesetsState extends MutableSource
    implements Cloneable<ValuesetsState> {
  List<ValuesetItemState> items;
  List<ValuesetItemState> originItems;
  List<ValuesetItemState> adls;
  List<ValuesetItemState> serviceTypes;
  int currentPage;
  String title;
  String type;
  bool isLoading;

  ValuesetsState({
    this.title,
    this.type,
    this.items,
    this.isLoading,
    this.currentPage,
    this.originItems,
  });

  @override
  ValuesetsState clone() {
    return ValuesetsState()
      ..items = items
      ..originItems = originItems
      ..adls = adls
      ..serviceTypes = serviceTypes
      ..title = title
      ..type = type
      ..isLoading = isLoading
      ..currentPage = currentPage
      ..type = type;
  }

  @override
  Object getItemData(int index) {
    return items[index];
  }

  @override
  String getItemType(int index) {
    return 'valueset-item';
  }

  @override
  // TODO: implement itemCount
  int get itemCount => items?.length ?? 0;

  @override
  void setItemData(int index, Object data) {
    items[index] = data;
  }
}

ValuesetsState initState(Map<String, dynamic> args) {
  return ValuesetsState()
    ..originItems = args.containsKey('items')
        ? args['items'] as List<ValuesetItemState> ?? []
        : []
    ..items = args.containsKey('items')
        ? args['items'] as List<ValuesetItemState> ?? []
        : []
    ..originItems = []
    ..adls = []
    ..serviceTypes = []
    ..type = args.containsKey('type')
        ? args['type'] as String ?? 'Invalid Type'
        : 'Invalid Type'
    ..title = args.containsKey('title')
        ? args['title'] as String ?? 'Invalid Title'
        : 'Invalid Title'
    ..isLoading = false
    ..currentPage = 0;
}

class ValuesetsStateConnector
    extends ConnOp<ValuesetsState, picker.ValuesetsState> {
  @override
  picker.ValuesetsState get(
    ValuesetsState state,
  ) =>
      picker.ValuesetsState()
        ..items = state.items
        ..isLoading = state.isLoading
        ..originItems = state.originItems
        ..adls = state.adls
        ..serviceTypes = state.serviceTypes
        ..currentPage = state.currentPage;

  @override
  void set(
    ValuesetsState state,
    picker.ValuesetsState subState,
  ) {
    state.items = subState.items.map(
      (x) => x
        ..isSelectable = false
        ..isSelected = false,
    );
    state.originItems = subState.originItems.map(
      (x) => x
        ..isSelectable = false
        ..isSelected = false,
    );
    state.isLoading = subState.isLoading;
    state.adls = subState.adls.map(
      (x) => x
        ..isSelectable = false
        ..isSelected = false,
    );
    state.serviceTypes = subState.serviceTypes.map(
      (x) => x
        ..isSelectable = false
        ..isSelected = false,
    );
    state.currentPage = subState.currentPage;
  }
}

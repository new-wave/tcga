import 'package:fish_redux/fish_redux.dart';

import '../valueset_picker_page/valuesets_adapter/adapter.dart';

import 'effect.dart';
import 'reducer.dart';
import 'state.dart' as state;
import 'view.dart';

class ValuesetsComponent extends Component<state.ValuesetsState> {
  ValuesetsComponent()
      : super(
          effect: buildEffect(),
          reducer: buildReducer(),
          view: buildView,
          filter: (state, action) {
            return action.payload is List && state.type == action.payload[0];
          },
          dependencies: Dependencies<state.ValuesetsState>(
              adapter: state.ValuesetsStateConnector() + ValuesetsAdapter(),
              slots: <String, Dependent<state.ValuesetsState>>{}),
        );
}

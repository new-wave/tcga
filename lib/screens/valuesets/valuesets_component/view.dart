import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tcga_flutter/config/palette.dart';

import 'action.dart';
import 'state.dart';

Widget buildView(
  ValuesetsState state,
  Dispatch dispatch,
  ViewService viewService,
) {
  final adapter = viewService.buildAdapter();
  return Container(
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(
            right: 16,
            left: 16,
            top: 8,
          ),
          child: Row(
            children: [
              Expanded(
                child: Text(
                  state.title,
                  style: textStyleTitleThird,
                ),
              ),
              InkWell(
                onTap: () {
                  dispatch(
                    ValuesetsComponentActionCreator.select(state.type),
                  );
                },
                child: Padding(
                  padding: const EdgeInsets.only(
                    left: 12,
                  ),
                  child: SvgPicture.asset(
                    'assets/icons/ic_button_add.svg',
                  ),
                ),
              )
            ],
          ),
        ),
        SizedBox(
          height: 8,
        ),
        ...List.generate(
          adapter.itemCount,
          (index) => Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8.0),
            child: Container(
              padding: const EdgeInsets.symmetric(vertical: 8),
              decoration: BoxDecoration(
                  color: colorBackground,
                  borderRadius: BorderRadius.circular(8)),
              child: adapter.itemBuilder(viewService.context, index),
            ),
          ),
        ),
      ],
    ),
  );
}

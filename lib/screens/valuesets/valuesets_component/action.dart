import 'package:fish_redux/fish_redux.dart';

import '../valueset_picker_page/valueset_item_component/state.dart';

enum ValueSetsAction {
  select,
  handleItemsSelected,
  delete,
  handleItemsDeleted,
}

class ValuesetsComponentActionCreator {
  static Action handleItemsSelected(
    String type,
    List<ValuesetItemState> items,
  ) {
    return Action(
      ValueSetsAction.handleItemsSelected,
      payload: [
        type,
        items,
      ],
    );
  }

  static Action select(String type) {
    return Action(
      ValueSetsAction.select,
      payload: [type],
    );
  }

  static Action handleItemsDeleted(
    ValuesetItemState item,
  ) {
    return Action(
      ValueSetsAction.handleItemsDeleted,
      payload: item,
    );
  }

  static Action delete(ValuesetItemState item) {
    return Action(
      ValueSetsAction.delete,
      payload: item,
    );
  }
}

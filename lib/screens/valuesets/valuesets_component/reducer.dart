import 'package:fish_redux/fish_redux.dart';

import '../valueset_picker_page/valueset_item_component/state.dart';

import 'action.dart';
import 'state.dart';

Reducer<ValuesetsState> buildReducer() {
  return asReducer(
    <Object, Reducer<ValuesetsState>>{
      ValueSetsAction.handleItemsSelected: _handleItemsSelected,
      ValueSetsAction.handleItemsDeleted: _handleItemsDeleted,
    },
  );
}

ValuesetsState _handleItemsSelected(
  ValuesetsState state,
  Action action,
) {
  final ValuesetsState newState = state.clone();

  newState.items = (action.payload[1] as List<ValuesetItemState> ?? [])
      .map(
        (x) => x
          ..isSelected = false
          ..isSelectable = false,
      )
      .toList();

  return newState;
}

ValuesetsState _handleItemsDeleted(ValuesetsState state, Action action) {
  final ValuesetsState newState = state.clone();
  ValuesetItemState item = action.payload;
  if (item != null) {
    newState.items.remove(item);
  }
  return newState;
}

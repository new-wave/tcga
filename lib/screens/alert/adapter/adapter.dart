import 'package:fish_redux/fish_redux.dart';
import '../alert_component/component.dart';

import '../state.dart';

class AlertAdapter extends SourceFlowAdapter<AlertState> {
  AlertAdapter()
      : super(
    pool: <String, Component<Object>>{'alert-item': AlertItemComponent()},
  );
}
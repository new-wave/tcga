import 'package:fish_redux/fish_redux.dart';

import 'action.dart';
import 'state.dart';

Reducer<AlertItemState> buildReducer() {
  return asReducer(
    <Object, Reducer<AlertItemState>>{
      AlertItemAction.done: _onDone,
    },
  );
}

AlertItemState _onDone(AlertItemState state, Action action) {
  final AlertItemState newState = state.clone();
  final bool isChoose = action.payload;

  if (isChoose == true) {
    return newState..isChoose = isChoose;
  }
  if (isChoose == false) {
    return newState..isChoose = !isChoose;
  }
  return newState;
}

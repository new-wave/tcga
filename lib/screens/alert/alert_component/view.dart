import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tcga_flutter/config/palette.dart';

import 'action.dart';
import 'state.dart';

Widget buildView(
    AlertItemState state, Dispatch dispatch, ViewService viewService) {
  return Container(
    height: 61,
    child: Column(
      children: [
        GestureDetector(
          onTap: () {
            dispatch(AlertItemActionCreator.onDone(state.isChoose));
          },
          child: Container(
            height: 60,
            padding: EdgeInsets.symmetric(
              horizontal: 16,
              vertical: 14,
            ),
            color: state.isChoose == false ? colorBackground : Colors.white,
            child: Row(
              children: <Widget>[
                SvgPicture.asset(
                  state.icon,
                  height: 26,
                  width: 26,
                ),
                SizedBox(
                  width: 24,
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Flexible(
                        child: Text(
                          state.title,
                          style: TextStyle(
                            fontSize: 14,
                            color: Colors.black,
                            fontWeight: FontWeight.w400,
                          ),
                          overflow: TextOverflow.ellipsis,
                          maxLines: 1,
                        ),
                      ),
                      Flexible(
                        child: Text(
                          state.title,
                          style: TextStyle(
                            fontSize: 12,
                            color: Colors.black,
                            fontWeight: FontWeight.w400,
                          ),
                          overflow: TextOverflow.ellipsis,
                          maxLines: 1,
                        ),
                      ),
                    ],
                  ),
                ),
                Text(
                  state.time,
                  style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.w400,
                  ),
                )
              ],
            ),
          ),
        ),
        Container(
          height: 0.5,
          color: colorDisabled,
        ),
      ],
    ),
  );
}

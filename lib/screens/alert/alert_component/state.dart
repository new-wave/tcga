import 'package:fish_redux/fish_redux.dart';
import 'package:uuid/uuid.dart';

class AlertItemState implements Cloneable<AlertItemState> {
  String id;
  String icon;
  String title;
  String content;
  String time;
  bool isChoose;

  AlertItemState({
    this.content,
    this.title,
    this.isChoose = false,
    this.icon,
    this.time,
    this.id,
  }) {
    id ??= Uuid().v4();
  }

  @override
  AlertItemState clone() {
    return AlertItemState()
      ..id = id
      ..icon = icon
      ..time = time
      ..title = title
      ..isChoose = isChoose
      ..content = content;
  }
}

AlertItemState initState(Map<String, dynamic> args) {
  return AlertItemState();
}

import 'package:fish_redux/fish_redux.dart';

import 'reducer.dart';
import 'state.dart';
import 'view.dart';

class AlertItemComponent extends Component<AlertItemState> {
  AlertItemComponent()
      : super(
          reducer: buildReducer(),
          view: buildView,
        );
}

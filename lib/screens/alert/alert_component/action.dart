import 'package:fish_redux/fish_redux.dart';

//TODO replace with your own action
enum AlertItemAction { done }

class AlertItemActionCreator {
  static Action onDone(bool id) {
    return Action(AlertItemAction.done, payload: id);
  }
}

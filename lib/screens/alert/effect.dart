import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/alert/alert_component/state.dart';
import 'action.dart';
import 'state.dart';

Effect<AlertState> buildEffect() {
  return combineEffects(<Object, Effect<AlertState>>{
    Lifecycle.initState: _onInit,
    // Lifecycle.dispose: _onDispose,
    //AlertAction.action: _onAction,
  });
}

//void _onAction(Action action, Context<AlertState> ctx) {}

void _onInit(Action action, Context<AlertState> ctx) {
  final List<AlertItemState> alerts = [
    AlertItemState(
      id: '1',
      icon: 'assets/icons/ic_alert_information.svg',
      title: 'Title of the information alert',
      content: 'Detail information explain',
      time: '12nd Sep',
      isChoose: false,
    ),
    AlertItemState(
      id: '2',
      icon: 'assets/icons/ic_alert_information.svg',
      title: 'Title of the information alert',
      content: 'Detail information explain',
      time: '12nd Sep',
      isChoose: false,
    ),
    AlertItemState(
      id: '3',
      icon: 'assets/icons/ic_alert_warning.svg',
      title: 'Title of warning alert',
      content: 'Detail information explain',
      time: '12nd Sep',
      isChoose: false,
    ),
    AlertItemState(
      id: '4',
      icon: 'assets/icons/ic_alert_warning.svg',
      title: 'Title of warning alert',
      content: 'Detail information explain',
      time: '12nd Sep',
      isChoose: true,
    ),
    AlertItemState(
      id: '5',
      icon: 'assets/icons/ic_alert_emergency.svg',
      title: 'Title of the emergency alert',
      content: 'Detail information explain',
      time: '11st Sep',
      isChoose: false,
    ),
    AlertItemState(
      id: '6',
      icon: 'assets/icons/ic_alert_emergency.svg',
      title: 'Title of the emergency alert',
      content: 'Detail information explain',
      time: '11st Sep',
      isChoose: true,
    ),
    AlertItemState(
      id: '7',
      icon: 'assets/icons/ic_alert_emergency.svg',
      title: 'Title of the emergency alert',
      content: 'Detail information explain',
      time: '11st Sep',
      isChoose: true,
    ),
  ];
  ctx.dispatch(AlertActionCreator.initAction(alerts));
}

// void _onDispose(Action action, Context<CareRecipientState> ctx) {}

import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/alert/alert_component/state.dart';
import 'action.dart';
import 'state.dart';

Reducer<AlertState> buildReducer() {
  return asReducer(
    <Object, Reducer<AlertState>>{
      AlertAction.init: _oninitAction,
    },
  );
}

AlertState _oninitAction(AlertState state, Action action) {
  final AlertState newState = state.clone();
  final List<AlertItemState> alerts = action.payload ?? <AlertItemState>[];
  newState.alerts = alerts;

  return newState;
}

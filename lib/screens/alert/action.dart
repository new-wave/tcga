import 'package:fish_redux/fish_redux.dart';

import 'alert_component/state.dart';

enum AlertAction {
  init,
}

class AlertActionCreator {
  static Action initAction(List<AlertItemState> items) {
    return Action(AlertAction.init, payload: items);
  }
}

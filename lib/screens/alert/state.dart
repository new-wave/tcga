import 'package:fish_redux/fish_redux.dart';

import 'alert_component/state.dart';

class AlertState extends MutableSource implements Cloneable<AlertState> {
  List<AlertItemState> alerts;

  @override
  AlertState clone() {
    return AlertState()..alerts = alerts;
  }

  @override
  Object getItemData(int index) => alerts[index];

  @override
  String getItemType(int index) {
    return 'alert-item';
  }

  @override
  // TODO: implement itemCount
  int get itemCount => alerts.length ?? 0;

  @override
  void setItemData(int index, Object data) => alerts[index] = data;
}

AlertState initState(Map<String, dynamic> args) {
  final AlertState state = AlertState();
  return state;
}

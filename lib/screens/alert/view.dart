import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:tcga_flutter/screens/drawer/drawer_page.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'state.dart';

Widget buildView(AlertState state, Dispatch dispatch, ViewService viewService) {
  final adapter = viewService.buildAdapter();
  return Builder(builder: (context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.black),
        title: Text(AppLocalizations.of(viewService.context).text("home.bottombar.alert")),
        elevation: 1.0,
        centerTitle: false,
        actions: [
          GestureDetector(
            onTap: () async {},
            child: Visibility(
              visible: true,
              child: SvgPicture.asset(
                "assets/icons/ic_search.svg",
                height: 24,
                width: 24,
              ),
            ),
          ),
          SizedBox(width: 16.0),
          GestureDetector(
            onTap: () {},
            child: Visibility(
              visible: true,
              child: SvgPicture.asset(
                "assets/icons/ic_button_add.svg",
                height: 24,
                width: 24,
              ),
            ),
          ),
          SizedBox(width: 16.0),
        ],
      ),
      drawer: DrawerPage(),
      body: ListView.builder(
        itemBuilder: adapter.itemBuilder,
        itemCount: adapter.itemCount,
      ),
    );
  });
}

import 'package:fish_redux/fish_redux.dart';
import 'adapter/adapter.dart';
import 'effect.dart';
import 'reducer.dart';
import 'state.dart';
import 'view.dart';

class AlertPage extends Page<AlertState, Map<String, dynamic>> {
  AlertPage()
      : super(
          initState: initState,
          effect: buildEffect(),
          reducer: buildReducer(),
          view: buildView,
          dependencies: Dependencies<AlertState>(
            adapter: NoneConn<AlertState>() + AlertAdapter(),
            slots: <String, Dependent<AlertState>>{},
          ),
        );
}

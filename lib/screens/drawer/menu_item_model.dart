import 'package:tcga_flutter/route_name.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';

class MenuItemModel {
  String icon;
  String text;
  String routeName;
  bool popToRoot;

  MenuItemModel({
    this.icon,
    this.text,
    this.routeName,
    this.popToRoot = false,
  });
}

List<MenuItemModel> menuItems = [
  MenuItemModel(
    icon: "assets/icons/teams.svg",
    text: AppLocalizations.instance.text('home.drawer.contacts'),
    routeName: RouteName.contacts,
  ),
  MenuItemModel(
    icon: "assets/icons/account.svg",
    text: AppLocalizations.instance.text('home.drawer.my_account'),
  ),
  MenuItemModel(
    icon: "assets/icons/mileage.svg",
    text: AppLocalizations.instance.text('home.drawer.mileage'),
    routeName: RouteName.mileage,
  ),
  MenuItemModel(
    icon: "assets/icons/forms.svg",
    text: AppLocalizations.instance.text('home.drawer.consent_forms'),
    routeName: RouteName.consentForms,
  ),
  MenuItemModel(
    icon: "assets/icons/teams.svg",
    text: AppLocalizations.instance.text('home.drawer.care_team'),
    routeName: RouteName.teams,
  ),
  MenuItemModel(
    icon: "assets/icons/expense.svg",
    text: AppLocalizations.instance.text('home.drawer.expense_tracking'),
    routeName: RouteName.expenses,
  ),
  MenuItemModel(
    icon: "assets/icons/about_us.svg",
    text: AppLocalizations.instance.text('home.drawer.about_us'),
  ),
  MenuItemModel(
    icon: "assets/icons/faq.svg",
    text: AppLocalizations.instance.text('home.drawer.faq'),
  ),
  MenuItemModel(
    icon: "assets/icons/setting.svg",
    text: AppLocalizations.instance.text('home.drawer.settings'),
  ),
  MenuItemModel(
    icon: "assets/icons/logout.svg",
    text: AppLocalizations.instance.text('home.drawer.log_out'),
    routeName: RouteName.signIn,
    popToRoot: true,
  ),
];

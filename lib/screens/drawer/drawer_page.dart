import 'package:flutter/material.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:package_info/package_info.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'menu_item_model.dart';
import 'menu_item_widget.dart';

class DrawerPage extends StatelessWidget {
  const DrawerPage({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Container(
        color: Colors.white,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              height: 100,
              margin: EdgeInsets.only(left: 50),
              child: Column(
                children: [
                  Spacer(),
                  Row(
                    children: <Widget>[
                      CircleAvatar(
                        radius: 25,
                        child: Image.asset("assets/images/img_avatar.png"),
                      ),
                      SizedBox(
                        width: 23,
                      ),
                      Text("Maria", style: textStylePrimary)
                    ],
                  ),
                ],
              ),
            ),
            SizedBox(height: 26),
            Expanded(
              child: Container(
                alignment: Alignment.centerLeft,
                margin: EdgeInsets.only(left: 50),
                child: ListView.builder(
                  padding: EdgeInsets.zero,
                  itemCount: menuItems.length,
                  itemBuilder: (BuildContext context, int index) {
                    final menuItem = menuItems[index];
                    return MenuItemWidget(
                      menuItem: menuItem,
                      menuItemTapped: () {
                        Navigator.pop(context);
                        SharedPreferences.getInstance().then(
                              (_p) {
                            _p.setBool('firstStart', false);
                            _p.setString('token', null);
                            _p.setString('refresh_token', null);
                          },
                        );
                        if (menuItem.routeName != null) {
                          var nav = Navigator.of(context);
                          if (menuItem.popToRoot) {
                            nav.pushNamedAndRemoveUntil(
                              menuItem.routeName,
                              (Route<dynamic> route) => false,
                            );
                          } else {
                            nav.pushNamed(menuItem.routeName);
                          }
                        }
                      },
                    );
                  },
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              margin: const EdgeInsets.only(left: 16, bottom: 5),
              child: FutureBuilder(
                future: PackageInfo.fromPlatform(),
                builder: (BuildContext context,
                    AsyncSnapshot<PackageInfo> snapshot) {
                  if (snapshot.hasData) {
                    return Row(
                      children: [
                        Text(
                          snapshot.data.appName,
                          style: textStyleSmall,
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Text(
                          "v" + snapshot.data.version,
                          style: textStyleSmall,
                        ),
                        Text(
                          " (" + snapshot.data.buildNumber + ")",
                          style: textStyleSmall,
                        )
                      ],
                    );
                  } else {
                    return Container();
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:tcga_flutter/config/palette.dart';

import 'menu_item_model.dart';

class MenuItemWidget extends StatelessWidget {
  const MenuItemWidget({
    Key key,
    this.menuItem,
    this.menuItemTapped,
  }) : super(key: key);

  final MenuItemModel menuItem;
  final GestureTapCallback menuItemTapped;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: menuItemTapped,
      child: Container(
        padding: const EdgeInsets.symmetric(
          vertical: 16,
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            SizedBox(
              child: SvgPicture.asset(
                menuItem.icon,
                color: Colors.black,
                width: 16,
                height: 16,
              ),
              width: 24,
            ),
            SizedBox(width: 27),
            Expanded(
              child: Text(
                menuItem.text,
                style: textStylePrimary,
                overflow: TextOverflow.ellipsis,
                maxLines: 1,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

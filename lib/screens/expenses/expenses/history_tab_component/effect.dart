import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/expenses/manager.dart';

import '../action.dart';
import 'action.dart';
import 'state.dart';

final _manager = ExpenseManager();

Effect<HistoryState> buildEffect() {
  return combineEffects(<Object, Effect<HistoryState>>{
    Lifecycle.initState: _initialize,
    HistoryAction.filter: _filter,
  });
}

Future _initialize(Action action, Context<HistoryState> ctx) async {
  final items = await _manager.query();

  ctx.dispatch(
    HistoryActionCreator.initialize(items),
  );
}

void _filter(Action action, Context<HistoryState> ctx) async {
  final index = action.payload as int;
  ctx.dispatch(
    HistoryActionCreator.handleFilter(index),
  );
  ctx.dispatch(ExpensesActionCreator.filterinitialize());
}

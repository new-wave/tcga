import 'package:fish_redux/fish_redux.dart';

import '../expense_item_component/component.dart';
import '../state.dart';

import 'effect.dart';
import 'reducer.dart';

class ExpenseAdapter extends SourceFlowAdapter<HistoryState> {
  ExpenseAdapter()
      : super(
          pool: <String, Component<Object>>{
            'expense-item': ExpenseItemComponent(),
          },
          // effect: buildEffect(),
          // reducer: buildReducer(),
        );
}

import 'package:fish_redux/fish_redux.dart';

import 'expense_item_component/state.dart';

enum HistoryAction {
  initialize,
  filter,
  handleFilter,
}

class HistoryActionCreator {
  static Action initialize(List<ExpenseItemState> expenseItems) {
    return Action(HistoryAction.initialize, payload: expenseItems);
  }

  static Action filter(int index) {
    return Action(HistoryAction.filter, payload: index);
  }

  static Action handleFilter(int selectFilters) {
    return Action(HistoryAction.handleFilter, payload: selectFilters);
  }
}

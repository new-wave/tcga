import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/database/models/enums.dart';

import 'expense_item_component/state.dart';

class HistoryState extends MutableSource implements Cloneable<HistoryState> {
  List<ExpenseItemState> expenseItems;
  List<bool> selectItems;

  static final optionsType = const {
    ExpenseTrackingOption.Pending: 'enums.expense_tracking_option.pending',
    ExpenseTrackingOption.Invoiced: 'enums.expense_tracking_option.Invoiced',
    ExpenseTrackingOption.Paid: 'enums.expense_tracking_option.paid',
  };

  @override
  HistoryState clone() {
    return HistoryState()
      ..expenseItems = expenseItems
      ..selectItems = selectItems;
  }

  @override
  Object getItemData(int index) {
    return expenseItems[index];
  }

  @override
  String getItemType(int index) {
    return 'expense-item';
  }

  @override
  int get itemCount => expenseItems?.length ?? 0;

  @override
  void setItemData(int index, Object data) {
    expenseItems[index] = data;
  }
}

HistoryState initState(Map<String, dynamic> args) {
  return HistoryState()
    ..selectItems = [false, false, false, false, false, false];
}

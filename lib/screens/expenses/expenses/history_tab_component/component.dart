import 'package:fish_redux/fish_redux.dart';

import 'effect.dart';
import 'expense_adapter/adapter.dart';
import 'reducer.dart';
import 'state.dart';
import 'view.dart';

class HistoryTabComponent extends Component<HistoryState> {
  HistoryTabComponent()
      : super(
          effect: buildEffect(),
          reducer: buildReducer(),
          view: buildView,
          dependencies: Dependencies<HistoryState>(
            adapter: NoneConn<HistoryState>() + ExpenseAdapter(),
            slots: <String, Dependent<HistoryState>>{},
          ),
        );
}

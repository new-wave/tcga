import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/expenses/expenses/history_tab_component/expense_item_component/state.dart';
import '../state.dart';
import '../history_tab_component/state.dart';
import 'action.dart';
import 'state.dart';

var items = List<ExpenseItemState>();
Reducer<HistoryState> buildReducer() {
  return asReducer(
    <Object, Reducer<HistoryState>>{
      HistoryAction.initialize: _initialize,
      HistoryAction.handleFilter: _handleFilter,
    },
  );
}

HistoryState _initialize(HistoryState state, Action action) {
  final expenseItems = action.payload ?? <ExpenseState>[];
  final HistoryState newState = state.clone();
  items = expenseItems;
  newState.selectItems = [false, false, false];
  newState.expenseItems = expenseItems;

  return newState;
}

HistoryState _handleFilter(HistoryState state, Action action) {
  final selectFilter = action.payload as int;
  final HistoryState newState = state.clone();
  newState.selectItems[selectFilter] = !state.selectItems[selectFilter];

  return newState;
}

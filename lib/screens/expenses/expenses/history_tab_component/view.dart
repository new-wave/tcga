import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:tcga_flutter/database/models/enums.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/utils/enum_to_string.dart';
import 'package:tcga_flutter/widget/import.dart';
import '../../../../config/palette.dart';
import '../action.dart';
import 'action.dart';
import 'state.dart';

Widget buildView(
    HistoryState state, Dispatch dispatch, ViewService viewService) {
  final ListAdapter adapter = viewService.buildAdapter();
  final options = HistoryState.optionsType.values.toList();

  checkStateItem(options, state);
  return Column(
    crossAxisAlignment: CrossAxisAlignment.stretch,
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      SizedBox(
        height: 16,
      ),
      Container(
        height: 36,
        margin: edgeHorizontalInsetsPrimary,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              height: 36,
              alignment: Alignment.centerLeft,
              child: Text(
                AppLocalizations.of(viewService.context)
                    .text('expenses.filter'),
                style: textStyleSecondary,
              ),
            ),
            Expanded(
              child: ListView.builder(
                itemCount: options.length,
                shrinkWrap: true,
                padding: EdgeInsets.all(0),
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, index) {
                  return GestureDetector(
                    onTap: () {
                      dispatch(HistoryActionCreator.filter(index));
                    },
                    child: Container(
                      margin: edgeLeftInsetsSecondary,
                      height: 36,
                      padding: const EdgeInsets.symmetric(
                        horizontal: 16,
                      ),
                      decoration: BoxDecoration(
                        color: state.selectItems != null
                            ? state.selectItems[index]
                                ? colorPrimary
                                : Colors.white
                            : Colors.white,
                        border: Border.all(
                          color: state.selectItems != null
                              ? state.selectItems[index]
                                  ? colorPrimary
                                  : colorTextPrimary
                              : colorTextPrimary,
                        ),
                        borderRadius: BorderRadius.circular(18),
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            AppLocalizations.of(viewService.context)
                                .text((options[index])),
                            style: state.selectItems != null
                                ? state.selectItems[index]
                                    ? textStylePrimary.copyWith(
                                        color: Colors.white,
                                      )
                                    : textStylePrimary
                                : textStylePrimary,
                          ),
                        ],
                      ),
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
      Expanded(
        child: ListView.builder(
          itemBuilder: adapter.itemBuilder,
          itemCount: adapter.itemCount,
          shrinkWrap: true,
        ),
      ),
    ],
  );
}

void checkStateItem(var options, HistoryState state) {
  if (state.selectItems == null) {
    state.selectItems = List((options.length));
    for (int i = 0; i < options.length; i++) {
      state.selectItems[i] = false;
    }
  } else {
    if (state.selectItems.length < options.length) {
      int difference = options.length - state.selectItems.length;

      for (int i = difference - 1; i < options.length; i++) {
        state.selectItems.add(false);
      }
    }
  }
}

import 'package:fish_redux/fish_redux.dart';

import 'action.dart';
import 'state.dart';

Reducer<ExpenseItemState> buildReducer() {
  return asReducer(
    <Object, Reducer<ExpenseItemState>>{
      ExpenseItemAction.action: _onAction,
    },
  );
}

ExpenseItemState _onAction(ExpenseItemState state, Action action) {
  final ExpenseItemState newState = state.clone();
  return newState;
}

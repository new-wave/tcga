import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/database/models/enums.dart';
import 'package:tcga_flutter/utils/currency_format.dart';
import 'package:tcga_flutter/utils/enum_to_string.dart';
import 'package:tcga_flutter/utils/format_date.dart';
import '../../action.dart';

import 'state.dart';

Widget buildView(
    ExpenseItemState state, Dispatch dispatch, ViewService viewService) {
  Widget _buildStatusTracking(ExpenseStatus status) {
    switch (status) {
      case ExpenseStatus.Pending:
        return buildStatus(getEnumValue(ExpenseStatus.Pending),
            color: Palette.warning);
        break;
      case ExpenseStatus.Invoiced:
        return buildStatus(getEnumValue(ExpenseStatus.Invoiced),
            color: colorDisabled);
        break;
      default:
        return buildStatus(getEnumValue(ExpenseStatus.Paid));
    }
  }

  return InkWell(
    onTap: () => dispatch(
      ExpensesActionCreator.view(state),
    ),
    child: SizedBox(
      height: 110,
      child: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 16,
          vertical: 8,
        ),
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          decoration: BoxDecoration(
            color: colorBackground,
            borderRadius: BorderRadius.circular(5.0),
          ),
          child: Center(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: Text(
                        state.title,
                        style: TextStyle(
                          color: colorPrimary,
                          fontSize: 14,
                        ),
                        maxLines: 1,
                        softWrap: true,
                        overflow: TextOverflow.visible,
                      ),
                    ),
                    Expanded(
                      child: Text(
                        '${convertMoney(state.amount)}',
                        style: TextStyle(
                          color: colorTextPrimary,
                          fontSize: 14,
                        ),
                        textAlign: TextAlign.right,
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 6,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      child: Text(
                        '${getEnumValue(state.category)}',
                        style: TextStyle(
                          color: colorTextSecondary,
                          fontSize: 12,
                        ),
                      ),
                    ),
                    Text(
                      '${dateFormatYMMMD(state.recordedOn)}',
                      style: TextStyle(
                        color: colorTextSecondary,
                        fontSize: 12,
                      ),
                      textAlign: TextAlign.right,
                    ),
                  ],
                ),
                SizedBox(
                  height: 5,
                ),
                Row(
                  children: [
                    Expanded(
                      child: Row(
                        children: [
                          SvgPicture.asset(
                            'assets/icons/ic_user_group.svg',
                            fit: BoxFit.fitHeight,
                            color: colorTextSecondary,
                            height: 14,
                          ),
                          const SizedBox(
                            width: 4,
                          ),
                          Text(
                            '${state.firstName} ${state.lastName}',
                            style: TextStyle(
                              color: colorTextSecondary,
                              fontSize: 12,
                            ),
                            maxLines: 1,
                            softWrap: true,
                            overflow: TextOverflow.visible,
                          ),
                        ],
                      ),
                    ),
                    _buildStatusTracking(state.status),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    ),
  );
}

Container buildStatus(String title, {Color color = colorIndicatorPositive}) {
  return Container(
    height: 14,
    padding: const EdgeInsets.symmetric(
      horizontal: 16,
    ),
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(7),
      color: color,
    ),
    child: Text(
      title,
      style: TextStyle(
        color: Colors.white,
        fontSize: 10,
      ),
    ),
  );
}

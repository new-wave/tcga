import 'package:fish_redux/fish_redux.dart';

import 'effect.dart';
import 'reducer.dart';
import 'state.dart';
import 'view.dart';

class ExpenseItemComponent extends Component<ExpenseItemState> {
  ExpenseItemComponent()
      : super(
          // effect: buildEffect(),
          // reducer: buildReducer(),
          view: buildView,
          dependencies: Dependencies<ExpenseItemState>(
              adapter: null, slots: <String, Dependent<ExpenseItemState>>{}),
        );
}

import 'package:fish_redux/fish_redux.dart';

//TODO replace with your own action
enum ExpenseItemAction { action }

class ExpenseItemActionCreator {
  static Action onAction() {
    return const Action(ExpenseItemAction.action);
  }
}

import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/database/models/enums.dart';
import 'package:tcga_flutter/screens/expenses/expense_editor/add_image_component/image_item_component/state.dart';

class ExpenseItemState implements Cloneable<ExpenseItemState> {
  String id;
  String title;

  double amount;
  String currencyCode;

  ExpenseCategory category;
  ExpenseStatus status;

  String personId;
  String firstName;
  String lastName;

  String get fullName => '$firstName $lastName';

  DateTime recordedOn;
  List<ImageItemState> photos;

  ExpenseItemState({
    this.id,
    this.amount,
    this.currencyCode,
    this.firstName,
    this.lastName,
    this.personId,
    this.title,
    this.category,
    this.status,
    this.recordedOn,
    this.photos,
  });

  @override
  ExpenseItemState clone() {
    return ExpenseItemState()
      ..id = id
      ..personId = personId
      ..amount = amount
      ..currencyCode = currencyCode
      ..firstName = firstName
      ..lastName = lastName
      ..category = category
      ..status = status
      ..title = title
      ..recordedOn = recordedOn
      ..photos = photos;
  }
}

ExpenseItemState initState(Map<String, dynamic> args) {
  return ExpenseItemState();
}

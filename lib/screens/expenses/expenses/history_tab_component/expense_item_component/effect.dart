import 'package:fish_redux/fish_redux.dart';
import 'action.dart';
import 'state.dart';

Effect<ExpenseItemState> buildEffect() {
  return combineEffects(<Object, Effect<ExpenseItemState>>{
    ExpenseItemAction.action: _onAction,
  });
}

void _onAction(Action action, Context<ExpenseItemState> ctx) {
}

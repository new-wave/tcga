import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';

import 'action.dart';
import 'state.dart';

Widget buildView(
    ExpenseState state, Dispatch dispatch, ViewService viewService) {
  return Scaffold(
    backgroundColor: Colors.white,
    appBar: _appBar(viewService, dispatch),
    body: Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        viewService.buildComponent('summary'),
        SizedBox(
          height: 16,
        ),
        Expanded(
          child: DefaultTabController(
            length: 2,
            initialIndex: 0,
            child: Scaffold(
              appBar: PreferredSize(
                preferredSize: Size(
                  double.infinity,
                  46,
                ),
                child: Stack(
                  children: [
                    TabBar(
                      indicatorColor: colorPrimary,
                      indicatorWeight: 3,
                      unselectedLabelColor: colorTextSecondary,
                      labelColor: colorPrimary,
                      labelStyle: textStylePrimaryTabBar,
                      unselectedLabelStyle: textStyleSecondaryTabBar,
                      tabs: [
                        Tab(
                            text: AppLocalizations.of(viewService.context)
                                .text('expenses.history')),
                        Tab(
                            text: AppLocalizations.of(viewService.context)
                                .text('expenses.report')),
                      ],
                    ),
                    Positioned(
                        bottom: 3,
                        left: 0,
                        right: 0,
                        child: Container(
                          height: 1,
                          color: Color(0xffE0E0E0),
                        )),
                  ],
                ),
              ),
              body: TabBarView(
                children: [
                  viewService.buildComponent('history-tab'),
                  viewService.buildComponent('report-tab'),
                ],
              ),
            ),
          ),
        )
      ],
    ),
  );
}

AppBar _appBar(ViewService viewService, Dispatch dispatch) {
  return AppBar(
    elevation: 3,
    backgroundColor: Colors.white,
    leading: IconButton(
      iconSize: 24,
      icon: SvgPicture.asset('assets/icons/ic_arrow_left_circle.svg'),
      onPressed: () {
        Navigator.pop(viewService.context);
      },
    ),
    title:
        Text(AppLocalizations.of(viewService.context).text('expenses.title')),
    actions: [
      IconButton(
        iconSize: 24,
        icon: SvgPicture.asset('assets/icons/ic_button_add.svg'),
        onPressed: () => dispatch(
          ExpensesActionCreator.create(),
        ),
      )
    ],
  );
}

import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'history_tab_component/expense_item_component/state.dart';
import '../../../utils/datetime_extensions.dart';

import '../currency_picker_page/currency_item_component/state.dart';
import 'action.dart';
import 'state.dart';

var items = List<ExpenseItemState>();
var currentItemStates = List<CurrencyItemState>();

Reducer<ExpenseState> buildReducer() {
  return asReducer(
    <Object, Reducer<ExpenseState>>{
      ExpensesAction.initialize: _initialize,
      ExpensesAction.handleExpenseCreated: _handleExpenseCreated,
      ExpensesAction.handleExpenseEdited: _handleExpenseEdited,
      ExpensesAction.handleExpenseDeleted: _handleExpenseDeleted,
      ExpensesAction.filterinitialize: _filterinitialize,
      ExpensesAction.selectCareRecipient: _selectCareRecipient,
      ExpensesAction.selectCurrency: _selectCurrency,
      ExpensesAction.selectTime: _selectTime,
      ExpensesAction.dispose: _dispose,
    },
  );
}

ExpenseState _dispose(ExpenseState state, Action action) {
  final ExpenseState newState = state.clone();
  for (var item in currentItemStates) {
    item.total = 0;
  }
  return newState;
}

ExpenseState _initialize(ExpenseState state, Action action) {
  final ExpenseState newState = state.clone();
  final map = action.payload;
  final expenseItems =
      map != null ? map['expense'] ?? <ExpenseItemState>[] : null;
  final currents = map != null ? map['currents'] ?? [] : null;
  currentItemStates = currents;
  newState.expenseItems = expenseItems;
  items = expenseItems;
  newState.selectItems = [false, false, false];
  newState.hasSelectedCareRecipient = false;
  newState.careRecipientFullName =
      AppLocalizations.instance.text('expenses.care_recipient');
  newState.hasSelectedCurrency = false;
  newState.selectedCurrency =
      AppLocalizations.instance.text('expenses.currencies');
  newState.myCurrencyCode = getMyCurrencyCode(items);

  for (var item in currentItemStates) {
    item.total = 0;
  }
  for (var item in items) {
    currentItemStates
        .firstWhere((element) => element.code == item.currencyCode)
        .total += item.amount;
  }
  newState.total =
      currentItemStates.where((element) => element.total > 0).toList();
  return newState;
}

String getMyCurrencyCode(List<ExpenseItemState> expenseItems) {
  String myCurrencyCode = '';
  if (items.length > 0) {
    myCurrencyCode = myCurrencyCode + items[0].currencyCode + ",";
    if (items.length > 1) {
      for (int i = 1; i < items.length; i++) {
        if (!myCurrencyCode.contains(items[i].currencyCode)) {
          myCurrencyCode = myCurrencyCode + items[i].currencyCode + ",";
        }
      }
    }
    myCurrencyCode = myCurrencyCode.substring(0, myCurrencyCode.length - 1);
  }
  return myCurrencyCode;
}

ExpenseState _handleExpenseCreated(ExpenseState state, Action action) {
  final ExpenseState newState = state.clone();
  final expense = action.payload as ExpenseItemState;
  items.add(expense);
  newState.expenseItem = expense;
  newState.expenseItems = items;
  newState.selectItems = [false, false, false];
  newState.myCurrencyCode = getMyCurrencyCode(items);
  currentItemStates
      .firstWhere(
        (element) => element.code == expense.currencyCode,
      )
      .total += expense.amount;
  newState.total =
      currentItemStates.where((element) => element.total > 0).toList();
  return newState;
}

ExpenseState _handleExpenseEdited(ExpenseState state, Action action) {
  final ExpenseState newState = state.clone();
  final item = action.payload as ExpenseItemState;

  final index = newState.expenseItems.indexWhere(
    (x) => x.id == item.id,
  );
  newState.expenseItem = item;
  newState.expenseItems[index] = item;
  items[index] = item;
  for (var item in currentItemStates) {
    item.total = 0;
  }
  for (var item in items) {
    currentItemStates
        .firstWhere(
          (element) => element.code == item.currencyCode,
        )
        .total += item.amount;
  }
  newState.total =
      currentItemStates.where((element) => element.total > 0).toList();
  newState.myCurrencyCode = getMyCurrencyCode(items);
  return newState;
}

ExpenseState _handleExpenseDeleted(ExpenseState state, Action action) {
  final ExpenseState newState = state.clone();
  final item = action.payload as String;

  final index = items.indexWhere(
    (x) => x.id == item,
  );
  newState.expenseItem = new ExpenseItemState();
  items.removeAt(index);
  for (var item in currentItemStates) {
    item.total = 0;
  }
  for (var item in items) {
    currentItemStates
        .firstWhere(
          (element) => element.code == item.currencyCode,
        )
        .total += item.amount;
  }
  newState.expenseItems = items;
  newState.total =
      currentItemStates.where((element) => element.total > 0).toList();
  newState.myCurrencyCode = getMyCurrencyCode(items);
  return newState;
}

ExpenseState _selectCareRecipient(ExpenseState state, Action action) {
  final item = action.payload as String;
  final ExpenseState newState = state.clone();
  newState.careRecipientFullName = item;
  if (item != AppLocalizations.instance.text('expenses.care_recipient')) {
    newState.hasSelectedCareRecipient = true;
  } else {
    newState.hasSelectedCareRecipient = false;
  }
  return newState;
}

ExpenseState _selectCurrency(ExpenseState state, Action action) {
  final item = action.payload as String;
  final ExpenseState newState = state.clone();

  if (item != AppLocalizations.instance.text('expenses.currencies')) {
    newState.hasSelectedCurrency = true;
  } else {
    newState.hasSelectedCurrency = false;
  }
  newState.selectedCurrency = item;
  newState.total = currentItemStates
      .where((element) =>
          newState.hasSelectedCurrency ? element.code == item : true)
      .toList();
  return newState;
}

ExpenseState _selectTime(ExpenseState state, Action action) {
  final ExpenseState newState = state.clone();
  final index = action.payload;
  newState.indexTimeFilter = index;
  return newState;
}

ExpenseState _filterinitialize(ExpenseState state, Action action) {
  final ExpenseState newState = state.clone();
  var filterCareRecipients = items
      .where((element) => state.hasSelectedCareRecipient
          ? element.fullName == state.careRecipientFullName
          : true)
      .toList();
  var filterCurrencys = filterCareRecipients
      .where((element) => state.hasSelectedCurrency
          ? element.currencyCode == state.selectedCurrency
          : true)
      .toList();
  DateTime dateTime = DateTime.now();
  DateTime date = new DateTime(dateTime.year, dateTime.month, dateTime.day);
  var timeFliter = filterCurrencys.where((element) {
    switch (state.indexTimeFilter) {
      case 1:
        var recordedOnDate = new DateTime(element.recordedOn.year,
            element.recordedOn.month, element.recordedOn.day);
        return recordedOnDate == date.add(new Duration(days: -1));
      case 2:
        return element.recordedOn.isInThisWeek();
      case 3:
        return element.recordedOn.isInLastWeek();
      case 4:
        return element.recordedOn.month == dateTime.month;
      case 5:
        return element.recordedOn.month == (dateTime.month - 1);
      case 0:
        var recordedOnDate = new DateTime(element.recordedOn.year,
            element.recordedOn.month, element.recordedOn.day);
        return recordedOnDate == date;
      default:
        return true;
    }
  }).toList();
  newState.expenseItems = timeFliter.where((element) {
    if ((newState.selectItems[0] &&
            newState.selectItems[1] &&
            newState.selectItems[2]) ||
        (!newState.selectItems[0] &&
            !newState.selectItems[1] &&
            !newState.selectItems[2])) return true;
    if (newState.selectItems[0] && newState.selectItems[1])
      return element.status.index != 2;
    if (newState.selectItems[1] && newState.selectItems[2])
      return element.status.index != 0;
    if (newState.selectItems[0] && newState.selectItems[2])
      return element.status.index != 1;
    else
      return newState.selectItems[0]
          ? element.status.index == 0
          : newState.selectItems[1]
              ? element.status.index == 1
              : element.status.index == 2;
  }).toList();

  for (var item in currentItemStates) {
    item.total = 0;
  }
  for (var item in items) {
    if (state.hasSelectedCareRecipient) {
      if (item.fullName == state.careRecipientFullName) {
        currentItemStates.firstWhere((element) {
          return element.code == item.currencyCode;
        }).total += item.amount;
      }
    } else {
      currentItemStates.firstWhere((element) {
        return element.code == item.currencyCode;
      }).total += item.amount;
    }
  }
  newState.total = currentItemStates
      .where((element) => state.hasSelectedCurrency
          ? (state.selectedCurrency == element.code && element.total > 0)
          : element.total > 0)
      .toList();
  return newState;
}

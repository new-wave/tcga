import 'package:fish_redux/fish_redux.dart';

import 'history_tab_component/component.dart';
import 'report_tab_component/component.dart';
import 'summary_component/component.dart';

import 'effect.dart';
import 'reducer.dart';
import 'state.dart';
import 'view.dart';

class ExpensePage extends Page<ExpenseState, Map<String, dynamic>> {
  ExpensePage()
      : super(
          initState: initState,
          effect: buildEffect(),
          reducer: buildReducer(),
          view: buildView,
          dependencies: Dependencies<ExpenseState>(
              slots: <String, Dependent<ExpenseState>>{
                'summary': SummaryStateConnector() + SummaryComponent(),
                'history-tab': HistoryStateConnector() + HistoryTabComponent(),
                'report-tab': ReportStateConnector() + ReportTabComponent(),
              }),
          middleware: <Middleware<ExpenseState>>[],
        );
}

import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/models/chart_model.dart';
import 'package:tcga_flutter/screens/expenses/expenses/history_tab_component/expense_item_component/state.dart';

class ReportTabState implements Cloneable<ReportTabState> {
  List<ExpenseItemState> expenseItems;
  List<ChartModel> singleChart;
  List<List<ChartModel>> multipleChart;

  @override
  ReportTabState clone() {
    return ReportTabState()
      ..expenseItems = expenseItems
      ..singleChart = singleChart
    ..multipleChart = multipleChart;
  }
}

ReportTabState initState(Map<String, dynamic> args) {
  ReportTabState newState = ReportTabState().clone();
  newState.expenseItems = [];
  newState.singleChart = [];
  newState.multipleChart = [];
  return newState;
}

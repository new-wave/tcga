import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/expenses/expenses/history_tab_component/expense_item_component/state.dart';
import 'action.dart';
import 'state.dart';

Effect<ReportTabState> buildEffect() {
  return combineEffects(<Object, Effect<ReportTabState>>{
    ReportTabAction.initialize: _onInit,
  });
}

void _onInit(Action action, Context<ReportTabState> ctx) {}

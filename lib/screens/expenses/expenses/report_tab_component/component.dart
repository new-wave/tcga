import 'package:fish_redux/fish_redux.dart';

import 'effect.dart';
import 'reducer.dart';
import 'state.dart';
import 'view.dart';

class ReportTabComponent extends Component<ReportTabState> {
  ReportTabComponent()
      : super(
          effect: buildEffect(),
          reducer: buildReducer(),
          view: buildView,
          dependencies: Dependencies<ReportTabState>(
              adapter: null, slots: <String, Dependent<ReportTabState>>{}),
        );
}

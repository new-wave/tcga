import 'package:fish_redux/fish_redux.dart';

//TODO replace with your own action
enum ReportTabAction { initialize }

class ReportTabActionCreator {
  static Action onInit() {
    return const Action(ReportTabAction.initialize);
  }
}

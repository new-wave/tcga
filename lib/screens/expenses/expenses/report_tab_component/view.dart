import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:tcga_flutter/screens/expenses/expenses/report_tab_component/charts_component/chart.dart';

import 'action.dart';
import 'state.dart';

Widget buildView(
    ReportTabState state, Dispatch dispatch, ViewService viewService) {
  return ChartContainer(state.expenseItems);
}



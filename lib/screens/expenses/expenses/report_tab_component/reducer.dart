import 'package:fish_redux/fish_redux.dart';

import 'action.dart';
import 'state.dart';

Reducer<ReportTabState> buildReducer() {
  return asReducer(
    <Object, Reducer<ReportTabState>>{
      ReportTabAction.initialize: _onAction,
    },
  );
}

ReportTabState _onAction(ReportTabState state, Action action) {
  final ReportTabState newState = state.clone();
  return newState;
}

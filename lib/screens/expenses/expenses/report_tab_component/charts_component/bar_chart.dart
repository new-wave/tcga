import 'package:flutter/material.dart';
import 'package:tcga_flutter/models/chart_model.dart';
import 'package:tcga_flutter/screens/expenses/expenses/report_tab_component/charts_component/chart_adapter.dart';
import 'package:tcga_flutter/screens/expenses/expenses/report_tab_component/charts_component/chart_detail.dart';
import 'package:tcga_flutter/widget/horizontal_bar_chart.dart';

class BarChartContainer extends StatelessWidget {
  final List<List<ChartModel>> data;

  const BarChartContainer(
    this.data, {
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: data.map((e) => BarChartItem(e)).toList(),
        ),
      ),
      bottomSheet: Container(
        height: 60,
        child: ChartDetail(),
      ),
    );
  }
}

class BarChartItem extends StatelessWidget {
  final List<ChartModel> chartData;

  const BarChartItem(
    this.chartData, {
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ChartLabel(getCurrencyBarChart(chartData)),
          Expanded(
            child: SizedBox(
              height: 90,
              child: HorizontalBarChart(chartData),
            ),
          ),
        ],
      ),
    );
  }
}

class ChartLabel extends StatelessWidget {
  final String label;

  const ChartLabel(
    this.label, {
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 24.0),
      child: Text(
        label,
        style: TextStyle(
          fontWeight: FontWeight.w300,
          fontSize: 12,
        ),
      ),
    );
  }
}

import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/models/chart_model.dart';
import 'package:tcga_flutter/screens/expenses/expenses/report_tab_component/charts_component/chart_detail.dart';

class PieChartByExpenseType extends StatefulWidget {
  final List<ChartModel> data;

  const PieChartByExpenseType({
    @required this.data,
    Key key,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => PieChart2State();
}

class PieChart2State extends State<PieChartByExpenseType> {
  int touchedIndex;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 20.0),
            child: Text(
              'Expense Report',
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.w400,
                color: Colors.black,
              ),
            ),
          ),
          Expanded(
            child: PieChart(
              PieChartData(
                pieTouchData: PieTouchData(touchCallback: (pieTouchResponse) {
                  setState(() {
                    if (pieTouchResponse.touchInput is FlLongPressEnd ||
                        pieTouchResponse.touchInput is FlPanEnd) {
                      touchedIndex = -1;
                    } else {
                      touchedIndex = pieTouchResponse.touchedSectionIndex;
                    }
                  });
                }),
                borderData: FlBorderData(
                  show: false,
                ),
                centerSpaceRadius: 35,
                sections: showingSections(widget.data),
              ),
            ),
          ),
          ChartDetail(data: widget.data),
        ],
      ),
    );
  }

  List<PieChartSectionData> showingSections(List<ChartModel> data) {
    return List.generate(4, (i) {
      final isTouched = i == touchedIndex;
      final double fontSize = isTouched ? 25 : 16;
      final double radius = isTouched ? 100 : 90;
      switch (i) {
        case 0:
          return PieChartSectionData(
            color: chartPalette[0],
            value: data[0].percent,
            title: data[0].percent > 0
                ? '${data[0].percent.toStringAsFixed(2)}%'
                : '',
            radius: radius,
            titleStyle: TextStyle(
                fontSize: fontSize,
                fontWeight: FontWeight.bold,
                color: const Color(0xf008080)),
          );
        case 1:
          return PieChartSectionData(
            color: chartPalette[1],
            value: data[1].percent,
            title: data[1].percent > 0
                ? '${data[1].percent.toStringAsFixed(2)}%'
                : '',
            radius: radius,
            titleStyle: TextStyle(
                fontSize: fontSize,
                fontWeight: FontWeight.bold,
                color: const Color(0xffffffff)),
          );
        case 2:
          return PieChartSectionData(
            color: chartPalette[2],
            value: data[2].percent,
            title: data[2].percent > 0
                ? '${data[2].percent.toStringAsFixed(2)}%'
                : '',
            radius: radius,
            titleStyle: TextStyle(
                fontSize: fontSize,
                fontWeight: FontWeight.bold,
                color: const Color(0xffffffff)),
          );
        case 3:
          return PieChartSectionData(
            color: chartPalette[3],
            value: data[3].percent,
            title: data[3].percent > 0
                ? '${data[3].percent.toStringAsFixed(2)}%'
                : '',
            radius: radius,
            titleStyle: TextStyle(
                fontSize: fontSize,
                fontWeight: FontWeight.bold,
                color: const Color(0xffffffff)),
          );
        default:
          return null;
      }
    });
  }
}

import 'package:flutter/material.dart';
import 'package:tcga_flutter/screens/expenses/expenses/history_tab_component/expense_item_component/state.dart';

import 'chart_adapter.dart';

class ChartContainer extends StatelessWidget {
  final List<ExpenseItemState> expenseItems;

  const ChartContainer(
    this.expenseItems, {
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return labChart(expenseItems);
  }
}

import 'package:flutter/cupertino.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/database/models/enums.dart';
import 'package:tcga_flutter/models/chart_model.dart';
import 'package:tcga_flutter/screens/expenses/expenses/history_tab_component/expense_item_component/state.dart';
import 'package:tcga_flutter/screens/expenses/expenses/report_tab_component/charts_component/bar_chart.dart';
import 'package:tcga_flutter/screens/expenses/expenses/report_tab_component/charts_component/pie_chart.dart';

Widget labChart(List<ExpenseItemState> expenseItems) {
  if (expenseItems.isEmpty) {
    //Todo when have no data to show
    return Center(
      child: Text(
        'No Data',
        style: TextStyle(
          color: colorIndicatorPositive,
        ),
      ),
    );
  } else {
    bool isMultipleCurrencyCode = checkMultipleCurrencyCode(expenseItems);
    if (isMultipleCurrencyCode) {
      //todo prepare bar chart data
      List<List<ChartModel>> data = getChartDataCurrencies(expenseItems);
      return BarChartContainer(data);
    } else {
      //todo prepare pie chart data
      List<ChartModel> data = getChartDataSingleCurrency(expenseItems);
      return PieChartByExpenseType(data: data);
    }
  }
}

List<List<ChartModel>> getChartDataCurrencies(
    List<ExpenseItemState> expenseItems) {
  List<List<ChartModel>> data = List<List<ChartModel>>();
  List<ExpenseItemState> dataByCurrency = [];
  List<ExpenseItemState> items = new List<ExpenseItemState>.from(expenseItems);
  while (items.length > 0) {
    dataByCurrency = items
        .where((element) => element.currencyCode == items[0].currencyCode)
        .toList();
    data.add(getChartDataSingleCurrency(dataByCurrency));
    dataByCurrency.forEach(items.remove);
  }
  return data ?? [[]];
}

List<ChartModel> getChartDataSingleCurrency(
    List<ExpenseItemState> expenseItems) {
  List<ChartModel> singleChart = [];
  double total = expenseItems.fold(
      0, (previousValue, element) => previousValue + element.amount);
  singleChart.addAll(ExpenseCategory.values
      .map((e) => ChartModel(category: e, value: 0, percent: 0))
      .toList());
  //progress expense to chart data
  expenseItems.forEach((element) {
    if (singleChart != null && singleChart.isNotEmpty) {
      singleChart.asMap().forEach((index, e) {
        if (e.category == element.category) {
          singleChart[index] = ChartModel(
            category: element.category,
            value: e.value + element.amount,
            percent: (element.amount + e.value) / total * 100,
            currencyCode: element.currencyCode,
          );
          return;
        }
      });
    }
  });
  return singleChart;
}

bool checkMultipleCurrencyCode(List<ExpenseItemState> expenses) {
  int length = expenses.length;

  for (int i = 0; i < length - 1; i++) {
    if (expenses[i].currencyCode != expenses[i + 1].currencyCode) {
      return true;
    }
  }
  return false;
}

String getCurrencyBarChart(List<ChartModel> list) {
  String currency = '';
  list.forEach((element) {
    if (element.currencyCode != null) {
      currency = element.currencyCode;
      return;
    }
  });
  return currency;
}

import 'package:flutter/material.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/database/models/enums.dart';
import 'package:tcga_flutter/models/chart_model.dart';
import 'package:tcga_flutter/widget/indicator.dart';

class ChartDetail extends StatelessWidget {
  final List<ChartModel> data;

  const ChartDetail({
    Key key,
    this.data,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: ExpenseCategory.values
              .asMap()
              .map((key, value) => MapEntry(
                    key,
                    Flexible(
                      flex: 1,
                      child: Indicator(
                        color: chartPalette[key],
                        text: '${value.toString().split(".")[1]}' ?? '',
                        subValue: data != null ? data[key].value : null,
                        size: 14,
                      ),
                    ),
                  ))
              .values
              .toList(),
        ),
      ),
    );
  }
}

import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/route_name.dart';
import 'package:tcga_flutter/screens/expenses/expenses/reducer.dart';
import 'package:tcga_flutter/utils/utils.dart';

import '../currency_picker_page/manager.dart';
import '../manager.dart';
import 'action.dart';
import 'state.dart';

final _manager = ExpenseManager();
final _currencyManager = CurrencyManager();

Effect<ExpenseState> buildEffect() {
  return combineEffects(<Object, Effect<ExpenseState>>{
    Lifecycle.initState: _initialize,
    Lifecycle.dispose: _dispose,
    ExpensesAction.create: _create,
    ExpensesAction.view: _view,
  });
}

Future _initialize(
  Action action,
  Context<ExpenseState> ctx,
) async {
  final expenseItems = await _manager.query();
  final currents = await _currencyManager.query(
    ctx.context,
  );
  Map map = Map<String, List>();
  map['expense'] = expenseItems;
  map['currents'] = currents;
  await ctx.dispatch(
    ExpensesActionCreator.initialize(
      map,
    ),
  );
}

Future _create(
  Action action,
  Context<ExpenseState> ctx,
) async {
  var result = await ctx.navigateTo(
    RouteName.expensesEditor,
  );

  if (result == null) return;

  ctx.dispatch(
    ExpensesActionCreator.handleExpenseCreated(
      result,
    ),
  );
}

Future _view(
  Action action,
  Context<ExpenseState> ctx,
) async {
  final item = action.payload;
  // await ctx.navigateTo(
  //   RouteName.expensesEditor,
  //   arguments: {
  //     'item': action.payload,
  //   },
  // );

  if (item == null) return;

  final result = await ctx.navigateTo(
    RouteName.expensesEditor,
    arguments: {
      'item': item.clone(),
    },
  );
  if (result == null) return;
  if (result is Map) {
    ctx.dispatch(
      ExpensesActionCreator.handleExpenseDeleted(
        result['deleted-item-id'],
      ),
    );
    return;
  }
  ctx.dispatch(
    ExpensesActionCreator.handleExpenseEdited(
      result,
    ),
  );
}

void _dispose(
  Action action,
  Context<ExpenseState> ctx,
) {
  ctx.dispatch(
    ExpensesActionCreator.dispose(),
  );
}

import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/expenses/expense_editor/state.dart';

import 'history_tab_component/expense_item_component/state.dart';

enum ExpensesAction {
  initialize,
  create,
  handleExpenseCreated,
  view,
  handleExpenseEdited,
  handleExpenseDeleted,
  filterinitialize,
  selectCareRecipient,
  selectCurrency,
  selectTime,
  dispose,
}

class ExpensesActionCreator {
  static Action initialize(Map map) {
    return Action(
      ExpensesAction.initialize,
      payload: map,
    );
  }

  static Action create() {
    return const Action(
      ExpensesAction.create,
    );
  }

  static Action view(ExpenseItemState item) {
    return Action(
      ExpensesAction.view,
      payload: item,
    );
  }

  static Action handleExpenseEdited(ExpenseItemState result) {
    return Action(
      ExpensesAction.handleExpenseEdited,
      payload: result,
    );
  }

  static Action handleExpenseDeleted(String result) {
    return Action(
      ExpensesAction.handleExpenseDeleted,
      payload: result,
    );
  }

  static Action handleExpenseCreated(ExpenseItemState result) {
    return Action(
      ExpensesAction.handleExpenseCreated,
      payload: result,
    );
  }

  static Action filterinitialize() {
    return const Action(ExpensesAction.filterinitialize);
  }

  static Action selectCareRecipient(String fullName) {
    return Action(ExpensesAction.selectCareRecipient, payload: fullName);
  }

  static Action selectCurrency(String currency) {
    return Action(ExpensesAction.selectCurrency, payload: currency);
  }

  static Action selectTime(int index) {
    return Action(ExpensesAction.selectTime, payload: index);
  }

  static Action dispose() {
    return const Action(ExpensesAction.dispose);
  }
}

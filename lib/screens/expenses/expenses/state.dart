import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/expenses/expenses/report_tab_component/state.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';

import '../currency_picker_page/currency_item_component/state.dart';
import 'history_tab_component/expense_item_component/state.dart';
import 'history_tab_component/state.dart';
import 'summary_component/state.dart';

class ExpenseState implements Cloneable<ExpenseState> {
  List<ExpenseItemState> expenseItems;
  List<bool> selectItems;
  ExpenseItemState expenseItem;
  bool hasSelectedCareRecipient;
  String careRecipientFullName;

  bool hasSelectedCurrency;
  String selectedCurrency;
  String myCurrencyCode;
  int indexTimeFilter;
  List<CurrencyItemState> total;
  @override
  ExpenseState clone() {
    return ExpenseState()
      ..expenseItems = expenseItems
      ..selectItems = selectItems
      ..careRecipientFullName = careRecipientFullName
      ..hasSelectedCareRecipient = hasSelectedCareRecipient
      ..hasSelectedCurrency = hasSelectedCurrency
      ..selectedCurrency = selectedCurrency
      ..myCurrencyCode = myCurrencyCode
      ..indexTimeFilter = indexTimeFilter
      ..total = total
      ..expenseItem = expenseItem;
  }
}

ExpenseState initState(Map<String, dynamic> args) {
  return ExpenseState()
    ..hasSelectedCareRecipient = false
    ..careRecipientFullName =
        AppLocalizations.instance.text('expenses.care_recipient')
    ..hasSelectedCurrency = false
    ..selectedCurrency = AppLocalizations.instance.text('expenses.currencies')
    ..myCurrencyCode = ''
    ..total = []
    ..expenseItems = [];
}

class SummaryStateConnector extends ConnOp<ExpenseState, SummaryState>
    with ReselectMixin<ExpenseState, SummaryState> {
  @override
  SummaryState computed(ExpenseState state) {
    return SummaryState()
      ..careRecipientFullName = state.careRecipientFullName
      ..hasSelectedCareRecipient = state.hasSelectedCareRecipient
      ..hasSelectedCurrency = state.hasSelectedCurrency
      ..selectedCurrency = state.selectedCurrency
      ..myCurrencyCode = state.myCurrencyCode
      ..indexTimeFilter = state.indexTimeFilter
      ..total = state.total;
  }

  @override
  List<dynamic> factors(ExpenseState state) {
    return [
      state.hasSelectedCurrency,
      state.hasSelectedCareRecipient,
      state.indexTimeFilter,
      state.total,
      state.expenseItems,
      state.expenseItem,
      state.myCurrencyCode,
    ];
  }

  @override
  void set(ExpenseState state, SummaryState subState) {
    state.myCurrencyCode = subState.myCurrencyCode;
  }
}

class HistoryStateConnector extends ConnOp<ExpenseState, HistoryState>
    with ReselectMixin<ExpenseState, HistoryState> {
  @override
  HistoryState computed(ExpenseState state) {
    return HistoryState()
      ..expenseItems = state.expenseItems
      ..selectItems = state.selectItems;
  }

  @override
  List<dynamic> factors(ExpenseState state) {
    return [
      state.expenseItem,
      state.expenseItems,
      state.expenseItems.length ?? 0,
    ];
  }

  @override
  void set(ExpenseState state, HistoryState subState) {}
}

class ReportStateConnector extends ConnOp<ExpenseState, ReportTabState> {
  // @override
  // HistoryState computed(ExpenseState state) {
  //   return ReportTabState().clone();
  // }

  // @override
  // List<dynamic> factors(ExpenseState state) {
  //   return <int>[
  //     state.expenseItems?.length,
  //   ];
  // }
  @override
  ReportTabState get(ExpenseState state) {
    return ReportTabState()..expenseItems = state.expenseItems;
  }

  @override
  void set(ExpenseState state, ReportTabState subState) =>
      state.expenseItems = subState.expenseItems;
}

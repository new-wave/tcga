import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart' hide Action;
import 'package:tcga_flutter/database/models/enums.dart';
import 'package:tcga_flutter/screens/expenses/currency_picker_page/currency_item_component/state.dart';
import 'package:tcga_flutter/screens/recipients/state.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/utils/enum_to_string.dart';
import 'package:tcga_flutter/utils/show_picker.dart';
import '../../../../route_name.dart';
import '../action.dart';
import 'action.dart';
import 'state.dart';

Effect<SummaryState> buildEffect() {
  return combineEffects(<Object, Effect<SummaryState>>{
    SummaryAction.selectCareRecipient: _selectCareRecipient,
    SummaryAction.clearSelectedCareRecipient: _clearSelectedCareRecipient,
    SummaryAction.clearSelectedCurrency: _clearSelectedCurrency,
    SummaryAction.selectCurrency: _selectCurrency,
    SummaryAction.selectDateRange: _selectDateRange,
  });
}

void _selectCareRecipient(Action action, Context<SummaryState> ctx) async {
  RecipientItemState selectedCareRecipient = await showModal(
    ctx.context,
    RouteName.recipientsPicker,
  );
  if (selectedCareRecipient != null) {
    ctx.dispatch(ExpensesActionCreator.selectCareRecipient(
        selectedCareRecipient.fullName));
    ctx.dispatch(ExpensesActionCreator.filterinitialize());
  }
}

void _clearSelectedCareRecipient(
    Action action, Context<SummaryState> ctx) async {
  ctx.dispatch(ExpensesActionCreator.selectCareRecipient(
      AppLocalizations.instance.text('expenses.care_recipient')));
  ctx.dispatch(ExpensesActionCreator.filterinitialize());
}

void _clearSelectedCurrency(Action action, Context<SummaryState> ctx) async {
  ctx.dispatch(ExpensesActionCreator.selectCurrency(
      AppLocalizations.instance.text('expenses.currencies')));
  ctx.dispatch(ExpensesActionCreator.filterinitialize());
}

void _selectCurrency(Action action, Context<SummaryState> ctx) async {
  CurrencyItemState selectedCurrency = await showModal(
    ctx.context,
    RouteName.currencyPicker,
    args: {
      'myCurrencyCode': action.payload as String ?? '',
    },
  );

  if (selectedCurrency == null) return;

  ctx.dispatch(ExpensesActionCreator.selectCurrency(selectedCurrency.code));
  ctx.dispatch(ExpensesActionCreator.filterinitialize());
}

void _selectDateRange(Action action, Context<SummaryState> ctx) async {
  print(ctx.state.indexTimeFilter);
  var timeFilter = convertEnumToList(TimeFilterOption.values);
  int indexTimeFilter = await showDialog(
    context: ctx.context,
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text('Duration'),
        content: Container(
          width: double.maxFinite,
          child: ListView.builder(
            shrinkWrap: true,
            itemCount: timeFilter.length,
            itemBuilder: (context, index) {
              return ListTile(
                onTap: () {
                  Navigator.of(context).pop(index);
                },
                title: Text('${timeFilter[index]}'),
              );
            },
          ),
        ),
      );
    },
  );
  ctx.dispatch(ExpensesActionCreator.selectTime(indexTimeFilter));
  ctx.dispatch(ExpensesActionCreator.filterinitialize());
}

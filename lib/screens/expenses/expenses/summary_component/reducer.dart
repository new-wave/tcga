import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/recipients/state.dart';

import 'action.dart';
import 'state.dart';

Reducer<SummaryState> buildReducer() {
  return asReducer(
    <Object, Reducer<SummaryState>>{
      SummaryAction.handleCareRecipient: _handleCareRecipient,
    },
  );
}

SummaryState _handleCareRecipient(SummaryState state, Action action) {
  final item = action.payload as RecipientItemState;
  final SummaryState newState = state.clone();
  newState.careRecipientFullName = item.fullName;
  return newState;
}

import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import '../../currency_picker_page/currency_item_component/state.dart';

class SummaryState implements Cloneable<SummaryState> {
  bool hasSelectedCareRecipient;
  String careRecipientFullName;

  bool hasSelectedCurrency;
  String selectedCurrency;
  String myCurrencyCode;
  int indexTimeFilter;
  List<CurrencyItemState> total;
  @override
  SummaryState clone() {
    return SummaryState()
      ..hasSelectedCareRecipient = hasSelectedCareRecipient
      ..careRecipientFullName = careRecipientFullName
      ..hasSelectedCurrency = hasSelectedCurrency
      ..selectedCurrency = selectedCurrency
      ..indexTimeFilter = indexTimeFilter
      ..myCurrencyCode = myCurrencyCode
      ..total = total;
  }
}

SummaryState initState(Map<String, dynamic> args) {
  return SummaryState()
    ..hasSelectedCareRecipient = false
    ..careRecipientFullName =
        AppLocalizations.instance.text('expenses.care_recipient')
    ..hasSelectedCurrency = false
    ..selectedCurrency = AppLocalizations.instance.text('expenses.currencies')
    ..myCurrencyCode = ''
    ..total = [];
}

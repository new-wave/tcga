import 'package:fish_redux/fish_redux.dart';

import 'effect.dart';
import 'reducer.dart';
import 'state.dart';
import 'view.dart';

class SummaryComponent extends Component<SummaryState> {
  SummaryComponent()
      : super(
          effect: buildEffect(),
          reducer: buildReducer(),
          view: buildView,
          dependencies: Dependencies<SummaryState>(
              adapter: null, slots: <String, Dependent<SummaryState>>{}),
        );
}

import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/database/models/enums.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/utils/currency_format.dart';
import 'package:tcga_flutter/utils/enum_to_string.dart';
import 'package:tcga_flutter/widget/import.dart';

import '../../../../config/palette.dart';
import 'action.dart';
import 'state.dart';

Widget buildView(
    SummaryState state, Dispatch dispatch, ViewService viewService) {
  ScrollController controller = ScrollController();
  double size = 0;
  return SizedBox(
    height: 168,
    child: Stack(
      children: [
        SizedBox(
          height: 112,
          child: Container(
            color: colorPrimary,
            child: Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 16,
                vertical: 20.0,
              ),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    child: Stack(
                      children: [
                        PickerWidget(
                          title: state.careRecipientFullName,
                          titleStyle: TextStyle(
                            color: colorPrimary,
                            fontSize: fontSizeNormal,
                          ),
                          suffixIcon: SvgPicture.asset(
                            'assets/icons/ic_user_group.svg',
                            fit: BoxFit.none,
                          ),
                          tap: () => dispatch(
                            SummaryActionCreator.selectCareRecipient(),
                          ),
                        ),
                        Visibility(
                          visible: state.hasSelectedCareRecipient,
                          child: Positioned(
                            height: 14,
                            width: 14,
                            child: InkWell(
                              onTap: () {
                                dispatch(SummaryActionCreator
                                    .removeSelectedCareRecipient());
                              },
                              child: Icon(
                                Icons.clear,
                                size: 16,
                                color: colorAccent,
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    width: 12,
                  ),
                  Expanded(
                    child: Stack(
                      children: [
                        PickerWidget(
                          title: state.selectedCurrency,
                          titleStyle: TextStyle(
                            color: colorPrimary,
                            fontSize: fontSizeNormal,
                          ),
                          tap: () => dispatch(
                            SummaryActionCreator.selectCurrency(
                                state.myCurrencyCode),
                          ),
                        ),
                        Visibility(
                          visible: state.hasSelectedCurrency,
                          child: Positioned(
                            height: 14,
                            width: 14,
                            child: InkWell(
                              onTap: () {
                                dispatch(SummaryActionCreator
                                    .removeSelectCurrency());
                              },
                              child: Icon(
                                Icons.clear,
                                size: 16,
                                color: colorAccent,
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        Positioned(
          top: 72,
          left: 0,
          right: 0,
          child: SizedBox(
            height: 96,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(5.0),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black.withOpacity(0.15),
                      offset: Offset(0, 1),
                      blurRadius: 10,
                    ),
                  ],
                ),
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 16,
                      ),
                      child: PickerWidget(
                        title: convertEnumToList(TimeFilterOption.values)[
                            state.indexTimeFilter ?? 0],
                        titleStyle: TextStyle(
                          color: colorPrimary,
                          fontSize: fontSizeNormal,
                        ),
                        suffixIcon:
                            SvgPicture.asset('assets/icons/ic_caret.svg'),
                        fillColor: Colors.white,
                        textAlign: TextAlign.center,
                        tap: () => dispatch(
                          SummaryActionCreator.selectDateRange(),
                        ),
                      ),
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: Container(
                            height: 0.5,
                            color: colorDisabled,
                          ),
                        ),
                        const SizedBox(
                          width: 16,
                        ),
                        Text(AppLocalizations.of(viewService.context)
                            .text('expenses.total')),
                        const SizedBox(
                          width: 16,
                        ),
                        Expanded(
                          child: Container(
                            height: 0.5,
                            color: colorDisabled,
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        InkResponse(
                          onTap: () {
                            if (size != 0) controller.jumpTo(size -= 100);
                          },
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: SvgPicture.asset(
                              'assets/icons/ic_chevron_left.svg',
                            ),
                          ),
                        ),
                        state.total.length > 1 ? Container() : Spacer(),
                        state.total.length != 0
                            ? state.total.length > 1
                                ? Expanded(
                                    child: SingleChildScrollView(
                                        controller: controller,
                                        scrollDirection: Axis.horizontal,
                                        child: Row(
                                          children: List.generate(
                                              state.total.length, (index) {
                                            return Container(
                                              padding:
                                                  const EdgeInsets.symmetric(
                                                      horizontal: 16),
                                              child: Row(
                                                children: [
                                                  state.total[index].symbol
                                                              .length >
                                                          1
                                                      ? state
                                                                  .total[index]
                                                                  .symbol
                                                                  .length >
                                                              2
                                                          ? Container(
                                                              alignment:
                                                                  Alignment
                                                                      .center,
                                                              width: 20,
                                                              height: 20,
                                                              decoration:
                                                                  BoxDecoration(
                                                                color: Color(
                                                                        0xFFFFC043)
                                                                    .withOpacity(
                                                                        0.3),
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            10),
                                                              ),
                                                              child: Text(
                                                                '${state.total[index].symbol}',
                                                                style: TextStyle(
                                                                    fontSize:
                                                                        8),
                                                              ),
                                                            )
                                                          : Container(
                                                              alignment:
                                                                  Alignment
                                                                      .center,
                                                              width: 20,
                                                              height: 20,
                                                              decoration:
                                                                  BoxDecoration(
                                                                color: Color(
                                                                        0xFFFFC043)
                                                                    .withOpacity(
                                                                        0.3),
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            10),
                                                              ),
                                                              child: Text(
                                                                '${state.total[index].symbol}',
                                                                style: TextStyle(
                                                                    fontSize:
                                                                        12),
                                                              ),
                                                            )
                                                      : Container(
                                                          alignment:
                                                              Alignment.center,
                                                          width: 20,
                                                          height: 20,
                                                          decoration:
                                                              BoxDecoration(
                                                            color: Color(
                                                                    0xFFFFC043)
                                                                .withOpacity(
                                                                    0.3),
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        10),
                                                          ),
                                                          child: Text(
                                                            '${state.total[index].symbol}',
                                                            style: TextStyle(
                                                                fontSize: 14),
                                                          ),
                                                        ),
                                                  SizedBox(
                                                    width: 4,
                                                  ),
                                                  Text(
                                                    '${convertMoney(state.total[index].total)}',
                                                    style: TextStyle(
                                                      fontSize: 18,
                                                      color: colorTextSecondary,
                                                    ),
                                                  )
                                                ],
                                              ),
                                            );
                                          }),
                                        )),
                                  )
                                : Container(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 16),
                                    child: state.total[0].symbol.length > 1
                                        ? state.total[0].symbol.length > 2
                                            ? Row(
                                                children: [
                                                  Container(
                                                    alignment: Alignment.center,
                                                    width: 20,
                                                    height: 20,
                                                    decoration: BoxDecoration(
                                                      color: Color(0xFFFFC043)
                                                          .withOpacity(0.3),
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              10),
                                                    ),
                                                    child: Text(
                                                      '${state.total[0].symbol}',
                                                      style: TextStyle(
                                                          fontSize: 8),
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    width: 4,
                                                  ),
                                                  Text(
                                                    '${convertMoney(state.total[0].total)}',
                                                    style: TextStyle(
                                                      fontSize: 18,
                                                      color: colorTextSecondary,
                                                    ),
                                                  )
                                                ],
                                              )
                                            : Row(
                                                children: [
                                                  Container(
                                                    alignment: Alignment.center,
                                                    width: 20,
                                                    height: 20,
                                                    decoration: BoxDecoration(
                                                      color: Color(0xFFFFC043)
                                                          .withOpacity(0.3),
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              10),
                                                    ),
                                                    child: Text(
                                                      '${state.total[0].symbol}',
                                                      style: TextStyle(
                                                          fontSize: 10),
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    width: 4,
                                                  ),
                                                  Text(
                                                    '${convertMoney(state.total[0].total)}',
                                                    style: TextStyle(
                                                      fontSize: 18,
                                                      color: colorTextSecondary,
                                                    ),
                                                  )
                                                ],
                                              )
                                        : Row(
                                            children: [
                                              Container(
                                                alignment: Alignment.center,
                                                width: 20,
                                                height: 20,
                                                decoration: BoxDecoration(
                                                  color: Color(0xFFFFC043)
                                                      .withOpacity(0.3),
                                                  borderRadius:
                                                      BorderRadius.circular(10),
                                                ),
                                                child: Text(
                                                  '${state.total[0].symbol}',
                                                  style: TextStyle(
                                                      fontSize: fontSizeNormal),
                                                ),
                                              ),
                                              SizedBox(
                                                width: 4,
                                              ),
                                              Text(
                                                '${convertMoney(state.total[0].total)}',
                                                style: TextStyle(
                                                  fontSize: 18,
                                                  color: colorTextSecondary,
                                                ),
                                              )
                                            ],
                                          ),
                                  )
                            : Container(),
                        state.total.length > 1 ? Container() : Spacer(),
                        InkResponse(
                          onTap: () {
                            if (controller.positions.length > 0) {
                              var sizeMax =
                                  controller?.position?.maxScrollExtent;

                              if (sizeMax > size &&
                                  sizeMax >
                                      MediaQuery.of(viewService.context)
                                          .size
                                          .width)
                                controller.jumpTo(size += 100);
                            }
                          },
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: SvgPicture.asset(
                              'assets/icons/ic_chevron_right.svg',
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ],
    ),
  );
}

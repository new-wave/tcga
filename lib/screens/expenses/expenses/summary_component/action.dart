import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/recipients/state.dart';

enum SummaryAction {
  selectCareRecipient,
  handleCareRecipient,
  clearSelectedCareRecipient,
  selectCurrency,
  handleCurrency,
  clearSelectedCurrency,
  selectDateRange,
  handleDateRange,
}

class SummaryActionCreator {
  static Action selectCareRecipient() {
    return const Action(SummaryAction.selectCareRecipient);
  }

  static Action removeSelectedCareRecipient() {
    return const Action(SummaryAction.clearSelectedCareRecipient);
  }

  static Action selectCurrency(String myCurrencyCode) {
    return Action(SummaryAction.selectCurrency, payload: myCurrencyCode);
  }

  static Action removeSelectCurrency() {
    return const Action(SummaryAction.clearSelectedCurrency);
  }

  static Action selectDateRange() {
    return const Action(SummaryAction.selectDateRange);
  }

  static Action handleCareRecipient(RecipientItemState item) {
    return Action(SummaryAction.handleCareRecipient, payload: item);
  }

  static Action handleCurrency(String item) {
    return Action(SummaryAction.handleCurrency, payload: item);
  }

  static Action handleDateRange(DateTime item) {
    return Action(SummaryAction.handleDateRange, payload: item);
  }
}

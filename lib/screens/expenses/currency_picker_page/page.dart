import 'package:fish_redux/fish_redux.dart';

import 'currency_adapter/adapter.dart';
import 'effect.dart';
import 'reducer.dart';
import 'state.dart';
import 'view.dart';

class CurrencyPickerPage
    extends Page<CurrencyPickerState, Map<String, dynamic>> {
  CurrencyPickerPage()
      : super(
          initState: initState,
          effect: buildEffect(),
          reducer: buildReducer(),
          view: buildView,
          dependencies: Dependencies<CurrencyPickerState>(
              adapter: NoneConn<CurrencyPickerState>() + CurrencyAdapter(),
              slots: <String, Dependent<CurrencyPickerState>>{}),
          middleware: <Middleware<CurrencyPickerState>>[],
        );
}

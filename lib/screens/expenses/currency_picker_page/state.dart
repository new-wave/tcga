import 'package:fish_redux/fish_redux.dart';

import 'currency_item_component/state.dart';

class CurrencyPickerState extends MutableSource
    implements Cloneable<CurrencyPickerState> {
  List<CurrencyItemState> items;
  String myCurrencyCode;
  bool confirmRequired;

  @override
  CurrencyPickerState clone() {
    return CurrencyPickerState()
      ..items = items
      ..myCurrencyCode = myCurrencyCode
      ..confirmRequired = confirmRequired;
  }

  @override
  Object getItemData(int index) {
    return items[index];
  }

  @override
  String getItemType(int index) {
    return 'currency-item';
  }

  @override
  int get itemCount => items?.length ?? 0;

  @override
  void setItemData(int index, Object data) {
    items[index] = data;
  }
}

CurrencyPickerState initState(Map<String, dynamic> args) {
  final myCurrencyCode = args != null ? args['myCurrencyCode'] as String : null;
  return CurrencyPickerState()
    ..items = []
    ..myCurrencyCode = myCurrencyCode
    ..confirmRequired = false;
}

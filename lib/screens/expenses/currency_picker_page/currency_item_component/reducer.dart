import 'package:fish_redux/fish_redux.dart';

import 'action.dart';
import 'state.dart';

Reducer<CurrencyItemState> buildReducer() {
  return asReducer(
    <Object, Reducer<CurrencyItemState>>{
      CurrencyItemAction.action: _onAction,
    },
  );
}

CurrencyItemState _onAction(CurrencyItemState state, Action action) {
  final CurrencyItemState newState = state.clone();
  return newState;
}

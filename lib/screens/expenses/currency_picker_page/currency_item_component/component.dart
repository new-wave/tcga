import 'package:fish_redux/fish_redux.dart';

import 'effect.dart';
import 'reducer.dart';
import 'state.dart';
import 'view.dart';

class CurrencyItemComponent extends Component<CurrencyItemState> {
  CurrencyItemComponent()
      : super(
            effect: buildEffect(),
            reducer: buildReducer(),
            view: buildView,
            dependencies: Dependencies<CurrencyItemState>(
                adapter: null,
                slots: <String, Dependent<CurrencyItemState>>{
                }),);

}

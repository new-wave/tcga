import 'package:fish_redux/fish_redux.dart';
import 'action.dart';
import 'state.dart';

Effect<CurrencyItemState> buildEffect() {
  return combineEffects(<Object, Effect<CurrencyItemState>>{
    CurrencyItemAction.action: _onAction,
  });
}

void _onAction(Action action, Context<CurrencyItemState> ctx) {
}

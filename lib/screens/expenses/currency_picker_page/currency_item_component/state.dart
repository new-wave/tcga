import 'dart:convert';

import 'package:fish_redux/fish_redux.dart';

class CurrencyItemState implements Cloneable<CurrencyItemState> {
  CurrencyItemState({
    this.symbol,
    this.name,
    this.symbolNative,
    this.decimalDigits,
    this.rounding,
    this.code,
    this.namePlural,
    this.total,
  });

  String symbol;
  String name;
  String symbolNative;
  int decimalDigits;
  double rounding;
  String code;
  String namePlural;
  double total;

  factory CurrencyItemState.fromRawJson(String str) =>
      CurrencyItemState.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory CurrencyItemState.fromJson(Map<String, dynamic> json) {
    final rounding = json["rounding"];
    return CurrencyItemState(
      symbol: json["symbol"],
      name: json["name"],
      symbolNative: json["symbol_native"],
      decimalDigits: json["decimal_digits"],
      rounding: rounding is int ? rounding.toDouble() : rounding,
      code: json["code"],
      namePlural: json["name_plural"],
      total: 0,
    );
  }

  Map<String, dynamic> toJson() => {
        "symbol": symbol,
        "name": name,
        "symbol_native": symbolNative,
        "decimal_digits": decimalDigits,
        "rounding": rounding,
        "code": code,
        "name_plural": namePlural,
      };

  @override
  CurrencyItemState clone() {
    return CurrencyItemState()
      ..code = code
      ..decimalDigits = decimalDigits
      ..name = name
      ..namePlural = namePlural
      ..rounding = rounding
      ..symbol = symbol
      ..symbolNative = symbolNative
      ..total = total;
  }
}

CurrencyItemState initState(Map<String, dynamic> args) {
  return CurrencyItemState();
}

import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';

import 'action.dart';
import 'state.dart';

Widget buildView(
    CurrencyItemState state, Dispatch dispatch, ViewService viewService) {
  return Container(
    padding: const EdgeInsets.symmetric(
      horizontal: 16,
    ),
    height: 40,
    child: Row(
      children: [
        Text(state.code),
        SizedBox(
          width: 8,
        ),
        Text(
          state.name,
          overflow: TextOverflow.ellipsis,
        ),
        SizedBox(
          width: 4,
        ),
        Text('('),
        Text(state.symbol),
        Text(')'),
      ],
    ),
  );
}

import 'package:fish_redux/fish_redux.dart';

import 'action.dart';
import 'currency_item_component/state.dart';
import 'state.dart';

Reducer<CurrencyPickerState> buildReducer() {
  return asReducer(
    <Object, Reducer<CurrencyPickerState>>{
      CurrencyPickerAction.initialize: _initialize,
    },
  );
}

CurrencyPickerState _initialize(CurrencyPickerState state, Action action) {
  final CurrencyPickerState newState = state.clone();
  List<CurrencyItemState> items = action.payload;
  newState.items = items ?? [];
  return newState;
}

import 'package:fish_redux/fish_redux.dart';
import 'currency_item_component/state.dart';

enum CurrencyPickerAction {
  initialize,
  confirm,
  search,
}

class CurrencyPickerActionCreator {
  static Action initialize(List<CurrencyItemState> items) {
    return Action(CurrencyPickerAction.initialize, payload: items);
  }

  static Action confirm([CurrencyItemState item]) {
    return Action(CurrencyPickerAction.confirm, payload: item);
  }

  static Action search(String keyword) {
    return Action(CurrencyPickerAction.search, payload: keyword);
  }
}

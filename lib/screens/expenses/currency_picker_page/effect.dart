import 'package:flutter/material.dart' hide Action;
import 'package:fish_redux/fish_redux.dart';
import 'action.dart';
import 'currency_item_component/state.dart';
import 'manager.dart';
import 'state.dart';

final _currencyManager = CurrencyManager();

Effect<CurrencyPickerState> buildEffect() {
  return combineEffects(<Object, Effect<CurrencyPickerState>>{
    Lifecycle.initState: _init,
    CurrencyPickerAction.search: _search,
    CurrencyPickerAction.confirm: _confirm,
  });
}

Future _init(Action action, Context<CurrencyPickerState> ctx) async {
  final items = await _currencyManager.query(
    ctx.context,
    myCurrencyCode: ctx.state.myCurrencyCode,
  );

  ctx.dispatch(CurrencyPickerActionCreator.initialize(items));
}

Future _search(Action action, Context<CurrencyPickerState> ctx) async {
  final keyword = action.payload as String ?? '';

  final items = await _currencyManager.query(
    ctx.context,
    keyword: keyword,
    myCurrencyCode: ctx.state.myCurrencyCode,
  );

  ctx.dispatch(CurrencyPickerActionCreator.initialize(items));
}

Future _confirm(Action action, Context<CurrencyPickerState> ctx) async {
  final selectedItem = action.payload as CurrencyItemState;

  if (selectedItem == null) return;

  Navigator.of(ctx.context).pop(selectedItem);
}

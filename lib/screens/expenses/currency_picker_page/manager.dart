import 'dart:convert';

import 'package:flutter/material.dart';

import 'currency_item_component/state.dart';

class CurrencyManager {
  static final _singleton = CurrencyManager._internal();

  factory CurrencyManager() {
    return _singleton;
  }

  CurrencyManager._internal();

  List<CurrencyItemState> items;

  Future<List<CurrencyItemState>> query(
    BuildContext context, {
    String keyword,
    String myCurrencyCode,
  }) async {
    if (items == null || items.length == 0) {
      var jsonString = await DefaultAssetBundle.of(
        context,
      ).loadString(
        'assets/json/currency.json',
      );
      final data = json.decode(jsonString) as List;

      items = data
          .map(
            (item) => CurrencyItemState.fromJson(item),
          )
          .toList();
    }

    List<CurrencyItemState> myItems = [];
    if (myCurrencyCode != null) {
      for (CurrencyItemState item in items) {
        if (myCurrencyCode.contains(item.code)) {
          myItems.add(item);
        }
      }
    } else {
      myItems = items;
    }

    keyword = (keyword ?? '').trim();
    if (keyword.isEmpty) return myItems;
    return myItems
        .where(
          (x) => x.name.contains(keyword),
        )
        .toList();
  }
}

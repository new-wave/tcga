import 'package:fish_redux/fish_redux.dart';
import '../state.dart';
import 'action.dart';

Effect<CurrencyPickerState> buildEffect() {
  return combineEffects(<Object, Effect<CurrencyPickerState>>{});
}

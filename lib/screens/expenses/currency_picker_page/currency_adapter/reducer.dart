import 'package:fish_redux/fish_redux.dart';

import '../state.dart';
import 'action.dart';

Reducer<CurrencyPickerState> buildReducer() {
  return asReducer(
    <Object, Reducer<CurrencyPickerState>>{},
  );
}

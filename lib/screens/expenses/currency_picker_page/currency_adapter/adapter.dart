import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/widgets.dart';

import '../currency_item_component/component.dart';

import '../state.dart';
import 'effect.dart';
import 'reducer.dart';

class CurrencyAdapter extends SourceFlowAdapter<CurrencyPickerState> {
  CurrencyAdapter()
      : super(
          pool: <String, Component<Object>>{
            'currency-item': CurrencyItemComponent(),
          },
          effect: buildEffect(),
          reducer: buildReducer(),
        );
}

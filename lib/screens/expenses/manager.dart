import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:image/image.dart' as Img;
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';

import 'package:tcga_flutter/database/models.dart';
import 'package:tcga_flutter/database/repository.dart';

import 'expense_editor/add_image_component/image_item_component/state.dart';
import 'expense_editor/state.dart';
import 'expenses/history_tab_component/expense_item_component/state.dart';
import 'mapper.dart';

class ExpenseManager {
  static final _singleton = ExpenseManager._internal();

  factory ExpenseManager() {
    return _singleton;
  }

  ExpenseManager._internal();

  final _expenseRepository = Repository<Expense>();
  final _personRepository = Repository<Person>();

  Future<List<ExpenseItemState>> query() async {
    var expenses = await _expenseRepository.query();

    var items = <ExpenseItemState>[];

    for (int i = 0; i < expenses.length; i++) {
      final entity = expenses[i];
      final personEntity = await _personRepository.get(
        entity.personId,
      );

      items.add(
        entity.toState()
          ..firstName = personEntity.firstName
          ..lastName = personEntity.lastName,
      );
    }
    return items;
  }

  Future<ExpenseEditorState> fill(ExpenseEditorState item) async {
    return item;
  }

  Future<ExpenseItemState> create(ExpenseEditorState item) async {
    final entity = item.toEntity();

    item.id = await _expenseRepository.create(entity);

    return entity.toState()
      ..firstName = item.recipient.firstName
      ..lastName = item.recipient.lastName;
  }

  Future<ExpenseItemState> update(ExpenseEditorState item) async {
    final entity = item.toEntity();
    final updated = await _expenseRepository.update(entity);

    if (updated) {
      return entity.toState()
        ..firstName = item.recipient.firstName
        ..lastName = item.recipient.lastName;
    }

    return null;
  }

  Future<bool> delete(id) {
    return _expenseRepository.delete(id);
  }

  Future<ImageItemState> loadImage(ImageSource imageSource) async {
    ImagePicker _imagePicker = ImagePicker();
    PickedFile file = await _imagePicker.getImage(source: imageSource);

    File mFile;
    if (null != file) {
      Directory directory = await getTemporaryDirectory();
      Map map = Map();
      map['path'] = file.path;
      map['directory'] = directory;
      mFile = await compute(saveImageToDisk, map);
      return ImageItemState(
        imageFile: mFile,
        url: mFile.path,
      );
    } else {
      return null;
    }
  }
}

Future<File> saveImageToDisk(Map map) async {
  try {
    String path = map['path'];
    Directory directory = map['directory'];
    File tempFile = File(path);
    Img.Image image = Img.decodeImage(tempFile.readAsBytesSync());
    Img.Image mImage = Img.copyResize(image, width: 512);
    String imgType = path.split('.').last;
    String mPath =
        '${directory.path.toString()}/image_${DateTime.now()}.$imgType';
    File dFile = File(mPath);
    if (imgType == 'jpg' || imgType == 'jpeg') {
      dFile.writeAsBytesSync(Img.encodeJpg(mImage));
    } else {
      dFile.writeAsBytesSync(Img.encodePng(mImage));
    }
    return dFile;
  } catch (e) {
    return null;
  }
}

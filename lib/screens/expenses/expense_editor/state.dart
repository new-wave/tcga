import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:tcga_flutter/database/models/enums.dart';
import 'package:tcga_flutter/screens/expenses/currency_picker_page/currency_item_component/state.dart';
import 'package:tcga_flutter/screens/expenses/expense_editor/add_image_component/image_item_component/state.dart';
import 'package:tcga_flutter/screens/expenses/expense_editor/add_image_component/state.dart';
import 'package:tcga_flutter/screens/expenses/expenses/history_tab_component/expense_item_component/state.dart';
import 'package:tcga_flutter/screens/recipients/recipient_picker_component/state.dart';
import 'package:tcga_flutter/screens/recipients/state.dart';

class ExpenseEditorState implements Cloneable<ExpenseEditorState> {
  String id;

  //String get title => onEditMode ? 'Edit Expense' : 'New Expense';

  bool get onEditMode => id != null;

  static final expenseTypes = {
    ExpenseCategory.Food: 'Food',
    ExpenseCategory.Parking: 'Parking',
    ExpenseCategory.Transportation: 'Transportation',
    ExpenseCategory.Other: 'Other',
  };

  static final expenseStatuses = {
    ExpenseStatus.Pending: 'Pending',
    ExpenseStatus.Invoiced: 'Invoiced',
    ExpenseStatus.Paid: 'Paid',
  };

  TextEditingController expenseTitlEeditingController;
  TextEditingController amountEditingController;
  TextEditingController currencyCodeEditingController;

  DateTime recordedOn;
  ExpenseStatus status;
  ExpenseCategory category;

  RecipientItemState recipient;
  CurrencyItemState currency;

  List<ImageItemState> photos;

  bool isCheckField;

  @override
  ExpenseEditorState clone() {
    return ExpenseEditorState()
      ..id = id
      ..expenseTitlEeditingController = expenseTitlEeditingController
      ..category = category
      ..recipient = recipient
      ..amountEditingController = amountEditingController
      ..currencyCodeEditingController = currencyCodeEditingController
      ..currency = currency
      ..photos = photos
      ..recordedOn = recordedOn
      ..status = status
      ..isCheckField = isCheckField;
  }
}

ExpenseEditorState initState(Map<String, dynamic> args) {
  final item = args['item'] as ExpenseItemState;
  final recordedOn = DateTime.now();

  var state = ExpenseEditorState()
    ..id = item?.id
    ..photos = item?.photos ?? []
    ..recordedOn = item?.recordedOn ?? recordedOn
    ..expenseTitlEeditingController = TextEditingController(
      text: item?.title ?? '',
    )
    ..currencyCodeEditingController = TextEditingController(
      text: item?.currencyCode ?? '',
    )
    ..amountEditingController = TextEditingController(
      text: item != null ? '${item.amount}' : '',
    )
    ..currency = CurrencyItemState(
      code: item?.currencyCode ?? 'USD',
      symbol: item?.currencyCode ?? '\$',
    )
    ..recipient = item != null
        ? RecipientItemState(
            id: item.personId,
            firstName: item.firstName,
            lastName: item.lastName,
          )
        : null
    ..category = item?.category
    ..isCheckField = false
    ..status = item?.status ?? ExpenseStatus.Pending;

  state.currencyCodeEditingController.text = state.currency.code;

  return state;
}

class AddImagesStateConnector
    extends ConnOp<ExpenseEditorState, AddImageState> {
  @override
  AddImageState get(ExpenseEditorState state) =>
      AddImageState()..items = state.photos;

  @override
  void set(ExpenseEditorState state, AddImageState subState) =>
      state.photos = subState.items
          .map(
            (e) => e.clone(),
          )
          .toList();
}

class RecipientPickerStateConnector
    extends ConnOp<ExpenseEditorState, RecipientPickerState> {
  @override
  RecipientPickerState get(ExpenseEditorState state) => RecipientPickerState(
        item: state.recipient,
        isCheckField: state.isCheckField,
      );

  @override
  void set(ExpenseEditorState state, RecipientPickerState subState) {
    state.recipient = subState.item;
    state.isCheckField = subState.isCheckField;
  }
}

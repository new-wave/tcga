import 'dart:io';

import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/widget/import.dart';
import 'package:tcga_flutter/widget/selectable_options_widget.dart';

import 'action.dart';
import 'state.dart';
import 'validator.dart';

Widget buildView(
    ExpenseEditorState state, Dispatch dispatch, ViewService viewService) {
  return Builder(builder: (context) {
    double height = ((Platform.isIOS)
            ? MediaQuery.of(viewService.context).padding.bottom / 2
            : 0) +
        45.0;
    return Padding(
      padding: EdgeInsets.only(
        bottom: MediaQuery.of(context).viewInsets.bottom,
      ),
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        //resizeToAvoidBottomPadding: false,
        backgroundColor: Colors.white,
        appBar: _appBar(
          viewService,
          state,
          delete: () {
            dispatch(
              ExpenseEditorActionCreator.delete(),
            );
          },
        ),
        body: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 16,
                  vertical: 24,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    EntryWidget(
                      controller: state.expenseTitlEeditingController,
                      title: AppLocalizations.of(viewService.context)
                          .text('expenses.editor.placeholder_title'),
                      keyboardType: TextInputType.text,
                      warningText: AppLocalizations.instance
                          .text('common.validator.expense_title'),
                      isRequired: true,
                      isCheckField: state.isCheckField ?? false,
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    PickerWidget(
                      items: ExpenseEditorState.expenseTypes.values.toList(),
                      initialIndex: state.category?.index,
                      title: AppLocalizations.of(viewService.context)
                          .text('expenses.editor.placeholder_type'),
                      suffixIcon: SvgPicture.asset(
                        'assets/icons/ic_chart_horizontal.svg',
                      ),
                      indexSelected: (index) {
                        dispatch(
                          ExpenseEditorActionCreator
                              .handleExpenseCategorySelected(
                            index,
                          ),
                        );
                      },
                      warningText: AppLocalizations.instance
                          .text('common.validator.expense_type'),
                      isRequired: true,
                      isCheckField: state.isCheckField ?? false,
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    viewService.buildComponent(
                      'recipient-picker',
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    SizedBox(
                      height: 36,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Expanded(
                            flex: 5,
                            child: DatePickerWidget(
                              initial: state.recordedOn,
                              min: DateTime.now().add(Duration(days: -30)),
                              max: DateTime.now(),
                              title: AppLocalizations.of(viewService.context)
                                  .text('expenses.editor.date'),
                              //date
                              suffixIcon: SvgPicture.asset(
                                'assets/icons/ic_date.svg',
                              ),
                              dateSelected: (date) {
                                dispatch(ExpenseEditorActionCreator
                                    .handleDateSelected(
                                  date,
                                ));
                              },
                            ),
                          ),
                          const SizedBox(
                            width: 16,
                          ),
                          Expanded(
                            flex: 4,
                            child: TimePickerWidget(
                              initial: state.recordedOn,
                              title: AppLocalizations.of(viewService.context)
                                  .text('expenses.editor.time'), //time
                              suffixIcon: SvgPicture.asset(
                                'assets/icons/ic_date.svg',
                              ),
                              timeSelected: (timeOfDay) {
                                dispatch(ExpenseEditorActionCreator
                                    .handleTimeSelected(
                                  timeOfDay,
                                ));
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    Text(
                      AppLocalizations.of(viewService.context)
                          .text('expenses.editor.status'),
                      style: TextStyle(
                        fontSize: 14,
                      ),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    SelectableOptionsWidget(
                      options:
                          ExpenseEditorState.expenseStatuses.values.toList(),
                      initialIndex: state.status?.index,
                      indexSelected: (index) => dispatch(
                        ExpenseEditorActionCreator.handleExpenseStatusSelected(
                          index,
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Expanded(
                          flex: 5,
                          child: EntryWidget(
                            controller: state.amountEditingController,
                            title: AppLocalizations.of(viewService.context)
                                .text('expenses.editor.USD'),
                            keyboardType: TextInputType.number,
                            prefixIcon: Text(state.currency.code),
                            warningText: AppLocalizations.instance
                                .text('common.validator.amount_money'),
                            isRequired: true,
                            isCheckField: state.isCheckField ?? false,
                          ),
                        ),
                        const SizedBox(
                          width: 16,
                        ),
                        Expanded(
                          flex: 3,
                          child: PickerWidget(
                            controller: state.currencyCodeEditingController,
                            title: AppLocalizations.of(viewService.context)
                                .text('expenses.editor.currency'),
                            suffixIcon: SvgPicture.asset(
                              'assets/icons/ic_caret.svg',
                            ),
                            tap: () => dispatch(
                              ExpenseEditorActionCreator.selectCurrency(
                                state.currency,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    Container(
                      height: 75,
                      child: viewService.buildComponent('add-images-component'),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        bottomNavigationBar: Builder(
          builder: (context) => Container(
            height: height,
            child: Container(
              decoration: BoxDecoration(
                  color: colorPrimary, border: Border.all(color: colorPrimary)),
              child: ButtonWidget(
                colorButton: colorPrimary,
                textButton: AppLocalizations.of(viewService.context)
                    .text('common.save'),
                onPress: () {
                  dispatch(
                    ExpenseEditorActionCreator.save(),
                  );
                },
              ),
            ),
          ),
        ),
      ),
    );
  });
}

AppBar _appBar(ViewService viewService, ExpenseEditorState state,
    {Function delete}) {
  return AppBar(
    elevation: 3,
    leading: IconButton(
      iconSize: 24,
      icon: SvgPicture.asset('assets/icons/ic_arrow_left_circle.svg'),
      onPressed: () {
        Navigator.pop(viewService.context);
      },
    ),
    title: Text(state.onEditMode
        ? AppLocalizations.of(viewService.context)
            .text('expenses.editor.header.edit')
        : AppLocalizations.of(viewService.context)
            .text('expenses.editor.header.create')),
    actions: [
      if (state.onEditMode)
        IconButton(
          iconSize: 24,
          icon: SvgPicture.asset('assets/icons/ic_trash.svg'),
          onPressed: delete,
        )
    ],
  );
}

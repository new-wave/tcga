import 'package:tcga_flutter/utils/app_localizations.dart';

import 'state.dart';

class Validator {
  static bool isValid(ExpenseEditorState state) {
    if (state.expenseTitlEeditingController.text == null ||
        state.expenseTitlEeditingController.text.isEmpty) return false;

    if (state.category == null) return false;

    if (state.recipient == null) return false;

    if (state.status == null) return false;

    if (double.tryParse(
          state.amountEditingController.text,
        ) ==
        null) return false;

    return true;
  }
}

import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart' hide Action, Page;
import 'package:tcga_flutter/screens/recipients/state.dart';

import '../currency_picker_page/currency_item_component/state.dart';

import 'action.dart';
import 'state.dart';

Reducer<ExpenseEditorState> buildReducer() {
  return asReducer(
    <Object, Reducer<ExpenseEditorState>>{
      ExpenseEditorAction.handleExpenseCategorySelected:
          _handleExpenseCategorySelected,
      ExpenseEditorAction.handleDateSelected: _handleDateSelected,
      ExpenseEditorAction.handleTimeSelected: _handleTimeSelected,
      ExpenseEditorAction.handleCurrencySelected: _handleCurrencySelected,
      ExpenseEditorAction.handleExpenseStatusSelected:
          _handleExpenseStatusSelected,
      ExpenseEditorAction.initialize: _initialize,
      ExpenseEditorAction.checkField: _checkFieldReducer,
    },
  );
}

ExpenseEditorState _initialize(
  ExpenseEditorState state,
  Action action,
) {
  return action.payload.clone();
}

ExpenseEditorState _checkFieldReducer(ExpenseEditorState state, Action action) {
  final ExpenseEditorState newState = state.clone();
  newState.isCheckField = true;
  return newState;
}

ExpenseEditorState _handleExpenseCategorySelected(
  ExpenseEditorState state,
  Action action,
) {
  final ExpenseEditorState newState = state.clone();

  newState.category = ExpenseEditorState.expenseTypes.keys.elementAt(
    action.payload,
  );

  return newState;
}

ExpenseEditorState _handleExpenseStatusSelected(
  ExpenseEditorState state,
  Action action,
) {
  final ExpenseEditorState newState = state.clone();

  newState.status = ExpenseEditorState.expenseStatuses.keys.elementAt(
    action.payload,
  );

  return newState;
}

ExpenseEditorState _handleCurrencySelected(
  ExpenseEditorState state,
  Action action,
) {
  final newState = state.clone();
  final selectedItem = action.payload as CurrencyItemState;

  if (selectedItem == null) return state;

  newState.currency = selectedItem;
  newState.currencyCodeEditingController.text = selectedItem.code;

  return newState;
}

ExpenseEditorState _handleDateSelected(
  ExpenseEditorState state,
  Action action,
) {
  final ExpenseEditorState newState = state.clone();

  final selectedDate = action.payload as DateTime;

  newState.recordedOn = DateTime(
    selectedDate.year,
    selectedDate.month,
    selectedDate.day,
    newState.recordedOn.hour,
    newState.recordedOn.minute,
    newState.recordedOn.second,
  );

  return newState;
}

ExpenseEditorState _handleTimeSelected(
  ExpenseEditorState state,
  Action action,
) {
  final ExpenseEditorState newState = state.clone();

  final selectedTime = action.payload as TimeOfDay;

  newState.recordedOn = DateTime(
    newState.recordedOn.year,
    newState.recordedOn.month,
    newState.recordedOn.day,
    selectedTime.hour,
    selectedTime.minute,
    0,
  );

  return newState;
}

import 'dart:io';

import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/expenses/expense_editor/add_image_component/image_item_component/state.dart';

import 'action.dart';
import 'state.dart';

Reducer<AddImageState> buildReducer() {
  return asReducer(
    <Object, Reducer<AddImageState>>{
      AddImageAction.handleCreate: _handleCreate,
      AddImageAction.handleDelete: _handleDelete,
    },
  );
}

AddImageState _handleCreate(AddImageState state, Action action) {
  final AddImageState newState = state.clone();

  final item = action.payload as ImageItemState;

  newState.items.add(item);

  return newState;
}

AddImageState _handleDelete(AddImageState state, Action action) {
  // final AddImageState newState = state.clone();
  //
  // final item = action.payload as ImageItemState;

  final newState = state.clone();

  final item = action.payload;

  final deleteIndex = newState.items.indexWhere((x) => x.imageFile == item);

  newState.items.removeAt(deleteIndex);

  //newState.items.remove(item);

  return newState;
}

// final newState = state.clone();
//
// final item = action.payload;
//
// final deleteIndex = newState.items.indexWhere((x) => x.imageFile == item);
//
// newState.items.removeAt(deleteIndex);

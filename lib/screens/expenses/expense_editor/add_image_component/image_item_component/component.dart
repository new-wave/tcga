import 'package:fish_redux/fish_redux.dart';

import 'state.dart';
import 'view.dart';

class ImageItemComponent extends Component<ImageItemState> {
  ImageItemComponent()
      : super(
            view: buildView,
            dependencies: Dependencies<ImageItemState>(
                adapter: null,
                slots: <String, Dependent<ImageItemState>>{
                }),);

}

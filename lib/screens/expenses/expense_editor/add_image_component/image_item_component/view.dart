import 'dart:io';

import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/screens/expenses/expense_editor/add_image_component/action.dart';

import 'state.dart';

Widget buildView(
    ImageItemState state, Dispatch dispatch, ViewService viewService) {
  return Row(
    children: [
      SizedBox(
        height: 70,
        width: 70,
        child: Stack(
          children: <Widget>[
            Container(
              height: 70,
              width: 70,
              clipBehavior: Clip.hardEdge,
              padding: EdgeInsets.all(2),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                border: Border.all(
                  color: Colors.grey,
                  width: 1,
                ),
              ),
              child: Image.file(
                File(state.url),
                filterQuality: FilterQuality.high,
                fit: BoxFit.cover,
              ),
            ),
            Positioned(
              right: 0,
              top: 0,
              child: InkWell(
                onTap: () {
                  dispatch(AddImageActionCreator.handleDelete(item: state.imageFile));
                },
                child: Container(
                  height: 30,
                  width: 30,
                  child: SvgPicture.asset(
                    'assets/icons/ic_delete_image.svg',
                    height: 24,
                    width: 24,
                    color: colorPrimary,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
      SizedBox(
        width: 16,
      ),
    ],
  );
}

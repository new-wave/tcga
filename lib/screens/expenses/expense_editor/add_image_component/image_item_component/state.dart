import 'dart:io';

import 'package:fish_redux/fish_redux.dart';

class ImageItemState implements Cloneable<ImageItemState> {
  File imageFile;
  String url;

  ImageItemState({
    this.imageFile,
    this.url,
  });

  @override
  ImageItemState clone() {
    return ImageItemState()
      ..imageFile = imageFile
      ..url = url;
  }
}

ImageItemState initState(Map<String, dynamic> args) {
  return ImageItemState();
}

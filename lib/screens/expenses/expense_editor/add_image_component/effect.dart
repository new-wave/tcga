import 'dart:io';

import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart' hide Action, Page;
import 'package:image_picker/image_picker.dart';
import 'package:tcga_flutter/screens/expenses/expense_editor/add_image_component/image_item_component/state.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';

import '../../manager.dart';
import 'action.dart';
import 'state.dart';

final _manager = ExpenseManager();

Effect<AddImageState> buildEffect() {
  return combineEffects(<Object, Effect<AddImageState>>{
    AddImageAction.create: _create,
    AddImageAction.delete: _delete,
    AddImageAction.showPopup: _showPopup,
  });
}

void _showPopup(Action action, Context<AddImageState> ctx) {}

void _create(Action action, Context<AddImageState> ctx) async {
  await showModalBottomSheet(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
      context: ctx.context,
      builder: (context) {
        return Container(
          padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
          height: 120,
          child: Column(
            children: <Widget>[
              GestureDetector(
                onTap: () async {
                  Navigator.pop(ctx.context);
                  final file = await _manager.loadImage(ImageSource.camera);
                  if (file != null) {
                    ctx.dispatch(
                        AddImageActionCreator.handleCreate(item: file));
                  }
                },
                child: Container(
                  height: 36,
                  width: double.infinity,
                  child: Text(
                    AppLocalizations.of(ctx.context)
                        .text('expense.editor.camera'),
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 8,
              ),
              GestureDetector(
                onTap: () async {
                  Navigator.pop(ctx.context);
                  final file = await _manager.loadImage(ImageSource.gallery);
                  if (file != null) {
                    ctx.dispatch(
                        AddImageActionCreator.handleCreate(item: file));
                  }
                },
                child: Container(
                  height: 36,
                  width: double.infinity,
                  child: Text(
                    AppLocalizations.of(ctx.context)
                        .text('expense.editor.gallery'),
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      });
}

void _delete(Action action, Context<AddImageState> ctx) {}

import 'package:fish_redux/fish_redux.dart';
import '../image_item_component/component.dart';
import '../state.dart';

class AddImageAdapter extends SourceFlowAdapter<AddImageState> {
  AddImageAdapter()
      : super(
          pool: <String, Component<Object>>{
            'image-item': ImageItemComponent(),
          },
        );
}

import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:tcga_flutter/config/palette.dart';

import 'action.dart';
import 'state.dart';

Widget buildView(
    AddImageState state, Dispatch dispatch, ViewService viewService) {
  final adapter = viewService.buildAdapter();
  return SingleChildScrollView(
    scrollDirection: Axis.horizontal,
    child: Container(
      height: 70,
      child: Row(
        children: <Widget>[
          GestureDetector(
            onTap: () {
              dispatch(AddImageActionCreator.create());
            },
            child: SvgPicture.asset(
              "assets/icons/ic_choose_image.svg",
              height: 70,
              width: 70,
            ),
          ),
          SizedBox(
            width: 16,
          ),
          Container(
            height: 75,
            child: Row(
              children: <Widget>[
                ...List.generate(
                  adapter.itemCount,
                  (index) => adapter.itemBuilder(viewService.context, index),
                )
              ],
            ),
          ),
        ],
      ),
    ),
  );
}

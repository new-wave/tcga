import 'dart:io';

import 'package:fish_redux/fish_redux.dart';

import 'image_item_component/state.dart';

class AddImageState extends MutableSource implements Cloneable<AddImageState> {
  List<ImageItemState> items;

  @override
  AddImageState clone() {
    return AddImageState()
      ..items = items
          .map(
            (e) => e.clone(),
          )
          .toList();
  }

  @override
  Object getItemData(int index) => items[index];

  @override
  String getItemType(int index) {
    return 'image-item';
  }

  @override
  int get itemCount => items.length ?? 0;

  @override
  void setItemData(int index, Object data) => items[index] = data;
}

AddImageState initState(Map<String, dynamic> args) {
  return AddImageState()..items = [];
}

import 'package:fish_redux/fish_redux.dart';

import 'add_image_adapter/adapter.dart';
import 'effect.dart';
import 'reducer.dart';
import 'state.dart';
import 'view.dart';

class AddImageComponent extends Component<AddImageState> {
  AddImageComponent()
      : super(
            effect: buildEffect(),
            reducer: buildReducer(),
            view: buildView,
            dependencies: Dependencies<AddImageState>(
                adapter: NoneConn<AddImageState>() + AddImageAdapter(),
                slots: <String, Dependent<AddImageState>>{}));
}

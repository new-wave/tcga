import 'dart:io';

import 'package:fish_redux/fish_redux.dart';

import 'image_item_component/state.dart';

//TODO replace with your own action
enum AddImageAction {
  create,
  handleCreate,
  showPopup,
  delete,
  handleDelete,
}

class AddImageActionCreator {
  static Action create({ImageItemState item}) {
    return Action(
      AddImageAction.create,
      payload: item,
    );
  }

  static Action handleCreate({ImageItemState item}) {
    return Action(
      AddImageAction.handleCreate,
      payload: item,
    );
  }

  static Action showPopup() {
    return Action(
      AddImageAction.showPopup,
    );
  }

  static Action delete({ImageItemState item}) {
    return Action(
      AddImageAction.delete,
      payload: item,
    );
  }

  static Action handleDelete({File item}) {
    return Action(
      AddImageAction.handleDelete,
      payload: item,
    );
  }
}

import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart' hide Action, Page;
import 'package:tcga_flutter/route_name.dart';
import 'package:tcga_flutter/screens/expenses/manager.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/utils/show_picker.dart';

import '../currency_picker_page/currency_item_component/state.dart';
import '../../../utils/utils.dart';
import 'validator.dart';
import 'action.dart';
import 'state.dart';

final _manager = ExpenseManager();

Effect<ExpenseEditorState> buildEffect() {
  return combineEffects(<Object, Effect<ExpenseEditorState>>{
    Lifecycle.initState: _init,
    ExpenseEditorAction.selectCurrency: _selectCurrency,
    ExpenseEditorAction.save: _save,
    ExpenseEditorAction.unFocus: _unFocus,
    ExpenseEditorAction.delete: _delete,
  });
}

Future _init(
  Action action,
  Context<ExpenseEditorState> ctx,
) async {
  if (ctx.state.onEditMode) {
    final newState = await _manager.fill(ctx.state);

    ctx.dispatch(
      ExpenseEditorActionCreator.initialize(
        newState,
      ),
    );
  }
}

void _unFocus(Action action, Context<ExpenseEditorState> ctx) {
  FocusScopeNode currentFocus = FocusScope.of(ctx.context);
  if (!currentFocus.hasPrimaryFocus) {
    currentFocus.unfocus();
  }
}

Future _save(
  Action action,
  Context<ExpenseEditorState> ctx,
) async {
  final state = ctx.state;
  bool isValid = Validator.isValid(state);
  if (!isValid) {
    ctx.dispatch(ExpenseEditorActionCreator.checkField());
    return;
  }

  final result = state.onEditMode
      ? await _manager.update(state)
      : await _manager.create(state);

  if (result == null) return;
  result.firstName = ctx.state.recipient.firstName;
  result.lastName = ctx.state.recipient.lastName;
  ctx.goBack(result);
  return;
}

Future _delete(
  Action action,
  Context<ExpenseEditorState> ctx,
) async {
  final confirmed = await ctx.confirm(
    title: AppLocalizations.instance.text('common.warning.delete.title'),
    message: AppLocalizations.instance.text('common.warning.delete.desc'),
    negativeActionTitle: AppLocalizations.instance.text('common.cancel'),
    positiveActionTitle: AppLocalizations.instance.text('common.sure'),
  );

  if (!confirmed) return;

  final deletedItemId = ctx.state.id;
  final deleted = await _manager.delete(deletedItemId);

  if (!deleted) {
    // TODO show error message
    return;
  }
  ctx.goBack(
    {
      'deleted-item-id': deletedItemId,
    },
  );
}

Future _selectCurrency(
  Action action,
  Context<ExpenseEditorState> ctx,
) async {
  CurrencyItemState selectedCurrency = await showModal(
    ctx.context,
    RouteName.currencyPicker,
  );

  if (selectedCurrency == null) return;

  ctx.dispatch(
    ExpenseEditorActionCreator.handleCurrencySelected(
      selectedCurrency,
    ),
  );
}

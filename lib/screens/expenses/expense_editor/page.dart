import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/recipients/recipient_picker_component/component.dart';
import 'add_image_component/component.dart';

import 'effect.dart';
import 'reducer.dart';
import 'state.dart';
import 'view.dart';

class ExpenseEditorPage extends Page<ExpenseEditorState, Map<String, dynamic>> {
  ExpenseEditorPage()
      : super(
          initState: initState,
          effect: buildEffect(),
          reducer: buildReducer(),
          view: buildView,
          dependencies: Dependencies<ExpenseEditorState>(
              adapter: null,
              slots: <String, Dependent<ExpenseEditorState>>{
                'add-images-component':
                    AddImagesStateConnector() + AddImageComponent(),
                'recipient-picker': RecipientPickerStateConnector() +
                    RecipientPickerComponent(),
              }),
          middleware: <Middleware<ExpenseEditorState>>[],
        );
}

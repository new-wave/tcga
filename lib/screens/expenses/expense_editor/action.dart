import 'package:flutter/material.dart' hide Action;

import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/expenses/expense_editor/state.dart';

import '../currency_picker_page/currency_item_component/state.dart';

enum ExpenseEditorAction {
  handleDateSelected,
  handleTimeSelected,
  selectCurrency,
  handleCurrencySelected,
  handleExpenseStatusSelected,
  handleExpenseCategorySelected,
  save,
  delete,
  initialize,
  unFocus,
  checkField,
}

class ExpenseEditorActionCreator {
  static Action handleDateSelected(DateTime currentItem) {
    return Action(
      ExpenseEditorAction.handleDateSelected,
      payload: currentItem,
    );
  }

  static Action checkField() {
    return const Action(
      ExpenseEditorAction.checkField,
    );
  }

  static Action unFocus() {
    return Action(ExpenseEditorAction.unFocus);
  }

  static Action handleTimeSelected(TimeOfDay currentItem) {
    return Action(
      ExpenseEditorAction.handleTimeSelected,
      payload: currentItem,
    );
  }

  static Action selectCurrency(CurrencyItemState currentItem) {
    return Action(
      ExpenseEditorAction.selectCurrency,
      payload: currentItem,
    );
  }

  static Action handleCurrencySelected(CurrencyItemState currentItem) {
    return Action(
      ExpenseEditorAction.handleCurrencySelected,
      payload: currentItem,
    );
  }

  static Action handleExpenseCategorySelected(int currentItem) {
    return Action(
      ExpenseEditorAction.handleExpenseCategorySelected,
      payload: currentItem,
    );
  }

  static Action handleExpenseStatusSelected(int currentItem) {
    return Action(
      ExpenseEditorAction.handleExpenseStatusSelected,
      payload: currentItem,
    );
  }

  static Action save() {
    return const Action(
      ExpenseEditorAction.save,
    );
  }

  static Action delete() {
    return const Action(
      ExpenseEditorAction.delete,
    );
  }

  static Action initialize(ExpenseEditorState newState) {
    return Action(
      ExpenseEditorAction.initialize,
      payload: newState,
    );
  }
}

import 'package:tcga_flutter/database/models.dart';
import 'package:tcga_flutter/database/models/enums.dart';
import 'package:tcga_flutter/screens/expenses/expense_editor/add_image_component/image_item_component/state.dart';

import 'expense_editor/state.dart';
import 'expenses/history_tab_component/expense_item_component/state.dart';

extension EntityToStateMapper on Expense {
  ExpenseItemState toState() => ExpenseItemState(
        id: this.id,
        title: this.title,
        category: ExpenseCategory.values[this.category],
        status: ExpenseStatus.values[this.status],
        amount: this.amount,
        currencyCode: this.currencyCode,
        personId: this.personId,
        recordedOn: this.recordedOn,
        photos: (this.photos ?? [])
            .map(
              (e) => ImageItemState(
                url: e.url,
              ),
            )
            .toList(),
      );
}

extension StateToEntityMapper on ExpenseEditorState {
  Expense toEntity() => Expense(
        id: this.id,
        title: this.expenseTitlEeditingController.text,
        category: this.category.index,
        status: this.status.index,
        amount: double.parse(this.amountEditingController.text),
        currencyCode: this.currencyCodeEditingController.text,
        personId: this.recipient.id,
        recordedOn: this.recordedOn,
        photos: this
            .photos
            .map(
              (e) => ExpensePhoto(
                url: e.url,
              ),
            )
            .toList(),
      );
}

import 'package:devicelocale/devicelocale.dart';
import 'package:flutter/cupertino.dart';
import 'package:tcga_flutter/database/models.dart';
import 'package:tcga_flutter/database/models/enums.dart';
import 'package:tcga_flutter/database/repository.dart';
import 'package:tcga_flutter/screens/auth/manager.dart';
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/address_item_component/state.dart';
import 'package:tcga_flutter/screens/global_widgets/language/language_selection_page/language_item_component/state.dart';
import 'package:tcga_flutter/screens/global_widgets/phone_number/manager.dart';
import 'package:tcga_flutter/screens/global_widgets/phone_number/phone_item_component/state.dart';
import 'package:tcga_flutter/screens/global_widgets/program/program_component/program_item_component/state.dart';
import 'package:tcga_flutter/screens/valuesets/valueset_picker_page/valueset_item_component/state.dart';
import 'package:tcga_flutter/utils/phone_format.dart';
import 'contact_editor_page/state.dart';
import 'state.dart';

class ContactManager {
  static final _singleton = ContactManager._internal();

  factory ContactManager() {
    return _singleton;
  }

  ContactManager._internal();

  final _authManager = AuthManager();
  final _personRepository = Repository<Person>();
  final _teamRepository = Repository<Team>();
  final _phoneManager = PhoneManager();

  Future<List<ContactItemState>> query({String keyword}) async {
    final team = await _getContactTeam();
    final members = team.members ?? [];

    final items = <ContactItemState>[];

    keyword = (keyword ?? '').trim().toUpperCase();

    for (var i = 0; i < members.length; i++) {
      if (members[i].personId == _authManager.currentCaregiverId) {
        continue;
      }

      final person = await _personRepository.get(
        members[i].personId,
      );

      final fullName = '${person.firstName} ${person.lastName}'.toUpperCase();
      if (!fullName.contains(keyword)) {
        continue;
      }

      items.add(ContactItemState(
        id: person.id,
        dateOfBirth: person.birthDate,
        firstName: person.firstName,
        lastName: person.lastName,
        genderType: person.gender != null
            ? AdministrativeGenderType.values[person.gender]
            : null,
        teamRole: TeamRole.values[members[i].role],
      ));
    }

    return items;
  }

  Future<ContactEditorState> fill(
      BuildContext context, ContactEditorState state) async {
    final person = await _personRepository.get(state.id);

    if (person == null) return state;

    final newState = state.clone();
    newState.generalDetails.firstNameController.text = person.firstName;
    newState.generalDetails.lastNameController.text = person.lastName;
    newState.generalDetails.dateOfBirth = person.birthDate;
    newState.generalDetails.emailAddressController.text = person.email;

    if (person.mobilePhone != null && person.mobilePhone.isNotEmpty) {
      List<String> phones =
          person.mobilePhone.split('-').toList(growable: false) ?? [];
      PhoneItemState phone;
      if (person.countryCode != null && person.countryCode.isNotEmpty) {
        phone = await _phoneManager.getCountryByCode(
          context,
          code: person.countryCode,
        );
      } else {
        phone = await _phoneManager.getCountryByDialCode(
          context,
          dialCode: (phones != null && phones.length > 0) ? phones[0] : '',
        );
      }
      newState.generalDetails.phoneItemState = phone;
      newState.generalDetails.phoneNumberController.text =
          formatMaskPhoneNumber(
              (phones != null && phones.length > 1) ? phones[1] : '',
              phone?.mask != null ? phone?.mask[0] : '' ?? '');
    } else {
      String countryCode = await getCountryCodeFromDevice();
      final phone =
          await _phoneManager.getCountryByCode(context, code: countryCode);
      newState.generalDetails.phoneItemState = phone;
      newState.generalDetails.phoneNumberController.text = '';
    }

    newState.generalDetails.gender = person.gender != null
        ? AdministrativeGenderType.values[person.gender]
        : null;
    newState.generalDetails.maritalStatus = person.maritalStatus != null
        ? MaritalStatusType.values[person.maritalStatus]
        : null;
    newState.generalDetails.prefix =
        person.prefix != null ? PrefixType.values[person.prefix] : null;
    newState.generalDetails.suffix =
        person.suffix != null ? SuffixType.values[person.suffix] : null;

    newState.orgDetails.orgNameController.text = person.organization;
    newState.orgDetails.websiteController.text = person.website;
    newState.orgDetails.distanceController.text = person.distance;

    newState.addresses = (person.addresses ?? [])
        .map(
          (x) => AddressItemState(
            id: x.id,
            address: x.addressContent,
            city: x.city,
            state: x.stateProvince,
            zip: x.zipPostalCode,
          ),
        )
        .toList();
    newState.languages = (person.languages ?? [])
        .map(
          (x) => LanguageItemState(
            code: x.name,
            display: x.name,
            isPrimary: x.isPrimary,
          ),
        )
        .toList();

    newState.credentials = (person.credentials ?? [])
        .map(
          (x) => ValuesetItemState(
            code: x.code,
            display: x.display,
            isSelectable: false,
            isSelected: false,
          ),
        )
        .toList();
    newState.specialties = (person.specialties ?? [])
        .map(
          (x) => ValuesetItemState(
            code: x.code,
            display: x.display,
            isSelectable: false,
            isSelected: false,
          ),
        )
        .toList();

    newState.programs = (person.programs ?? [])
        .map(
          (x) => ProgramItemState(program: x),
        )
        .toList();

    return newState;
  }

  Future<ContactItemState> create(ContactEditorState state) async {
    final team = await _getContactTeam();

    List<Address> addresses = state.addresses
        .where((element) => !element.isDelete)
        .map(
          (x) => Address(
            id: x.id,
            contactId: x.id,
            title: x.title,
            addressContent: x.address,
            city: x.city,
            stateProvince: x.state,
            zipPostalCode: x.zip,
          ),
        )
        .toList();

    List<String> programs = state.programs
        .where((element) => !element.isDelete)
        .map(
          (x) => x.program,
        )
        .toList();

    List<ValueSet> specialties = state.specialties
        .map(
          (x) => ValueSet(
        code: x.code,
        display: x.display,
      ),
    )
        .toList();

    List<ValueSet> credentials = state.credentials
        .map(
          (x) => ValueSet(
        code: x.code,
        display: x.display,
      ),
    )
        .toList();

    final person = Person(
      firstName: state.generalDetails.firstNameController.text.trim(),
      lastName: state.generalDetails.lastNameController.text.trim(),
      birthDate: state.generalDetails.dateOfBirth,
      email: state.generalDetails.emailAddressController.text.trim(),
      mobilePhone: formatPhoneNumber(
        state.generalDetails?.phoneItemState,
        state.generalDetails.phoneNumberController.text.trim(),
      ),
      countryCode: state.generalDetails?.phoneItemState?.id,
      prefix: state.generalDetails.prefix?.index,
      suffix: state.generalDetails.suffix?.index,
      gender: state.generalDetails.gender?.index,
      maritalStatus: state.generalDetails.maritalStatus?.index,
      distance: state.orgDetails.distanceController.text.trim(),
      organization: state.orgDetails.orgNameController.text.trim(),
      website: state.orgDetails.websiteController.text.trim(),
      languages: state.languages
          .map(
            (x) => Language(
              code: x.code,
              name: x.display,
              isPrimary: x.isPrimary,
            ),
          )
          .toList(),
      addresses: addresses,
      specialties: specialties,
      credentials: credentials,
      programs: programs,
    );

    person.id = await _personRepository.create(person);

    team.members.add(TeamMember(
      personId: person.id,
      role: state.teamRole.index,
    ));

    await _teamRepository.update(team);

    return ContactItemState(
      id: person.id,
      firstName: person.firstName,
      lastName: person.lastName,
      dateOfBirth: person.birthDate,
      teamRole: state.teamRole,
      genderType: state.generalDetails.gender,
    );
  }

  Future<ContactItemState> update(ContactEditorState state) async {
    final team = await _getContactTeam();

    final teamMember = team.members.firstWhere(
      (element) => element.personId == state.id,
    );

    if (teamMember == null) return null;

    List<Address> addresses = state.addresses
        .where((element) => !element.isDelete)
        .map(
          (x) => Address(
            id: x.id,
            contactId: x.id,
            title: x.title,
            addressContent: x.address,
            city: x.city,
            stateProvince: x.state,
            zipPostalCode: x.zip,
          ),
        )
        .toList();

    List<String> programs = state.programs
        .where((element) => !element.isDelete)
        .map(
          (x) => x.program,
        )
        .toList();

    List<ValueSet> specialties = state.specialties
        .map(
          (x) => ValueSet(
        code: x.code,
        display: x.display,
      ),
    )
        .toList();

    List<ValueSet> credentials = state.credentials
        .map(
          (x) => ValueSet(
        code: x.code,
        display: x.display,
      ),
    )
        .toList();

    final person = Person(
      id: state.id,
      firstName: state.generalDetails.firstNameController.text.trim(),
      lastName: state.generalDetails.lastNameController.text.trim(),
      birthDate: state.generalDetails.dateOfBirth,
      email: state.generalDetails.emailAddressController.text.trim(),
      mobilePhone: formatPhoneNumber(
        state.generalDetails?.phoneItemState,
        state.generalDetails.phoneNumberController.text.trim(),
      ),
      countryCode: state.generalDetails?.phoneItemState?.id,
      prefix: state.generalDetails.prefix?.index,
      suffix: state.generalDetails.suffix?.index,
      gender: state.generalDetails.gender?.index,
      maritalStatus: state.generalDetails.maritalStatus?.index,
      distance: state.orgDetails.distanceController.text.trim(),
      organization: state.orgDetails.orgNameController.text.trim(),
      website: state.orgDetails.websiteController.text.trim(),
      languages: state.languages
          .map(
            (x) => Language(
              code: x.code,
              name: x.display,
              isPrimary: x.isPrimary,
            ),
          )
          .toList(),
      addresses: addresses,
      specialties: specialties,
      credentials: credentials,
      programs: programs,
    );

    await _personRepository.update(person);

    teamMember.role = state.teamRole.index;

    await _teamRepository.update(team);

    return ContactItemState(
      id: person.id,
      firstName: person.firstName,
      lastName: person.lastName,
      dateOfBirth: person.birthDate,
      teamRole: state.teamRole,
    );
  }

  Future delete(String id) async {
    final team = await _getContactTeam();

    final teamMember = team.members.firstWhere(
      (element) => element.personId == id,
    );

    if (teamMember == null) return false;

    team.members.remove(teamMember);
    await _personRepository.delete(id);
    await _teamRepository.update(team);

    return true;
  }

  Future<ContactItemState> get(String id) async {
    final person = await _personRepository.get(id);

    return ContactItemState(
      id: person?.id,
      firstName: person?.firstName,
      lastName: person?.lastName,
      dateOfBirth: person?.birthDate,
      // teamRole: state.teamRole,
    );
  }

  Future<Team> _getContactTeam() async {
    final personId = _authManager.currentCaregiverId;
    final exists = await _teamRepository.query(
      filter: (team) {
        return team.ownerId == personId &&
            team.ownerType == TeamOwnerType.Caregiver.index &&
            team.type == TeamType.Contacts.index;
      },
    );

    if (exists.length > 0) {
      return exists[0];
    }

    final team = Team(
        ownerId: personId,
        name: 'Contacts',
        ownerType: TeamOwnerType.Caregiver.index,
        type: TeamType.Contacts.index,
        members: [
          TeamMember(
            isActive: true,
            personId: personId,
            role: TeamRole.CareGiver.index,
          ),
        ]);

    team.id = await _teamRepository.create(team);

    return team;
  }
}

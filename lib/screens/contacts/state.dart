import 'package:tcga_flutter/database/models/enums.dart';
import 'package:tcga_flutter/screens/recipients/state.dart';

class ContactItemState extends RecipientItemState {
  TeamRole teamRole;

  static final teamRoles = {
    TeamRole.CareGiver: 'CareGiver',
    TeamRole.CareRecipient: 'Care Recipient',
    TeamRole.CaseManager: 'CaseManager',
    TeamRole.General: 'General',
    TeamRole.EmergencyContact: 'Emergency Contact',
    TeamRole.PowerOfAttorney: 'Power Of Attorney',
    TeamRole.Provider: 'Provider',
    TeamRole.Facility: 'Facility',
    TeamRole.PCP: 'PCP',
    TeamRole.InsuranceCompany: 'Insurance Company',
    TeamRole.FederalAgency: 'Federal Agency',
    TeamRole.SocialWorker: 'Social Worker',
    TeamRole.Employer: 'Employer',
    TeamRole.FamilyMember: 'Family Member',
    TeamRole.Spouse: 'Spouse',
    TeamRole.Child: 'Child',
    TeamRole.Sibling: 'Sibling',
    TeamRole.Parent: 'Parent',
    TeamRole.Neighbor: 'Neighbor',
    TeamRole.Roommate: 'Roommate',
    TeamRole.Friend: 'Friend',
    TeamRole.DomesticPartner: 'DomesticP artner',
    TeamRole.InLaw: 'InLaw',
    TeamRole.Other: 'Other',
  };

  bool isSelected;

  String get teamRoleDisplay => teamRole?.toString()?.split('.')?.last;

  ContactItemState({
    String id,
    String firstName,
    String lastName,
    DateTime dateOfBirth,
    AdministrativeGenderType genderType,
    this.teamRole,
    this.isSelected = false,
  }) : super(
          id: id,
          dateOfBirth: dateOfBirth,
          firstName: firstName,
          lastName: lastName,
          genderType: genderType,
        );

  @override
  ContactItemState clone() {
    return ContactItemState()
      ..id = id
      ..firstName = firstName
      ..lastName = lastName
      ..dateOfBirth = dateOfBirth
      ..genderType = genderType
      ..teamRole = teamRole
      ..isSelected = isSelected;
  }
}

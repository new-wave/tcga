import 'package:fish_redux/fish_redux.dart';

import '../state.dart';
import 'action.dart';
import 'state.dart';

Reducer<ContactPickerState> buildReducer() {
  return asReducer(
    <Object, Reducer<ContactPickerState>>{
      ContactPickerAction.initialize: _initialize,
      ContactPickerAction.handleItemTapped: _handleItemTapped,
    },
  );
}

ContactPickerState _initialize(ContactPickerState state, Action action) {
  final ContactPickerState newState = state.clone();
  final items = action.payload as List<ContactItemState> ?? [];

  for (var item in items) {
    item.isSelected = newState.selectedItems.any(
      (x) => x.id == item.id,
    );
  }

  newState.items = items;
  return newState;
}

ContactPickerState _handleItemTapped(ContactPickerState state, Action action) {
  final newState = state.clone();
  final selectedItem = action.payload as ContactItemState;
  final selectedIndex = state.items.indexOf(selectedItem);

  if (!state.multiSelection) {
    if (!selectedItem.isSelected) {
      for (var item in newState.items) {
        if (item.isSelected) {
          item.isSelected = false;
        }
      }
    }

    newState.selectedItems = [
      selectedItem,
    ];
    selectedItem.isSelected = true;
  } else {
    selectedItem.isSelected = !selectedItem.isSelected;

    if (selectedItem.isSelected) {
      final index = newState.selectedItems.indexWhere(
        (item) => item.id == selectedItem.id,
      );

      if (index == -1) {
        newState.selectedItems.add(selectedItem);
      }
    } else {
      final index = newState.selectedItems.indexWhere(
        (item) => item.id == selectedItem.id,
      );

      if (index > -1) {
        newState.selectedItems.removeAt(index);
      }
    }
  }

  newState.items[selectedIndex] = selectedItem;
  return newState;
}

import 'package:fish_redux/fish_redux.dart';

import '../state.dart';

class ContactPickerState extends MutableSource
    implements Cloneable<ContactPickerState> {
  List<ContactItemState> items;
  List<ContactItemState> selectedItems;

  bool confirmRequired;
  bool multiSelection;

  @override
  ContactPickerState clone() {
    return ContactPickerState()
      ..items = items
      ..selectedItems = selectedItems
      ..confirmRequired = confirmRequired
      ..multiSelection = multiSelection;
  }

  @override
  Object getItemData(int index) {
    return items[index];
  }

  @override
  String getItemType(int index) {
    return 'contact-item';
  }

  @override
  int get itemCount => items?.length ?? 0;

  @override
  void setItemData(int index, Object data) {
    items[index] = data;
  }
}

ContactPickerState initState(Map<String, dynamic> args) {
  args = args ?? {};
  final multiSelection = args.containsKey('multi-selection');
  final confirmRequired =
      multiSelection || args.containsKey('confirm-required');
  return ContactPickerState()
    ..items = []
    ..selectedItems =
        args.containsKey('selected-items') ? args['selected-items'] : []
    ..multiSelection = multiSelection
    ..confirmRequired = confirmRequired;
}

import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/contacts/contact_picker_page/contact_adapter/adapter.dart';

import 'effect.dart';
import 'reducer.dart';
import 'state.dart';
import 'view.dart';

class ContactPickerPage extends Page<ContactPickerState, Map<String, dynamic>> {
  ContactPickerPage()
      : super(
          initState: initState,
          effect: buildEffect(),
          reducer: buildReducer(),
          view: buildView,
          dependencies: Dependencies<ContactPickerState>(
              adapter: NoneConn<ContactPickerState>() + ContactAdapter(),
              slots: <String, Dependent<ContactPickerState>>{}),
          middleware: <Middleware<ContactPickerState>>[],
        );
}

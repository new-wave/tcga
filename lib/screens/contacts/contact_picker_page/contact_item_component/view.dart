import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:mdi/mdi.dart';
import 'package:tcga_flutter/config/palette.dart';

import '../../state.dart';

Widget buildView(
    ContactItemState state, Dispatch dispatch, ViewService viewService) {
  return SizedBox(
    height: 40,
    child: Container(
        padding: const EdgeInsets.symmetric(
          horizontal: 16,
        ),
        child: Row(
          children: [
            Container(
              height: 40,
              width: 40,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(
                  20,
                ),
                color: colorPrimary,
              ),
              child: Text(
                '${state.initial}',
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 14,
                ),
              ),
            ),
            SizedBox(
              width: 16,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    state.fullName ?? '',
                    style: TextStyle(
                      color: colorTextPrimary,
                      fontSize: 14,
                    ),
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                  ),
                  Row(
                    children: [
                      Text(
                        state.gender,
                        style: TextStyle(
                          color: colorTextPrimary,
                          fontSize: 12,
                        ),
                      ),
                      Text(
                        ' - ',
                        style: TextStyle(
                          color: colorTextPrimary,
                          fontSize: 12,
                        ),
                      ),
                      Text(
                        'Age: ${state.age}',
                        style: TextStyle(
                          color: colorTextPrimary,
                          fontSize: 12,
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
            if (state.isSelected)
              Icon(
                Mdi.checkboxMarkedCircle,
                color: colorIndicatorPositive,
              ),
            SizedBox(
              width: 16,
            ),
          ],
        )),
  );
}

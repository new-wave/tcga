import 'package:flutter/material.dart' hide Action;

import 'package:fish_redux/fish_redux.dart';
import '../manager.dart';

import '../state.dart';
import 'action.dart';
import 'state.dart';

final _contactManager = ContactManager();

Effect<ContactPickerState> buildEffect() {
  return combineEffects(<Object, Effect<ContactPickerState>>{
    Lifecycle.initState: _init,
    ContactPickerAction.search: _search,
    ContactPickerAction.confirm: _confirm,
  });
}

Future _init(Action action, Context<ContactPickerState> ctx) async {
  final items = await _contactManager.query();

  ctx.dispatch(ContactPickerActionCreator.initialize(items));
}

Future _search(Action action, Context<ContactPickerState> ctx) async {
  final keyword = action.payload as String ?? '';

  final items = await _contactManager.query(
    keyword: keyword,
  );

  ctx.dispatch(ContactPickerActionCreator.initialize(items));
}

Future _confirm(Action action, Context<ContactPickerState> ctx) async {
  final selectedItem = action.payload as ContactItemState;

  if (selectedItem != null) {
    Navigator.of(ctx.context).pop(selectedItem);
  }

  if (ctx.state.selectedItems.length == 0) return;

  Navigator.of(ctx.context).pop(
    ctx.state.multiSelection
        ? ctx.state.selectedItems
        : ctx.state.selectedItems.first,
  );
}

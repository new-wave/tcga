import 'package:fish_redux/fish_redux.dart';

import '../state.dart';

enum ContactPickerAction {
  initialize,
  search,
  confirm,
  handleItemTapped,
}

class ContactPickerActionCreator {
  static Action initialize(List<ContactItemState> items) {
    return Action(ContactPickerAction.initialize, payload: items);
  }

  static Action search(String keyword) {
    return Action(ContactPickerAction.search, payload: keyword);
  }

  static Action confirm([ContactItemState item]) {
    return Action(ContactPickerAction.confirm, payload: item);
  }

  static Action handleItemTapped(ContactItemState item) {
    return Action(
      ContactPickerAction.handleItemTapped,
      payload: item,
    );
  }
}

import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/database/models/enums.dart';
import 'package:tcga_flutter/screens/contacts/state.dart';
import 'package:tcga_flutter/screens/recipients/state.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';

import '../../../route_name.dart';
import '../../../utils/utils.dart';

import '../manager.dart';

import 'action.dart';
import 'state.dart';

final _manager = ContactManager();

Effect<ContactsState> buildEffect() {
  return combineEffects(<Object, Effect<ContactsState>>{
    Lifecycle.initState: _init,
    ContactsAction.add: _add,
    ContactsAction.edit: _edit,
    ContactsAction.delete: _delete,
    ContactsAction.addNote: _addNote,
    ContactsAction.viewNotes: _viewNotes,
  });
}

Future _init(Action action, Context<ContactsState> ctx) async {
  final contacts = await _manager.query();
  ctx.dispatch(
    ContactsActionCreator.initialize(
      contacts,
    ),
  );
}

Future _add(Action action, Context<ContactsState> ctx) async {
  final result = await ctx.navigateTo(
    RouteName.contactsEditor,
  );

  if (result is ContactItemState) {
    ctx.dispatch(
      ContactsActionCreator.handleContactAdded(
        result,
      ),
    );
  }
}

Future _edit(Action action, Context<ContactsState> ctx) async {
  final contact = action.payload as ContactItemState;

  final result = await ctx.navigateTo(
    contact.teamRole == TeamRole.CareRecipient
        ? RouteName.recipientsEditor
        : RouteName.contactsEditor,
    arguments: {
      'item': action.payload,
    },
  );

  if (result is ContactItemState) {
    ctx.dispatch(
      ContactsActionCreator.handleContactEdited(
        result,
      ),
    );
  } else if (result is RecipientItemState) {
    ctx.dispatch(
      ContactsActionCreator.handleContactEdited(
        ContactItemState(
          id: result.id,
          firstName: result.firstName,
          lastName: result.lastName,
          dateOfBirth: result.dateOfBirth,
          genderType: result.genderType,
          teamRole: TeamRole.CareRecipient,
        ),
      ),
    );
  } else if (result is Map<String, dynamic> &&
      result.containsKey('deleted-item-id')) {
    ctx.dispatch(
      ContactsActionCreator.handleContactDeleted(
        result['deleted-item-id'],
      ),
    );
  }
}

Future _delete(Action action, Context<ContactsState> ctx) async {
  final confirmed = await ctx.confirm(
    title: AppLocalizations.instance.text('common.warning.delete.title'),
    message: AppLocalizations.instance.text('common.warning.delete.desc'),
    negativeActionTitle: AppLocalizations.instance.text('common.cancel'),
    positiveActionTitle: AppLocalizations.instance.text('common.sure'),
  );

  if (!confirmed) return;

  final deletedItemId = action.payload.id;
  final deleted = await _manager.delete(deletedItemId);

  if (!deleted) {
    // TODO show error message
    return;
  }

  ctx.dispatch(
    ContactsActionCreator.handleContactDeleted(
      action.payload.id,
    ),
  );
}

Future _addNote(Action action, Context<ContactsState> ctx) {
  return ctx.navigateTo(
    RouteName.notesEditor,
    arguments: {
      'item-id': action.payload.id,
      'type': NoteType.Contact,
      'recipient-id': action.payload.id,
    },
  );
}

Future _viewNotes(Action action, Context<ContactsState> ctx) {
  return ctx.navigateTo(
    RouteName.notes,
    arguments: {
      'type': NoteType.Contact,
      'recipient-id': action.payload.id,
    },
  );
}

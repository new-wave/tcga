import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart' hide Action, Page;
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/screens/contacts/contacts_page/action.dart';
import 'package:tcga_flutter/widget/list_item_widget.dart';

import '../../state.dart';

Widget buildView(
  ContactItemState state,
  Dispatch dispatch,
  ViewService viewService,
) {
  return SizedBox(
    height: 40,
    child: ListItemWidget(
      padding: const EdgeInsets.all(0),
      contextActions: [
        'recipients.popup.add_note',
        'recipients.popup.view_notes',
        'recipients.popup.edit',
        'recipients.popup.delete',
      ],
      onTap: () {
        dispatch(
          ContactsActionCreator.edit(state),
        );
      },
      onContextActionSelected: (String action) {
        switch (action) {
          case 'recipients.popup.add_note':
            {
              dispatch(
                ContactsActionCreator.addNote(
                  state,
                ),
              );
            }
            break;
          case 'recipients.popup.view_notes':
            {
              dispatch(
                ContactsActionCreator.viewNotes(
                  state,
                ),
              );
            }
            break;
          case 'recipients.popup.edit':
            {
              dispatch(
                ContactsActionCreator.edit(
                  state,
                ),
              );
            }
            break;
          case 'recipients.popup.delete':
            {
              dispatch(
                ContactsActionCreator.delete(
                  state,
                ),
              );
            }
            break;
        }
      },
      child: Container(
        child: Row(
          children: [
            SizedBox(
              width: 16,
            ),
            Container(
              height: 40,
              width: 40,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(
                  20,
                ),
                color: colorPrimary,
              ),
              child: Text(
                '${state.initial}',
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 14,
                ),
              ),
            ),
            SizedBox(
              width: 16,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  width: MediaQuery.of(viewService.context).size.width - 115,
                  child: Text(
                    state.fullName,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      color: colorTextPrimary,
                      fontSize: 14,
                    ),
                  ),
                ),
                Text(
                  state.teamRoleDisplay,
                  style: TextStyle(
                    color: colorTextSecondary,
                    fontSize: 12,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    ),
  );
}

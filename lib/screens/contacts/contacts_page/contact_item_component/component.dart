import 'package:fish_redux/fish_redux.dart';

import '../../state.dart';
import 'view.dart';

class ContactItemComponent extends Component<ContactItemState> {
  ContactItemComponent()
      : super(
          view: buildView,
        );
}

import 'package:fish_redux/fish_redux.dart';

import '../state.dart';

enum ContactsAction {
  initialize,
  add,
  handleContactAdded,
  edit,
  handleContactEdited,
  delete,
  handleContactDeleted,
  addNote,
  viewNotes,
}

class ContactsActionCreator {
  static Action initialize(List<ContactItemState> items) {
    return Action(
      ContactsAction.initialize,
      payload: items,
    );
  }

  static Action add() {
    return const Action(
      ContactsAction.add,
    );
  }

  static Action handleContactAdded(
    ContactItemState result,
  ) {
    return Action(
      ContactsAction.handleContactAdded,
      payload: result,
    );
  }

  static Action edit(ContactItemState state) {
    return Action(
      ContactsAction.edit,
      payload: state,
    );
  }

  static Action handleContactEdited(ContactItemState result) {
    return Action(
      ContactsAction.handleContactEdited,
      payload: result,
    );
  }

  static Action delete(ContactItemState state) {
    return Action(
      ContactsAction.delete,
      payload: state,
    );
  }

  static Action handleContactDeleted(String result) {
    return Action(
      ContactsAction.handleContactDeleted,
      payload: result,
    );
  }

  static Action addNote(ContactItemState state) {
    return Action(
      ContactsAction.addNote,
      payload: state,
    );
  }

  static Action viewNotes(ContactItemState state) {
    return Action(
      ContactsAction.viewNotes,
      payload: state,
    );
  }
}

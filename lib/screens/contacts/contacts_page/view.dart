import 'package:flutter/material.dart';

import 'package:fish_redux/fish_redux.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';

import 'action.dart';
import 'state.dart';

Widget buildView(
    ContactsState state, Dispatch dispatch, ViewService viewService) {
  final ListAdapter adapter = viewService.buildAdapter();
  return Builder(
    builder: (context) {
      return Scaffold(
        backgroundColor: Colors.white,
        appBar: _appBar(viewService, dispatch),
        body: ListView.separated(
          padding: const EdgeInsets.symmetric(
            vertical: 16,
          ),
          separatorBuilder: (context, index) {
            return Divider();
          },
          itemCount: adapter.itemCount,
          itemBuilder: adapter.itemBuilder,
        ),
      );
    },
  );
}

AppBar _appBar(ViewService viewService, Dispatch dispatch) {
  return AppBar(
    elevation: 3,
    backgroundColor: Colors.white,
    leading: IconButton(
      iconSize: 24,
      icon: SvgPicture.asset('assets/icons/ic_arrow_left_circle.svg'),
      onPressed: () {
        Navigator.pop(viewService.context);
      },
    ),
    title: Text(AppLocalizations.of(viewService.context).text('contacts')),
    actions: [
      IconButton(
        iconSize: 24,
        icon: SvgPicture.asset('assets/icons/ic_button_add.svg'),
        onPressed: () {
          dispatch(
            ContactsActionCreator.add(),
          );
        },
      )
    ],
  );
}

import 'package:fish_redux/fish_redux.dart';

import '../state.dart';

class ContactsState extends MutableSource implements Cloneable<ContactsState> {
  List<ContactItemState> items;

  @override
  ContactsState clone() {
    return ContactsState()
      ..items = items
          .map(
            (e) => e.clone(),
          )
          .toList();
  }

  @override
  Object getItemData(int index) => items[index];

  @override
  String getItemType(int index) => 'contact-item';

  @override
  int get itemCount => items?.length ?? 0;

  @override
  void setItemData(int index, Object data) => items[index] = data;
}

ContactsState initState(Map<String, dynamic> args) {
  return ContactsState()..items = [];
}

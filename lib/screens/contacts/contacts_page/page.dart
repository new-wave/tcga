import 'package:fish_redux/fish_redux.dart';

import 'contact_adapter/adapter.dart';
import 'state.dart';
import 'view.dart';
import 'effect.dart';
import 'reducer.dart';

class ContactsPage extends Page<ContactsState, Map<String, dynamic>> {
  ContactsPage()
      : super(
          initState: initState,
          effect: buildEffect(),
          reducer: buildReducer(),
          view: buildView,
          dependencies: Dependencies<ContactsState>(
            adapter: NoneConn<ContactsState>() + ContactAdapter(),
            slots: <String, Dependent<ContactsState>>{},
          ),
        );
}

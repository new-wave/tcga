import 'package:fish_redux/fish_redux.dart';

import '../state.dart';
import 'action.dart';
import 'state.dart';

Reducer<ContactsState> buildReducer() {
  return asReducer(
    <Object, Reducer<ContactsState>>{
      ContactsAction.initialize: _initialize,
      ContactsAction.handleContactAdded: _handleContactAdded,
      ContactsAction.handleContactEdited: _handleContactEdited,
      ContactsAction.handleContactDeleted: _handleContactDeleted,
    },
  );
}

ContactsState _initialize(ContactsState state, Action action) {
  final items = action.payload ?? <ContactItemState>[];
  final newState = state.clone();
  newState.items = items;

  return newState;
}

ContactsState _handleContactAdded(ContactsState state, Action action) {
  final item = action.payload as ContactItemState;
  final newState = state.clone();

  newState.items.add(item);

  return newState;
}

ContactsState _handleContactEdited(ContactsState state, Action action) {
  final item = action.payload as ContactItemState;
  final newState = state.clone();

  final editedIndex = newState.items.indexWhere(
    (x) => x.id == item.id,
  );

  newState.items[editedIndex] = item;

  return newState;
}

ContactsState _handleContactDeleted(ContactsState state, Action action) {
  final id = action.payload;
  final newState = state.clone();

  final deletedIndex = newState.items.indexWhere(
    (x) => x.id == id,
  );

  newState.items.removeAt(deletedIndex);

  return newState;
}

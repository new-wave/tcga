import 'package:fish_redux/fish_redux.dart';

import '../contact_item_component/component.dart';
import '../state.dart';

class ContactAdapter extends SourceFlowAdapter<ContactsState> {
  ContactAdapter()
      : super(
          pool: <String, Component<Object>>{
            'contact-item': ContactItemComponent(),
          },
        );
}

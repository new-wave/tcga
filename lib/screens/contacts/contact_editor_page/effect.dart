import 'package:flutter/material.dart' hide Action, Page;

import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/database/models/enums.dart';
import 'package:tcga_flutter/screens/global_widgets/phone_number/manager.dart';
import 'package:tcga_flutter/utils/phone_format.dart';
import 'package:tcga_flutter/utils/show_picker.dart';

import '../../../utils/utils.dart';
import '../../../route_name.dart';
import '../manager.dart';
import 'validator.dart';
import 'action.dart';
import 'state.dart';

final _manager = ContactManager();
final _phoneManager = PhoneManager();

Effect<ContactEditorState> buildEffect() {
  return combineEffects(<Object, Effect<ContactEditorState>>{
    Lifecycle.initState: _init,
    ContactEditorAction.save: _save,
    ContactEditorAction.delete: _delete,
    ContactEditorAction.selectSpecialties: _selectSpecialties,
    ContactEditorAction.selectCredentials: _selectCredentials,
  });
}

Future _init(
  Action action,
  Context<ContactEditorState> ctx,
) async {
  final state = ctx.state;

  if (state.onEditMode) {
    final result = await _manager.fill(ctx.context, state);

    ctx.dispatch(
      ContactEditorActionCreator.initialize(result),
    );
  } else {
    String countryCode = await getCountryCodeFromDevice();
    final phone =
        await _phoneManager.getCountryByCode(ctx.context, code: countryCode);
    ContactEditorState state = ctx.state.clone();
    state.generalDetails.phoneItemState = phone;
    state.generalDetails.phoneNumberController.text = '';
    ctx.dispatch(
      ContactEditorActionCreator.initialize(state),
    );
  }
}

Future _save(
  Action action,
  Context<ContactEditorState> ctx,
) async {
  final state = ctx.state;

  bool isValid = Validator.isValid(state);
  if (!isValid) {
    ctx.dispatch(ContactEditorActionCreator.checkField());
    return;
  }

  if (state.onEditMode) {
    final updatedContactItemState = await _manager.update(state);

    if (updatedContactItemState == null) {
      final snackbar = SnackBar(
        content: Text(
          'Invalid operation - Not able to update contact',
        ),
      );

      Scaffold.of(ctx.context).showSnackBar(snackbar);

      return;
    }

    Navigator.of(ctx.context).pop(updatedContactItemState);
    return;
  }

  final newContactItemState = await _manager.create(state);
  Navigator.of(ctx.context).pop(newContactItemState);
}

Future _delete(
  Action action,
  Context<ContactEditorState> ctx,
) async {
  final state = ctx.state;

  if (!state.onEditMode) {
    return;
  }

  var confirmed = await ctx.confirm(
      title: 'Deletion warning!',
      message: 'Are you sure to delete this item?',
      negativeActionTitle: 'Cancel',
      positiveActionTitle: 'Yes, I am sure');

  if (!confirmed) return;

  final deleted = await _manager.delete(state.id);

  if (!deleted) {
    // TODO show an error message
    return;
  }
  Navigator.of(ctx.context).pop({
    'deleted-item-id': state.id,
  });
}

void _selectSpecialties(Action action, Context<ContactEditorState> ctx) async {
  var selectedItems = await showModal(
    ctx.context,
    RouteName.valuesetPicker,
    args: {
      'type': ValueSetType.specialty,
      'multi-selection': true,
    },
  );

  if (selectedItems != null) {
    ctx.dispatch(
      ContactEditorActionCreator.handleSpecialtiesSelected(
        selectedItems,
      ),
    );
  }
}

void _selectCredentials(Action action, Context<ContactEditorState> ctx) async {
  var selectedItems = await showModal(
    ctx.context,
    RouteName.valuesetPicker,
    args: {
      'type': ValueSetType.credential,
      'multi-selection': true,
    },
  );

  if (selectedItems != null) {
    ctx.dispatch(
      ContactEditorActionCreator.handleCredentialsSelected(
        selectedItems,
      ),
    );
  }
}

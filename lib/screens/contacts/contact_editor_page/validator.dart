import 'package:email_validator/email_validator.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';

import '../../../utils/string_extensions.dart';

import 'state.dart';

class Validator {
  static bool isValid(ContactEditorState state) {
    final firstName = state.generalDetails.firstNameController.text ?? '';
    if (firstName.isEmptyOrWhitespace)
      return false;

    final lastName = state.generalDetails.lastNameController.text ?? '';
    if (lastName.isEmptyOrWhitespace)
      return false;

    final dob = state.generalDetails.dateOfBirth;
    if (dob == null || !dob.difference(DateTime.now()).isNegative)
      return false;

    if (state.generalDetails.gender == null)
      return false;

    if (state.teamRole == null)
      return false;

    final emailAddress =
        (state.generalDetails.emailAddressController.text ?? '').trim();
    if (emailAddress.isNotEmpty && !EmailValidator.validate(emailAddress))
      return false;

    final phoneNumber =
    (state.generalDetails.phoneNumberController.text ?? '').trim();
    if (phoneNumber.length > 0 &&
        state.generalDetails?.phoneItemState?.mask != null) {
      if (phoneNumber.length !=
          state.generalDetails.phoneItemState.mask[0].length) {
        return false;
      }
    }

    return true;
  }
}

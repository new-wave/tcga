import 'package:email_validator/email_validator.dart';
import 'package:fish_redux/fish_redux.dart';

import 'package:tcga_flutter/screens/valuesets/valueset_picker_page/valueset_item_component/state.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import '../../../utils/string_extensions.dart';
import 'action.dart';
import 'state.dart';

Reducer<ContactEditorState> buildReducer() {
  return asReducer(
    <Object, Reducer<ContactEditorState>>{
      ContactEditorAction.handleCredentialsSelected: _handleCredentialsSelected,
      ContactEditorAction.initialize: _initialize,
      ContactEditorAction.handleSpecialtiesSelected: _handleSpecialtiesSelected,
      ContactEditorAction.handleTeamRoleSelected: _handleTeamRoleSelected,
      ContactEditorAction.checkField: _checkFieldReducer,
    },
  );
}

ContactEditorState _initialize(
  ContactEditorState state,
  Action action,
) {
  return action.payload.clone();
}

ContactEditorState _checkFieldReducer(ContactEditorState state, Action action) {
  final ContactEditorState newState = state.clone();
  newState.generalDetails.isCheckField = true;
  newState.isCheckField = true;
  return newState;
}

ContactEditorState _handleSpecialtiesSelected(
  ContactEditorState state,
  Action action,
) {
  final ContactEditorState newState = state.clone();

  final items = action.payload as List<ValuesetItemState> ?? [];

  newState.specialties = items;

  return newState;
}

ContactEditorState _handleCredentialsSelected(
  ContactEditorState state,
  Action action,
) {
  final ContactEditorState newState = state.clone();

  final items = action.payload as List<ValuesetItemState> ?? [];

  newState.credentials = items;

  return newState;
}

ContactEditorState _handleTeamRoleSelected(
  ContactEditorState state,
  Action action,
) {
  final ContactEditorState newState = state.clone();
  final selectedIndex = action.payload as int;

  newState.teamRole = state.teamRoles.keys.elementAt(
    selectedIndex,
  );

  return newState;
}

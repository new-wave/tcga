import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';

class OrganizationDetailsState implements Cloneable<OrganizationDetailsState> {
  TextEditingController websiteController;
  TextEditingController distanceController;
  TextEditingController orgNameController;

  @override
  OrganizationDetailsState clone() {
    return OrganizationDetailsState()
      ..distanceController = distanceController
      ..orgNameController = orgNameController
      ..websiteController = websiteController;
  }
}

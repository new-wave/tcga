import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/widget/import.dart';

import 'state.dart';

Widget buildView(OrganizationDetailsState state, Dispatch dispatch,
    ViewService viewService) {
  return Container(
    padding: const EdgeInsets.all(16.0),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          AppLocalizations.of(viewService.context)
              .text('contact.editor.organization.header'),
          style: textStyleTitleThird,
        ),
        SizedBox(
          height: 8,
        ),
        EntryWidget(
          title: AppLocalizations.of(viewService.context)
              .text('contact.editor.organization.placeholder_name'),
          keyboardType: TextInputType.name,
          titleStyle: textStyleHinttext,
          controller: state.orgNameController,
        ),
        SizedBox(
          height: 8,
        ),
        Row(
          children: [
            Expanded(
              child: EntryWidget(
                title: AppLocalizations.of(viewService.context)
                    .text('contact.editor.organization.placeholder_website'),
                keyboardType: TextInputType.text,
                titleStyle: textStyleHinttext,
                controller: state.websiteController,
              ),
            ),
            SizedBox(
              width: 16,
            ),
            Expanded(
              child: EntryWidget(
                title: AppLocalizations.of(viewService.context)
                    .text('contact.editor.organization.placeholder_distance'),
                keyboardType: TextInputType.text,
                titleStyle: textStyleHinttext,
                controller: state.distanceController,
              ),
            ),
          ],
        )
      ],
    ),
  );
}

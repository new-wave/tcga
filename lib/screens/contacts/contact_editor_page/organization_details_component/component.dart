import 'package:fish_redux/fish_redux.dart';

import 'state.dart';
import 'view.dart';

class OrganizationDetailsComponent extends Component<OrganizationDetailsState> {
  OrganizationDetailsComponent()
      : super(
          view: buildView,
          dependencies: Dependencies<OrganizationDetailsState>(
              adapter: null,
              slots: <String, Dependent<OrganizationDetailsState>>{}),
        );
}

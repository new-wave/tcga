import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/valuesets/valueset_picker_page/valueset_item_component/state.dart';

import 'state.dart';

enum ContactEditorAction {
  initialize,
  checkField,
  save,
  delete,
  handleTeamRoleSelected,
  selectSpecialties,
  handleSpecialtiesSelected,
  selectCredentials,
  handleCredentialsSelected,
}

class ContactEditorActionCreator {
  static Action handleTeamRoleSelected(int index) {
    return Action(
      ContactEditorAction.handleTeamRoleSelected,
      payload: index,
    );
  }

  static Action checkField() {
    return const Action(
      ContactEditorAction.checkField,
    );
  }

  static Action save() {
    return const Action(
      ContactEditorAction.save,
    );
  }

  static Action delete() {
    return const Action(
      ContactEditorAction.delete,
    );
  }

  static Action onSpecialty() {
    return Action(
      ContactEditorAction.selectSpecialties,
    );
  }

  static Action handleSpecialtiesSelected(List<ValuesetItemState> items) {
    return Action(
      ContactEditorAction.handleSpecialtiesSelected,
      payload: items,
    );
  }

  static Action selectCredentials() {
    return Action(
      ContactEditorAction.selectCredentials,
    );
  }

  static Action handleCredentialsSelected(List<ValuesetItemState> items) {
    return Action(
      ContactEditorAction.handleCredentialsSelected,
      payload: items,
    );
  }

  static Action initialize(ContactEditorState person) {
    return Action(
      ContactEditorAction.initialize,
      payload: person,
    );
  }
}

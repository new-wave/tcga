import 'dart:io';

import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/widget/import.dart';

import 'action.dart';
import 'state.dart';
import 'validator.dart';

Widget buildView(
    ContactEditorState state, Dispatch dispatch, ViewService viewService) {
  double height = ((Platform.isIOS)
          ? (MediaQuery.of(viewService.context).padding.bottom / 2)
          : 0) +
      45.0;
  return Builder(builder: (context) {
    return Padding(
      padding: EdgeInsets.only(
        bottom: MediaQuery.of(context).viewInsets.bottom,
      ),
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        // resizeToAvoidBottomPadding: false,
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 3,
          backgroundColor: Colors.white,
          leading: IconButton(
            iconSize: 24,
            icon: SvgPicture.asset('assets/icons/ic_arrow_left_circle.svg'),
            onPressed: () {
              Navigator.pop(viewService.context);
            },
          ),
          title: Text(state.onEditMode
              ? AppLocalizations.of(viewService.context)
                  .text('contact.editor.title.edit')
              : AppLocalizations.of(viewService.context)
                  .text('contact.editor.title.create')),
          actions: [
            if (state.onEditMode)
              IconButton(
                iconSize: 24,
                icon: SvgPicture.asset('assets/icons/ic_trash.svg'),
                onPressed: () => dispatch(
                  ContactEditorActionCreator.delete(),
                ),
              )
          ],
        ),
        body: CustomScrollView(
          slivers: [
            SliverToBoxAdapter(
              child: viewService.buildComponent(
                'general-details-component',
              ),
            ),
            SliverToBoxAdapter(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: PickerWidget(
                  title: AppLocalizations.of(context)
                      .text('contact.editor.team_role.title'),
                  initialIndex: state.teamRole != null
                      ? state.teamRoles.keys.toList().indexOf(
                            state.teamRole,
                          )
                      : -1,
                  items: state.teamRoles.values.toList(),
                  indexSelected: (index) {
                    dispatch(
                      ContactEditorActionCreator.handleTeamRoleSelected(
                        index,
                      ),
                    );
                  },
                  warningText: AppLocalizations.instance.text('common.validator.team_role'),
                  isRequired: true,
                  isCheckField: state.isCheckField ?? false,
                ),
              ),
            ),
            SliverToBoxAdapter(
              child: viewService.buildComponent(
                'organization-details-component',
              ),
            ),
            SliverToBoxAdapter(
              child: viewService.buildComponent(
                'addresses-component',
              ),
            ),
            SliverToBoxAdapter(
              child: viewService.buildComponent(
                'languages-component',
              ),
            ),
            SliverToBoxAdapter(
              child: viewService.buildComponent(
                'programs-component',
              ),
            ),
            SliverToBoxAdapter(
              child: viewService.buildComponent(
                'credentials-component',
              ),
            ),
            SliverToBoxAdapter(
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: viewService.buildComponent(
                  'specialties-component',
                ),
              ),
            ),
          ],
        ),
        bottomNavigationBar: Builder(
          builder: (context) => Container(
            height: height,
            decoration: BoxDecoration(
                color: colorPrimary, border: Border.all(color: colorPrimary)),
            child: ButtonWidget(
              colorButton: colorPrimary,
              textButton:
                  AppLocalizations.of(viewService.context).text('common.save'),
              onPress: () {
                dispatch(
                  ContactEditorActionCreator.save(),
                );
              },
            ),
          ),
        ),
      ),
    );
  });
}

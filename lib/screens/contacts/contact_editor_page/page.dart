import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/database/models/enums.dart';
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/component.dart';
import 'package:tcga_flutter/screens/global_widgets/general_details_component/component.dart';
import 'package:tcga_flutter/screens/global_widgets/language/languages_component/component.dart';
import 'package:tcga_flutter/screens/global_widgets/program/program_component/component.dart';
import 'package:tcga_flutter/screens/valuesets/valuesets_component/component.dart';

import 'organization_details_component/component.dart';

import 'state.dart';
import 'view.dart';
import 'effect.dart';
import 'reducer.dart';

class ContactsEditorPage
    extends Page<ContactEditorState, Map<String, dynamic>> {
  ContactsEditorPage()
      : super(
            initState: initState,
            effect: buildEffect(),
            reducer: buildReducer(),
            view: buildView,
            dependencies: Dependencies<ContactEditorState>(
              adapter: null,
              slots: <String, Dependent<ContactEditorState>>{
                'general-details-component':
                    GeneralDetailsStateConnector() + GeneralDetailsComponent(),
                'organization-details-component':
                    OrganizationDetailsStateConnector() +
                        OrganizationDetailsComponent(),
                'addresses-component':
                    AddressesStateConnector() + AddressesComponent(),
                'languages-component':
                    LanguagesStateConnector() + LanguagesComponent(),
                'programs-component':
                    ProgramsStateConnector() + ProgramsComponent(),
                'credentials-component': ValuesetsStateConnector(
                      type: ValueSetType.credential,
                      title: 'Credential',
                    ) +
                    ValuesetsComponent(),
                'specialties-component': ValuesetsStateConnector(
                      type: ValueSetType.specialty,
                      title: 'Specialty',
                    ) +
                    ValuesetsComponent(),
              },
            ));
}

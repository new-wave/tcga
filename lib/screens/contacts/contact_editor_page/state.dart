import 'package:flutter/material.dart' hide Action, Page;

import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/database/models/enums.dart';

import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/address_item_component/state.dart';
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/state.dart';
import 'package:tcga_flutter/screens/global_widgets/general_details_component/state.dart';
import 'package:tcga_flutter/screens/global_widgets/language/language_selection_page/language_item_component/state.dart';
import 'package:tcga_flutter/screens/global_widgets/language/languages_component/state.dart';
import 'package:tcga_flutter/screens/global_widgets/program/program_component/program_item_component/state.dart';
import 'package:tcga_flutter/screens/global_widgets/program/program_component/state.dart';
import 'package:tcga_flutter/screens/valuesets/valueset_picker_page/valueset_item_component/state.dart';
import 'package:tcga_flutter/screens/valuesets/valuesets_component/state.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';

import '../state.dart';
import 'organization_details_component/state.dart';

class ContactEditorState implements Cloneable<ContactEditorState> {
  String id;

  bool get onEditMode => id != null && id.isNotEmpty;

  //String get title => ;

  List<AddressItemState> addresses;
  List<LanguageItemState> languages;
  List<ProgramItemState> programs;
  List<ValuesetItemState> specialties;
  List<ValuesetItemState> credentials;

  GeneralDetailsState generalDetails;
  OrganizationDetailsState orgDetails;
  TeamRole teamRole;

  bool isCheckField;

  Map<TeamRole, String> get teamRoles => {
        TeamRole.CareGiver: 'Caregiver',
        // TeamRole.CareRecipient: 'Care Recipient',
        TeamRole.CaseManager: 'Case Manager',
        TeamRole.General: 'General',
        TeamRole.EmergencyContact: 'Emergency Contact',
        TeamRole.PowerOfAttorney: 'Power Of Attorney',
        TeamRole.Provider: 'Provider',
        TeamRole.Facility: 'Facility',
        TeamRole.PCP: 'PCP',
        TeamRole.InsuranceCompany: 'Insurance Company',
        TeamRole.FederalAgency: 'Federal Agency',
        TeamRole.SocialWorker: 'Social Worker',
        TeamRole.Employer: 'Employer',
        TeamRole.FamilyMember: 'FamilyMember',
        TeamRole.Spouse: 'Spouse',
        TeamRole.Child: 'Child',
        TeamRole.Sibling: 'Sibling',
        TeamRole.Parent: 'Parent',
        TeamRole.Neighbor: 'Neighbor',
        TeamRole.Roommate: 'Roommate',
        TeamRole.Friend: 'Friend',
        TeamRole.DomesticPartner: 'Domestic Partner',
        TeamRole.InLaw: 'In-Law',
        TeamRole.Other: 'Other',
      };

  @override
  ContactEditorState clone() {
    return ContactEditorState()
      ..id = id
      ..addresses = addresses
      ..languages = languages
      ..programs = programs
      ..specialties = specialties
      ..credentials = credentials
      ..generalDetails = generalDetails
      ..orgDetails = orgDetails
      ..teamRole = teamRole
      ..isCheckField = isCheckField;
  }
}

ContactEditorState initState(Map<String, dynamic> args) {
  final contactOnEdit = args != null ? args['item'] as ContactItemState : null;

  final state = ContactEditorState()
    ..id = contactOnEdit?.id
    ..generalDetails = (GeneralDetailsState()
      ..firstNameController = TextEditingController(
        text: contactOnEdit?.firstName,
      )
      ..lastNameController = TextEditingController(
        text: contactOnEdit?.lastName,
      )
      ..emailAddressController = TextEditingController(
        text: '',
      )
      ..phoneNumberController = TextEditingController(
        text: '',
      )
      ..dateOfBirth = contactOnEdit?.dateOfBirth)
    ..orgDetails = (OrganizationDetailsState()
      ..orgNameController = TextEditingController(
        text: '',
      )
      ..websiteController = TextEditingController(
        text: '',
      )
      ..distanceController = TextEditingController(
        text: '',
      ))
    ..teamRole = contactOnEdit?.teamRole
    ..addresses = []
    ..programs = []
    ..languages = []
    ..credentials = []
    ..specialties = []
    ..isCheckField = false;

  return state;
}

class AddressesStateConnector
    extends ConnOp<ContactEditorState, AddressesState> {
  @override
  AddressesState get(ContactEditorState state) =>
      AddressesState()..items = state.addresses;

  @override
  void set(ContactEditorState state, AddressesState subState) =>
      state.addresses = subState.items
          .map(
            (e) => e.clone(),
          )
          .toList();
}

class GeneralDetailsStateConnector
    extends ConnOp<ContactEditorState, GeneralDetailsState> {
  @override
  GeneralDetailsState get(ContactEditorState state) =>
      state.generalDetails.clone();

  @override
  void set(ContactEditorState state, GeneralDetailsState subState) {
    state.generalDetails = subState.clone();
  }
}

class OrganizationDetailsStateConnector
    extends ConnOp<ContactEditorState, OrganizationDetailsState> {
  @override
  OrganizationDetailsState get(ContactEditorState state) =>
      state.orgDetails.clone();

  @override
  void set(ContactEditorState state, OrganizationDetailsState subState) {
    state.orgDetails = subState.clone();
  }
}

class LanguagesStateConnector
    extends ConnOp<ContactEditorState, LanguagesState> {
  @override
  LanguagesState get(ContactEditorState state) => LanguagesState(
        items: state.languages,
      );

  @override
  void set(ContactEditorState state, LanguagesState subState) {
    state.languages = subState.items;
  }
}

class ProgramsStateConnector extends ConnOp<ContactEditorState, ProgramState> {
  @override
  ProgramState get(ContactEditorState state) =>
      ProgramState()..items = state.programs;

  @override
  void set(ContactEditorState state, ProgramState subState) =>
      state.programs = subState.items
          .map(
            (e) => e.clone(),
          )
          .toList();
}

class ValuesetsStateConnector
    extends ConnOp<ContactEditorState, ValuesetsState> {
  final String title;
  final String type;

  ValuesetsStateConnector({
    this.title,
    this.type,
  });

  @override
  ValuesetsState get(ContactEditorState state) =>
      ValuesetsState(title: title, type: type)
        ..items = type == ValueSetType.credential
            ? state.credentials
            : type == ValueSetType.specialty ? state.specialties : [];

  @override
  void set(ContactEditorState state, ValuesetsState subState) {
    switch (type) {
      case ValueSetType.credential:
        {
          state.credentials = subState.items;
        }
        break;
      case ValueSetType.specialty:
        {
          state.specialties = subState.items;
        }
        break;
    }
  }
}

import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:tcga_flutter/database/models.dart';
import 'package:tcga_flutter/database/models/enums.dart';
import 'package:tcga_flutter/database/models/trip.dart';
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/address_item_component/state.dart';
import 'mileage_page/trips_tab_component/trip_item_component/state.dart';

extension EntityToTripItemStateMapper on Trip {
  TripItemState toState() => TripItemState(
        id: this.id,
        fromAddress: this.fromAddress.toState(),
        toAddress: this.toAddress.toState(),
        miles: this.miles,
        duration: this.duration,
        description: this.description,
      );
}

extension TripItemStateToEntityMapper on TripItemState {
  Trip toEntity() => Trip(
        id: this.id,
        fromAddress: this.fromAddress.toEntity(),
        toAddress: this.toAddress.toEntity(),
        miles: this.miles,
        duration: this.duration,
        description: this.description,
      );
}

extension EntityToAddressItemStateMapper on Address {
  AddressItemState toState() => AddressItemState(
        id: this.contactId,
        address: this.addressContent,
        title: this.title,
        city: this.city,
        state: this.stateProvince,
        country: this.country,
        zip: this.zipPostalCode,
        latLng: (this.lat != null && this.lng != null)
            ? LatLng(this.lat, this.lng)
            : null,
      );
}

extension AddressItemStateToEntityMapper on AddressItemState {
  Address toEntity() => Address(
        id: this.id,
        contactId: this.id,
        title: this.title,
        addressContent: this.address,
        city: this.city,
        county: this.title,
        stateProvince: this.state,
        country: this.country,
        zipPostalCode: this.zip,
        lat: this.latLng?.latitude,
        lng: this.latLng?.longitude,
      );
}

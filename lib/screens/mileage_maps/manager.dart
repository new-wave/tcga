import 'package:tcga_flutter/database/models.dart';
import 'package:tcga_flutter/database/models/trip.dart';
import 'package:tcga_flutter/database/repository.dart';
import 'package:tcga_flutter/screens/auth/manager.dart';
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/address_item_component/state.dart';

import '../../utils/utils.dart';
import 'mapper.dart';
import 'mileage_page/trips_tab_component/trip_item_component/state.dart';

class MileageManager {
  static final _singleton = MileageManager._internal();

  factory MileageManager() {
    return _singleton;
  }

  MileageManager._internal();

  final _authManager = AuthManager();
  final _tripRepository = Repository<Trip>();

  final _personRepository = Repository<Person>();
  final _addressRepository = Repository<Address>();

  Future<List<TripItemState>> query({String keyword}) async {
    final currentCaregiverId = _authManager.currentCaregiverId;
    final trips = await _tripRepository.query();

    var items = <TripItemState>[];

    for (int i = 0; i < trips.length; i++) {
      final entity = trips[i];
      items.add(entity.toState()..id = entity.id);
    }
    return items;
  }

  Future<List<TripItemState>> queryTrip({
    String keyword,
    DateTime fromDate,
    DateTime toDate,
    AddressItemState fromAddress,
    AddressItemState toAddress,
  }) async {
    final filter = (Trip trip) {
      bool isOk = true;
      if (fromDate != null && toDate != null) {
        isOk = trip.createdOn.isDateBetween(fromDate, toDate);
        if (!isOk) return isOk;
      }
      if (fromDate == null && toDate != null) {
        isOk = trip.createdOn.isBefore(toDate);
        if (!isOk) return isOk;
      }

      if (fromAddress != null) {
        isOk = trip.fromAddress?.contactId == fromAddress?.id;
        if (!isOk) return isOk;
      }

      if (toAddress != null) {
        isOk = trip.toAddress?.contactId == toAddress?.id;
        if (!isOk) return isOk;
      }

      if (keyword != null && keyword.isNotEmpty) {
        isOk = trip.contains(keyword.trim().toLowerCase());
      }
      return isOk;
    };

    final items = await _tripRepository.query(
      filter: filter,
    );
    items.sort((a, b) => b.createdOn.compareTo(a.createdOn));
    return items.map((x) => x.toState()).toList();
  }

  Future<List<AddressItemState>> getAddressesByContact(String id) async {
    final person = await _personRepository.get(id);
    if (person == null) return [];

    List<AddressItemState> addresses = (person.addresses ?? [])
        .map(
          (x) => AddressItemState(
            id: x.id,
            address: x.addressContent,
            city: x.city,
            state: x.stateProvince,
            zip: x.zipPostalCode,
          ),
        )
        .toList();
    return addresses;
  }

  Future<List<AddressItemState>> queryAddress({
    String keyword,
  }) async {
    final filter = (Address address) {
      bool isOk = true;

      if (keyword != null && keyword.isNotEmpty) {
        isOk = address.contains(keyword.trim().toLowerCase());
      }
      return isOk;
    };

    final addresses = await _addressRepository.query(
      filter: filter,
    );
    addresses.sort((a, b) => b.createdOn.compareTo(a.createdOn));
    List<Address> subListAddress = [];
    subListAddress = addresses.length > 4 ? addresses.sublist(0, 4) : addresses;
    var items = <AddressItemState>[];
    for (int i = 0; i < subListAddress.length; i++) {
      final entity = subListAddress[i];
      items.add(entity.toState());
    }
    return items;
  }

  Future<bool> updateAddressPersonById(AddressItemState state) async {
    Person person = await _personRepository.get(state.id);
    Address address = state?.toEntity();
    List<Address> addresses = [address];
    person.addresses = addresses;
    bool isUpdate = await _personRepository.update(person);
    return isUpdate;
  }

  Future<String> createRecentlyAddress(AddressItemState state) async {
    final address = Address(
      contactId: state.id,
      title: state.title,
      addressContent: state.address,
      city: state.city,
      county: state.title,
      country: state.country,
      stateProvince: state.state,
      zipPostalCode: state.zip,
      isPrimary: false,
      lat: state.latLng?.latitude,
      lng: state.latLng?.longitude,
    );
    final id = await _addressRepository.create(address);
    return id;
  }

  Future deleteAddress(String id) async {
    final addresses = await queryAddress();
    final address = addresses.firstWhere(
      (element) => element.id == id,
    );
    if (address == null) return false;
    await _addressRepository.delete(id);
    return true;
  }

  Future<String> createTrip(TripItemState state) async {
    final trip = Trip(
      id: state.id,
      fromAddress: state.fromAddress.toEntity(),
      toAddress: state.toAddress.toEntity(),
      miles: state.miles,
      duration: state.duration,
      description: state.description,
    );
    final id = await _tripRepository.create(trip);
    return id;
  }

  Future deleteTrip(String id) async {
    final trips = await queryTrip();
    final trip = trips.firstWhere(
      (element) => element.id == id,
    );
    if (trip == null) return false;
    await _tripRepository.delete(id);
    return true;
  }
}

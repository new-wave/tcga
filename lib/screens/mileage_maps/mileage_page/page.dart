import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/trips_tab_component/component.dart';

import 'effect.dart';
import 'reducer.dart';
import 'routes_tab_component/component.dart';
import 'state.dart';
import 'view.dart';

class MileagePage extends Page<MileageState, Map<String, dynamic>> {
  MileagePage()
      : super(
    initState: initState,
    effect: buildEffect(),
    reducer: buildReducer(),
    view: buildView,
    dependencies: Dependencies<MileageState>(
      slots: <String, Dependent<MileageState>>{
        'routes-tab':
        RoutesMileageConnector() + RoutesTabComponent(),
        'trips-tab':
        TripsMileageConnector() + TripsTabComponent(),
      },
    ),
    middleware: <Middleware<MileageState>>[],
  );
}

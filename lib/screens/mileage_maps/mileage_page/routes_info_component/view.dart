import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/database/models/enums.dart';
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/routes_tab_component/result_routes_page/action.dart';
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/trips_tab_component/trip_detail_page/action.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';

import 'state.dart';

Widget buildView(
    RouteInfoState state, Dispatch dispatch, ViewService viewService) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.center,
    mainAxisSize: MainAxisSize.min,
    children: [
      Container(
        padding: EdgeInsets.all(10.0),
        decoration: BoxDecoration(
          border: Border.all(
            width: 2.0,
            color: colorBackground,
          ),
          borderRadius: BorderRadius.all(
              Radius.circular(5.0) //                 <--- border radius here
              ),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: colorDivider,
              offset: Offset(0.5, 0.5),
              blurRadius: 1.5,
            ),
          ],
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              state.title ??
                  AppLocalizations.instance
                      .text('mileage.routes.search_result'),
              style: textStylePrimaryNormal,
            ),
            SizedBox(
              height: 8.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(
                      state.miles?.toStringAsFixed(2) ?? '0',
                      style: textStyleTitleThird,
                    ),
                    Text(
                      AppLocalizations.instance.text('mileage.routes.miles'),
                      style: textStyleHinttext,
                    ),
                  ],
                ),
                Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(
                      state.duration ?? '0',
                      style: textStyleTitleThird,
                    ),
                    Text(
                      AppLocalizations.instance.text('mileage.routes.duration'),
                      style: textStyleHinttext,
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
      InkWell(
        onTap: () {
          if (state.type == RouteInfoScreenType.TripDetail) {
            dispatch(
              TripDetailActionCreator.showTripMap(),
            );
          } else {
            dispatch(
              ResultRoutesActionCreator.showTripMap(),
            );
          }
        },
        child: Container(
          padding: EdgeInsets.all(16.0),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                AppLocalizations.instance.text('mileage.routes.show_map'),
                style: textStylePrimary,
              ),
              SizedBox(
                width: 8.0,
              ),
              SvgPicture.asset(
                "assets/icons/ic_map.svg",
                width: 20,
                height: 20,
              ),
            ],
          ),
        ),
      ),
    ],
  );
}

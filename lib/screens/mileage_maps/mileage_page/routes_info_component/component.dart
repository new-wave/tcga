import 'package:fish_redux/fish_redux.dart';

import 'effect.dart';
import 'reducer.dart';
import 'state.dart';
import 'view.dart';

class RouteInfoComponent extends Component<RouteInfoState> {
  RouteInfoComponent()
      : super(
    effect: buildEffect(),
    reducer: buildReducer(),
    view: buildView,
    dependencies: Dependencies<RouteInfoState>(
        adapter: null, slots: <String, Dependent<RouteInfoState>>{}),
  );
}

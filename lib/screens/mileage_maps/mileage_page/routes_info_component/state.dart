import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/database/models/enums.dart';

class RouteInfoState implements Cloneable<RouteInfoState> {
  String title;
  double miles;
  String duration;

  RouteInfoScreenType type;

  @override
  RouteInfoState clone() {
    return RouteInfoState()
      ..title = title
      ..miles = miles
      ..duration = duration
      ..type = type;
  }
}

RouteInfoState initState(Map<String, dynamic> args) {
  final title = args.containsKey('title') ? args['title'] as String : null;
  final miles = args.containsKey('miles') ? args['miles'] as double : null;
  final duration =
      args.containsKey('duration') ? args['duration'] as String : null;
  final type =
      args.containsKey('type') ? args['type'] as RouteInfoScreenType : null;

  return RouteInfoState()
    ..title = title
    ..miles = miles
    ..duration = duration
    ..type = type;
}

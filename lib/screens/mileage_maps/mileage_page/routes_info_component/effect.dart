import 'package:fish_redux/fish_redux.dart';
import 'action.dart';
import 'state.dart';

Effect<RouteInfoState> buildEffect() {
  return combineEffects(<Object, Effect<RouteInfoState>>{
    RouteInfoAction.action: _onAction,
  });
}

void _onAction(Action action, Context<RouteInfoState> ctx) {
}

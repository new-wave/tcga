import 'package:fish_redux/fish_redux.dart';

import 'action.dart';
import 'state.dart';

Reducer<RouteInfoState> buildReducer() {
  return asReducer(
    <Object, Reducer<RouteInfoState>>{
      RouteInfoAction.action: _onAction,
    },
  );
}

RouteInfoState _onAction(RouteInfoState state, Action action) {
  final RouteInfoState newState = state.clone();
  return newState;
}

import 'package:fish_redux/fish_redux.dart';

import 'action.dart';
import 'state.dart';

Reducer<MileageState> buildReducer() {
  return asReducer(
    <Object, Reducer<MileageState>>{
      MileageAction.initialize: _initialize,
    },
  );
}

MileageState _initialize(MileageState state, Action action) {
  final MileageState newState = state.clone();
 // newState.items = action.payload;
  return newState;
}

// MileageState _handleTeamEdited(MileageState state, Action action) {
//   final item = action.payload as TeamItemState;
//   final newState = state.clone();
//
//   final editedIndex = newState.items.indexWhere(
//         (x) => x.id == item.id,
//   );
//
//   newState.items[editedIndex] = item;
//
//   return newState;
// }

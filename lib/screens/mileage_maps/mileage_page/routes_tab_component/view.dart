import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/widget/address_picker_widget.dart';
import 'package:tcga_flutter/widget/button_widget.dart';
import 'action.dart';
import 'state.dart';

Widget buildView(
    RoutesState state, Dispatch dispatch, ViewService viewService) {
  // final ListAdapter adapter = viewService.buildAdapter();
  return Container(
    padding: EdgeInsets.only(top: 24.0, bottom: 16.0),
    child: Column(
      children: [
        viewService.buildComponent('address-picker-component'),
        Spacer(),
        Container(
          margin: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
          child: ButtonWidget(
            colorButton: colorPrimary,
            textButton: AppLocalizations.of(viewService.context)
                .text('mileage.calculate'),
            onPress: () => dispatch(
              RoutesActionCreator.calculate(),
            ),
            borderRadius: BorderRadius.circular(5.0),
            height: 36.0,
          ),
        ),
      ],
    ),
  );
}

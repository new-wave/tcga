import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/trips_tab_component/trip_item_component/state.dart';
import '../../../../utils/utils.dart';
import '../../../../route_name.dart';
import '../../manager.dart';
import 'action.dart';
import 'state.dart';

final _manager = MileageManager();

Effect<RoutesState> buildEffect() {
  return combineEffects(<Object, Effect<RoutesState>>{
    Lifecycle.initState: _initialize,
    RoutesAction.calculate: _calculate,
  });
}

Future _initialize(Action action, Context<RoutesState> ctx) async {
  ctx.dispatch(
    RoutesActionCreator.initialize(),
  );
}

Future _calculate(Action action, Context<RoutesState> ctx) async {
  if (ctx.state.fromAddress != null && ctx.state.toAddress != null) {
    final result = await ctx.navigateTo(
      RouteName.resultRoute,
      arguments: {
        'from-address': ctx.state.fromAddress,
        'to-address': ctx.state.toAddress,
      },
    );

    if (result == null) return;
    if (result is Map<String, dynamic>) {
      ctx.dispatch(
        RoutesActionCreator.refresh(
          result,
        ),
      );
    }

    if (result is TripItemState) {
      ctx.dispatch(
        RoutesActionCreator.refresh(
          null,
        ),
      );
      // ctx.dispatch(
      //   RoutesActionCreator.tripCreated(
      //     result,
      //   ),
      // );
    }
  }
}

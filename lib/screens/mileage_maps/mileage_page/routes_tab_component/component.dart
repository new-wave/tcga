import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/routes_tab_component/address_picker_component/component.dart';

import 'effect.dart';
import 'reducer.dart';
import 'state.dart';
import 'view.dart';

class RoutesTabComponent extends Component<RoutesState> {
  RoutesTabComponent()
      : super(
    effect: buildEffect(),
    reducer: buildReducer(),
    view: buildView,
    dependencies: Dependencies<RoutesState>(
        slots: <String, Dependent<RoutesState>>{
          'address-picker-component': AddressPickerStateConnector() + AddressPickerComponent(),
        }),
  );
}

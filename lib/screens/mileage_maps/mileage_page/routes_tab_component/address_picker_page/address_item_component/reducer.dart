import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/address_item_component/state.dart';

import '../../state.dart';
import 'action.dart';

Reducer<AddressItemState> buildReducer() {
  return asReducer(
    <Object, Reducer<AddressItemState>>{
      AddressItemAction.action: _onAction,
    },
  );
}

AddressItemState _onAction(AddressItemState state, Action action) {
  final AddressItemState newState = state.clone();
  return newState;
}

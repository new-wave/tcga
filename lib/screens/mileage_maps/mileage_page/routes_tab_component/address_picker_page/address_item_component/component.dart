import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/address_item_component/state.dart';

import '../../state.dart';
import 'effect.dart';
import 'reducer.dart';
import 'view.dart';

class AddressItemComponent extends Component<AddressItemState> {
  AddressItemComponent()
      : super(
    effect: buildEffect(),
    reducer: buildReducer(),
    view: buildView,
    dependencies: Dependencies<AddressItemState>(
        adapter: null, slots: <String, Dependent<AddressItemState>>{}),
  );
}

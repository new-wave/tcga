import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/address_item_component/state.dart';

import '../../state.dart';
import 'action.dart';

Effect<AddressItemState> buildEffect() {
  return combineEffects(<Object, Effect<AddressItemState>>{
    AddressItemAction.action: _onAction,
  });
}

void _onAction(Action action, Context<AddressItemState> ctx) {}

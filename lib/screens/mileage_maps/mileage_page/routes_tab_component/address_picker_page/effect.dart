import 'package:flutter/material.dart' hide Action;

import 'package:fish_redux/fish_redux.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:tcga_flutter/screens/contacts/manager.dart';
import 'package:tcga_flutter/screens/contacts/state.dart';
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/address_item_component/state.dart';
import 'package:tcga_flutter/screens/mileage_maps/manager.dart';
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/trips_tab_component/trip_item_component/state.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import '../../../../../route_name.dart';
import '../../../../../utils/utils.dart';
import '../state.dart';
import 'action.dart';
import 'state.dart';

final _mileageManager = MileageManager();
final _contactManager = ContactManager();

Effect<AddressPickerState> buildEffect() {
  return combineEffects(<Object, Effect<AddressPickerState>>{
    Lifecycle.initState: _init,
    AddressPickerAction.search: _search,
    AddressPickerAction.selectAddress: _selectAddress,
    AddressPickerAction.getMyLocation: _getMyLocation,
    AddressPickerAction.showMap: _showMap,
  });
}

Future _init(Action action, Context<AddressPickerState> ctx) async {
  bool isInTrip = ctx.state.isInTrip;

  List<AddressItemState> mylistAddress = [];
  List<AddressItemState> mylistAddressTemp = [];
  List<AddressItemState> recentlyAddress = [];
  if (!isInTrip) {
    recentlyAddress = await _mileageManager.queryAddress();
    List<ContactItemState> contacts = await _contactManager.query();
    for (ContactItemState contact in contacts) {
      List<AddressItemState> listFullAddress =
          await _mileageManager.getAddressesByContact(contact.id);
      String address = '';
      if ((listFullAddress != null && listFullAddress.isNotEmpty)) {
        if (listFullAddress.length > 1) {
          address =
              "${listFullAddress.length} ${AppLocalizations.instance.text('mileage.routes.addresses')}";
        } else {
          address = listFullAddress.first.address;
        }
      }
      AddressItemState addressItemState = AddressItemState(
        id: contact.id,
        title: contact.fullName,
        address: address,
        city: (listFullAddress != null && listFullAddress.isNotEmpty)
            ? listFullAddress.first.city
            : '',
        zip: (listFullAddress != null && listFullAddress.isNotEmpty)
            ? listFullAddress.first.zip
            : '',
        listFullAddress: listFullAddress,
      );
      mylistAddress.add(addressItemState);
      mylistAddressTemp.add(addressItemState);
    }
  } else {
    List<TripItemState> trips = await _mileageManager.queryTrip();
    for (TripItemState trip in trips) {
      recentlyAddress
          // ignore: null_aware_in_condition
          .add(ctx.state?.isFromAddress ? trip.fromAddress : trip.toAddress);
    }
    mylistAddress = [];
  }

  AddressPickerState newState = ctx.state.clone();
  newState.recentlyAddress = recentlyAddress;
  newState.mylistAddress = mylistAddress;
  newState.mylistAddressTemp = mylistAddressTemp;
  ctx.dispatch(AddressPickerActionCreator.initialize(newState));
}

Future _search(Action action, Context<AddressPickerState> ctx) async {
  final keyword = action.payload as String ?? '';

  AddressPickerState newState = ctx.state.clone();
  List<AddressItemState> addressSearchList = await _mileageManager.queryAddress(
    keyword: keyword,
  );
  newState.recentlyAddress = addressSearchList;
  newState.isSearch = keyword.isNotEmpty;
  if (keyword.isEmpty) {
    newState.mylistAddress = newState.mylistAddressTemp;
  } else {
    newState.mylistAddress = [];
  }

  ctx.dispatch(AddressPickerActionCreator.initialize(newState));
}

Future _selectAddress(Action action, Context<AddressPickerState> ctx) async {
  final selectedItem = action.payload as AddressItemState;
  if (selectedItem == null) return;

  final _ = await _mileageManager.createRecentlyAddress(selectedItem);
  Navigator.of(ctx.context).pop(selectedItem);
}

Future _getMyLocation(Action action, Context<AddressPickerState> ctx) async {
  LatLng currentPosition = await getCurrentLocation();
  AddressItemState address;
  List<Placemark> placeMarks = await placemarkFromCoordinates(
      currentPosition?.latitude, currentPosition?.longitude);
  List<AddressItemState> addresses = [];
  if (placeMarks != null && placeMarks.isNotEmpty) {
    addresses = placeMarks
        .map(
          (e) => AddressItemState(
            id: placeMarks.indexOf(e).toString(),
            title: "",
            address: e.street,
            city: e.locality,
            zip: e.postalCode,
            country: e.country,
            latLng: currentPosition,
          ),
        )
        .toList(growable: false);
  }
  address = addresses?.first ?? null;
  if (address == null) return;

  final _ = await _mileageManager.createRecentlyAddress(address);
  Navigator.of(ctx.context).pop(address);
}

Future _showMap(Action action, Context<AddressPickerState> ctx) async {
  final selectedItem = await ctx.navigateTo(
    RouteName.addressPickerMaps,
  ) as AddressItemState;
  if (selectedItem == null) return;

  final _ = await _mileageManager.createRecentlyAddress(selectedItem);
  Navigator.of(ctx.context).pop(selectedItem);
}

Future<LatLng> getCurrentLocation() async {
  var position = await GeolocatorPlatform.instance
      .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
  return LatLng(position.latitude, position.longitude);
}

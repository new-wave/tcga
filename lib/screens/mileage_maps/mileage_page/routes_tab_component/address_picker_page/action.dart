import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/address_item_component/state.dart';
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/routes_tab_component/address_picker_page/state.dart';

enum AddressPickerAction {
  initialize,
  search,
  selectAddress,
  getMyLocation,
  showMap,
}

class AddressPickerActionCreator {
  static Action initialize(AddressPickerState state) {
    return Action(
      AddressPickerAction.initialize,
      payload: state,
    );
  }

  static Action search(String keyword) {
    return Action(AddressPickerAction.search, payload: keyword);
  }

  static Action selectAddress(AddressItemState item) {
    return Action(AddressPickerAction.selectAddress, payload: item);
  }

  static Action getMyLocation() {
    return Action(AddressPickerAction.getMyLocation);
  }

  static Action showMap() {
    return Action(AddressPickerAction.showMap);
  }
}

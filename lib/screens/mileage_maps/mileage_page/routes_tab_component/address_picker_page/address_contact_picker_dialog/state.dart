import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart' hide Action;
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/address_item_component/state.dart';

class AddressContactPickerState
    implements Cloneable<AddressContactPickerState> {
  AddressItemState selectedOption;
  List<AddressItemState> options;

  @override
  AddressContactPickerState clone() {
    return AddressContactPickerState()
      ..selectedOption = selectedOption
      ..options = options;
  }
}

AddressContactPickerState initState(Map<String, dynamic> args) {
  final selectedOption = args['selectedOption'] as AddressItemState;
  final options = args['options'] as List<AddressItemState>;

  return AddressContactPickerState()
    ..selectedOption = selectedOption ?? ''
    ..options = options ?? [];
}

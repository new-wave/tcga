import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/database/models/enums.dart';
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/address_item_component/state.dart';

import 'action.dart';
import 'state.dart';

Reducer<AddressContactPickerState> buildReducer() {
  return asReducer(
    <Object, Reducer<AddressContactPickerState>>{
    },
  );
}

AddressContactPickerState _handleChooseOption(
  AddressContactPickerState state,
  Action action,
) {
  final AddressContactPickerState newState = state.clone();
  AddressItemState selectedOption = action.payload;
  newState.selectedOption = selectedOption;
  return newState;
}

import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart' hide Action, Page;
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/address_item_component/state.dart';
import '../../../../../../utils/utils.dart';
import 'action.dart';
import 'state.dart';

Effect<AddressContactPickerState> buildEffect() {
  return combineEffects(<Object, Effect<AddressContactPickerState>>{
    AddressContactPickerAction.save: _save,
    AddressContactPickerAction.unFocus: _unFocus,
    AddressContactPickerAction.handleChooseOption: _handleChooseOption,
  });
}

void _save(
  Action action,
  Context<AddressContactPickerState> ctx,
) async {
  ctx.goBack({
    'selectedOption': ctx.state.selectedOption,
  });
}

void _handleChooseOption(
  Action action,
  Context<AddressContactPickerState> ctx,
) {
  ctx.goBack({
    'selectedOption': action.payload as AddressItemState,
  });
}

void _unFocus(Action action, Context<AddressContactPickerState> ctx) {
  FocusScopeNode currentFocus = FocusScope.of(ctx.context);
  if (!currentFocus.hasPrimaryFocus) {
    currentFocus.unfocus();
  }
}

import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/database/models/enums.dart';
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/address_item_component/state.dart';

//TODO replace with your own action
enum AddressContactPickerAction {
  handleChooseOption,
  save,
  unFocus,
}

class AddressContactPickerActionCreator {
  static Action handleChooseOption(AddressItemState address) {
    return Action(
      AddressContactPickerAction.handleChooseOption,
      payload: address,
    );
  }

  static Action save() {
    return Action(
      AddressContactPickerAction.save,
    );
  }

  static Action unFocus() {
    return Action(
      AddressContactPickerAction.unFocus,
    );
  }
}

import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/config/style.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/widget/button_widget.dart';
import 'package:tcga_flutter/widget/entry_widget.dart';

import 'action.dart';
import 'state.dart';

Widget buildView(
  AddressContactPickerState state,
  Dispatch dispatch,
  ViewService viewService,
) {
  double height = 100.0 + (state.options.length * 40);
  return Center(
    child: Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15.0),
        color: Colors.white,
      ),
      height: height > (MediaQuery.of(viewService.context).size.height - 100)
          ? (MediaQuery.of(viewService.context).size.height - 100)
          : height,
      //Change height of dialog box.
      width: MediaQuery.of(viewService.context).size.width - 64,
      child: GestureDetector(
        onTap: () {
          dispatch(AddressContactPickerActionCreator.unFocus());
        },
        child: Material(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(
                height: 16.0,
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: spacingMd),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      AppLocalizations.of(viewService.context)
                          .text('mileage.routes.select_address'),
                      textAlign: TextAlign.start,
                      style: textStylePrimaryTabBar,
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 4.0,
              ),
              Expanded(
                child: ListView.builder(
                  shrinkWrap: true,
                  padding: const EdgeInsets.all(8),
                  itemCount: state.options.length,
                  itemBuilder: (BuildContext context, int index) {
                    return GestureDetector(
                      onTap: () {
                        dispatch(
                          AddressContactPickerActionCreator.handleChooseOption(
                            state.options[index],
                          ),
                        );
                      },
                      child: Container(
                        padding: EdgeInsets.all(10.0),
                        margin: EdgeInsets.all(4.0),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Expanded(
                              child: Text(
                                state?.options[index].fullAddress ?? '',
                                style: Style.labelNormal,
                                overflow: TextOverflow.ellipsis,
                                maxLines: 1,
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  GestureDetector(
                    onTap: () {
                      Navigator.of(viewService.context).pop();
                    },
                    child: Text(
                      AppLocalizations.instance.text('common.cancel'),
                    ),
                  ),
                  SizedBox(
                    width: 16.0,
                  ),
                ],
              ),
              SizedBox(
                height: 16.0,
              ),
            ],
          ),
        ),
      ),
    ),
  );
}

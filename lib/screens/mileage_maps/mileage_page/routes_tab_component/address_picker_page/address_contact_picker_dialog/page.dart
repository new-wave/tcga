import 'package:fish_redux/fish_redux.dart';

import 'effect.dart';
import 'reducer.dart';
import 'state.dart';
import 'view.dart';

class AddressContactPickerPage extends Page<AddressContactPickerState, Map<String, dynamic>> {
  AddressContactPickerPage()
      : super(
    initState: initState,
    effect: buildEffect(),
    reducer: buildReducer(),
    view: buildView,
    dependencies: Dependencies<AddressContactPickerState>(
        adapter: null,
        slots: <String, Dependent<AddressContactPickerState>>{
        }),
    middleware: <Middleware<AddressContactPickerState>>[
    ],);

}

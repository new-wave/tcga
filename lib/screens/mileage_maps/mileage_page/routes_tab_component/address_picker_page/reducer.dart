import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/address_item_component/state.dart';

import 'action.dart';
import 'state.dart';

Reducer<AddressPickerState> buildReducer() {
  return asReducer(
    <Object, Reducer<AddressPickerState>>{
      AddressPickerAction.initialize: _initialize,
    },
  );
}

AddressPickerState _initialize(AddressPickerState state, Action action) {
  final state = action.payload as AddressPickerState;
  return state;
}

import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/address_item_component/state.dart';

import 'action.dart';
import 'state.dart';

Reducer<RecentlyAddressState> buildReducer() {
  return asReducer(
    <Object, Reducer<RecentlyAddressState>>{
      RecentlyAddressAction.action: _onAction,
      RecentlyAddressAction.handleAddressSelected: _handleAddressSelected,
    },
  );
}

RecentlyAddressState _onAction(RecentlyAddressState state, Action action) {
  final RecentlyAddressState newState = state.clone();
  return newState;
}

RecentlyAddressState _handleAddressSelected(
    RecentlyAddressState state, Action action) {
  final RecentlyAddressState newState = state.clone();
  newState.selectedAddress = action.payload as AddressItemState;
  return newState;
}

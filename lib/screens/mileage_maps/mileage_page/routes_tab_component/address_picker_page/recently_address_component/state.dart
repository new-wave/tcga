import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/contacts/state.dart';
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/address_item_component/state.dart';

class RecentlyAddressState extends MutableSource
    implements Cloneable<RecentlyAddressState> {
  List<AddressItemState> items;
  AddressItemState selectedAddress;
  String id;
  bool isInTrip;
  bool isFromAddress;
  bool isSearch;

  @override
  RecentlyAddressState clone() {
    return RecentlyAddressState()
      ..items = items
          .map(
            (e) => e.clone(),
          )
          .toList()
      ..selectedAddress = selectedAddress
      ..id = id
      ..isInTrip = isInTrip
      ..isFromAddress = isFromAddress
      ..isSearch = isSearch;
  }

  @override
  Object getItemData(int index) => items[index];

  @override
  String getItemType(int index) => 'address-item';

  @override
  int get itemCount => items?.length ?? 0;

  @override
  void setItemData(int index, Object data) => items[index] = data;
}

RecentlyAddressState initState(Map<String, dynamic> args) {
  return RecentlyAddressState();
}

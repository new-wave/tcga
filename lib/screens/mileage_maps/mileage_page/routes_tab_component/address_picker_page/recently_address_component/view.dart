import 'dart:math' as math;

import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/address_item_component/state.dart';

import 'action.dart';
import 'state.dart';

Widget buildView(
  RecentlyAddressState state,
  Dispatch dispatch,
  ViewService viewService,
) {
  final adapter = viewService.buildAdapter();
  return SliverList(
    delegate: SliverChildBuilderDelegate(
      (context, index) {
        final int itemIndex = index ~/ 2;
        if (index.isEven) {
          return SizedBox(
            height: 40,
            child: Stack(
              children: [
                adapter.itemBuilder(
                  context,
                  itemIndex,
                ),
                InkWell(
                  onTap: () {
                    dispatch(
                      RecentlyAddressActionCreator.selectAddress(
                        state.getItemData(itemIndex),
                      ),
                    );
                  },
                  child: SizedBox.expand(
                    child: Container(
                      color: Colors.transparent,
                    ),
                  ),
                )
              ],
            ),
          );
        }
        return SizedBox(height: 8);
      },
      semanticIndexCallback: (Widget widget, int localIndex) {
        if (localIndex.isEven) {
          return localIndex ~/ 2;
        }
        return null;
      },
      childCount: math.max(0, adapter.itemCount * 2 - 1),
    ),
  );
}

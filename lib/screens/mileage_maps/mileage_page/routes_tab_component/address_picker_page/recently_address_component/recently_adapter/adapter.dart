import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/routes_tab_component/address_picker_page/recently_address_component/recently_item_component/component.dart';

import '../state.dart';

class RecentlyAddressAdapter extends SourceFlowAdapter<RecentlyAddressState> {
  RecentlyAddressAdapter()
      : super(
    pool: <String, Component<Object>>{
      'address-item': RecentlyItemComponent(),
    },
  );
}

import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/address_item_component/state.dart';
import '../../../../manager.dart';
import 'action.dart';
import 'state.dart';
import 'package:flutter/material.dart' hide Action;

final _mileageManager = MileageManager();

Effect<RecentlyAddressState> buildEffect() {
  return combineEffects(<Object, Effect<RecentlyAddressState>>{
    RecentlyAddressAction.action: _onAction,
    RecentlyAddressAction.selectAddress: _selectAddress,
  });
}

void _onAction(Action action, Context<RecentlyAddressState> ctx) {}

void _selectAddress(Action action, Context<RecentlyAddressState> ctx) async {
  AddressItemState selectedItem = action.payload as AddressItemState;

  final _ = await _mileageManager.createRecentlyAddress(selectedItem);
  Navigator.of(ctx.context).pop(selectedItem);
  // ctx.dispatch(
  //   RecentlyAddressActionCreator.handleAddressSelected(
  //     action.payload as AddressItemState,
  //   ),
  // );
}

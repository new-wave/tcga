import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/routes_tab_component/address_picker_page/recently_address_component/recently_adapter/adapter.dart';

import 'effect.dart';
import 'reducer.dart';
import 'state.dart';
import 'view.dart';

class RecentlyAddressComponent extends Component<RecentlyAddressState> {
  RecentlyAddressComponent()
      : super(
    effect: buildEffect(),
    reducer: buildReducer(),
    view: buildView,
    dependencies: Dependencies<RecentlyAddressState>(
        adapter: NoneConn<RecentlyAddressState>() + RecentlyAddressAdapter(),
        slots: <String, Dependent<RecentlyAddressState>>{}),
  );
}

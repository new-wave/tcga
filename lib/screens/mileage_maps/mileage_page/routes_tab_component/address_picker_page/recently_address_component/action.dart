import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/address_item_component/state.dart';

//TODO replace with your own action
enum RecentlyAddressAction {
  action,
  selectAddress,
  handleAddressSelected,
}

class RecentlyAddressActionCreator {
  static Action onAction() {
    return const Action(RecentlyAddressAction.action);
  }

  static Action selectAddress(AddressItemState item) {
    return Action(
      RecentlyAddressAction.selectAddress,
      payload: item,
    );
  }

  static Action handleAddressSelected(AddressItemState item) {
    return Action(
      RecentlyAddressAction.handleAddressSelected,
      payload: item,
    );
  }
}

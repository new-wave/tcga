import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/address_item_component/state.dart';
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/routes_tab_component/address_picker_page/mylist_address_component/state.dart';
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/routes_tab_component/address_picker_page/recently_address_component/state.dart';

import '../state.dart';

class AddressPickerState implements Cloneable<AddressPickerState> {
  List<AddressItemState> recentlyAddress;
  List<AddressItemState> mylistAddress;
  List<AddressItemState> mylistAddressTemp;
  AddressItemState selectedAddress;
  String title;
  bool isInTrip;
  bool isFromAddress;
  bool isSearch;

  @override
  AddressPickerState clone() {
    return AddressPickerState()
      ..recentlyAddress = recentlyAddress
      ..mylistAddress = mylistAddress
      ..mylistAddressTemp = mylistAddressTemp
      ..selectedAddress = selectedAddress
      ..isInTrip = isInTrip
      ..isFromAddress = isFromAddress
      ..isSearch = isSearch
      ..title = title;
  }
}

AddressPickerState initState(Map<String, dynamic> args) {
  final title = args.containsKey('title') ? args['title'] as String : null;
  final isInTrip =
      args.containsKey('is-in-trip') ? args['is-in-trip'] as bool : null;
  final isFromAddress =
  args.containsKey('is-from-address') ? args['is-from-address'] as bool : null;

  return AddressPickerState()
    ..recentlyAddress = []
    ..mylistAddress = []
    ..mylistAddressTemp = []
    ..selectedAddress = AddressItemState()
    ..title = title
    ..isInTrip = isInTrip
    ..isFromAddress = isFromAddress
    ..isSearch = false;
}

class RecentlyStateConnector
    extends ConnOp<AddressPickerState, RecentlyAddressState> {
  @override
  RecentlyAddressState get(AddressPickerState state) => RecentlyAddressState()
    ..items = state.recentlyAddress
    ..selectedAddress = state.selectedAddress
    ..isInTrip = state.isInTrip
    ..isFromAddress = state.isFromAddress
    ..isSearch = state.isSearch;

  @override
  void set(AddressPickerState state, RecentlyAddressState subState) {
    state.recentlyAddress = subState.items
        .map(
          (e) => e.clone(),
        )
        .toList();
    state.selectedAddress = subState.selectedAddress;
    state.isInTrip = subState.isInTrip;
    state.isSearch = subState.isSearch;
    state.isFromAddress = subState.isFromAddress;
  }
}

class MyListStateConnector
    extends ConnOp<AddressPickerState, MyListAddressState> {
  @override
  MyListAddressState get(AddressPickerState state) => MyListAddressState()
    ..items = state.mylistAddress
    ..itemsTemp = state.mylistAddressTemp
    ..selectedAddress = state.selectedAddress
    ..isFromAddress = state.isFromAddress;

  @override
  void set(AddressPickerState state, MyListAddressState subState) {
    state.mylistAddress = subState.items
        .map(
          (e) => e.clone(),
        )
        .toList();
    state.mylistAddressTemp = subState.itemsTemp
        .map(
          (e) => e.clone(),
    )
        .toList();
    state.selectedAddress = subState.selectedAddress;
    state.isFromAddress = subState.isFromAddress;
  }
}

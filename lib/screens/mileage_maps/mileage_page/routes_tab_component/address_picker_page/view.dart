import 'dart:math' as math;

import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/widget/entry_widget.dart';

import 'action.dart';
import 'state.dart';

Widget buildView(
  AddressPickerState state,
  Dispatch dispatch,
  ViewService viewService,
) {
  return Container(
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(
        15,
      ),
    ),
    child: Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          state?.title ?? '',
        ),
        actions: [
          InkWell(
            onTap: () {
              Navigator.of(viewService.context).pop();
            },
            child: SizedBox(
              height: 48,
              width: 48,
              child: Padding(
                padding: const EdgeInsets.all(12),
                child: SvgPicture.asset('assets/icons/ic_close.svg'),
              ),
            ),
          )
        ],
      ),
      body: NestedScrollView(
        headerSliverBuilder: (context, bool) => [
          SliverToBoxAdapter(
            child: Column(
              children: [
                Container(
                  padding: EdgeInsets.only(top: 16, left: 16, right: 16),
                  child: EntryWidget(
                    title: AppLocalizations.of(viewService.context).text(
                        'expenses.currencies.placeholder_enter_to_search'),
                    suffixIcon: SvgPicture.asset(
                      'assets/icons/ic_search.svg',
                    ),
                    onTextChanged: (text) {
                      dispatch(
                        AddressPickerActionCreator.search(
                          text,
                        ),
                      );
                    },
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    InkWell(
                      onTap: () => dispatch(
                        AddressPickerActionCreator.getMyLocation(),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 16,
                          vertical: 16,
                        ),
                        child: Row(
                          children: [
                            SvgPicture.asset(
                              'assets/icons/ic_address.svg',
                            ),
                            SizedBox(
                              width: 8.0,
                            ),
                            Text(
                              AppLocalizations.instance
                                  .text('mileage.routes.my_location'),
                              style: textStylePrimary,
                            ),
                          ],
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () => dispatch(
                        AddressPickerActionCreator.showMap(),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 16,
                          vertical: 16,
                        ),
                        child: Row(
                          children: [
                            Text(
                              AppLocalizations.instance
                                  .text('mileage.routes.show_map'),
                              style: textStylePrimary,
                            ),
                            SizedBox(
                              width: 8.0,
                            ),
                            SvgPicture.asset(
                              'assets/icons/ic_map.svg',
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
        body: CustomScrollView(
          slivers: [
            SliverToBoxAdapter(
              child: Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 16,
                ),
                child: Text(
                  (state.isInTrip || state.isSearch)
                      ? AppLocalizations.of(viewService.context)
                          .text('mileage.routes.select_address')
                      : AppLocalizations.of(viewService.context)
                          .text('mileage.routes.recently'),
                  style: textStylePrimary,
                ),
              ),
            ),
            SliverToBoxAdapter(
              child: SizedBox(
                height: 16,
              ),
            ),
            viewService.buildComponent('recently-component'),
            SliverToBoxAdapter(
              child: SizedBox(
                height: 16,
              ),
            ),
            SliverToBoxAdapter(
              child: Row(
                children: [
                  SizedBox(
                    width: 16,
                  ),
                  Text(
                    (state.mylistAddress == null ||
                            // ignore: null_aware_in_logical_operator
                            state.mylistAddress?.isEmpty)
                        ? ''
                        : AppLocalizations.of(viewService.context)
                            .text('mileage.routes.mylist'),
                    style: textStylePrimary,
                  ),
                  Spacer(),
                ],
              ),
            ),
            SliverToBoxAdapter(
              child: SizedBox(
                height: 16,
              ),
            ),
            viewService.buildComponent('mylist-component'),
          ],
        ),
      ),
    ),
  );
}

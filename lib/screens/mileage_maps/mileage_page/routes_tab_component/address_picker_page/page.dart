import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/routes_tab_component/address_picker_page/recently_address_component/component.dart';

import 'effect.dart';
import 'mylist_address_component/component.dart';
import 'reducer.dart';
import 'state.dart';
import 'view.dart';

class AddressPickerPage extends Page<AddressPickerState, Map<String, dynamic>> {
  AddressPickerPage()
      : super(
          initState: initState,
          effect: buildEffect(),
          reducer: buildReducer(),
          view: buildView,
          dependencies: Dependencies<AddressPickerState>(
              adapter: null,
              slots: <String, Dependent<AddressPickerState>>{
                'recently-component':
                    RecentlyStateConnector() + RecentlyAddressComponent(),
                'mylist-component':
                    MyListStateConnector() + MyListAddressComponent(),
              }),
          middleware: <Middleware<AddressPickerState>>[],
        );
}

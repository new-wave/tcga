import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/address_item_component/state.dart';

import 'effect.dart';
import 'mylist_adapter/adapter.dart';
import 'reducer.dart';
import 'state.dart';
import 'view.dart';

class MyListAddressComponent extends Component<MyListAddressState> {
  MyListAddressComponent()
      : super(
    effect: buildEffect(),
    reducer: buildReducer(),
    view: buildView,
    dependencies: Dependencies<MyListAddressState>(
        adapter: NoneConn<MyListAddressState>() + MyListAddressAdapter(),
        slots: <String, Dependent<MyListAddressState>>{}),
  );
}

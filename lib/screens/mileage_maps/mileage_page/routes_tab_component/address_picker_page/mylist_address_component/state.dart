import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/contacts/state.dart';
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/address_item_component/state.dart';

class MyListAddressState extends MutableSource
    implements Cloneable<MyListAddressState> {
  List<AddressItemState> items;
  List<AddressItemState> itemsTemp;
  AddressItemState selectedAddress;
  String id;
  bool isFromAddress;

  @override
  MyListAddressState clone() {
    return MyListAddressState()
      ..items = items
          .map(
            (e) => e.clone(),
          )
          .toList()
      ..itemsTemp = itemsTemp
          .map(
            (e) => e.clone(),
      )
          .toList()
      ..selectedAddress = selectedAddress
      ..id = id
      ..isFromAddress = isFromAddress;
  }

  @override
  Object getItemData(int index) => items[index];

  @override
  String getItemType(int index) => 'address-item';

  @override
  int get itemCount => items?.length ?? 0;

  @override
  void setItemData(int index, Object data) => items[index] = data;
}

MyListAddressState initState(Map<String, dynamic> args) {
  return MyListAddressState();
}

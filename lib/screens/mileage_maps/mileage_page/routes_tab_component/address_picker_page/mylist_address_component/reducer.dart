import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/address_item_component/state.dart';

import 'action.dart';
import 'state.dart';

Reducer<MyListAddressState> buildReducer() {
  return asReducer(
    <Object, Reducer<MyListAddressState>>{
      MyListAddressAction.action: _onAction,
      MyListAddressAction.handleAddressSelected: _handleAddressSelected,
    },
  );
}

MyListAddressState _onAction(MyListAddressState state, Action action) {
  final MyListAddressState newState = state.clone();
  return newState;
}

MyListAddressState _handleAddressSelected(
    MyListAddressState state, Action action) {
  final MyListAddressState newState = state.clone();
  newState.selectedAddress = action.payload as AddressItemState;
  return newState;
}

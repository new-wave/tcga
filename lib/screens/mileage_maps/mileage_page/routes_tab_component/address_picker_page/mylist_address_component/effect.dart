import 'package:fish_redux/fish_redux.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:tcga_flutter/database/models.dart';
import 'package:tcga_flutter/models/place_api.dart';
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/address_item_component/state.dart';
import 'package:tcga_flutter/utils/show_picker.dart';
import '../../../../../../route_name.dart';
import '../../../../manager.dart';
import 'action.dart';
import 'state.dart';
import 'package:flutter/material.dart' hide Action;

final _mileageManager = MileageManager();

Effect<MyListAddressState> buildEffect() {
  return combineEffects(<Object, Effect<MyListAddressState>>{
    MyListAddressAction.action: _onAction,
    MyListAddressAction.selectAddress: _selectAddress,
  });
}

void _onAction(Action action, Context<MyListAddressState> ctx) {}

void _selectAddress(Action action, Context<MyListAddressState> ctx) async {
  AddressItemState selectedItem = action.payload as AddressItemState;
  // ignore: null_aware_before_operator
  if (selectedItem?.listFullAddress != null &&
      // ignore: null_aware_in_logical_operator
      selectedItem?.listFullAddress?.isNotEmpty) {
    // ignore: null_aware_before_operator
    if (selectedItem?.listFullAddress?.length > 1) {
      final item = await showCustomDialog(
        ctx.context,
        RouteName.addressContactPicker,
        args: {
          'selectedOption': selectedItem?.listFullAddress?.first,
          'options': selectedItem?.listFullAddress,
        },
      );

      if (item == null) return;
      AddressItemState address = item['selectedOption'] ?? null;
      selectedItem.address = address.address;

      LatLng latLng = await getLocationFromAddress(address?.fullAddress);
      selectedItem.latLng = latLng;
    } else {
      LatLng latLng = await getLocationFromAddress(selectedItem?.fullAddress);
      selectedItem.listFullAddress = [selectedItem];
      selectedItem.latLng = latLng;
    }
    final _ = await _mileageManager.createRecentlyAddress(selectedItem);
    Navigator.of(ctx.context).pop(selectedItem);
  } else {
    final result =
        await showModal2(ctx.context, RouteName.addressEditor, args: {
      'item': null,
    }) as AddressItemState;

    if (result != null) {
      selectedItem.address = result.address;
      selectedItem.city = result.city;
      selectedItem.state = result.state;
      selectedItem.zip = result.zip;
      selectedItem.country = result.country;

      LatLng latLng = await getLocationFromAddress(selectedItem?.fullAddress);
      selectedItem.latLng = latLng;

      AddressItemState address = selectedItem.clone()..listFullAddress = [];
      selectedItem.listFullAddress = [address];

      final isUpdate = await _mileageManager.updateAddressPersonById(selectedItem);
      final _ = await _mileageManager.createRecentlyAddress(selectedItem);
      Navigator.of(ctx.context).pop(selectedItem);
    }
  }

}

Future<LatLng> getLocationFromAddress(String address) async {
  if (address != null) {
    LatLng location;
    try {
      location = await PlaceApiProvider().getLocationFromAddress(address ?? '');
      return location;
    } catch (error) {
      return null;
    }
  } else {
    return null;
  }
}

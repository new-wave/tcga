import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/address_item_component/state.dart';

//TODO replace with your own action
enum MyListAddressAction {
  action,
  selectAddress,
  handleAddressSelected,
}

class MyListAddressActionCreator {
  static Action onAction() {
    return const Action(MyListAddressAction.action);
  }

  static Action selectAddress(AddressItemState item) {
    return Action(
      MyListAddressAction.selectAddress,
      payload: item,
    );
  }

  static Action handleAddressSelected(AddressItemState item) {
    return Action(
      MyListAddressAction.handleAddressSelected,
      payload: item,
    );
  }
}

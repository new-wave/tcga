import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/address_item_component/state.dart';
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/routes_tab_component/address_picker_page/mylist_address_component/mylist_item_component/component.dart';

import '../state.dart';

class MyListAddressAdapter extends SourceFlowAdapter<MyListAddressState> {
  MyListAddressAdapter()
      : super(
    pool: <String, Component<Object>>{
      'address-item': MyListItemComponent(),
    },
  );
}

import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/cupertino.dart';
import 'package:geolocator/geolocator.dart';
import 'package:tcga_flutter/database/models/enums.dart';
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/address_item_component/state.dart';
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/routes_info_component/state.dart';
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/routes_tab_component/address_picker_component/state.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';

class ResultRoutesState implements Cloneable<ResultRoutesState> {
  String title;
  AddressItemState fromAddress;
  AddressItemState toAddress;
  double miles;
  String duration;
  TextEditingController descriptionEditingController;

  @override
  ResultRoutesState clone() {
    return ResultRoutesState()
      ..fromAddress = fromAddress
      ..toAddress = toAddress
      ..title = title
      ..miles = miles
      ..duration = duration
      ..descriptionEditingController = descriptionEditingController;
  }
}

ResultRoutesState initState(Map<String, dynamic> args) {
  final fromAddress = args.containsKey('from-address')
      ? args['from-address'] as AddressItemState
      : null;
  final toAddress = args.containsKey('to-address')
      ? args['to-address'] as AddressItemState
      : null;

  return ResultRoutesState()
    ..fromAddress = fromAddress
    ..toAddress = toAddress
    ..title = AppLocalizations.instance.text('mileage.routes.search_result')
    ..miles = 0.0
    ..duration = '0.0'
    ..descriptionEditingController = TextEditingController(
      text: '',
    );
}

class AddressPickerStateConnector
    extends ConnOp<ResultRoutesState, AddressPickerState>
    with ReselectMixin<ResultRoutesState, AddressPickerState> {
  @override
  AddressPickerState computed(ResultRoutesState state) {
    return AddressPickerState()
      ..fromAddress = state.fromAddress
      ..toAddress = state.toAddress;
  }

  @override
  List<dynamic> factors(ResultRoutesState state) {
    return [
      state.fromAddress,
      state.toAddress,
    ];
  }

  @override
  void set(ResultRoutesState state, AddressPickerState subState) {
    state.fromAddress = subState.fromAddress;
    state.toAddress = subState.toAddress;
  }
}

class RouteInfoStateConnector extends ConnOp<ResultRoutesState, RouteInfoState>
    with ReselectMixin<ResultRoutesState, RouteInfoState> {
  @override
  RouteInfoState computed(ResultRoutesState state) {
    return RouteInfoState()
      ..title = state.title
      ..miles = state.miles
      ..duration = state.duration
      ..type = RouteInfoScreenType.ResultRoutes;
  }

  @override
  List<dynamic> factors(ResultRoutesState state) {
    return [
      state.title,
      state.miles,
      state.duration,
    ];
  }

  @override
  void set(ResultRoutesState state, RouteInfoState subState) {
    state.title = subState.title;
    state.miles = subState.miles;
    state.duration = subState.duration;
  }
}

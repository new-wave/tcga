import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/database/models/maps/element_matrix.dart';
import 'package:tcga_flutter/models/place_api.dart';

//TODO replace with your own action
enum ResultRoutesAction {
  initialize,
  onBack,
  saveTrip,
  showTripMap,
}

class ResultRoutesActionCreator {
  static Action initialize(ElementMatrix elementMatrix) {
    return Action(
      ResultRoutesAction.initialize,
      payload: elementMatrix,
    );
  }

  static Action onBack() {
    return const Action(ResultRoutesAction.onBack);
  }

  static Action saveTrip() {
    return const Action(ResultRoutesAction.saveTrip);
  }

  static Action showTripMap() {
    return const Action(ResultRoutesAction.showTripMap);
  }
}

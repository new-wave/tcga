import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/routes_info_component/component.dart';
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/routes_tab_component/address_picker_component/component.dart';
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/trips_tab_component/component.dart';

import 'effect.dart';
import 'reducer.dart';
import 'state.dart';
import 'view.dart';

class ResultRoutesPage extends Page<ResultRoutesState, Map<String, dynamic>> {
  ResultRoutesPage()
      : super(
          initState: initState,
          effect: buildEffect(),
          reducer: buildReducer(),
          view: buildView,
          dependencies: Dependencies<ResultRoutesState>(
            slots: <String, Dependent<ResultRoutesState>>{
              'address-picker-component':
                  AddressPickerStateConnector() + AddressPickerComponent(),
              'route-info-component':
                  RouteInfoStateConnector() + RouteInfoComponent(),
            },
          ),
          middleware: <Middleware<ResultRoutesState>>[],
        );
}

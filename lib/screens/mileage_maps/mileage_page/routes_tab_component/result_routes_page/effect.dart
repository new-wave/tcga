import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart' hide Action;
import 'package:tcga_flutter/database/models/maps/element_matrix.dart';
import 'package:tcga_flutter/models/place_api.dart';
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/trips_tab_component/trip_item_component/state.dart';
import 'package:uuid/uuid.dart';
import '../../../../../route_name.dart';
import '../../../manager.dart';
import '../../../../../utils/utils.dart';
import '../action.dart';
import 'action.dart';
import 'state.dart';

final _mileageManager = MileageManager();

Effect<ResultRoutesState> buildEffect() {
  return combineEffects(<Object, Effect<ResultRoutesState>>{
    Lifecycle.initState: _initialize,
    ResultRoutesAction.onBack: _onBack,
    ResultRoutesAction.saveTrip: _saveTrip,
    ResultRoutesAction.showTripMap: _showTripMap,
  });
}

Future _initialize(Action action, Context<ResultRoutesState> ctx) async {
  ElementMatrix element = await PlaceApiProvider().getDistanceMatrix(
    ctx.state?.fromAddress?.latLng,
    ctx.state?.toAddress?.latLng,
  );

  ctx.dispatch(
    ResultRoutesActionCreator.initialize(element),
  );
}

Future _onBack(Action action, Context<ResultRoutesState> ctx) async {
  Navigator.of(ctx.context).pop({
    'from-address': ctx.state.fromAddress,
    'to-address': ctx.state.toAddress,
  });
}

Future _showTripMap(Action action, Context<ResultRoutesState> ctx) async {
  final result = await ctx.navigateTo(
    RouteName.tripMapDetail,
    arguments: {
      'from-address': ctx.state.fromAddress,
      'to-address': ctx.state.toAddress,
      'miles': ctx.state.miles,
    },
  );
  if (result == null) return;
}

Future _saveTrip(Action action, Context<ResultRoutesState> ctx) async {
  ResultRoutesState newState = ctx.state.clone();
  TripItemState tripState = TripItemState(
    fromAddress: newState.fromAddress,
    toAddress: newState.toAddress,
    miles: newState.miles,
    duration: newState.duration,
    description: newState.descriptionEditingController.text,
  );
  final trip = await _mileageManager.createTrip(tripState);
  Navigator.of(ctx.context).pop(tripState);
}

import 'package:fish_redux/fish_redux.dart';
import 'package:geolocator/geolocator.dart';
import 'package:tcga_flutter/database/models/maps/element_matrix.dart';
import 'package:tcga_flutter/models/place_api.dart';
import '../state.dart';
import 'action.dart';
import 'state.dart';

var items = List<ResultRoutesState>();

Reducer<ResultRoutesState> buildReducer() {
  return asReducer(
    <Object, Reducer<ResultRoutesState>>{
      ResultRoutesAction.initialize: _initialize,
    },
  );
}

ResultRoutesState _initialize(ResultRoutesState state, Action action) {
  final ResultRoutesState newState = state.clone();
  ElementMatrix element = action.payload;
  double miles = 0;
  String duration = '0';
  if (element != null) {
    miles = (element.distance?.value ?? 0) / 1609.344;
    duration = element.duration?.text ?? '';
  } else {
    if (newState?.fromAddress?.latLng?.latitude != null &&
        newState?.fromAddress?.latLng?.longitude != null &&
        newState?.toAddress?.latLng?.latitude != null &&
        newState?.toAddress?.latLng?.longitude != null) {
      miles = Geolocator.distanceBetween(
                  newState?.fromAddress?.latLng?.latitude,
                  newState?.fromAddress?.latLng?.longitude,
                  newState?.toAddress?.latLng?.latitude,
                  newState?.toAddress?.latLng?.longitude) /
              1000 ??
          0;
      duration = '0';
    }
  }
  newState.miles = miles;
  newState.duration = duration;
  return newState;
}

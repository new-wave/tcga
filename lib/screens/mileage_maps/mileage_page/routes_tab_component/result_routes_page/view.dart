import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/widget/button_widget.dart';
import 'package:tcga_flutter/widget/text_widget.dart';

import 'action.dart';
import 'state.dart';

Widget buildView(
    ResultRoutesState state, Dispatch dispatch, ViewService viewService) {
  // final ListAdapter adapter = viewService.buildAdapter();
  return Scaffold(
    backgroundColor: Colors.white,
    appBar: _appBar(viewService, dispatch),
    body: Container(
      padding: EdgeInsets.only(top: 16.0, bottom: 16.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          viewService.buildComponent('address-picker-component'),
          SizedBox(
            height: 20.0,
          ),
          Container(
            margin: EdgeInsets.symmetric(
              horizontal: 16.0,
            ),
            child: viewService.buildComponent('route-info-component'),
          ),
          Spacer(),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 16.0),
            child: TextWidget(
              controller: state.descriptionEditingController,
              hinText: AppLocalizations.of(viewService.context)
                  .text("notes.description"),
              textStyle: textStylePrimary,
              obscureText: false,
              height: 100,
              keyboardType: TextInputType.name,
              textInputAction: TextInputAction.next,
              isAlignCenter: false,
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
            child: ButtonWidget(
              colorButton: colorPrimary,
              textButton: AppLocalizations.of(viewService.context)
                  .text('mileage.save_trip'),
              onPress: () => dispatch(
                ResultRoutesActionCreator.saveTrip(),
              ),
              borderRadius: BorderRadius.circular(5.0),
              height: 36.0,
            ),
          ),
        ],
      ),
    ),
  );
}

AppBar _appBar(ViewService viewService, Dispatch dispatch) {
  return AppBar(
    iconTheme: IconThemeData(color: Colors.black),
    elevation: 3,
    backgroundColor: Color(0xFFFFFFFF),
    leading: IconButton(
      icon: SvgPicture.asset(
        "assets/icons/ic_back.svg",
        width: 24,
        height: 24,
      ),
      onPressed: () => dispatch(
        ResultRoutesActionCreator.onBack(),
      ),
    ),
    centerTitle: false,
    title: Text(
      AppLocalizations.of(viewService.context).text('mileage.title'),
      style: TextStyle(
          color: Color(0xff4F4F4F), fontSize: 20, fontWeight: FontWeight.w400),
    ),
    actions: [],
  );
}

import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/address_item_component/state.dart';
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/trips_tab_component/trip_item_component/state.dart';
import '../state.dart';
import 'action.dart';
import 'state.dart';

var items = List<RoutesState>();

Reducer<RoutesState> buildReducer() {
  return asReducer(
    <Object, Reducer<RoutesState>>{
      RoutesAction.initialize: _initialize,
      RoutesAction.refresh: _refresh,
      RoutesAction.tripCreated: _tripCreated,
    },
  );
}

RoutesState _initialize(RoutesState state, Action action) {
//  final expenseItems = action.payload ?? <RoutesState>[];
  final RoutesState newState = state.clone();
//  items = expenseItems;
//  newState.items = expenseItems;

  return newState;
}

RoutesState _refresh(RoutesState state, Action action) {
  final RoutesState newState = state.clone();
  if (action.payload != null) {
    final fromAddress = action.payload.containsKey('from-address')
        ? action.payload['from-address'] as AddressItemState
        : null;
    final toAddress = action.payload.containsKey('to-address')
        ? action.payload['to-address'] as AddressItemState
        : null;
    newState.fromAddress = fromAddress;
    newState.toAddress = toAddress;
  } else {
    newState.fromAddress = null;
    newState.toAddress = null;
  }
  return newState;
}

RoutesState _tripCreated(RoutesState state, Action action) {
  final RoutesState newState = state.clone();
  final trip = action.payload as TripItemState;
  List<TripItemState> trips = newState.tripTodayList ?? [];
  trips.insert(0, trip);
  newState.tripTodayList = trips;
  return newState;
}

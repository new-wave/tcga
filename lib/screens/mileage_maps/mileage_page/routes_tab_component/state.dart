import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/address_item_component/state.dart';
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/trips_tab_component/trip_item_component/state.dart';

import 'address_picker_component/state.dart';

class RoutesState implements Cloneable<RoutesState> {
  AddressItemState fromAddress;
  AddressItemState toAddress;

  List<TripItemState> tripTodayList;
  List<TripItemState> tripYesterdayList;
  List<TripItemState> tripOlderList;

  @override
  RoutesState clone() {
    return RoutesState()
      ..fromAddress = fromAddress
      ..toAddress = toAddress
      ..tripTodayList = tripTodayList
      ..tripYesterdayList = tripYesterdayList
      ..tripOlderList = tripOlderList;
  }
}

RoutesState initState(Map<String, dynamic> args) {
  return RoutesState();
}

class AddressPickerStateConnector
    extends ConnOp<RoutesState, AddressPickerState> {
  @override
  AddressPickerState get(RoutesState state) {
    return AddressPickerState()
      ..fromAddress = state.fromAddress
      ..toAddress = state.toAddress;
  }

  @override
  void set(RoutesState state, AddressPickerState subState) {
    state.fromAddress = subState.fromAddress;
    state.toAddress = subState.toAddress;
  }
}

import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/routes_tab_component/maps_page/address_item_component/component.dart';
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/routes_tab_component/maps_page/address_item_component/view.dart';

import '../state.dart';

class AddressAdapter extends SourceFlowAdapter<MapsState> {
  AddressAdapter()
      : super(
    pool: <String, Component<Object>>{'address-item': AddressItemComponent()},
  );
}

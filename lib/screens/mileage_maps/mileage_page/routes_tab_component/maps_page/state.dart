import 'dart:async';

import 'package:fish_redux/fish_redux.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:tcga_flutter/models/place_api.dart';
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/address_item_component/state.dart';

class MapsState extends MutableSource implements Cloneable<MapsState> {
  String title;
  Completer<GoogleMapController> controller;
  LatLng currentPosition;
  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
  List<AddressItemState> addresses;
  List<Suggestion> suggestions;
  bool isLoadingAddress;

  @override
  MapsState clone() {
    return MapsState()
      ..title = title
      ..controller = controller
      ..currentPosition = currentPosition
      ..markers = markers
      ..addresses = addresses
      ..suggestions = suggestions
      ..isLoadingAddress = isLoadingAddress;
  }

  @override
  Object getItemData(int index) => addresses[index];

  @override
  String getItemType(int index) => 'address-item';

  @override
  int get itemCount => addresses?.length ?? 0;

  @override
  void setItemData(int index, Object data) => addresses[index] = data;
}

MapsState initState(Map<String, dynamic> args) {
  return MapsState()
    ..title = ''
    ..controller = Completer()
    ..markers = <MarkerId, Marker>{}
    ..addresses = []
    ..suggestions = []
    ..isLoadingAddress = false;
}

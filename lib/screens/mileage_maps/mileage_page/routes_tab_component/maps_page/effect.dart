import 'dart:async';

import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart' hide Action;
import 'package:geocoding/geocoding.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:tcga_flutter/models/place_api.dart';
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/address_item_component/state.dart';
import 'package:tcga_flutter/utils/maps_utils.dart';
import 'package:tcga_flutter/utils/show_picker.dart';
import 'package:uuid/uuid.dart';

import 'action.dart';
import 'state.dart';

Timer timerGetAddress;
Timer timerSearchSuggestion;
BitmapDescriptor markerIcon;
List<AddressItemState> addresses;

Effect<MapsState> buildEffect() {
  return combineEffects(<Object, Effect<MapsState>>{
    Lifecycle.initState: _initialize,
    MapsAction.search: _search,
    MapsAction.createMap: _createMap,
    MapsAction.moveCamera: _moveCamera,
    MapsAction.selectAddress: _selectAddress,
    MapsAction.selectSuggestion: _selectSuggestion,
    MapsAction.showInfoWindowMarker: _showInfoWindowMarker,
  });
}

Future _initialize(Action action, Context<MapsState> ctx) async {
  MapsState newState = ctx.state.clone();

  LatLng currentPosition = await MapsUtils.getCurrentLocation();
  await getAddress(currentPosition, ctx);
  newState.currentPosition = currentPosition;

  ctx.dispatch(
    MapsActionCreator.initialize(newState),
  );
}

Future _selectAddress(Action action, Context<MapsState> ctx) async {
  AddressItemState address = action.payload;
  if (address == null) return;
  Navigator.of(ctx.context).pop(address);
}

Future _selectSuggestion(Action action, Context<MapsState> ctx) async {
  Suggestion address = action.payload;
  if (address == null) return;

  String fullAddress = address.description ?? '';
  LatLng location =
      await PlaceApiProvider().getLocationFromAddress(fullAddress);

  String state = fullAddress.substring(
      fullAddress.lastIndexOf(',') + 1, fullAddress.length);
  fullAddress = fullAddress.substring(0, fullAddress.lastIndexOf(','));
  String city = fullAddress.substring(
      fullAddress.lastIndexOf(',') + 1, fullAddress.length);
  fullAddress = fullAddress.substring(0, fullAddress.lastIndexOf(','));

  Navigator.of(ctx.context).pop(
    AddressItemState(
      id: address.placeId,
      title: "",
      address: fullAddress,
      state: state,
      city: city,
      latLng: location,
    ),
  );
}

Future<Marker> addMarker(
    LatLng currentPosition, String markerIdVal, Context<MapsState> ctx) async {
  final MarkerId markerId = MarkerId(markerIdVal);
  if (markerIcon == null) {
    markerIcon = await MapsUtils.getBitmapDescriptorFromSvgAsset(
        ctx.context, 'assets/icons/ic_address.svg');
  }
  final Marker marker = Marker(
    markerId: markerId,
    position: currentPosition,
    icon: markerIcon,
    infoWindow: InfoWindow(title: markerIdVal, snippet: '*'),
    onTap: () {},
  );
  return marker;
}

Future<void> getAddress(LatLng position, Context<MapsState> ctx) async {
  if (timerGetAddress != null) {
    timerGetAddress.cancel();
    timerGetAddress = null;
  }
  timerGetAddress = Timer(Duration(milliseconds: 1200), () async {
    if (position?.latitude != null && position?.longitude != null) {
      ctx.dispatch(
        MapsActionCreator.showHideLoadingAddress(true),
      );
      List<Placemark> placeMarks = await placemarkFromCoordinates(
          position?.latitude, position?.longitude);
      addresses = [];
      if (placeMarks != null && placeMarks.isNotEmpty) {
        addresses = placeMarks
            .map(
              (e) => AddressItemState(
                id: placeMarks.indexOf(e).toString(),
                title: "",
                address: e.street,
                city: e.locality,
                state: e.country,
                zip: e.postalCode,
                country: e.country,
                latLng: position,
              ),
            )
            .toList(growable: false);
      }
      ctx.dispatch(
        MapsActionCreator.handleListAddress(addresses),
      );
    } else {
      ctx.dispatch(
        MapsActionCreator.handleListAddress(
          [],
        ),
      );
    }
  });
}

Future _search(Action action, Context<MapsState> ctx) async {
  if (timerSearchSuggestion != null) {
    timerSearchSuggestion.cancel();
    timerSearchSuggestion = null;
  }
  timerSearchSuggestion = Timer(Duration(milliseconds: 300), () async {
    String keyword = action.payload ?? '';
    MapsState newState = ctx.state.clone();
    List<Suggestion> suggestions = [];
    if (keyword.isNotEmpty) {
      suggestions = await PlaceApiProvider().fetchSuggestions(keyword);
    } else {
      suggestions = [];
    }
    newState.suggestions = suggestions;
    ctx.dispatch(
      MapsActionCreator.handleSearchResult(newState),
    );
  });
}

Future _createMap(Action action, Context<MapsState> ctx) async {
  GoogleMapController controller = action.payload;
  ctx.dispatch(
    MapsActionCreator.mapCreated(controller),
  );
}

Future _moveCamera(Action action, Context<MapsState> ctx) async {
  CameraPosition position = action.payload;
  await getAddress(position?.target, ctx);
  MapsState newState = ctx.state.clone();
  newState.currentPosition = position?.target;

  ctx.dispatch(
    MapsActionCreator.cameraMoved(newState),
  );
}

Future _showInfoWindowMarker(Action action, Context<MapsState> ctx) async {
  Offset position = action.payload;
  if (position == null) return;
  await showInfoWindowMarker(ctx.context, position, addresses?.first,
      (address) {
    if (address == null) return;
    Navigator.of(ctx.context).pop(address);
  });
}

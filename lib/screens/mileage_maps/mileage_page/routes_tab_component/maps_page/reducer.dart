import 'dart:async';

import 'package:fish_redux/fish_redux.dart';
import 'package:geocoding/geocoding.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/address_item_component/state.dart';
import '../state.dart';
import 'action.dart';
import 'state.dart';
import 'package:flutter/material.dart' hide Action;

var items = List<MapsState>();

Reducer<MapsState> buildReducer() {
  return asReducer(
    <Object, Reducer<MapsState>>{
      MapsAction.initialize: _initialize,
      MapsAction.mapCreated: _mapCreated,
      MapsAction.cameraMoved: _cameraMoved,
      MapsAction.handleListAddress: _handleListAddress,
      MapsAction.handleSearchResult: _handleSearchResult,
      MapsAction.hideSuggestion: _hideSuggestion,
      MapsAction.showHideLoadingAddress: _showHideLoadingAddress,
    },
  );
}

MapsState _initialize(MapsState state, Action action) {
  final newState = action.payload as MapsState;
  return newState;
}

MapsState _cameraMoved(MapsState state, Action action) {
  final newState = action.payload as MapsState;
  return newState;
}

MapsState _handleSearchResult(MapsState state, Action action) {
  final newState = action.payload as MapsState;
  return newState;
}

MapsState _hideSuggestion(MapsState state, Action action) {
  final newState = state.clone();
  // ignore: null_aware_in_logical_operator
  if (newState.suggestions != null && newState.suggestions?.isNotEmpty) {
    newState.suggestions = [];
  }
  return newState;
}

MapsState _showHideLoadingAddress(MapsState state, Action action) {
  bool isLoadingAddress = action.payload ?? false;
  final newState = state.clone();
  newState.isLoadingAddress = isLoadingAddress;
  return newState;
}

// void addMarker(
//     MapsState newState, LatLng currentPosition, String markerIdVal) async {
//   final MarkerId markerId = MarkerId(markerIdVal);
//
//   final markerIcon =
//       await _createMarkerImageFromAsset("assets/icons/ic_address.svg");
//   final Marker marker = Marker(
//     markerId: markerId,
//     position: currentPosition,
//     icon: markerIcon,
//     infoWindow: InfoWindow(title: markerIdVal, snippet: '*'),
//     onTap: () {},
//   );
//   newState.markers[markerId] = marker;
// }

MapsState _mapCreated(MapsState state, Action action) {
  final MapsState newState = state.clone();
  newState.controller.complete(action.payload as GoogleMapController);
  return newState;
}

MapsState _handleListAddress(MapsState state, Action action) {
  final addresses = action.payload as List<AddressItemState>;
  final MapsState newState = state.clone();
  newState.addresses = addresses;
  newState.isLoadingAddress = false;
  return newState;
}

// Future<BitmapDescriptor> _createMarkerImageFromAsset(String iconPath) async {
//   ImageConfiguration configuration = ImageConfiguration();
//   BitmapDescriptor bitmapImage =
//       await BitmapDescriptor.fromAssetImage(configuration, iconPath);
//   return bitmapImage;
// }

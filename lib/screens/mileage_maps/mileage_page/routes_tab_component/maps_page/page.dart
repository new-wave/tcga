import 'package:fish_redux/fish_redux.dart';

import 'address_adapter/adapter.dart';
import 'effect.dart';
import 'reducer.dart';
import 'state.dart';
import 'view.dart';

class MapsPage extends Page<MapsState, Map<String, dynamic>> {
  MapsPage()
      : super(
    initState: initState,
    effect: buildEffect(),
    reducer: buildReducer(),
    view: buildView,
    dependencies: Dependencies<MapsState>(
        adapter: NoneConn<MapsState>() + AddressAdapter(),
        slots: <String, Dependent<MapsState>>{
        }),
    middleware: <Middleware<MapsState>>[
    ],);

}

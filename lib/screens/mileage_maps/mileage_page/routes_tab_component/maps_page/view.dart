import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/routes_tab_component/maps_page/action.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/widget/entry_widget.dart';

import 'state.dart';

Widget buildView(MapsState state, Dispatch dispatch, ViewService viewService) {
  final ListAdapter adapter = viewService.buildAdapter();
  Offset _tapPosition;
  return Scaffold(
    backgroundColor: Colors.white,
    appBar: _appBar(viewService, dispatch),
    body: GestureDetector(
      onTap: () {
        dispatch(
          MapsActionCreator.hideSuggestion(),
        );
      },
      child: Container(
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.only(
                  top: 16.0, left: 16.0, right: 16.0, bottom: 16.0),
              child: EntryWidget(
                title: AppLocalizations.of(viewService.context)
                    .text('expenses.currencies.placeholder_enter_to_search'),
                suffixIcon: SvgPicture.asset(
                  'assets/icons/ic_search.svg',
                ),
                onTextChanged: (text) {
                  dispatch(
                    MapsActionCreator.search(
                      text,
                    ),
                  );
                },
              ),
            ),
            Expanded(
              flex: 3,
              child: Stack(
                children: [
                  state?.currentPosition != null
                      ? GoogleMap(
                          mapType: MapType.normal,
                          initialCameraPosition: CameraPosition(
                            target: state?.currentPosition,
                            zoom: 13,
                          ),
                          myLocationEnabled: true,
                          onMapCreated: (GoogleMapController controller) {
                            dispatch(
                              MapsActionCreator.createMap(controller),
                            );
                            //   state.controller.complete(controller);
                          },
                          onCameraMove: (position) => dispatch(
                            MapsActionCreator.moveCamera(position),
                          ),
                          markers: Set<Marker>.of(state.markers.values),
                        )
                      : Center(
                          child: CircularProgressIndicator(),
                        ),
                  Center(
                    child: GestureDetector(
                      onTapDown: (details) {
                        _tapPosition = details.globalPosition;
                      },
                      child: SvgPicture.asset(
                        "assets/icons/ic_address.svg",
                        width: 36,
                        height: 36,
                      ),
                      onTap: () {
                        dispatch(
                          MapsActionCreator.showInfoWindowMarker(_tapPosition),
                        );
                      },
                    ),
                  ),
                  Visibility(
                    visible: (state.suggestions != null &&
                        state.suggestions.isNotEmpty),
                    child: Container(
                      color: colorWhite,
                      child: ListView.separated(
                        shrinkWrap: true,
                        separatorBuilder: (context, index) => Divider(
                          height: 8,
                          thickness: 0.2,
                          color: colorDivider,
                        ),
                        itemCount: state.suggestions.length,
                        itemBuilder: (context, index) {
                          return InkWell(
                            onTap: () {
                              dispatch(
                                MapsActionCreator.selectSuggestion(
                                  state.suggestions[index],
                                ),
                              );
                            },
                            child: Container(
                              padding: EdgeInsets.all(12.0),
                              child: Text(
                                state.suggestions[index].description,
                                style: textStylePrimary,
                              ),
                            ),
                          );
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(viewService.context).size.width,
              color: colorBackground,
              padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 8.0),
              child: Text(
                AppLocalizations.instance
                    .text('mileage.routes.suggested_location'),
                style: textStyleHinttext,
              ),
            ),
            Container(
              height: adapter.itemCount < 4
                  ? (60.0 + adapter.itemCount * 50.0)
                  : 180.0,
              child: Stack(
                children: [
                  ListView.separated(
                    shrinkWrap: true,
                    padding: EdgeInsets.only(top: 12),
                    separatorBuilder: (context, index) => Divider(
                      height: 16,
                      thickness: 0,
                      color: Colors.transparent,
                    ),
                    itemCount: adapter.itemCount,
                    itemBuilder: adapter.itemBuilder,
                  ),
                  Visibility(
                    visible: state.isLoadingAddress,
                    child: Container(
                      color: colorWhite,
                      height: 80,
                      child: Center(
                        child: CircularProgressIndicator(),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    ),
  );
}

AppBar _appBar(ViewService viewService, Dispatch dispatch) {
  return AppBar(
    iconTheme: IconThemeData(color: Colors.black),
    elevation: 3,
    backgroundColor: Color(0xFFFFFFFF),
    leading: IconButton(
        icon: SvgPicture.asset(
          "assets/icons/ic_back.svg",
          width: 24,
          height: 24,
        ),
        onPressed: () {
          Navigator.pop(viewService.context);
        }),
    centerTitle: false,
    title: Text(
      AppLocalizations.of(viewService.context)
          .text('mileage.routes.select_address'),
      style: TextStyle(
          color: Color(0xff4F4F4F), fontSize: 20, fontWeight: FontWeight.w400),
    ),
    actions: [],
  );
}

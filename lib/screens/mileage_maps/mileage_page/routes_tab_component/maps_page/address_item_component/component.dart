import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/address_item_component/state.dart';

import 'view.dart';

class AddressItemComponent extends Component<AddressItemState> {
  AddressItemComponent()
      : super(
    view: buildView,
  );
}

import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/address_item_component/state.dart';

import '../action.dart';

Widget buildView(
    AddressItemState state, Dispatch dispatch, ViewService viewService) {
  return GestureDetector(
    onTap: () => dispatch(
      MapsActionCreator.selectAddress(
        state,
      ),
    ),
    child: SizedBox(
      height: 40,
      child: Container(
          padding: const EdgeInsets.symmetric(
            horizontal: 16,
          ),
          child: Row(
            children: [
              SvgPicture.asset(
                "assets/icons/ic_address.svg",
                width: 20,
                height: 20,
              ),
              SizedBox(
                width: 16,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    width: MediaQuery.of(viewService.context).size.width - 110,
                    child: Text(
                      state?.address ?? '',
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        color: colorTextPrimary,
                        fontSize: 14,
                      ),
                      maxLines: 1,
                      softWrap: true,
                    ),
                  ),
                  Text(
                    "${state.city}, ${state.country}",
                    style: TextStyle(
                      color: colorTextPrimary,
                      fontSize: 12,
                    ),
                  ),
                ],
              ),
            ],
          )),
    ),
  );
}

import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/gestures.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:tcga_flutter/models/place_api.dart';
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/address_item_component/state.dart';
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/routes_tab_component/maps_page/state.dart';

//TODO replace with your own action
enum MapsAction {
  initialize,
  createMap,
  moveCamera,
  mapCreated,
  cameraMoved,
  search,
  handleSearchResult,
  selectSuggestion,
  handleSuggestionSelected,
  handleListAddress,
  selectAddress,
  hideSuggestion,
  showInfoWindowMarker,
  showHideLoadingAddress,
}

class MapsActionCreator {
  static Action initialize(MapsState state) {
    return Action(
      MapsAction.initialize,
      payload: state,
    );
  }

  static Action hideSuggestion() {
    return Action(
      MapsAction.hideSuggestion,
    );
  }

  static Action search(String keyword) {
    return Action(
      MapsAction.search,
      payload: keyword,
    );
  }

  static Action createMap(GoogleMapController controller) {
    return Action(
      MapsAction.createMap,
      payload: controller,
    );
  }

  static Action moveCamera(CameraPosition position) {
    return Action(
      MapsAction.moveCamera,
      payload: position,
    );
  }

  static Action mapCreated(GoogleMapController controller) {
    return Action(
      MapsAction.mapCreated,
      payload: controller,
    );
  }

  static Action cameraMoved(MapsState state) {
    return Action(
      MapsAction.cameraMoved,
      payload: state,
    );
  }

  static Action handleSearchResult(MapsState state) {
    return Action(
      MapsAction.handleSearchResult,
      payload: state,
    );
  }

  static Action handleListAddress(List<AddressItemState> addresses) {
    return Action(
      MapsAction.handleListAddress,
      payload: addresses,
    );
  }

  static Action selectAddress(AddressItemState address) {
    return Action(
      MapsAction.selectAddress,
      payload: address,
    );
  }

  static Action handleSuggestionSelected(Suggestion address) {
    return Action(
      MapsAction.handleSuggestionSelected,
      payload: address,
    );
  }

  static Action selectSuggestion(Suggestion address) {
    return Action(
      MapsAction.selectSuggestion,
      payload: address,
    );
  }

  static Action showInfoWindowMarker(Offset position) {
    return Action(
      MapsAction.showInfoWindowMarker,
      payload: position,
    );
  }

  static Action showHideLoadingAddress(bool isLoadingAddress) {
    return Action(
      MapsAction.showHideLoadingAddress,
      payload: isLoadingAddress,
    );
  }
}

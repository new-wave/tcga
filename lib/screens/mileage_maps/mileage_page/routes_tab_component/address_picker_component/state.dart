import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/address_item_component/state.dart';

class AddressPickerState implements Cloneable<AddressPickerState> {
  AddressItemState fromAddress;
  AddressItemState toAddress;

  AddressPickerState({
    this.fromAddress,
    this.toAddress,
  });

  @override
  AddressPickerState clone() {
    return AddressPickerState()
      ..fromAddress = fromAddress
      ..toAddress = toAddress;
  }
}

AddressPickerState initState(Map<String, dynamic> args) {
  return AddressPickerState();
}

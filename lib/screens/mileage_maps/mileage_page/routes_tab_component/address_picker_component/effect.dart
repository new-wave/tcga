import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';

import '../../../../../route_name.dart';
import '../../../../../utils/utils.dart';

import 'action.dart';
import 'state.dart';

Effect<AddressPickerState> buildEffect() {
  return combineEffects(<Object, Effect<AddressPickerState>>{
    AddressPickerAction.selectFromAddress: _selectFromAddress,
    AddressPickerAction.selectToAddress: _selectToAddress,
  });
}

Future _selectFromAddress(
    Action action, Context<AddressPickerState> ctx) async {
  final item = await ctx.navigateTo(
    RouteName.addressPicker,
    arguments: {
      'title': AppLocalizations.instance.text('mileage.routes.from'),
      'is-in-trip': false,
    },
  );

  if (item == null) return;

  ctx.dispatch(
    AddressPickerActionCreator.handleFromAddressSelected(
      item,
    ),
  );
}

Future _selectToAddress(Action action, Context<AddressPickerState> ctx) async {
  final item = await ctx.navigateTo(
    RouteName.addressPicker,
    arguments: {
      'title': AppLocalizations.instance.text('mileage.routes.to'),
      'is-in-trip': false,
    },
  );

  if (item == null) return;

  ctx.dispatch(
    AddressPickerActionCreator.handleToAddressSelected(
      item,
    ),
  );
}

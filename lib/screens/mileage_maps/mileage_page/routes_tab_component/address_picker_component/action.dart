import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/address_item_component/state.dart';
import 'package:tcga_flutter/screens/recipients/state.dart';

enum AddressPickerAction {
  selectFromAddress,
  selectToAddress,
  handleFromAddressSelected,
  handleToAddressSelected,
  switchFromToAddress,
}

class AddressPickerActionCreator {
  static Action selectFromAddress() {
    return const Action(AddressPickerAction.selectFromAddress);
  }

  static Action selectToAddress() {
    return const Action(AddressPickerAction.selectToAddress);
  }

  static Action handleFromAddressSelected(AddressItemState item) {
    return Action(
      AddressPickerAction.handleFromAddressSelected,
      payload: item,
    );
  }

  static Action handleToAddressSelected(AddressItemState item) {
    return Action(
      AddressPickerAction.handleToAddressSelected,
      payload: item,
    );
  }

  static Action switchFromToAddress() {
    return Action(
      AddressPickerAction.switchFromToAddress,
    );
  }
}

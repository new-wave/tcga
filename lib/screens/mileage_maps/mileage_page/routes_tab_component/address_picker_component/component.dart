import 'package:fish_redux/fish_redux.dart';

import 'effect.dart';
import 'reducer.dart';
import 'state.dart';
import 'view.dart';

class AddressPickerComponent extends Component<AddressPickerState> {
  AddressPickerComponent()
      : super(
          effect: buildEffect(),
          reducer: buildReducer(),
          view: buildView,
          dependencies: Dependencies<AddressPickerState>(
            adapter: null,
            slots: <String, Dependent<AddressPickerState>>{},
          ),
        );
}

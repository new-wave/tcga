import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/address_item_component/state.dart';

import 'action.dart';
import 'state.dart';

Reducer<AddressPickerState> buildReducer() {
  return asReducer(
    <Object, Reducer<AddressPickerState>>{
      AddressPickerAction.handleFromAddressSelected: _handleFromAddressSelected,
      AddressPickerAction.handleToAddressSelected: _handleToAddressSelected,
      AddressPickerAction.switchFromToAddress: _switchFromToAddress,
    },
  );
}

AddressPickerState _handleFromAddressSelected(
    AddressPickerState state, Action action) {
  final AddressPickerState newState = state.clone();
  newState.fromAddress = action.payload as AddressItemState;
  return newState;
}

AddressPickerState _handleToAddressSelected(
    AddressPickerState state, Action action) {
  final AddressPickerState newState = state.clone();
  newState.toAddress = action.payload as AddressItemState;
  return newState;
}

AddressPickerState _switchFromToAddress(
    AddressPickerState state, Action action) {
  final AddressPickerState newState = state.clone();
  newState.fromAddress = state.toAddress;
  newState.toAddress = state.fromAddress;
  return newState;
}

import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/routes_tab_component/address_picker_component/state.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/widget/address_picker_widget.dart';
import 'action.dart';

Widget buildView(
    AddressPickerState state, Dispatch dispatch, ViewService viewService) {
  return Row(
    mainAxisAlignment: MainAxisAlignment.end,
    children: [
      SizedBox(
        width: 8.0,
      ),
      Expanded(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Row(
              children: [
                Container(
                  padding: EdgeInsets.all(8.0),
                  child: SvgPicture.asset(
                    state?.fromAddress != null
                        ? "assets/icons/ic_address.svg"
                        : "assets/icons/ic_address_inactive.svg",
                    width: 20,
                    height: 20,
                  ),
                ),
                Expanded(
                  child: AddressPickerWidget(
                    isActive: state?.fromAddress != null,
                    address: state.fromAddress,
                    hint: AppLocalizations.instance.text('mileage.routes.from'),
                    onPress: () => dispatch(
                      AddressPickerActionCreator.selectFromAddress(),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 4.0,
            ),
            Row(
              children: [
                Container(
                  padding: EdgeInsets.all(8.0),
                  child: SvgPicture.asset(
                    state?.toAddress != null
                        ? "assets/icons/ic_address.svg"
                        : "assets/icons/ic_address_inactive.svg",
                    width: 20,
                    height: 20,
                  ),
                ),
                Expanded(
                  child: AddressPickerWidget(
                    isActive: state?.toAddress != null,
                    address: state.toAddress,
                    hint: AppLocalizations.instance.text('mileage.routes.to'),
                    onPress: () => dispatch(
                      AddressPickerActionCreator.selectToAddress(),
                    ),
                  ),
                ),
              ],
            ),
            Container(),
          ],
        ),
      ),
      InkWell(
        child: Container(
          padding: EdgeInsets.all(8.0),
          child: SvgPicture.asset(
            "assets/icons/ic_sort.svg",
            width: 20,
            height: 20,
          ),
        ),
        onTap: () => dispatch(
          AddressPickerActionCreator.switchFromToAddress(),
        ),
      ),
    ],
  );
}

import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/address_item_component/state.dart';
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/routes_tab_component/state.dart';
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/trips_tab_component/trip_item_component/state.dart';

enum RoutesAction {
  initialize,
  calculate,
  handleFilter,
  refresh,
  tripCreated,
}

class RoutesActionCreator {
  static Action initialize() {
    return Action(RoutesAction.initialize);
  }

  static Action calculate() {
    return const Action(
      RoutesAction.calculate,
    );
  }

  static Action refresh(Map<String, dynamic> args) {
    return Action(
      RoutesAction.refresh,
      payload: args,
    );
  }

  static Action handleFilter(int selectFilters) {
    return Action(RoutesAction.handleFilter, payload: selectFilters);
  }

  static Action tripCreated(TripItemState trip) {
    return Action(
      RoutesAction.tripCreated,
      payload: trip,
    );
  }
}

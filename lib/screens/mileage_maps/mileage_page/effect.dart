import 'package:fish_redux/fish_redux.dart';

import '../../../route_name.dart';
import '../../../utils/utils.dart';

import '../manager.dart';
import 'action.dart';
import 'state.dart';

final _manager = MileageManager();

Effect<MileageState> buildEffect() {
  return combineEffects(<Object, Effect<MileageState>>{
    Lifecycle.initState: _init,
    MileageAction.create: _create,
  });
}

Future _init(Action action, Context<MileageState> ctx) async {
  // final items = await _manager.query();

  ctx.dispatch(
    MileageActionCreator.initialize(),
  );
}

Future _create(Action action, Context<MileageState> ctx) async {
  // final result = await ctx.navigateTo(
  //   RouteName.teamEditor,
  // );
  //
  // if (result == null) return;

  // ctx.dispatch(
  //   MileageActionCreator.create(),
  // );
}

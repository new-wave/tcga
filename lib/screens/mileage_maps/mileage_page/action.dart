import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/teams/teams_page/team_item_component/state.dart';

enum MileageAction {
  initialize,
  create,
}

class MileageActionCreator {
  static Action initialize() {
    return Action(
      MileageAction.initialize,
    );
  }

  static Action create() {
    return const Action(MileageAction.create);
  }
}

import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/widget/entry_widget.dart';

import '../action.dart';
import 'state.dart';

Widget buildView(
  TripHeaderState state,
  Dispatch dispatch,
  ViewService viewService,
) {
  return Column(
    mainAxisSize: MainAxisSize.min,
    children: [
      Container(
        padding: EdgeInsets.only(top: 16, left: 16, right: 16, bottom: 16),
        child: EntryWidget(
          title: AppLocalizations.of(viewService.context)
              .text('expenses.currencies.placeholder_enter_to_search'),
          suffixIcon: SvgPicture.asset(
            'assets/icons/ic_search.svg',
          ),
          onTextChanged: (text) {
            dispatch(
              TripsActionCreator.search(
                 text,
               ),
            );
          },
        ),
      ),
      Row(
        children: [
          SizedBox(
            width: 16,
          ),
          InkWell(
            child: Row(
              children: [
                Container(
                  width: 25,
                  height: 25,
                  padding: EdgeInsets.all(3),
                  decoration: BoxDecoration(
                    color: (state.isFilter) ? colorPrimary : Colors.transparent,
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: SvgPicture.asset(
                    "assets/icons/ic_filter.svg",
                    color: (state.isFilter) ? Colors.white : colorPrimary,
                  ),
                ),
                SizedBox(
                  width: 8.0,
                ),
                Text(
                  AppLocalizations.of(viewService.context)
                      .text("notes.filter.filter"),
                  style: textStylePrimary,
                ),
              ],
            ),
            onTap: () => dispatch(
              TripsActionCreator.handleClickFilter(),
            ),
          ),
          Spacer(),
          InkWell(
            child: Row(
              children: [
                Container(
                  width: 25,
                  height: 25,
                  padding: EdgeInsets.all(3),
                  decoration: BoxDecoration(
                    color: Colors.transparent,
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: SvgPicture.asset(
                    "assets/icons/ic_calculate.svg",
                  ),
                ),
                SizedBox(
                  width: 8.0,
                ),
                Text(AppLocalizations.of(viewService.context)
                    .text("mileage.calculate")),
              ],
            ),
            onTap: () => dispatch(
              TripsActionCreator.handleClickCalculate(),
            ),
          ),
          SizedBox(
            width: 16,
          )
        ],
      ),
    ],
  );
}

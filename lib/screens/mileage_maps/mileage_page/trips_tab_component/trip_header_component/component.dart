import 'package:fish_redux/fish_redux.dart';

import 'state.dart';
import 'view.dart';

class TripHeaderComponent extends Component<TripHeaderState> {
  TripHeaderComponent()
      : super(
          view: buildView,
        );
}

import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/address_item_component/state.dart';
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/trips_tab_component/state.dart';

class TripHeaderState extends Cloneable<TripHeaderState> {
  bool isFilter;
  bool isSearch;
  AddressItemState fromAddress;
  AddressItemState toAddress;
  DateTime fromDate;
  DateTime toDate;

  TripHeaderState({
    this.isFilter,
    this.isSearch,
    this.fromAddress,
    this.toAddress,
    this.fromDate,
    this.toDate,
  });

  @override
  TripHeaderState clone() {
    return TripHeaderState()
      ..fromAddress = fromAddress
      ..toAddress = toAddress
      ..fromDate = fromDate
      ..toDate = toDate
      ..isFilter = isFilter
      ..isSearch = isSearch;
  }
}

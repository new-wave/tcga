import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/trips_tab_component/action.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'state.dart';

Widget buildView(
    TripItemState state, Dispatch dispatch, ViewService viewService) {
  return GestureDetector(
    onTap: () => dispatch(
      TripsActionCreator.handleClickTripDetail(state),
    ),
    child: Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(6.0),
        color: colorBackground,
      ),
      padding: EdgeInsets.all(16.0),
      margin: EdgeInsets.symmetric(horizontal: 8.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Visibility(
            // ignore: null_aware_in_logical_operator
            visible: !(state?.fromAddress?.title?.isEmpty &&
                // ignore: null_aware_in_logical_operator
                state?.toAddress?.title?.isEmpty),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  flex: 1,
                  child: Text(
                    state?.fromAddress?.title ?? '',
                    style: textStyleThird,
                    textAlign: TextAlign.center,
                  ),
                ),
                SvgPicture.asset(
                  "assets/icons/ic_next.svg",
                  width: 16,
                  height: 16,
                ),
                Expanded(
                  flex: 1,
                  child: Text(
                    state?.toAddress?.title ?? '',
                    style: textStyleThird,
                    textAlign: TextAlign.center,
                  ),
                ),
              ],
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                flex: 1,
                child: Text(
                  state?.fromAddress?.fullAddress ?? '',
                  style: textStyleHintTextSmall,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  textAlign: TextAlign.center,
                ),
              ),
              SizedBox(
                width: 8.0,
              ),
              Visibility(
                // ignore: null_aware_in_logical_operator
                visible: (state?.fromAddress?.title?.isEmpty &&
                    // ignore: null_aware_in_logical_operator
                    state?.toAddress?.title?.isEmpty),
                child: SvgPicture.asset(
                  "assets/icons/ic_next.svg",
                  width: 16,
                  height: 16,
                ),
              ),
              SizedBox(
                width: 8.0,
              ),
              Expanded(
                flex: 1,
                child: Text(
                  state?.toAddress?.fullAddress ?? '',
                  style: textStyleHintTextSmall,
                  textAlign: TextAlign.center,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                ),
              ),
            ],
          ),
          Container(
            color: colorDivider,
            height: 0.5,
            margin: EdgeInsets.symmetric(vertical: 12.0),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Expanded(
                flex: 1,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      width: 24,
                      height: 24,
                      padding: EdgeInsets.all(3),
                      decoration: BoxDecoration(
                        color: Colors.transparent,
                        borderRadius: BorderRadius.circular(5),
                      ),
                      child: SvgPicture.asset(
                        "assets/icons/ic_map.svg",
                        color: colorPrimary,
                      ),
                    ),
                    SizedBox(
                      width: 4.0,
                    ),
                    Text(
                      AppLocalizations.of(viewService.context)
                          .text("mileage.routes.view_detail"),
                      style: textStylePrimaryNormal,
                    ),
                  ],
                ),
              ),
              Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    state?.miles?.toStringAsFixed(2) ?? '0',
                    style: textStyleThird,
                  ),
                  Text(
                    AppLocalizations.instance.text('mileage.routes.miles'),
                    style: textStyleHinttext,
                  ),
                ],
              ),
              Expanded(
                flex: 1,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(
                      state?.duration ?? '0',
                      style: textStyleThird,
                    ),
                    Text(
                      AppLocalizations.instance.text('mileage.routes.duration'),
                      style: textStyleHinttext,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    ),
  );
}

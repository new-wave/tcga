import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/address_item_component/state.dart';

class TripItemState implements Cloneable<TripItemState> {
  String id;
  AddressItemState fromAddress;
  AddressItemState toAddress;
  double miles;
  String duration;
  String description;

  TripItemState({
    this.id,
    this.fromAddress,
    this.toAddress,
    this.miles,
    this.duration,
    this.description,
  });

  @override
  TripItemState clone() {
    return TripItemState()
      ..id = id
      ..fromAddress = fromAddress
      ..toAddress = toAddress
      ..miles = miles
      ..duration = duration
      ..description = description;
  }
}

TripItemState initState(Map<String, dynamic> args) {
  return TripItemState();
}

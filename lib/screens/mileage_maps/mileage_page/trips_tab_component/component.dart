import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/trips_tab_component/trip_header_component/component.dart';
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/trips_tab_component/trip_older_component/component.dart';
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/trips_tab_component/trip_today_component/component.dart';
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/trips_tab_component/trip_yesterday_component/component.dart';

import 'effect.dart';
import 'reducer.dart';
import 'state.dart';
import 'view.dart';

class TripsTabComponent extends Component<TripsState> {
  TripsTabComponent()
      : super(
          effect: buildEffect(),
          reducer: buildReducer(),
          view: buildView,
          dependencies: Dependencies<TripsState>(
            adapter: null,
            slots: <String, Dependent<TripsState>>{
              'trip-header-component':
                  TripHeaderPageConnector() + TripHeaderComponent(),
              'trip-today-component':
                  TripTodayStateConnector() + TripTodayComponent(),
              'trip-yesterday-component':
                  TripYesterdayStateConnector() + TripYesterdayComponent(),
              'trip-older-component':
                  TripOlderStateConnector() + TripOlderComponent(),
            },
          ),
        );
}

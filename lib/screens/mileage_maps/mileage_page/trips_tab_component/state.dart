import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/trips_tab_component/trip_calculate_page/state.dart';
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/trips_tab_component/trip_filter_component/state.dart';
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/trips_tab_component/trip_header_component/state.dart';
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/trips_tab_component/trip_item_component/state.dart';
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/trips_tab_component/trip_older_component/state.dart';

import 'trip_today_component/state.dart';
import 'trip_yesterday_component/state.dart';

class TripsState implements Cloneable<TripsState> {
  List<TripItemState> tripTodayList;
  List<TripItemState> tripYesterdayList;
  List<TripItemState> tripOlderList;

  List<TripItemState> tripYesterdayListInit;
  List<TripItemState> tripOlderListInit;

  String recipientId;
  bool isFilter;
  bool isSearch;
  TripFilterState tripFilterState;
  TripCalculateState tripCalculateState;

  TripsState({
    this.tripTodayList,
    this.tripYesterdayList,
    this.tripOlderList,
    this.tripYesterdayListInit,
    this.tripOlderListInit,
    this.recipientId,
    this.isFilter = false,
    this.isSearch = false,
    this.tripFilterState,
    this.tripCalculateState,
  });

  @override
  TripsState clone() {
    return TripsState()
      ..recipientId = recipientId
      ..tripTodayList = tripTodayList
      ..tripYesterdayList = tripYesterdayList
      ..tripOlderList = tripOlderList
      ..tripYesterdayListInit = tripYesterdayListInit
      ..tripOlderListInit = tripOlderListInit
      ..isFilter = isFilter
      ..isSearch = isSearch
      ..tripFilterState = tripFilterState
      ..tripCalculateState = tripCalculateState;
  }
}

TripsState initState(Map<String, dynamic> args) {
  return TripsState()
    ..tripTodayList = []
    ..tripYesterdayList = []
    ..tripOlderList = []
    ..tripYesterdayListInit = []
    ..tripOlderListInit = []
    ..isFilter = false
    ..isSearch = false;
}

class TripHeaderPageConnector extends ConnOp<TripsState, TripHeaderState> {
  @override
  TripHeaderState get(TripsState state) {
    TripHeaderState mState = TripHeaderState();
    mState.isFilter = state.isFilter ?? false;
    mState.isSearch = state.isSearch ?? false;
    mState.fromDate = state.tripFilterState?.fromDate;
    mState.toDate = state.tripFilterState?.toDate;
    mState.fromAddress = state.tripFilterState?.fromAddress;
    mState.toAddress = state.tripFilterState?.toAddress;
    return mState;
  }

  @override
  void set(TripsState state, TripHeaderState subState) {
    subState.isFilter = state.isFilter ?? false;
    subState.isSearch = state.isSearch ?? false;
    subState.fromAddress = state.tripFilterState?.fromAddress;
    subState.toAddress = state.tripFilterState?.toAddress;
    subState.fromDate = state.tripFilterState?.fromDate;
    subState.toDate = state.tripFilterState?.toDate;
  }
}

class TripTodayStateConnector extends ConnOp<TripsState, TripTodayState> {
  @override
  TripTodayState get(TripsState state) =>
      TripTodayState()..items = state.tripTodayList;

  @override
  void set(TripsState state, TripTodayState subState) {
    state.tripTodayList = subState.items
        .map(
          (e) => e.clone(),
        )
        .toList();
  }
}

class TripYesterdayStateConnector
    extends ConnOp<TripsState, TripYesterdayState> {
  @override
  TripYesterdayState get(TripsState state) => TripYesterdayState()
    ..items = state.tripYesterdayList
    ..initItems = state.tripYesterdayListInit;

  @override
  void set(TripsState state, TripYesterdayState subState) {
    state.tripYesterdayList = subState.items
        .map(
          (e) => e.clone(),
        )
        .toList();
    state.tripYesterdayListInit = subState.initItems
        .map(
          (e) => e.clone(),
    )
        .toList();
  }
}

class TripOlderStateConnector extends ConnOp<TripsState, TripOlderState> {
  @override
  TripOlderState get(TripsState state) => TripOlderState()
    ..items = state.tripOlderList
    ..initItems = state.tripOlderListInit;

  @override
  void set(TripsState state, TripOlderState subState) {
    state.tripOlderList = subState.items
        .map(
          (e) => e.clone(),
        )
        .toList();
    state.tripOlderListInit = subState.initItems
        .map(
          (e) => e.clone(),
    )
        .toList();
  }
}

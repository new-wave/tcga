import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/address_item_component/state.dart';

import 'action.dart';
import 'state.dart';

Reducer<TripFilterState> buildReducer() {
  return asReducer(
    <Object, Reducer<TripFilterState>>{
      TripFilterAction.initialize: _initialize,
      TripFilterAction.handleStartDateSelected: _handleStartDateSelected,
      TripFilterAction.handleEndDateSelected: _handleEndDateSelected,
      TripFilterAction.handleContactFromSelected: _handleContactFromSelected,
      TripFilterAction.handleContactToSelected: _handleContactToSelected,
    },
  );
}

TripFilterState _initialize(
  TripFilterState state,
  Action action,
) {
  return action.payload.clone();
}

TripFilterState _handleStartDateSelected(TripFilterState state, Action action) {
  final TripFilterState newState = state.clone();
  newState.fromDate = action.payload as DateTime;
  return newState;
}

TripFilterState _handleEndDateSelected(TripFilterState state, Action action) {
  final TripFilterState newState = state.clone();
  newState.toDate = action.payload as DateTime;
  return newState;
}

TripFilterState _handleContactFromSelected(
    TripFilterState state, Action action) {
  final TripFilterState newState = state.clone();
  newState.fromAddress = action.payload as AddressItemState;
  return newState;
}

TripFilterState _handleContactToSelected(TripFilterState state, Action action) {
  final TripFilterState newState = state.clone();
  newState.toAddress = action.payload as AddressItemState;
  return newState;
}

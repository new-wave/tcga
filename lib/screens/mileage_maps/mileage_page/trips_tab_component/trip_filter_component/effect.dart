import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart' hide Action;
import 'package:tcga_flutter/database/models/enums.dart';
import 'package:tcga_flutter/screens/recipients/manager.dart';
import 'package:tcga_flutter/screens/recipients/state.dart';
import 'package:uuid/uuid.dart';

import '../state.dart';
import 'action.dart';
import 'state.dart';

final _uuid = Uuid();
final _manager = RecipientManager();

Effect<TripFilterState> buildEffect() {
  return combineEffects(<Object, Effect<TripFilterState>>{
    Lifecycle.initState: _init,
    TripFilterAction.close: _close,
    TripFilterAction.submit: _submit,
  });
}

void _init(Action action, Context<TripFilterState> ctx) async {
  var state = ctx.state;
  if (state.fromDate == null) {
    state.fromDate = DateTime.now().subtract(Duration(days: 30));
  }
  if (state.toDate == null) {
    state.toDate = DateTime.now();
  }
  ctx.dispatch(
    TripFilterActionCreator.initialize(state),
  );
}

void _close(Action action, Context<TripFilterState> ctx) {
  final state = ctx.state;
  Navigator.of(ctx.context).pop(TripsState(
    isFilter: false,
  ));
}

void _submit(Action action, Context<TripFilterState> ctx) {
  final state = ctx.state;
  Navigator.of(ctx.context).pop(TripsState(
    tripFilterState: TripFilterState(
      fromDate: state.fromDate,
      toDate: state.toDate,
      fromAddress: state.fromAddress,
      toAddress: state.toAddress,
    ),
    isFilter: true,
  ));
}

import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/address_item_component/state.dart';
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/trips_tab_component/trip_item_component/state.dart';
import '../state.dart';

class TripFilterState implements Cloneable<TripFilterState> {
  List<TripItemState> items;
  AddressItemState fromAddress;
  AddressItemState toAddress;

  DateTime fromDate;
  DateTime toDate;

  bool isFilter;

  TripFilterState({
    this.items,
    this.fromAddress,
    this.toAddress,
    this.fromDate,
    this.toDate,
    this.isFilter,
  });

  @override
  TripFilterState clone() {
    return TripFilterState()
      ..fromAddress = fromAddress
      ..toAddress = toAddress
      ..items = items
      ..fromDate = fromDate
      ..toDate = toDate
      ..isFilter = isFilter;
  }
}

TripFilterState initState(Map<String, dynamic> args) {
  final trip = args['item'] as TripsState;

  return TripFilterState()
    ..fromAddress = trip?.tripFilterState?.fromAddress
    ..toAddress = trip?.tripFilterState?.toAddress
    ..items = trip?.tripTodayList
    ..fromDate = trip?.tripFilterState?.fromDate
    ..toDate = trip?.tripFilterState?.toDate;
}

import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/address_item_component/state.dart';
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/trips_tab_component/trip_filter_component/state.dart';

import '../state.dart';

//TODO replace with your own action
enum TripFilterAction {
  initialize,
  close,
  submit,
  handleStartDateSelected,
  handleEndDateSelected,
  handleContactFromSelected,
  handleContactToSelected,
}

class TripFilterActionCreator {
  static Action initialize(TripFilterState result) {
    return Action(
      TripFilterAction.initialize,
      payload: result,
    );
  }

  static Action close({TripFilterState state}) {
    return Action(
      TripFilterAction.close,
      payload: state,
    );
  }

  static Action submit() {
    return Action(
      TripFilterAction.submit,
    );
  }

  static Action handleStartDateSelected(DateTime date) {
    return Action(
      TripFilterAction.handleStartDateSelected,
      payload: date,
    );
  }

  static Action handleEndDateSelected(DateTime date) {
    return Action(
      TripFilterAction.handleEndDateSelected,
      payload: date,
    );
  }

  static Action handleContactFromSelected(AddressItemState state) {
    return Action(
      TripFilterAction.handleContactFromSelected,
      payload: state,
    );
  }

  static Action handleContactToSelected(AddressItemState state) {
    return Action(
      TripFilterAction.handleContactToSelected,
      payload: state,
    );
  }
}

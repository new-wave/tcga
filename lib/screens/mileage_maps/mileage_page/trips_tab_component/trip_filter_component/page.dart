import 'package:fish_redux/fish_redux.dart';

import 'effect.dart';
import 'reducer.dart';
import 'state.dart';
import 'view.dart';

class TripFilterPage extends Page<TripFilterState, Map<String, dynamic>> {
  TripFilterPage()
      : super(
          initState: initState,
          effect: buildEffect(),
          reducer: buildReducer(),
          view: buildView,
          dependencies: Dependencies<TripFilterState>(
              adapter: null, slots: <String, Dependent<TripFilterState>>{}),
          middleware: <Middleware<TripFilterState>>[],
        );
}

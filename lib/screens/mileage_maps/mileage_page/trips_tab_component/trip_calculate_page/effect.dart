import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart' hide Action;
import 'package:tcga_flutter/database/models/maps/element_matrix.dart';
import 'package:tcga_flutter/models/place_api.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/utils/confirm_dialog.dart';
import 'package:uuid/uuid.dart';
import '../../../../../route_name.dart';
import '../../../manager.dart';
import '../state.dart';
import 'action.dart';
import 'state.dart';
import '../../../../../utils/utils.dart';

final _mileageManager = MileageManager();

Effect<TripCalculateState> buildEffect() {
  return combineEffects(<Object, Effect<TripCalculateState>>{
    Lifecycle.initState: _initialize,
    TripCalculateAction.selectFromAddress: _selectFromAddress,
    TripCalculateAction.selectToAddress: _selectToAddress,
    TripCalculateAction.calculate: _calculate,
  });
}

Future _initialize(Action action, Context<TripCalculateState> ctx) async {
  var state = ctx.state;
  if (state.fromDate == null) {
    state.fromDate = DateTime.now().subtract(
      Duration(days: 30),
    );
  }
  if (state.toDate == null) {
    state.toDate = DateTime.now();
  }
  ctx.dispatch(
    TripCalculateActionCreator.initialize(),
  );
}

Future _calculate(Action action, Context<TripCalculateState> ctx) async {
  final state = ctx.state;
  ctx.dispatch(
    TripCalculateActionCreator.showHideLoadingResult(true),
  );
  ElementMatrix element = await PlaceApiProvider().getDistanceMatrix(
    state?.fromAddress?.latLng,
    state?.toAddress?.latLng,
  );
  ctx.dispatch(
    TripCalculateActionCreator.handleCalculated(element),
  );
}

Future _selectFromAddress(
    Action action, Context<TripCalculateState> ctx) async {
  final item = await ctx.navigateTo(
    RouteName.addressPicker,
    arguments: {
      'title': AppLocalizations.instance.text('mileage.routes.departure'),
      'is-in-trip': true,
      'is-from-address': true,
    },
  );
  if (item == null) return;
  ctx.dispatch(
    TripCalculateActionCreator.handleFromAddressSelected(item),
  );
}

Future _selectToAddress(Action action, Context<TripCalculateState> ctx) async {
  final item = await ctx.navigateTo(
    RouteName.addressPicker,
    arguments: {
      'title': AppLocalizations.instance.text('mileage.routes.destination'),
      'is-in-trip': true,
      'is-from-address': false,
    },
  );
  if (item == null) return;
  ctx.dispatch(
    TripCalculateActionCreator.handleToAddressSelected(item),
  );
}

// Future _saveTrip(Action action, Context<TripCalculateState> ctx) async {
//   TripCalculateState newState = ctx.state.clone();
//   TripItemState tripState = TripItemState(
//     fromAddress: newState.fromAddress,
//     toAddress: newState.toAddress,
//     miles: newState.miles,
//     duration: newState.duration,
//     description: newState.descriptionEditingController.text,
//   );
//   final _ = await _mileageManager.createTrip(tripState);
//   Navigator.of(ctx.context).pop(tripState);
// }

import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/widget/address_picker_widget.dart';
import 'package:tcga_flutter/widget/button_widget.dart';
import 'package:tcga_flutter/widget/date_picker_widget.dart';
import 'package:tcga_flutter/widget/entry_widget.dart';
import 'package:tcga_flutter/widget/picker_widget.dart';
import 'package:tcga_flutter/widget/text_widget.dart';
import '../../../../../utils/utils.dart';
import 'action.dart';
import 'state.dart';

Widget buildView(
    TripCalculateState state, Dispatch dispatch, ViewService viewService) {
  // final ListAdapter adapter = viewService.buildAdapter();
  return Scaffold(
    backgroundColor: Colors.white,
    appBar: _appBar(viewService, dispatch),
    body: Container(
      padding: EdgeInsets.only(left: 16.0, right: 16.0, bottom: 16.0),
      child: Column(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            margin: edgeOnlyTopBotPrimaryMini,
                            child: Text(
                              AppLocalizations.of(viewService.context)
                                  .text("notes.filter.from"),
                              style: textStylePrimary,
                            ),
                          ),
                          Container(
                            child: DatePickerWidget(
                              initial: state.fromDate,
                              max: DateTime.now(),
                              min: DateTime.now().subtract(
                                Duration(days: 30),
                              ),
                              dateSelected: (date) => dispatch(
                                TripCalculateActionCreator
                                    .handleStartDateSelected(
                                  date,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      width: 8.0,
                    ),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            margin: edgeOnlyTopBotPrimaryMini,
                            child: Text(
                              AppLocalizations.of(viewService.context)
                                  .text("notes.filter.to"),
                              style: textStylePrimary,
                            ),
                          ),
                          Container(
                            child: DatePickerWidget(
                              initial: state.toDate,
                              max: DateTime.now(),
                              min: DateTime.now().subtract(
                                Duration(days: 30),
                              ),
                              dateSelected: (date) => dispatch(
                                TripCalculateActionCreator
                                    .handleEndDateSelected(
                                  date,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                Container(
                  margin: edgeOnlyTopBotPrimaryMini,
                  child: Text(
                    AppLocalizations.of(viewService.context)
                        .text("mileage.routes.departure"),
                    style: textStylePrimary,
                  ),
                ),
                AddressPickerWidget(
                  isActive: state?.fromAddress != null,
                  address: state.fromAddress,
                  hint: AppLocalizations.instance
                      .text('mileage.routes.all_place'),
                  isPrefixIconVisible: true,
                  onPress: () => dispatch(
                    TripCalculateActionCreator.selectFromAddress(),
                  ),
                ),
                Container(
                  margin: edgeOnlyTopBotPrimaryMini,
                  child: Text(
                    AppLocalizations.of(viewService.context)
                        .text("mileage.routes.destination"),
                    style: textStylePrimary,
                  ),
                ),
                AddressPickerWidget(
                  isActive: state?.toAddress != null,
                  address: state.toAddress,
                  hint: AppLocalizations.instance
                      .text('mileage.routes.all_place'),
                  isPrefixIconVisible: true,
                  onPress: () => dispatch(
                    TripCalculateActionCreator.selectToAddress(),
                  ),
                ),
                SizedBox(
                  height: 32.0,
                ),
                Visibility(
                  visible: state.isLoadingResultVisible,
                  child: Container(
                    height: 80,
                    child: Center(
                      child: CircularProgressIndicator(),
                    ),
                  ),
                ),
                Visibility(
                  visible: state.isResultVisible,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text(
                        AppLocalizations.instance.text('mileage.routes.total'),
                        style: textStylePrimarySmall,
                      ),
                      SizedBox(
                        height: 12.0,
                      ),
                      Container(
                        padding: EdgeInsets.all(16.0),
                        decoration: BoxDecoration(
                          border: Border.all(
                            width: 2.0,
                            color: colorBackground,
                          ),
                          borderRadius: BorderRadius.all(Radius.circular(5.0)),
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                              color: colorDivider,
                              offset: Offset(0.5, 0.5),
                              blurRadius: 1.5,
                            ),
                          ],
                        ),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  "${state.days} ${AppLocalizations.instance.text('mileage.routes.days')}",
                                  style: textStylePrimaryNormal,
                                ),
                                SizedBox(
                                  width: 24.0,
                                ),
                                Text(
                                  "${state?.fromDate?.toShortDateString()}",
                                  style: textStylePrimaryNormal,
                                ),
                                SvgPicture.asset(
                                  "assets/icons/ic_next.svg",
                                  width: 8,
                                  height: 8,
                                ),
                                Text(
                                  "${state?.toDate?.toShortDateString()}",
                                  style: textStylePrimaryNormal,
                                ),
                              ],
                            ),
                            Container(
                              height: 0.4,
                              color: colorDivider,
                              margin: EdgeInsets.symmetric(vertical: 12.0),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Column(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Text(
                                      state?.miles?.toStringAsFixed(2) ?? '0',
                                      style: textStyleTitleThird,
                                    ),
                                    Text(
                                      AppLocalizations.instance
                                          .text('mileage.routes.miles'),
                                      style: textStyleHinttext,
                                    ),
                                  ],
                                ),
                                Column(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Text(
                                      state?.duration?.toString() ?? '0',
                                      style: textStyleTitleThird,
                                    ),
                                    Text(
                                      AppLocalizations.instance
                                          .text('mileage.routes.duration'),
                                      style: textStyleHinttext,
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 16.0,
                ),
              ],
            ),
          ),
          ButtonWidget(
            colorButton: colorPrimary,
            textButton: AppLocalizations.of(viewService.context)
                .text("mileage.calculate"),
            onPress: () {
              dispatch(
                TripCalculateActionCreator.calculate(),
              );
            },
          ),
        ],
      ),
    ),
  );
}

AppBar _appBar(ViewService viewService, Dispatch dispatch) {
  return AppBar(
    iconTheme: IconThemeData(color: Colors.black),
    elevation: 3,
    backgroundColor: Color(0xFFFFFFFF),
    leading: IconButton(
      icon: SvgPicture.asset(
        "assets/icons/ic_back.svg",
        width: 24,
        height: 24,
      ),
      onPressed: () {
        Navigator.of(viewService.context).pop();
      },
    ),
    centerTitle: false,
    title: Text(
      AppLocalizations.of(viewService.context)
          .text('mileage.routes.trip_log_search'),
      style: TextStyle(
          color: Color(0xff4F4F4F), fontSize: 20, fontWeight: FontWeight.w400),
    ),
    actions: [],
  );
}

import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/database/models/maps/element_matrix.dart';
import 'package:tcga_flutter/models/place_api.dart';
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/address_item_component/state.dart';

//TODO replace with your own action
enum TripCalculateAction {
  initialize,
  calculate,
  handleCalculated,
  showHideLoadingResult,
  handleStartDateSelected,
  handleEndDateSelected,
  selectFromAddress,
  selectToAddress,
  handleFromAddressSelected,
  handleToAddressSelected,
}

class TripCalculateActionCreator {
  static Action initialize() {
    return const Action(TripCalculateAction.initialize);
  }

  static Action calculate() {
    return Action(
      TripCalculateAction.calculate,
    );
  }

  static Action handleCalculated(ElementMatrix element) {
    return Action(
      TripCalculateAction.handleCalculated,
      payload: element,
    );
  }

  static Action showHideLoadingResult(bool isLoadingResult) {
    return Action(
      TripCalculateAction.showHideLoadingResult,
      payload: isLoadingResult,
    );
  }

  static Action handleStartDateSelected(DateTime date) {
    return Action(
      TripCalculateAction.handleStartDateSelected,
      payload: date,
    );
  }

  static Action handleEndDateSelected(DateTime date) {
    return Action(
      TripCalculateAction.handleEndDateSelected,
      payload: date,
    );
  }

  static Action selectFromAddress() {
    return Action(
      TripCalculateAction.selectFromAddress,
    );
  }

  static Action selectToAddress() {
    return Action(
      TripCalculateAction.selectToAddress,
    );
  }

  static Action handleFromAddressSelected(AddressItemState address) {
    return Action(
      TripCalculateAction.handleFromAddressSelected,
      payload: address,
    );
  }

  static Action handleToAddressSelected(AddressItemState address) {
    return Action(
      TripCalculateAction.handleToAddressSelected,
      payload: address,
    );
  }
}

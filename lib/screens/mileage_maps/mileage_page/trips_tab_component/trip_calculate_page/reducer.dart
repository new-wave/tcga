import 'package:fish_redux/fish_redux.dart';
import 'package:geolocator/geolocator.dart';
import 'package:tcga_flutter/database/models/maps/element_matrix.dart';
import 'package:tcga_flutter/models/place_api.dart';
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/address_item_component/state.dart';
import '../../../../../utils/utils.dart';
import 'action.dart';
import 'state.dart';

var items = List<TripCalculateState>();

Reducer<TripCalculateState> buildReducer() {
  return asReducer(
    <Object, Reducer<TripCalculateState>>{
      TripCalculateAction.initialize: _initialize,
      TripCalculateAction.handleStartDateSelected: _handleStartDateSelected,
      TripCalculateAction.handleEndDateSelected: _handleEndDateSelected,
      TripCalculateAction.handleFromAddressSelected: _handleFromAddressSelected,
      TripCalculateAction.handleToAddressSelected: _handleToAddressSelected,
      TripCalculateAction.handleCalculated: _handleCalculated,
      TripCalculateAction.showHideLoadingResult: _showHideLoadingResult,
    },
  );
}

TripCalculateState _initialize(TripCalculateState state, Action action) {
  final TripCalculateState newState = state.clone();
  return newState;
}

TripCalculateState _showHideLoadingResult(TripCalculateState state, Action action) {
  final TripCalculateState newState = state.clone();
  newState.isLoadingResultVisible = true;
  newState.isResultVisible = false;
  return newState;
}

TripCalculateState _handleCalculated(TripCalculateState state, Action action) {
  final TripCalculateState newState = state.clone();
  ElementMatrix element = action.payload;
  double miles = 0;
  String duration = '0';
  if (element != null) {
    miles = (element.distance?.value ?? 0) / 1609.344;
    duration = element.duration?.text ?? '';
  } else {
    if (newState?.fromAddress?.latLng?.latitude != null &&
        newState?.fromAddress?.latLng?.longitude != null &&
        newState?.toAddress?.latLng?.latitude != null &&
        newState?.toAddress?.latLng?.longitude != null) {
      miles = Geolocator.distanceBetween(
          newState?.fromAddress?.latLng?.latitude,
          newState?.fromAddress?.latLng?.longitude,
          newState?.toAddress?.latLng?.latitude,
          newState?.toAddress?.latLng?.longitude) /
          1000 ??
          0;
      duration = '0';
    }
  }
  newState.miles = miles;
  newState.duration = duration;
  newState.days = newState.toDate.countDateBetween(state.fromDate);
  newState.isResultVisible = true;
  newState.isLoadingResultVisible = false;
  return newState;
}

TripCalculateState _handleStartDateSelected(
    TripCalculateState state, Action action) {
  final TripCalculateState newState = state.clone();
  newState.fromDate = action.payload as DateTime;
  return newState;
}

TripCalculateState _handleEndDateSelected(
    TripCalculateState state, Action action) {
  final TripCalculateState newState = state.clone();
  newState.toDate = action.payload as DateTime;
  return newState;
}

TripCalculateState _handleFromAddressSelected(
    TripCalculateState state, Action action) {
  final TripCalculateState newState = state.clone();
  newState.fromAddress = action.payload as AddressItemState;
  return newState;
}

TripCalculateState _handleToAddressSelected(
    TripCalculateState state, Action action) {
  final TripCalculateState newState = state.clone();
  newState.toAddress = action.payload as AddressItemState;
  return newState;
}

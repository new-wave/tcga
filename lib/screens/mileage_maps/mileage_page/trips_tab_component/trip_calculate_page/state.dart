import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/address_item_component/state.dart';
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/trips_tab_component/state.dart';

class TripCalculateState implements Cloneable<TripCalculateState> {
  DateTime fromDate;
  DateTime toDate;

  AddressItemState fromAddress;
  AddressItemState toAddress;
  bool isResultVisible;
  bool isLoadingResultVisible;

  int days;
  double miles;
  String duration;

  TripCalculateState({
    this.fromDate,
    this.toDate,
    this.fromAddress,
    this.toAddress,
    this.days,
    this.miles = 0,
    this.duration = '0',
    this.isResultVisible = false,
    this.isLoadingResultVisible = false
  });

  @override
  TripCalculateState clone() {
    return TripCalculateState()
      ..fromDate = fromDate
      ..toDate = toDate
      ..fromAddress = fromAddress
      ..toAddress = toAddress
      ..days = days
      ..miles = miles
      ..duration = duration
      ..isResultVisible = isResultVisible
      ..isLoadingResultVisible = isLoadingResultVisible;
  }
}

TripCalculateState initState(Map<String, dynamic> args) {
  final trip = args['item'] as TripsState;

  return TripCalculateState()
    ..fromDate = trip?.tripCalculateState?.fromDate
    ..toDate = trip?.tripCalculateState?.toDate
    ..fromAddress = trip?.tripCalculateState?.fromAddress
    ..toAddress = trip?.tripCalculateState?.toAddress
    ..days = trip?.tripCalculateState?.days
    ..miles = trip?.tripCalculateState?.miles
    ..duration = trip?.tripCalculateState?.duration;
}

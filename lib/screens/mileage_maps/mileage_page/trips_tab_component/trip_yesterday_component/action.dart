import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/trips_tab_component/trip_item_component/state.dart';

//TODO replace with your own action
enum TripYesterdayAction {
  action,
  selectTrip,
  handleTripSelected,
}

class TripYesterdayActionCreator {
  static Action onAction() {
    return const Action(TripYesterdayAction.action);
  }

  static Action selectTrip(TripItemState item) {
    return Action(
      TripYesterdayAction.selectTrip,
      payload: item,
    );
  }

  static Action handleTripSelected(TripItemState item) {
    return Action(
      TripYesterdayAction.handleTripSelected,
      payload: item,
    );
  }
}

import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/trips_tab_component/trip_item_component/component.dart';

import '../state.dart';

class TripYesterdayAdapter extends SourceFlowAdapter<TripYesterdayState> {
  TripYesterdayAdapter()
      : super(
    pool: <String, Component<Object>>{
      'trip-item': TripItemComponent(),
    },
  );
}

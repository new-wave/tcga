import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/trips_tab_component/trip_older_component/state.dart';
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/trips_tab_component/trip_yesterday_component/state.dart';

import 'adapter/adapter.dart';
import 'effect.dart';
import 'reducer.dart';
import 'view.dart';

class TripYesterdayComponent extends Component<TripYesterdayState> {
  TripYesterdayComponent()
      : super(
    effect: buildEffect(),
    reducer: buildReducer(),
    view: buildView,
    dependencies: Dependencies<TripYesterdayState>(
        adapter: NoneConn<TripYesterdayState>() + TripYesterdayAdapter(),
        slots: <String, Dependent<TripYesterdayState>>{}),
  );
}

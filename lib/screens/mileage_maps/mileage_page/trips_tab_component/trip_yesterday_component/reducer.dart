import 'package:fish_redux/fish_redux.dart';

import 'action.dart';
import 'state.dart';

Reducer<TripYesterdayState> buildReducer() {
  return asReducer(
    <Object, Reducer<TripYesterdayState>>{
      TripYesterdayAction.action: _onAction,
    },
  );
}

TripYesterdayState _onAction(TripYesterdayState state, Action action) {
  final TripYesterdayState newState = state.clone();
  return newState;
}

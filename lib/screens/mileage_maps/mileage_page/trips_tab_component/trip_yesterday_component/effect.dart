import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/address_item_component/state.dart';
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/trips_tab_component/trip_item_component/state.dart';
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/trips_tab_component/trip_older_component/state.dart';
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/trips_tab_component/trip_yesterday_component/state.dart';
import '../../../manager.dart';
import 'action.dart';
import 'package:flutter/material.dart' hide Action;

final _mileageManager = MileageManager();

Effect<TripYesterdayState> buildEffect() {
  return combineEffects(<Object, Effect<TripYesterdayState>>{
    TripYesterdayAction.action: _onAction,
    TripYesterdayAction.selectTrip: _selectTrip,
  });
}

void _onAction(Action action, Context<TripYesterdayState> ctx) {}

void _selectTrip(Action action, Context<TripYesterdayState> ctx) async {
  TripItemState selectedItem = action.payload as TripItemState;
  final _ = await _mileageManager.createTrip(selectedItem);
  Navigator.of(ctx.context).pop(selectedItem);
  // ctx.dispatch(
  //   TripYesterdayActionCreator.handleAddressSelected(
  //     action.payload as AddressItemState,
  //   ),
  // );
}

import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/trips_tab_component/trip_item_component/state.dart';

import 'state.dart';

enum TripsAction {
  initialize,
  search,
  filter,
  handleClickFilter,
  handleClickCalculate,
  handleClickTripDetail,
  handleSearchResult,
}

class TripsActionCreator {
  static Action initialize(TripsState state) {
    return Action(
      TripsAction.initialize,
      payload: state,
    );
  }

  static Action search(String keyword) {
    return Action(
      TripsAction.search,
      payload: keyword,
    );
  }

  static Action handleSearchResult(TripsState state) {
    return Action(
      TripsAction.handleSearchResult,
      payload: state,
    );
  }

  static Action filter({TripsState state}) {
    return Action(
      TripsAction.filter,
      payload: state,
    );
  }

  static Action handleClickFilter() {
    return Action(
      TripsAction.handleClickFilter,
    );
  }

  static Action handleClickCalculate() {
    return Action(TripsAction.handleClickCalculate);
  }

  static Action handleClickTripDetail(TripItemState state) {
    return Action(
      TripsAction.handleClickTripDetail,
      payload: state,
    );
  }
}

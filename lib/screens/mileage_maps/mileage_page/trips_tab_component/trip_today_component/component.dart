import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/trips_tab_component/trip_today_component/state.dart';

import 'adapter/adapter.dart';
import 'effect.dart';
import 'reducer.dart';
import 'view.dart';

class TripTodayComponent extends Component<TripTodayState> {
  TripTodayComponent()
      : super(
    effect: buildEffect(),
    reducer: buildReducer(),
    view: buildView,
    dependencies: Dependencies<TripTodayState>(
        adapter: NoneConn<TripTodayState>() + TripTodayAdapter(),
        slots: <String, Dependent<TripTodayState>>{}),
  );
}

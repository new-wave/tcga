import 'dart:math' as math;

import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';

import 'action.dart';
import 'state.dart';

Widget buildView(
    TripTodayState state,
    Dispatch dispatch,
    ViewService viewService,
    ) {
  final adapter = viewService.buildAdapter();
  return SliverList(
    delegate: SliverChildBuilderDelegate(
          (context, index) {
        final int itemIndex = index ~/ 2;
        if (index.isEven) {
          return GestureDetector(
            child: adapter.itemBuilder(
              context,
              itemIndex,
            ),
            onTap: () {
              dispatch(
                TripTodayActionCreator.selectTrip(
                  state.getItemData(itemIndex),
                ),
              );
            },
          );

          //   SizedBox(
          //   child: Stack(
          //     children: [
          //       adapter.itemBuilder(
          //         context,
          //         itemIndex,
          //       ),
          //       InkWell(
          //         onTap: () {
          //           dispatch(
          //             TripTodayActionCreator.selectTrip(
          //               state.getItemData(itemIndex),
          //             ),
          //           );
          //         },
          //         child: SizedBox.expand(
          //           child: Container(
          //             color: Colors.transparent,
          //           ),
          //         ),
          //       )
          //     ],
          //   ),
          // );
        }
        return SizedBox(height: 8);
      },
      semanticIndexCallback: (Widget widget, int localIndex) {
        if (localIndex.isEven) {
          return localIndex ~/ 2;
        }
        return null;
      },
      childCount: math.max(0, adapter.itemCount * 2 - 1),
    ),
  );
}

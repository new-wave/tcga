import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/trips_tab_component/trip_item_component/state.dart';

//TODO replace with your own action
enum TripTodayAction {
  action,
  selectTrip,
  handleTripSelected,
}

class TripTodayActionCreator {
  static Action onAction() {
    return const Action(TripTodayAction.action);
  }

  static Action selectTrip(TripItemState item) {
    return Action(
      TripTodayAction.selectTrip,
      payload: item,
    );
  }

  static Action handleTripSelected(TripItemState item) {
    return Action(
      TripTodayAction.handleTripSelected,
      payload: item,
    );
  }
}

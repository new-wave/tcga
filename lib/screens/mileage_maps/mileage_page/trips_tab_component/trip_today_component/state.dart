import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/trips_tab_component/trip_item_component/state.dart';

class TripTodayState extends MutableSource
    implements Cloneable<TripTodayState> {
  List<TripItemState> items;
  String id;

  @override
  TripTodayState clone() {
    return TripTodayState()
      ..items = items
          .map(
            (e) => e.clone(),
          )
          .toList()
      ..id = id;
  }

  @override
  Object getItemData(int index) => items[index];

  @override
  String getItemType(int index) => 'trip-item';

  @override
  int get itemCount => items?.length ?? 0;

  @override
  void setItemData(int index, Object data) => items[index] = data;
}

TripTodayState initState(Map<String, dynamic> args) {
  return TripTodayState();
}

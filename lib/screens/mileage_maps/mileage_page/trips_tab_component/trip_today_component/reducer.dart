import 'package:fish_redux/fish_redux.dart';

import 'action.dart';
import 'state.dart';

Reducer<TripTodayState> buildReducer() {
  return asReducer(
    <Object, Reducer<TripTodayState>>{
      TripTodayAction.action: _onAction,
    },
  );
}

TripTodayState _onAction(TripTodayState state, Action action) {
  final TripTodayState newState = state.clone();
  return newState;
}

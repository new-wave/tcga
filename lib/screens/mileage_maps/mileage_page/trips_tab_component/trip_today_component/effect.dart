import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart' hide Action;
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/trips_tab_component/trip_item_component/state.dart';
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/trips_tab_component/trip_today_component/state.dart';

import '../../../manager.dart';
import 'action.dart';

final _mileageManager = MileageManager();

Effect<TripTodayState> buildEffect() {
  return combineEffects(<Object, Effect<TripTodayState>>{
    TripTodayAction.action: _onAction,
    TripTodayAction.selectTrip: _selectTrip,
  });
}

void _onAction(Action action, Context<TripTodayState> ctx) {}

void _selectTrip(Action action, Context<TripTodayState> ctx) async {
  TripItemState selectedItem = action.payload as TripItemState;
  final _ = await _mileageManager.createTrip(selectedItem);
  Navigator.of(ctx.context).pop(selectedItem);
  // ctx.dispatch(
  //   TripTodayActionCreator.handleAddressSelected(
  //     action.payload as AddressItemState,
  //   ),
  // );
}

import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/trips_tab_component/trip_item_component/state.dart';
import 'package:tcga_flutter/utils/show_picker.dart';
import '../../../../utils/utils.dart';
import '../../../../route_name.dart';
import '../../manager.dart';
import 'action.dart';
import 'state.dart';

final _manager = MileageManager();

Effect<TripsState> buildEffect() {
  return combineEffects(<Object, Effect<TripsState>>{
    Lifecycle.initState: _initialize,
    TripsAction.handleClickFilter: _handleClickFilter,
    TripsAction.search: _handleSearch,
    TripsAction.handleClickCalculate: _handleClickCalculate,
    TripsAction.handleClickTripDetail: _handleClickTripDetail,
  });
}

Future _initialize(Action action, Context<TripsState> ctx) async {
  DateTime fromDateToday = DateTime(DateTime.now().year, DateTime.now().month,
      DateTime.now().day, 0, 0, 0, 0, 0);
  DateTime toDateToday = DateTime.now();
  List<TripItemState> tripTodayList =
      await _manager.queryTrip(fromDate: fromDateToday, toDate: toDateToday);

  DateTime fromDateYesterday = DateTime(DateTime.now().year,
      DateTime.now().month, DateTime.now().day - 1, 0, 0, 0, 0, 0);
  DateTime toDateYesterday = DateTime(DateTime.now().year, DateTime.now().month,
      DateTime.now().day - 1, 23, 59, 59, 59, 59);
  List<TripItemState> tripYesterdayList = await _manager.queryTrip(
      fromDate: fromDateYesterday, toDate: toDateYesterday);

  DateTime toDateOlder = DateTime(DateTime.now().year, DateTime.now().month,
      DateTime.now().day - 2, 23, 59, 59, 59, 59);
  List<TripItemState> tripOlderList =
      await _manager.queryTrip(toDate: toDateOlder);

  TripsState tripsState = TripsState();
  tripsState.tripTodayList = tripTodayList;
  tripsState.tripYesterdayList = tripYesterdayList;
  tripsState.tripOlderList = tripOlderList;

  tripsState.tripYesterdayListInit = tripYesterdayList
      .map(
        (e) => e.clone(),
      )
      .toList();
  tripsState.tripOlderListInit = tripOlderList
      .map(
        (e) => e.clone(),
      )
      .toList();

  ctx.dispatch(
    TripsActionCreator.initialize(tripsState),
  );
}

Future _handleSearch(Action action, Context<TripsState> ctx) async {
  TripsState newState = ctx.state.clone();
  String keyword = action.payload as String ?? '';
  if(keyword.isNotEmpty) {
    List<TripItemState> tripSearchList = await _manager.queryTrip(
      keyword: keyword,
    );
    newState.tripTodayList = tripSearchList;

    newState.tripYesterdayList =
    keyword.isNotEmpty ? [] : newState.tripYesterdayListInit;
    newState.tripOlderList =
    keyword.isNotEmpty ? [] : newState.tripOlderListInit;
    newState.isSearch = keyword.isNotEmpty;

    ctx.dispatch(
      TripsActionCreator.handleSearchResult(
        newState,
      ),
    );
  } else {
    _initialize(action, ctx);
  }
}

Future _handleClickFilter(Action action, Context<TripsState> ctx) async {
  final parameter = ctx.state;
  var result = await showModal2(ctx.context, RouteName.tripFilter, args: {
    'item': parameter,
  }) as TripsState;

  if (result == null) return;

  if (result.isFilter) {
    TripsState newState;
    final items = await _manager.queryTrip(
      fromDate: result.tripFilterState?.fromDate,
      toDate: result.tripFilterState?.toDate,
      fromAddress: result.tripFilterState?.fromAddress,
      toAddress: result.tripFilterState?.toAddress,
    );
    newState = result.clone()
      ..isFilter = result.isFilter
      ..tripTodayList = items;

    ctx.dispatch(
      TripsActionCreator.filter(
        state: newState,
      ),
    );
  } else {
    _initialize(action, ctx);
  }
}

Future _handleClickCalculate(Action action, Context<TripsState> ctx) async {
  final result = await ctx.navigateTo(
    RouteName.tripCalculate,
    arguments: {
      'item': ctx.state,
    },
  );
  if (result == null) return;
}

Future _handleClickTripDetail(Action action, Context<TripsState> ctx) async {
  final result = await ctx.navigateTo(
    RouteName.tripDetail,
    arguments: {
      'trip-detail': action.payload as TripItemState,
    },
  );
  if (result == null) return;
}

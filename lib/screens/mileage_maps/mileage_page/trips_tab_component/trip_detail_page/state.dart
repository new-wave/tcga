import 'package:fish_redux/fish_redux.dart';
import 'package:geolocator/geolocator.dart';
import 'package:tcga_flutter/database/models/enums.dart';
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/address_item_component/state.dart';
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/routes_info_component/state.dart';
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/trips_tab_component/trip_item_component/state.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';

class TripDetailState implements Cloneable<TripDetailState> {
  String title;
  AddressItemState fromAddress;
  AddressItemState toAddress;
  double miles;
  String duration;
  String description;

  @override
  TripDetailState clone() {
    return TripDetailState()
      ..fromAddress = fromAddress
      ..toAddress = toAddress
      ..title = title
      ..miles = miles
      ..duration = duration
      ..description = description;
  }
}

TripDetailState initState(Map<String, dynamic> args) {
  final tripItem = args.containsKey('trip-detail')
      ? args['trip-detail'] as TripItemState
      : null;

  return TripDetailState()
    ..fromAddress = tripItem?.fromAddress
    ..toAddress = tripItem?.toAddress
    ..title = AppLocalizations.instance.text('mileage.routes.trip_info')
    ..miles = tripItem?.miles ?? 0
    ..duration = tripItem?.duration ?? 0
    ..description = tripItem?.description ?? '';
}

class RouteInfoStateConnector extends ConnOp<TripDetailState, RouteInfoState>
    with ReselectMixin<TripDetailState, RouteInfoState> {
  @override
  RouteInfoState computed(TripDetailState state) {
    return RouteInfoState()
      ..title = state.title
      ..miles = state.miles
      ..duration = state.duration
      ..type = RouteInfoScreenType.TripDetail;
  }

  @override
  List<dynamic> factors(TripDetailState state) {
    return [
      state.title,
      state.miles,
      state.duration,
    ];
  }

  @override
  void set(TripDetailState state, RouteInfoState subState) {
    state.title = subState.title;
    state.miles = subState.miles;
    state.duration = subState.duration;
  }
}

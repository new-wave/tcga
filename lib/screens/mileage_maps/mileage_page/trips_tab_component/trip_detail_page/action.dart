import 'package:fish_redux/fish_redux.dart';

//TODO replace with your own action
enum TripDetailAction {
  initialize,
  showTripMap,
}

class TripDetailActionCreator {
  static Action initialize() {
    return const Action(TripDetailAction.initialize);
  }

  static Action showTripMap() {
    return const Action(TripDetailAction.showTripMap);
  }
}

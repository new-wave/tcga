import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart' hide Action;
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/trips_tab_component/trip_item_component/state.dart';
import '../../../../../route_name.dart';
import '../../../manager.dart';
import '../action.dart';
import 'action.dart';
import 'state.dart';
import "../../../../../utils/utils.dart";

final _mileageManager = MileageManager();

Effect<TripDetailState> buildEffect() {
  return combineEffects(<Object, Effect<TripDetailState>>{
    Lifecycle.initState: _initialize,
    TripDetailAction.showTripMap: _showTripMap,
  });
}

Future _initialize(Action action, Context<TripDetailState> ctx) async {
  ctx.dispatch(
    TripDetailActionCreator.initialize(),
  );
}

Future _showTripMap(Action action, Context<TripDetailState> ctx) async {
  final result = await ctx.navigateTo(
    RouteName.tripMapDetail,
    arguments: {
      'from-address': ctx.state.fromAddress,
      'to-address': ctx.state.toAddress,
      'miles': ctx.state.miles,
    },
  );
  if (result == null) return;
}

// Future _onBack(Action action, Context<TripDetailState> ctx) async {
//   Navigator.of(ctx.context).pop({
//     'from-address': ctx.state.fromAddress,
//     'to-address': ctx.state.toAddress,
//   });
// }
//
// Future _saveTrip(Action action, Context<TripDetailState> ctx) async {
//   TripDetailState newState = ctx.state.clone();
//   TripItemState tripState = TripItemState(
//     fromAddress: newState.fromAddress,
//     toAddress: newState.toAddress,
//     miles: newState.miles,
//     duration: newState.duration,
//     description: newState.descriptionEditingController.text,
//   );
//   final _ = await _mileageManager.createTrip(tripState);
//   Navigator.of(ctx.context).pop(tripState);
// }

import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/widget/address_picker_widget.dart';
import 'package:tcga_flutter/widget/button_widget.dart';
import 'package:tcga_flutter/widget/entry_widget.dart';
import 'package:tcga_flutter/widget/text_widget.dart';
import 'action.dart';
import 'state.dart';

Widget buildView(
    TripDetailState state, Dispatch dispatch, ViewService viewService) {
  // final ListAdapter adapter = viewService.buildAdapter();
  return Scaffold(
    backgroundColor: Colors.white,
    appBar: _appBar(viewService, dispatch),
    body: Container(
      padding:
          EdgeInsets.only(top: 16.0, left: 16.0, right: 16.0, bottom: 16.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Visibility(
                visible: state?.fromAddress?.title?.isNotEmpty,
                child: Text(
                  state?.fromAddress?.title ?? '',
                  style: textStyleTitleThird,
                ),
              ),
              Text(
                state?.fromAddress?.fullAddress ?? '',
                style: textStyleHinttext,
              ),
              Row(
                children: [
                  Expanded(
                    flex: 1,
                    child: Container(
                      height: 0.4,
                      color: colorDivider,
                      margin: EdgeInsets.symmetric(vertical: 8.0),
                    ),
                  ),
                  SvgPicture.asset(
                    "assets/icons/ic_arrow_down.svg",
                  ),
                  Expanded(
                    flex: 1,
                    child: Container(
                      height: 0.5,
                      color: colorDivider,
                      margin: EdgeInsets.symmetric(vertical: 8.0),
                    ),
                  ),
                ],
              ),
              Visibility(
                visible: state?.toAddress?.title?.isNotEmpty,
                child: Text(
                  state?.toAddress?.title ?? '',
                  style: textStyleTitleThird,
                ),
              ),
              Text(
                state?.toAddress?.fullAddress ?? '',
                style: textStyleHinttext,
              ),
            ],
          ),
          SizedBox(
            height: 16.0,
          ),
          Container(
            margin: EdgeInsets.symmetric(vertical: 8.0),
            child: viewService.buildComponent('route-info-component'),
          ),
          SizedBox(
            height: 16.0,
          ),
          Text(
            AppLocalizations.instance.text('notes.description'),
            style: textStylePrimary,
            textAlign: TextAlign.left,
          ),
          SizedBox(
            height: 8.0,
          ),
          Text(
            state.description ?? '',
            style: textStylePrimary,
          ),
        ],
      ),
    ),
  );
}

AppBar _appBar(ViewService viewService, Dispatch dispatch) {
  return AppBar(
    iconTheme: IconThemeData(color: Colors.black),
    elevation: 3,
    backgroundColor: Color(0xFFFFFFFF),
    leading: IconButton(
      icon: SvgPicture.asset(
        "assets/icons/ic_back.svg",
        width: 24,
        height: 24,
      ),
      onPressed: () {
        Navigator.of(viewService.context).pop();
      },
    ),
    centerTitle: false,
    title: Text(
      AppLocalizations.of(viewService.context)
          .text('mileage.routes.trip_detail'),
      style: TextStyle(
          color: Color(0xff4F4F4F), fontSize: 20, fontWeight: FontWeight.w400),
    ),
    actions: [],
  );
}

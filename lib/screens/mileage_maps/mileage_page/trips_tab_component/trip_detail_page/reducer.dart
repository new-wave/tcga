import 'package:fish_redux/fish_redux.dart';
import 'package:geolocator/geolocator.dart';
import '../state.dart';
import 'action.dart';
import 'state.dart';

var items = List<TripDetailState>();

Reducer<TripDetailState> buildReducer() {
  return asReducer(
    <Object, Reducer<TripDetailState>>{
      TripDetailAction.initialize: _initialize,
    },
  );
}

TripDetailState _initialize(TripDetailState state, Action action) {
  final TripDetailState newState = state.clone();
  return newState;
}

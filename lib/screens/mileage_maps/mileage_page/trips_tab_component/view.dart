import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';

import 'state.dart';

Widget buildView(TripsState state, Dispatch dispatch, ViewService viewService) {
//  final ListAdapter adapter = viewService.buildAdapter();
  return Scaffold(
    backgroundColor: Colors.white,
    body: NestedScrollView(
      headerSliverBuilder: (context, bool) => [
        SliverToBoxAdapter(
          child: viewService.buildComponent('trip-header-component'),
        ),
      ],
      body: CustomScrollView(
        slivers: [
          SliverToBoxAdapter(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
              child: Text(
                (state.isFilter ||
                        state.tripTodayList == null ||
                        // ignore: null_aware_in_logical_operator
                        state.tripTodayList?.isEmpty)
                    ? AppLocalizations.of(viewService.context)
                        .text('mileage.routes.result_filter')
                    : state.isSearch
                        ? AppLocalizations.of(viewService.context)
                            .text('mileage.trips.search_result')
                        : AppLocalizations.of(viewService.context)
                            .text('mileage.routes.today'),
                style: textStylePrimary,
              ),
            ),
          ),
          viewService.buildComponent('trip-today-component'),
          SliverToBoxAdapter(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
              child: Text(
                (state.isFilter ||
                        state.tripYesterdayList == null ||
                        state.tripYesterdayList?.isEmpty)
                    ? ''
                    : AppLocalizations.of(viewService.context)
                        .text('mileage.routes.yesterday'),
                style: textStylePrimary,
              ),
            ),
          ),
          viewService.buildComponent('trip-yesterday-component'),
          SliverToBoxAdapter(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
              child: Text(
                (state.isFilter ||
                        state.tripOlderList == null ||
                        state.tripOlderList?.isEmpty)
                    ? ''
                    : AppLocalizations.of(viewService.context)
                        .text('mileage.routes.older'),
                style: textStylePrimary,
              ),
            ),
          ),
          viewService.buildComponent('trip-older-component'),
        ],
      ),
    ),
  );
}

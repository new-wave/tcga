import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/address_item_component/state.dart';
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/trips_tab_component/trip_item_component/state.dart';
import '../../../manager.dart';
import 'action.dart';
import 'state.dart';
import 'package:flutter/material.dart' hide Action;

final _mileageManager = MileageManager();

Effect<TripOlderState> buildEffect() {
  return combineEffects(<Object, Effect<TripOlderState>>{
    TripOlderAction.action: _onAction,
    TripOlderAction.selectTrip: _selectTrip,
  });
}

void _onAction(Action action, Context<TripOlderState> ctx) {}

void _selectTrip(Action action, Context<TripOlderState> ctx) async {
  TripItemState selectedItem = action.payload as TripItemState;
  final _ = await _mileageManager.createTrip(selectedItem);
  Navigator.of(ctx.context).pop(selectedItem);
  // ctx.dispatch(
  //   TripOlderActionCreator.handleAddressSelected(
  //     action.payload as AddressItemState,
  //   ),
  // );
}

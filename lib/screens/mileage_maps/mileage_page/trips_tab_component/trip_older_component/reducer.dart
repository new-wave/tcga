import 'package:fish_redux/fish_redux.dart';

import 'action.dart';
import 'state.dart';

Reducer<TripOlderState> buildReducer() {
  return asReducer(
    <Object, Reducer<TripOlderState>>{
      TripOlderAction.action: _onAction,
    },
  );
}

TripOlderState _onAction(TripOlderState state, Action action) {
  final TripOlderState newState = state.clone();
  return newState;
}

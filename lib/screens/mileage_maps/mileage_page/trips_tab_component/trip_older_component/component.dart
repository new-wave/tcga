import 'package:fish_redux/fish_redux.dart';

import 'adapter/adapter.dart';
import 'effect.dart';
import 'reducer.dart';
import 'state.dart';
import 'view.dart';

class TripOlderComponent extends Component<TripOlderState> {
  TripOlderComponent()
      : super(
    effect: buildEffect(),
    reducer: buildReducer(),
    view: buildView,
    dependencies: Dependencies<TripOlderState>(
        adapter: NoneConn<TripOlderState>() + TripOlderAdapter(),
        slots: <String, Dependent<TripOlderState>>{}),
  );
}

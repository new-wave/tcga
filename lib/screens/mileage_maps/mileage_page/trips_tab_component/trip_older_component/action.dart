import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/trips_tab_component/trip_item_component/state.dart';

//TODO replace with your own action
enum TripOlderAction {
  action,
  selectTrip,
  handleTripSelected,
}

class TripOlderActionCreator {
  static Action onAction() {
    return const Action(TripOlderAction.action);
  }

  static Action selectTrip(TripItemState item) {
    return Action(
      TripOlderAction.selectTrip,
      payload: item,
    );
  }

  static Action handleTripSelected(TripItemState item) {
    return Action(
      TripOlderAction.handleTripSelected,
      payload: item,
    );
  }
}

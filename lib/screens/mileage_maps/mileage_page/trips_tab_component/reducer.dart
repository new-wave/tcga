import 'package:fish_redux/fish_redux.dart';
import 'action.dart';
import 'state.dart';

var items = List<TripsState>();

Reducer<TripsState> buildReducer() {
  return asReducer(
    <Object, Reducer<TripsState>>{
      TripsAction.initialize: _initialize,
      TripsAction.filter: _filter,
      TripsAction.handleSearchResult: _handleSearchResult,
    },
  );
}

TripsState _initialize(TripsState state, Action action) {
  final tripState = action.payload ?? TripsState;
  return tripState;
}

TripsState _filter(TripsState state, Action action) {
  final tripState = action.payload ?? TripsState;
  return tripState;
}

TripsState _handleSearchResult(TripsState state, Action action) {
  final tripState = action.payload ?? TripsState;
  return tripState;
}

// TripsState _search(TripsState state, Action action) {
//   final tripState = action.payload ?? TripsState;
//   final TripsState newState = tripState.clone();
//   return newState;
// }

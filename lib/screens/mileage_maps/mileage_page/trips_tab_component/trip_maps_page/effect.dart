import 'dart:async';

import 'package:fish_redux/fish_redux.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/models/place_api.dart';
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/address_item_component/state.dart';
import 'package:tcga_flutter/utils/maps_utils.dart';
import 'package:uuid/uuid.dart';

import '../../../manager.dart';
import 'action.dart';
import 'state.dart';

final _manager = MileageManager();
Timer timer;
bool isRunTimer = false;
BitmapDescriptor markerIcon;

Effect<TripMapsState> buildEffect() {
  return combineEffects(<Object, Effect<TripMapsState>>{
    Lifecycle.initState: _initialize,
    TripMapsAction.createMap: _createMap,
  });
}

Future _initialize(Action action, Context<TripMapsState> ctx) async {
  final TripMapsState newState = ctx.state.clone();
  if (newState.fromAddress?.latLng != null &&
      newState.toAddress?.latLng != null) {
    String encodedPoly = await PlaceApiProvider().getRouteCoordinates(
        newState.fromAddress?.latLng, newState?.toAddress?.latLng);
    newState.centerPoint = MapsUtils.computeCentroid(
        [newState.fromAddress?.latLng, newState?.toAddress?.latLng]);
    if (encodedPoly.isNotEmpty) {
      List<Polyline> polylines = [];
      polylines.add(Polyline(
          polylineId: PolylineId("1"),
          width: 4,
          geodesic: true,
          points: MapsUtils.convertToLatLng(MapsUtils.decodePoly(encodedPoly)),
          color: colorPrimary));
      newState.polylines = polylines;
    }
    final markerFrom = await addMarker(newState.fromAddress, "1", ctx);
    final markerTo = await addMarker(newState.toAddress, "2", ctx);
    final MarkerId markerIdFrom = MarkerId("1");
    final MarkerId markerIdTo = MarkerId("2");
    newState.markers[markerIdFrom] = markerFrom;
    newState.markers[markerIdTo] = markerTo;

    newState.zoom = newState.miles < 5
        ? 15
        : (newState.miles >= 5 && newState.miles < 10)
            ? 14
            : (newState.miles >= 10 && newState.miles < 15)
                ? 13
                : (newState.miles >= 15 && newState.miles < 20)
                    ? 12
                    : 11;
  }
  ctx.dispatch(
    TripMapsActionCreator.initialize(newState),
  );
}

Future _createMap(Action action, Context<TripMapsState> ctx) async {
  GoogleMapController controller = action.payload;
  ctx.dispatch(
    TripMapsActionCreator.mapCreated(controller),
  );
}

Future<Marker> addMarker(AddressItemState address, String markerIdVal,
    Context<TripMapsState> ctx) async {
  final MarkerId markerId = MarkerId(markerIdVal);
  if (markerIcon == null) {
    markerIcon = await MapsUtils.getBitmapDescriptorFromSvgAsset(
        ctx.context, 'assets/icons/ic_address.svg');
  }
  final Marker marker = Marker(
    markerId: markerId,
    position: address?.latLng,
    icon: markerIcon,
    infoWindow: InfoWindow(
        title: '${address?.title}' ?? 'Address',
        snippet: '${address?.fullAddress}'),
    onTap: () {},
  );
  return marker;
}

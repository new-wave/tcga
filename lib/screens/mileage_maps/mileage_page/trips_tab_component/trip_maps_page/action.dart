import 'package:fish_redux/fish_redux.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/address_item_component/state.dart';
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/trips_tab_component/trip_maps_page/state.dart';

//TODO replace with your own action
enum TripMapsAction {
  initialize,
  createMap,
  mapCreated,
}

class TripMapsActionCreator {
  static Action initialize(TripMapsState state) {
    return Action(
      TripMapsAction.initialize,
      payload: state,
    );
  }

  static Action createMap(GoogleMapController controller) {
    return Action(
      TripMapsAction.createMap,
      payload: controller,
    );
  }

  static Action mapCreated(GoogleMapController controller) {
    return Action(
      TripMapsAction.mapCreated,
      payload: controller,
    );
  }
}

import 'dart:async';

import 'package:fish_redux/fish_redux.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/address_item_component/state.dart';

class TripMapsState implements Cloneable<TripMapsState> {
  AddressItemState fromAddress;
  AddressItemState toAddress;
  LatLng centerPoint;
  double miles;
  double zoom;
  String title;
  Completer<GoogleMapController> controller;
  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
  List<Polyline> polylines;

  @override
  TripMapsState clone() {
    return TripMapsState()
      ..fromAddress = fromAddress
      ..toAddress = toAddress
      ..zoom = zoom
      ..centerPoint = centerPoint
      ..miles = miles
      ..title = title
      ..controller = controller
      ..markers = markers
      ..polylines = polylines;
  }
}

TripMapsState initState(Map<String, dynamic> args) {
  final fromAddress = args.containsKey('from-address')
      ? args['from-address'] as AddressItemState
      : null;
  final toAddress = args.containsKey('to-address')
      ? args['to-address'] as AddressItemState
      : null;

  double miles = args.containsKey('miles') ? args['miles'] as double : 0;

  return TripMapsState()
    ..fromAddress = fromAddress
    ..toAddress = toAddress
    ..title = ''
    ..controller = Completer()
    ..markers = <MarkerId, Marker>{}
    ..polylines = []
    ..miles = miles
    ..zoom = 14;
}

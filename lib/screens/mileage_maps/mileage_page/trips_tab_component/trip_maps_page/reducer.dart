import 'dart:convert';

import 'package:fish_redux/fish_redux.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/address_item_component/state.dart';
import 'package:tcga_flutter/utils/utils.dart';

import '../../../../../app.dart';
import 'action.dart';
import 'state.dart';

var items = List<TripMapsState>();

Reducer<TripMapsState> buildReducer() {
  return asReducer(
    <Object, Reducer<TripMapsState>>{
      TripMapsAction.initialize: _initialize,
      TripMapsAction.mapCreated: _mapCreated,
    },
  );
}

TripMapsState _initialize(TripMapsState state, Action action) {
  final TripMapsState newState = action.payload;
  return newState;
}

TripMapsState _mapCreated(TripMapsState state, Action action) {
  final TripMapsState newState = state.clone();
  newState.controller.complete(action.payload as GoogleMapController);
  return newState;
}

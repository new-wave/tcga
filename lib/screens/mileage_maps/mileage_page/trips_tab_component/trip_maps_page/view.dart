import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';

import 'action.dart';
import 'state.dart';

Widget buildView(
    TripMapsState state, Dispatch dispatch, ViewService viewService) {
  return Scaffold(
    backgroundColor: Colors.white,
    appBar: _appBar(viewService, dispatch),
    body: Container(
      child: (state?.centerPoint != null)
          ? GoogleMap(
              mapType: MapType.normal,
              initialCameraPosition: CameraPosition(
                target: state.centerPoint,
                zoom: state.zoom,
              ),
              myLocationEnabled: true,
              onMapCreated: (GoogleMapController controller) {
                dispatch(
                  TripMapsActionCreator.createMap(controller),
                );
                //   state.controller.complete(controller);
              },
              onCameraMove: (position) {},
              markers: Set<Marker>.of(state.markers.values),
              polylines: Set<Polyline>.of(state.polylines),
            )
          : Center(
              child: CircularProgressIndicator(),
            ),
    ),
  );
}

AppBar _appBar(ViewService viewService, Dispatch dispatch) {
  return AppBar(
    iconTheme: IconThemeData(color: Colors.black),
    elevation: 3,
    backgroundColor: Color(0xFFFFFFFF),
    leading: IconButton(
        icon: SvgPicture.asset(
          "assets/icons/ic_back.svg",
          width: 24,
          height: 24,
        ),
        onPressed: () {
          Navigator.pop(viewService.context);
        }),
    centerTitle: false,
    title: Text(
      AppLocalizations.of(viewService.context)
          .text('mileage.routes.trip_detail_map'),
      style: TextStyle(
          color: Color(0xff4F4F4F), fontSize: 20, fontWeight: FontWeight.w400),
    ),
    actions: [],
  );
}

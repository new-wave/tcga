import 'package:fish_redux/fish_redux.dart';

import 'effect.dart';
import 'reducer.dart';
import 'state.dart';
import 'view.dart';

class TripMapsPage extends Page<TripMapsState, Map<String, dynamic>> {
  TripMapsPage()
      : super(
    initState: initState,
    effect: buildEffect(),
    reducer: buildReducer(),
    view: buildView,
    dependencies: Dependencies<TripMapsState>(
        adapter: null,
        slots: <String, Dependent<TripMapsState>>{
        }),
    middleware: <Middleware<TripMapsState>>[
    ],);

}

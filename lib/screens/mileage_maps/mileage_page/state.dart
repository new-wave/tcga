import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/address_item_component/state.dart';
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/routes_tab_component/state.dart';
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/trips_tab_component/state.dart';
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/trips_tab_component/trip_item_component/state.dart';

import 'trips_tab_component/trip_calculate_page/state.dart';
import 'trips_tab_component/trip_filter_component/state.dart';

class MileageState implements Cloneable<MileageState> {
  AddressItemState fromAddress;
  AddressItemState toAddress;

  List<TripItemState> tripTodayList;
  List<TripItemState> tripYesterdayList;
  List<TripItemState> tripOlderList;
  bool isFilter;
  bool isSearch;
  TripFilterState tripFilterState;
  TripCalculateState tripCalculateState;

  @override
  MileageState clone() {
    return MileageState()
      ..fromAddress = fromAddress
      ..toAddress = toAddress
      ..tripTodayList = tripTodayList
      ..tripYesterdayList = tripYesterdayList
      ..tripOlderList = tripOlderList
      ..isFilter = isFilter
      ..isSearch = isSearch
      ..tripFilterState = tripFilterState
      ..tripCalculateState = tripCalculateState;
  }
}

MileageState initState(Map<String, dynamic> args) {
  return MileageState();
}

class RoutesMileageConnector extends ConnOp<MileageState, RoutesState> {
  @override
  RoutesState get(MileageState state) {
    return RoutesState()
      ..fromAddress = state.fromAddress
      ..toAddress = state.toAddress
      ..tripTodayList = state.tripTodayList
      ..tripYesterdayList = state.tripYesterdayList
      ..tripOlderList = state.tripOlderList;
  }

  @override
  void set(MileageState state, RoutesState subState) {
    state.fromAddress = subState.fromAddress;
    state.toAddress = subState.toAddress;
    state.tripTodayList = subState.tripTodayList;
    state.tripYesterdayList = subState.tripYesterdayList;
    state.tripOlderList = subState.tripOlderList;
  }
}

class TripsMileageConnector extends ConnOp<MileageState, TripsState> {
  @override
  TripsState get(MileageState state) => TripsState()
    ..tripTodayList = state.tripTodayList
    ..tripYesterdayList = state.tripYesterdayList
    ..tripOlderList = state.tripOlderList
    ..isFilter = state.isFilter ?? false
    ..isSearch = state.isSearch ?? false
    ..tripFilterState = state.tripFilterState
    ..tripCalculateState = state.tripCalculateState;

  @override
  void set(MileageState state, TripsState subState) {
    state.tripTodayList = subState.tripTodayList;
    state.tripYesterdayList = subState.tripYesterdayList;
    state.tripOlderList = subState.tripOlderList;
    state.isFilter = subState.isFilter;
    state.isSearch = subState.isSearch;
    state.tripFilterState = subState.tripFilterState;
    state.tripCalculateState = subState.tripCalculateState;
  }
}

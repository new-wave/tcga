import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';

import 'action.dart';
import 'state.dart';

Widget buildView(
    MileageState state, Dispatch dispatch, ViewService viewService) {
  return Scaffold(
    backgroundColor: Colors.white,
    appBar: _appBar(viewService, dispatch),
    body: DefaultTabController(
      length: 2,
      initialIndex: 0,
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: Size(
            double.infinity,
            46,
          ),
          child: Stack(
            children: [
              TabBar(
                indicatorColor: colorPrimary,
                indicatorWeight: 3,
                unselectedLabelColor: colorTextSecondary,
                labelColor: colorPrimary,
                labelStyle: textStylePrimaryTabBar,
                unselectedLabelStyle: textStyleSecondaryTabBar,
                tabs: [
                  Tab(
                      text: AppLocalizations.of(viewService.context)
                          .text('mileage.routes')),
                  Tab(
                      text: AppLocalizations.of(viewService.context)
                          .text('mileage.trips')),
                ],
              ),
              Positioned(
                  bottom: 3,
                  left: 0,
                  right: 0,
                  child: Container(
                    height: 1,
                    color: Color(0xffE0E0E0),
                  )),
            ],
          ),
        ),
        body: TabBarView(
          children: [
            viewService.buildComponent('routes-tab'),
            viewService.buildComponent('trips-tab'),
          ],
        ),
      ),
    ),
  );
}

AppBar _appBar(ViewService viewService, Dispatch dispatch) {
  return AppBar(
    iconTheme: IconThemeData(color: Colors.black),
    elevation: 3,
    backgroundColor: Color(0xFFFFFFFF),
    leading: IconButton(
        icon: SvgPicture.asset(
          "assets/icons/ic_back.svg",
          width: 24,
          height: 24,
        ),
        onPressed: () {
          Navigator.pop(viewService.context);
        }),
    centerTitle: false,
    title: Text(
      AppLocalizations.of(viewService.context).text('mileage.title'),
      style: TextStyle(
          color: Color(0xff4F4F4F), fontSize: 20, fontWeight: FontWeight.w400),
    ),
    actions: [
      // IconButton(
      //   icon: SvgPicture.asset(
      //     "assets/icons/ic_button_add.svg",
      //     width: 24,
      //     height: 24,
      //   ),
      //   onPressed: () => dispatch(
      //     MileageActionCreator.create(),
      //   ),
      // ),
    ],
  );
}

import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';

import 'action.dart';
import 'state.dart';

Widget buildView(EmergencyContactState state, Dispatch dispatch,
    ViewService viewService) {
  final adapter = viewService.buildAdapter();
  return Container(
    padding: const EdgeInsets.all(16),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Expanded(
              child: Text("${AppLocalizations.of(viewService.context).text('care_recipient.emergency_contact.title')}",
                style: textStyleTitleThird,
              ),
            ),
            InkWell(
              onTap: () =>
                  dispatch(
                    EmergencyContactActionCreator.createOrEdit(),
                  ),
              child: Padding(
                padding: const EdgeInsets.only(
                  left: 12,
                ),
                child: SvgPicture.asset(
                  'assets/icons/ic_button_add.svg',
                ),
              ),
            )
          ],
        ),
        SizedBox(
          height: 8,
        ),
        ...List.generate(
          adapter.itemCount,
              (index) =>
              adapter.itemBuilder(
                viewService.context,
                index,
              ),
        ),
      ],
    ),
  );
}

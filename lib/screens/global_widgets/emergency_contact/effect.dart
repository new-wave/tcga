import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/route_name.dart';
import 'package:tcga_flutter/screens/global_widgets/emergency_contact/contact_item_component/state.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/utils/show_picker.dart';
import '../../../utils/utils.dart';
import 'action.dart';
import 'state.dart';

Effect<EmergencyContactState> buildEffect() {
  return combineEffects(<Object, Effect<EmergencyContactState>>{
    EmergencyContactAction.createOrEdit: _createOrEdit,
    EmergencyContactAction.delete: _delete,
  });
}

void _createOrEdit(Action action, Context<EmergencyContactState> ctx) async {
  final parameter = action.payload as EmergencyContactItemState;

  final result = await showModal2(ctx.context, RouteName.contactEditor, args: {
    'item': parameter,
  });

  if (result != null) {
    ctx.dispatch(
      EmergencyContactActionCreator.handleCreateOrEdit(
        item: result,
      ),
    );
  }
}

void _delete(Action action, Context<EmergencyContactState> ctx) async {
  final item = action.payload as EmergencyContactItemState;

  if (item == null) return;

  final confirmed = await ctx.confirm(
    title: AppLocalizations.instance.text('common.warning.delete.title'),
    message: AppLocalizations.instance.text('common.warning.delete.desc'),
    positiveActionTitle: AppLocalizations.instance.text('common.sure'),
    negativeActionTitle: AppLocalizations.instance.text('common.cancel'),
  );
  if (!confirmed) return;

  ctx.dispatch(
    EmergencyContactActionCreator.handleDelete(item),
  );
}

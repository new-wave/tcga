import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/global_widgets/phone_number/phone_picker_component/component.dart';

import 'effect.dart';
import 'reducer.dart';
import 'state.dart';
import 'view.dart';

class ContactEditorPage extends Page<ContactEditorState, Map<String, dynamic>> {
  ContactEditorPage()
      : super(
          initState: initState,
          effect: buildEffect(),
          reducer: buildReducer(),
          view: buildView,
          dependencies: Dependencies<ContactEditorState>(
              adapter: null,
              slots: <String, Dependent<ContactEditorState>>{
                'phone-picker-component':
                    PhonePickerStateConnector() + PhonePickerComponent(),
              }),
        );
}

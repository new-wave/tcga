import 'package:email_validator/email_validator.dart';

import 'state.dart';

class Validator {
  static bool isValid(ContactEditorState state) {
    if (_isNullEmptyOrWhiteSpace(
      state.firstNameController.text,
    )) {
      return false;
    }
    if (_isNullEmptyOrWhiteSpace(
      state.lastNameController.text,
    )) {
      return false;
    }
    if (_isNullEmptyOrWhiteSpace(
      state.emailController.text,
    )) {
      return false;
    }

    if (_isNullEmptyOrWhiteSpace(
      state.phoneController.text,
    )) {
      return false;
    }

    if (!EmailValidator.validate(state.emailController.text))
      return false;

    final phoneNumber = (state.phoneController.text ?? '').trim();
    if (state.phoneItemState?.mask != null) {
      if (phoneNumber.length !=
          state.phoneItemState.mask[0]
              .length) {
        return false;
      }
    }

    return true;
  }

  static bool _isNullEmptyOrWhiteSpace(String text) {
    if (text == null) return true;

    return text.trim().isEmpty;
  }
}

import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/global_widgets/emergency_contact/contact_editor_page/state.dart';

//TODO replace with your own action
enum ContactEditorAction {
  initialize,
  validateAndSubmit,
  checkField,
}

class ContactEditorActionCreator {
  static Action initialize(ContactEditorState state) {
    return Action(
      ContactEditorAction.initialize,
      payload: state,
    );
  }

  static Action validateAndSubmit() {
    return const Action(ContactEditorAction.validateAndSubmit);
  }

  static Action checkField() {
    return const Action(
      ContactEditorAction.checkField,
    );
  }
}

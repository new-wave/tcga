import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:tcga_flutter/screens/global_widgets/emergency_contact/contact_item_component/state.dart';
import 'package:tcga_flutter/screens/global_widgets/phone_number/phone_item_component/state.dart';
import 'package:tcga_flutter/screens/global_widgets/phone_number/phone_picker_component/state.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';

class ContactEditorState implements Cloneable<ContactEditorState> {
  String id;

  TextEditingController firstNameController;
  TextEditingController lastNameController;
  TextEditingController emailController;
  TextEditingController phoneController;

  bool isCheckField;
  PhoneItemState phoneItemState;
  String countryCode;

  final scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  ContactEditorState clone() {
    return ContactEditorState()
      ..id = id
      ..emailController = emailController
      ..firstNameController = firstNameController
      ..lastNameController = lastNameController
      ..phoneController = phoneController
      ..countryCode = countryCode
      ..isCheckField = isCheckField
      ..phoneItemState = phoneItemState;
  }
}

ContactEditorState initState(Map<String, dynamic> args) {
  final contact = args['item'] as EmergencyContactItemState;

  return ContactEditorState()
    ..id = contact?.id
    ..lastNameController = TextEditingController(text: contact?.lastName)
    ..firstNameController = TextEditingController(text: contact?.firstName)
    ..emailController = TextEditingController(text: contact?.email)
    ..phoneController = TextEditingController(text: contact?.phone)
    ..countryCode = contact?.countryCode
    ..isCheckField = false;
}

class PhonePickerStateConnector
    extends ConnOp<ContactEditorState, PhonePickerState> {
  @override
  PhonePickerState get(ContactEditorState state) {
    int maskLength = state.phoneItemState?.mask != null
        ? state.phoneItemState?.mask[0].length
        : 0 ?? 0;
    return PhonePickerState()
      ..countryCode = state.countryCode
      ..phoneItemState = state.phoneItemState
      ..phoneEditingController = state.phoneController
      ..isCheckField = state.isCheckField
      ..isShowWarningText = state.phoneController.text.length != maskLength
      ..warningText = state.phoneController.text.length == 0
          ? AppLocalizations.instance.text('common.validator.phone_number')
          : AppLocalizations.instance
              .text('common.validator.phone_number_is_not_valid');
  }

  @override
  void set(ContactEditorState state, PhonePickerState subState) {
    state.phoneItemState = subState.phoneItemState;
    state.phoneController = subState.phoneEditingController;
    state.isCheckField = subState.isCheckField;
    state.countryCode = subState.countryCode;
  }
}

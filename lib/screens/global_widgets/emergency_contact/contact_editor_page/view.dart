import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/database/models/enums.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/widget/button_widget.dart';
import 'package:tcga_flutter/widget/entry_widget.dart';

import 'action.dart';
import 'state.dart';

Widget buildView(
    ContactEditorState state, Dispatch dispatch, ViewService viewService) {
  return Container(
    padding: EdgeInsets.only(
      top: 4,
    ),
    height: 400,
    child: Scaffold(
      resizeToAvoidBottomInset: false,
      key: state.scaffoldKey,
      body: Padding(
        padding: edgeAllInsetsPrimary,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          mainAxisSize: MainAxisSize.max,
          children: [
            Row(
              children: [
                Spacer(
                  flex: 5,
                ),
                Text(
                  AppLocalizations.of(viewService.context)
                      .text('care_recipient.emergency_contact.title'),
                  style: textStyleThird,
                ),
                Spacer(
                  flex: 4,
                ),
                GestureDetector(
                    onTap: () async => Navigator.pop(viewService.context),
                    child: SvgPicture.asset("assets/icons/ic_close.svg")),
              ],
            ),
            SizedBox(),
            EntryWidget(
              title: AppLocalizations.of(viewService.context)
                  .text('sign_up.first_name_required'),
              keyboardType: TextInputType.name,
              controller: state.firstNameController,
              warningText:
                  AppLocalizations.instance.text('common.validator.first_name'),
              isRequired: true,
              isCheckField: state.isCheckField ?? false,
            ),
            SizedBox(
              height: 8,
            ),
            EntryWidget(
              title: AppLocalizations.of(viewService.context)
                  .text('sign_up.last_name_required'),
              keyboardType: TextInputType.name,
              controller: state.lastNameController,
              warningText:
                  AppLocalizations.instance.text('common.validator.last_name'),
              isRequired: true,
              isCheckField: state.isCheckField ?? false,
            ),
            SizedBox(
              height: 8,
            ),
            EntryWidget(
              title: AppLocalizations.of(viewService.context)
                  .text('sign_up.email_address_required'),
              keyboardType: TextInputType.emailAddress,
              controller: state.emailController,
              warningText: AppLocalizations.instance
                  .text('common.validator.email_address'),
              isRequired: true,
              isCheckField: state.isCheckField ?? false,
              entryWidgetType: EntryWidgetType.Email,
            ),
            SizedBox(
              height: 8,
            ),
            viewService.buildComponent('phone-picker-component'),
          ],
        ),
      ),
      bottomNavigationBar: Builder(
        builder: (context) => Container(
          decoration: BoxDecoration(
              color: colorPrimary,
              border: Border.all(
                color: colorPrimary,
              )),
          child: ButtonWidget(
            colorButton: colorPrimary,
            textButton:
                AppLocalizations.of(viewService.context).text('common.save'),
            onPress: () {
              dispatch(ContactEditorActionCreator.validateAndSubmit());
            },
          ),
        ),
      ),
    ),
  );
}

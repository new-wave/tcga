import 'package:fish_redux/fish_redux.dart';

import 'action.dart';
import 'state.dart';

Reducer<ContactEditorState> buildReducer() {
  return asReducer(
    <Object, Reducer<ContactEditorState>>{
      ContactEditorAction.initialize: _onInitialize,
      ContactEditorAction.validateAndSubmit: _onValidateAndSubmit,
      ContactEditorAction.checkField: _checkFieldReducer,
    },
  );
}

ContactEditorState _onInitialize(ContactEditorState state, Action action) {
  final newState = action.payload as ContactEditorState;
  return newState;
}

ContactEditorState _checkFieldReducer(ContactEditorState state, Action action) {
  final ContactEditorState newState = state.clone();
  newState.isCheckField = true;
  return newState;
}

ContactEditorState _onValidateAndSubmit(ContactEditorState state, Action action) {
  final ContactEditorState newState = state.clone();
  return newState;
}

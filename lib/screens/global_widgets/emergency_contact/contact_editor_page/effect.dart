import 'package:devicelocale/devicelocale.dart';
import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart' hide Action;
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/screens/global_widgets/emergency_contact/contact_editor_page/validator.dart';
import 'package:tcga_flutter/screens/global_widgets/emergency_contact/contact_item_component/state.dart';
import 'package:tcga_flutter/screens/global_widgets/phone_number/manager.dart';
import 'package:tcga_flutter/screens/global_widgets/phone_number/phone_item_component/state.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/utils/phone_format.dart';
import 'package:uuid/uuid.dart';
import 'dart:io' show Platform;
import 'action.dart';
import 'state.dart';

final _uuid = Uuid();
final _phoneManager = PhoneManager();

Effect<ContactEditorState> buildEffect() {
  return combineEffects(<Object, Effect<ContactEditorState>>{
    Lifecycle.initState: _init,
    ContactEditorAction.validateAndSubmit: _validateAndSubmit,
  });
}

void _init(Action action, Context<ContactEditorState> ctx) async {
  var newState = ctx.state.clone();

  String phoneNumber = newState.phoneController.text ?? '';
  if (phoneNumber.isNotEmpty) {
    List<String> phones = phoneNumber.split('-').toList(growable: false) ?? [];
    PhoneItemState phoneItemState;
    if (newState.countryCode != null && newState.countryCode.isNotEmpty) {
      phoneItemState = await _phoneManager.getCountryByCode(
        ctx.context,
        code: newState.countryCode,
      );
    } else {
      phoneItemState = await _phoneManager.getCountryByDialCode(
        ctx.context,
        dialCode: (phones != null && phones.length > 0) ? phones[0] : '',
      );
    }
    newState.phoneItemState = phoneItemState;
    newState.phoneController.text = formatMaskPhoneNumber(
        (phones != null && phones.length > 1) ? phones[1] : '',
        phoneItemState?.mask != null ? phoneItemState?.mask[0] : '' ?? '');
  } else {
    String countryCode = await getCountryCodeFromDevice();
    final phone =
        await _phoneManager.getCountryByCode(ctx.context, code: countryCode);
    newState.phoneItemState = phone;
    newState.countryCode = phone.id;
    newState.phoneController.text = '';
  }

  ctx.dispatch(ContactEditorActionCreator.initialize(newState));
}

void _validateAndSubmit(Action action, Context<ContactEditorState> ctx) {
  final state = ctx.state;

  bool isValid = Validator.isValid(state);
  if (!isValid) {
    ctx.dispatch(ContactEditorActionCreator.checkField());
    return;
  }

  Navigator.of(ctx.context).pop(EmergencyContactItemState(
    id: state.id ?? _uuid.v4(),
    firstName: state.firstNameController.text.trim(),
    lastName: state.lastNameController.text.trim(),
    email: state.emailController.text.trim(),
    phone: formatPhoneNumber(
      state.phoneItemState,
      state.phoneController.text.trim(),
    ),
    countryCode: state.countryCode,
  ));
}

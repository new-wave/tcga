import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/global_widgets/emergency_contact/contact_item_component/state.dart';

class EmergencyContactState extends MutableSource
    implements Cloneable<EmergencyContactState> {
  List<EmergencyContactItemState> items;

  @override
  EmergencyContactState clone() {
    return EmergencyContactState()..items = items;
  }

  @override
  Object getItemData(int index) {
    List<EmergencyContactItemState> contacts =
        items?.where((element) => !element.isDelete)?.toList();
    return contacts[index];
  }

  @override
  String getItemType(int index) {
    return 'contact-item';
  }

  @override
  // TODO: implement itemCount
  int get itemCount =>
      items?.where((element) => !element.isDelete)?.toList()?.length ?? 0;

  @override
  void setItemData(int index, Object data) {
    List<EmergencyContactItemState> contacts =
        items?.where((element) => !element.isDelete)?.toList();
    contacts[index] = data;
  }
}

EmergencyContactState initState(Map<String, dynamic> args) {
  return EmergencyContactState()..items = [];
}

import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/global_widgets/emergency_contact/contact_item_component/component.dart';

import '../state.dart';

class ContactAdapter extends SourceFlowAdapter<EmergencyContactState> {
  ContactAdapter()
      : super(
          pool: <String, Component<Object>>{
            'contact-item': ContactItemComponent(),
          },
        );
}

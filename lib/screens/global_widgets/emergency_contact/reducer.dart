import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/global_widgets/emergency_contact/contact_item_component/state.dart';

import 'action.dart';
import 'state.dart';

Reducer<EmergencyContactState> buildReducer() {
  return asReducer(
    <Object, Reducer<EmergencyContactState>>{
      EmergencyContactAction.handleCreateOrEdit: _handleCreateOrEdit,
      EmergencyContactAction.handleDelete: _handleDelete,
    },
  );
}

EmergencyContactState _handleCreateOrEdit(
    EmergencyContactState state, Action action) {
  final EmergencyContactState newState = state.clone();
  final item = action.payload as EmergencyContactItemState;

  final foundIndex =
      newState.items.indexWhere((element) => element.id == item.id);

  if (foundIndex > -1) {
    newState.items[foundIndex] = item;
  } else {
    newState.items.add(item);
  }

  return newState;
}

EmergencyContactState _handleDelete(
    EmergencyContactState state, Action action) {
  final EmergencyContactState newState = state.clone();
  final item = action.payload as EmergencyContactItemState;
  item.isDelete = true;

  final foundIndex =
      newState.items.indexWhere((element) => element.id == item.id);
  if (foundIndex > -1) {
    newState.items[foundIndex] = item;
  }
  return newState;
}

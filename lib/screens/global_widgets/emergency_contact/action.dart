import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/global_widgets/emergency_contact/contact_item_component/state.dart';

enum EmergencyContactAction {
  createOrEdit,
  handleCreateOrEdit,
  delete,
  handleDelete
}

class EmergencyContactActionCreator {
  static Action createOrEdit({EmergencyContactItemState item}) {
    return Action(
      EmergencyContactAction.createOrEdit,
      payload: item,
    );
  }

  static Action handleCreateOrEdit({EmergencyContactItemState item}) {
    return Action(
      EmergencyContactAction.handleCreateOrEdit,
      payload: item,
    );
  }

  static Action delete({
    EmergencyContactItemState item,
  }) {
    return Action(
      EmergencyContactAction.delete,
      payload: item,
    );
  }

  static Action handleDelete(EmergencyContactItemState item) {
    return Action(
      EmergencyContactAction.handleDelete,
      payload: item,
    );
  }
}

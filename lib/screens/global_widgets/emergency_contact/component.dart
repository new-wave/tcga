import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/global_widgets/emergency_contact/contact_adapter/adapter.dart';

import 'effect.dart';
import 'reducer.dart';
import 'state.dart';
import 'view.dart';

class EmergencyContactComponent extends Component<EmergencyContactState> {
  EmergencyContactComponent()
      : super(
          effect: buildEffect(),
          reducer: buildReducer(),
          view: buildView,
          dependencies: Dependencies<EmergencyContactState>(
              adapter: NoneConn<EmergencyContactState>() + ContactAdapter(),
              slots: <String, Dependent<EmergencyContactState>>{}),
        );
}

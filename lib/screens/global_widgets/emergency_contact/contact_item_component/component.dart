import 'package:fish_redux/fish_redux.dart';

import 'state.dart';
import 'view.dart';

class ContactItemComponent extends Component<EmergencyContactItemState> {
  ContactItemComponent()
      : super(
          view: buildView,
          dependencies: Dependencies<EmergencyContactItemState>(
              adapter: null,
              slots: <String, Dependent<EmergencyContactItemState>>{}),
        );
}

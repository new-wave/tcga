import 'package:fish_redux/fish_redux.dart';

class EmergencyContactItemState
    implements Cloneable<EmergencyContactItemState> {
  String id;
  String firstName;
  String lastName;
  String email;
  String phone;
  bool isDelete;
  String countryCode;

  EmergencyContactItemState({
    this.id,
    this.firstName,
    this.lastName,
    this.email,
    this.phone,
    this.countryCode,
    this.isDelete = false,
  });

  @override
  EmergencyContactItemState clone() {
    return EmergencyContactItemState()
      ..id = id
      ..firstName = firstName
      ..lastName = lastName
      ..email = email
      ..phone = phone
      ..countryCode = countryCode
      ..isDelete = isDelete;
  }
}

EmergencyContactItemState initState(Map<String, dynamic> args) {
  return EmergencyContactItemState();
}

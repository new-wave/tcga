import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/utils/phone_format.dart';

import '../action.dart';

import 'state.dart';

Widget buildView(EmergencyContactItemState state, Dispatch dispatch,
    ViewService viewService) {
  return GestureDetector(
    onTap: () {
      dispatch(EmergencyContactActionCreator.createOrEdit(
        item: state,
      ));
    },
    child: Container(
      height: 50,
      alignment: Alignment.center,
      padding: edgeHorizonThird,
      margin: EdgeInsets.only(bottom: 8.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        color: colorBackground,
      ),
      child: Row(
        children: [
          Container(
            height: 24,
            width: 24,
            child: SvgPicture.asset('assets/icons/account.svg'),
          ),
          SizedBox(width: 8.0),
          Expanded(
            child: GestureDetector(
              onLongPress: () {},
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Flexible(
                      child: Container(
                        child: Text(
                          "${state.firstName} ${state.lastName}",
                          style: textStylePrimary,
                        ),
                      ),
                    ),
                    Flexible(
                      child: Container(
                        child: Text(
                          state.phone ?? '',
                          style: textStyleSecondary,
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
          GestureDetector(
            onTap: () => dispatch(
              EmergencyContactActionCreator.delete(
                item: state,
              ),
            ),
            child: Container(
              child: SvgPicture.asset(
                'assets/icons/ic_trash.svg',
                height: 24,
                width: 24,
              ),
            ),
          ),
        ],
      ),
    ),
  );
}

import 'package:fish_redux/fish_redux.dart';

import 'reducer.dart';
import 'state.dart';
import 'view.dart';

class LanguageItemComponent extends Component<LanguageItemState> {
  LanguageItemComponent()
      : super(
          reducer: buildReducer(),
          view: buildView,
          dependencies: Dependencies<LanguageItemState>(
              adapter: null, slots: <String, Dependent<LanguageItemState>>{}),
        );
}

import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:mdi/mdi.dart';
import 'package:tcga_flutter/config/palette.dart';

import '../action.dart';

import 'action.dart';
import 'state.dart';

Widget buildView(
  LanguageItemState state,
  Dispatch dispatch,
  ViewService viewService,
) {
  return InkWell(
    onTap: () {
      dispatch(
        LanguageItemActionCreator.select(
          state,
        ),
      );
    },
    child: Container(
      padding: const EdgeInsets.all(16.0),
      child: Row(
        children: [
          Expanded(
            child: Row(
              children: [
                Icon(
                  state.selected
                      ? Mdi.checkboxMarkedCircleOutline
                      : Mdi.checkboxBlankCircleOutline,
                  color: state.selected ? Colors.green : Colors.grey,
                ),
                SizedBox(width: 16),
                Text(state.display),
              ],
            ),
          ),
          if (state.selected)
            InkWell(
              onTap: () {
                dispatch(
                  LanguageSelectionActionCreator.markAsPrimary(
                    state,
                  ),
                );
              },
              child: Container(
                decoration: BoxDecoration(
                  color: state.isPrimary ? Colors.red : Colors.white,
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all(
                    color: state.isPrimary == true
                        ? Colors.red
                        : Colors.grey,
                  ),
                ),
                padding: const EdgeInsets.symmetric(
                  horizontal: 12,
                  vertical: 4,
                ),
                child: Text(
                  'PRIMARY',
                  style: TextStyle(
                    fontSize: 12,
                    color: state.isPrimary ? Colors.white : Colors.grey,
                  ),
                ),
              ),
            ),
        ],
      ),
    ),
  );
}

import 'dart:convert';

import 'package:fish_redux/fish_redux.dart';

class LanguageItemState implements Cloneable<LanguageItemState> {
  String display;
  String code;
  bool selected;
  bool isPrimary;

  LanguageItemState({
    this.code,
    this.display,
    this.selected = false,
    this.isPrimary = false,
  });

  @override
  LanguageItemState clone() {
    return LanguageItemState()
      ..code = code
      ..display = display
      ..selected = selected
      ..isPrimary = isPrimary;
  }

  factory LanguageItemState.fromRawJson(String str) =>
      LanguageItemState.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory LanguageItemState.fromJson(Map<String, dynamic> json) {
    return LanguageItemState(
      code: json["code"],
      display: json["display"],
    );
  }

  Map<String, dynamic> toJson() => {
        "code": code,
        "display": display,
      };
}

LanguageItemState initState(Map<String, dynamic> args) {
  return LanguageItemState()
    ..selected = false
    ..isPrimary = false;
}

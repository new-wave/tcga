import 'package:fish_redux/fish_redux.dart';

import 'state.dart';

enum LanguageItemAction {
  select,
}

class LanguageItemActionCreator {
  static Action select(LanguageItemState item) {
    return Action(
      LanguageItemAction.select,
      payload: item,
    );
  }
}

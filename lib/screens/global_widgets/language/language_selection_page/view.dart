import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/widget/button_widget.dart';

import 'action.dart';
import 'state.dart';

Widget buildView(
  LanguageSelectionState state,
  Dispatch dispatch,
  ViewService viewService,
) {
  final adapter = viewService.buildAdapter();
  return Scaffold(
    appBar: AppBar(
      title: Text(AppLocalizations.of(viewService.context)
          .text('languages.selection.title')),
      actions: [
        InkWell(
          onTap: () {
            Navigator.of(viewService.context).pop();
          },
          child: Container(
            padding: const EdgeInsets.symmetric(
              horizontal: 16,
            ),
            child: SvgPicture.asset('assets/icons/ic_close.svg'),
          ),
        )
      ],
    ),
    body: ListView.separated(
      itemBuilder: adapter.itemBuilder,
      separatorBuilder: (context, index) {
        return Divider();
      },
      itemCount: adapter.itemCount,
    ),
    bottomNavigationBar: Container(
      child: Container(
        decoration: BoxDecoration(
            color: colorPrimary, border: Border.all(color: colorPrimary)),
        child: ButtonWidget(
          textButton:
              AppLocalizations.of(viewService.context).text('common.confirm'),
          colorButton: colorPrimary,
          onPress: () {
            dispatch(
              LanguageSelectionActionCreator.confirm(),
            );
          },
        ),
      ),
    ),
  );
}

import 'package:fish_redux/fish_redux.dart';

import 'language_item_component/state.dart';

class LanguageSelectionState extends MutableSource
    implements Cloneable<LanguageSelectionState> {
  List<LanguageItemState> items;
  List<LanguageItemState> selectedItems;

  @override
  LanguageSelectionState clone() {
    return LanguageSelectionState()
      ..selectedItems = selectedItems
      ..items = items.map((e) => e.clone()).toList();
  }

  @override
  Object getItemData(int index) {
    return items[index];
  }

  @override
  String getItemType(int index) {
    return 'language-item';
  }

  @override
  int get itemCount => items?.length ?? 0;

  @override
  void setItemData(int index, Object data) {
    items[index] = data;
  }
}

LanguageSelectionState initState(Map<String, dynamic> args) {
  return LanguageSelectionState()
    ..items = []
    ..selectedItems = args['selected-items'] as List<LanguageItemState> ?? [];
}

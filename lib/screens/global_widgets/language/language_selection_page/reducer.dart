import 'package:fish_redux/fish_redux.dart';

import 'language_item_component/state.dart';

import 'action.dart';
import 'state.dart';

Reducer<LanguageSelectionState> buildReducer() {
  return asReducer(
    <Object, Reducer<LanguageSelectionState>>{
      LanguageSelectionAction.initialize: _initialize,
      LanguageSelectionAction.markAsPrimary: _markAsPrimary,
    },
  );
}

LanguageSelectionState _initialize(
  LanguageSelectionState state,
  Action action,
) {
  final newState = state.clone();

  final items = action.payload as List<LanguageItemState> ?? [];

  if (state.selectedItems.length > 0) {
    for (var item in items) {
      item.isPrimary = state.selectedItems.any(
        (x) => x.isPrimary && x.code == item.code,
      );
      item.selected = state.selectedItems.any(
        (x) => x.code == item.code,
      );
    }
  }

  newState.items = items;
  return newState;
}

LanguageSelectionState _markAsPrimary(
  LanguageSelectionState state,
  Action action,
) {
  final item = action.payload as LanguageItemState;

  if (item.isPrimary == true) {
    return state;
  }

  final newState = state.clone();

  var currentPrimaryItemIndex = newState.items.indexWhere((x) => x.isPrimary);

  if (currentPrimaryItemIndex > -1) {
    final currentPrimaryItem = newState.items.firstWhere(
      (element) => element.isPrimary,
    );
    currentPrimaryItem.isPrimary = false;
  }

  final newPrimaryItem = newState.items.firstWhere(
    (element) => element.code == item.code,
  );

  newPrimaryItem.isPrimary = true;

  return newState;
}

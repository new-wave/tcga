import 'package:fish_redux/fish_redux.dart';

import 'effect.dart';
import 'language_adapter/adapter.dart';
import 'reducer.dart';
import 'state.dart';
import 'view.dart';

class LanguageSelectionPage
    extends Page<LanguageSelectionState, Map<String, dynamic>> {
  LanguageSelectionPage()
      : super(
          initState: initState,
          effect: buildEffect(),
          reducer: buildReducer(),
          view: buildView,
          dependencies: Dependencies<LanguageSelectionState>(
              adapter: NoneConn<LanguageSelectionState>() + LanguageAdapter(),
              slots: <String, Dependent<LanguageSelectionState>>{}),
          middleware: <Middleware<LanguageSelectionState>>[],
        );
}

import 'package:fish_redux/fish_redux.dart';

import 'language_item_component/state.dart';

enum LanguageSelectionAction {
  initialize,
  markAsPrimary,
  confirm,
}

class LanguageSelectionActionCreator {
  static Action markAsPrimary(LanguageItemState item) {
    return Action(
      LanguageSelectionAction.markAsPrimary,
      payload: item,
    );
  }

  static Action initialize(List<LanguageItemState> items) {
    return Action(
      LanguageSelectionAction.initialize,
      payload: items,
    );
  }

  static Action confirm() {
    return const Action(
      LanguageSelectionAction.confirm,
    );
  }
}

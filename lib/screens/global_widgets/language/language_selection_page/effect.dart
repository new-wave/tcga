import 'package:flutter/material.dart' hide Action;

import 'package:fish_redux/fish_redux.dart';

import '../manager.dart';

import 'action.dart';
import 'state.dart';

final _manager = LanguageManager();

Effect<LanguageSelectionState> buildEffect() {
  return combineEffects(<Object, Effect<LanguageSelectionState>>{
    Lifecycle.initState: _initialize,
    LanguageSelectionAction.confirm: _confirm,
  });
}

Future _initialize(Action action, Context<LanguageSelectionState> ctx) async {
  final items = await _manager.query(ctx.context);

  ctx.dispatch(
    LanguageSelectionActionCreator.initialize(
      items,
    ),
  );
}

Future _confirm(Action action, Context<LanguageSelectionState> ctx) async {
  final selectedItems = ctx.state.items.where((x) => x.selected).toList();

  Navigator.of(ctx.context).pop(selectedItems);
}

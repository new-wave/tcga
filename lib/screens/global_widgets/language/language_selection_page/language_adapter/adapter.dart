import 'package:fish_redux/fish_redux.dart';

import '../language_item_component/component.dart';

import '../state.dart';

class LanguageAdapter extends SourceFlowAdapter<LanguageSelectionState> {
  LanguageAdapter()
      : super(
          pool: <String, Component<Object>>{
            'language-item': LanguageItemComponent(),
          },
        );
}

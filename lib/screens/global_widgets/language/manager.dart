import 'dart:convert';

import 'package:flutter/material.dart';

import 'language_selection_page/language_item_component/state.dart';

class LanguageManager {
  static final _singleton = LanguageManager._internal();

  factory LanguageManager() {
    return _singleton;
  }

  LanguageManager._internal();

  List<LanguageItemState> items;

  Future<List<LanguageItemState>> query(
    BuildContext context, {
    String keyword,
  }) async {
    if (items == null || items.length == 0) {
      var jsonString = await DefaultAssetBundle.of(
        context,
      ).loadString(
        'assets/json/valueset-languages.json',
      );
      final data = json.decode(jsonString) as List;

      items = data
          .map(
            (item) => LanguageItemState.fromJson(item),
          )
          .toList();
    }

    keyword = (keyword ?? '').trim();

    if (keyword.isEmpty) return items;

    return items
        .where(
          (x) => x.display.contains(keyword),
        )
        .toList();
  }
}

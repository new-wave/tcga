import 'package:fish_redux/fish_redux.dart';

import '../language_selection_page/language_item_component/state.dart';

enum LanguagesAction {
  selectLanguages,
  handleLanguagesSelected,
  markAsPrimary,
}

class LanguagesActionCreator {
  static Action handleLanguagesSelected(
    List<LanguageItemState> items,
  ) {
    return Action(
      LanguagesAction.handleLanguagesSelected,
      payload: items,
    );
  }

  static Action selectLanguages(
    List<LanguageItemState> items,
  ) {
    return Action(
      LanguagesAction.selectLanguages,
      payload: items,
    );
  }

  static Action markAsPrimary(LanguageItemState state) {
    return Action(
      LanguagesAction.markAsPrimary,
      payload: state,
    );
  }
}

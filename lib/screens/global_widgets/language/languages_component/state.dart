import 'package:fish_redux/fish_redux.dart';

import '../language_selection_page/language_item_component/state.dart';

class LanguagesState extends MutableSource
    implements Cloneable<LanguagesState> {
  List<LanguageItemState> items;
  LanguageItemState currentPrimaryItem;

  LanguagesState({
    this.items,
  });

  @override
  LanguagesState clone() {
    return LanguagesState()
      ..currentPrimaryItem = currentPrimaryItem
      ..items = items.map((e) => e.clone()).toList();
  }

  @override
  Object getItemData(int index) {
    return items[index];
  }

  @override
  String getItemType(int index) {
    return 'language-item';
  }

  @override
  int get itemCount => items?.length ?? 0;

  @override
  void setItemData(int index, Object data) {
    items[index] = data;
  }
}

LanguagesState initState(Map<String, dynamic> args) {
  return LanguagesState()..items = [];
}

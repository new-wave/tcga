import 'package:fish_redux/fish_redux.dart';

import 'language_adapter/adapter.dart';

import 'effect.dart';
import 'reducer.dart';
import 'state.dart';
import 'view.dart';

class LanguagesComponent extends Component<LanguagesState> {
  LanguagesComponent()
      : super(
          effect: buildEffect(),
          reducer: buildReducer(),
          view: buildView,
          dependencies: Dependencies<LanguagesState>(
            adapter: NoneConn<LanguagesState>() + LanguageAdapter(),
            slots: <String, Dependent<LanguagesState>>{},
          ),
        );
}

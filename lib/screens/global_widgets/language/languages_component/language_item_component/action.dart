import 'package:fish_redux/fish_redux.dart';

import 'package:tcga_flutter/screens/global_widgets/language/language_selection_page/language_item_component/state.dart';

enum LanguageItemAction {
  select,
}

class LanguageItemActionCreator {
  static Action select(LanguageItemState item) {
    return Action(
      LanguageItemAction.select,
      payload: item,
    );
  }
}

import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';

import '../../language_selection_page/language_item_component/state.dart';

import '../action.dart';

Widget buildView(
  LanguageItemState state,
  Dispatch dispatch,
  ViewService viewService,
) {
  return Container(
    margin: const EdgeInsets.symmetric(horizontal: 0.0, vertical: 8),
    padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8),
    decoration: BoxDecoration(
        color: colorBackground, borderRadius: BorderRadius.circular(8)),
    child: Row(
      children: [
        Expanded(
          child: Text(state.display),
        ),
        InkWell(
          onTap: () {
            dispatch(
              LanguagesActionCreator.markAsPrimary(
                state,
              ),
            );
          },
          child: Container(
            decoration: BoxDecoration(
              color: state.isPrimary ? Colors.red : Colors.white,
              borderRadius: BorderRadius.circular(8),
              border: Border.all(
                color: state.isPrimary == true ? Colors.red : Colors.grey,
              ),
            ),
            padding: const EdgeInsets.symmetric(
              horizontal: 12,
              vertical: 4,
            ),
            child: Text(
              AppLocalizations.of(viewService.context).text('languages.primary'),
              style: TextStyle(
                fontSize: 12,
                color: state.isPrimary ? Colors.white : Colors.grey,
              ),
            ),
          ),
        ),
      ],
    ),
  );
}

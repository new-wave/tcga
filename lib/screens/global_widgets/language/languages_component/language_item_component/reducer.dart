import 'package:fish_redux/fish_redux.dart';

import '../../language_selection_page/language_item_component/state.dart';

import 'action.dart';

Reducer<LanguageItemState> buildReducer() {
  return asReducer(
    <Object, Reducer<LanguageItemState>>{
      LanguageItemAction.select: _select,
    },
  );
}

LanguageItemState _select(LanguageItemState state, Action action) {
  final selectedState = action.payload as LanguageItemState;

  if (selectedState.code == state.code) {
    final LanguageItemState newState = state.clone();
    newState.selected = !newState.selected;

    if (!newState.selected) {
      newState.isPrimary = false;
    }

    return newState;
  }

  return state;
}

import 'package:fish_redux/fish_redux.dart';

import '../language_selection_page/language_item_component/state.dart';

import 'action.dart';
import 'state.dart';

Reducer<LanguagesState> buildReducer() {
  return asReducer(
    <Object, Reducer<LanguagesState>>{
      LanguagesAction.markAsPrimary: _markAsPrimary,
      LanguagesAction.handleLanguagesSelected: _handleLanguagesSelected,
    },
  );
}

LanguagesState _markAsPrimary(LanguagesState state, Action action) {
  final item = action.payload as LanguageItemState;

  if (item.isPrimary == true) {
    return state;
  }

  final newState = state.clone();

  var currentPrimaryItemIndex = newState.items.indexWhere((x) => x.isPrimary);

  if (currentPrimaryItemIndex > -1) {
    final currentPrimaryItem = newState.items.firstWhere(
      (element) => element.isPrimary,
    );
    currentPrimaryItem.isPrimary = false;
  }

  final newPrimaryItem = newState.items.firstWhere(
    (element) => element.code == item.code,
  );

  newPrimaryItem.isPrimary = true;

  return newState;
}

LanguagesState _handleLanguagesSelected(LanguagesState state, Action action) {
  final LanguagesState newState = state.clone();

  newState.items = action.payload as List<LanguageItemState> ?? [];

  final primaryItems = newState.items.where((e) => e.isPrimary).toList();

  if (primaryItems.length > 0) {
    newState.currentPrimaryItem = primaryItems.first;
  }

  return newState;
}

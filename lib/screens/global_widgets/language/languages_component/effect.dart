import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/route_name.dart';
import 'package:tcga_flutter/utils/show_picker.dart';

import '../language_selection_page/language_item_component/state.dart';

import 'action.dart';
import 'state.dart';

Effect<LanguagesState> buildEffect() {
  return combineEffects(<Object, Effect<LanguagesState>>{
    LanguagesAction.selectLanguages: _selectLangauges,
  });
}

void _selectLangauges(Action action, Context<LanguagesState> ctx) async {
  final items = ctx.state.items;

  var selectedItems = await showModal(
    ctx.context,
    RouteName.languagesSelection,
    args: {
      'selected-items': items,
    },
  );

  if (selectedItems is List<LanguageItemState>) {
    ctx.dispatch(
      LanguagesActionCreator.handleLanguagesSelected(
        selectedItems,
      ),
    );
  }
}

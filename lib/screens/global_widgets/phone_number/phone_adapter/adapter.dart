import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/global_widgets/phone_number/phone_item_component/component.dart';
import 'package:tcga_flutter/screens/global_widgets/phone_number/phone_picker_page/state.dart';

import 'reducer.dart';

class PhoneAdapter extends SourceFlowAdapter<PhonePickerState> {
  PhoneAdapter()
      : super(
          pool: <String, Component<Object>>{
            'phone-item': PhoneItemComponent(),
          },
          reducer: buildReducer(),
        );
}

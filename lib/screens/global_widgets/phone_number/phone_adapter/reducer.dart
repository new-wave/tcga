import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/global_widgets/phone_number/phone_picker_page/state.dart';

import 'action.dart';

Reducer<PhonePickerState> buildReducer() {
  return asReducer(
    <Object, Reducer<PhonePickerState>>{
      PhoneAction.action: _onAction,
    },
  );
}

PhonePickerState _onAction(PhonePickerState state, Action action) {
  final PhonePickerState newState = state.clone();
  return newState;
}

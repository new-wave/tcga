import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/global_widgets/phone_number/phone_item_component/state.dart';

class PhonePickerState extends MutableSource
    implements Cloneable<PhonePickerState> {
  List<PhoneItemState> items;
  bool confirmRequired;

  @override
  PhonePickerState clone() {
    return PhonePickerState()
      ..items = items
      ..confirmRequired = confirmRequired;
  }

  @override
  Object getItemData(int index) {
    return items[index];
  }

  @override
  String getItemType(int index) {
    return 'phone-item';
  }

  @override
  int get itemCount => items?.length ?? 0;

  @override
  void setItemData(int index, Object data) {
    items[index] = data;
  }
}

PhonePickerState initState(Map<String, dynamic> args) {
  return PhonePickerState()
    ..items = []
    ..confirmRequired = false;
}

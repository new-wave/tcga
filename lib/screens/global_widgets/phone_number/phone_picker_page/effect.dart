import 'package:flutter/material.dart' hide Action;

import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/global_widgets/phone_number/phone_item_component/state.dart';

import '../manager.dart';
import 'action.dart';
import 'state.dart';

final _phoneManager = PhoneManager();

Effect<PhonePickerState> buildEffect() {
  return combineEffects(<Object, Effect<PhonePickerState>>{
    Lifecycle.initState: _init,
    PhonePickerAction.search: _search,
    PhonePickerAction.confirm: _confirm,
  });
}

Future _init(Action action, Context<PhonePickerState> ctx) async {
  final items = await _phoneManager.queryPhoneNumber();
  ctx.dispatch(PhonePickerActionCreator.initialize(items));
}

Future _search(Action action, Context<PhonePickerState> ctx) async {
  final keyword = action.payload as String ?? '';
  final items = await _phoneManager.queryPhoneNumber(keyword: keyword);
  ctx.dispatch(PhonePickerActionCreator.initialize(items));
}

Future _confirm(Action action, Context<PhonePickerState> ctx) async {
  final selectedItem = action.payload as PhoneItemState;

  if (selectedItem == null) return;

  Navigator.of(ctx.context).pop(selectedItem);
}

import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/global_widgets/phone_number/phone_adapter/adapter.dart';

import 'effect.dart';
import 'reducer.dart';
import 'state.dart';
import 'view.dart';

class PhonePickerPage
    extends Page<PhonePickerState, Map<String, dynamic>> {
  PhonePickerPage()
      : super(
          initState: initState,
          effect: buildEffect(),
          reducer: buildReducer(),
          view: buildView,
          dependencies: Dependencies<PhonePickerState>(
              adapter: NoneConn<PhonePickerState>() + PhoneAdapter(),
              slots: <String, Dependent<PhonePickerState>>{}),
          middleware: <Middleware<PhonePickerState>>[],
        );
}

import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/global_widgets/phone_number/phone_item_component/state.dart';

enum PhonePickerAction {
  initialize,
  search,
  confirm,
}

class PhonePickerActionCreator {
  static Action initialize(List<PhoneItemState> items) {
    return Action(PhonePickerAction.initialize, payload: items);
  }

  static Action search(String keyword) {
    return Action(PhonePickerAction.search, payload: keyword);
  }

  static Action confirm([PhoneItemState item]) {
    return Action(PhonePickerAction.confirm, payload: item);
  }
}

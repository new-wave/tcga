import 'package:fish_redux/fish_redux.dart';

import 'action.dart';
import 'state.dart';

Reducer<PhonePickerState> buildReducer() {
  return asReducer(
    <Object, Reducer<PhonePickerState>>{
      PhonePickerAction.initialize: _initialize,
    },
  );
}

PhonePickerState _initialize(PhonePickerState state, Action action) {
  final PhonePickerState newState = state.clone();
  newState.items = action.payload ?? [];
  return newState;
}

import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/global_widgets/phone_number/phone_item_component/state.dart';

import 'action.dart';
import 'state.dart';

Reducer<PhonePickerState> buildReducer() {
  return asReducer(
    <Object, Reducer<PhonePickerState>>{
      PhonePickerAction.handleCountrySelected: _handleCountrySelected,
      PhonePickerAction.handleTextChanged: _handleTextChanged,
    },
  );
}

PhonePickerState _handleCountrySelected(PhonePickerState state, Action action) {
  final PhonePickerState newState = state.clone();
  PhoneItemState item = action.payload;
  newState.phoneItemState = item;
  newState.countryCode = item?.id;
  return newState;
}

PhonePickerState _handleTextChanged(PhonePickerState state, Action action) {
  final PhonePickerState newState = state.clone();
  newState.isShowWarningText = action.payload;
  return newState;
}

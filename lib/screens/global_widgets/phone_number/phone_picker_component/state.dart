import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:tcga_flutter/screens/global_widgets/phone_number/phone_item_component/state.dart';

class PhonePickerState implements Cloneable<PhonePickerState> {
  PhoneItemState phoneItemState;
  String countryCode;
  TextEditingController phoneEditingController;

  bool isRequired;
  bool isCheckField;
  bool isShowWarningText = true;
  String warningText;

  PhonePickerState({
    this.phoneItemState,
    this.isRequired = true,
    this.isCheckField = false,
    this.isShowWarningText = true,
    this.phoneEditingController,
    this.countryCode,
    this.warningText = '',
  });

  @override
  PhonePickerState clone() {
    return PhonePickerState()
      ..phoneItemState = phoneItemState
      ..phoneEditingController = phoneEditingController
      ..isCheckField = isCheckField
      ..isRequired = isRequired
      ..isShowWarningText = isShowWarningText
      ..countryCode = countryCode
      ..warningText = warningText;
  }
}

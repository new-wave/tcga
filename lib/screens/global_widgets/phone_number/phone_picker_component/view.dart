import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'action.dart';
import 'state.dart';

Widget buildView(
    PhonePickerState state, Dispatch dispatch, ViewService viewService) {
  final node = FocusScope.of(viewService.context);
  bool isRequired = state.isRequired;
  if (!isRequired) {
    if (state.phoneEditingController.text.length > 0) {
      isRequired = true;
    } else {
      isRequired = false;
    }
  }

  return Column(
    children: [
      Container(
        height: 36,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          color: colorBackground,
        ),
        padding: EdgeInsets.symmetric(
          horizontal: 16.0,
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            InkWell(
              onTap: () {
                dispatch(
                  PhonePickerActionCreator.selectCountry(),
                );
              },
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    state?.phoneItemState?.flag ?? '    ',
                    style: textStyleTitleSecondary,
                  ),
                  SizedBox(
                    width: 8.0,
                  ),
                  SvgPicture.asset(
                    'assets/icons/ic_down.svg',
                  ),
                  SizedBox(
                    width: 8.0,
                  ),
                  Text(
                    state?.phoneItemState?.dialCode ?? '  ',
                    style: textStylePrimary,
                  ),
                ],
              ),
            ),
            Expanded(
              child: TextField(
                // ignore: null_aware_in_condition
                inputFormatters: state.phoneItemState?.maskFormatter?.isNotEmpty ?? false
                    ? [state.phoneItemState?.maskFormatter[0]]
                    : [],
                onChanged: (text) {
                  // dispatch(
                  //   PhonePickerActionCreator.changeText(text),
                  // );
                },
                decoration: InputDecoration(
                  counterText: '',
                  hintText: state.isRequired
                      ? AppLocalizations.of(viewService.context)
                          .text('sign_up.phone_number')
                      : AppLocalizations.of(viewService.context)
                          .text('general_information.placeholder_phone_number'),
                  hintStyle: TextStyle(
                    fontSize: 14,
                  ),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5.0),
                    gapPadding: 0,
                    borderSide: BorderSide.none,
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5.0),
                    gapPadding: 0,
                    borderSide: BorderSide.none,
                  ),
                  contentPadding: const EdgeInsets.symmetric(
                    horizontal: 16,
                    vertical: 8,
                  ),
                ),
                keyboardType: TextInputType.number,
                controller: state.phoneEditingController,
                onEditingComplete: () => node.unfocus(),
              ),
            ),
          ],
        ),
      ),
      Visibility(
        visible: (isRequired && state.isCheckField && state.isShowWarningText),
        child: Container(
          alignment: Alignment.centerLeft,
          margin: const EdgeInsets.only(top: 5),
          child: Text(
            state.warningText,
            style: textStylePrimaryRed,
            maxLines: 2,
          ),
        ),
      ),
    ],
  );
}

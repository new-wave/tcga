import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/global_widgets/phone_number/manager.dart';
import 'package:tcga_flutter/screens/global_widgets/phone_number/phone_item_component/state.dart';

import '../../../../route_name.dart';
import 'action.dart';
import 'state.dart';
import '../../../../utils/utils.dart';
import 'dart:io' show Platform;

final _manager = PhoneManager();
int countTextIsNotEmpty = 0;

Effect<PhonePickerState> buildEffect() {
  return combineEffects(<Object, Effect<PhonePickerState>>{
  //  Lifecycle.initState: _init,
    PhonePickerAction.selectCountry: _selectCountry,
    PhonePickerAction.changeText: _changeText,
  });
}

Future _init(Action action, Context<PhonePickerState> ctx) async {
  PhoneItemState item;
  if (ctx.state?.phoneItemState == null) {
    String countryCode = Platform.localeName.split('_')[1];
    item = await _manager.getCountryByCode(ctx.context, code: countryCode);
  } else {
    item = await _manager.getCountryByCode(ctx.context,
        code: ctx.state?.phoneItemState?.id);
  }
  ctx.dispatch(
    PhonePickerActionCreator.handleCountrySelected(
      item,
    ),
  );
}

Future _changeText(Action action, Context<PhonePickerState> ctx) async {
  String text = action.payload ?? '';
  if (text.isEmpty) {
    countTextIsNotEmpty = 0;
    ctx.dispatch(
      PhonePickerActionCreator.handleTextChanged(
        true,
      ),
    );
  } else {
    countTextIsNotEmpty++;
    if (countTextIsNotEmpty == 1) {
      ctx.dispatch(
        PhonePickerActionCreator.handleTextChanged(
          false,
        ),
      );
    }
  }
}

Future _selectCountry(Action action, Context<PhonePickerState> ctx) async {
  final item = await ctx.navigateTo(
    RouteName.phonePicker,
    arguments: {
      'selected-item': ctx.state.phoneItemState,
    },
  );

  if (item == null) return;

  ctx.dispatch(
    PhonePickerActionCreator.handleCountrySelected(
      item,
    ),
  );
}

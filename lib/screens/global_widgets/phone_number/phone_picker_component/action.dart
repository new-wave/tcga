import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/global_widgets/phone_number/phone_item_component/state.dart';

enum PhonePickerAction {
  selectCountry,
  handleCountrySelected,
  changeText,
  handleTextChanged,
}

class PhonePickerActionCreator {
  static Action selectCountry() {
    return const Action(PhonePickerAction.selectCountry);
  }

  static Action changeText(String text) {
    return Action(
      PhonePickerAction.changeText,
      payload: text,
    );
  }

  static Action handleCountrySelected(PhoneItemState item) {
    return Action(
      PhonePickerAction.handleCountrySelected,
      payload: item,
    );
  }

  static Action handleTextChanged(bool isShowWarningText) {
    return Action(
      PhonePickerAction.handleTextChanged,
      payload: isShowWarningText,
    );
  }
}

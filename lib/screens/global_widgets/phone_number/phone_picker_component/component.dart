import 'package:fish_redux/fish_redux.dart';

import 'effect.dart';
import 'reducer.dart';
import 'state.dart';
import 'view.dart';

class PhonePickerComponent extends Component<PhonePickerState> {
  PhonePickerComponent()
      : super(
          effect: buildEffect(),
          reducer: buildReducer(),
          view: buildView,
          // dependencies: Dependencies<PhonePickerState>(
          //     adapter: null,
          //     slots: <String, Dependent<PhonePickerState>>{}),
        );
}

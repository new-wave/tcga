import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:tcga_flutter/database/models/country.dart';
import 'package:tcga_flutter/database/repository.dart';
import 'package:tcga_flutter/screens/global_widgets/phone_number/phone_item_component/state.dart';

class PhoneManager {
  static final _singleton = PhoneManager._internal();
  final QueryOnlyRepository<Country> _countryRepository = QueryOnlyRepository();

  factory PhoneManager() {
    return _singleton;
  }

  PhoneManager._internal();

  Future<List<PhoneItemState>> queryPhoneNumber({
    String keyword,
    int pageSize = 400,
  }) async {
    final keywordUpperCase = (keyword ?? '').trim().toUpperCase();
    final filter = keywordUpperCase.isEmpty
        ? null
        : (Country item) => item.name.toUpperCase().contains(keywordUpperCase);

    final items = await _countryRepository.query(
      filter: filter,
      pageSize: pageSize,
    );

    return items
        .map((item) => PhoneItemState(
              id: item?.isoCode,
              flag: item?.flag,
              name: item?.name,
              dialCode: item?.dialCode,
              mask: item?.mask,
              maskFormatter: item?.mask?.map((e) {
                return MaskTextInputFormatter(
                  mask: e != null ? e : '',
                  filter: {"#": RegExp(r'[0-9]')},
                );
              })?.toList(growable: false),
            ))
        .toList();
  }

  Future<PhoneItemState> getCountryByCode(BuildContext context,
      {String code}) async {
    if (code == null) return null;
    if (code.isEmpty) return null;

    code = code.trim();
    final countries = await _countryRepository.query();
    Country country = countries.firstWhere((element) => element.isoCode == code,
        orElse: () => null);
    return PhoneItemState(
      id: country?.isoCode,
      flag: country?.flag,
      name: country?.name,
      dialCode: country?.dialCode,
      mask: country?.mask,
      maskFormatter: country?.mask?.map((e) {
        return MaskTextInputFormatter(
          mask: e != null ? e : '',
          filter: {"#": RegExp(r'[0-9]')},
        );
      })?.toList(growable: false),
    );
  }

  Future<PhoneItemState> getCountryByDialCode(BuildContext context,
      {String dialCode}) async {
    if (dialCode == null) return null;
    if (dialCode.isEmpty) return null;

    dialCode = dialCode.trim();
    final countries = await _countryRepository.query();
    Country country = countries.firstWhere(
        (element) => element.dialCode == dialCode,
        orElse: () => null);
    return PhoneItemState(
      id: country?.isoCode,
      flag: country?.flag,
      name: country?.name,
      dialCode: country?.dialCode,
      mask: country?.mask,
      maskFormatter: country?.mask?.map((e) {
        return MaskTextInputFormatter(
          mask: e != null ? e : '',
          filter: {"#": RegExp(r'[0-9]')},
        );
      })?.toList(growable: false),
    );
  }

// Future<List<Country>> convertFromJSON(BuildContext context) async {
//   try {
//     var countriesJson = await DefaultAssetBundle.of(context)
//         .loadString('assets/json/countries.json');
//     var counties = json.decode(countriesJson) as List;
//     List<Country> data = counties.map((e) => Country.fromJson(e)).toList();
//     return data ?? [];
//   } catch (e) {
//     return [];
//   }
// }
}

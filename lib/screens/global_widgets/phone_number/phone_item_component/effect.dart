import 'package:fish_redux/fish_redux.dart';

import 'state.dart';
import 'action.dart';

Effect<PhoneItemState> buildEffect() {
  return combineEffects(<Object, Effect<PhoneItemState>>{
    PhoneItemAction.action: _onAction,
  });
}

void _onAction(Action action, Context<PhoneItemState> ctx) {}

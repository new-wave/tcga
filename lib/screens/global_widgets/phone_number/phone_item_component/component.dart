import 'package:fish_redux/fish_redux.dart';

import 'state.dart';
import 'effect.dart';
import 'reducer.dart';
import 'view.dart';

class PhoneItemComponent extends Component<PhoneItemState> {
  PhoneItemComponent()
      : super(
          effect: buildEffect(),
          reducer: buildReducer(),
          view: buildView,
          dependencies: Dependencies<PhoneItemState>(
              adapter: null, slots: <String, Dependent<PhoneItemState>>{}),
        );
}

import 'package:fish_redux/fish_redux.dart';

import 'state.dart';
import 'action.dart';

Reducer<PhoneItemState> buildReducer() {
  return asReducer(
    <Object, Reducer<PhoneItemState>>{
      PhoneItemAction.action: _onAction,
    },
  );
}

PhoneItemState _onAction(PhoneItemState state, Action action) {
  final PhoneItemState newState = state.clone();
  return newState;
}

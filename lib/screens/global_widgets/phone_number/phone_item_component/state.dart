import 'package:fish_redux/fish_redux.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

class PhoneItemState implements Cloneable<PhoneItemState> {
  String id;
  String flag;
  String name;
  String dialCode;
  List<String> mask;
  List<MaskTextInputFormatter> maskFormatter;

  PhoneItemState({
    this.id,
    this.flag,
    this.name,
    this.dialCode,
    this.mask,
    this.maskFormatter,
  });

  @override
  PhoneItemState clone() {
    return PhoneItemState()
      ..id = id
      ..flag = flag
      ..name = name
      ..dialCode = dialCode
      ..mask = mask
      ..maskFormatter = maskFormatter;
  }
}

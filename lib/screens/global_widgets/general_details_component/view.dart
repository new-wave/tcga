import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/database/models/enums.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/widget/entry_widget.dart';
import 'package:tcga_flutter/widget/import.dart';
import 'package:tcga_flutter/widget/picker_widget.dart';

import 'action.dart';
import 'state.dart';

Widget buildView(
    GeneralDetailsState state, Dispatch dispatch, ViewService viewService) {
  return Container(
    padding: const EdgeInsets.all(16),
    child: GestureDetector(
      onTap: () {
        dispatch(GeneralDetailsActionCreator.unFocus());
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            AppLocalizations.of(viewService.context)
                .text('general_information.header'),
            style: textStyleTitleThird,
          ),
          SizedBox(
            height: 8,
          ),
          SizedBox(
            height: 46,
            child: Row(
              children: [
                Expanded(
                  child: PickerWidget(
                    initialIndex: state.prefix?.index,
                    items: state.prefixes.values.toList(),
                    title: AppLocalizations.of(viewService.context)
                        .text('general_information.placeholder_prefix'),
                    indexSelected: (index) {
                      dispatch(GeneralDetailsActionCreator.selectPrefix(index));
                    },
                  ),
                ),
                SizedBox(
                  width: 16,
                ),
                Expanded(
                  child: PickerWidget(
                    initialIndex: state.suffix?.index,
                    items: state.suffixes.values.toList(),
                    title: AppLocalizations.of(viewService.context)
                        .text('general_information.placeholder_suffix'),
                    indexSelected: (index) {
                      dispatch(GeneralDetailsActionCreator.selectSuffix(index));
                    },
                  ),
                )
              ],
            ),
          ),
          SizedBox(
            height: 8,
          ),
          EntryWidget(
            title: AppLocalizations.of(viewService.context)
                .text('general_information.placeholder_first_name'),
            keyboardType: TextInputType.name,
            titleStyle: textStyleHinttext,
            titlePosition: TitlePosition.leading,
            controller: state.firstNameController,
            warningText:
                AppLocalizations.instance.text('common.validator.first_name'),
            isCheckField: state.isCheckField ?? false,
            isRequired: true,
          ),
          SizedBox(
            height: 8,
          ),
          EntryWidget(
            title: AppLocalizations.of(viewService.context)
                .text('general_information.placeholder_last_name'),
            keyboardType: TextInputType.name,
            titleStyle: textStyleHinttext,
            titlePosition: TitlePosition.leading,
            controller: state.lastNameController,
            warningText:
                AppLocalizations.instance.text('common.validator.last_name'),
            isCheckField: state.isCheckField ?? false,
            isRequired: true,
          ),
          SizedBox(
            height: 8,
          ),
          DatePickerWidget(
            title: AppLocalizations.of(viewService.context)
                .text('general_information.placeholder_date_of_birth'),
            titleStyle: textStyleHinttext,
            titlePosition: TitlePosition.leading,
            initial: state.dateOfBirth,
            max: DateTime.now(),
            min: DateTime.now().add(
              Duration(days: -120 * 365),
            ),
            dateSelected: (date) {
              dispatch(
                GeneralDetailsActionCreator.selectDateOfBirth(
                  date,
                ),
              );
            },
            warningText: AppLocalizations.instance.text('common.validator.dob'),
            isRequired: true,
            isCheckField: state.isCheckField ?? false,
          ),
          SizedBox(
            height: 8,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 5,
                child: PickerWidget(
                  initialIndex: state.gender?.index,
                  items: state.genders.values.toList(),
                  title: AppLocalizations.of(viewService.context)
                      .text('general_information.placeholder_gender'),
                  indexSelected: (index) {
                    dispatch(
                      GeneralDetailsActionCreator.selectGender(
                        index,
                      ),
                    );
                  },
                  warningText:
                      AppLocalizations.instance.text('common.validator.gender'),
                  isRequired: true,
                  isCheckField: state.isCheckField ?? false,
                ),
              ),
              SizedBox(
                width: 8,
              ),
              Expanded(
                flex: 7,
                child: PickerWidget(
                  initialIndex: state.maritalStatus?.index,
                  items: state.maritalStatuses.values.toList(),
                  title: AppLocalizations.of(viewService.context)
                      .text('general_information.placeholder_marital_status'),
                  indexSelected: (index) {
                    dispatch(
                      GeneralDetailsActionCreator.selectMaritalStatus(
                        index,
                      ),
                    );
                  },
                ),
              )
            ],
          ),
          SizedBox(
            height: 8,
          ),
          viewService.buildComponent('phone-picker-component'),
          // PhonePickerWidget(
          //   tap: () {},
          //   phoneNumberInitial: state.phoneNumber,
          //   onChangePhoneNumber: (phoneNumber) {
          //
          //   },
          //   onInputValidated: (isValid){
          //     dispatch(
          //       GeneralDetailsActionCreator.checkPhoneValid(
          //         isValid,
          //       ),
          //     );
          //   },
          //   controller: state.phoneNumberController,
          //   warningText:
          //       AppLocalizations.instance.text('common.validator.phone_number'),
          //   isRequired: state.isRequiredPhone,
          //   isCheckField: state.isCheckField,
          // ),
          SizedBox(
            height: 8,
          ),
          EntryWidget(
            title: AppLocalizations.of(viewService.context)
                .text('general_information.placeholder_email_address'),
            keyboardType: TextInputType.emailAddress,
            titleStyle: textStyleHinttext,
            titlePosition: TitlePosition.leading,
            controller: state.emailAddressController,
            isCheckField: state.isCheckField ?? false,
            isRequired: false,
            entryWidgetType: EntryWidgetType.Email,
            warningText: AppLocalizations.instance
                .text('common.validator.email_address_is_not_valid'),
          ),
        ],
      ),
    ),
  );
}

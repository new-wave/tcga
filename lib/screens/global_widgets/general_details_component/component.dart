import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/global_widgets/phone_number/phone_picker_component/component.dart';

import 'effect.dart';
import 'reducer.dart';
import 'state.dart';
import 'view.dart';

class GeneralDetailsComponent extends Component<GeneralDetailsState> {
  GeneralDetailsComponent()
      : super(
          effect: buildEffect(),
          reducer: buildReducer(),
          view: buildView,
          dependencies: Dependencies<GeneralDetailsState>(
              adapter: null,
              slots: <String, Dependent<GeneralDetailsState>>{
                'phone-picker-component':
                    PhonePickerStateConnector() + PhonePickerComponent(),
              }),
        );
}

import 'package:fish_redux/fish_redux.dart';

enum GeneralDetailsAction {
  selectPrefix,
  selectSuffix,
  selectGender,
  selectMaritalStatus,
  selectDateOfBirth,
  unFocus,
}

class GeneralDetailsActionCreator {
  static Action unFocus() {
    return Action(
      GeneralDetailsAction.unFocus,
    );
  }

  static Action selectSuffix(int index) {
    return Action(
      GeneralDetailsAction.selectSuffix,
      payload: index,
    );
  }

  static Action selectPrefix(int index) {
    return Action(
      GeneralDetailsAction.selectPrefix,
      payload: index,
    );
  }

  static Action selectGender(int index) {
    return Action(
      GeneralDetailsAction.selectGender,
      payload: index,
    );
  }

  static Action selectMaritalStatus(int index) {
    return Action(
      GeneralDetailsAction.selectMaritalStatus,
      payload: index,
    );
  }

  static Action selectDateOfBirth(DateTime date) {
    return Action(
      GeneralDetailsAction.selectDateOfBirth,
      payload: date,
    );
  }
}

import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart' hide Action, Page;
import 'action.dart';
import 'state.dart';

Effect<GeneralDetailsState> buildEffect() {
  return combineEffects(<Object, Effect<GeneralDetailsState>>{
    GeneralDetailsAction.unFocus: _unFocus,
  });
}

void _unFocus(Action action, Context<GeneralDetailsState> ctx) {
  FocusScopeNode currentFocus = FocusScope.of(ctx.context);
  if (!currentFocus.hasPrimaryFocus) {
    currentFocus.unfocus();
  }
}

import 'package:fish_redux/fish_redux.dart';

import 'action.dart';
import 'state.dart';

Reducer<GeneralDetailsState> buildReducer() {
  return asReducer(
    <Object, Reducer<GeneralDetailsState>>{
      GeneralDetailsAction.selectDateOfBirth: _selectDateOfBirth,
      GeneralDetailsAction.selectGender: _selectGender,
      GeneralDetailsAction.selectMaritalStatus: _selectMaritalStatus,
      GeneralDetailsAction.selectPrefix: _selectPrefix,
      GeneralDetailsAction.selectSuffix: _selectSuffix,
    },
  );
}

GeneralDetailsState _selectDateOfBirth(
    GeneralDetailsState state, Action action) {
  final GeneralDetailsState newState = state.clone();

  newState.dateOfBirth = action.payload as DateTime;

  return newState;
}

GeneralDetailsState _selectGender(GeneralDetailsState state, Action action) {
  final GeneralDetailsState newState = state.clone();
  int index = action.payload;
  newState.gender = newState.genders.keys.elementAt(index);

  return newState;
}

GeneralDetailsState _selectMaritalStatus(
    GeneralDetailsState state, Action action) {
  final GeneralDetailsState newState = state.clone();
  int index = action.payload;
  newState.maritalStatus =
      index >= 0 ? newState.maritalStatuses.keys.elementAt(index) : null;

  return newState;
}

GeneralDetailsState _selectPrefix(GeneralDetailsState state, Action action) {
  final GeneralDetailsState newState = state.clone();
  int index = action.payload;
  newState.prefix = index >= 0 ? newState.prefixes.keys.elementAt(index) : null;

  return newState;
}

GeneralDetailsState _selectSuffix(GeneralDetailsState state, Action action) {
  final GeneralDetailsState newState = state.clone();
  int index = action.payload;
  newState.suffix = index >= 0 ? newState.suffixes.keys.elementAt(index) : null;

  return newState;
}

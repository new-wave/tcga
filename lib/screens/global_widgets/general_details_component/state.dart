import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:tcga_flutter/database/models/enums.dart';
import 'package:tcga_flutter/screens/global_widgets/phone_number/phone_item_component/state.dart';
import 'package:tcga_flutter/screens/global_widgets/phone_number/phone_picker_component/state.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';

class GeneralDetailsState implements Cloneable<GeneralDetailsState> {
  Map<PrefixType, String> get prefixes => {
        PrefixType.Mr: 'Mr',
        PrefixType.Ms: 'Ms',
        PrefixType.Mrs: 'Mrs',
        PrefixType.Dr: 'Dr',
      };

  Map<SuffixType, String> get suffixes => {
        SuffixType.MD: 'MD',
        SuffixType.Second: 'Second',
        SuffixType.Third: 'Third',
      };

  Map<AdministrativeGenderType, String> get genders => {
        AdministrativeGenderType.Male: 'Male',
        AdministrativeGenderType.Female: 'Female',
        AdministrativeGenderType.Other: 'Other',
        AdministrativeGenderType.Unknown: 'Unknown',
      };

  Map<MaritalStatusType, String> get maritalStatuses => {
    //    MaritalStatusType.Annulled: 'Annulled',
        MaritalStatusType.Divorced: 'Divorced',
    //    MaritalStatusType.Interlocutory: 'Interlocutory',
        MaritalStatusType.LegallySeparated: 'LegallySeparated',
        MaritalStatusType.Married: 'Married',
        MaritalStatusType.Polygamous: 'Polygamous',
   //     MaritalStatusType.NeverMarried: 'NeverMarried',
        MaritalStatusType.DomesticPartner: 'DomesticPartner',
        MaritalStatusType.Unmarried: 'Unmarried',
        MaritalStatusType.Widowed: 'Widowed',
        MaritalStatusType.Unknown: 'Unknown',
      };

  PrefixType prefix;
  SuffixType suffix;
  AdministrativeGenderType gender;
  MaritalStatusType maritalStatus;
  DateTime dateOfBirth;

  TextEditingController firstNameController;
  TextEditingController lastNameController;
  TextEditingController emailAddressController;
  TextEditingController phoneNumberController;

  bool isCheckField = false;

  PhoneItemState phoneItemState;
  bool isRequiredPhone = false;

  @override
  GeneralDetailsState clone() {
    return GeneralDetailsState()
      ..gender = gender
      ..maritalStatus = maritalStatus
      ..prefix = prefix
      ..suffix = suffix
      ..dateOfBirth = dateOfBirth
      ..isCheckField = isCheckField
      ..firstNameController = firstNameController
      ..lastNameController = lastNameController
      ..emailAddressController = emailAddressController
      ..phoneNumberController = phoneNumberController
      ..phoneItemState = phoneItemState
      ..isRequiredPhone = isRequiredPhone;
  }
}

GeneralDetailsState initState(Map<String, dynamic> args) {
  final state = GeneralDetailsState()
    ..isCheckField = false
    ..isRequiredPhone = false;
  return state;
}

class PhonePickerStateConnector
    extends ConnOp<GeneralDetailsState, PhonePickerState> {
  @override
  PhonePickerState get(GeneralDetailsState state) {
    int maskLength = state.phoneItemState?.mask != null
        ? state.phoneItemState?.mask[0].length
        : 0 ?? 0;
    return PhonePickerState(
      phoneItemState: state.phoneItemState,
      isRequired: state.isRequiredPhone,
      phoneEditingController: state.phoneNumberController,
      isCheckField: state.isCheckField,
      isShowWarningText: state.phoneNumberController.text.length != maskLength,
      warningText: state.phoneNumberController.text.length == 0
          ? AppLocalizations.instance.text('common.validator.phone_number')
          : AppLocalizations.instance
              .text('common.validator.phone_number_is_not_valid'),
    );
  }

  @override
  void set(GeneralDetailsState state, PhonePickerState subState) {
    state.phoneItemState = subState.phoneItemState;
    state.isRequiredPhone = subState.isRequired;
    state.phoneNumberController = subState.phoneEditingController;
    state.isCheckField = subState.isCheckField;
  }
}

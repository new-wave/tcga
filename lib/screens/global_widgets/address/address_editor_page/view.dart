import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/widget/entry_widget.dart';
import 'package:tcga_flutter/widget/import.dart';

import 'action.dart';
import 'state.dart';

Widget buildView(
    AddressEditorState state, Dispatch dispatch, ViewService viewService) {
  final node = FocusScope.of(viewService.context);
  return Container(
    padding: EdgeInsets.only(
      top: 4,
    ),
    height: 430,
    child: Scaffold(
      resizeToAvoidBottomInset: false,
      key: state.scaffoldKey,
      body: Padding(
        padding: edgeAllInsetsPrimary,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Row(
              children: [
                Spacer(
                  flex: 5,
                ),
                Text(
                  AppLocalizations.of(viewService.context)
                      .text('address.header'),
                  style: textStyleThird,
                ),
                Spacer(
                  flex: 4,
                ),
                GestureDetector(
                    onTap: () async => Navigator.pop(viewService.context),
                    child: SvgPicture.asset("assets/icons/ic_close.svg")),
              ],
            ),
            InkWell(
              onTap: () => dispatch(
                AddressEditorActionCreator.selectFromMap(),
              ),
              child: Container(
                padding: const EdgeInsets.symmetric(
                  horizontal: 8,
                  vertical: 16,
                ),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    SvgPicture.asset(
                      'assets/icons/ic_map.svg',
                    ),
                    SizedBox(
                      width: 8.0,
                    ),
                    Text(
                      AppLocalizations.instance
                          .text('mileage.routes.select_from_map'),
                      style: textStylePrimaryNormal,
                    ),
                  ],
                ),
              ),
            ),
            EntryWidget(
              title: AppLocalizations.of(viewService.context)
                  .text('address.editor.placeholder_street'),
              keyboardType: TextInputType.streetAddress,
              controller: state.addressController,
              warningText:
                  AppLocalizations.instance.text('common.validator.street'),
              isRequired: true,
              isCheckField: state.isCheckField ?? false,
            ),
            SizedBox(
              height: 8,
            ),
            EntryWidget(
              title: AppLocalizations.of(viewService.context)
                  .text('address.editor.placeholder_city'),
              keyboardType: TextInputType.streetAddress,
              controller: state.cityController,
              warningText:
                  AppLocalizations.instance.text('common.validator.city'),
              isRequired: true,
              isCheckField: state.isCheckField ?? false,
            ),
            SizedBox(
              height: 8,
            ),
            EntryWidget(
              title: AppLocalizations.of(viewService.context)
                  .text('address.editor.placeholder_state'),
              keyboardType: TextInputType.streetAddress,
              controller: state.stateController,
              warningText:
                  AppLocalizations.instance.text('common.validator.state'),
              isRequired: true,
              isCheckField: state.isCheckField ?? false,
            ),
            SizedBox(
              height: 8,
            ),
            EntryWidget(
              title: AppLocalizations.of(viewService.context)
                  .text('address.editor.placeholder_zip_code'),
              keyboardType: TextInputType.number,
              controller: state.zipController,
              onEditingComplete: () => node.unfocus(),
              warningText:
                  AppLocalizations.instance.text('common.validator.zip_code'),
              isRequired: true,
              isCheckField: state.isCheckField ?? false,
            ),
          ],
        ),
      ),
      bottomNavigationBar: Builder(
        builder: (context) => Container(
          decoration: BoxDecoration(
              color: colorPrimary,
              border: Border.all(
                color: colorPrimary,
              )),
          child: ButtonWidget(
            textButton:
                AppLocalizations.of(viewService.context).text('common.save'),
            onPress: () {
              dispatch(AddressEditorActionCreator.validateAndSubmit());
            },
          ),
        ),
      ),
    ),
  );
}

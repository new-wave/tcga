import 'package:flutter/material.dart' hide Action;
import 'package:fish_redux/fish_redux.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/screens/mileage_maps/mileage_page/routes_tab_component/address_picker_page/mylist_address_component/effect.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:uuid/uuid.dart';
import '../../../../route_name.dart';
import '../../../../utils/utils.dart';
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/address_item_component/state.dart';

import 'state.dart';
import 'action.dart';
import 'validator.dart';

final _uuid = Uuid();

Effect<AddressEditorState> buildEffect() {
  return combineEffects(<Object, Effect<AddressEditorState>>{
    AddressEditorAction.validateAndSubmit: _validateAndSubmit,
    AddressEditorAction.selectFromMap: _selectFromMap,
  });
}

void _selectFromMap(Action action, Context<AddressEditorState> ctx) async {
  final selectedItem = await ctx.navigateTo(
    RouteName.addressPickerMaps,
  ) as AddressItemState;
  if (selectedItem == null) return;

  ctx.dispatch(AddressEditorActionCreator.fillDataFromLocationMap(
    selectedItem,
  ));
}

void _validateAndSubmit(Action action, Context<AddressEditorState> ctx) async {
  final state = ctx.state;

  bool isValid = Validator.isValid(state);
  if (!isValid) {
    ctx.dispatch(AddressEditorActionCreator.checkField());
    return;
  }
  AddressItemState address = AddressItemState(
    id: state.id ?? _uuid.v4(),
    address: state.addressController.text.trim(),
    city: state.cityController.text.trim(),
    state: state.stateController.text.trim(),
    zip: state.zipController.text.trim(),
  );
  LatLng latLng = await getLocationFromAddress(address?.fullAddress);
  address.latLng = latLng;

  Navigator.of(ctx.context).pop(address);
}

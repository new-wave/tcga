import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/address_item_component/state.dart';

import 'action.dart';
import 'state.dart';

Reducer<AddressEditorState> buildReducer() {
  return asReducer(
    <Object, Reducer<AddressEditorState>>{
      // AddressEditorAction.validateAndSubmit: _onAction,
      AddressEditorAction.checkField: _checkFieldReducer,
      AddressEditorAction.fillDataFromLocationMap: _fillDataFromLocationMap,
    },
  );
}

AddressEditorState _fillDataFromLocationMap(
    AddressEditorState state, Action action) {
  final AddressEditorState newState = state.clone();
  AddressItemState address = action.payload;
  newState.addressController.text = address.address ?? '';
  newState.stateController.text = address.state ?? '';
  newState.cityController.text = address.city ?? '';
  newState.zipController.text = address.zip ?? '';
  return newState;
}

AddressEditorState _checkFieldReducer(AddressEditorState state, Action action) {
  final AddressEditorState newState = state.clone();
  newState.isCheckField = true;
  return newState;
}

// AddressEditorState _onAction(AddressEditorState state, Action action) {
//   final AddressEditorState newState = state.clone();
//   return newState;
// }

import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import '../addresses_component/address_item_component/state.dart';

class AddressEditorState implements Cloneable<AddressEditorState> {
  String id;

  TextEditingController addressController;
  TextEditingController cityController;
  TextEditingController stateController;
  TextEditingController zipController;

  bool isCheckField;

  final scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  AddressEditorState clone() {
    return AddressEditorState()
      ..id = id
      ..addressController = addressController
      ..cityController = cityController
      ..stateController = stateController
      ..zipController = zipController
      ..isCheckField = isCheckField;
  }
}

AddressEditorState initState(Map<String, dynamic> args) {
  final address = args['item'] as AddressItemState;

  return AddressEditorState()
    ..id = address?.id
    ..addressController = TextEditingController(
      text: address?.address,
    )
    ..cityController = TextEditingController(
      text: address?.city,
    )
    ..stateController = TextEditingController(
      text: address?.state,
    )
    ..zipController = TextEditingController(
      text: address?.zip,
    )
    ..isCheckField = false;
}

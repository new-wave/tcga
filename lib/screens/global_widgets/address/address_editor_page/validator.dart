import 'state.dart';

class Validator {
  static bool isValid(AddressEditorState state) {
    if (_isNullEmptyOrWhiteSpace(
      state.addressController.text,
    )) {
      return false;
    }
    if (_isNullEmptyOrWhiteSpace(
      state.cityController.text,
    )) {
      return false;
    }
    if (_isNullEmptyOrWhiteSpace(
      state.stateController.text,
    )) {
      return false;
    }
    if (_isNullEmptyOrWhiteSpace(
      state.zipController.text,
    )) {
      return false;
    }

    return true;
  }

  static bool _isNullEmptyOrWhiteSpace(String text) {
    if (text == null) return true;

    return text.trim().isEmpty;
  }
}

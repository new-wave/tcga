import 'package:fish_redux/fish_redux.dart';

import 'effect.dart';
import 'reducer.dart';
import 'state.dart';
import 'view.dart';

class AddressEditorPage extends Page<AddressEditorState, Map<String, dynamic>> {
  AddressEditorPage()
      : super(
          initState: initState,
          effect: buildEffect(),
          reducer: buildReducer(),
          view: buildView,
          dependencies: Dependencies<AddressEditorState>(
              adapter: null, slots: <String, Dependent<AddressEditorState>>{}),
          middleware: <Middleware<AddressEditorState>>[],
        );
}

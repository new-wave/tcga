import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/address_item_component/state.dart';

enum AddressEditorAction {
  validateAndSubmit,
  checkField,
  selectFromMap,
  fillDataFromLocationMap,
}

class AddressEditorActionCreator {
  static Action validateAndSubmit() {
    return const Action(
      AddressEditorAction.validateAndSubmit,
    );
  }

  static Action selectFromMap() {
    return Action(
      AddressEditorAction.selectFromMap,
    );
  }

  static Action fillDataFromLocationMap(AddressItemState item) {
    return Action(
      AddressEditorAction.fillDataFromLocationMap,
      payload: item,
    );
  }


  static Action checkField() {
    return const Action(
      AddressEditorAction.checkField,
    );
  }
}

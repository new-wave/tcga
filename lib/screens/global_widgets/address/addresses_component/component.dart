import 'package:fish_redux/fish_redux.dart';

import 'address_adapter/adapter.dart';
import 'effect.dart';
import 'reducer.dart';
import 'state.dart';
import 'view.dart';

class AddressesComponent extends Component<AddressesState> {
  AddressesComponent()
      : super(
          effect: buildEffect(),
          reducer: buildReducer(),
          view: buildView,
          dependencies: Dependencies<AddressesState>(
              adapter: NoneConn<AddressesState>() + AddressAdapter(),
              slots: <String, Dependent<AddressesState>>{}),
        );
}

import 'package:fish_redux/fish_redux.dart';

import 'address_item_component/state.dart';

import 'action.dart';
import 'state.dart';

Reducer<AddressesState> buildReducer() {
  return asReducer(
    <Object, Reducer<AddressesState>>{
      AddressesAction.handleCreateOrEdit: _handleCreateOrEdit,
      AddressesAction.handleDelete: _handleDelete,
    },
  );
}

AddressesState _handleCreateOrEdit(AddressesState state, Action action) {
  final AddressesState newState = state.clone();
  final item = action.payload as AddressItemState;

  final foundIndex =
      newState.items.indexWhere((element) => element.id == item.id);

  if (foundIndex > -1) {
    newState.items[foundIndex] = item;
  } else {
    newState.items.add(item);
  }

  return newState;
}

AddressesState _handleDelete(AddressesState state, Action action) {
  final AddressesState newState = state.clone();
  final item = action.payload as AddressItemState;
  item.isDelete = true;
  final foundIndex = newState.items
      .indexWhere((element) => element.id == item.id);
  if (foundIndex > -1) {
    newState.items[foundIndex] = item;
  }
  return newState;
}

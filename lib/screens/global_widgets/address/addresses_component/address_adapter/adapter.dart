import 'package:fish_redux/fish_redux.dart';

import '../address_item_component/component.dart';

import '../state.dart';

class AddressAdapter extends SourceFlowAdapter<AddressesState> {
  AddressAdapter()
      : super(
          pool: <String, Component<Object>>{
            'address-item': AddressItemComponent(),
          },
        );
}

import 'package:fish_redux/fish_redux.dart';

import 'address_item_component/state.dart';

class AddressesState extends MutableSource
    implements Cloneable<AddressesState> {
  List<AddressItemState> items;

  @override
  AddressesState clone() {
    return AddressesState()
      ..items = items
          .map(
            (e) => e.clone(),
          )
          .toList();
  }

  @override
  Object getItemData(int index) {
    List<AddressItemState> addresses =
        items?.where((element) => !element.isDelete)?.toList();
    return addresses[index];
  }

  @override
  String getItemType(int index) {
    return 'address-item';
  }

  @override
  int get itemCount =>
      items?.where((element) => !element.isDelete)?.length ?? 0;

  @override
  void setItemData(int index, Object data) {
    List<AddressItemState> addresses =
        items?.where((element) => !element.isDelete)?.toList();
    addresses[index] = data;
  }
}

AddressesState initState(Map<String, dynamic> args) {
  return AddressesState()..items = [];
}

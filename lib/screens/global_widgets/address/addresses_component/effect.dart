import 'package:flutter/material.dart' hide Action;

import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/route_name.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/utils/show_picker.dart';
import 'action.dart';
import 'address_item_component/state.dart';
import 'state.dart';
import '../../../../utils/utils.dart';

Effect<AddressesState> buildEffect() {
  return combineEffects(<Object, Effect<AddressesState>>{
    AddressesAction.delete: _delete,
    AddressesAction.createOrEdit: _createOrEdit,
  });
}

Future _createOrEdit(Action action, Context<AddressesState> ctx) async {
  final parameter = action.payload as AddressItemState;

  final result = await showModal2(ctx.context, RouteName.addressEditor, args: {
    'item': parameter,
  });

  if (result != null) {
    ctx.dispatch(
      AddressesActionCreator.handleCreateOrEdit(
        item: result,
      ),
    );
  }
}

Future _delete(Action action, Context<AddressesState> ctx) async {
  final item = action.payload as AddressItemState;
  if (item == null) return;

  final confirmed = await ctx.confirm(
    title: AppLocalizations.instance.text('common.warning.delete.title'),
    message: AppLocalizations.instance.text('common.warning.delete.desc'),
    positiveActionTitle: AppLocalizations.instance.text('common.sure'),
    negativeActionTitle: AppLocalizations.instance.text('common.cancel'),
  );
  if (!confirmed) return;

  ctx.dispatch(
    AddressesActionCreator.handleDelete(item: item),
  );
}

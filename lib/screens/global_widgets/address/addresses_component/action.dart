import 'package:fish_redux/fish_redux.dart';

import 'address_item_component/state.dart';

enum AddressesAction {
  createOrEdit,
  handleCreateOrEdit,
  delete,
  handleDelete,
}

class AddressesActionCreator {
  static Action createOrEdit({AddressItemState item}) {
    return Action(
      AddressesAction.createOrEdit,
      payload: item,
    );
  }

  static Action handleCreateOrEdit({AddressItemState item}) {
    return Action(
      AddressesAction.handleCreateOrEdit,
      payload: item,
    );
  }

  static Action delete({
    AddressItemState item,
  }) {
    return Action(
      AddressesAction.delete,
      payload: item,
    );
  }

  static Action handleDelete({AddressItemState item}) {
    return Action(
      AddressesAction.handleDelete,
      payload: item,
    );
  }
}

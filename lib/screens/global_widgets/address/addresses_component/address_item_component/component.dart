import 'package:fish_redux/fish_redux.dart';

import 'state.dart';
import 'view.dart';

class AddressItemComponent extends Component<AddressItemState> {
  AddressItemComponent()
      : super(
          view: buildView,
          dependencies: Dependencies<AddressItemState>(
            adapter: null,
            slots: <String, Dependent<AddressItemState>>{},
          ),
        );
}

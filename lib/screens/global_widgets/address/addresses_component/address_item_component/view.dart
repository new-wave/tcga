import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/config/style.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';

import '../action.dart';

import 'state.dart';

Widget buildView(
    AddressItemState state, Dispatch dispatch, ViewService viewService) {
  return GestureDetector(
    onTap: () {
      dispatch(
        AddressesActionCreator.createOrEdit(item: state),
      );
    },
    child: Container(
    margin: EdgeInsets.only(bottom: 10),
    padding: EdgeInsets.only(left: 16, top: 8, bottom: 8, right: 8),
    height: 85,
    decoration: BoxDecoration(
      color: colorBackground,
      borderRadius: BorderRadius.circular(10),
    ),
    child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                RichText(
                  text: TextSpan(
                    children: <TextSpan>[
                      TextSpan(
                          text: AppLocalizations.of(viewService.context)
                              .text('address.item.address'),
                          style: Style.labelNormal),
                      TextSpan(
                          text: '${state.address}', style: Style.labelNormal),
                    ],
                  ),
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
                RichText(
                  text: TextSpan(
                    children: <TextSpan>[
                      TextSpan(
                          text: AppLocalizations.of(viewService.context)
                              .text('address.item.city'),
                          style: Style.labelNormal),
                      TextSpan(text: '${state.city}', style: Style.labelNormal),
                    ],
                  ),
                ),
                RichText(
                  text: TextSpan(
                    children: <TextSpan>[
                      TextSpan(
                          text: AppLocalizations.of(viewService.context)
                              .text('address.item.state'),
                          style: Style.labelNormal),
                      TextSpan(text: '${state.state}', style: Style.labelNormal),
                    ],
                  ),
                ),
                RichText(
                  text: TextSpan(
                    children: <TextSpan>[
                      TextSpan(
                          text: AppLocalizations.of(viewService.context)
                              .text('address.item.zipcode'),
                          style: Style.labelNormal),
                      TextSpan(text: '${state.zip}', style: Style.labelNormal),
                    ],
                  ),
                ),

                // Text("Address: ${state.address}"),
                // Text("City: ${state.city}"),
                // Text("State/Province: ${state.state}"),
                // Text("Zip/Postal: ${state.zip}"),
              ],
            ),
          ),
          GestureDetector(
            onTap: () {
              dispatch(
                AddressesActionCreator.delete(item: state),
              );
            },
            child: Container(
              child: SvgPicture.asset(
                'assets/icons/ic_trash.svg',
                height: 24,
                width: 24,
              ),
            ),
          ),
        ],
      ),
    ),
  );
}

import 'package:fish_redux/fish_redux.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';

class AddressItemState implements Cloneable<AddressItemState> {
  String id;
  String title;
  String address;
  String city;
  String state;
  String zip;
  String country;
  LatLng latLng;
  List<AddressItemState> listFullAddress;
  bool isDelete;

  AddressItemState({
    this.id,
    this.title,
    this.address,
    this.city,
    this.state,
    this.zip,
    this.country,
    this.latLng,
    this.listFullAddress,
    this.isDelete = false,
  });

  String get initial {
    if (title == null || title.isEmpty) return '';
    final names = title?.split(' ');
    var initial = names[0][0] + title[title.lastIndexOf(" ") + 1];
    return initial.toUpperCase();
  }

  String get fullAddress {
    String fullAddress = '';
    if (address != null && address.isNotEmpty) {
      fullAddress = fullAddress + address + ",";
      if (address
          .contains(AppLocalizations.instance.text('mileage.routes.addresses'))) {
        return address;
      }
    }
    if (city != null && city.isNotEmpty) {
      fullAddress = fullAddress + city + ",";
    }
    if (country != null && country.isNotEmpty) {
      fullAddress = fullAddress + country + ",";
    }
    if (fullAddress.isEmpty) return null;
    fullAddress = fullAddress.substring(0, fullAddress.length - 1);
    return fullAddress;
  }

  @override
  AddressItemState clone() {
    return AddressItemState()
      ..id = id
      ..title = title
      ..address = address
      ..city = city
      ..state = state
      ..zip = zip
      ..country = country
      ..latLng = latLng
      ..listFullAddress = listFullAddress
      ..isDelete = isDelete;
  }
}

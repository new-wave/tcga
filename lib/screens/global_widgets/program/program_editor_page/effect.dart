import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart' hide Action, Page;
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/screens/global_widgets/program/program_component/program_item_component/state.dart';
import 'package:tcga_flutter/screens/global_widgets/program/program_editor_page/validator.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:uuid/uuid.dart';

import 'action.dart';
import 'state.dart';

final _uuid = Uuid();

Effect<ProgramEditorState> buildEffect() {
  return combineEffects(<Object, Effect<ProgramEditorState>>{
    ProgramEditorAction.validateAndSubmit: _validateAndSubmit,
  });
}

void _validateAndSubmit(Action action, Context<ProgramEditorState> ctx) {
  final state = ctx.state;

  if (!Validator.isValid(state)) {
    final snackBar = SnackBar(
      content: Text(
        AppLocalizations.of(ctx.context)
            .text('common.warning.please_fill_in_valid_details'),
      ),
      backgroundColor: colorIndicatorWarning,
    );
    ctx.state.scaffoldKey.currentState.showSnackBar(snackBar);
    return;
  }

  Navigator.of(ctx.context).pop(ProgramItemState(
    id: state.id ?? _uuid.v4(),
    program: state.programEditController.text.trim(),
  ));
}

import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/widget/button_widget.dart';
import 'package:tcga_flutter/widget/entry_widget.dart';

import 'action.dart';
import 'state.dart';

Widget buildView(
    ProgramEditorState state, Dispatch dispatch, ViewService viewService) {
  final node = FocusScope.of(viewService.context);
  return Container(
    padding: EdgeInsets.only(
      top: 13,
    ),
    height: 320,
    child: Scaffold(
      resizeToAvoidBottomInset: false,
      key: state.scaffoldKey,
      body: Container(
        color: Colors.white,
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: [
            Row(
              children: [
                Spacer(
                  flex: 5,
                ),
                Text(
                  AppLocalizations.of(viewService.context)
                      .text('program.header'),
                  style: textStyleThird,
                ),
                Spacer(
                  flex: 4,
                ),
                GestureDetector(
                    onTap: () async => Navigator.pop(viewService.context),
                    child: SvgPicture.asset("assets/icons/ic_close.svg")),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            EntryWidget(
              title: AppLocalizations.of(viewService.context)
                  .text('program.editor.placeholder_program'),
              keyboardType: TextInputType.name,
              controller: state.programEditController,
              onEditingComplete: () => node.unfocus(),
            ),
          ],
        ),
      ),
      bottomNavigationBar: Builder(
        builder: (context) => Container(
          decoration: BoxDecoration(
              color: colorPrimary, border: Border.all(color: colorPrimary)),
          child: ButtonWidget(
            colorButton: colorPrimary,
            textButton:
                AppLocalizations.of(viewService.context).text('common.save'),
            onPress: () {
              dispatch(ProgramEditorActionCreator.validateAndSubmit());
            },
          ),
        ),
      ),
    ),
  );
}

import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tcga_flutter/screens/global_widgets/program/program_component/program_item_component/state.dart';

class ProgramEditorState implements Cloneable<ProgramEditorState> {
  String id;
  TextEditingController programEditController;
  final scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  ProgramEditorState clone() {
    return ProgramEditorState()
      ..id = id
      ..programEditController = programEditController;
  }
}

ProgramEditorState initState(Map<String, dynamic> args) {
  final program = args['item'] as ProgramItemState;
  return ProgramEditorState()
    ..id = program?.id
    ..programEditController = TextEditingController(
      text: program?.program,
    );
}

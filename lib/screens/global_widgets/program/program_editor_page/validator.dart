import 'state.dart';

class Validator {
  static bool isValid(ProgramEditorState state) {
    if (_isNullEmptyOrWhiteSpace(
      state.programEditController.text,
    )) {
      return false;
    }

    return true;
  }

  static bool _isNullEmptyOrWhiteSpace(String text) {
    if (text == null) return true;

    return text.trim().isEmpty;
  }
}

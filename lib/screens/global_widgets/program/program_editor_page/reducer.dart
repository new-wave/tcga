import 'package:fish_redux/fish_redux.dart';

import 'action.dart';
import 'state.dart';

Reducer<ProgramEditorState> buildReducer() {
  return asReducer(
    <Object, Reducer<ProgramEditorState>>{
      //ProgramEditorAction.validateAndSubmit: _onAction,
    },
  );
}

// ProgramEditorState _onAction(ProgramEditorState state, Action action) {
//   final ProgramEditorState newState = state.clone();
//   return newState;
// }

import 'package:fish_redux/fish_redux.dart';

import 'effect.dart';
import 'reducer.dart';
import 'state.dart';
import 'view.dart';

class ProgramEditorPage extends Page<ProgramEditorState, Map<String, dynamic>> {
  ProgramEditorPage()
      : super(
          initState: initState,
          effect: buildEffect(),
          reducer: buildReducer(),
          view: buildView,
          dependencies: Dependencies<ProgramEditorState>(
              adapter: null, slots: <String, Dependent<ProgramEditorState>>{}),
          middleware: <Middleware<ProgramEditorState>>[],
        );
}

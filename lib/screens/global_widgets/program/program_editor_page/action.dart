import 'package:fish_redux/fish_redux.dart';

//TODO replace with your own action
enum ProgramEditorAction { validateAndSubmit }

class ProgramEditorActionCreator {
  static Action validateAndSubmit() {
    return const Action(
      ProgramEditorAction.validateAndSubmit,
    );
  }
}

import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/global_widgets/program/program_component/program_item_component/state.dart';

class ProgramState extends MutableSource implements Cloneable<ProgramState> {
  List<ProgramItemState> items;

  @override
  ProgramState clone() {
    return ProgramState()
      ..items = items
          .map(
            (e) => e.clone(),
          )
          .toList();
  }

  @override
  Object getItemData(int index) {
    List<ProgramItemState> programs =
        items?.where((element) => !element.isDelete)?.toList();
    return programs[index];
  }

  @override
  String getItemType(int index) {
    return 'program-item';
  }

  @override
  // TODO: implement itemCount
  int get itemCount =>
      items?.where((element) => !element.isDelete)?.length ?? 0;

  @override
  void setItemData(int index, Object data) {
    List<ProgramItemState> programs =
        items?.where((element) => !element.isDelete)?.toList();
    programs[index] = data;
  }
}

ProgramState initState(Map<String, dynamic> args) {
  return ProgramState()..items = [];
}

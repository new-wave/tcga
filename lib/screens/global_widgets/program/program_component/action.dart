import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/global_widgets/program/program_component/program_item_component/state.dart';

enum ProgramAction {
  createOrEdit,
  handleCreateOrEdit,
  delete,
  handleDelete,
}

class ProgramActionCreator {
  static Action createOrEdit({ProgramItemState item}) {
    return Action(
      ProgramAction.createOrEdit,
      payload: item,
    );
  }

  static Action handleCreateOrEdit({ProgramItemState item}) {
    return Action(
      ProgramAction.handleCreateOrEdit,
      payload: item,
    );
  }

  static Action delete({
    ProgramItemState item,
  }) {
    return Action(
      ProgramAction.delete,
      payload: item,
    );
  }

  static Action handleDelete({ProgramItemState item}) {
    return Action(
      ProgramAction.handleDelete,
      payload: item,
    );
  }
}

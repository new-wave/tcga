import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/global_widgets/program/program_component/program_item_component/state.dart';

import 'action.dart';
import 'state.dart';

Reducer<ProgramState> buildReducer() {
  return asReducer(
    <Object, Reducer<ProgramState>>{
      ProgramAction.handleCreateOrEdit: _handleCreateOrEdit,
      ProgramAction.handleDelete: _handleDelete,
    },
  );
}

ProgramState _handleCreateOrEdit(ProgramState state, Action action) {
  final ProgramState newState = state.clone();
  final item = action.payload as ProgramItemState;

  final foundIndex =
      newState.items.indexWhere((element) => element.id == item.id);

  if (foundIndex > -1) {
    newState.items[foundIndex] = item;
  } else {
    newState.items.add(item);
  }

  return newState;
}

ProgramState _handleDelete(ProgramState state, Action action) {
  final ProgramState newState = state.clone();
  final item = action.payload as ProgramItemState;
  item.isDelete = true;
  final foundIndex =
      newState.items.indexWhere((element) => element.id == item.id);
  if (foundIndex > -1) {
    newState.items[foundIndex] = item;
  }
  return newState;
}

import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/global_widgets/program/program_component/program_adapter/adapter.dart';

import 'effect.dart';
import 'reducer.dart';
import 'state.dart';
import 'view.dart';

class ProgramsComponent extends Component<ProgramState> {
  ProgramsComponent()
      : super(
          effect: buildEffect(),
          reducer: buildReducer(),
          view: buildView,
          dependencies: Dependencies<ProgramState>(
              adapter: NoneConn<ProgramState>() + ProgramAdapter(),
              slots: <String, Dependent<ProgramState>>{}),
        );
}

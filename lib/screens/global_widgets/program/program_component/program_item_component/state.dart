import 'package:fish_redux/fish_redux.dart';
import 'package:uuid/uuid.dart';

class ProgramItemState implements Cloneable<ProgramItemState> {
  String id;
  String program;
  bool isDelete;

  ProgramItemState({
    id,
    this.program,
    this.isDelete = false,
  }) {
    this.id = id ?? Uuid().v4();
  }

  @override
  ProgramItemState clone() {
    return ProgramItemState()
      ..id = id
      ..program = program
      ..isDelete = isDelete;
  }
}

ProgramItemState initState(Map<String, dynamic> args) {
  return ProgramItemState();
}

import 'package:fish_redux/fish_redux.dart';

import 'state.dart';
import 'view.dart';

class ProgramItemComponent extends Component<ProgramItemState> {
  ProgramItemComponent()
      : super(
          view: buildView,
          dependencies: Dependencies<ProgramItemState>(
            adapter: null,
            slots: <String, Dependent<ProgramItemState>>{},
          ),
        );
}

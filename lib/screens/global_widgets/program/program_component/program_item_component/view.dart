import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:tcga_flutter/config/palette.dart';

import '../action.dart';
import 'state.dart';

Widget buildView(
    ProgramItemState state, Dispatch dispatch, ViewService viewService) {
  return GestureDetector(
    onTap: () {
      dispatch(
        ProgramActionCreator.createOrEdit(item: state),
      );
    },
    child: Container(
      margin: EdgeInsets.only(bottom: 10),
      padding: EdgeInsets.only(left: 16, top: 8, bottom: 8, right: 8),
      height: 36,
      decoration: BoxDecoration(
        color: colorBackground,
        borderRadius: BorderRadius.circular(5),
      ),
      child: GestureDetector(
        onTap: () {
          dispatch(
            ProgramActionCreator.delete(item: state),
          );
        },
        child: Row(
          children: [
            Text(state.program),
            Spacer(),
            GestureDetector(
              onTap: () {
                dispatch(
                  ProgramActionCreator.delete(item: state),
                );
              },
              child: Container(
                child: SvgPicture.asset(
                  'assets/icons/ic_trash.svg',
                  height: 24,
                  width: 24,
                ),
              ),
            ),
          ],
        ),
      ),
    ),
  );
}

import 'package:fish_redux/fish_redux.dart';
import '../program_item_component/component.dart';
import '../state.dart';

class ProgramAdapter extends SourceFlowAdapter<ProgramState> {
  ProgramAdapter(): super(
    pool: <String, Component<Object>>{
      'program-item': ProgramItemComponent(),
    }
  );
}
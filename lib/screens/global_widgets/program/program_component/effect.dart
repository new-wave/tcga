import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/global_widgets/program/program_component/program_item_component/state.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/utils/show_picker.dart';
import '../../../../utils/utils.dart';
import '../../../../route_name.dart';
import 'action.dart';
import 'state.dart';

Effect<ProgramState> buildEffect() {
  return combineEffects(<Object, Effect<ProgramState>>{
    ProgramAction.createOrEdit: _createOrEdit,
    ProgramAction.delete: _delete,
  });
}

void _createOrEdit(Action action, Context<ProgramState> ctx) async {
  final parameter = action.payload as ProgramItemState;

  final result = await showModal2(ctx.context, RouteName.programEditor,
      args: {'item': parameter});

  if (result != null) {
    ctx.dispatch(ProgramActionCreator.handleCreateOrEdit(
      item: result,
    ));
  }
}

Future _delete(Action action, Context<ProgramState> ctx) async {
  final item = action.payload as ProgramItemState;

  if (item == null) return;

  final confirmed = await ctx.confirm(
    title: AppLocalizations.instance.text('common.warning.delete.title'),
    message: AppLocalizations.instance.text('common.warning.delete.desc'),
    positiveActionTitle: AppLocalizations.instance.text('common.sure'),
    negativeActionTitle: AppLocalizations.instance.text('common.cancel'),
  );
  if (!confirmed) return;

  ctx.dispatch(
    ProgramActionCreator.handleDelete(item: item),
  );
}
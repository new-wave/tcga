import 'state.dart';

class Validator {
  static bool isValid(TeamEditorState state) {
    if (state.nameEditingController.text == null ||
        state.nameEditingController.text.trim().isEmpty) return false;

    if (state.teamType == null) return false;

    if (state.members.length == 0) return false;

    return true;
  }
}

import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/teams/team_editor_page/team_member_item_component/state.dart';

import 'action.dart';
import 'state.dart';

Reducer<TeamEditorState> buildReducer() {
  return asReducer(
    <Object, Reducer<TeamEditorState>>{
      TeamEditorAction.initialize: _initialize,
      TeamEditorAction.handleTeamTypeSelected: _handleTeamTypeSelected,
      TeamEditorAction.handleMemberAdded: _handleMemberAdded,
      TeamEditorAction.handleTitleChanged: _handleTitleChanged,
      TeamEditorAction.handleTeamMemberRemoved: _handleTeamMemberRemoved,
      TeamEditorAction.handleTeamMemberAactivated: _handleTeamMemberActivated,
      TeamEditorAction.handleTeamMemberDeactivated:
          _handleTeamMemberDeactivated,
    },
  );
}

TeamEditorState _initialize(TeamEditorState state, Action action) {
  return action.payload.clone();
}

TeamEditorState _handleTitleChanged(TeamEditorState state, Action action) {
  final TeamEditorState newState = state.clone();
  return newState;
}

TeamEditorState _handleTeamTypeSelected(TeamEditorState state, Action action) {
  final TeamEditorState newState = state.clone();
  newState.teamType = TeamEditorState.teamTypes.keys.elementAt(
    action.payload,
  );
  return newState;
}

TeamEditorState _handleMemberAdded(TeamEditorState state, Action action) {
  final TeamEditorState newState = state.clone();
  newState.members = [...action.payload];
  return newState;
}

TeamEditorState _handleTeamMemberRemoved(TeamEditorState state, Action action) {
  final item = action.payload as TeamMemberItemState;
  final index = state.members.indexWhere(
    (x) => x.id == item.id,
  );

  if (index == -1) return state;

  final TeamEditorState newState = state.clone();
  newState.members.removeAt(index);
  return newState;
}

TeamEditorState _handleTeamMemberDeactivated(
    TeamEditorState state, Action action) {
  final item = action.payload as TeamMemberItemState;
  final index = state.members.indexWhere(
    (x) => x.id == item.id,
  );

  if (index == -1) return state;

  final TeamEditorState newState = state.clone();
  newState.members[index] = item.clone()..isActive = false;
  return newState;
}

TeamEditorState _handleTeamMemberActivated(
  TeamEditorState state,
  Action action,
) {
  final item = action.payload as TeamMemberItemState;
  final index = state.members.indexWhere(
    (x) => x.id == item.id,
  );

  if (index == -1) return state;

  final TeamEditorState newState = state.clone();
  newState.members[index] = item.clone()..isActive = true;
  return newState;
}

import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:tcga_flutter/database/models/enums.dart';
import 'package:tcga_flutter/screens/teams/team_editor_page/team_member_item_component/state.dart';
import 'package:tcga_flutter/screens/teams/teams_page/team_item_component/state.dart';

class TeamEditorState extends MutableSource
    implements Cloneable<TeamEditorState> {
  bool isOwner;
  String id;

  String ownerId;
  TeamOwnerType ownerType;

  static final teamTypes = {
    // TeamType.Contacts: 'Contacts',
    TeamType.EmergencyContacts: 'Emergency Contacts',
    TeamType.Family: 'Family',
    TeamType.CareTeam: 'CareTeam',
    TeamType.Other: 'Other',
  };

  bool get onEditMode => id != null;
  //String get title => onEditMode ? nameEditingController.text : 'New Team';

  TextEditingController nameEditingController;
  TeamType teamType;

  List<TeamMemberItemState> members;

  @override
  TeamEditorState clone() {
    return TeamEditorState()
      ..id = id
      ..isOwner = isOwner
      ..ownerId = ownerId
      ..ownerType = ownerType
      ..nameEditingController = nameEditingController
      ..teamType = teamType
      ..members = members;
  }

  @override
  Object getItemData(int index) {
    return members[index]..onEditMode = onEditMode;
  }

  @override
  String getItemType(int index) => 'team-member-item';

  @override
  int get itemCount => members?.length ?? 0;

  @override
  void setItemData(int index, Object data) {
    members[index] = data;
  }
}

TeamEditorState initState(Map<String, dynamic> args) {
  final item = args['item'] as TeamItemState;
  return TeamEditorState()
    ..id = item?.id
    ..isOwner = item?.isOwner ?? true
    ..ownerType = TeamOwnerType.Caregiver
    ..teamType = item?.teamType
    ..nameEditingController = TextEditingController(
      text: item?.name ?? '',
    )
    ..members = [];
}

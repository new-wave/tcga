import 'package:fish_redux/fish_redux.dart';

import '../team_member_item_component/component.dart';

import '../state.dart';

class TeamMembersAdapter extends SourceFlowAdapter<TeamEditorState> {
  TeamMembersAdapter()
      : super(
          pool: <String, Component<Object>>{
            'team-member-item': TeamMemberItemComponent(),
          },
        );
}

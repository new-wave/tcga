import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/teams/team_editor_page/state.dart';
import 'package:tcga_flutter/screens/teams/team_editor_page/team_member_item_component/state.dart';

enum TeamEditorAction {
  initialize,
  save,
  delete,
  addMember,
  handleMemberAdded,
  handleTeamTypeSelected,
  handleTitleChanged,
  transferOwnership,
  removeTeamMember,
  handleTeamMemberRemoved,
  activateTeamMember,
  handleTeamMemberAactivated,
  deactivateTeamMember,
  handleTeamMemberDeactivated,
}

class TeamEditorActionCreator {
  static Action initialize(TeamEditorState state) {
    return Action(
      TeamEditorAction.initialize,
      payload: state,
    );
  }

  static Action handleTitleChanged(String text) {
    return Action(
      TeamEditorAction.handleTitleChanged,
      payload: text,
    );
  }

  static Action delete() {
    return const Action(TeamEditorAction.delete);
  }

  static Action save() {
    return const Action(TeamEditorAction.save);
  }

  static Action addMember() {
    return const Action(TeamEditorAction.addMember);
  }

  static Action handleMemberAdded(List<TeamMemberItemState> result) {
    return Action(
      TeamEditorAction.handleMemberAdded,
      payload: result,
    );
  }

  static Action handleTeamTypeSelected(int index) {
    return Action(
      TeamEditorAction.handleTeamTypeSelected,
      payload: index,
    );
  }

  static Action handleTeamMemberRemoved(TeamMemberItemState state) {
    return Action(
      TeamEditorAction.handleTeamMemberRemoved,
      payload: state,
    );
  }

  static Action transferOwnership(TeamMemberItemState state) {
    return Action(
      TeamEditorAction.transferOwnership,
      payload: state,
    );
  }

  static Action handleTeamMemberDeactivated(TeamMemberItemState state) {
    return Action(
      TeamEditorAction.handleTeamMemberDeactivated,
      payload: state,
    );
  }

  static Action activateTeamMember(TeamMemberItemState state) {
    return Action(
      TeamEditorAction.activateTeamMember,
      payload: state,
    );
  }

  static Action deactivateTeamMember(TeamMemberItemState state) {
    return Action(
      TeamEditorAction.deactivateTeamMember,
      payload: state,
    );
  }

  static Action handleTeamMemberActivated(TeamMemberItemState state) {
    return Action(
      TeamEditorAction.handleTeamMemberAactivated,
      payload: state,
    );
  }

  static Action removeTeamMember(TeamMemberItemState state) {
    return Action(
      TeamEditorAction.removeTeamMember,
      payload: state,
    );
  }
}

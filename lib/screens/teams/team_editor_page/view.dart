import 'dart:developer';
import 'dart:io';
import 'dart:math' as math;

import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/widget/import.dart';

import 'action.dart';
import 'state.dart';
import 'validator.dart';

Widget buildView(
  TeamEditorState state,
  Dispatch dispatch,
  ViewService viewService,
) {
  final adapter = viewService.buildAdapter();
  return Builder(builder: (context) {
    return Padding(
      padding: EdgeInsets.only(
        bottom: MediaQuery.of(context).viewInsets.bottom,
      ),
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.white,
        appBar: _appBar(viewService, state, dispatch),
        bottomNavigationBar: state.isOwner
            ? _bottomNavigationBar(viewService, state, dispatch)
            : null,
        body: CustomScrollView(
          slivers: [
            _verticalSpacer(),
            _teamEditorHeader(viewService, state, dispatch),
            _verticalSpacer(),
            if (state.isOwner) _addMemberButton(viewService, dispatch),
            if (state.isOwner) _verticalSpacer(),
            SliverList(
              delegate: SliverChildBuilderDelegate(
                (context, index) {
                  final int itemIndex = index ~/ 2;
                  if (index.isEven) {
                    return adapter.itemBuilder(
                      context,
                      itemIndex,
                    );
                  }
                  return SizedBox(height: 8);
                },
                // childCount: adapter.itemCount,
                semanticIndexCallback: (Widget widget, int localIndex) {
                  if (localIndex.isEven) {
                    return localIndex ~/ 2;
                  }
                  return null;
                },
                childCount: math.max(0, adapter.itemCount * 2 - 1),
              ),
            ),
            _verticalSpacer(),
          ],
        ),
      ),
    );
  });
}

SliverToBoxAdapter _teamEditorHeader(
    ViewService viewService, TeamEditorState state, Dispatch dispatch) {
  return SliverToBoxAdapter(
    child: Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        children: [
          EntryWidget(
            readOnly: !state.isOwner,
            controller: state.nameEditingController,
            title: AppLocalizations.of(viewService.context)
                .text('teams.editor.placeholder_team_name'),
            keyboardType: TextInputType.text,
            onTextChanged: (text) => dispatch(
              TeamEditorActionCreator.handleTitleChanged(
                text,
              ),
            ),
          ),
          SizedBox(
            height: 16,
          ),
          PickerWidget(
            readOnly: !state.isOwner,
            items: TeamEditorState.teamTypes.values.toList(),
            initialIndex: state.teamType != null
                ? TeamEditorState.teamTypes.keys
                    .toList()
                    .indexOf(state.teamType)
                : null,
            title: AppLocalizations.of(viewService.context)
                .text('teams.editor.placeholder_team_type'),
            indexSelected: (index) => dispatch(
              TeamEditorActionCreator.handleTeamTypeSelected(
                index,
              ),
            ),
          ),
        ],
      ),
    ),
  );
}

SliverToBoxAdapter _verticalSpacer() {
  return SliverToBoxAdapter(
    child: SizedBox(
      height: 16,
    ),
  );
}

SliverToBoxAdapter _addMemberButton(
    ViewService viewService, Dispatch dispatch) {
  return SliverToBoxAdapter(
    child: InkWell(
      onTap: () => dispatch(
        TeamEditorActionCreator.addMember(),
      ),
      child: SizedBox(
        height: 36,
        child: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 16,
          ),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SvgPicture.asset(
                'assets/icons/ic_button_add.svg',
              ),
              SizedBox(
                width: 16,
              ),
              Text(
                AppLocalizations.of(viewService.context)
                    .text('teams.editor.add_new_member'),
                style: textStylePrimary,
              ),
            ],
          ),
        ),
      ),
    ),
  );
}

Builder _bottomNavigationBar(
    ViewService viewService, TeamEditorState state, Dispatch dispatch) {
  double height = ((Platform.isIOS)
          ? (MediaQuery.of(viewService.context).padding.bottom / 2)
          : 0) +
      45.0;
  return Builder(
    builder: (context) => Container(
      height: height,
      child: Container(
        decoration: BoxDecoration(
            color: colorPrimary, border: Border.all(color: colorPrimary)),
        child: ButtonWidget(
          colorButton: colorPrimary,
          textButton:
              AppLocalizations.of(viewService.context).text('common.save'),
          onPress: () {
            if (!Validator.isValid(state)) {
              final snackbar = SnackBar(
                content: Container(
                  child: Text(
                    AppLocalizations.of(viewService.context)
                        .text('common.warning.please_fill_in_valid_details'),
                  ),
                ),
                backgroundColor: colorIndicatorWarning,
              );

              Scaffold.of(context).showSnackBar(snackbar);

              return;
            }
            dispatch(
              TeamEditorActionCreator.save(),
            );
          },
        ),
      ),
    ),
  );
}

AppBar _appBar(
    ViewService viewService, TeamEditorState state, Dispatch dispatch) {
  return AppBar(
    iconTheme: IconThemeData(color: Colors.black),
    elevation: 3,
    backgroundColor: Color(0xFFFFFFFF),
    leading: IconButton(
        icon: SvgPicture.asset(
          "assets/icons/ic_back.svg",
          width: 24,
          height: 24,
        ),
        onPressed: () {
          Navigator.pop(viewService.context);
        }),
    centerTitle: false,
    title: Text(
      state.onEditMode
          ? state.nameEditingController.text
          : AppLocalizations.of(viewService.context).text('teams.editor.title'),
      style: TextStyle(
          color: Color(0xff4F4F4F), fontSize: 20, fontWeight: FontWeight.w400),
    ),
    actions: [
      if (state.isOwner && state.onEditMode)
        IconButton(
          icon: SvgPicture.asset(
            "assets/icons/ic_trash.svg",
            width: 24,
            height: 24,
          ),
          onPressed: () => dispatch(
            TeamEditorActionCreator.delete(),
          ),
        ),
    ],
  );
}

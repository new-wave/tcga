import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/route_name.dart';
import 'package:tcga_flutter/screens/teams/team_editor_page/team_member_item_component/state.dart';
import 'package:tcga_flutter/utils/utils.dart';
import 'package:tcga_flutter/screens/teams/manager.dart';

import 'action.dart';
import 'state.dart';

final _manager = TeamManager();

Effect<TeamEditorState> buildEffect() {
  return combineEffects(<Object, Effect<TeamEditorState>>{
    Lifecycle.initState: _init,
    TeamEditorAction.addMember: _addMember,
    TeamEditorAction.save: _save,
    TeamEditorAction.delete: _delete,
    TeamEditorAction.transferOwnership: _transferOwnership,
    TeamEditorAction.removeTeamMember: _removeTeamMember,
    TeamEditorAction.deactivateTeamMember: _deactivateTeamMember,
    TeamEditorAction.activateTeamMember: _activateTeamMember,
  });
}

Future _init(
  Action action,
  Context<TeamEditorState> ctx,
) async {
  final state = ctx.state;

  if (state.onEditMode) {
    final newState = await _manager.fill(state);

    ctx.dispatch(
      TeamEditorActionCreator.initialize(
        newState,
      ),
    );
  }
}

Future _save(
  Action action,
  Context<TeamEditorState> ctx,
) async {
  final result = ctx.state.onEditMode
      ? await _manager.update(ctx.state)
      : await _manager.create(ctx.state);

  if (result == null) return;

  ctx.goBack(result);
}

Future _addMember(
  Action action,
  Context<TeamEditorState> ctx,
) async {
  final state = ctx.state;

  final result = await ctx.navigateTo(
    RouteName.teamMemberPicker,
    arguments: {
      'members': [...state.members],
    },
  );

  if (result is List<TeamMemberItemState>) {
    ctx.dispatch(
      TeamEditorActionCreator.handleMemberAdded(
        result,
      ),
    );
  }
}

Future _delete(
  Action action,
  Context<TeamEditorState> ctx,
) async {
  final state = ctx.state;

  if (!state.onEditMode) {
    return;
  }

  var confirmed = await ctx.confirm(
      title: 'Deletion warning!',
      message: 'Are you sure to delete this item?',
      negativeActionTitle: 'Cancel',
      positiveActionTitle: 'Yes, I am sure');

  if (!confirmed) return;

  final result = await _manager.delete(state.id);

  if (!result) return; // TODO show error message

  ctx.goBack(
    {
      'deleted-item-id': state.id,
    },
  );
}

Future _transferOwnership(
  Action action,
  Context<TeamEditorState> ctx,
) async {
  final state = ctx.state;

  if (!state.onEditMode) {
    return;
  }

  final item = action.payload as TeamMemberItemState;

  var confirmed = await ctx.confirm(
    title: 'Transfer ownership warning!',
    message: 'Are you sure to transfer the ownership to this member?',
    negativeActionTitle: 'Cancel',
    positiveActionTitle: 'Yes, I am sure',
  );

  if (!confirmed) return;

  final index = state.members.indexWhere(
    (x) => x.id == item.id,
  );

  if (index == -1) return;

  final TeamEditorState newState = state.clone();
  newState.ownerId = item.id;
  newState.isOwner = false;

  final result = await _manager.update(newState);

  if (result == null) return;

  ctx.goBack(result);
}

Future _removeTeamMember(
  Action action,
  Context<TeamEditorState> ctx,
) async {
  var confirmed = await ctx.confirm(
      title: 'Team member removal warning!',
      message: 'Are you sure to remove this member?',
      negativeActionTitle: 'Cancel',
      positiveActionTitle: 'Yes, I am sure');

  if (!confirmed) return;

  final item = action.payload as TeamMemberItemState;

  ctx.dispatch(
    TeamEditorActionCreator.handleTeamMemberRemoved(item),
  );
}

Future _activateTeamMember(
  Action action,
  Context<TeamEditorState> ctx,
) async {
  var confirmed = await ctx.confirm(
      title: 'Team member activation warning!',
      message: 'Are you sure to activate this member?',
      negativeActionTitle: 'Cancel',
      positiveActionTitle: 'Yes, I am sure');

  if (!confirmed) return;

  final item = action.payload as TeamMemberItemState;

  ctx.dispatch(
    TeamEditorActionCreator.handleTeamMemberActivated(item),
  );
}

Future _deactivateTeamMember(
  Action action,
  Context<TeamEditorState> ctx,
) async {
  var confirmed = await ctx.confirm(
      title: 'Team member deactivation warning!',
      message: 'Are you sure to deactivate this member?',
      negativeActionTitle: 'Cancel',
      positiveActionTitle: 'Yes, I am sure');

  if (!confirmed) return;

  final item = action.payload as TeamMemberItemState;

  ctx.dispatch(
    TeamEditorActionCreator.handleTeamMemberDeactivated(item),
  );
}

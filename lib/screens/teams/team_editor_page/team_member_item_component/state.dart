import 'package:tcga_flutter/database/models/enums.dart';
import 'package:tcga_flutter/screens/contacts/state.dart';

class TeamMemberItemState extends ContactItemState {
  bool isOwner;
  bool isReadonly;
  bool isActive = true;
  bool onEditMode;

  TeamMemberItemState({
    String id,
    String firstName,
    String lastName,
    DateTime dateOfBirth,
    AdministrativeGenderType genderType,
    TeamRole teamRole,
    this.isOwner = false,
    this.isReadonly = false,
    this.isActive = true,
    this.onEditMode = false,
  }) : super(
          id: id,
          firstName: firstName,
          lastName: lastName,
          dateOfBirth: dateOfBirth,
          genderType: genderType,
          teamRole: teamRole,
        );

  @override
  TeamMemberItemState clone() {
    return TeamMemberItemState(
      id: id,
      firstName: firstName,
      lastName: lastName,
      dateOfBirth: dateOfBirth,
      genderType: genderType,
      teamRole: teamRole,
      isReadonly: isReadonly,
      isActive: isActive,
      onEditMode: onEditMode,
    );
  }
}

TeamMemberItemState initState(Map<String, dynamic> args) {
  return TeamMemberItemState();
}

import 'package:fish_redux/fish_redux.dart';

import 'effect.dart';
import 'reducer.dart';
import 'state.dart';
import 'view.dart';

class TeamMemberItemComponent extends Component<TeamMemberItemState> {
  TeamMemberItemComponent()
      : super(
            effect: buildEffect(),
            reducer: buildReducer(),
            view: buildView,
            dependencies: Dependencies<TeamMemberItemState>(
                adapter: null,
                slots: <String, Dependent<TeamMemberItemState>>{
                }),);

}

import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart' hide Action, Page;
import 'package:flutter_svg/svg.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/widget/import.dart';
import 'package:tcga_flutter/widget/list_item_widget.dart';

import '../action.dart';
import 'state.dart';

Widget buildView(
  TeamMemberItemState state,
  Dispatch dispatch,
  ViewService viewService,
) {
  return SizedBox(
    height: 64,
    child: Stack(
      children: [
        ListItemWidget(
          contextActions: [
            'teams.editor.item.alert.remove_from_team',
            if (state.onEditMode) 'teams.editor.item.alert.set_as_the_owner',
            if (state.onEditMode)
              state.isActive
                  ? 'teams.editor.item.alert.deactivate'
                  : 'teams.editor.item.alert.activate',
          ],
          contextActionsVisible: !state.isReadonly,
          onContextActionSelected: (String action) {
            switch (action) {
              case ('Remove from team'):
                {
                  dispatch(
                    TeamEditorActionCreator.removeTeamMember(
                      state,
                    ),
                  );
                }
                break;
              case 'Set as the owner':
                {
                  dispatch(
                    TeamEditorActionCreator.transferOwnership(
                      state,
                    ),
                  );
                }
                break;
              case 'Deactivate':
                {
                  dispatch(
                    TeamEditorActionCreator.deactivateTeamMember(
                      state,
                    ),
                  );
                }
                break;
              case 'Activate':
                {
                  dispatch(
                    TeamEditorActionCreator.activateTeamMember(
                      state,
                    ),
                  );
                }
                break;
            }
          },
          child: _itemContent(viewService, state),
        ),
        Positioned(
          right: 0,
          bottom: 0,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Switch(
                onChanged: (value) {},
                value: state.isActive,
              ),
              Opacity(
                opacity: state.isOwner ? 1 : 0,
                child: Container(
                  padding: const EdgeInsets.only(
                    right: 8,
                    bottom: 8,
                  ),
                  child: SizedBox(
                    child: SvgPicture.asset(
                      'assets/icons/ic_owner.svg',
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    ),
  );
}

Container _itemContent(ViewService viewService, TeamMemberItemState state) {
  return Container(
    child: Row(
      children: [
        SizedBox(
          width: 16,
        ),
        SizedBox(
          height: 40,
          width: 40,
          child: Stack(
            children: [
              Container(
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(
                    20,
                  ),
                  color: colorPrimary,
                ),
                child: Text(
                  '${state.initial}',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 14,
                  ),
                ),
              ),
            ],
          ),
        ),
        SizedBox(
          width: 16,
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              state.fullName,
              style: TextStyle(
                color: colorTextPrimary,
                fontSize: 14,
              ),
            ),
            Row(
              children: [
                Text(
                  state.gender,
                  style: TextStyle(
                    color: colorTextSecondary,
                    fontSize: 12,
                  ),
                ),
                Text(
                  ' | ',
                  style: TextStyle(
                    color: colorTextSecondary,
                    fontSize: 12,
                  ),
                ),
                Text(
                  AppLocalizations.of(viewService.context)
                      .text('teams.editor.item.age'),
                  style: TextStyle(
                    color: colorTextSecondary,
                    fontSize: 12,
                  ),
                ),
                Text(
                  ': ${state.age}',
                  style: TextStyle(
                    color: colorTextSecondary,
                    fontSize: 12,
                  ),
                ),
              ],
            ),
            Text(
              state.teamRoleDisplay,
              style: TextStyle(
                color: colorTextSecondary,
                fontSize: 12,
              ),
            ),
          ],
        ),
      ],
    ),
  );
}

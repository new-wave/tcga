import 'package:fish_redux/fish_redux.dart';

import 'effect.dart';
import 'reducer.dart';
import 'state.dart';
import 'team_members_adapter/adapter.dart';
import 'view.dart';

class TeamEditorPage extends Page<TeamEditorState, Map<String, dynamic>> {
  TeamEditorPage()
      : super(
          initState: initState,
          effect: buildEffect(),
          reducer: buildReducer(),
          view: buildView,
          dependencies: Dependencies<TeamEditorState>(
            adapter: NoneConn<TeamEditorState>() + TeamMembersAdapter(),
            slots: <String, Dependent<TeamEditorState>>{},
          ),
          middleware: <Middleware<TeamEditorState>>[],
        );
}

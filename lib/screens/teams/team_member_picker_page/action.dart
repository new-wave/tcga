import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/contacts/state.dart';

enum TeamMemberPickerAction {
  initialize,
  handleRoleSelected,
  search,
  removeMember,
  confirm,
  addContact,
  handleContactAdded,
}

class TeamMemberPickerActionCreator {
  static Action initialize(List<ContactItemState> contacts) {
    return Action(
      TeamMemberPickerAction.initialize,
      payload: contacts,
    );
  }

  static Action handleRoleSelected(ContactItemState item) {
    return Action(
      TeamMemberPickerAction.handleRoleSelected,
      payload: item,
    );
  }

  static Action addContact() {
    return const Action(
      TeamMemberPickerAction.addContact,
    );
  }

  static Action search(String keyword) {
    return Action(
      TeamMemberPickerAction.search,
      payload: keyword,
    );
  }

  static Action removeMember(ContactItemState state) {
    return Action(
      TeamMemberPickerAction.removeMember,
      payload: state,
    );
  }

  static Action confirm() {
    return const Action(
      TeamMemberPickerAction.confirm,
    );
  }

  static Action handleContactAdded(ContactItemState result) {
    return Action(
      TeamMemberPickerAction.handleContactAdded,
      payload: result,
    );
  }
}

import 'package:fish_redux/fish_redux.dart';

import 'available_members_component/component.dart';
import 'effect.dart';
import 'reducer.dart';
import 'selected_members_component/component.dart';
import 'state.dart';
import 'view.dart';

class TeamMemberPickerPage
    extends Page<TeamMemberPickerState, Map<String, dynamic>> {
  TeamMemberPickerPage()
      : super(
          initState: initState,
          effect: buildEffect(),
          reducer: buildReducer(),
          view: buildView,
          dependencies: Dependencies<TeamMemberPickerState>(
              adapter: null,
              slots: <String, Dependent<TeamMemberPickerState>>{
                'members-component':
                    MembersStateConnector() + SelectedMembersComponent(),
                'contacts-component':
                    ContactsStateConnector() + AvailableMembersComponent(),
              }),
          middleware: <Middleware<TeamMemberPickerState>>[],
        );
}

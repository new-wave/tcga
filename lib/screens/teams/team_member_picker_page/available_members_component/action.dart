import 'package:fish_redux/fish_redux.dart';

//TODO replace with your own action
enum AvailableMembersAction { action }

class AvailableMembersActionCreator {
  static Action onAction() {
    return const Action(AvailableMembersAction.action);
  }
}

import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/contacts/state.dart';

class AvailableMembersState extends MutableSource
    implements Cloneable<AvailableMembersState> {
  List<ContactItemState> items;

  @override
  AvailableMembersState clone() {
    return AvailableMembersState()
      ..items = items
          .map(
            (e) => e.clone(),
          )
          .toList();
  }

  @override
  Object getItemData(int index) => items[index];

  @override
  String getItemType(int index) => 'contact-item';

  @override
  int get itemCount => items?.length ?? 0;

  @override
  void setItemData(int index, Object data) => items[index] = data;
}

AvailableMembersState initState(Map<String, dynamic> args) {
  return AvailableMembersState();
}

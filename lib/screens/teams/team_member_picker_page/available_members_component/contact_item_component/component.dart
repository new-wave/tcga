import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/contacts/state.dart';

import 'view.dart';

class ContactItemComponent extends Component<ContactItemState> {
  ContactItemComponent()
      : super(
          view: buildView,
        );
}

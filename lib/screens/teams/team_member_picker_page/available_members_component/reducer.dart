import 'package:fish_redux/fish_redux.dart';

import 'action.dart';
import 'state.dart';

Reducer<AvailableMembersState> buildReducer() {
  return asReducer(
    <Object, Reducer<AvailableMembersState>>{
      AvailableMembersAction.action: _onAction,
    },
  );
}

AvailableMembersState _onAction(AvailableMembersState state, Action action) {
  final AvailableMembersState newState = state.clone();
  return newState;
}

import 'package:fish_redux/fish_redux.dart';

import 'contact_adapter/adapter.dart';
import 'effect.dart';
import 'reducer.dart';
import 'state.dart';
import 'view.dart';

class AvailableMembersComponent extends Component<AvailableMembersState> {
  AvailableMembersComponent()
      : super(
          effect: buildEffect(),
          reducer: buildReducer(),
          view: buildView,
          dependencies: Dependencies<AvailableMembersState>(
              adapter: NoneConn<AvailableMembersState>() + ContactsAdapter(),
              slots: <String, Dependent<AvailableMembersState>>{}),
        );
}

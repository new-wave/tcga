import 'package:fish_redux/fish_redux.dart';

import '../contact_item_component/component.dart';
import '../state.dart';

class ContactsAdapter extends SourceFlowAdapter<AvailableMembersState> {
  ContactsAdapter()
      : super(
          pool: <String, Component<Object>>{
            'contact-item': ContactItemComponent(),
          },
        );
}

import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/contacts/state.dart';
import 'package:tcga_flutter/screens/teams/team_editor_page/team_member_item_component/state.dart';

import 'action.dart';
import 'state.dart';

Reducer<TeamMemberPickerState> buildReducer() {
  return asReducer(
    <Object, Reducer<TeamMemberPickerState>>{
      TeamMemberPickerAction.initialize: _initialize,
      TeamMemberPickerAction.handleRoleSelected: _handleRoleSelected,
      TeamMemberPickerAction.removeMember: _removeMember,
      TeamMemberPickerAction.handleContactAdded: _handleContactAdded,
    },
  );
}

TeamMemberPickerState _initialize(
  TeamMemberPickerState state,
  Action action,
) {
  final TeamMemberPickerState newState = state.clone();

  newState.contacts = action.payload;

  return newState;
}

TeamMemberPickerState _handleRoleSelected(
  TeamMemberPickerState state,
  Action action,
) {
  final TeamMemberPickerState newState = state.clone();

  if (action.payload is TeamMemberItemState) {
    final member = action.payload as TeamMemberItemState;
    final selectedIndex = newState.members.indexWhere(
      (x) => x.id == member.id && x.teamRole != member.teamRole,
    );

    if (selectedIndex == -1) return state;

    newState.members[selectedIndex] = member;
  } else if (action.payload is ContactItemState) {
    final contact = action.payload as ContactItemState;
    final selectedIndex = newState.contacts.indexWhere(
      (x) => x.id == action.payload.id,
    );

    if (selectedIndex == -1) return state;

    newState.contacts.removeAt(selectedIndex);

    var member = TeamMemberItemState(
      id: contact.id,
      firstName: contact.firstName,
      lastName: contact.lastName,
      dateOfBirth: contact.dateOfBirth,
      genderType: contact.genderType,
      teamRole: contact.teamRole,
      isOwner: false,
    );

    newState.members.add(member);
  }

  return newState;
}

TeamMemberPickerState _removeMember(
  TeamMemberPickerState state,
  Action action,
) {
  final TeamMemberPickerState newState = state.clone();

  if (action.payload is TeamMemberItemState) {
    final member = action.payload as TeamMemberItemState;
    final selectedIndex = newState.members.indexWhere(
      (x) => x.id == member.id && x.isOwner == false,
    );

    if (selectedIndex == -1) return state;

    newState.members.removeAt(selectedIndex);
    newState.contacts.add(ContactItemState(
      id: member.id,
      firstName: member.firstName,
      lastName: member.lastName,
      dateOfBirth: member.dateOfBirth,
      genderType: member.genderType,
      teamRole: member.teamRole,
    ));
  }

  return newState;
}

TeamMemberPickerState _handleContactAdded(
  TeamMemberPickerState state,
  Action action,
) {
  final TeamMemberPickerState newState = state.clone();

  newState.contacts.add(action.payload);

  return newState;
}

import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/contacts/state.dart';
import 'package:tcga_flutter/screens/teams/team_editor_page/team_member_item_component/state.dart';

import 'available_members_component/state.dart';
import 'selected_members_component/state.dart';

class TeamMemberPickerState implements Cloneable<TeamMemberPickerState> {
  List<TeamMemberItemState> members;
  List<ContactItemState> contacts;

  @override
  TeamMemberPickerState clone() {
    return TeamMemberPickerState()
      ..members = members
      ..contacts = contacts;
  }
}

TeamMemberPickerState initState(Map<String, dynamic> args) {
  return TeamMemberPickerState()
    ..members = args.containsKey('members')
        ? args['members'] as List<TeamMemberItemState> ?? []
        : []
    ..contacts = [];
}

class MembersStateConnector
    extends ConnOp<TeamMemberPickerState, SelectedMembersState> {
  @override
  SelectedMembersState get(TeamMemberPickerState state) =>
      SelectedMembersState()..items = state.members;

  @override
  void set(TeamMemberPickerState state, SelectedMembersState subState) =>
      state.members = subState.items
          .map(
            (e) => e.clone(),
          )
          .toList();
}

class ContactsStateConnector
    extends ConnOp<TeamMemberPickerState, AvailableMembersState> {
  @override
  AvailableMembersState get(TeamMemberPickerState state) =>
      AvailableMembersState()..items = state.contacts;

  @override
  void set(TeamMemberPickerState state, AvailableMembersState subState) =>
      state.contacts = subState.items
          .map(
            (e) => e.clone(),
          )
          .toList();
}

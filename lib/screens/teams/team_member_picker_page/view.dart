import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/widget/import.dart';

import 'action.dart';
import 'state.dart';

Widget buildView(
    TeamMemberPickerState state, Dispatch dispatch, ViewService viewService) {
  return Builder(builder: (context) {
    return Padding(
      padding: EdgeInsets.only(
        bottom: MediaQuery.of(context).viewInsets.bottom,
      ),
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.white,
        appBar: _appBar(viewService),
        bottomNavigationBar: state.members.length > 0
            ? _bottomNavigationBar(viewService, state, dispatch)
            : null,
        body: NestedScrollView(
          headerSliverBuilder: (context, bool) => [
            SliverToBoxAdapter(
              child: Container(
                padding: const EdgeInsets.all(16),
                child: EntryWidget(
                  title: AppLocalizations.of(viewService.context)
                      .text('teams.search'),
                  suffixIcon: SvgPicture.asset(
                    'assets/icons/ic_search.svg',
                  ),
                  onTextChanged: (text) => dispatch(
                    TeamMemberPickerActionCreator.search(
                      text,
                    ),
                  ),
                ),
              ),
            ),
          ],
          body: CustomScrollView(
            slivers: [
              SliverToBoxAdapter(
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 16,
                  ),
                  child: Text(
                    AppLocalizations.of(viewService.context)
                        .text('teams.editor.add_new_member.selected_members'),
                    style: textStylePrimary,
                  ),
                ),
              ),
              SliverToBoxAdapter(
                child: SizedBox(
                  height: 16,
                ),
              ),
              viewService.buildComponent('members-component'),
              SliverToBoxAdapter(
                child: SizedBox(
                  height: 16,
                ),
              ),
              SliverToBoxAdapter(
                child: Row(
                  children: [
                    SizedBox(
                      width: 16,
                    ),
                    Text(
                      AppLocalizations.of(viewService.context).text(
                          'teams.editor.add_new_member.available_contacts'),
                      style: textStylePrimary,
                    ),
                    Spacer(),
                    InkWell(
                      onTap: () => dispatch(
                        TeamMemberPickerActionCreator.addContact(),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 16,
                          vertical: 8,
                        ),
                        child: SvgPicture.asset(
                          'assets/icons/ic_button_add.svg',
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SliverToBoxAdapter(
                child: SizedBox(
                  height: 16,
                ),
              ),
              viewService.buildComponent('contacts-component'),
            ],
          ),
        ),
      ),
    );
  });
}

AppBar _appBar(ViewService viewService) {
  return AppBar(
    iconTheme: IconThemeData(color: Colors.black),
    elevation: 3,
    backgroundColor: Color(0xFFFFFFFF),
    leading: IconButton(
        icon: SvgPicture.asset(
          "assets/icons/ic_back.svg",
          width: 24,
          height: 24,
        ),
        onPressed: () {
          Navigator.pop(viewService.context);
        }),
    centerTitle: false,
    title: Text(
      AppLocalizations.of(viewService.context)
          .text('teams.editor.add_new_member.header'),
      style: TextStyle(
          color: Color(0xff4F4F4F), fontSize: 20, fontWeight: FontWeight.w400),
    ),
  );
}

Builder _bottomNavigationBar(
    ViewService viewService, TeamMemberPickerState state, Dispatch dispatch) {
  return Builder(
    builder: (context) => Container(
      decoration: BoxDecoration(
          color: colorPrimary, border: Border.all(color: colorPrimary)),
      child: ButtonWidget(
        colorButton: colorPrimary,
        textButton:
            AppLocalizations.of(viewService.context).text('common.confirm'),
        onPress: () {
          dispatch(
            TeamMemberPickerActionCreator.confirm(),
          );
        },
      ),
    ),
  );
}

import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/contacts/state.dart';
import 'package:tcga_flutter/screens/teams/manager.dart';
import 'package:tcga_flutter/utils/utils.dart';
import '../../../route_name.dart';
import 'action.dart';
import 'state.dart';

final _manager = TeamManager();

Effect<TeamMemberPickerState> buildEffect() {
  return combineEffects(<Object, Effect<TeamMemberPickerState>>{
    Lifecycle.initState: _init,
    TeamMemberPickerAction.confirm: _confirm,
    TeamMemberPickerAction.search: _search,
    TeamMemberPickerAction.addContact: _addContact,
  });
}

Future _init(
  Action action,
  Context<TeamMemberPickerState> ctx,
) async {
  final memberIDs = ctx.state.members
      .map(
        (x) => x.id,
      )
      .toList();
  final contacts = await _manager.getAvailableContacts(
    memberIds: memberIDs,
  );

  ctx.dispatch(
    TeamMemberPickerActionCreator.initialize(
      contacts,
    ),
  );
}

Future _search(
  Action action,
  Context<TeamMemberPickerState> ctx,
) async {
  final memberIDs = ctx.state.members
      .map(
        (x) => x.id,
      )
      .toList();
  final contacts = await _manager.getAvailableContacts(
    memberIds: memberIDs,
    keyword: action.payload,
  );

  ctx.dispatch(
    TeamMemberPickerActionCreator.initialize(
      contacts,
    ),
  );
}

Future _addContact(
  Action action,
  Context<TeamMemberPickerState> ctx,
) async {
  final result = await ctx.navigateTo(
    RouteName.contactsEditor,
  );

  if (result is ContactItemState) {
    ctx.dispatch(
      TeamMemberPickerActionCreator.handleContactAdded(
        result,
      ),
    );
  }
}

Future _confirm(
  Action action,
  Context<TeamMemberPickerState> ctx,
) async {
  if (ctx.state.members.length == 0) return;

  ctx.goBack(
    ctx.state.members,
  );
}

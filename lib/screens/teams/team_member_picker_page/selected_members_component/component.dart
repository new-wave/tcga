import 'package:fish_redux/fish_redux.dart';

import 'effect.dart';
import 'reducer.dart';
import 'state.dart';
import 'team_members_adapter/adapter.dart';
import 'view.dart';

class SelectedMembersComponent extends Component<SelectedMembersState> {
  SelectedMembersComponent()
      : super(
          effect: buildEffect(),
          reducer: buildReducer(),
          view: buildView,
          dependencies: Dependencies<SelectedMembersState>(
              adapter: NoneConn<SelectedMembersState>() + TeamMembersAdapter(),
              slots: <String, Dependent<SelectedMembersState>>{}),
        );
}

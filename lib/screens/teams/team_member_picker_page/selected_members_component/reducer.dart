import 'package:fish_redux/fish_redux.dart';

import 'action.dart';
import 'state.dart';

Reducer<SelectedMembersState> buildReducer() {
  return asReducer(
    <Object, Reducer<SelectedMembersState>>{
      SelectedMembersAction.action: _onAction,
    },
  );
}

SelectedMembersState _onAction(SelectedMembersState state, Action action) {
  final SelectedMembersState newState = state.clone();
  return newState;
}

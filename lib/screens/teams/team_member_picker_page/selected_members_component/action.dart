import 'package:fish_redux/fish_redux.dart';

//TODO replace with your own action
enum SelectedMembersAction { action }

class SelectedMembersActionCreator {
  static Action onAction() {
    return const Action(SelectedMembersAction.action);
  }
}

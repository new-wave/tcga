import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/teams/team_editor_page/team_member_item_component/state.dart';

class SelectedMembersState extends MutableSource
    implements Cloneable<SelectedMembersState> {
  List<TeamMemberItemState> items;

  @override
  SelectedMembersState clone() {
    return SelectedMembersState();
  }

  @override
  Object getItemData(int index) {
    return items[index];
  }

  @override
  String getItemType(int index) => 'team-member-item';

  @override
  int get itemCount => items?.length ?? 0;

  @override
  void setItemData(int index, Object data) {
    items[index] = data;
  }
}

SelectedMembersState initState(Map<String, dynamic> args) {
  return SelectedMembersState();
}

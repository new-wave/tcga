import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart' hide Action, Page;
import 'package:flutter_svg/svg.dart';
import 'package:mdi/mdi.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/screens/contacts/state.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/widget/import.dart';

import '../../../team_editor_page/team_member_item_component/state.dart';
import '../../action.dart';

Widget buildView(
  TeamMemberItemState state,
  Dispatch dispatch,
  ViewService viewService,
) {
  return SizedBox(
    height: 48,
    child: Stack(
      children: [
        Container(
          child: Row(
            children: [
              SizedBox(
                width: 16,
              ),
              Container(
                height: 40,
                width: 40,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(
                    20,
                  ),
                  color: colorPrimary,
                ),
                child: Text(
                  '${state.initial}',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 14,
                  ),
                ),
              ),
              SizedBox(
                width: 16,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    width:MediaQuery.of(viewService.context).size.width-110,
                    child: Text(
                      state.fullName,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        color: colorTextPrimary,
                        fontSize: 14,
                      ),
                    ),
                  ),
                  Row(
                    children: [
                      Text(
                        state.gender,
                        style: TextStyle(
                          color: colorTextSecondary,
                          fontSize: 12,
                        ),
                      ),
                      Text(
                        ' | ',
                        style: TextStyle(
                          color: colorTextSecondary,
                          fontSize: 12,
                        ),
                      ),
                      Text(
                        AppLocalizations.of(viewService.context)
                            .text('teams.editor.item.age'),
                        style: TextStyle(
                          color: colorTextSecondary,
                          fontSize: 12,
                        ),
                      ),
                      Text(
                        ': ${state.age}',
                        style: TextStyle(
                          color: colorTextSecondary,
                          fontSize: 12,
                        ),
                      ),
                    ],
                  ),
                  Text(
                    state.teamRoleDisplay,
                    style: TextStyle(
                      color: colorTextSecondary,
                      fontSize: 12,
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
        Opacity(
          opacity: 0,
          child: PickerWidget(
            items: ContactItemState.teamRoles.values.toList(),
            initialIndex: state.teamRole != null
                ? ContactItemState.teamRoles.keys
                    .toList()
                    .indexOf(state.teamRole)
                : null,
            title: AppLocalizations.of(viewService.context)
                .text('teams.editor.add_new_member.select_member_role'),
            indexSelected: (index) {
              dispatch(
                TeamMemberPickerActionCreator.handleRoleSelected(
                  state.clone()
                    ..teamRole =
                        ContactItemState.teamRoles.keys.elementAt(index),
                ),
              );
            },
          ),
        ),
        _rightAction(
          state,
          dispatch,
        ),
      ],
    ),
  );
}

_rightAction(
  TeamMemberItemState state,
  Dispatch dispatch,
) {
  return state.isOwner
      ? Row(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 16,
                vertical: 8,
              ),
              child: SvgPicture.asset(
                'assets/icons/ic_owner.svg',
              ),
            ),
          ],
        )
      : Row(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            InkResponse(
              onTap: () => dispatch(
                TeamMemberPickerActionCreator.removeMember(
                  state,
                ),
              ),
              child: Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 16,
                  vertical: 8,
                ),
                child: Icon(Mdi.close),
              ),
            ),
          ],
        );
}

import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/teams/team_editor_page/team_member_item_component/state.dart';
import 'action.dart';

Effect<TeamMemberItemState> buildEffect() {
  return combineEffects(<Object, Effect<TeamMemberItemState>>{
    TeamMemberItemAction.action: _onAction,
  });
}

void _onAction(Action action, Context<TeamMemberItemState> ctx) {}

import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/teams/team_editor_page/team_member_item_component/state.dart';

import 'effect.dart';
import 'reducer.dart';
import 'view.dart';

class TeamMemberItemComponent extends Component<TeamMemberItemState> {
  TeamMemberItemComponent()
      : super(
          effect: buildEffect(),
          reducer: buildReducer(),
          view: buildView,
          dependencies: Dependencies<TeamMemberItemState>(
              adapter: null, slots: <String, Dependent<TeamMemberItemState>>{}),
        );
}

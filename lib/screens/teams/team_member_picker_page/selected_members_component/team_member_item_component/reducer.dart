import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/teams/team_editor_page/team_member_item_component/state.dart';

import 'action.dart';

Reducer<TeamMemberItemState> buildReducer() {
  return asReducer(
    <Object, Reducer<TeamMemberItemState>>{
      TeamMemberItemAction.action: _onAction,
    },
  );
}

TeamMemberItemState _onAction(TeamMemberItemState state, Action action) {
  final TeamMemberItemState newState = state.clone();
  return newState;
}

import 'package:fish_redux/fish_redux.dart';
import 'action.dart';
import 'state.dart';

Effect<SelectedMembersState> buildEffect() {
  return combineEffects(<Object, Effect<SelectedMembersState>>{
    SelectedMembersAction.action: _onAction,
  });
}

void _onAction(Action action, Context<SelectedMembersState> ctx) {
}

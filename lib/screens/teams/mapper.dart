import 'package:tcga_flutter/database/models.dart';
import 'package:tcga_flutter/database/models/enums.dart';

import 'team_editor_page/state.dart';
import 'team_editor_page/team_member_item_component/state.dart';
import 'teams_page/team_item_component/state.dart';

extension EntityToStateMapper on Team {
  TeamItemState toState() => TeamItemState(
        id: this.id,
        name: this.name,
        memberCount: this.members.length,
        teamType: TeamType.values[this.type],
      );
}

extension PersonEntityToStateMapper on Person {
  TeamMemberItemState toState() => TeamMemberItemState(
        id: this.id,
        firstName: this.firstName,
        lastName: this.lastName,
        dateOfBirth: this.birthDate,
        genderType: this.gender != null
            ? AdministrativeGenderType.values[this.gender]
            : null,
      );
}

extension TeamMemberItemStateToEntityMapper on TeamMemberItemState {
  TeamMember toEntity() => TeamMember(
        personId: this.id,
        role: this.teamRole.index,
      );
}

extension StateToEntityMapper on TeamEditorState {
  Team toEntity() => Team(
        id: this.id,
        name: this.nameEditingController.text,
        ownerId: this.ownerId,
        ownerType: this.ownerType.index,
        type: this.teamType.index,
        members: this
            .members
            .map(
              (x) => x.toEntity(),
            )
            .toList(),
      );
}

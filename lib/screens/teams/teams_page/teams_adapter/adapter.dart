import 'package:fish_redux/fish_redux.dart';

import '../team_item_component/component.dart';

class TeamsAdapter<T extends MutableSource> extends SourceFlowAdapter<T> {
  TeamsAdapter()
      : super(
          pool: <String, Component<Object>>{
            'team-item': TeamItemComponent(),
          },
        );
}

import 'package:fish_redux/fish_redux.dart';

import '../teams_adapter/adapter.dart';

import 'state.dart';
import 'view.dart';

class JointTeamsComponent extends Component<JointTeamsState> {
  JointTeamsComponent()
      : super(
          view: buildView,
          dependencies: Dependencies<JointTeamsState>(
            adapter:
                NoneConn<JointTeamsState>() + TeamsAdapter<JointTeamsState>(),
            slots: <String, Dependent<JointTeamsState>>{},
          ),
        );
}

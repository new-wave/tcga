import 'dart:math' as math;

import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';

import 'state.dart';

Widget buildView(
  JointTeamsState state,
  Dispatch dispatch,
  ViewService viewService,
) {
  final adapter = viewService.buildAdapter();
  return SliverList(
    delegate: SliverChildBuilderDelegate(
      (context, index) {
        final int itemIndex = index ~/ 2;
        if (index.isEven) {
          return adapter.itemBuilder(
            context,
            itemIndex,
          );
        }
        return SizedBox(height: 8);
      },
      semanticIndexCallback: (Widget widget, int localIndex) {
        if (localIndex.isEven) {
          return localIndex ~/ 2;
        }
        return null;
      },
      childCount: math.max(0, adapter.itemCount * 2 - 1),
    ),
  );
}

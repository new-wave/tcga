import 'package:fish_redux/fish_redux.dart';

import '../team_item_component/state.dart';

class JointTeamsState extends MutableSource
    implements Cloneable<JointTeamsState> {
  List<TeamItemState> items;

  @override
  JointTeamsState clone() {
    return JointTeamsState()..items = items;
  }

  @override
  Object getItemData(int index) {
    return items[index];
  }

  @override
  String getItemType(int index) {
    return 'team-item';
  }

  @override
  int get itemCount => items?.length ?? 0;

  @override
  void setItemData(int index, Object data) {
    items[index] = data;
  }
}

JointTeamsState initState(Map<String, dynamic> args) {
  return JointTeamsState()..items = [];
}

import 'package:fish_redux/fish_redux.dart';

import 'effect.dart';
import 'joint_teams_component/component.dart';
import 'owned_teams_component/component.dart';
import 'reducer.dart';
import 'state.dart';
import 'view.dart';

class TeamsPage extends Page<TeamsState, Map<String, dynamic>> {
  TeamsPage()
      : super(
          initState: initState,
          effect: buildEffect(),
          reducer: buildReducer(),
          view: buildView,
          dependencies: Dependencies<TeamsState>(
            slots: <String, Dependent<TeamsState>>{
              'participated-teams-component':
                  ParticipatedTeamStateConnector() + JointTeamsComponent(),
              'owned-teams-component':
                  OwnedTeamStateConnector() + OwnedTeamsComponent(),
            },
          ),
          middleware: <Middleware<TeamsState>>[],
        );
}

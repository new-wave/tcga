import 'package:fish_redux/fish_redux.dart';

import 'action.dart';
import 'state.dart';
import 'team_item_component/state.dart';

Reducer<TeamsState> buildReducer() {
  return asReducer(
    <Object, Reducer<TeamsState>>{
      TeamsAction.initialize: _initialize,
      TeamsAction.handleTeamCreated: _handleTeamAdded,
      TeamsAction.handleTeamEdited: _handleTeamEdited,
      TeamsAction.handleTeamDeleted: _handleTeamDeleted,
    },
  );
}

TeamsState _initialize(TeamsState state, Action action) {
  final TeamsState newState = state.clone();
  newState.items = action.payload;
  return newState;
}

TeamsState _handleTeamAdded(TeamsState state, Action action) {
  final item = action.payload as TeamItemState;
  final newState = state.clone();

  newState.items.add(item);

  return newState;
}

TeamsState _handleTeamDeleted(TeamsState state, Action action) {
  final id = action.payload as String;
  final deletedIndex = state.items.indexWhere((x) => x.id == id);

  if (deletedIndex == -1) return state;

  final newState = state.clone();

  newState.items.removeAt(deletedIndex);

  return newState;
}

TeamsState _handleTeamEdited(TeamsState state, Action action) {
  final item = action.payload as TeamItemState;
  final newState = state.clone();

  final editedIndex = newState.items.indexWhere(
    (x) => x.id == item.id,
  );

  newState.items[editedIndex] = item;

  return newState;
}

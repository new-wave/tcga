import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/teams/manager.dart';
import 'package:tcga_flutter/screens/teams/teams_page/team_item_component/state.dart';

import '../../../route_name.dart';
import '../../../utils/utils.dart';

import 'action.dart';
import 'state.dart';

final _manager = TeamManager();

Effect<TeamsState> buildEffect() {
  return combineEffects(<Object, Effect<TeamsState>>{
    Lifecycle.initState: _init,
    TeamsAction.create: _create,
    TeamsAction.addMembers: _addMembers,
  });
}

Future _init(Action action, Context<TeamsState> ctx) async {
  final items = await _manager.query();

  ctx.dispatch(
    TeamsActionCreator.initialize(
      items,
    ),
  );
}

Future _create(Action action, Context<TeamsState> ctx) async {
  final result = await ctx.navigateTo(
    RouteName.teamEditor,
  );

  if (result == null) return;

  ctx.dispatch(
    TeamsActionCreator.handleTeamCreated(
      result,
    ),
  );
}

Future _addMembers(Action action, Context<TeamsState> ctx) async {
  final team = action.payload as TeamItemState;
  final result =
      await ctx.navigateTo(RouteName.teamEditor, arguments: {'item': team});

  if (result == null) return;

  if (result is TeamItemState) {
    if (result.isOwner) {
      ctx.dispatch(
        TeamsActionCreator.handleTeamEdited(
          result,
        ),
      );
    } else {
      ctx.dispatch(
        TeamsActionCreator.handleTeamDeleted(
          result.id,
        ),
      );
    }
  } else if (result is Map<String, dynamic>) {
    ctx.dispatch(
      TeamsActionCreator.handleTeamDeleted(
        result['deleted-item-id'],
      ),
    );
  }
}

import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tcga_flutter/config/palette.dart';

import 'state.dart';
import '../action.dart';

Widget buildView(
    TeamItemState state, Dispatch dispatch, ViewService viewService) {
  return SizedBox(
    height: 40,
    child: InkWell(
      onTap: () => dispatch(
        TeamsActionCreator.addMembers(state),
      ),
      child: Container(
        child: Row(
          children: [
            SizedBox(
              width: 16,
            ),
            Container(
              height: 40,
              width: 40,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(
                  20,
                ),
                color: colorPrimary,
              ),
              child: Text(
                '${state.initial}',
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 14,
                ),
              ),
            ),
            SizedBox(
              width: 16,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  state.name,
                  style: TextStyle(
                    color: colorTextPrimary,
                    fontSize: 14,
                  ),
                ),
                Row(
                  children: [
                    Text(
                      '${state.memberCountDisplay}',
                      style: TextStyle(
                        color: colorTextSecondary,
                        fontSize: 12,
                      ),
                    ),
                  ],
                )
              ],
            ),
            Spacer(),
            if (state.isOwner)
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                child: SvgPicture.asset(
                  'assets/icons/add_member.svg',
                ),
              ),
            if (!state.isOwner)
              SizedBox(
                width: 16,
              ),
          ],
        ),
      ),
    ),
  );
}

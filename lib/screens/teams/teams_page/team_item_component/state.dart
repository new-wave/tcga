import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/database/models/enums.dart';

class TeamItemState implements Cloneable<TeamItemState> {
  String id;
  String name;
  int memberCount;
  bool isOwner;
  TeamType teamType;

  TeamItemState({
    this.id,
    this.name,
    this.memberCount,
    this.isOwner = false,
    this.teamType,
  });

  get initial {
    if (name == null || name.trim().isEmpty) return '';

    final nameParts = name.split(' ');

    if (nameParts.length == 1) {
      return nameParts.first
          .substring(
            0,
            1,
          )
          .toUpperCase();
    }

    return (nameParts.first.substring(
              0,
              1,
            ) +
            nameParts.last.substring(
              0,
              1,
            ))
        .toUpperCase();
  }

  get memberCountDisplay =>
      memberCount == 1 ? '$memberCount member' : '$memberCount members';

  @override
  TeamItemState clone() {
    return TeamItemState()
      ..id = id
      ..name = name
      ..memberCount = memberCount
      ..isOwner = isOwner
      ..teamType = teamType;
  }
}

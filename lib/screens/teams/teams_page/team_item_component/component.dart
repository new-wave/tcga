import 'package:fish_redux/fish_redux.dart';

import 'state.dart';
import 'view.dart';

class TeamItemComponent extends Component<TeamItemState> {
  TeamItemComponent()
      : super(
          view: buildView,
        );
}

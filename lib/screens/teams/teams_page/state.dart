import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/teams/teams_page/owned_teams_component/state.dart';

import 'joint_teams_component/state.dart';
import 'team_item_component/state.dart';

class TeamsState extends MutableSource implements Cloneable<TeamsState> {
  List<TeamItemState> items;

  @override
  TeamsState clone() {
    return TeamsState()..items = items;
  }

  @override
  Object getItemData(int index) {
    return items[index];
  }

  @override
  String getItemType(int index) {
    return 'team-item';
  }

  @override
  int get itemCount => items?.length ?? 0;

  @override
  void setItemData(int index, Object data) {
    items[index] = data;
  }
}

TeamsState initState(Map<String, dynamic> args) {
  return TeamsState()..items = [];
}

class ParticipatedTeamStateConnector
    extends ConnOp<TeamsState, JointTeamsState> {
  @override
  JointTeamsState get(TeamsState state) =>
      JointTeamsState()..items = state.items.where((x) => !x.isOwner).toList();

  @override
  void set(TeamsState state, JointTeamsState subState) {}
}

class OwnedTeamStateConnector extends ConnOp<TeamsState, OwnedTeamsState> {
  @override
  OwnedTeamsState get(TeamsState state) =>
      OwnedTeamsState()..items = state.items.where((x) => x.isOwner).toList();

  @override
  void set(TeamsState state, OwnedTeamsState subState) {}
}

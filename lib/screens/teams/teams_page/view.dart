import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';

import 'action.dart';
import 'state.dart';

Widget buildView(TeamsState state, Dispatch dispatch, ViewService viewService) {
  return Scaffold(
    backgroundColor: Colors.white,
    appBar: _appBar(viewService, dispatch),
    body: CustomScrollView(
      slivers: [
        SliverToBoxAdapter(
          child: SizedBox(
            height: 16,
          ),
        ),
        SliverToBoxAdapter(
          child: Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 16,
              vertical: 8,
            ),
            child: Text(
              AppLocalizations.of(viewService.context)
                  .text('teams.teams_I_manage'),
              style: textStylePrimary,
            ),
          ),
        ),
        viewService.buildComponent(
          'owned-teams-component',
        ),
        SliverToBoxAdapter(
          child: SizedBox(
            height: 16,
          ),
        ),
        SliverToBoxAdapter(
          child: Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 16,
              vertical: 8,
            ),
            child: Text(
              AppLocalizations.of(viewService.context)
                  .text('teams.teams_I_participate'),
              style: textStylePrimary,
            ),
          ),
        ),
        viewService.buildComponent(
          'participated-teams-component',
        ),
      ],
    ),
  );
}

AppBar _appBar(ViewService viewService, Dispatch dispatch) {
  return AppBar(
    iconTheme: IconThemeData(color: Colors.black),
    elevation: 3,
    backgroundColor: Color(0xFFFFFFFF),
    leading: IconButton(
        icon: SvgPicture.asset(
          "assets/icons/ic_back.svg",
          width: 24,
          height: 24,
        ),
        onPressed: () {
          Navigator.pop(viewService.context);
        }),
    centerTitle: false,
    title: Text(
      AppLocalizations.of(viewService.context).text('teams.title'),
      style: TextStyle(
          color: Color(0xff4F4F4F), fontSize: 20, fontWeight: FontWeight.w400),
    ),
    actions: [
      IconButton(
        icon: SvgPicture.asset(
          "assets/icons/ic_button_add.svg",
          width: 24,
          height: 24,
        ),
        onPressed: () => dispatch(
          TeamsActionCreator.create(),
        ),
      ),
    ],
  );
}

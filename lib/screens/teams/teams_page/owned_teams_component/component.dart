import 'package:fish_redux/fish_redux.dart';

import '../teams_adapter/adapter.dart';

import 'state.dart';
import 'view.dart';

class OwnedTeamsComponent extends Component<OwnedTeamsState> {
  OwnedTeamsComponent()
      : super(
          view: buildView,
          dependencies: Dependencies<OwnedTeamsState>(
            adapter:
                NoneConn<OwnedTeamsState>() + TeamsAdapter<OwnedTeamsState>(),
            slots: <String, Dependent<OwnedTeamsState>>{},
          ),
        );
}

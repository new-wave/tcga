import 'package:fish_redux/fish_redux.dart';

import '../team_item_component/state.dart';

class OwnedTeamsState extends MutableSource
    implements Cloneable<OwnedTeamsState> {
  List<TeamItemState> items;

  @override
  OwnedTeamsState clone() {
    return OwnedTeamsState()..items = items;
  }

  @override
  Object getItemData(int index) {
    return items[index];
  }

  @override
  String getItemType(int index) {
    return 'team-item';
  }

  @override
  int get itemCount => items?.length ?? 0;

  @override
  void setItemData(int index, Object data) {
    items[index] = data;
  }
}

OwnedTeamsState initState(Map<String, dynamic> args) {
  return OwnedTeamsState()..items = [];
}

import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/teams/teams_page/team_item_component/state.dart';

enum TeamsAction {
  initialize,
  create,
  handleTeamCreated,
  handleTeamDeleted,
  addMembers,
  handleTeamEdited,
}

class TeamsActionCreator {
  static Action initialize(List<TeamItemState> items) {
    return Action(
      TeamsAction.initialize,
      payload: items,
    );
  }

  static Action handleTeamDeleted(String id) {
    return Action(
      TeamsAction.handleTeamDeleted,
      payload: id,
    );
  }

  static Action create() {
    return const Action(TeamsAction.create);
  }

  static Action handleTeamCreated(TeamItemState item) {
    return Action(
      TeamsAction.handleTeamCreated,
      payload: item,
    );
  }

  static Action handleTeamEdited(TeamItemState item) {
    return Action(
      TeamsAction.handleTeamEdited,
      payload: item,
    );
  }

  static Action addMembers(TeamItemState item) {
    return Action(
      TeamsAction.addMembers,
      payload: item,
    );
  }
}

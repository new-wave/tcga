import 'package:fish_redux/fish_redux.dart';

import 'effect.dart';
import 'reducer.dart';
import 'state.dart';
import 'view.dart';

class TeamMemberRolePickerPage extends Page<TeamMemberRolePickerState, Map<String, dynamic>> {
  TeamMemberRolePickerPage()
      : super(
            initState: initState,
            effect: buildEffect(),
            reducer: buildReducer(),
            view: buildView,
            dependencies: Dependencies<TeamMemberRolePickerState>(
                adapter: null,
                slots: <String, Dependent<TeamMemberRolePickerState>>{
                }),
            middleware: <Middleware<TeamMemberRolePickerState>>[
            ],);

}

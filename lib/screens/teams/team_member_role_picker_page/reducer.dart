import 'package:fish_redux/fish_redux.dart';

import 'action.dart';
import 'state.dart';

Reducer<TeamMemberRolePickerState> buildReducer() {
  return asReducer(
    <Object, Reducer<TeamMemberRolePickerState>>{
      TeamMemberRolePickerAction.action: _onAction,
    },
  );
}

TeamMemberRolePickerState _onAction(TeamMemberRolePickerState state, Action action) {
  final TeamMemberRolePickerState newState = state.clone();
  return newState;
}

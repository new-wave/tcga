import 'package:fish_redux/fish_redux.dart';
import 'action.dart';
import 'state.dart';

Effect<TeamMemberRolePickerState> buildEffect() {
  return combineEffects(<Object, Effect<TeamMemberRolePickerState>>{
    TeamMemberRolePickerAction.action: _onAction,
  });
}

void _onAction(Action action, Context<TeamMemberRolePickerState> ctx) {
}

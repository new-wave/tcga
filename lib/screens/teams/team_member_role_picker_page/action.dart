import 'package:fish_redux/fish_redux.dart';

//TODO replace with your own action
enum TeamMemberRolePickerAction { action }

class TeamMemberRolePickerActionCreator {
  static Action onAction() {
    return const Action(TeamMemberRolePickerAction.action);
  }
}

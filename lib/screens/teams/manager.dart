import 'package:tcga_flutter/database/models.dart';
import 'package:tcga_flutter/database/models/enums.dart';
import 'package:tcga_flutter/database/repository.dart';
import 'package:tcga_flutter/screens/auth/manager.dart';
import 'package:tcga_flutter/screens/contacts/state.dart';
import 'package:tcga_flutter/screens/teams/team_editor_page/state.dart';
import 'package:tcga_flutter/screens/teams/team_editor_page/team_member_item_component/state.dart';

import 'mapper.dart';
import 'teams_page/team_item_component/state.dart';

class TeamManager {
  static final _singleton = TeamManager._internal();

  factory TeamManager() {
    return _singleton;
  }

  TeamManager._internal();

  final _authManager = AuthManager();
  final _personRepository = Repository<Person>();
  final _teamRepository = Repository<Team>();

  Future<List<TeamItemState>> query({String keyword}) async {
    final currentCaregiverId = _authManager.currentCaregiverId;
    final teams = await _teamRepository.query(
        filter: (x) =>
            x.type != TeamType.Contacts.index &&
            (x.ownerId == currentCaregiverId ||
                x.ownerId != currentCaregiverId &&
                    x.members.any(
                      (x) => x.personId == currentCaregiverId,
                    )));

    return teams
        .map(
          (x) => x.toState()..isOwner = x.ownerId == currentCaregiverId,
        )
        .toList();
  }

  Future<TeamEditorState> fill(TeamEditorState state) async {
    final newState = state.clone();
    final team = await _teamRepository.get(state.id);
    final members = <TeamMemberItemState>[];

    for (var member in team.members) {
      if (team.ownerId == member.personId) continue;

      final person = await _personRepository.get(member.personId);
      members.add(
        person.toState()
          ..teamRole = TeamRole.values[member.role]
          ..isOwner = team.ownerId == person.id
          ..isActive = member.isActive,
      );
    }

    newState.ownerId = team.ownerId;
    newState.ownerType = TeamOwnerType.values[team.ownerType];
    newState.members = members;
    return newState;
  }

  Future<TeamItemState> update(TeamEditorState state) async {
    final entity = state.toEntity();

    final _ = await _teamRepository.update(entity);

    return entity.toState()
      ..isOwner = entity.ownerId == _authManager.currentCaregiverId;
  }

  Future<TeamItemState> create(TeamEditorState state) async {
    final entity = state.toEntity()..ownerId = _authManager.currentCaregiverId;

    entity.id = await _teamRepository.create(entity);

    return entity.toState()..isOwner = true;
  }

  Future<List<ContactItemState>> getAvailableContacts({
    List<String> memberIds,
    String keyword,
  }) async {
    keyword = (keyword ?? '').trim().toUpperCase();

    final team = await _getContactTeam();
    final members = team.members ?? [];

    final items = <ContactItemState>[];

    for (var i = 0; i < members.length; i++) {
      if (members[i].personId == _authManager.currentCaregiverId ||
          memberIds.contains(members[i].personId)) {
        continue;
      }

      final person = await _personRepository.get(
        members[i].personId,
      );
      final fullName = '${person.firstName} ${person.lastName}'.toUpperCase();

      if (keyword.isNotEmpty && !fullName.contains(keyword)) {
        continue;
      }

      items.add(ContactItemState(
        id: person.id,
        dateOfBirth: person.birthDate,
        firstName: person.firstName,
        lastName: person.lastName,
        genderType: person.gender != null
            ? AdministrativeGenderType.values[person.gender]
            : null,
        teamRole: TeamRole.values[members[i].role],
      ));
    }

    return items;
  }

  Future<Team> _getContactTeam() async {
    final personId = _authManager.currentCaregiverId;
    final exists = await _teamRepository.query(
      filter: (team) {
        return team.ownerId == personId &&
            team.ownerType == TeamOwnerType.Caregiver.index &&
            team.type == TeamType.Contacts.index;
      },
    );

    if (exists.length > 0) {
      return exists[0];
    }

    final team = Team(
        ownerId: personId,
        name: 'Contacts',
        ownerType: TeamOwnerType.Caregiver.index,
        type: TeamType.Contacts.index,
        members: [
          TeamMember(
            isActive: true,
            personId: personId,
            role: TeamRole.CareGiver.index,
          ),
        ]);

    team.id = await _teamRepository.create(team);

    return team;
  }

  Future<bool> delete(String id) {
    return _teamRepository.delete(id);
  }
}

import 'package:fish_redux/fish_redux.dart';

enum NoteAction { action, movepage }

class NoteActionCreator {
  static Action onAction() {
    return const Action(NoteAction.action);
  }

  static Action onMovePage() {
    return Action(NoteAction.movepage);
  }
}

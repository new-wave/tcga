import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart' hide Action, Page;
import 'package:tcga_flutter/route_name.dart';
import 'action.dart';
import 'state.dart';

Effect<NoteState> buildEffect() {
  return combineEffects(<Object, Effect<NoteState>>{
    // Lifecycle.initState: _onInit,
    // Lifecycle.dispose: _onDispose,
    NoteAction.action: _onAction,
    NoteAction.movepage: _onMovePage,
  });
}

void _onAction(Action action, Context<NoteState> ctx) {}
void _onMovePage(Action action, Context<NoteState> ctx) {
  Navigator.of(ctx.context)
      .pushNamed(RouteName.recipientsDashboard, arguments: null);
}

// void _onInit(Action action, Context<CareRecipientState> ctx) {}

// void _onDispose(Action action, Context<CareRecipientState> ctx) {}

import 'package:fish_redux/fish_redux.dart';
import 'action.dart';
import 'state.dart';

Reducer<NoteState> buildReducer() {
  return asReducer(
    <Object, Reducer<NoteState>>{
      NoteAction.action: _onAction,
    },
  );
}

NoteState _onAction(NoteState state, Action action) {
  final NoteState newState = state.clone();
  return newState;
}

import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'action.dart';
import 'state.dart';

Widget buildView(NoteState state, Dispatch dispatch, ViewService viewService) {
  //final _adapter = viewService.buildAdapter();
  return Builder(builder: (context) {
    return Container(
      child: Scaffold(
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              GestureDetector(
                onTap: () {
                  dispatch(NoteActionCreator.onMovePage());
                },
                child: Container(
                  color: Colors.grey,
                  height: 50,
                  width: 50,
                ),
              ),
              Text(AppLocalizations.of(context).text("home.bottombar.notes")),
            ],
          ),
        ),
      ),
    );
  });
}

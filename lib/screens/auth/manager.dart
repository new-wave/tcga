import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:flutter/services.dart';
import 'package:flutter_appauth/flutter_appauth.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:tcga_flutter/api/api_manager.dart';
import 'package:tcga_flutter/api/base_api_model.dart';
import 'package:tcga_flutter/database/models/api/registration_api.dart';
import 'package:tcga_flutter/database/models/api/user_api.dart';
import 'package:tcga_flutter/database/models/enums.dart';

import 'package:tcga_flutter/database/models/person.dart';
import 'package:tcga_flutter/database/models/team.dart';
import 'package:tcga_flutter/database/models/user.dart';
import 'package:tcga_flutter/database/repository.dart';
import 'package:tcga_flutter/screens/valuesets/manager.dart';

import 'password_hasher.dart';

class AuthManager {
  static final _singleton = AuthManager._internal();

  factory AuthManager() {
    return _singleton;
  }

  AuthManager._internal();

  final _storage = new FlutterSecureStorage();
  final _userRepository = Repository<UserModel>();
  final _personRepository = Repository<Person>();
  final _teamRepository = Repository<Team>();
  final _apiManager = APIManager();

  static const String _apiKey = '6LeCwZYUAAAAAJo8IVvGX9dH65Rw89vxaxErCeou';
  static const String _apiSecret = '6LeCwZYUAAAAAKGahIjwfOARevvRETgvwhPMKCs_';
  final FlutterAppAuth appAuth = FlutterAppAuth();
  final FlutterSecureStorage secureStorage = const FlutterSecureStorage();

  String get CLIENT_ID => 'c6f0e0e0-dde9-4f8a-9db9-a2146bd5ea14';

  String get REDIRECT_URI => 'tcga://sign-up/';

  String get ISSUER =>
      'https://thecaregiverappb2c.b2clogin.com/51e2f68d-9cc0-430b-8d80-42a6c70bf5c0/v2.0/';

  String get AUTHORIZATION_ENDPOINT =>
      'https://thecaregiverappb2c.b2clogin.com/thecaregiver.app/b2c_1_signin/oauth2/v2.0/authorize';

  String get TOKEN_ENDPOINT =>
      'https://thecaregiverappb2c.b2clogin.com/thecaregiver.app/b2c_1_signin/oauth2/v2.0/token';

  String get apiKey => _apiKey;

  String get apiSecret => _apiSecret;
  String _currentCaregiverId;

  String get currentCaregiverId => _currentCaregiverId;

  Future<bool> signIn() async {
    // final users = await _userRepository.query(
    //   filter: (x) {
    //     return x.emailAddress != null &&
    //         x.emailAddress.toUpperCase() == emailAddress.toUpperCase();
    //   },
    // );
    //
    // if (users.length != 1) {
    //   return false;
    // }
    //
    // var isValid = PasswordHasher.verify(password, users[0].password, '');
    //
    // if (isValid) {
    //  _currentCaregiverId = users[0].personId;
    await ValueSetManager().importDataFromJson();
    await ValueSetManager().copyDbFiles();
    //}
    return true;
//    return isValid;
  }

  Future<bool> validateOTP({
    String id,
    String emailAddress,
    String mobileNumber,
    List<Links> links,
    String registrationCode,
    String password,
    String userAgent,
  }) async {
    var baseApi = await _apiManager.activateUser(
      id,
      Registration(
          email: emailAddress,
          mobileNumber: mobileNumber,
          links: links,
          registrationCode: registrationCode,
          password: password,
          userAgent: userAgent),
    );
    //if (baseApi.data == null) return false;
    return baseApi;
  }

  Future<bool> checkEmailExisted({
    String emailAddress,
  }) async {
    emailAddress = emailAddress.trim();
    final users = await _userRepository.query(
      filter: (x) {
        return x.emailAddress != null &&
            x.emailAddress.toUpperCase() == emailAddress.toUpperCase();
      },
    );

    if (users.length > 0) {
      return false;
    }
    return true;
  }

  Future<bool> signUp({
    String id,
    String firstName,
    String lastName,
    String emailAddress,
    String emailOtp,
    String password,
    bool isHealthcareProfessional,
  }) async {
    emailAddress = emailAddress.trim();
    final users = await _userRepository.query(
      filter: (x) {
        return x.emailAddress != null &&
            x.emailAddress.toUpperCase() == emailAddress.toUpperCase();
      },
    );

    if (users.length > 0) {
      return false;
    }

    final person = Person(
      firstName: firstName.trim(),
      lastName: lastName.trim(),
      email: emailAddress,
    );

    final personId = await _personRepository.create(person);

    final hashed = PasswordHasher.hash(password);

    final user = UserModel(
      id: id,
      emailAddress: emailAddress,
      password: hashed[0],
      passwordSalt: hashed[1],
      isHealthcareProfessional: isHealthcareProfessional,
      personId: personId,
    );
    final _ = await _userRepository.create(user);

    final team = Team(
        ownerId: personId,
        name: 'Contacts',
        ownerType: TeamOwnerType.Caregiver.index,
        type: TeamType.Contacts.index,
        members: [
          TeamMember(
            isActive: true,
            personId: personId,
            role: TeamRole.CareGiver.index,
          ),
        ]);

    final __ = await _teamRepository.create(team);

    _currentCaregiverId = personId;
    await ValueSetManager().copyDbFiles();

    return true;
  }

  Future<bool> savePasscode(String passcode) async {
    final hashed = PasswordHasher.hash(passcode);

    await _storage.write(key: 'passcode', value: hashed[0]);
    await _storage.write(key: 'passcode_salt', value: hashed[1]);
    return true;
  }

  Future<AndroidDeviceInfo> getInfoAndroid() async {
    try {
      DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
      var deviceData = await deviceInfo.androidInfo;
      return deviceData;
    } on PlatformException {
      return null;
    }
  }

  Future<IosDeviceInfo> getInfoIOS() async {
    try {
      DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
      var deviceData = await deviceInfo.iosInfo;
      return deviceData;
    } on PlatformException {
      return null;
    }
  }

  Future signOut() async {
    _currentCaregiverId = null;
  }

  Future<bool> regiontions({
    String email,
    String firstName,
    String lastName,
    String userAgent,
    String recaptchaResponse,
  }) async {
    BaseApiModel<UserAPI> baseApi = await _apiManager.createUser(
      UserAPI(
        email: email,
        firstName: firstName,
        lastName: lastName,
        userAgent: userAgent,
        recaptchaResponse: recaptchaResponse,
      ),
    );
    if (baseApi.data == null) return false;
    _currentCaregiverId = baseApi.data.id;
    return true;
  }
}

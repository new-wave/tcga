import 'dart:convert';

import 'package:crypto/crypto.dart';

class PasswordHasher {
  static List<String> hash(String text) {
    final bytes = utf8.encode(text);
    final hashed = sha512.convert(bytes);

    return [hashed.toString(), ''];
  }

  static bool verify(String text, String hashed, String salt) {
    final bytes = utf8.encode(text);
    final toCheck = sha512.convert(bytes);

    return toCheck.toString() == hashed;
  }
}

import 'package:fish_redux/fish_redux.dart';

import 'general/component.dart';
import 'otp/component.dart';
import 'passcode/component.dart';
import 'passcode_confirm/component.dart';
import 'success/component.dart';

import 'effect.dart';
import 'reducer.dart';
import 'state.dart';

import 'view.dart';

class SignUpPage extends Page<PageState, Map<String, dynamic>> {
  SignUpPage()
      : super(
          initState: initState,
          effect: buildEffect(),
          reducer: buildReducer(),
          view: buildView,
          dependencies: Dependencies<PageState>(
            slots: <String, Dependent<PageState>>{
              'general-details':
                  NoneConn<PageState>() + GeneralDetailsComponent(),
              'otp': NoneConn<PageState>() + OtpComponent(),
              'passcode': NoneConn<PageState>() + PasscodeComponent(),
              'passcode-confirm':
                  NoneConn<PageState>() + PasscodeConfirmComponent(),
              'success': NoneConn<PageState>() + SuccessComponent(),
            },
          ),

          /// 页面私有AOP, 如果需要
          // middleware: <Middleware<PageState>>[
          //   logMiddleware(tag: 'ToDoListPage'),
          // ],
        );
}

import 'package:fish_redux/fish_redux.dart';

import 'reducer.dart';
import 'effect.dart';
import '../state.dart';
import 'view.dart';

class PhoneNumberComponent extends Component<PageState> {
  PhoneNumberComponent()
      : super(
          view: buildView,
          effect: buildEffect(),
          reducer: buildReducer(),
        );
}

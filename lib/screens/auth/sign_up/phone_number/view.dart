import 'dart:js';

import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:mdi/mdi.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';

import 'action.dart';
import '../state.dart';

Widget buildView(
  PageState state,
  Dispatch dispatch,
  ViewService viewService,
) {
  return Column(
    mainAxisAlignment: MainAxisAlignment.end,
    children: [
      Expanded(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 42),
          margin: EdgeInsets.only(bottom: 100),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Row(
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      dispatch(
                        ComponentActionCreator.onChooseCountryCode(),
                      );
                    },
                    child: Row(
                      children: [
                        Icon(
                          Mdi.chevronDown,
                          color: colorTitleSecondary,
                        ),
                        Container(
                          margin: edgeLeftInsetsSecondary,
                          child: Image.asset("assets/images/ic_flag.png"),
                        ),
                        Container(
                          margin: edgeLeftInsetsSecondary,
                          child: Text(
                            AppLocalizations.of(viewService.context).text("sign_up.vn_code"),
                            style: textStylePrimary,
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    child: Expanded(
                      child: TextFormField(
                        style: textStylePrimary,
                        keyboardType: TextInputType.phone,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          focusedBorder: InputBorder.none,
                          enabledBorder: InputBorder.none,
                          errorBorder: InputBorder.none,
                          disabledBorder: InputBorder.none,
                          hintText: AppLocalizations.of(viewService.context).text("sign_up.enter_your_phone_number"),
                          hintStyle: textStyleHinttext,
                          contentPadding: edgeLeftInsetsSecondary,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              Divider(
                height: 1,
                color: Color(0xffE5E5E5),
              ),
            ],
          ),
        ),
      ),
    ],
  );
}

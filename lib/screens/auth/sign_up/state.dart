import 'dart:ui';

import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_recaptcha_v2/flutter_recaptcha_v2.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';

import '../../../global_store/state.dart';

enum SignUpStep {
  General,
  EmailAddressVerification,
  Passcode,
  PasscodeVerification,
  Success,
}

class PageState implements GlobalBaseState, Cloneable<PageState> {
  PageController stepController;
  String actionTitle;
  SignUpStep step;

  bool get isLastStep => step == SignUpStep.Success;
  String apiKey;
  String apiSecret;
  RecaptchaV2Controller recaptchaV2Controller;
  String recaptchaToken;

  //final scaffoldKey = GlobalKey<ScaffoldState>();

  String emailTextWarning;
  bool isCheckField;
  bool isSamePassword;
  String confirmPasswordTextWarning;
  String textWarning;

  TextEditingController firstName;
  TextEditingController lastName;
  TextEditingController emailAddress;
  TextEditingController password;
  TextEditingController confirmPassword;

  bool isProfessionalHealthCareProvider;

  TextEditingController otp;

  TextEditingController passcode;
  TextEditingController confirmPasscode;

  @override
  Color themeColor;

  @override
  PageState clone() {
    return PageState()
      ..apiKey = apiKey
      ..apiSecret = apiSecret
      ..recaptchaToken = recaptchaToken
      ..recaptchaV2Controller = recaptchaV2Controller
      ..firstName = firstName
      ..lastName = lastName
      ..emailAddress = emailAddress
      ..password = password
      ..confirmPassword = confirmPassword
      ..isProfessionalHealthCareProvider = isProfessionalHealthCareProvider
      ..otp = otp
      ..passcode = passcode
      ..confirmPasscode = confirmPasscode
      ..themeColor = themeColor
      ..stepController = stepController
      ..step = step
      ..actionTitle = actionTitle
      ..emailTextWarning = emailTextWarning
      ..isCheckField = isCheckField
      ..isSamePassword = isSamePassword
      ..confirmPasswordTextWarning = confirmPasswordTextWarning
      ..textWarning = textWarning;
  }
}

PageState initState(Map<String, dynamic> args) {
  return PageState()
    ..apiKey = ''
    ..apiSecret = ''
    ..recaptchaV2Controller = RecaptchaV2Controller()
    ..recaptchaToken = ''
    ..isProfessionalHealthCareProvider = false
    ..emailTextWarning = ''
    ..isCheckField = false
    ..textWarning = ''
    ..isSamePassword = false
    ..confirmPasswordTextWarning = ''
    ..stepController = PageController(
      initialPage: 0,
    )
    ..firstName = TextEditingController(
      text: '',
      //    text: 'Caregiver',
    )
    ..lastName = TextEditingController(
      text: '',
      //     text: 'X',
    )
    ..emailAddress = TextEditingController(
      text: '',
      //    text: 'tcga@nx.net',
    )
    ..password = TextEditingController(
      text: '',
      //    text: 'Aa@12345',
    )
    ..confirmPassword = TextEditingController(
      text: '',
      //    text: 'Aa@12345',
    )
    ..passcode = TextEditingController(
      text: '',
      //     text: '123456',
    )
    ..confirmPasscode = TextEditingController(
      text: '',
      //     text: '123456',
    )
    ..otp = TextEditingController(
      text: '',
      //     text: '123456',
    )
    ..actionTitle = AppLocalizations.instance.text('sign_up.next')
    ..step = SignUpStep.General;
}

import 'package:tcga_flutter/screens/auth/sign_up/state.dart';
import '../../../utils/string_extensions.dart';
import '../manager.dart';

final _authManager = AuthManager();

class Validator {
  static bool isValid(PageState state) {
    switch (state.step) {
      case SignUpStep.General:
        if (state.firstName.text.isEmptyOrWhitespace) return false;

        if (state.lastName.text.isEmptyOrWhitespace) return false;

        if (state.emailAddress.text.isEmptyOrWhitespace) return false;

        if (!state.emailAddress.text.isValidEmailAddress) return false;

        if (state.password.text.isEmptyOrWhitespace) return false;

        if (!state.password.text.isStrongPassword) return false;

        if (state.password.text != state.confirmPassword.text) return false;

        break;
      case SignUpStep.EmailAddressVerification:
        if (state.otp.text.isEmptyOrWhitespace) return false;

        if (!state.otp.text.isValidPasscode) return false;

        break;
      case SignUpStep.Passcode:
        if (state.passcode.text.isEmptyOrWhitespace) return false;

        if (!state.passcode.text.isValidPasscode) return false;

        break;
      case SignUpStep.PasscodeVerification:
        if (state.passcode.text != state.confirmPasscode.text) return false;

        break;
      default:
        return true;
    }

    return true;
  }

  static Future<bool> checkEmail(PageState state) async {
    var items = (await _authManager.checkEmailExisted(
        emailAddress: state.emailAddress.text));
    return items;
  }
}

// if (state.emailAddress.text.isEmptyOrWhitespace) return false;
//         if (!state.emailAddress.text.isValidEmailAddress) return false;

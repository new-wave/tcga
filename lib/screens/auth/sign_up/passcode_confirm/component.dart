import 'package:fish_redux/fish_redux.dart';

import 'reducer.dart';
import 'effect.dart';
import '../state.dart';
import 'view.dart';

export 'state.dart';

class PasscodeConfirmComponent extends Component<PageState> {
  PasscodeConfirmComponent()
      : super(
          view: buildView,
          effect: buildEffect(),
          reducer: buildReducer(),
        );
}

import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart' as material;
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'action.dart';
import 'state.dart';

Reducer<PageState> buildReducer() {
  return asReducer(
    <Object, Reducer<PageState>>{
      PageAction.init: _initState,
      PageAction.next: _nextReducer,
      PageAction.back: _backReducer,
      PageAction.emailWarningExisted: _emailWarningExistedReducer,
      PageAction.checkField: _checkFieldReducer,
      PageAction.confirm: _confirmReducer,
      PageAction.recaptchav2: _recaptchav2,
    },
  );
}

final _nextSteps = {
  SignUpStep.General: SignUpStep.EmailAddressVerification,
  SignUpStep.EmailAddressVerification: SignUpStep.Passcode,
  SignUpStep.Passcode: SignUpStep.PasscodeVerification,
  SignUpStep.PasscodeVerification: SignUpStep.Success,
};

PageState _initState(PageState state, Action action) {
  final PageState newState = state.clone();
  final map = action.payload as Map<String, String>;
  newState.apiKey = map['apiKey'];
  newState.apiSecret = map['apiSecret'];

  return newState;
}

PageState _confirmReducer(PageState state, Action action) {
  final PageState newState = state.clone();
  if (state.step == SignUpStep.General) {
    if (newState.confirmPassword.text != newState.password.text) {
      newState.isSamePassword = false;
      newState.confirmPasswordTextWarning =
          AppLocalizations.instance.text('sign_up.warning_cfpassword');
    } else {
      newState.isSamePassword = true;
      newState.confirmPasswordTextWarning = '';
    }
  } else if (state.step == SignUpStep.EmailAddressVerification) {
    newState.textWarning =
        AppLocalizations.instance.text('common.warning.otp_was_wrong');
  } else if (state.step == SignUpStep.Passcode) {
    newState.textWarning =
        AppLocalizations.instance.text('common.warning.passcode');
  } else if (state.step == SignUpStep.PasscodeVerification) {
    newState.textWarning =
        AppLocalizations.instance.text('common.warning.confirmpasscode');
  }
  newState.isCheckField = true;

  return newState;
}

PageState _checkFieldReducer(PageState state, Action action) {
  final PageState newState = state.clone();
  if (state.step == SignUpStep.General) {
    newState.isCheckField = true;
  }
  return newState;
}

PageState _recaptchav2(PageState state, Action action) {
  final token = action.payload as String;
  final PageState newState = state.clone();
  if (token == null)
    newState.recaptchaV2Controller.show();
  else {
    newState.recaptchaV2Controller.hide();
    newState.recaptchaToken = token;
  }
  return newState;
}

PageState _emailWarningExistedReducer(PageState state, Action action) {
  final PageState newState = state.clone();
  newState.isCheckField = true;
  newState.emailTextWarning =
      AppLocalizations.instance.text('common.warning.email_has_already');
  return newState;
}

PageState _nextReducer(PageState state, Action action) {
  final PageState newState = state.clone()
    ..isCheckField = false
    ..textWarning = ''
    ..emailTextWarning = ''
    ..confirmPasswordTextWarning = '';

  if (newState.isLastStep) {
    return state;
  }

  changeStep(
    newState,
    _nextSteps,
  );

  return newState;
}

final _backSteps = {
  SignUpStep.PasscodeVerification: SignUpStep.PasscodeVerification,
  SignUpStep.Passcode: SignUpStep.EmailAddressVerification,
  SignUpStep.EmailAddressVerification: SignUpStep.General,
};

PageState _backReducer(PageState state, Action action) {
  final PageState newState = state.clone();

  changeStep(
    newState,
    _backSteps,
  );

  return newState;
}

void changeStep(PageState newState, Map<SignUpStep, SignUpStep> steps) {
  final step = steps[newState.step];

  newState.step = step;
  newState.actionTitle = newState.isLastStep
      ? AppLocalizations.instance.text('sign_up.go_to_dashboard')
      : AppLocalizations.instance.text('sign_up.next');

  newState.stepController.animateToPage(
    step.index,
    duration: Duration(milliseconds: 250),
    curve: material.Curves.easeIn,
  );
}

import 'package:fish_redux/fish_redux.dart';

enum ComponentAction {
  toggleProfessionalHealthCareProvider,
}

class ComponentActionCreator {
  static Action onToggleProfessionalHealthCareProviderAction(bool checked) {
    return Action(
      ComponentAction.toggleProfessionalHealthCareProvider,
      payload: checked,
    );
  }
}

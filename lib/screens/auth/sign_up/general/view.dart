import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/database/models/enums.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/widget/import.dart';

import 'action.dart';
import '../state.dart';

Widget buildView(
  PageState state,
  Dispatch dispatch,
  ViewService viewService,
) {
  return SingleChildScrollView(
    child: Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          margin: EdgeInsets.only(top: 24, left: 16, right: 16),
          child: EntryWidget(
            label: AppLocalizations.of(viewService.context)
                .text("sign_up.first_name_required"),
            hideLabel: true,
            hint: AppLocalizations.of(viewService.context)
                .text("sign_up.enter_your_first_name"),
            controller: state.firstName,
            hintStyle: textStylePrimary,
            keyboardType: TextInputType.name,
            isCheckField: state.isCheckField,
            isRequired: true,
            warningText:
                AppLocalizations.instance.text('sign_up.fill_your_first_name'),
            textInputAction: TextInputAction.next,
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 20, left: 16, right: 16),
          child: EntryWidget(
            label: AppLocalizations.of(viewService.context)
                .text("sign_up.last_name_required"),
            hideLabel: true,
            hint: AppLocalizations.of(viewService.context)
                .text("sign_up.enter_your_last_name"),
            hintStyle: textStylePrimary,
            controller: state.lastName,
            textInputAction: TextInputAction.next,
            keyboardType: TextInputType.name,
            warningText:
                AppLocalizations.instance.text('sign_up.fill_your_last_name'),
            isCheckField: state.isCheckField,
            isRequired: true,
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 20, left: 16, right: 16),
          child: EntryWidget(
            label: AppLocalizations.of(viewService.context)
                .text("sign_up.email_required"),
            hideLabel: true,
            controller: state.emailAddress,
            hint: AppLocalizations.of(viewService.context)
                .text("sign_up.enter_your_mail"),
            hintStyle: textStylePrimary,
            textInputAction: TextInputAction.next,
            keyboardType: TextInputType.emailAddress,
            warningText: state.emailTextWarning,
            isCheckField: state.isCheckField,
            isRequired: true,
            entryWidgetType: EntryWidgetType.Email,
            onTextChanged: (text) {
              state.emailAddress = text as TextEditingController;
            },
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 20, left: 16, right: 16),
          child: EntryWidget(
            label: AppLocalizations.of(viewService.context)
                .text("sign_up.password_required"),
            hideLabel: true,
            controller: state.password,
            hint: AppLocalizations.of(viewService.context)
                .text("sign_up.enter_your_password"),
            hintStyle: textStylePrimary,
            textInputAction: TextInputAction.next,
            keyboardType: TextInputType.text,
            warningText:
                AppLocalizations.instance.text('sign_up.fill_your_password'),
            isCheckField: state.isCheckField,
            isRequired: true,
            entryWidgetType: EntryWidgetType.Password,
            hideText: true,
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 20, left: 16, right: 16),
          child: EntryWidget(
            label: AppLocalizations.of(viewService.context)
                .text("sign_up.confirm_password_required"),
            hideLabel: true,
            controller: state.confirmPassword,
            hint: AppLocalizations.of(viewService.context)
                .text("sign_up.enter_your_confirm_password"),
            hintStyle: textStylePrimary,
            hideText: true,
            textInputAction: TextInputAction.next,
            keyboardType: TextInputType.text,
            warningText: state.confirmPasswordTextWarning,
            isCheckField: state.isCheckField,
            isRequired: true,
            entryWidgetType: EntryWidgetType.Password,
            onTextChanged: (text) {
              state.confirmPassword = text as TextEditingController;
            },
          ),
        ),
        Container(
          margin: edgeTopInsetsPrimary,
          child: Row(
            children: [
              Checkbox(
                value: state.isProfessionalHealthCareProvider,
                onChanged: (checked) {
                  dispatch(
                    ComponentActionCreator
                        .onToggleProfessionalHealthCareProviderAction(
                      checked,
                    ),
                  );
                },
              ),
              Text(AppLocalizations.of(viewService.context)
                  .text("sign_up.im_a_professional_healthcare_provider")),
            ],
          ),
        ),
      ],
    ),
  );
}

import 'package:fish_redux/fish_redux.dart';

import 'action.dart';
import '../state.dart';

Reducer<PageState> buildReducer() {
  return asReducer(<Object, Reducer<PageState>>{
    ComponentAction.toggleProfessionalHealthCareProvider:
        _toggleProfessionaleHealthCareProvider,
  });
}

PageState _toggleProfessionaleHealthCareProvider(
    PageState state, Action action) {
  if (action.payload is bool) {
    return state.clone()..isProfessionalHealthCareProvider = action.payload;
  }
  return state;
}

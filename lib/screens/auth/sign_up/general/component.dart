import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/auth/sign_up/state.dart';

import 'reducer.dart';
import 'view.dart';
import '../state.dart';

class GeneralDetailsComponent extends Component<PageState> {
  GeneralDetailsComponent()
      : super(
          view: buildView,
          // effect: buildEffect(),
          reducer: buildReducer(),
        );
}

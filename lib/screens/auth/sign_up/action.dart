import 'package:fish_redux/fish_redux.dart';

enum PageAction {
  init,
  validateAndSubmit,
  next,
  back,
  goHome,
  emailWarning,
  emailWarningExisted,
  checkField,
  onTextChange,
  refresh,
  confirm,
  recaptchav2,
  onVerifiedSuccessfully,
}

class PageActionCreator {
  static Action onVerifiedSuccessfully(String token) {
    return Action(
      PageAction.onVerifiedSuccessfully,
      payload: token,
    );
  }

  static Action init(Map<String, String> map) {
    return Action(
      PageAction.init,
      payload: map,
    );
  }

  static Action validateAndSubmit() {
    return const Action(
      PageAction.validateAndSubmit,
    );
  }

  static Action onTextChange(String text) {
    return const Action(
      PageAction.onTextChange,
    );
  }

  static Action checkField() {
    return const Action(
      PageAction.checkField,
    );
  }

  static Action next() {
    return const Action(
      PageAction.next,
    );
  }

  static Action emailWarningExisted() {
    return Action(
      PageAction.emailWarningExisted,
    );
  }

  static Action confirm() {
    return Action(
      PageAction.confirm,
    );
  }

  static Action back() {
    return const Action(
      PageAction.back,
    );
  }

  static Action goHome() {
    return const Action(
      PageAction.goHome,
    );
  }

  static Action recaptchav2(String token) {
    return Action(
      PageAction.recaptchav2,
      payload: token,
    );
  }
}

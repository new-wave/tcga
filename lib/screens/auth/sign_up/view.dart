import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart' hide Action, Page;
import 'package:flutter_recaptcha_v2/flutter_recaptcha_v2.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/widget/button_widget.dart';
import 'package:tcga_flutter/widget/import.dart';

import 'action.dart';
import 'state.dart';

Widget buildView(PageState state, Dispatch dispatch, ViewService viewService) {
  return Builder(builder: (context) {
    return Padding(
      padding: EdgeInsets.only(
        bottom: MediaQuery.of(context).viewInsets.bottom,
      ),
      child: Scaffold(
        backgroundColor: Colors.white,
        resizeToAvoidBottomInset: false,
        // resizeToAvoidBottomPadding: false,
        appBar: state.isLastStep
            ? PreferredSize(
                child: SizedBox.shrink(),
                preferredSize: Size.zero,
              )
            : AppBar(
                centerTitle: true,
                title: Text(
                  AppLocalizations.of(viewService.context)
                      .text("sign_up.sign_up"),
                  style: textStyleTitlePrimary,
                ),
                leading: GestureDetector(
                  onTap: () {
                    var canPop = state.stepController.page < 1;

                    if (canPop) {
                      Navigator.of(viewService.context).pop();

                      return;
                    }

                    dispatch(PageActionCreator.back());
                  },
                  child: Container(
                    padding: EdgeInsets.only(left: 16),
                    child: SvgPicture.asset(
                      'assets/icons/ic_arrow_left_circle.svg',
                    ),
                  ),
                ),
                leadingWidth: 46,
              ),
        body: Stack(
          children: [
            GestureDetector(
              onTap: () {
                FocusScopeNode currentFocus =
                    FocusScope.of(viewService.context);

                if (!currentFocus.hasPrimaryFocus) {
                  currentFocus.unfocus();
                }
              },
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Expanded(
                    child: PageView(
                      controller: state.stepController,
                      physics: NeverScrollableScrollPhysics(),
                      children: [
                        viewService.buildComponent('general-details'),
                        viewService.buildComponent('otp'),
                        viewService.buildComponent('passcode'),
                        viewService.buildComponent('passcode-confirm'),
                        viewService.buildComponent('success'),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            RecaptchaV2(
              apiKey: state.apiKey,
              apiSecret: state.apiSecret,
              controller: state.recaptchaV2Controller,
              onVerifiedSuccessfully: (t, u) {
                if (t) dispatch(PageActionCreator.onVerifiedSuccessfully(u));
              },
            ),
          ],
        ),
        bottomNavigationBar: Container(
          child: ButtonWidget(
            onPress: () {
              dispatch(
                state.isLastStep
                    ? PageActionCreator.goHome()
                    : PageActionCreator.validateAndSubmit(),
              );
            },
            colorButton: colorPrimary,
            textButton: state.actionTitle.toUpperCase(),
          ),
        ),
      ),
    );
  });
}

import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/widget/import.dart';

import '../state.dart';

Widget buildView(
  PageState state,
  Dispatch dispatch,
  ViewService viewService,
) {
  return Column(
    children: <Widget>[
      Expanded(
        flex: 6,
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Spacer(
                flex: 1,
              ),
              Center(
                child: Text(
                  "${AppLocalizations.of(viewService.context).text("sign_up.we_send_the_code_to_your_email_address")}:",
                  textAlign: TextAlign.center,
                  style: textStylePrimary,
                ),
              ),
              Center(
                child: Text(
                  state.emailAddress?.text,
                  textAlign: TextAlign.center,
                  style: textStylePrimary,
                ),
              ),
              Spacer(
                flex: 1,
              ),
              Container(
                child: Text(
                  AppLocalizations.of(viewService.context)
                      .text("sign_up.enter_the_code"),
                  textAlign: TextAlign.center,
                  style: textStylePrimary,
                ),
              ),
              Container(
                child: Padding(
                  padding: const EdgeInsets.only(top: 16),
                  child: OtpWidget(
                    controller: state.otp,
                    maxLength: 6,
                    isSecured: false,
                    textWarning: state.textWarning,
                    isCheckField: state.isCheckField,
                  ),
                ),
              ),
              Spacer(
                flex: 1,
              ),
            ],
          ),
        ),
      ),
    ],
  );
}

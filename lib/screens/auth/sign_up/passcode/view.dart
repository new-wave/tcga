import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/widget/import.dart';

import '../state.dart';

Widget buildView(
  PageState state,
  Dispatch dispatch,
  ViewService viewService,
) {
  return Column(
    children: <Widget>[
      Expanded(
        flex: 6,
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 16),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Center(
                child: Text(
                  AppLocalizations.of(viewService.context)
                      .text("sign_up.setup_passcode_for_offline_access"),
                  textAlign: TextAlign.center,
                  style: textStylePrimary,
                ),
              ),
              Center(
                child: Text(
                  AppLocalizations.of(viewService.context)
                      .text("sign_up.please_enter_your_new_passcode"),
                  textAlign: TextAlign.center,
                  style: textStylePrimary,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 48),
                child: OtpWidget(
                  controller: state.passcode,
                  maxLength: 6,
                  isSecured: true,
                  textWarning: state.textWarning,
                  isCheckField: state.isCheckField,
                ),
              ),
            ],
          ),
        ),
      ),
    ],
  );
}

import 'dart:io';

import 'package:flutter/material.dart' hide Action, Page;

import 'package:fish_redux/fish_redux.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:tcga_flutter/routes.dart';

import '../manager.dart';

import 'action.dart';
import 'state.dart';
import 'validator.dart';

final _authManager = AuthManager();
String _deviceInfo;
Effect<PageState> buildEffect() {
  return combineEffects(<Object, Effect<PageState>>{
    Lifecycle.initState: _initState,
    PageAction.goHome: _goHome,
    PageAction.validateAndSubmit: _validateAndSubmit,
    PageAction.onVerifiedSuccessfully: _onVerifiedSuccessfully,
  });
}

void _initState(Action action, Context<PageState> ctx) {
  Map<String, String> map = Map<String, String>();
  map['apiKey'] = _authManager.apiKey;
  map['apiSecret'] = _authManager.apiSecret;
  ctx.dispatch(PageActionCreator.init(map));
}

Future _goHome(Action action, Context<PageState> ctx) async {
  await _pushToHomePage(ctx.context);
}

void _onVerifiedSuccessfully(Action action, Context<PageState> ctx) async {
  final token = action.payload;
  final _ = await ctx.dispatch(PageActionCreator.recaptchav2(token));
  if (token != null) ctx.dispatch(PageActionCreator.validateAndSubmit());
}

Future _validateAndSubmit(Action action, Context<PageState> ctx) async {
  final state = ctx.state;
  var isValid = Validator.isValid(state);
  ctx.dispatch(PageActionCreator.checkField());
  if (!isValid) {
    ctx.dispatch(PageActionCreator.confirm());
    return;
  }
  if (_deviceInfo == null) {
    if (Platform.isAndroid) {
      var info = await _authManager.getInfoAndroid();
      _deviceInfo =
          '${info.model}/Android ${info.version.release} sdk ${info.version.sdkInt}';
    } else if (Platform.isIOS) {
      var info = await _authManager.getInfoIOS();
      _deviceInfo = '${info.name}/${info.systemName} ${info.systemVersion}';
    }
  }
  if (state.recaptchaToken.isEmpty) {
    ctx.dispatch(PageActionCreator.onVerifiedSuccessfully(null));
    return;
  }

  switch (state.step) {
    case SignUpStep.General:
      bool reiontion = await _authManager.regiontions(
        email: state.emailAddress.text,
        firstName: state.firstName.text,
        lastName: state.lastName.text,
        recaptchaResponse: state.recaptchaToken,
        userAgent: _deviceInfo,
      );
      if (!reiontion) {
        ctx.dispatch(PageActionCreator.emailWarningExisted());
        return;
      }
      break;
    case SignUpStep.EmailAddressVerification:
      {
        final isValidOtp = await _authManager.validateOTP(
          id: _authManager.currentCaregiverId,
          emailAddress: state.emailAddress.text,
          registrationCode: state.otp.text,
          password: state.password.text,
          userAgent: _deviceInfo,
        );

        if (!isValidOtp) {
          ctx.dispatch(PageActionCreator.confirm());
          return;
        }

        final signUpOk = await _authManager.signUp(
          id: _authManager.currentCaregiverId,
          firstName: state.firstName.text,
          lastName: state.lastName.text,
          emailAddress: state.emailAddress.text,
          emailOtp: state.otp.text,
          password: state.password.text,
          isHealthcareProfessional: state.isProfessionalHealthCareProvider,
        );

        if (!signUpOk) {
          //TODO show a snackbar msg
          return;
        }
      }
      break;
    case SignUpStep.PasscodeVerification:
      {
        final savePasscodeOk = await _authManager.savePasscode(
          state.passcode.text,
        );

        if (!savePasscodeOk) {
          //TODO show a snackbar msg
          return;
        }
      }
      break;
    default:
      break;
  }
  ctx.dispatch(PageActionCreator.next());
  return;
}

Future _pushToHomePage(BuildContext context) async {
  await Navigator.of(context).pushReplacement(
    PageRouteBuilder(
      pageBuilder: (_, __, ___) {
        return routes.buildPage(
          'bottomBar',
          null,
        );
      },
      settings: RouteSettings(name: 'bottomBar'),
    ),
  );
}

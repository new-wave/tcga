import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';

import '../state.dart';

Widget buildView(
  PageState state,
  Dispatch dispatch,
  ViewService viewService,
) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.stretch,
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      Center(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(bottom: 45),
              child: SvgPicture.asset(
                "assets/icons/success.svg",
                color: colorSecondary,
              ),
            ),
            Text(
              AppLocalizations.of(viewService.context).text("sign_up.congrats"),
              style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.w400,
              ),
            ),
            Text(
              AppLocalizations.of(viewService.context).text("sign_up.you_are_on"),
              style: TextStyle(
                fontSize: 14,
                height: 2.5,
              ),
            ),
          ],
        ),
      ),
    ],
  );
}

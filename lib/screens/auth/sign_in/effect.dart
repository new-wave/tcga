import 'dart:io';

import 'package:flutter/material.dart' hide Action, Page;

import 'package:fish_redux/fish_redux.dart';
import 'package:flutter_appauth/flutter_appauth.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:local_auth/local_auth.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:tcga_flutter/routes.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/utils/confirm_dialog.dart';
import 'package:tcga_flutter/utils/show_picker.dart';

import '../../../route_name.dart';
import '../manager.dart';

import 'action.dart';
import 'state.dart';

final _authManager = AuthManager();

Effect<SignInState> buildEffect() {
  return combineEffects(<Object, Effect<SignInState>>{
    LoginAction.action: _onAction,
    LoginAction.signIn: _onLogin,
    LoginAction.signUp: _onSignUp,
    LoginAction.checkFingerprint: _checkFingerprint,
    Lifecycle.initState: _onInit,
  });
}

void _onAction(Action action, Context<SignInState> ctx) {
  FocusScopeNode currentFocus = FocusScope.of(ctx.context);
  if (!currentFocus.hasPrimaryFocus) {
    currentFocus.unfocus();
  }
}

void _checkFingerprint(Action action, Context<SignInState> ctx) async {
  ctx.state.checkBio = await ctx.state.auth.canCheckBiometrics;

  if (ctx.state.checkBio) {
    ctx.state.listBiometricType = await ctx.state.auth.getAvailableBiometrics();
    if (ctx.state.listBiometricType.isEmpty) {
      await showModal2(
        ctx.context,
        RouteName.checkFingerprint,
        args: null,
      );
    }

    if (Platform.isIOS) {
      if (ctx.state.listBiometricType.contains(BiometricType.face)) {
        bool authenticated = await ctx.state.auth.authenticateWithBiometrics(
          localizedReason:
              AppLocalizations.of(ctx.context).text("login.bio_enable_face"),
          useErrorDialogs: true,
          stickyAuth: true,
          sensitiveTransaction: true,
        );
        if (authenticated) {
          _checkLogin(action, ctx);
        }
      }
      if (ctx.state.listBiometricType.contains(BiometricType.fingerprint)) {
        bool authenticated = await ctx.state.auth.authenticateWithBiometrics(
          localizedReason:
              AppLocalizations.of(ctx.context).text("login.bio_enable_touch"),
          useErrorDialogs: true,
        );
        if (authenticated) {
          _checkLogin(action, ctx);
        }
      }
    }
    if (Platform.isAndroid) {
      bool authenticated = await ctx.state.auth.authenticateWithBiometrics(
        localizedReason:
            AppLocalizations.of(ctx.context).text("login.bio_enable"),
        useErrorDialogs: true,
      );
      if (authenticated) {
        _checkLogin(action, ctx);
      }
    }
  }
}

void _checkLogin(Action action, Context<SignInState> ctx) async {
  var isOk = await _authManager.signIn();

  if (isOk) {
    SharedPreferences.getInstance().then(
      (_p) {
        _p.setBool('firstStart', true);
      },
    );
    await _pushToHomePage(ctx.context);
  }
}

Future _onSignUp(Action action, Context<SignInState> ctx) async {
  await Navigator.of(ctx.context).push(
    PageRouteBuilder(
      pageBuilder: (_, __, ___) {
        return routes.buildPage(
          'sign-up',
          null,
        );
      },
      settings: RouteSettings(name: 'sign-up'),
    ),
  );
}

void _onInit(Action action, Context<SignInState> ctx) async {
  ctx.state.pageController = PageController();

  SharedPreferences.getInstance().then((_p) async {
    final idToken = _p.getString('token');
    final isFirst = _p.getBool('isFirst');
    print('_isFirst = ${_p.getBool('firstStart')}');
    print('token = ${_p.getString('token')}');
    if (idToken != null) {
      await _pushToHomePage(ctx.context);
    } else {
      ctx.dispatch(LoginActionCreator.setIsFirst(isFirst));
    }
  });
}

void _onLogin(Action action, Context<SignInState> ctx) async {
  EasyLoading.show(maskType: EasyLoadingMaskType.black);
  final AuthorizationTokenResponse result = await _authManager.appAuth
      .authorizeAndExchangeCode(
    AuthorizationTokenRequest(
      _authManager.CLIENT_ID,
      _authManager.REDIRECT_URI,
      serviceConfiguration: AuthorizationServiceConfiguration(
        _authManager.AUTHORIZATION_ENDPOINT,
        _authManager.TOKEN_ENDPOINT,
      ),
      scopes: [
        'openid',
        'profile',
        'email',
        'offline_access',
      ],
    ),
  )
      .catchError((error, stackTrace) {
    EasyLoading.dismiss();
    print("AuthorizeAndExchangeCode Error: $error");
  });

  if (result != null) {
    EasyLoading.dismiss();
    if (result.idToken.isNotEmpty) {
      print("idToken: ${result.idToken}");
      var isOk = false;
      isOk = await _authManager.signIn();
      if (isOk) {
        SharedPreferences.getInstance().then(
          (_p) {
            _p.setBool('firstStart', true);
            _p.setString('token', result.idToken);
            _p.setString('refresh_token', result.refreshToken);
          },
        );
        await _pushToHomePage(ctx.context);
      }
    } else {
      showErrorDialog(
        ctx.context,
        title: AppLocalizations.instance.text('common.error'),
        description:
            AppLocalizations.instance.text('login.missing_username_password'),
      );
    }
  } else {
    EasyLoading.dismiss();
  }
}

Future _pushToHomePage(BuildContext context) async {
  await Navigator.of(context).pushReplacement(
    PageRouteBuilder(
      pageBuilder: (_, __, ___) {
        return routes.buildPage(
          'bottomBar',
          null,
        );
      },
      settings: RouteSettings(name: 'bottomBar'),
    ),
  );
}

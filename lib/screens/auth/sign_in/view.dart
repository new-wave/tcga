import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/widget/import.dart';

import 'action.dart';
import 'state.dart';

Widget buildView(
    SignInState state, Dispatch dispatch, ViewService viewService) {
  final node = FocusScope.of(viewService.context);
  return GestureDetector(
    onTap: () {
      dispatch(LoginActionCreator.action());
    },
    child: Scaffold(
      //resizeToAvoidBottomPadding: false,
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      body: Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset(
                  "assets/images/ic_logo.png",
                ),
                Text(
                  AppLocalizations.of(viewService.context)
                      .text("login.the_caregiver_app"),
                  style: TextStyle(
                    fontSize: 24,
                    color: colorSecondary,
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 24),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Visibility(
                    visible: false,
                    child: EntryWidget(
                      controller: state.emailAddress,
                      hint: AppLocalizations.of(viewService.context)
                          .text("login.user_name"),
                      hintStyle: textStylePrimary,
                      keyboardType: TextInputType.text,
                      onEditingComplete: () => node.nextFocus(),
                    ),
                  ),
                  Spacer(
                    flex: 1,
                  ),
                  Visibility(
                    visible: false,
                    child: EntryWidget(
                      controller: state.password,
                      hint: AppLocalizations.of(viewService.context)
                          .text("login.password"),
                      hintStyle: textStylePrimary,
                      hideText: true,
                      keyboardType: TextInputType.text,
                      textInputAction: TextInputAction.done,
                      onEditingComplete: () {
                        node.unfocus();
                        dispatch(LoginActionCreator.signIn());
                      },
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 30),
                    height: 45,
                    width: double.infinity,
                    child: ButtonWidget(
                      onPress: () {
                        dispatch(LoginActionCreator.signIn());
                      },
                      colorButton: colorPrimary,
                      textButton: AppLocalizations.of(viewService.context)
                          .text("login.login_button"),
                    ),
                  ),
                  Center(
                    child: Container(
                      margin: edgeTopInsetsPrimary,
                      child: Text(
                        AppLocalizations.of(viewService.context)
                            .text("login.forgot_password"),
                        style:
                            TextStyle(fontSize: 14, color: Color(0xffE13B3B)),
                      ),
                    ),
                  ),
                  Spacer(
                    flex: 3,
                  ),
                  GestureDetector(
                    onTap: () {
                      dispatch(LoginActionCreator.checkFingerprint());
                    },
                    child: Center(
                      child: Image.asset(
                        "assets/images/ic_fingerprint.png",
                        height: 50,
                        width: 50,
                      ),
                    ),
                  ),
                  Spacer(
                    flex: 15,
                  ),
                  Container(
                    margin: EdgeInsets.only(bottom: 16),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(AppLocalizations.of(viewService.context)
                            .text("login.you_dont_have_an_account")),
                        GestureDetector(
                          onTap: () {
                            dispatch(LoginActionCreator.signUp());
                          },
                          child: Text(
                            AppLocalizations.of(viewService.context)
                                .text("sign_up.sign_up"),
                            style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.bold,
                              color: Color(0xffE13B3B),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    ),
  );
}

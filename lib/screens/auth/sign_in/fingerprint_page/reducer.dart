import 'package:fish_redux/fish_redux.dart';

import 'action.dart';
import 'state.dart';

Reducer<FingerprintState> buildReducer() {
  return asReducer(
    <Object, Reducer<FingerprintState>>{
      FingerprintAction.action: _onAction,
    },
  );
}

FingerprintState _onAction(FingerprintState state, Action action) {
  final FingerprintState newState = state.clone();
  return newState;
}

import 'package:fish_redux/fish_redux.dart';

//TODO replace with your own action
enum FingerprintAction { action, cancel, moveToHome }

class FingerprintActionCreator {
  static Action onAction() {
    return const Action(FingerprintAction.action);
  }

  static Action onCancel() {
    return const Action(FingerprintAction.cancel);
  }

  static Action onMoveToHome() {
    return const Action(FingerprintAction.moveToHome);
  }
}

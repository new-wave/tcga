import 'package:fish_redux/fish_redux.dart';
import 'action.dart';
import 'state.dart';

Effect<FingerprintState> buildEffect() {
  return combineEffects(<Object, Effect<FingerprintState>>{
    FingerprintAction.action: _onAction,
    FingerprintAction.cancel: _onCancel,
    FingerprintAction.moveToHome: _onMoveToHome,
  });
}

void _onAction(Action action, Context<FingerprintState> ctx) {}

void _onCancel(Action action, Context<FingerprintState> ctx) async {
  // await Navigator.of(ctx.context).
}

void _onMoveToHome(Action action, Context<FingerprintState> ctx) {}

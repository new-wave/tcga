import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';

import 'action.dart';
import 'state.dart';

Widget buildView(
    FingerprintState state, Dispatch dispatch, ViewService viewService) {
  return Container(
    height: 300,
    padding: edgeAllInsetsPrimary,
    color: Colors.white,
    child: Column(
      children: <Widget>[
        Image.asset(
          "assets/images/ic_fingerprint.png",
          height: 50,
          width: 50,
        ),
        GestureDetector(
          onTap: () {},
          child: Text(
            AppLocalizations.of(viewService.context).text("login.bio-errors"),
            style: textStylePrimary,
          ),
        )
      ],
    ),
  );
}

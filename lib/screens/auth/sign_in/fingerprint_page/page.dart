import 'package:fish_redux/fish_redux.dart';

import 'effect.dart';
import 'reducer.dart';
import 'state.dart';
import 'view.dart';

class FingerprintPage extends Page<FingerprintState, Map<String, dynamic>> {
  FingerprintPage()
      : super(
            initState: initState,
            effect: buildEffect(),
            reducer: buildReducer(),
            view: buildView,
            dependencies: Dependencies<FingerprintState>(
                adapter: null,
                slots: <String, Dependent<FingerprintState>>{
                }),
            middleware: <Middleware<FingerprintState>>[
            ],);

}

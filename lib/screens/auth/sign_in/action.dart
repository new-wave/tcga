import 'package:fish_redux/fish_redux.dart';

enum LoginAction { action, signIn, setIsFirst, signUp, checkFingerprint }

class LoginActionCreator {
  static Action action() {
    return const Action(LoginAction.action);
  }

  static Action signIn() {
    return const Action(LoginAction.signIn);
  }

  static Action signUp() {
    return const Action(LoginAction.signUp);
  }

  static Action setIsFirst(bool isFirst) {
    return Action(LoginAction.setIsFirst, payload: isFirst);
  }

  static Action checkFingerprint() {
    return const Action(LoginAction.checkFingerprint);
  }
}

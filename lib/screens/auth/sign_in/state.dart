import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:local_auth/local_auth.dart';

import 'fingerprint_page/state.dart';

class SignInState implements Cloneable<SignInState> {
  TextEditingController emailAddress;
  TextEditingController password;
  PageController pageController;
  FingerprintState fingerprint;
  bool isFirstTime;
  bool checkBio;
  LocalAuthentication auth = LocalAuthentication();
  List<BiometricType> listBiometricType = List<BiometricType>();

  SignInState clone() {
    return SignInState()
      ..emailAddress = emailAddress
      ..pageController = pageController
      ..isFirstTime = isFirstTime
      ..password = password
      ..fingerprint = fingerprint
      ..auth = auth
      ..checkBio = checkBio
      ..listBiometricType = listBiometricType;
  }
}

SignInState initState(Map<String, dynamic> args) {
  return SignInState()
    ..password = TextEditingController(
   //   text: 'Aa@12345',
      text: '',
    )
    ..emailAddress = TextEditingController(
  //    text: 'tcga@nx.net',
      text: '',
    )
    ..fingerprint
    ..auth
    ..checkBio
    ..listBiometricType;
}

class FingerprintStateConnector extends ConnOp<SignInState, FingerprintState> {
  @override
  FingerprintState get(SignInState state) => state.fingerprint.clone();

  @override
  void set(SignInState state, FingerprintState subState) {
    state.fingerprint = subState.clone();
  }
}

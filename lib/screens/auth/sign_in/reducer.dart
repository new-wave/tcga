import 'package:fish_redux/fish_redux.dart';
import 'action.dart';
import 'state.dart';

Reducer<SignInState> buildReducer() {
  return asReducer(
    <Object, Reducer<SignInState>>{
      //LoginAction.action: _onAction,
      LoginAction.setIsFirst: _setIsFirst,
    },
  );
}

SignInState _onAction(SignInState state, Action action) {
  final SignInState newState = state.clone();
  return newState;
}

SignInState _setIsFirst(SignInState state, Action action) {
  final bool _isFirst = action.payload;
  final SignInState newState = state.clone();
  newState.isFirstTime = _isFirst;
  return newState;
}

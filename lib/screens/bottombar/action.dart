import 'package:fish_redux/fish_redux.dart';

enum BottomBarAction { action, tabchanged }

class BottomBarActionCreator {
  static Action onAction() {
    return const Action(BottomBarAction.action);
  }

  static Action onTabChanged(int index) {
    return Action(BottomBarAction.tabchanged, payload: index);
  }
}

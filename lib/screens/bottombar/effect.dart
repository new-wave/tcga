import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/BottomBar/action.dart';
import 'package:tcga_flutter/screens/BottomBar/state.dart';

Effect<BottomBarState> buildEffect() {
  return combineEffects(<Object, Effect<BottomBarState>>{
    BottomBarAction.action: _onAction,
    Lifecycle.initState: _onInit,
  });
}

void _onAction(Action action, Context<BottomBarState> ctx) {}

void _onInit(Action action, Context<BottomBarState> ctx) {}

import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/BottomBar/effect.dart';
import 'package:tcga_flutter/screens/BottomBar/reducer.dart';
import 'package:tcga_flutter/screens/BottomBar/state.dart';
import 'package:tcga_flutter/screens/BottomBar/view.dart';

class MainPage extends Page<BottomBarState, Map<String, dynamic>> {
  MainPage()
      : super(
          initState: initState,
          effect: buildEffect(),
          reducer: buildReducer(),
          view: buildView,
          // dependencies: Dependencies<BottomBarState>(
          //     adapter: null, slots: <String, Dependent<BottomBarState>>{}),
          // middleware: <Middleware<BottomBarState>>[],
        );
}

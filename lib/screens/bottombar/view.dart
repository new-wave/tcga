import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/config/style.dart';
import 'package:tcga_flutter/screens/BottomBar/action.dart';
import 'package:tcga_flutter/screens/BottomBar/state.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/widget/keep_alive_widget.dart';

Widget buildView(
    BottomBarState state, Dispatch dispatch, ViewService viewService) {
  return Builder(
    builder: (context) {
      final pageController = PageController();

      Widget _buildPage(Widget page) {
        return KeepAliveWidget(page);
      }

      return Scaffold(
        key: state.scaffoldKey,
        body: PageView(
          physics: NeverScrollableScrollPhysics(),
          children: state.pages.map(_buildPage).toList(),
          controller: pageController,
          onPageChanged: (int i) =>
              dispatch(BottomBarActionCreator.onTabChanged(i)),
        ),
        bottomNavigationBar: BottomNavigationBar(
          items: [
            BottomNavigationBarItem(
              icon: Container(
                margin: EdgeInsets.only(bottom: 7),
                child: SvgPicture.asset("assets/icons/home.svg"),
              ),
              activeIcon: Container(
                margin: EdgeInsets.only(bottom: 7),
                child: SvgPicture.asset("assets/icons/ic_home_selected.svg"),
              ),
              label: AppLocalizations.of(context).text('home.bottombar.home'),
            ),
            BottomNavigationBarItem(
              icon: Container(
                margin: EdgeInsets.only(bottom: 7),
                child: SvgPicture.asset(
                  "assets/icons/recipient.svg",
                  color: Colors.white,
                ),
              ),
              activeIcon: Container(
                margin: EdgeInsets.only(bottom: 7),
                child: SvgPicture.asset(
                  "assets/icons/ic_recipients_selected.svg",
                  color: Colors.white,
                ),
              ),
              label: AppLocalizations.of(context).text('home.bottombar.recipients'),
            ),
            BottomNavigationBarItem(
              icon: Container(
                margin: EdgeInsets.only(bottom: 7),
                child: SvgPicture.asset("assets/icons/calendar.svg"),
              ),
              activeIcon: Container(
                margin: EdgeInsets.only(bottom: 7),
                child:
                    SvgPicture.asset("assets/icons/ic_calendar_selected.svg"),
              ),
              label: AppLocalizations.of(context).text('home.bottombar.calendar'),
            ),
            BottomNavigationBarItem(
              icon: Container(
                margin: EdgeInsets.only(bottom: 7),
                child: SvgPicture.asset("assets/icons/ic_note.svg"),
              ),
              activeIcon: Container(
                margin: EdgeInsets.only(bottom: 7),
                child: SvgPicture.asset("assets/icons/ic_note_selected.svg"),
              ),
              label: AppLocalizations.of(context).text('home.bottombar.notes'),
            ),
            BottomNavigationBarItem(
              icon: Container(
                margin: EdgeInsets.only(bottom: 7),
                child: SvgPicture.asset("assets/icons/alert.svg"),
              ),
              activeIcon: Container(
                margin: EdgeInsets.only(bottom: 7),
                child: SvgPicture.asset("assets/icons/ic_alert_selected.svg"),
              ),
              label: AppLocalizations.of(context).text('home.bottombar.alert'),
            ),
          ],
          selectedLabelStyle: Style.labelIconBottomBar,
          backgroundColor: colorPrimary,
          selectedItemColor: Colors.white,
          unselectedItemColor: Colors.white,
          currentIndex: state.selectedIndex,
          onTap: (int index) {
            pageController.jumpToPage(index);
          },
          type: BottomNavigationBarType.fixed,
        ),
      );
    },
  );
}

import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/BottomBar/action.dart';
import 'package:tcga_flutter/screens/BottomBar/state.dart';

Reducer<BottomBarState> buildReducer() {
  return asReducer(
    <Object, Reducer<BottomBarState>>{
      BottomBarAction.action: _onAction,
      BottomBarAction.tabchanged: _onTabChanged
    },
  );
}

BottomBarState _onAction(BottomBarState state, Action action) {
  final BottomBarState newState = state.clone();
  return newState;
}

BottomBarState _onTabChanged(BottomBarState state, Action action) {
  final int newindex = action.payload ?? 0;
  final BottomBarState newState = state.clone();
  newState.selectedIndex = newindex;
  return newState;
}

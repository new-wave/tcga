import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';

import '../../route_name.dart';
import '../../routes.dart';

class BottomBarState implements Cloneable<BottomBarState> {
  int selectedIndex = 0;
  List<Widget> pages;
  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  BottomBarState clone() {
    return BottomBarState()
      ..pages = pages
      ..scaffoldKey = scaffoldKey
      ..selectedIndex = selectedIndex;
  }
}

BottomBarState initState(Map<String, dynamic> args) {
  return BottomBarState()
    ..pages = List<Widget>.unmodifiable(
      [
        routes.buildPage(RouteName.home, null),
        routes.buildPage(RouteName.recipients, null),
        routes.buildPage(RouteName.calendar, null),
        routes.buildPage(RouteName.notes, null),
        routes.buildPage(RouteName.alert, null)
      ],
    );
}

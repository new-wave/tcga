import 'package:tcga_flutter/database/models.dart';
import 'package:tcga_flutter/database/models/enums.dart';

import 'allergy_editor/state.dart';
import 'allergies_page/allergy_item_component/state.dart';

extension EntityToStateMapper on Allergy {
  AllergyItemState toState() => AllergyItemState(
        id: this.id,
        criticality: this.criticality != null
            ? AllergyCriticality.values[this.criticality]
            : null,
        code: this.code,
        title: this.name,
        lastOccuredDate: this.lastOccuredDate,
        category: this.category != null
            ? AllergyCategory.values[this.category]
            : null,
        type:
            this.type != null ? AllergyIntoleranceType.values[this.type] : null,
      );
}

extension StateToEntityMapper on AllergyEditorState {
  Allergy toEntity() => Allergy(
        id: this.id,
        criticality: this.criticality.index,
        code: this.allergy.code,
        name: this.allergy.display,
        personId: this.recipientId,
        lastOccuredDate: this.lastOccuredDate,
        category: this.category.index,
        type: this.type.index,
      );
}

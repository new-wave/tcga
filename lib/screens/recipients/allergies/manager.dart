import 'package:tcga_flutter/screens/recipients/allergies/allergy_editor/state.dart';

import '../../../database/models.dart';
import '../../../database/repository.dart';

import 'allergies_page/allergy_item_component/state.dart';
import 'mapper.dart';

class AllergyManager {
  static final _singleton = AllergyManager._internal();

  factory AllergyManager() {
    return _singleton;
  }

  AllergyManager._internal();

  final _allergyRepository = Repository<Allergy>();

  Future<List<AllergyItemState>> query({
    int pageSize = 100,
    String recipientId,
  }) async {
    final items = await _allergyRepository.query(
        filter: (x) => x.personId == recipientId);

    return items.map((x) => x.toState()).toList();
  }

  Future<bool> delete(String id) {
    return _allergyRepository.delete(id);
  }

  Future<AllergyItemState> update(AllergyEditorState state) async {
    final entity = state.toEntity();

    final _ = await _allergyRepository.update(entity);

    return entity.toState();
  }

  Future<AllergyItemState> create(AllergyEditorState state) async {
    final entity = state.toEntity();

    entity.id = await _allergyRepository.create(entity);

    return entity.toState();
  }
}

import 'package:tcga_flutter/utils/app_localizations.dart';

import '../manager.dart';
import 'state.dart';

final _manager = AllergyManager();

class Validator {
  static bool isValid(AllergyEditorState state) {
    if (state.titleEditController.text == '') return false;
    if (state.type == null) return false;

    if (state.category == null) return false;

    if (state.criticality == null) return false;

    if (state.lastOccuredDate == null) return false;

    return true;
  }

  static Future<bool> checkCondition(AllergyEditorState state) async {
    var items = (await _manager.query(recipientId: state.recipientId)).where(
        (element) =>
            element.code == state.allergy.code && element.id != state.id);

    if (items.length > 0) return false;

    return true;
  }
}

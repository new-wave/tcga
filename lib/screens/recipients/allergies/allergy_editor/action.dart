import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/valuesets/valueset_picker_page/valueset_item_component/state.dart';

enum AllergyEditorAction {
  save,
  delete,
  selectAllergy,
  handleAllergySelected,
  handleAllergyTypeSelected,
  handleLastOcurredDateSelected,
  toggleIsPrimary,
  handleAllergyCategorySelected,
  handleAllergyCriticalitySelected,
  checkValidator,
  checkField,
}

class AllergyEditorActionCreator {
  static Action save() {
    return const Action(
      AllergyEditorAction.save,
    );
  }

  static Action checkField() {
    return const Action(
      AllergyEditorAction.checkField,
    );
  }

  static Action checkValidator() {
    return const Action(
      AllergyEditorAction.checkValidator,
    );
  }

  static Action delete() {
    return const Action(
      AllergyEditorAction.delete,
    );
  }

  static Action selectAllergy() {
    return const Action(
      AllergyEditorAction.selectAllergy,
    );
  }

  static Action handleAllergySelected(ValuesetItemState item) {
    return Action(
      AllergyEditorAction.handleAllergySelected,
      payload: item,
    );
  }

  static Action handleAllergyTypeSelected(int index) {
    return Action(
      AllergyEditorAction.handleAllergyTypeSelected,
      payload: index,
    );
  }

  static Action handleLastOcurredDateSelected(DateTime date) {
    return Action(
      AllergyEditorAction.handleLastOcurredDateSelected,
      payload: date,
    );
  }

  static Action toggleIsPrimary() {
    return Action(
      AllergyEditorAction.toggleIsPrimary,
    );
  }

  static Action handleAllergyCategorySelected(int index) {
    return Action(
      AllergyEditorAction.handleAllergyCategorySelected,
      payload: index,
    );
  }

  static Action handleAllergyCriticalitySelected(int index) {
    return Action(
      AllergyEditorAction.handleAllergyCriticalitySelected,
      payload: index,
    );
  }
}

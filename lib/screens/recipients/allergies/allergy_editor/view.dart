import 'package:flutter/material.dart';

import 'package:fish_redux/fish_redux.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/widget/import.dart';
import 'package:tcga_flutter/widget/selectable_options_widget.dart';

import 'action.dart';
import 'state.dart';
import 'validator.dart';

Widget buildView(
  AllergyEditorState state,
  Dispatch dispatch,
  ViewService viewService,
) {
  return Builder(builder: (context) {
    return Scaffold(
      key: state.scaffoldKey,
      backgroundColor: Colors.white,
      appBar: _appBar(state, viewService, dispatch),
      body: Padding(
        padding: edgeInsetsBigTop,
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                  AppLocalizations.of(viewService.context)
                      .text('allergies.editor.name'),
                  style: textStylePrimary),
              Container(
                margin: EdgeInsets.symmetric(vertical: spacingSm),
                child: PickerWidget(
                  tap: () => dispatch(
                    AllergyEditorActionCreator.selectAllergy(),
                  ),
                  hint: AppLocalizations.of(viewService.context)
                      .text('allergies.editor.select_allergy'),
                  controller: state.titleEditController,
                  suffixIcon: SvgPicture.asset(
                    'assets/icons/ic_search.svg',
                    color: colorPrimary,
                    fit: BoxFit.none,
                  ),
                  warningText: AppLocalizations.instance.text('common.validator.allergy_name'),
                  isRequired: true,
                  isCheckField: state.isCheckField ?? false,
                ),
              ),
              Container(
                margin: edgeOnlyTopBotPrimaryLarge,
                child: Text(
                  AppLocalizations.of(viewService.context)
                      .text('allergies.editor.allergy_type'),
                  style: textStylePrimary,
                ),
              ),
              SelectableOptionsWidget(
                options: state.allergyTypes.values.toList(),
                initialIndex: state.type?.index,
                indexSelected: (index) => dispatch(
                  AllergyEditorActionCreator.handleAllergyTypeSelected(
                    index,
                  ),
                ),
              ),
              Container(
                margin: edgeOnlyTopBotPrimaryLarge,
                child: Text(
                  AppLocalizations.of(viewService.context)
                      .text('allergies.editor.category'),
                  style: textStylePrimary,
                ),
              ),
              SelectableOptionsWidget(
                options: state.allegeryCategories.values.toList(),
                initialIndex: state.category?.index,
                indexSelected: (index) => dispatch(
                  AllergyEditorActionCreator.handleAllergyCategorySelected(
                    index,
                  ),
                ),
              ),
              Container(
                margin: edgeOnlyTopBotPrimaryLarge,
                child: Text(
                  AppLocalizations.of(viewService.context)
                      .text('allergies.editor.criticality'),
                  style: textStylePrimary,
                ),
              ),
              SelectableOptionsWidget(
                options: state.allergyCriticalities.values.toList(),
                initialIndex: state.criticality?.index,
                indexSelected: (index) => dispatch(
                  AllergyEditorActionCreator.handleAllergyCriticalitySelected(
                    index,
                  ),
                ),
              ),
              Container(
                margin: edgeOnlyTopBotPrimaryMini,
                child: Text(
                  AppLocalizations.of(viewService.context)
                      .text('allergies.editor.last_occured_date'),
                  style: textStylePrimary,
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 8),
                child: DatePickerWidget(
                  initial: state.lastOccuredDate,
                  max: DateTime.now(),
                  min: DateTime.now().subtract(
                    Duration(days: 30),
                  ),
                  dateSelected: (date) => dispatch(
                    AllergyEditorActionCreator.handleLastOcurredDateSelected(
                      date,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: Builder(
        builder: (context) => Container(
          decoration: BoxDecoration(
              color: colorPrimary, border: Border.all(color: colorPrimary)),
          child: ButtonWidget(
            colorButton: colorPrimary,
            textButton:
            AppLocalizations.of(viewService.context).text('common.save'),
            onPress: () {
              dispatch(AllergyEditorActionCreator.checkValidator());
            },
          ),
        ),
      ),
    );
  });
}

AppBar _appBar(
    AllergyEditorState state, ViewService viewService, Dispatch dispatch) {
  return AppBar(
    elevation: 3,
    centerTitle: false,
    title: Text(
      state.title,
    ),
    leading: GestureDetector(
      onTap: () => Navigator.pop(viewService.context),
      child: Container(
        margin: EdgeInsets.only(left: 16),
        child: SvgPicture.asset('assets/icons/ic_arrow_left_circle.svg'),
      ),
    ),
    leadingWidth: 46,
    actions: [
      if (state.onEditMode)
        IconButton(
          iconSize: 24,
          icon: SvgPicture.asset('assets/icons/ic_trash.svg'),
          onPressed: () => dispatch(
            AllergyEditorActionCreator.delete(),
          ),
        )
    ],
  );
}

import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/valuesets/valueset_picker_page/valueset_item_component/state.dart';

import 'action.dart';
import 'state.dart';

Reducer<AllergyEditorState> buildReducer() {
  return asReducer(
    <Object, Reducer<AllergyEditorState>>{
      AllergyEditorAction.handleAllergySelected: _handleAllergySelected,
      AllergyEditorAction.handleAllergyTypeSelected: _handleAllergyTypeSelected,
      AllergyEditorAction.handleAllergyCriticalitySelected:
          _handleAllergyCriticalitySelected,
      AllergyEditorAction.handleLastOcurredDateSelected:
          _handleLastOcurredDateSelected,
      AllergyEditorAction.handleAllergyCategorySelected:
          _handleAllergyCategorySelected,
      AllergyEditorAction.checkField: _checkFieldReducer,
    },
  );
}

AllergyEditorState _checkFieldReducer(AllergyEditorState state, Action action) {
  final AllergyEditorState newState = state.clone();
  newState.isCheckField = true;
  return newState;
}

AllergyEditorState _handleAllergySelected(
  AllergyEditorState state,
  Action action,
) {
  final item = action.payload as ValuesetItemState;
  final newState = state.clone()..allergy = item;
  newState.titleEditController.text = item.display;
  return newState;
}

AllergyEditorState _handleAllergyTypeSelected(
  AllergyEditorState state,
  Action action,
) {
  final index = action.payload as int;
  final newState = state.clone()
    ..type = state.allergyTypes.keys.elementAt(index);
  return newState;
}

AllergyEditorState _handleAllergyCategorySelected(
  AllergyEditorState state,
  Action action,
) {
  final index = action.payload as int;
  final newState = state.clone()
    ..category = state.allegeryCategories.keys.elementAt(index);
  return newState;
}

AllergyEditorState _handleAllergyCriticalitySelected(
  AllergyEditorState state,
  Action action,
) {
  final index = action.payload as int;
  final newState = state.clone()
    ..criticality = state.allergyCriticalities.keys.elementAt(index);
  return newState;
}

AllergyEditorState _handleLastOcurredDateSelected(
  AllergyEditorState state,
  Action action,
) {
  final date = action.payload as DateTime;
  final newState = state.clone()..lastOccuredDate = date;
  return newState;
}

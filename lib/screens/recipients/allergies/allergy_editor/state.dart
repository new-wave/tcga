import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'package:fish_redux/fish_redux.dart';

import 'package:tcga_flutter/database/models/enums.dart';
import 'package:tcga_flutter/screens/valuesets/valueset_picker_page/valueset_item_component/state.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';

import '../allergies_page/allergy_item_component/state.dart';

class AllergyEditorState implements Cloneable<AllergyEditorState> {
  Map<AllergyIntoleranceType, String> get allergyTypes => {
        AllergyIntoleranceType.Allergy: AppLocalizations.instance
            .text('enums.allergy_intolerance_type.allergy'),
        AllergyIntoleranceType.Intolerance: AppLocalizations.instance
            .text('enums.allergy_intolerance_type.intolerance'),
      };

  Map<AllergyCategory, String> get allegeryCategories => {
        AllergyCategory.Food:
            AppLocalizations.instance.text('enums.allergy_category.food'),
        AllergyCategory.Medication:
            AppLocalizations.instance.text('enums.allergy_category.medication'),
        AllergyCategory.Environmental: AppLocalizations.instance
            .text('enums.allergy_category.environmental'),
        AllergyCategory.Biologic:
            AppLocalizations.instance.text('enums.allergy_category.biologic'),
      };

  Map<AllergyCriticality, String> get allergyCriticalities => {
        AllergyCriticality.Low:
            AppLocalizations.instance.text('enums.allergy_criticality.low'),
        AllergyCriticality.High:
            AppLocalizations.instance.text('enums.allergy_criticality.high'),
        AllergyCriticality.UnableToAssess: AppLocalizations.instance
            .text('enums.allergy_criticality.unable_to_assess'),
      };

  List<AllergyItemState> items;
  final scaffoldKey = GlobalKey<ScaffoldState>();
  String id;
  String recipientId;

  TextEditingController titleEditController;

  ValuesetItemState allergy;

  AllergyIntoleranceType type;
  AllergyCriticality criticality;
  AllergyCategory category;
  DateTime lastOccuredDate;

  bool isCheckField;

  get onEditMode => id != null;
  get title =>
      onEditMode ? AppLocalizations.instance.text('allergies.editor.appbar.title_edit') : AppLocalizations.instance.text('allergies.editor.appbar.title_new');

  @override
  AllergyEditorState clone() {
    return AllergyEditorState()
      ..id = id
      ..recipientId = recipientId
      ..allergy = allergy
      ..type = type
      ..criticality = criticality
      ..items = items
      ..category = category
      ..lastOccuredDate = lastOccuredDate
      ..isCheckField = isCheckField
      ..titleEditController = titleEditController;
  }
}

AllergyEditorState initState(Map<String, dynamic> arg) {
  final item = arg.containsKey('item') ? arg['item'] as AllergyItemState : null;
  final recipientId =
      arg.containsKey('recipient-id') ? arg['recipient-id'] as String : null;
  final items =
      arg.containsKey('items') ? arg['items'] as List<AllergyItemState> : null;
  final state = AllergyEditorState()
    ..allergy = ValuesetItemState(
      code: item?.code,
      display: item?.title,
    )
    ..id = item?.id
    ..recipientId = recipientId
    ..items = items
    ..criticality = item?.criticality ?? AllergyCriticality.Low
    ..lastOccuredDate = item?.lastOccuredDate ?? DateTime.now()
    ..category = item?.category ?? AllergyCategory.Food
    ..type = item?.type ?? AllergyIntoleranceType.Allergy
    ..isCheckField = false
    ..titleEditController = TextEditingController(text: item?.title);
  return state;
}

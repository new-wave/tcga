import 'package:fish_redux/fish_redux.dart';

import '../allergy_item_component/component.dart';
import '../state.dart';

class AllergiesAdapter extends SourceFlowAdapter<AllergiesState> {
  AllergiesAdapter()
      : super(
          pool: <String, Component<Object>>{
            'allergy-item': AllergyItemComponent()
          },
        );
}

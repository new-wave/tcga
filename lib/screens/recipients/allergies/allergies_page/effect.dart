import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/database/models/enums.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';

import '../../../../route_name.dart';
import '../../../../utils/utils.dart';

import '../manager.dart';
import 'action.dart';
import 'allergy_item_component/state.dart';
import 'state.dart';

final _manager = AllergyManager();

Effect<AllergiesState> buildEffect() {
  return combineEffects(<Object, Effect<AllergiesState>>{
    Lifecycle.initState: _init,
    AllergiesAction.create: _create,
    AllergiesAction.edit: _edit,
    AllergiesAction.delete: _delete,
    AllergiesAction.addNote: _addNote,
  });
}

Future _init(
  Action action,
  Context<AllergiesState> ctx,
) async {
  final items = await _manager.query(
    recipientId: ctx.state.recipient.id,
  );

  ctx.dispatch(
    AllergiesActionCreator.initialize(
      items,
    ),
  );
}

Future _create(Action action, Context<AllergiesState> ctx) async {
  final result = await ctx.navigateTo(
    RouteName.recipientsAllergyEdit,
    arguments: {
      'recipient-id': ctx.state.recipient.id,
      'items': ctx.state.items,
    },
  );

  if (result == null) return;

  ctx.dispatch(
    AllergiesActionCreator.handleAllergyCreated(
      result,
    ),
  );
}

Future _edit(Action action, Context<AllergiesState> ctx) async {
  final result = await ctx.navigateTo(
    RouteName.recipientsAllergyEdit,
    arguments: {
      'item': action.payload,
      'recipient-id': ctx.state.recipient.id,
      'items': ctx.state.items,
    },
  );

  if (result == null) return;

  if (result is AllergyItemState) {
    ctx.dispatch(
      AllergiesActionCreator.handleAllergyEdited(
        result,
      ),
    );
  } else {
    ctx.dispatch(
      AllergiesActionCreator.handleAllergyDeleted(
        action.payload.id,
      ),
    );
  }
}

Future _delete(Action action, Context<AllergiesState> ctx) async {
  final confirmed = await ctx.confirm(
    title: AppLocalizations.instance.text('common.warning.delete.title'),
    message: AppLocalizations.instance.text('common.warning.delete.desc'),
    negativeActionTitle: AppLocalizations.instance.text('common.cancel'),
    positiveActionTitle: AppLocalizations.instance.text('common.sure'),
  );

  if (!confirmed) return;

  final deletedItemId = action.payload.id;
  final deleted = await _manager.delete(deletedItemId);

  if (!deleted) {
    // TODO show error message
    return;
  }

  ctx.dispatch(
    AllergiesActionCreator.handleAllergyDeleted(
      deletedItemId,
    ),
  );
}

Future _addNote(Action action, Context<AllergiesState> ctx) {
  return ctx.navigateTo(
    RouteName.notesEditor,
    arguments: {
      'item-id': action.payload.id,
      'type': NoteType.AllergyIntolerance,
      'recipient-id': ctx.state.recipient.id,
    },
  );
}

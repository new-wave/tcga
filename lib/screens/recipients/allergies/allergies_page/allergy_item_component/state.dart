import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/database/models/enums.dart';

class AllergyItemState implements Cloneable<AllergyItemState> {
  String id;
  String title;
  String code;
  AllergyIntoleranceType type;
  AllergyCriticality criticality;
  AllergyCategory category;
  DateTime lastOccuredDate;

  AllergyItemState({
    this.id,
    this.title,
    this.code,
    this.criticality,
    this.category,
    this.lastOccuredDate,
    this.type,
  });

  @override
  AllergyItemState clone() {
    return AllergyItemState()
      ..id = id
      ..title = title
      ..code = code
      ..type = type
      ..criticality = criticality
      ..category = category
      ..lastOccuredDate = lastOccuredDate;
  }
}

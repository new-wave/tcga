import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';

import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/utils/enum_to_string.dart';
import 'package:tcga_flutter/utils/format_date.dart';
import 'package:tcga_flutter/widget/list_item_widget.dart';

import '../action.dart';
import 'state.dart';

Widget buildView(
  AllergyItemState state,
  Dispatch dispatch,
  ViewService viewService,
) {
  return SizedBox(
    height: 80,
    child: ListItemWidget(
      contextActions: [
        'recipients.popup.add_note',
        'recipients.popup.edit',
        'recipients.popup.delete',
      ],
      onTap: () => dispatch(
        AllergiesActionCreator.edit(
          state,
        ),
      ),
      onContextActionSelected: (String action) {
        switch (action) {
          case 'recipients.popup.add_note':
            {
              dispatch(
                AllergiesActionCreator.addNote(
                  state,
                ),
              );
            }
            break;
          case 'recipients.popup.edit':
            {
              dispatch(
                AllergiesActionCreator.edit(
                  state,
                ),
              );
            }
            break;
          case 'recipients.popup.delete':
            {
              dispatch(
                AllergiesActionCreator.delete(
                  state,
                ),
              );
            }
            break;
        }
      },
      contextPositioned: ContextPositioned.start,
      child: Container(
        margin: EdgeInsets.only(left: spacingPrimary),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              state.title,
              style: textStylePrimaryFontWeight,
              overflow: TextOverflow.ellipsis,
              maxLines: 1,
            ),
            Text(
              '• ${AppLocalizations.of(viewService.context).text('allergies.last_occurrence')}: ${dateFormatYMD(state.lastOccuredDate)}',
              style: textStylePrimaryGray,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  '${AppLocalizations.of(viewService.context).text('allergies.criticality')}: ${getEnumValue(state.criticality)}',
                  style: textStylePrimaryGray,
                ),
                Container(
                  child: Row(
                    children: [
                      Text(
                        getEnumValue(state.category),
                        style: textStyleMiniWeight,
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Text(
                        getEnumValue(state.type),
                        style: textStyleMiniWeight,
                      )
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    ),
  );
}

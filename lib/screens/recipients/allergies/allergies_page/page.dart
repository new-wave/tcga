import 'package:fish_redux/fish_redux.dart';

import '../../recipient_dashboard/recipient_header_component/component.dart';

import 'allergies_adapter/adapter.dart';
import 'effect.dart';
import 'state.dart';
import 'view.dart';
import 'reducer.dart';

class AllergiesPage extends Page<AllergiesState, Map<String, dynamic>> {
  AllergiesPage()
      : super(
          initState: initState,
          effect: buildEffect(),
          reducer: buildReducer(),
          view: buildView,
          dependencies: Dependencies<AllergiesState>(
            adapter: NoneConn<AllergiesState>() + AllergiesAdapter(),
            slots: <String, Dependent<AllergiesState>>{
              'recipient-header':
                  RecipientHeaderStateConnector() + RecipientHeaderComponent(),
            },
          ),
        );
}

import 'package:fish_redux/fish_redux.dart';

import 'action.dart';
import 'allergy_item_component/state.dart';
import 'state.dart';

Reducer<AllergiesState> buildReducer() {
  return asReducer(
    <Object, Reducer<AllergiesState>>{
      AllergiesAction.initialize: _initialize,
      AllergiesAction.handleAllergyCreated: _handleAllergyAdded,
      AllergiesAction.handleAllergyEdited: _handleAllergyEdited,
      AllergiesAction.handleAllergyDeleted: _handleAllergyDeleted,
    },
  );
}

AllergiesState _initialize(AllergiesState state, Action action) {
  final AllergiesState newState = state.clone();

  newState.items = action.payload as List<AllergyItemState> ?? [];

  return newState;
}

AllergiesState _handleAllergyAdded(AllergiesState state, Action action) {
  final item = action.payload as AllergyItemState;
  final newState = state.clone();

  newState.items.add(item);

  return newState;
}

AllergiesState _handleAllergyEdited(AllergiesState state, Action action) {
  final item = action.payload as AllergyItemState;
  final newState = state.clone();

  final editedIndex = newState.items.indexWhere(
    (x) => x.id == item.id,
  );

  newState.items[editedIndex] = item;

  return newState;
}

AllergiesState _handleAllergyDeleted(AllergiesState state, Action action) {
  final id = action.payload;
  final newState = state.clone();

  final deletedIndex = newState.items.indexWhere(
    (x) => x.id == id,
  );

  newState.items.removeAt(deletedIndex);

  return newState;
}

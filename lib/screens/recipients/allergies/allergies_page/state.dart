import 'package:fish_redux/fish_redux.dart';

import '../../recipient_dashboard/recipient_header_component/state.dart';
import '../../state.dart';

import 'allergy_item_component/state.dart';

class AllergiesState extends MutableSource
    implements Cloneable<AllergiesState> {
  List<AllergyItemState> items;
  RecipientItemState recipient;
  AllergyItemState primaryAllergy;

  @override
  AllergiesState clone() {
    return AllergiesState()
      ..items = items
      ..recipient = recipient
      ..primaryAllergy = primaryAllergy;
  }

  @override
  Object getItemData(int index) => items[index];

  @override
  String getItemType(int index) => 'allergy-item';

  @override
  int get itemCount => items?.length ?? 0;

  @override
  void setItemData(int index, Object data) => items[index] = data;
}

AllergiesState initState(Map<String, dynamic> arg) {
  return AllergiesState()..recipient = arg['recipient'];
}

class RecipientHeaderStateConnector
    extends ConnOp<AllergiesState, RecipientHeaderState> {
  @override
  RecipientHeaderState get(AllergiesState state) => RecipientHeaderState(
        recipient: state.recipient,
      );

  @override
  void set(AllergiesState state, RecipientHeaderState subState) {
    state.recipient = subState.recipient;
  }
}

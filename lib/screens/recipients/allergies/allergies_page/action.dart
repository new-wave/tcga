import 'package:fish_redux/fish_redux.dart';

import 'allergy_item_component/state.dart';

enum AllergiesAction {
  initialize,
  create,
  handleAllergyCreated,
  edit,
  handleAllergyEdited,
  delete,
  handleAllergyDeleted,
  addNote,
}

class AllergiesActionCreator {
  static Action initialize(List<AllergyItemState> items) {
    return Action(
      AllergiesAction.initialize,
      payload: items,
    );
  }

  static Action edit(AllergyItemState item) {
    return Action(
      AllergiesAction.edit,
      payload: item,
    );
  }

  static Action handleAllergyEdited(AllergyItemState item) {
    return Action(
      AllergiesAction.handleAllergyEdited,
      payload: item,
    );
  }

  static Action addNote(AllergyItemState item) {
    return Action(
      AllergiesAction.addNote,
      payload: item,
    );
  }

  static Action delete(AllergyItemState item) {
    return Action(
      AllergiesAction.delete,
      payload: item,
    );
  }

  static Action handleAllergyDeleted(String id) {
    return Action(
      AllergiesAction.handleAllergyDeleted,
      payload: id,
    );
  }

  static Action create(String recipientId) {
    return Action(
      AllergiesAction.create,
      payload: recipientId,
    );
  }

  static Action handleAllergyCreated(AllergyItemState item) {
    return Action(
      AllergiesAction.handleAllergyCreated,
      payload: item,
    );
  }
}

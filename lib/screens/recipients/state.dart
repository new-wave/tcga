import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/database/models/enums.dart';

class RecipientItemState implements Cloneable<RecipientItemState> {
  String id;
  String firstName;
  String lastName;
  DateTime dateOfBirth;
  AdministrativeGenderType genderType;

  RecipientItemState({
    this.id,
    this.firstName,
    this.lastName,
    this.dateOfBirth,
    this.genderType,
  });

  String get fullName {
    return '${firstName ?? ''} ${lastName ?? ''}';
  }

  String get gender => genderType?.toString()?.split('.')?.last ?? '';

  String get initial {
    if (fullName == null || fullName.isEmpty) return '';

    final names = fullName?.split(' ');

    // if (names[0] == null ||
    //     names[0].isEmpty ||
    //     names[1].isEmpty ||
    //     names[1] == null) return '';
    // var initial = names.first.substring(0, 1);
    // if (names.length > 1) {
    //   initial += names.last.substring(0, 1);
    // }
    var initial = names[0][0]+fullName[fullName.lastIndexOf(" ")+1];
    return initial.toUpperCase();
  }

  int get age {
    if (dateOfBirth == null) return 0;

    final duration = DateTime.now().difference(dateOfBirth);

    return (duration.inDays ~/ 365);
  }

  @override
  RecipientItemState clone() {
    return RecipientItemState()
      ..id = id
      ..firstName = firstName
      ..lastName = lastName
      ..dateOfBirth = dateOfBirth
      ..genderType = genderType;
  }
}

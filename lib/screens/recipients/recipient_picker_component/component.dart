import 'package:fish_redux/fish_redux.dart';

import 'effect.dart';
import 'reducer.dart';
import 'state.dart';
import 'view.dart';

class RecipientPickerComponent extends Component<RecipientPickerState> {
  RecipientPickerComponent()
      : super(
          effect: buildEffect(),
          reducer: buildReducer(),
          view: buildView,
          dependencies: Dependencies<RecipientPickerState>(
              adapter: null,
              slots: <String, Dependent<RecipientPickerState>>{}),
        );
}

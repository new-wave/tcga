import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mdi/mdi.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/widget/entry_widget.dart';

import 'action.dart';
import 'state.dart';

Widget buildView(
    RecipientPickerState state, Dispatch dispatch, ViewService viewService) {
  return GestureDetector(
    onTap: () => dispatch(
      RecipientPickerActionCreator.selectCareRecipient(),
    ),
    child: Container(
      height: (state.item == null && state.isCheckField) ? 120 : 95,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        color: colorBackground,
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          SizedBox(
            height: 12,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                padding: const EdgeInsets.only(
                  left: 16,
                ),
                child: Text(
                  state.isRequired
                      ? AppLocalizations.of(viewService.context)
                          .text('expenses.editor.care_recipient_picker')
                      : AppLocalizations.of(viewService.context)
                          .text('enums.team_role.care_recipient'),
                  style: textStylePrimary,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                  right: 16,
                ),
                child: SvgPicture.asset(
                  "assets/icons/ic_user_group.svg",
                  color: colorPrimary,
                  height: 24,
                  width: 24,
                  fit: BoxFit.none,
                ),
              ),
            ],
          ),
          Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Expanded(
                child: Container(
                  margin: EdgeInsets.only(
                      left: (state.item != null && !state.isRequired) ? 16 : 0),
                  child: EntryWidget(
                    onTap: () => dispatch(
                      RecipientPickerActionCreator.selectCareRecipient(),
                    ),
                    readOnly: true,
                    fillColor: (state.item != null && !state.isRequired)
                        ? Colors.white
                        : Colors.transparent,
                    keyboardType: TextInputType.text,
                    title: AppLocalizations.of(viewService.context).text(
                        'expenses.editor.placeholder_select_care_recipient_picker'),
                    controller: state.fullNameEditingController,
                    warningText: AppLocalizations.instance
                        .text('common.validator.expense.care_recipient'),
                    isRequired: true,
                    isCheckField: state.isCheckField ?? false,
                  ),
                ),
              ),
              Visibility(
                visible: (state.item != null && !state.isRequired),
                child: Padding(
                  padding: EdgeInsets.only(
                    right: 16,
                  ),
                  child: IconButton(
                    icon: Icon(Mdi.close),
                    iconSize: 24,
                    onPressed: () {
                      dispatch(
                        RecipientPickerActionCreator.removeCareRecipient(),
                      );
                    },
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 8,
          ),
        ],
      ),
    ),
  );
}

import 'package:fish_redux/fish_redux.dart';

import 'action.dart';
import 'state.dart';

Reducer<RecipientPickerState> buildReducer() {
  return asReducer(
    <Object, Reducer<RecipientPickerState>>{
      RecipientPickerAction.handleCareRecipientSelected:
          _handleCareRecipientSelected,
      RecipientPickerAction.handleCareRecipientRemoved:
          _handleCareRecipientRemoved,
    },
  );
}

RecipientPickerState _handleCareRecipientSelected(
    RecipientPickerState state, Action action) {
  final RecipientPickerState newState = state.clone();

  newState.item = action.payload;
  newState.fullNameEditingController.text = newState.item.fullName;
  newState.isEditedForm = true;
  return newState;
}

RecipientPickerState _handleCareRecipientRemoved(
    RecipientPickerState state, Action action) {
  final RecipientPickerState newState = state.clone();
  newState.item = null;
  newState.fullNameEditingController.text = '';
  newState.isEditedForm = true;
  return newState;
}

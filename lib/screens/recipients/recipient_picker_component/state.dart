import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:tcga_flutter/screens/recipients/state.dart';

class RecipientPickerState implements Cloneable<RecipientPickerState> {
  RecipientItemState item;
  TextEditingController fullNameEditingController;
  bool isRequired;
  bool isCheckField;

  bool isEditedForm;

  RecipientPickerState({
    this.item,
    this.isRequired = true,
    this.isCheckField = false,
    this.isEditedForm = false,
  }) {
    fullNameEditingController = TextEditingController(
      text: item?.fullName,
    );
  }

  @override
  RecipientPickerState clone() {
    return RecipientPickerState()
      ..item = item
      ..fullNameEditingController = fullNameEditingController
      ..isCheckField = isCheckField
      ..isEditedForm = isEditedForm
      ..isRequired = isRequired;
  }
}

RecipientPickerState initState(Map<String, dynamic> args) {
  final state = RecipientPickerState()
    ..item = args?.containsKey('item') == true ? args['item'] : null;
  state.fullNameEditingController = TextEditingController(
    text: state.item?.fullName,
  );
  state.isRequired = true;
  state.isCheckField = false;
  return state;
}

import 'package:fish_redux/fish_redux.dart';

import '../../../utils/utils.dart';
import '../../../route_name.dart';

import 'action.dart';
import 'state.dart';

Effect<RecipientPickerState> buildEffect() {
  return combineEffects(<Object, Effect<RecipientPickerState>>{
    RecipientPickerAction.selectCareRecipient: _selectCareRecipient,
    RecipientPickerAction.removeCareRecipient: _removeCareRecipient,
  });
}

Future _selectCareRecipient(
    Action action, Context<RecipientPickerState> ctx) async {
  final item = await ctx.navigateTo(
    RouteName.recipientsPicker,
    arguments: {
      'selected-item': ctx.state.item,
    },
  );

  if (item == null) return;

  ctx.dispatch(
    RecipientPickerActionCreator.handleCareRecipientSelected(
      item,
    ),
  );
}

Future _removeCareRecipient(
    Action action, Context<RecipientPickerState> ctx) async {
  ctx.dispatch(
    RecipientPickerActionCreator.handleCareRecipientRemoved(),
  );
}

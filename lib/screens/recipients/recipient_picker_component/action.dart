import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/recipients/state.dart';

enum RecipientPickerAction {
  selectCareRecipient,
  handleCareRecipientSelected,
  removeCareRecipient,
  handleCareRecipientRemoved,
}

class RecipientPickerActionCreator {
  static Action selectCareRecipient() {
    return const Action(RecipientPickerAction.selectCareRecipient);
  }

  static Action handleCareRecipientSelected(RecipientItemState item) {
    return Action(
      RecipientPickerAction.handleCareRecipientSelected,
      payload: item,
    );
  }

  static Action removeCareRecipient() {
    return const Action(RecipientPickerAction.removeCareRecipient);
  }

  static Action handleCareRecipientRemoved() {
    return Action(
      RecipientPickerAction.handleCareRecipientRemoved,
    );
  }
}

import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/recipients/state.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';

import '../../../route_name.dart';
import '../../../utils/utils.dart';

import '../manager.dart';

import 'action.dart';
import 'state.dart';

final _manager = RecipientManager();

Effect<RecipientsState> buildEffect() {
  return combineEffects(<Object, Effect<RecipientsState>>{
    Lifecycle.initState: _init,
    RecipientsAction.add: _add,
    RecipientsAction.edit: _edit,
    RecipientsAction.delete: _delete,
    RecipientsAction.viewDashboard: _viewDashboard,
  });
}

Future _init(Action action, Context<RecipientsState> ctx) async {
  final items = await _manager.query();
  ctx.dispatch(
    RecipientsActionCreator.initialize(
      items,
    ),
  );
}

Future _add(Action action, Context<RecipientsState> ctx) async {
  final addedItem = await ctx.navigateTo(
    RouteName.recipientsEditor,
  );

  if (addedItem == null) return;

  ctx.dispatch(
    RecipientsActionCreator.handleRecipientAdded(addedItem),
  );
}

void _edit(Action action, Context<RecipientsState> ctx) async {
  final result = await ctx.navigateTo(
    RouteName.recipientsEditor,
    arguments: {
      'item': action.payload,
    },
  );

  if (result is RecipientItemState) {
    ctx.dispatch(
      RecipientsActionCreator.handleRecipientEdited(result),
    );
  } else if (result is Map<String, dynamic> &&
      result.containsKey(
        'deleted-item-id',
      )) {
    ctx.dispatch(
      RecipientsActionCreator.handleRecipientDeleted(
        result['deleted-item-id'],
      ),
    );
  }
}

void _delete(Action action, Context<RecipientsState> ctx) async {
  final confirmed = await ctx.confirm(
    title: AppLocalizations.instance.text('common.warning.delete.title'),
    message: AppLocalizations.instance.text('common.warning.delete.desc'),
    positiveActionTitle: AppLocalizations.instance.text('common.sure'),
    negativeActionTitle: AppLocalizations.instance.text('common.cancel'),
  );

  if (!confirmed) return;

  ctx.dispatch(
    RecipientsActionCreator.handleRecipientDeleted(
      action.payload.id,
    ),
  );
}

void _viewDashboard(Action action, Context<RecipientsState> ctx) async {
  await ctx.navigateTo(
    RouteName.recipientsDashboard,
    arguments: {
      'item': action.payload,
    },
  );
}

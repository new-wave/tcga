import 'package:fish_redux/fish_redux.dart';

import '../state.dart';
import 'action.dart';
import 'state.dart';

Reducer<RecipientsState> buildReducer() {
  return asReducer(
    <Object, Reducer<RecipientsState>>{
      RecipientsAction.initialize: _initialize,
      RecipientsAction.handleRecipientAdded: _handleRecipientAdded,
      RecipientsAction.handleRecipientEdited: _handleRecipientEdited,
      RecipientsAction.handleRecipientDeleted: _handleRecipientDeleted,
    },
  );
}

RecipientsState _initialize(RecipientsState state, Action action) {
  final items = action.payload ?? <RecipientItemState>[];
  final newState = state.clone();
  newState.items = items;

  return newState;
}

RecipientsState _handleRecipientAdded(RecipientsState state, Action action) {
  final item = action.payload as RecipientItemState;
  final newState = state.clone();

  newState.items.add(item);

  return newState;
}

RecipientsState _handleRecipientEdited(RecipientsState state, Action action) {
  final item = action.payload as RecipientItemState;
  final newState = state.clone();

  final editedIndex = newState.items.indexWhere(
    (x) => x.id == item.id,
  );

  newState.items[editedIndex] = item;

  return newState;
}

RecipientsState _handleRecipientDeleted(RecipientsState state, Action action) {
  final id = action.payload;
  final newState = state.clone();

  final deletedIndex = newState.items.indexWhere(
    (x) => x.id == id,
  );

  newState.items.removeAt(deletedIndex);

  return newState;
}

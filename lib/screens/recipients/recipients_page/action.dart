import 'package:fish_redux/fish_redux.dart';

import '../state.dart';

enum RecipientsAction {
  initialize,
  add,
  handleRecipientAdded,
  edit,
  handleRecipientEdited,
  delete,
  handleRecipientDeleted,
  viewDashboard,
}

class RecipientsActionCreator {
  static Action initialize(List<RecipientItemState> items) {
    return Action(
      RecipientsAction.initialize,
      payload: items,
    );
  }

  static Action add() {
    return const Action(
      RecipientsAction.add,
    );
  }

  static Action handleRecipientAdded(
    RecipientItemState result,
  ) {
    return Action(
      RecipientsAction.handleRecipientAdded,
      payload: result,
    );
  }

  static Action edit(RecipientItemState state) {
    return Action(
      RecipientsAction.edit,
      payload: state,
    );
  }

  static Action handleRecipientEdited(RecipientItemState result) {
    return Action(
      RecipientsAction.handleRecipientEdited,
      payload: result,
    );
  }

  static Action delete(RecipientItemState state) {
    return Action(
      RecipientsAction.delete,
      payload: state,
    );
  }

  static Action handleRecipientDeleted(String result) {
    return Action(
      RecipientsAction.handleRecipientDeleted,
      payload: result,
    );
  }

  static Action viewDashboard(RecipientItemState state) {
    return Action(
      RecipientsAction.viewDashboard,
      payload: state,
    );
  }
}

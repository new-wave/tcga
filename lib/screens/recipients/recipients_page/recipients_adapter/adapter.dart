import 'package:fish_redux/fish_redux.dart';

import '../recipient_item_component/component.dart';
import '../state.dart';

class RecipientsAdapter extends SourceFlowAdapter<RecipientsState> {
  RecipientsAdapter()
      : super(
          pool: <String, Component<Object>>{
            'recipient-item': RecipientItemComponent(),
          },
        );
}

import 'package:fish_redux/fish_redux.dart';
import 'recipients_adapter/adapter.dart';

import 'effect.dart';
import 'reducer.dart';
import 'state.dart';
import 'view.dart';

class RecipientsPage extends Page<RecipientsState, Map<String, dynamic>> {
  RecipientsPage()
      : super(
          initState: initState,
          effect: buildEffect(),
          reducer: buildReducer(),
          view: buildView,
          dependencies: Dependencies<RecipientsState>(
            adapter: NoneConn<RecipientsState>() + RecipientsAdapter(),
            slots: <String, Dependent<RecipientsState>>{},
          ),
          // middleware: <Middleware<RecipientsState>>[],
        );
}

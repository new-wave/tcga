import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tcga_flutter/screens/drawer/drawer_page.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';

import 'action.dart';
import 'state.dart';

Widget buildView(
    RecipientsState state, Dispatch dispatch, ViewService viewService) {
  final adapter = viewService.buildAdapter();

  return Scaffold(
    backgroundColor: Colors.white,
    appBar: _appBar(dispatch,viewService),
    drawer: DrawerPage(),
    body: Container(
      child: ListView.separated(
        padding: const EdgeInsets.symmetric(
          vertical: 16,
        ),
        separatorBuilder: (context, index) => Divider(),
        itemBuilder: adapter.itemBuilder,
        itemCount: adapter.itemCount,
      ),
    ),
  );
}

AppBar _appBar(Dispatch dispatch,ViewService viewService) {
  return AppBar(
    elevation: 3,
    iconTheme: IconThemeData(color: Colors.black),
    title: Text(AppLocalizations.of(viewService.context).text('recipients.appbar.title')),
    centerTitle: false,
    actions: [
      GestureDetector(
        onTap: () async {},
        child: Visibility(
          visible: true,
          child: SvgPicture.asset(
            "assets/icons/ic_search.svg",
            height: 24,
            width: 24,
          ),
        ),
      ),
      SizedBox(width: 16.0),
      GestureDetector(
        onTap: () => dispatch(
          RecipientsActionCreator.add(),
        ),
        child: Visibility(
          visible: true,
          child: SvgPicture.asset(
            "assets/icons/ic_button_add.svg",
            height: 24,
            width: 24,
          ),
        ),
      ),
      SizedBox(width: 16.0),
    ],
  );
}

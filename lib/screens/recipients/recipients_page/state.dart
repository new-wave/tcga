import 'package:fish_redux/fish_redux.dart';

import '../state.dart';

class RecipientsState extends MutableSource
    implements Cloneable<RecipientsState> {
  List<RecipientItemState> items;

  @override
  RecipientsState clone() {
    return RecipientsState()..items = items;
  }

  @override
  Object getItemData(int index) {
    return items[index];
  }

  @override
  String getItemType(int index) {
    return 'recipient-item';
  }

  @override
  int get itemCount => items?.length ?? 0;

  @override
  void setItemData(int index, Object data) {
    items[index] = data;
  }
}

RecipientsState initState(Map<String, dynamic> args) {
  return RecipientsState()..items = [];
}

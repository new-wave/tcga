import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';

import 'package:tcga_flutter/widget/list_item_widget.dart';

import '../../state.dart';
import '../action.dart';

Widget buildView(
    RecipientItemState state, Dispatch dispatch, ViewService viewService) {
  return SizedBox(
    height: 40,
    child: ListItemWidget(
      contextActions: [
        'recipients.popup.edit',
        'recipients.popup.delete',
      ],
      onTap: () => dispatch(
        RecipientsActionCreator.viewDashboard(state),
      ),
      onContextActionSelected: (String action) {
        switch (action) {
          case 'recipients.popup.edit':
            {
              dispatch(
                RecipientsActionCreator.edit(
                  state,
                ),
              );
            }
            break;
          case 'recipients.popup.delete':
            {
              dispatch(
                RecipientsActionCreator.delete(
                  state,
                ),
              );
            }
            break;
        }
      },
      child: Container(
        child: Row(
          children: [
            SizedBox(
              width: 16,
            ),
            Container(
              height: 40,
              width: 40,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(
                  20,
                ),
                color: colorPrimary,
              ),
              child: Text(
                '${state.initial}',
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 14,
                ),
              ),
            ),
            SizedBox(
              width: 16,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  width: MediaQuery.of(viewService.context).size.width - 120,
                  child: Text(
                    state.fullName,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      color: colorTextPrimary,
                      fontSize: 14,
                    ),
                  ),
                ),
                Row(
                  children: [
                    Text(
                      state.gender,
                      style: TextStyle(
                        color: colorTextSecondary,
                        fontSize: 12,
                      ),
                    ),
                    Text(
                      ' | ${AppLocalizations.of(viewService.context).text('recipients.item.age')}: ${state.age}',
                      style: TextStyle(
                        color: colorTextSecondary,
                        fontSize: 12,
                      ),
                    ),
                  ],
                )
              ],
            ),
          ],
        ),
      ),
    ),
  );
}

import 'package:fish_redux/fish_redux.dart';

import '../../state.dart';
import 'view.dart';

class RecipientItemComponent extends Component<RecipientItemState> {
  RecipientItemComponent()
      : super(
          view: buildView,
        );
}

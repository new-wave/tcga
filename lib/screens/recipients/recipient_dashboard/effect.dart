import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/recipients/conditions/conditions_page/condition_item_component/state.dart';
import 'package:tcga_flutter/screens/recipients/conditions/manager.dart';
import 'package:tcga_flutter/screens/recipients/recipient_dashboard/action.dart';
import 'package:tcga_flutter/screens/recipients/recipient_dashboard/model.dart';

import '../../../utils/utils.dart';

import 'state.dart';

final _manager = ConditionManager();

Effect<RecipientDashboardState> buildEffect() {
  return combineEffects(<Object, Effect<RecipientDashboardState>>{
    RecipientDashboardAction.viewItem: _viewItem,
  });
}

void _viewItem(Action action, Context<RecipientDashboardState> ctx) async {
  final dashboardItem = action.payload as ListItemsInfo;

  if (dashboardItem.namedRoute == null) return;

  ctx.navigateTo(
    dashboardItem.namedRoute,
    arguments: {
      'recipient': ctx.state.recipient,
      'recipient-id': ctx.state.recipient?.id ?? null,
    },
  );
}

import 'package:fish_redux/fish_redux.dart';
import 'action.dart';
import 'state.dart';

Reducer<RecipientDashboardState> buildReducer() {
  return asReducer(
    <Object, Reducer<RecipientDashboardState>>{
      //   RecipientDashboardAction.init: _init,
    },
  );
}

// RecipientDashboardState _init(RecipientDashboardState state, Action action) {
//   final RecipientDashboardState newState = state.clone();
//   newState.recipient = action.payload;
//   return newState;
// }

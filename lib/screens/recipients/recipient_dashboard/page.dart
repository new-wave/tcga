import 'package:fish_redux/fish_redux.dart';

import 'recipient_header_component/component.dart';

import 'state.dart';
import 'view.dart';
import 'reducer.dart';
import 'effect.dart';

class RecipientDashboardPage
    extends Page<RecipientDashboardState, Map<String, dynamic>> {
  RecipientDashboardPage()
      : super(
          initState: initState,
          effect: buildEffect(),
          reducer: buildReducer(),
          view: buildView,
          dependencies: Dependencies<RecipientDashboardState>(
            slots: <String, Dependent<RecipientDashboardState>>{
              'recipient-header':
                  RecipientHeaderStateConnector() + RecipientHeaderComponent(),
            },
          ),
        );
}

import 'package:fish_redux/fish_redux.dart';

import 'package:tcga_flutter/screens/recipients/recipient_dashboard/model.dart';

enum RecipientDashboardAction {
  viewItem,
}

class RecipientDashboardActionCreator {
  static Action viewItem(ListItemsInfo item) {
    return Action(
      RecipientDashboardAction.viewItem,
      payload: item,
    );
  }
}

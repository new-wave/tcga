import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';

import 'action.dart';
import 'model.dart';
import 'state.dart';

Widget buildView(
  RecipientDashboardState state,
  Dispatch dispatch,
  ViewService viewService,
) {
  List itemTitles = [
    'care_recipient_dashboard.item.conditions.title',
    'care_recipient_dashboard.item.medications.title',
    'care_recipient_dashboard.item.immunizations.title',
    'care_recipient_dashboard.item.allergies.title',
    'care_recipient_dashboard.item.tasks.title',
    'care_recipient_dashboard.item.calendar.title',
    'care_recipient_dashboard.item.health_library.title',
    'care_recipient_dashboard.item.consents.title',
    'care_recipient_dashboard.item.notes.title',
    'care_recipient_dashboard.item.settings.title',
  ];
  return Scaffold(
    backgroundColor: Colors.white,
    appBar: _appBar(viewService),
    body: SingleChildScrollView(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          viewService.buildComponent(
            'recipient-header',
          ),
          Container(
            margin: edgeOnlyBottom,
            height: 5,
            color: colorBackground,
          ),
          GridView.builder(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            padding: const EdgeInsets.only(right: 5.0),
            itemCount: dashboardItems.length,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 3,
              crossAxisSpacing: 8,
              mainAxisSpacing: 4,
            ),
            itemBuilder: (
              BuildContext context,
              int index,
            ) {
              return Container(
                alignment: Alignment.center,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    GestureDetector(
                        onTap: () => dispatch(
                              RecipientDashboardActionCreator.viewItem(
                                dashboardItems[index],
                              ),
                            ),
                        child: dashboardItems[index].icon),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      AppLocalizations.of(viewService.context)
                          .text(itemTitles[index]),
                      style: textStylePrimary,
                      textAlign: TextAlign.center,
                    )
                  ],
                ),
              );
            },
          ),
        ],
      ),
    ),
  );
}

AppBar _appBar(ViewService viewService) {
  return AppBar(
    elevation: 3,
    centerTitle: false,
    title: Text(
      AppLocalizations.of(viewService.context)
          .text('recipients_dashboard.appbar.title'),
      style: textStyleTitleThird,
    ),
    leadingWidth: 46,
    leading: GestureDetector(
      onTap: () => Navigator.pop(viewService.context),
      child: Container(
        margin: EdgeInsets.only(left: 16),
        child: SvgPicture.asset(
          'assets/icons/ic_arrow_left_circle.svg',
          height: 24,
          width: 24,
        ),
      ),
    ),
  );
}

import 'package:fish_redux/fish_redux.dart';
import '../conditions/conditions_page/condition_item_component/state.dart';
import 'package:tcga_flutter/screens/recipients/state.dart';

import 'recipient_header_component/state.dart';

class RecipientDashboardState implements Cloneable<RecipientDashboardState> {
  RecipientItemState recipient;

  @override
  RecipientDashboardState clone() {
    return RecipientDashboardState()
      ..recipient = recipient;
  }
}

RecipientDashboardState initState(Map<String, dynamic> args) {
  final state = RecipientDashboardState();
  state.recipient = args['item'] as RecipientItemState;
  return state;
}

class RecipientHeaderStateConnector
    extends ConnOp<RecipientDashboardState, RecipientHeaderState> {
  @override
  RecipientHeaderState get(RecipientDashboardState state) =>
      RecipientHeaderState(
        recipient: state.recipient,
      );

  @override
  void set(RecipientDashboardState state, RecipientHeaderState subState) {
    state.recipient = subState.recipient;
  }
}

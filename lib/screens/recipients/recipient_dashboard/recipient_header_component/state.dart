import 'package:fish_redux/fish_redux.dart';
import '../../conditions/conditions_page/condition_item_component/state.dart';

import '../../state.dart';

class RecipientHeaderState implements Cloneable<RecipientHeaderState> {
  RecipientItemState recipient;
  int type;
  String title;

  RecipientHeaderState({
    this.recipient,
    this.type,
    this.title,
  });

  @override
  RecipientHeaderState clone() {
    return RecipientHeaderState()
      ..recipient = recipient
      ..type = type
      ..title = title;
  }
}

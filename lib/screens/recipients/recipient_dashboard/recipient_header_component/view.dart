import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tcga_flutter/config/palette.dart';
import '../../../../utils/utils.dart';
import 'package:tcga_flutter/database/models/enums.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/utils/enum_to_string.dart';

import 'state.dart';

Widget buildView(
  RecipientHeaderState state,
  Dispatch dispatch,
  ViewService viewService,
) {
  return Container(
    margin: edgeAllInsetsPrimaryLarge,
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                child: Text(
                  "${state.recipient.fullName}",
                  overflow: TextOverflow.ellipsis,
                  style: textStyleTitlePrimary,
                ),
              ),
              Container(
                margin: edegMiniTopBot,
                child: Text(
                  "${AppLocalizations.of(viewService.context).text('recipients.item.age')}: ${state.recipient.age}",
                  style: textStylePrimary,
                ),
              ),
              Text(
                "${AppLocalizations.of(viewService.context).text('recipients_dashboard.gender')}: ${state.recipient.gender}",
                style: textStylePrimary,
              ),
              state.type != null
                  ? Text(
                      "Primary ${convertEnumToList(ConditionType.values)[state.type]} Condition")
                  : Text(""),
              state.title != null
                  ? Container(
                      width: 230,
                      child: Text(
                        state.title,
                        overflow: TextOverflow.ellipsis,
                        maxLines: 1,
                        softWrap: false,
                      ),
                    )
                  : Text(""),
            ],
          ),
        ),
        Container(
          height: 100,
          width: 100,
          child: SvgPicture.asset(
            "assets/icons/account.svg",
            color: Colors.blueGrey,
          ),
        ),
      ],
    ),
  );
}

import 'package:fish_redux/fish_redux.dart';

import 'state.dart';
import 'view.dart';

class RecipientHeaderComponent extends Component<RecipientHeaderState> {
  RecipientHeaderComponent()
      : super(
          view: buildView,
          dependencies: Dependencies<RecipientHeaderState>(
              adapter: null,
              slots: <String, Dependent<RecipientHeaderState>>{}),
        );
}

import 'package:flutter/material.dart';
import 'package:mdi/mdi.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/route_name.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';

class ListItemsInfo {
  final Text infoName;
  final String namedRoute;
  final Icon icon;

  const ListItemsInfo({this.infoName, this.icon, this.namedRoute});
}

final dashboardItems = <ListItemsInfo>[
  ListItemsInfo(
    infoName: Text(
      AppLocalizations.instance.text('care_recipient_dashboard.item.conditions.title'),
      style: textStylePrimary,
    ),
    namedRoute: RouteName.recipientsCondition,
    icon: Icon(
      Mdi.clipboardOutline,
      size: 50,
      color: buttonBackgroundColor,
    ),
  ),
  ListItemsInfo(
    infoName: Text(
      AppLocalizations.instance.text('care_recipient_dashboard.item.medications.title'),
      style: textStylePrimary,
    ),
    namedRoute: RouteName.recipientsMedication,
    icon: Icon(
      Mdi.pill,
      size: 50,
      color: buttonBackgroundColor,
    ),
  ),
  ListItemsInfo(
    infoName: Text(
      AppLocalizations.instance.text('care_recipient_dashboard.item.immunizations.title'),
      style: textStylePrimary,
    ),
    namedRoute: RouteName.recipientsImmunization,
    icon: Icon(
      Mdi.needle,
      size: 50,
      color: buttonBackgroundColor,
    ),
  ),
  ListItemsInfo(
    infoName: Text(
      AppLocalizations.instance.text('care_recipient_dashboard.item.allergies.title'),
      style: textStylePrimary,
    ),
    namedRoute: RouteName.recipientsAllergy,
    icon: Icon(
      Mdi.allergy,
      size: 50,
      color: buttonBackgroundColor,
    ),
  ),
  ListItemsInfo(
    infoName: Text(
      AppLocalizations.instance.text('care_recipient_dashboard.item.tasks.title'),
      style: textStylePrimary,
    ),
    // namedRoute: RouteName.recipientsTasks,
    icon: Icon(
      Mdi.formatListBulletedSquare,
      size: 50,
      color: buttonBackgroundColor,
    ),
  ),

  ListItemsInfo(
    infoName: Text(
      AppLocalizations.instance.text('care_recipient_dashboard.item.calendar.title'),
      style: textStylePrimary,
    ),
    namedRoute: RouteName.calendar,
    icon: Icon(
      Mdi.calendarMonth,
      size: 50,
      color: buttonBackgroundColor,
    ),
  ),
  ListItemsInfo(
    infoName: Text(
      AppLocalizations.instance.text('care_recipient_dashboard.item.health_library.title'),
      style: textStylePrimary,
    ),
    // namedRoute: RouteName.recipientsHealthLibrary,
    icon: Icon(
      Mdi.library,
      size: 50,
      color: buttonBackgroundColor,
    ),
  ),
  ListItemsInfo(
    infoName: Text(
      AppLocalizations.instance.text('care_recipient_dashboard.item.consents.title'),
      style: textStylePrimary,
    ),
    // namedRoute: RouteName.recipientsConsents,
    icon: Icon(
      Mdi.newspaperVariantOutline,
      size: 50,
      color: buttonBackgroundColor,
    ),
  ),
  ListItemsInfo(
    infoName: Text(
      AppLocalizations.instance.text('care_recipient_dashboard.item.notes.title'),
      style: textStylePrimary,
    ),
    namedRoute: RouteName.notes,
    icon: Icon(
      Mdi.noteOutline,
      size: 50,
      color: buttonBackgroundColor,
    ),
  ),
  ListItemsInfo(
    infoName: Text(
      AppLocalizations.instance.text('care_recipient_dashboard.item.settings.title'),
      textAlign: TextAlign.center,
      style: textStylePrimary,
    ),
    // namedRoute: RouteName.recipientsSettings,
    icon: Icon(
      Mdi.cog,
      size: 50,
      color: buttonBackgroundColor,
    ),
  ),
];

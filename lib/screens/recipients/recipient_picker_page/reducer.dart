import 'package:fish_redux/fish_redux.dart';

import 'action.dart';
import 'state.dart';

Reducer<RecipientPickerState> buildReducer() {
  return asReducer(
    <Object, Reducer<RecipientPickerState>>{
      RecipientPickerAction.initialize: _initialize,
    },
  );
}

RecipientPickerState _initialize(RecipientPickerState state, Action action) {
  final RecipientPickerState newState = state.clone();
  newState.items = action.payload ?? [];
  return newState;
}

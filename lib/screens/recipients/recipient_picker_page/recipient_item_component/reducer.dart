import 'package:fish_redux/fish_redux.dart';

import '../../state.dart';
import 'action.dart';

Reducer<RecipientItemState> buildReducer() {
  return asReducer(
    <Object, Reducer<RecipientItemState>>{
      RecipientItemAction.action: _onAction,
    },
  );
}

RecipientItemState _onAction(RecipientItemState state, Action action) {
  final RecipientItemState newState = state.clone();
  return newState;
}

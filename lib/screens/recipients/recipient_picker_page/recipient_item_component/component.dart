import 'package:fish_redux/fish_redux.dart';

import '../../state.dart';
import 'effect.dart';
import 'reducer.dart';
import 'view.dart';

class RecipientItemComponent extends Component<RecipientItemState> {
  RecipientItemComponent()
      : super(
          effect: buildEffect(),
          reducer: buildReducer(),
          view: buildView,
          dependencies: Dependencies<RecipientItemState>(
              adapter: null, slots: <String, Dependent<RecipientItemState>>{}),
        );
}

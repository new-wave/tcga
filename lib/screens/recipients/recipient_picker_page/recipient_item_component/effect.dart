import 'package:fish_redux/fish_redux.dart';

import '../../state.dart';
import 'action.dart';

Effect<RecipientItemState> buildEffect() {
  return combineEffects(<Object, Effect<RecipientItemState>>{
    RecipientItemAction.action: _onAction,
  });
}

void _onAction(Action action, Context<RecipientItemState> ctx) {}

import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/recipients/recipient_picker_page/recipient_adapter/adapter.dart';

import 'effect.dart';
import 'reducer.dart';
import 'state.dart';
import 'view.dart';

class RecipientPickerPage
    extends Page<RecipientPickerState, Map<String, dynamic>> {
  RecipientPickerPage()
      : super(
          initState: initState,
          effect: buildEffect(),
          reducer: buildReducer(),
          view: buildView,
          dependencies: Dependencies<RecipientPickerState>(
              adapter: NoneConn<RecipientPickerState>() + RecipientAdapter(),
              slots: <String, Dependent<RecipientPickerState>>{}),
          middleware: <Middleware<RecipientPickerState>>[],
        );
}

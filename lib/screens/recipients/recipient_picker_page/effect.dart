import 'package:flutter/material.dart' hide Action;

import 'package:fish_redux/fish_redux.dart';
import '../manager.dart';

import '../state.dart';
import 'action.dart';
import 'state.dart';

final _recipientManager = RecipientManager();

Effect<RecipientPickerState> buildEffect() {
  return combineEffects(<Object, Effect<RecipientPickerState>>{
    Lifecycle.initState: _init,
    RecipientPickerAction.search: _search,
    RecipientPickerAction.confirm: _confirm,
  });
}

Future _init(Action action, Context<RecipientPickerState> ctx) async {
  final items = await _recipientManager.query();

  ctx.dispatch(RecipientPickerActionCreator.initialize(items));
}

Future _search(Action action, Context<RecipientPickerState> ctx) async {
  final keyword = action.payload as String ?? '';

  final items = await _recipientManager.query(
    keyword: keyword,
  );

  ctx.dispatch(RecipientPickerActionCreator.initialize(items));
}

Future _confirm(Action action, Context<RecipientPickerState> ctx) async {
  final selectedItem = action.payload as RecipientItemState;

  if (selectedItem == null) return;

  Navigator.of(ctx.context).pop(selectedItem);
}

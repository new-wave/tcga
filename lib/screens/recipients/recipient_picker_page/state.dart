import 'package:fish_redux/fish_redux.dart';

import '../state.dart';

class RecipientPickerState extends MutableSource
    implements Cloneable<RecipientPickerState> {
  List<RecipientItemState> items;
  bool confirmRequired;

  @override
  RecipientPickerState clone() {
    return RecipientPickerState()
      ..items = items
      ..confirmRequired = confirmRequired;
  }

  @override
  Object getItemData(int index) {
    return items[index];
  }

  @override
  String getItemType(int index) {
    return 'recipient-item';
  }

  @override
  int get itemCount => items?.length ?? 0;

  @override
  void setItemData(int index, Object data) {
    items[index] = data;
  }
}

RecipientPickerState initState(Map<String, dynamic> args) {
  return RecipientPickerState()
    ..items = []
    ..confirmRequired = false;
}

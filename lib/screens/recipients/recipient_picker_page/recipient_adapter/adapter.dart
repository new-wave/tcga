import 'package:fish_redux/fish_redux.dart';

import '../recipient_item_component/component.dart';

import '../state.dart';
import 'reducer.dart';

class RecipientAdapter extends SourceFlowAdapter<RecipientPickerState> {
  RecipientAdapter()
      : super(
          pool: <String, Component<Object>>{
            'recipient-item': RecipientItemComponent(),
          },
          reducer: buildReducer(),
        );
}

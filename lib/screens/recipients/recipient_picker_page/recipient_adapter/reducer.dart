import 'package:fish_redux/fish_redux.dart';

import '../state.dart';

import 'action.dart';

Reducer<RecipientPickerState> buildReducer() {
  return asReducer(
    <Object, Reducer<RecipientPickerState>>{
      RecipientAction.action: _onAction,
    },
  );
}

RecipientPickerState _onAction(RecipientPickerState state, Action action) {
  final RecipientPickerState newState = state.clone();
  return newState;
}

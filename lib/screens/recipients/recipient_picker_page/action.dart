import 'package:fish_redux/fish_redux.dart';

import '../state.dart';

enum RecipientPickerAction {
  initialize,
  search,
  confirm,
}

class RecipientPickerActionCreator {
  static Action initialize(List<RecipientItemState> items) {
    return Action(RecipientPickerAction.initialize, payload: items);
  }

  static Action search(String keyword) {
    return Action(RecipientPickerAction.search, payload: keyword);
  }

  static Action confirm([RecipientItemState item]) {
    return Action(RecipientPickerAction.confirm, payload: item);
  }
}

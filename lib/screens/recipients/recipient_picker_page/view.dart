import 'dart:math' as math;

import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/widget/button_widget.dart';
import 'package:tcga_flutter/widget/entry_widget.dart';

import 'action.dart';
import 'state.dart';

Widget buildView(
  RecipientPickerState state,
  Dispatch dispatch,
  ViewService viewService,
) {
  final ListAdapter adapter = viewService.buildAdapter();
  return Container(
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(
        15,
      ),
    ),
    child: Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: true,
        title: Text(AppLocalizations.of(viewService.context)
            .text('expenses.editor.select_care_recipient_picker.header')),
        actions: [
          InkWell(
            onTap: () {
              Navigator.of(viewService.context).pop();
            },
            child: SizedBox(
              height: 48,
              width: 48,
              child: Padding(
                padding: const EdgeInsets.all(12),
                child: SvgPicture.asset('assets/icons/ic_close.svg'),
              ),
            ),
          )
        ],
      ),
      body: CustomScrollView(
        slivers: [
          SliverToBoxAdapter(
            child: Container(
              padding: const EdgeInsets.all(16),
              child: EntryWidget(
                title: AppLocalizations.of(viewService.context)
                    .text('recipients.placeholder_enter_to_search'),
                keyboardType: TextInputType.text,
                suffixIcon: SvgPicture.asset('assets/icons/ic_search.svg'),
                onTextChanged: (text) {
                  dispatch(
                    RecipientPickerActionCreator.search(text),
                  );
                },
              ),
            ),
          ),
          SliverList(
            delegate: SliverChildBuilderDelegate(
              (context, index) {
                final int itemIndex = index ~/ 2;
                if (index.isEven) {
                  if (state.confirmRequired) {
                    return adapter.itemBuilder(
                      context,
                      itemIndex,
                    );
                  }

                  return SizedBox(
                    height: 40,
                    child: Stack(
                      children: [
                        adapter.itemBuilder(
                          context,
                          itemIndex,
                        ),
                        InkWell(
                          onTap: () {
                            dispatch(
                              RecipientPickerActionCreator.confirm(
                                state.getItemData(itemIndex),
                              ),
                            );
                          },
                          child: SizedBox.expand(
                            child: Container(
                              color: Colors.transparent,
                            ),
                          ),
                        )
                      ],
                    ),
                  );
                }
                return SizedBox(height: 8);
              },
              // childCount: adapter.itemCount,
              semanticIndexCallback: (Widget widget, int localIndex) {
                if (localIndex.isEven) {
                  return localIndex ~/ 2;
                }
                return null;
              },
              childCount: math.max(0, adapter.itemCount * 2 - 1),
            ),
          ),
        ],
      ),
      bottomNavigationBar: state.confirmRequired
          ? ButtonWidget(
              textButton: AppLocalizations.of(viewService.context)
                  .text('common.confirm'),
              colorButton: colorPrimary,
              onPress: () {
                dispatch(RecipientPickerActionCreator.confirm());
              },
            )
          : SizedBox.shrink(),
    ),
  );
}

import 'package:fish_redux/fish_redux.dart';

import 'medication_item_component/state.dart';

enum MedicationsAction {
  initialize,
  create,
  handleMedicationCreated,
  edit,
  handleMedicationEdited,
  delete,
  handleMedicationDeleted,
  addNote,
}

class MedicationsActionCreator {
  static Action initialize(List<MedicationItemState> items) {
    return Action(
      MedicationsAction.initialize,
      payload: items,
    );
  }

  static Action edit(MedicationItemState item) {
    return Action(
      MedicationsAction.edit,
      payload: item,
    );
  }

  static Action handleMedicationEdited(MedicationItemState item) {
    return Action(
      MedicationsAction.handleMedicationEdited,
      payload: item,
    );
  }

  static Action addNote(MedicationItemState item) {
    return Action(
      MedicationsAction.addNote,
      payload: item,
    );
  }

  static Action delete(MedicationItemState item) {
    return Action(
      MedicationsAction.delete,
      payload: item,
    );
  }

  static Action handleMedicationDeleted(String id) {
    return Action(
      MedicationsAction.handleMedicationDeleted,
      payload: id,
    );
  }

  static Action create(String recipientId) {
    return Action(
      MedicationsAction.create,
      payload: recipientId,
    );
  }

  static Action handleMedicationCreated(MedicationItemState item) {
    return Action(
      MedicationsAction.handleMedicationCreated,
      payload: item,
    );
  }
}

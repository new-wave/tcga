import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/database/models/enums.dart';
import 'package:tcga_flutter/screens/recipients/medications/medications_page/medication_item_component/state.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';

import '../../../../route_name.dart';
import '../../../../utils/utils.dart';

import '../manager.dart';
import 'action.dart';
import 'state.dart';

final _manager = MedicationManager();

Effect<MedicationsState> buildEffect() {
  return combineEffects(<Object, Effect<MedicationsState>>{
    Lifecycle.initState: _init,
    MedicationsAction.create: _create,
    MedicationsAction.edit: _edit,
    MedicationsAction.delete: _delete,
    MedicationsAction.addNote: _addNote,
  });
}

Future _init(
  Action action,
  Context<MedicationsState> ctx,
) async {
  final items = await _manager.query(
    recipientId: ctx.state.recipient.id,
  );

  ctx.dispatch(
    MedicationsActionCreator.initialize(
      items,
    ),
  );
}

Future _create(Action action, Context<MedicationsState> ctx) async {
  final result = await ctx.navigateTo(
    RouteName.recipientsMedicationEdit,
    arguments: {
      'recipient-id': ctx.state.recipient.id,
      'items': ctx.state.items,
    },
  );

  if (result == null) return;

  ctx.dispatch(
    MedicationsActionCreator.handleMedicationCreated(
      result,
    ),
  );
}

Future _edit(Action action, Context<MedicationsState> ctx) async {
  final result = await ctx.navigateTo(
    RouteName.recipientsMedicationEdit,
    arguments: {
      'item': action.payload,
      'recipient-id': ctx.state.recipient.id,
      'items': ctx.state.items,
    },
  );

  if (result == null) return;

  if (result is MedicationItemState) {
    ctx.dispatch(
      MedicationsActionCreator.handleMedicationEdited(
        result,
      ),
    );
  } else {
    ctx.dispatch(
      MedicationsActionCreator.handleMedicationDeleted(
        action.payload.id,
      ),
    );
  }
}

Future _delete(Action action, Context<MedicationsState> ctx) async {
  final confirmed = await ctx.confirm(
    title: AppLocalizations.instance.text('common.warning.delete.title'),
    message: AppLocalizations.instance.text('common.warning.delete.desc'),
    negativeActionTitle: AppLocalizations.instance.text('common.cancel'),
    positiveActionTitle: AppLocalizations.instance.text('common.sure'),
  );

  if (!confirmed) return;

  final deletedItemId = action.payload.id;
  final deleted = await _manager.delete(deletedItemId);

  if (!deleted) {
    // TODO show error message
    return;
  }

  ctx.dispatch(
    MedicationsActionCreator.handleMedicationDeleted(
      deletedItemId,
    ),
  );
}

Future _addNote(Action action, Context<MedicationsState> ctx) {
  return ctx.navigateTo(
    RouteName.notesEditor,
    arguments: {
      'item-id': action.payload.id,
      'type': NoteType.Medication,
      'recipient-id': ctx.state.recipient.id,
    },
  );
}

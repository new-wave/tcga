import 'package:fish_redux/fish_redux.dart';

import '../../recipient_dashboard/recipient_header_component/component.dart';

import 'medications_adapter/adapter.dart';
import 'effect.dart';
import 'state.dart';
import 'view.dart';
import 'reducer.dart';

class MedicationsPage extends Page<MedicationsState, Map<String, dynamic>> {
  MedicationsPage()
      : super(
          initState: initState,
          effect: buildEffect(),
          reducer: buildReducer(),
          view: buildView,
          dependencies: Dependencies<MedicationsState>(
            adapter: NoneConn<MedicationsState>() + MedicationsAdapter(),
            slots: <String, Dependent<MedicationsState>>{
              'recipient-header':
                  RecipientHeaderStateConnector() + RecipientHeaderComponent(),
            },
          ),
        );
}

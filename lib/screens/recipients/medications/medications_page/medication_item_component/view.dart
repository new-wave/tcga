import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';

import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/database/models/enums.dart';
import 'package:tcga_flutter/utils/enum_to_string.dart';
import 'package:tcga_flutter/widget/list_item_widget.dart';

import '../action.dart';
import 'state.dart';

Widget buildView(
  MedicationItemState state,
  Dispatch dispatch,
  ViewService viewService,
) {
  return SizedBox(
    height: 70,
    child: ListItemWidget(
      contextActions: [
        'recipients.popup.add_note',
        'recipients.popup.edit',
        'recipients.popup.delete',
      ],
      onTap: () => dispatch(
        MedicationsActionCreator.edit(
          state,
        ),
      ),
      onContextActionSelected: (String action) {
        switch (action) {
          case 'recipients.popup.add_note':
            {
              dispatch(
                MedicationsActionCreator.addNote(
                  state,
                ),
              );
            }
            break;
          case 'recipients.popup.edit':
            {
              dispatch(
                MedicationsActionCreator.edit(
                  state,
                ),
              );
            }
            break;
          case 'recipients.popup.delete':
            {
              dispatch(
                MedicationsActionCreator.delete(
                  state,
                ),
              );
            }
            break;
        }
      },
      child: Container(
        margin: EdgeInsets.only(
          left: spacingPrimary,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              children: [
                Expanded(
                  child: Container(
                    child: Text(
                      state.title,
                      style: TextStyle(
                        color: colorTextPrimary,
                        fontSize: 14,
                        fontWeight: FontWeight.w700,
                      ),
                      overflow: TextOverflow.ellipsis,
                      maxLines: 1,
                      softWrap: false,
                    ),
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
                Text(
                  "(${convertEnumToList(MedicationRoute.values)[state.route]})",
                  style: TextStyle(
                    color: colorTextPrimary,
                    fontSize: 14,
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
                Text(
                  "${convertEnumToList(MedicationStatus.values)[state.status]}",
                  style: TextStyle(
                    color: colorTextPrimary,
                    fontSize: 14,
                  ),
                  overflow: TextOverflow.clip,
                  maxLines: 1,
                  softWrap: false,
                ),
              ],
            ),
            SizedBox(
              height: 8,
            ),
            Row(
              children: [
                Icon(
                  Icons.schedule,
                  size: 17,
                  color: colorTextSecondary,
                ),
                SizedBox(
                  width: 10,
                ),
                Text(
                  "Schedule",
                  style: TextStyle(
                    color: colorTextSecondary,
                    fontSize: 12,
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 4,
            ),
            Text("${state.date.format(
              state.recordedDate != null ? state.recordedDate : DateTime.now(),
            )}")
          ],
        ),
      ),
    ),
  );
}

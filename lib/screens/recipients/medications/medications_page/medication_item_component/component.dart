import 'package:fish_redux/fish_redux.dart';

import 'state.dart';
import 'view.dart';

class MedicationItemComponent extends Component<MedicationItemState> {
  MedicationItemComponent()
      : super(
          view: buildView,
        );
}

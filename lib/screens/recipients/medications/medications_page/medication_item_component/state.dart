import 'package:fish_redux/fish_redux.dart';
import 'package:intl/intl.dart';
import 'package:tcga_flutter/database/models/enums.dart';

class MedicationItemState implements Cloneable<MedicationItemState> {
  String id;
  String title;
  String code;
  int status;
  int route;
  DateTime recordedDate;
  var date = new DateFormat('d/MM/y');

  MedicationItemState({
    this.id,
    this.title,
    this.code,
    this.status,
    this.recordedDate,
    this.route,
  });

  @override
  MedicationItemState clone() {
    return MedicationItemState()
      ..id = id
      ..title = title
      ..code = code
      ..status = status
      ..recordedDate = recordedDate
      ..route = route;
  }
}

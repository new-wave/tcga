import 'package:fish_redux/fish_redux.dart';

import '../medication_item_component/component.dart';
import '../state.dart';

class MedicationsAdapter extends SourceFlowAdapter<MedicationsState> {
  MedicationsAdapter()
      : super(
          pool: <String, Component<Object>>{
            'medication-item': MedicationItemComponent()
          },
        );
}

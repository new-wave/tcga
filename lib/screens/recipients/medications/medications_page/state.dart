import 'package:fish_redux/fish_redux.dart';

import '../../recipient_dashboard/recipient_header_component/state.dart';
import '../../state.dart';

import 'medication_item_component/state.dart';

class MedicationsState extends MutableSource
    implements Cloneable<MedicationsState> {
  List<MedicationItemState> items;
  RecipientItemState recipient;
  MedicationItemState primaryMedication;

  @override
  MedicationsState clone() {
    return MedicationsState()
      ..items = items
      ..recipient = recipient
      ..primaryMedication = primaryMedication;
  }

  @override
  Object getItemData(int index) => items[index];

  @override
  String getItemType(int index) => 'medication-item';

  @override
  int get itemCount => items?.length ?? 0;

  @override
  void setItemData(int index, Object data) => items[index] = data;
}

MedicationsState initState(Map<String, dynamic> arg) {
  return MedicationsState()..recipient = arg['recipient'];
}

class RecipientHeaderStateConnector
    extends ConnOp<MedicationsState, RecipientHeaderState> {
  @override
  RecipientHeaderState get(MedicationsState state) => RecipientHeaderState(
        recipient: state.recipient,
      );

  @override
  void set(MedicationsState state, RecipientHeaderState subState) {
    state.recipient = subState.recipient;
  }
}

import 'package:fish_redux/fish_redux.dart';

import 'action.dart';
import 'medication_item_component/state.dart';
import 'state.dart';

Reducer<MedicationsState> buildReducer() {
  return asReducer(
    <Object, Reducer<MedicationsState>>{
      MedicationsAction.initialize: _initialize,
      MedicationsAction.handleMedicationCreated: _handleMedicationAdded,
      MedicationsAction.handleMedicationEdited: _handleMedicationEdited,
      MedicationsAction.handleMedicationDeleted: _handleMedicationDeleted,
    },
  );
}

MedicationsState _initialize(MedicationsState state, Action action) {
  final MedicationsState newState = state.clone();

  newState.items = action.payload as List<MedicationItemState> ?? [];

  return newState;
}

MedicationsState _handleMedicationAdded(MedicationsState state, Action action) {
  final item = action.payload as MedicationItemState;
  final newState = state.clone();

  newState.items.add(item);

  return newState;
}

MedicationsState _handleMedicationEdited(
    MedicationsState state, Action action) {
  final item = action.payload as MedicationItemState;
  final newState = state.clone();

  final editedIndex = newState.items.indexWhere(
    (x) => x.id == item.id,
  );

  newState.items[editedIndex] = item;

  return newState;
}

MedicationsState _handleMedicationDeleted(
    MedicationsState state, Action action) {
  final id = action.payload;
  final newState = state.clone();

  final deletedIndex = newState.items.indexWhere(
    (x) => x.id == id,
  );

  newState.items.removeAt(deletedIndex);

  return newState;
}

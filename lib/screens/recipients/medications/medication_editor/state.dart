import 'package:flutter/material.dart' hide Action, Page;
import 'package:flutter/widgets.dart';

import 'package:fish_redux/fish_redux.dart';

import 'package:tcga_flutter/database/models/enums.dart';
import 'package:tcga_flutter/screens/valuesets/valueset_picker_page/valueset_item_component/state.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';

import '../medications_page/medication_item_component/state.dart';

class MedicationEditorState implements Cloneable<MedicationEditorState> {
  Map<MedicationStatus, String> get medicationStatuses => {
        MedicationStatus.Active:
            AppLocalizations.instance.text('enums.medication_status.active'),
        MedicationStatus.Inactive:
            AppLocalizations.instance.text('enums.medication_status.inactive'),
        MedicationStatus.EnteredInError: AppLocalizations.instance
            .text('enums.medication_status.entered_in_error'),
      };

  Map<MedicationRoute, String> get medicationRoutes => {
        MedicationRoute.Oral:
            AppLocalizations.instance.text('enums.medication_route.oral'),
        MedicationRoute.Cutaneous:
            AppLocalizations.instance.text('enums.medication_route.cutaneous'),
        MedicationRoute.Inhalation:
            AppLocalizations.instance.text('enums.medication_route.inhalation'),
        MedicationRoute.Intramuscular: AppLocalizations.instance
            .text('enums.medication_route.intramuscular'),
        MedicationRoute.Intravenous: AppLocalizations.instance
            .text('enums.medication_route.intravenous'),
        MedicationRoute.Nasal:
            AppLocalizations.instance.text('enums.medication_route.nasal'),
        MedicationRoute.Ocular:
            AppLocalizations.instance.text('enums.medication_route.ocular'),
        MedicationRoute.Otic:
            AppLocalizations.instance.text('enums.medication_route.otic'),
        MedicationRoute.Rectal:
            AppLocalizations.instance.text('enums.medication_route.rectal'),
        MedicationRoute.Subcutaneous: AppLocalizations.instance
            .text('enums.medication_route.subcutaneous'),
        MedicationRoute.Sublingual:
            AppLocalizations.instance.text('enums.medication_route.sublingual'),
        MedicationRoute.Topical:
            AppLocalizations.instance.text('enums.medication_route.topical'),
        MedicationRoute.Transdermal: AppLocalizations.instance
            .text('enums.medication_route.transdermal'),
        MedicationRoute.Vaginal:
            AppLocalizations.instance.text('enums.medication_route.vaginal'),
      };
  List<MedicationItemState> items;
  final scaffoldKey = GlobalKey<ScaffoldState>();
  String id;
  String recipientId;

  TextEditingController titleEditController;

  ValuesetItemState medication;

  int status;
  int route;

  DateTime recordedDate;

  bool isCheckField;

  get onEditMode => id != null;

  get title => onEditMode
      ? AppLocalizations.instance.text('medication.editor.appbar.title_edit')
      : AppLocalizations.instance.text('medication.editor.appbar.title_new');

  @override
  MedicationEditorState clone() {
    return MedicationEditorState()
      ..medication = medication
      ..id = id
      ..recipientId = recipientId
      ..items = items
      ..recordedDate = recordedDate
      ..status = status
      ..route = route
      ..isCheckField = isCheckField
      ..titleEditController = titleEditController;
  }
}

MedicationEditorState initState(Map<String, dynamic> arg) {
  final item =
      arg.containsKey('item') ? arg['item'] as MedicationItemState : null;
  final recipientId =
      arg.containsKey('recipient-id') ? arg['recipient-id'] as String : null;
  final items = arg.containsKey('items')
      ? arg['items'] as List<MedicationItemState>
      : null;

  final state = MedicationEditorState()
    ..medication = ValuesetItemState(
      code: item?.code,
      display: item?.title,
    )
    ..id = item?.id
    ..recipientId = recipientId
    ..items = items
    ..route = item?.route ?? 0
    ..recordedDate = item?.recordedDate ?? DateTime.now()
    ..status = item?.status ?? 0
    ..isCheckField = false
    ..titleEditController = TextEditingController(text: item?.title);
  return state;
}

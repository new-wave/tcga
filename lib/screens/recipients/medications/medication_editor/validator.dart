import 'package:tcga_flutter/utils/app_localizations.dart';

import '../manager.dart';
import 'state.dart';

final _manager = MedicationManager();

class Validator {
  static bool isValid(MedicationEditorState state) {
    if (state.titleEditController.text == '') return false;

    if (state.status == null) return false;

    if (state.route == null) return false;

    if (state.recordedDate == null) return false;

    return true;
  }

  static Future<bool> checkCondition(MedicationEditorState state) async {
    var items = (await _manager.query(recipientId: state.recipientId)).where(
            (element) =>
        element.code == state.medication.code && element.id != state.id);

    if (items.length > 0) return false;

    return true;
  }
}

import 'dart:io';

import 'package:flutter/material.dart';

import 'package:fish_redux/fish_redux.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/database/models/enums.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/utils/enum_to_string.dart';
import 'package:tcga_flutter/widget/import.dart';
import 'package:tcga_flutter/widget/selectable_options_widget.dart';

import 'action.dart';
import 'state.dart';
import 'validator.dart';

Widget buildView(
  MedicationEditorState state,
  Dispatch dispatch,
  ViewService viewService,
) {
  return Builder(builder: (context) {
    return Scaffold(
      key: state.scaffoldKey,
      backgroundColor: Colors.white,
      appBar: _appBar(state, viewService, dispatch),
      body: Padding(
        padding: edgeInsetsBigTop,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
                AppLocalizations.of(viewService.context)
                    .text('condition.editor.name'),
                style: textStylePrimary),
            Container(
              margin: EdgeInsets.symmetric(vertical: spacingSm),
              child: PickerWidget(
                tap: () => dispatch(
                  MedicationEditorActionCreator.selectMedication(),
                ),
                hint: AppLocalizations.of(viewService.context)
                    .text('calendar.editor.placeholder_medication'),
                controller: state.titleEditController,
                suffixIcon: SvgPicture.asset(
                  'assets/icons/ic_search.svg',
                  color: colorPrimary,
                  fit: BoxFit.none,
                ),
                warningText: AppLocalizations.instance.text('common.validator.medication_name'),
                isRequired: true,
                isCheckField: state.isCheckField ?? false,
              ),
            ),
            Container(
              margin: edgeOnlyTopBotPrimaryLarge,
              child: Text(AppLocalizations.of(viewService.context)
                  .text('medication.editor.router')),
            ),
            PickerWidget(
              items: convertEnumToList(MedicationRoute.values),
              initialIndex: state.route,
              hint: AppLocalizations.of(viewService.context)
                  .text('medication.editor.router'),
              indexSelected: (index) => dispatch(
                MedicationEditorActionCreator.handleMedicationRouteSelected(
                  index,
                ),
              ),
            ),
            Container(
              margin: edgeOnlyTopBotPrimaryLarge,
              child: Text(
                AppLocalizations.of(viewService.context)
                    .text('medication.editor.status'),
                style: textStylePrimary,
              ),
            ),
            SelectableOptionsWidget(
              options: convertEnumToList(MedicationStatus.values),
              initialIndex: state.status,
              indexSelected: (index) => dispatch(
                MedicationEditorActionCreator.handleMedicationStatusSelected(
                  index,
                ),
              ),
            ),
            Container(
              margin: edgeOnlyTopBotPrimaryMini,
              child: Text(
                AppLocalizations.of(viewService.context)
                    .text('condition.editor.diagnose_date.label'),
                style: textStylePrimary,
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 8),
              child: DatePickerWidget(
                initial: state.recordedDate,
                max: DateTime.now(),
                min: DateTime.now().subtract(
                  Duration(days: 30),
                ),
                dateSelected: (date) => dispatch(
                  MedicationEditorActionCreator.handleDiagnosedDateSelected(
                    date,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: Builder(
        builder: (context) => Container(
          decoration: BoxDecoration(
              color: colorPrimary, border: Border.all(color: colorPrimary)),
          child: ButtonWidget(
            colorButton: colorPrimary,
            textButton:
                AppLocalizations.of(viewService.context).text('common.save'),
            onPress: () {
              dispatch(MedicationEditorActionCreator.checkValidator());
            },
          ),
        ),
      ),
    );
  });
}

AppBar _appBar(
    MedicationEditorState state, ViewService viewService, Dispatch dispatch) {
  return AppBar(
    backgroundColor: Colors.white,
    elevation: 3,
    centerTitle: false,
    title: Text((state.onEditMode)
        ? AppLocalizations.of(viewService.context)
            .text('medication.editor.appbar.title_edit')
        : AppLocalizations.of(viewService.context)
            .text('medication.editor.appbar.title_new')),
    leading: GestureDetector(
      onTap: () => Navigator.pop(viewService.context),
      child: Container(
        margin: EdgeInsets.only(left: 16),
        child: SvgPicture.asset('assets/icons/ic_arrow_left_circle.svg'),
      ),
    ),
    leadingWidth: 46,
    actions: [
      if (state.onEditMode)
        IconButton(
          iconSize: 24,
          icon: SvgPicture.asset('assets/icons/ic_trash.svg'),
          onPressed: () => dispatch(
            MedicationEditorActionCreator.delete(),
          ),
        )
    ],
  );
}

import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/valuesets/valueset_picker_page/valueset_item_component/state.dart';

enum MedicationEditorAction {
  save,
  delete,
  selectMedication,
  handleMedicationSelected,
  handleRecordedDateSelected,
  handleMedicationRouteSelected,
  handleMedicationStatusSelected,
  checkValidator,
  checkField,
}

class MedicationEditorActionCreator {
  static Action save() {
    return const Action(
      MedicationEditorAction.save,
    );
  }

  static Action checkField() {
    return const Action(
      MedicationEditorAction.checkField,
    );
  }

  static Action checkValidator() {
    return Action(
      MedicationEditorAction.checkValidator,
    );
  }

  static Action delete() {
    return const Action(
      MedicationEditorAction.delete,
    );
  }

  static Action selectMedication() {
    return const Action(
      MedicationEditorAction.selectMedication,
    );
  }

  static Action handleMedicationSelected(ValuesetItemState item) {
    return Action(
      MedicationEditorAction.handleMedicationSelected,
      payload: item,
    );
  }

  static Action handleDiagnosedDateSelected(DateTime date) {
    return Action(
      MedicationEditorAction.handleRecordedDateSelected,
      payload: date,
    );
  }

  static Action handleMedicationRouteSelected(int index) {
    return Action(
      MedicationEditorAction.handleMedicationRouteSelected,
      payload: index,
    );
  }

  static Action handleMedicationStatusSelected(int index) {
    return Action(
      MedicationEditorAction.handleMedicationStatusSelected,
      payload: index,
    );
  }
}

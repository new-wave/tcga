import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/valuesets/valueset_picker_page/valueset_item_component/state.dart';

import 'action.dart';
import 'state.dart';

Reducer<MedicationEditorState> buildReducer() {
  return asReducer(
    <Object, Reducer<MedicationEditorState>>{
      MedicationEditorAction.handleMedicationSelected:
          _handleMedicationSelected,
      MedicationEditorAction.handleMedicationStatusSelected:
          _handleMedicationStatusSelected,
      MedicationEditorAction.handleRecordedDateSelected:
          _handleRecordedDateSelected,
      MedicationEditorAction.handleMedicationRouteSelected:
          _handleMedicationRouteSelected,
      MedicationEditorAction.checkField: _checkFieldReducer,
    },
  );
}

MedicationEditorState _checkFieldReducer(MedicationEditorState state, Action action) {
  final MedicationEditorState newState = state.clone();
  newState.isCheckField = true;
  return newState;
}

MedicationEditorState _handleMedicationSelected(
  MedicationEditorState state,
  Action action,
) {
  final item = action.payload as ValuesetItemState;
  final newState = state.clone()..medication = item;
  newState.titleEditController.text = item.display;
  return newState;
}

MedicationEditorState _handleMedicationRouteSelected(
  MedicationEditorState state,
  Action action,
) {
  final index = action.payload as int;
  final MedicationEditorState newState = state.clone();
  newState.route = index;
  return newState;
}

MedicationEditorState _handleMedicationStatusSelected(
  MedicationEditorState state,
  Action action,
) {
  final index = action.payload as int;
  final MedicationEditorState newState = state.clone();
  newState.status = index;
  return newState;
}

MedicationEditorState _handleRecordedDateSelected(
  MedicationEditorState state,
  Action action,
) {
  final date = action.payload as DateTime;
  final newState = state.clone()..recordedDate = date;
  return newState;
}

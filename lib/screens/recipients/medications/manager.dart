import 'package:tcga_flutter/screens/recipients/medications/medication_editor/state.dart';

import '../../../database/models.dart';
import '../../../database/repository.dart';

import 'medications_page/medication_item_component/state.dart';
import 'mapper.dart';

class MedicationManager {
  static final _singleton = MedicationManager._internal();

  factory MedicationManager() {
    return _singleton;
  }

  MedicationManager._internal();

  final _medicationRepository = Repository<Medication>();

  Future<List<MedicationItemState>> query({
    int pageSize = 100,
    String recipientId,
  }) async {
    final items = await _medicationRepository.query(
        filter: (x) => x.personId == recipientId);

    return items.map((x) => x.toState()).toList();
  }

  Future<bool> delete(String id) {
    return _medicationRepository.delete(id);
  }

  Future<MedicationItemState> update(MedicationEditorState state) async {
    final entity = state.toEntity();

    final _ = await _medicationRepository.update(entity);

    return entity.toState();
  }

  Future<MedicationItemState> create(MedicationEditorState state) async {
    final entity = state.toEntity();

    entity.id = await _medicationRepository.create(entity);

    return entity.toState();
  }
}

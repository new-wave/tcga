import 'package:tcga_flutter/database/models.dart';
import 'package:tcga_flutter/database/models/enums.dart';

import 'medication_editor/state.dart';
import 'medications_page/medication_item_component/state.dart';

extension EntityToStateMapper on Medication {
  MedicationItemState toState() => MedicationItemState(
        id: this.id,
        route: this.route,
        code: this.code,
        title: this.name,
        recordedDate: this.recordedDate,
        status: this.status,
      );
}

extension StateToEntityMapper on MedicationEditorState {
  Medication toEntity() => Medication(
        id: this.id,
        route: this.route,
        code: this.medication.code,
        name: this.medication.display,
        personId: this.recipientId,
        recordedDate: this.recordedDate,
        status: this.status,
      );
}

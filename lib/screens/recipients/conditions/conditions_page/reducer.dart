import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/expenses/expenses/history_tab_component/reducer.dart';

import 'action.dart';
import 'condition_item_component/state.dart';
import 'state.dart';

Reducer<ConditionsState> buildReducer() {
  return asReducer(
    <Object, Reducer<ConditionsState>>{
      ConditionsAction.initialize: _initialize,
      ConditionsAction.handleConditionCreated: _handleConditionAdded,
      ConditionsAction.handleConditionEdited: _handleConditionEdited,
      ConditionsAction.handleConditionDeleted: _handleConditionDeleted,
    },
  );
}

ConditionsState _initialize(ConditionsState state, Action action) {
  final ConditionsState newState = state.clone();

  newState.items = action.payload as List<ConditionItemState> ?? [];

  return newState;
}

ConditionsState _handleConditionAdded(ConditionsState state, Action action) {
  final item = action.payload as ConditionItemState;
  final newState = state.clone();

  if (item.isPrimary == true) {
    newState.primaryCondition = item;
  }
  newState.items.add(item);

  return newState;
}

ConditionsState _handleConditionEdited(ConditionsState state, Action action) {
  final item = action.payload as ConditionItemState;
  final newState = state.clone();

  final editedIndex = newState.items.indexWhere(
    (x) => x.id == item.id,
  );

  newState.items[editedIndex] = item;

  if (item.isPrimary == true) {
    newState.primaryCondition = item;
  } else {
    if (newState.items.where((element) => element.isPrimary == true).length <
        1) {
      newState.primaryCondition = null;
    }
  }

  return newState;
}

ConditionsState _handleConditionDeleted(ConditionsState state, Action action) {
  final id = action.payload;
  final newState = state.clone();

  final deletedIndex = newState.items.indexWhere(
    (x) => x.id == id,
  );

  newState.items.removeAt(deletedIndex);

  return newState;
}

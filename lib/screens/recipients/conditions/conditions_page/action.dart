import 'package:fish_redux/fish_redux.dart';

import 'condition_item_component/state.dart';

enum ConditionsAction {
  initialize,
  create,
  handleConditionCreated,
  edit,
  handleConditionEdited,
  delete,
  handleConditionDeleted,
  addNote,
}

class ConditionsActionCreator {
  static Action initialize(List<ConditionItemState> items) {
    return Action(
      ConditionsAction.initialize,
      payload: items,
    );
  }

  static Action edit(ConditionItemState item) {
    return Action(
      ConditionsAction.edit,
      payload: item,
    );
  }

  static Action handleConditionEdited(ConditionItemState item) {
    return Action(
      ConditionsAction.handleConditionEdited,
      payload: item,
    );
  }

  static Action addNote(ConditionItemState item) {
    return Action(
      ConditionsAction.addNote,
      payload: item,
    );
  }

  static Action delete(ConditionItemState item) {
    return Action(
      ConditionsAction.delete,
      payload: item,
    );
  }

  static Action handleConditionDeleted(String id) {
    return Action(
      ConditionsAction.handleConditionDeleted,
      payload: id,
    );
  }

  static Action create(String recipientId) {
    return Action(
      ConditionsAction.create,
      payload: recipientId,
    );
  }

  static Action handleConditionCreated(ConditionItemState item) {
    return Action(
      ConditionsAction.handleConditionCreated,
      payload: item,
    );
  }
}

import 'package:fish_redux/fish_redux.dart';

import 'state.dart';
import 'view.dart';

class ConditionItemComponent extends Component<ConditionItemState> {
  ConditionItemComponent()
      : super(
          view: buildView,
        );
}

import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';

import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/database/models/enums.dart';
import 'package:tcga_flutter/utils/enum_to_string.dart';
import 'package:tcga_flutter/widget/list_item_widget.dart';

import '../action.dart';
import 'state.dart';

Widget buildView(
  ConditionItemState state,
  Dispatch dispatch,
  ViewService viewService,
) {
  return SizedBox(
    height: 70,
    child: ListItemWidget(
      contextActions: [
        'recipients.popup.add_note',
        'recipients.popup.edit',
        'recipients.popup.delete',
      ],
      onTap: () => dispatch(
        ConditionsActionCreator.edit(
          state,
        ),
      ),
      onContextActionSelected: (String action) {
        switch (action) {
          case 'recipients.popup.add_note':
            {
              dispatch(
                ConditionsActionCreator.addNote(
                  state,
                ),
              );
            }
            break;
          case 'recipients.popup.edit':
            {
              dispatch(
                ConditionsActionCreator.edit(
                  state,
                ),
              );
            }
            break;
          case 'recipients.popup.delete':
            {
              dispatch(
                ConditionsActionCreator.delete(
                  state,
                ),
              );
            }
            break;
        }
      },
      child: Container(
        margin: EdgeInsets.only(left: spacingPrimary),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              state.title,
              style: TextStyle(
                color: colorTextPrimary,
                fontSize: 16,
                fontWeight: FontWeight.w600,
              ),
              overflow: TextOverflow.ellipsis,
              maxLines: 1,
              softWrap: false,
            ),
            SizedBox(
              height: 4,
            ),
            Container(
              child: Text(
                '${convertEnumToList(ConditionStatus.values)[state.status]} • ${convertEnumToList(ChronicityType.values)[state.chronicity]}',
                style: textStyleSmall,
              ),
            ),
            SizedBox(
              height: 4,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Text(
                  'State Date: ${state.date.format(state.diagnoseDate)}',
                  style: textStyleSmall,
                ),
                Spacer(),
                Text(
                  '${convertEnumToList(ConditionType.values)[state.type]}',
                  style: textStyleMiniWeight,
                )
              ],
            ),
          ],
        ),
      ),
    ),
  );
}

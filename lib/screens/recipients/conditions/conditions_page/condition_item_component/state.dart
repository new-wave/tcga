import 'package:fish_redux/fish_redux.dart';
import 'package:intl/intl.dart';
import 'package:tcga_flutter/database/models/enums.dart';

class ConditionItemState implements Cloneable<ConditionItemState> {
  String id;
  String title;
  String code;
  int type;
  int chronicity;
  int status;
  DateTime diagnoseDate;
  bool isPrimary;
  var date = new DateFormat('d/MM/y');

  ConditionItemState({
    this.id,
    this.title,
    this.code,
    this.chronicity,
    this.status,
    this.diagnoseDate,
    this.isPrimary,
    this.type,
  });

  @override
  ConditionItemState clone() {
    return ConditionItemState()
      ..id = id
      ..title = title
      ..code = code
      ..type = type
      ..chronicity = chronicity
      ..status = status
      ..diagnoseDate = diagnoseDate
      ..isPrimary = isPrimary;
  }
}

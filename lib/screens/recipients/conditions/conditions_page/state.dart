import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/expenses/expenses/history_tab_component/reducer.dart';

import '../../recipient_dashboard/recipient_header_component/state.dart';
import '../../state.dart';

import 'condition_item_component/state.dart';

class ConditionsState extends MutableSource
    implements Cloneable<ConditionsState> {
  List<ConditionItemState> items;
  RecipientItemState recipient;
  ConditionItemState primaryCondition;

  @override
  ConditionsState clone() {
    return ConditionsState()
      ..items = items
      ..recipient = recipient
      ..primaryCondition = primaryCondition;
  }

  @override
  Object getItemData(int index) => items[index];

  @override
  String getItemType(int index) => 'condition-item';

  @override
  int get itemCount => items?.length ?? 0;

  @override
  void setItemData(int index, Object data) => items[index] = data;
}

ConditionsState initState(Map<String, dynamic> arg) {
  return ConditionsState()..recipient = arg['recipient'];
}

class RecipientHeaderStateConnector
    extends ConnOp<ConditionsState, RecipientHeaderState>
    with ReselectMixin<ConditionsState, RecipientHeaderState> {
  @override
  RecipientHeaderState computed(ConditionsState state) {
    return RecipientHeaderState(
      title: state.primaryCondition?.title,
      type: state.primaryCondition?.type,
      recipient: state.recipient,
    );
  }

  @override
  List<dynamic> factors(ConditionsState state) {
    return [
      state?.primaryCondition?.title,
      state?.primaryCondition?.type,
      state.recipient,
    ];
  }
}

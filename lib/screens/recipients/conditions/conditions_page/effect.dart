import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/database/models/enums.dart';
import 'package:tcga_flutter/screens/recipients/conditions/conditions_page/condition_item_component/state.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';

import '../../../../route_name.dart';
import '../../../../utils/utils.dart';

import '../manager.dart';
import 'action.dart';
import 'state.dart';

final _manager = ConditionManager();

Effect<ConditionsState> buildEffect() {
  return combineEffects(<Object, Effect<ConditionsState>>{
    Lifecycle.initState: _init,
    ConditionsAction.create: _create,
    ConditionsAction.edit: _edit,
    ConditionsAction.delete: _delete,
    ConditionsAction.addNote: _addNote,
  });
}

Future _init(
  Action action,
  Context<ConditionsState> ctx,
) async {
  final items = await _manager.query(
    recipientId: ctx.state.recipient.id,
  );

  for (int i = 0; i < items.length; i++) {
    // if (items.where((element) => element.isPrimary == true) != null) {
    //   ctx.state.primaryCondition = items[i];
    // }
    ctx.state.primaryCondition =
        items.firstWhere((element) => element.isPrimary == true, orElse: () {
      return null;
    });
  }

  ctx.dispatch(
    ConditionsActionCreator.initialize(
      items,
    ),
  );
}

Future _create(Action action, Context<ConditionsState> ctx) async {
  final result = await ctx.navigateTo(
    RouteName.recipientsConditionEdit,
    arguments: {
      'recipient-id': ctx.state.recipient.id,
      'items': ctx.state.items,
    },
  );

  if (result == null) return;

  ctx.dispatch(
    ConditionsActionCreator.handleConditionCreated(
      result,
    ),
  );
}

Future _edit(Action action, Context<ConditionsState> ctx) async {
  final result = await ctx.navigateTo(
    RouteName.recipientsConditionEdit,
    arguments: {
      'item': action.payload,
      'recipient-id': ctx.state.recipient.id,
      'items': ctx.state.items,
    },
  );

  if (result == null) return;
  if (!(result is ConditionItemState)) {
    ctx.dispatch(
      ConditionsActionCreator.handleConditionEdited(
        action.payload,
      ),
    );
  }
  if (result is ConditionItemState) {
    ctx.dispatch(
      ConditionsActionCreator.handleConditionEdited(
        result,
      ),
    );
  } else {
    ctx.dispatch(
      ConditionsActionCreator.handleConditionDeleted(
        action.payload.id,
      ),
    );
  }
}

Future _delete(Action action, Context<ConditionsState> ctx) async {
  final confirmed = await ctx.confirm(
    title: AppLocalizations.instance.text('common.warning.delete.title'),
    message: AppLocalizations.instance.text('common.warning.delete.desc'),
    negativeActionTitle: AppLocalizations.instance.text('common.cancel'),
    positiveActionTitle: AppLocalizations.instance.text('common.sure'),
  );

  if (!confirmed) return;

  final deletedItemId = action.payload.id;
  final deleted = await _manager.delete(deletedItemId);

  if (!deleted) {
    return;
  }

  ctx.dispatch(
    ConditionsActionCreator.handleConditionDeleted(
      deletedItemId,
    ),
  );
}

Future _addNote(Action action, Context<ConditionsState> ctx) {
  return ctx.navigateTo(
    RouteName.notesEditor,
    arguments: {
      'item-id': action.payload.id,
      'type': NoteType.Condition,
      'recipient-id': ctx.state.recipient.id,
    },
  );
}

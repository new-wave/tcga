import 'package:fish_redux/fish_redux.dart';

import '../../recipient_dashboard/recipient_header_component/component.dart';

import 'conditions_adapter/adapter.dart';
import 'effect.dart';
import 'state.dart';
import 'view.dart';
import 'reducer.dart';

class ConditionsPage extends Page<ConditionsState, Map<String, dynamic>> {
  ConditionsPage()
      : super(
          initState: initState,
          effect: buildEffect(),
          reducer: buildReducer(),
          view: buildView,
          dependencies: Dependencies<ConditionsState>(
            adapter: NoneConn<ConditionsState>() + ConditionsAdapter(),
            slots: <String, Dependent<ConditionsState>>{
              'recipient-header':
                  RecipientHeaderStateConnector() + RecipientHeaderComponent(),
            },
          ),
        );
}

import 'package:fish_redux/fish_redux.dart';

import '../condition_item_component/component.dart';
import '../state.dart';

class ConditionsAdapter extends SourceFlowAdapter<ConditionsState> {
  ConditionsAdapter()
      : super(
          pool: <String, Component<Object>>{
            'condition-item': ConditionItemComponent()
          },
        );
}

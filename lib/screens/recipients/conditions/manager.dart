import 'package:tcga_flutter/screens/recipients/conditions/condition_editor/state.dart';

import '../../../database/models.dart';
import '../../../database/repository.dart';

import 'conditions_page/condition_item_component/state.dart';
import 'mapper.dart';

class ConditionManager {
  static final _singleton = ConditionManager._internal();

  factory ConditionManager() {
    return _singleton;
  }

  ConditionManager._internal();

  final _conditionRepository = Repository<Condition>();

  Future<List<ConditionItemState>> query({
    int pageSize = 100,
    String recipientId,
  }) async {
    final items = await _conditionRepository.query(
        filter: (x) => x.personId == recipientId);

    return items.map((x) => x.toState()).toList();
  }

  Future<bool> delete(String id) {
    return _conditionRepository.delete(id);
  }

  Future<ConditionItemState> update(ConditionEditorState state) async {
    final entity = state.toEntity();

    final _ = await _conditionRepository.update(entity);

    return entity.toState();
  }

  Future<ConditionItemState> create(ConditionEditorState state) async {
    final entity = state.toEntity();

    entity.id = await _conditionRepository.create(entity);

    return entity.toState();
  }

  Future<List<Condition>> getPrimaryCondition({String personId}) async {
    final items = await _conditionRepository.query();

    var item = items.where(
        (e) => e.toState().id == personId && e.toState().isPrimary == true);

    return item;
  }
}

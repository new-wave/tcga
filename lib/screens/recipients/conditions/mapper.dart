import 'package:tcga_flutter/database/models.dart';
import 'package:tcga_flutter/database/models/enums.dart';

import 'condition_editor/state.dart';
import 'conditions_page/condition_item_component/state.dart';

extension EntityToStateMapper on Condition {
  ConditionItemState toState() => ConditionItemState(
        id: this.id,
        chronicity: this.chronicity,
        code: this.code,
        title: this.name,
        diagnoseDate: this.recordedDate,
        isPrimary: this.isPrimary,
        status: this.status,
        type: this.type,
      );
}

extension StateToEntityMapper on ConditionEditorState {
  Condition toEntity() => Condition(
        id: this.id,
        chronicity: this.chronicity,
        code: this.condition.code,
        name: this.condition.display,
        isPrimary: this.isPrimary,
        personId: this.recipientId,
        recordedDate: this.diagnosedDate,
        status: this.status,
        type: this.type,
      );
}

import 'package:flutter/material.dart' hide Action, Page;

import 'package:fish_redux/fish_redux.dart';

import 'package:tcga_flutter/database/models/enums.dart';
import 'package:tcga_flutter/screens/valuesets/valueset_picker_page/valueset_item_component/state.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';

import '../conditions_page/condition_item_component/state.dart';

class ConditionEditorState implements Cloneable<ConditionEditorState> {
  Map<ConditionType, String> get conditionTypes => {
        ConditionType.Behavioral: AppLocalizations.instance
            .text('condition.condition_type.behavioral'),
        ConditionType.Medical:
            AppLocalizations.instance.text('condition.condition_type.medical'),
      };

  Map<ChronicityType, String> get chronicityTypes => {
        ChronicityType.Acute:
            AppLocalizations.instance.text('condition.chronicity_type.acute'),
        ChronicityType.Chronic:
            AppLocalizations.instance.text('condition.chronicity_type.chronic'),
      };

  Map<ConditionStatus, String> get conditionStatuses => {
        ConditionStatus.Active:
            AppLocalizations.instance.text('condition.condition_status.active'),
        ConditionStatus.Inactive: AppLocalizations.instance
            .text('condition.condition_status.inactive'),
      };
  final scaffoldKey = GlobalKey<ScaffoldState>();

  List<ConditionItemState> items;

  String id;
  String recipientId;

  TextEditingController titleEditController;

  ValuesetItemState condition;

  int type;
  int chronicity;
  int status;

  DateTime diagnosedDate;
  bool isPrimary;

  bool isCheckField;

  get onEditMode => id != null;

  get title => onEditMode
      ? AppLocalizations.instance.text('condition.title.edit')
      : AppLocalizations.instance.text('condition.title.new');

  @override
  ConditionEditorState clone() {
    return ConditionEditorState()
      ..items = items
      ..condition = condition
      ..id = id
      ..recipientId = recipientId
      ..chronicity = chronicity
      ..diagnosedDate = diagnosedDate
      ..status = status
      ..isPrimary = isPrimary
      ..type = type
      ..titleEditController = titleEditController
      ..isCheckField = isCheckField;
  }
}

ConditionEditorState initState(Map<String, dynamic> arg) {
  final item =
      arg.containsKey('item') ? arg['item'] as ConditionItemState : null;
  final recipientId =
      arg.containsKey('recipient-id') ? arg['recipient-id'] as String : null;
  final items = arg.containsKey('items')
      ? arg['items'] as List<ConditionItemState>
      : null;

  final state = ConditionEditorState()
    ..condition = ValuesetItemState(
      code: item?.code,
      display: item?.title,
    )
    ..id = item?.id
    ..recipientId = recipientId
    ..items = items
    ..chronicity = item?.chronicity ?? 0
    ..diagnosedDate = item?.diagnoseDate ?? DateTime.now()
    ..status = item?.status ?? 0
    ..type = item?.type ?? 0
    ..isPrimary = item?.isPrimary ?? false
    ..isCheckField = false
    ..titleEditController = TextEditingController(text: item?.title);
  return state;
}

import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/valuesets/valueset_picker_page/valueset_item_component/state.dart';

enum ConditionEditorAction {
  save,
  delete,
  selectCondition,
  handleConditionSelected,
  handleConditionTypeSelected,
  handleDiagnosedDateSelected,
  toggleIsPrimary,
  handleChronicityTypeSelected,
  handleConditionStatusSelected,
  checkValidator,
  checkField,
}

class ConditionEditorActionCreator {
  static Action save() {
    return const Action(
      ConditionEditorAction.save,
    );
  }

  static Action checkField() {
    return const Action(
      ConditionEditorAction.checkField,
    );
  }

  static Action checkValidator() {
    return const Action(
      ConditionEditorAction.checkValidator,
    );
  }

  static Action delete() {
    return const Action(
      ConditionEditorAction.delete,
    );
  }

  static Action selectCondition() {
    return const Action(
      ConditionEditorAction.selectCondition,
    );
  }

  static Action handleConditionSelected(ValuesetItemState item) {
    return Action(
      ConditionEditorAction.handleConditionSelected,
      payload: item,
    );
  }

  static Action handleConditionTypeSelected(int index) {
    return Action(
      ConditionEditorAction.handleConditionTypeSelected,
      payload: index,
    );
  }

  static Action handleDiagnosedDateSelected(DateTime date) {
    return Action(
      ConditionEditorAction.handleDiagnosedDateSelected,
      payload: date,
    );
  }

  static Action toggleIsPrimary() {
    return Action(
      ConditionEditorAction.toggleIsPrimary,
    );
  }

  static Action handleChronicityTypeSelected(int index) {
    return Action(
      ConditionEditorAction.handleChronicityTypeSelected,
      payload: index,
    );
  }

  static Action handleConditionStatusSelected(int index) {
    return Action(
      ConditionEditorAction.handleConditionStatusSelected,
      payload: index,
    );
  }
}

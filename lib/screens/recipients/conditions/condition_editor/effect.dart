import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart' hide Action, Page;
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/database/models/enums.dart';
import 'package:tcga_flutter/screens/valuesets/valueset_picker_page/valueset_item_component/state.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';

import '../../../../utils/utils.dart';
import '../manager.dart';

import 'action.dart';
import 'state.dart';
import 'validator.dart';

final _manager = ConditionManager();

Effect<ConditionEditorState> buildEffect() {
  return combineEffects(<Object, Effect<ConditionEditorState>>{
    ConditionEditorAction.save: _save,
    ConditionEditorAction.delete: _delete,
    ConditionEditorAction.selectCondition: _selectCondition,
    ConditionEditorAction.checkValidator: _checkValidator,
  });
}

Future _save(
  Action action,
  Context<ConditionEditorState> ctx,
) async {
  final state = ctx.state;

  final result = state.onEditMode
      ? await _manager.update(state)
      : await _manager.create(state);

  if (result == null) return;

  ctx.goBack(result);
}

Future _checkValidator(
  Action action,
  Context<ConditionEditorState> ctx,
) async {
  final state = ctx.state;

  bool isValid = Validator.isValid(state);
  if (!isValid) {
    ctx.dispatch(ConditionEditorActionCreator.checkField());
    return;
  }

  if (!await Validator.checkCondition(state)) {
    ctx.state.scaffoldKey.currentState.hideCurrentSnackBar();
    final Widget snackBar = SnackBar(
      content: Container(
        child: Text(
          AppLocalizations.of(ctx.context)
              .text('condition.editor.error.condition_name_false'),
        ),
      ),
      backgroundColor: colorIndicatorWarning,
    );

    ctx.state.scaffoldKey.currentState.showSnackBar(snackBar);
    return;
  }

  return ctx.dispatch(
    ConditionEditorActionCreator.save(),
  );
}

Future _delete(
  Action action,
  Context<ConditionEditorState> ctx,
) async {
  final confirmed = await ctx.confirm(
    title: AppLocalizations.instance.text('common.warning.delete.title'),
    message: AppLocalizations.instance.text('common.warning.delete.desc'),
    negativeActionTitle: AppLocalizations.instance.text('common.cancel'),
    positiveActionTitle: AppLocalizations.instance.text('common.sure'),
  );

  if (!confirmed) return;

  final deletedItemId = ctx.state.id;
  final deleted = await _manager.delete(deletedItemId);

  if (!deleted) {
    // TODO show error message
    return;
  }

  ctx.goBack(
    {
      'deleted-item-id': deletedItemId,
    },
  );
}

Future _selectCondition(
  Action action,
  Context<ConditionEditorState> ctx,
) async {
  var selectedItem = await ctx.pickValuesetItem(
    ValueSetType.condition,
    confirmRequired: true,
  ) as ValuesetItemState;

  if (selectedItem != null) {
    ctx.dispatch(
      ConditionEditorActionCreator.handleConditionSelected(
        selectedItem,
      ),
    );
  }
}

import 'dart:io';

import 'package:flutter/material.dart';

import 'package:fish_redux/fish_redux.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/database/models/enums.dart';
import 'package:tcga_flutter/screens/recipients/conditions/conditions_page/action.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/utils/enum_to_string.dart';
import 'package:tcga_flutter/widget/import.dart';
import 'package:tcga_flutter/widget/selectable_options_widget.dart';

import 'action.dart';
import 'state.dart';
import 'validator.dart';

Widget buildView(
  ConditionEditorState state,
  Dispatch dispatch,
  ViewService viewService,
) {
  return Builder(builder: (context) {
    return Scaffold(
      key: state.scaffoldKey,
      backgroundColor: Colors.white,
      appBar: _appBar(state, viewService, dispatch),
      body: Padding(
        padding: edgeInsetsBigTop,
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                  AppLocalizations.of(viewService.context)
                      .text('condition.editor.name'),
                  style: textStylePrimary),
              Container(
                margin: EdgeInsets.symmetric(vertical: spacingSm),
                child: PickerWidget(
                  tap: () => dispatch(
                    ConditionEditorActionCreator.selectCondition(),
                  ),
                  hint: AppLocalizations.of(viewService.context)
                      .text('condition.editor.is_primary.entry.placehodel'),
                  controller: state.titleEditController,
                  suffixIcon: SvgPicture.asset(
                    'assets/icons/ic_search.svg',
                    color: colorPrimary,
                    fit: BoxFit.none,
                  ),
                  warningText: AppLocalizations.instance.text('common.validator.condition_name'),
                  isRequired: true,
                  isCheckField: state.isCheckField ?? false,
                ),
              ),
              Container(
                margin: edgeOnlyTopBotPrimaryLarge,
                child: Text(AppLocalizations.instance
                    .text('condition.editor.condition_type.label')),
              ),
              SelectableOptionsWidget(
                options: convertEnumToList(ConditionType.values),
                initialIndex: state.type,
                indexSelected: (index) => dispatch(
                  ConditionEditorActionCreator.handleConditionTypeSelected(
                    index,
                  ),
                ),
              ),
              Container(
                margin: edgeOnlyTopBotPrimaryLarge,
                child: Text(
                  AppLocalizations.of(viewService.context)
                      .text('condition.editor.chronicity_type.label'),
                  style: textStylePrimary,
                ),
              ),
              SelectableOptionsWidget(
                options: convertEnumToList(ChronicityType.values),
                initialIndex: state.chronicity,
                indexSelected: (index) => dispatch(
                  ConditionEditorActionCreator.handleChronicityTypeSelected(
                    index,
                  ),
                ),
              ),
              Container(
                margin: edgeOnlyTopBotPrimaryLarge,
                child: Text(
                  AppLocalizations.of(viewService.context)
                      .text('condition.editor.status'),
                  style: textStylePrimary,
                ),
              ),
              SelectableOptionsWidget(
                options: convertEnumToList(ConditionStatus.values),
                initialIndex: state.status,
                indexSelected: (index) => dispatch(
                  ConditionEditorActionCreator.handleConditionStatusSelected(
                    index,
                  ),
                ),
              ),
              Container(
                margin: edgeOnlyTopBotPrimaryMini,
                child: Text(
                  AppLocalizations.of(viewService.context)
                      .text('condition.editor.diagnose_date.label'),
                  style: textStylePrimary,
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 8),
                child: DatePickerWidget(
                  initial: state.diagnosedDate,
                  max: DateTime.now(),
                  min: DateTime.now().subtract(
                    Duration(days: 30),
                  ),
                  dateSelected: (date) => dispatch(
                    ConditionEditorActionCreator.handleDiagnosedDateSelected(
                      date,
                    ),
                  ),
                ),
              ),
              Container(
                child: GestureDetector(
                  onTap: () => dispatch(
                    ConditionEditorActionCreator.toggleIsPrimary(),
                  ),
                  child: Row(
                    children: [
                      Checkbox(
                        hoverColor: Colors.pink,
                        activeColor: colorPrimary,
                        value: state.isPrimary,
                        onChanged: (value) {
                          dispatch(
                              ConditionEditorActionCreator.toggleIsPrimary());
                        },
                      ),
                      Text(
                        AppLocalizations.of(viewService.context)
                            .text('condition.editor.is_primary.label'),
                        style: textStylePrimary,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: Builder(
        builder: (context) => Container(
          decoration: BoxDecoration(
              color: colorPrimary,
              border: Border.all(
                color: colorPrimary,
              )),
          child: ButtonWidget(
            colorButton: colorPrimary,
            textButton:
                AppLocalizations.of(viewService.context).text('common.save'),
            onPress: () {
              dispatch(ConditionEditorActionCreator.checkValidator());
            },
          ),
        ),
      ),
    );
  });
}

AppBar _appBar(
    ConditionEditorState state, ViewService viewService, Dispatch dispatch) {
  return AppBar(
    elevation: 3,
    centerTitle: false,
    title: Text(
      (state.onEditMode)
          ? AppLocalizations.of(viewService.context)
              .text('condition.editor.appbar.title_edit')
          : AppLocalizations.of(viewService.context)
              .text('condition.editor.appbar.title_new'),
    ),
    leading: GestureDetector(
      onTap: () => Navigator.pop(viewService.context),
      child: Container(
        margin: EdgeInsets.only(left: 16),
        child: SvgPicture.asset('assets/icons/ic_arrow_left_circle.svg'),
      ),
    ),
    leadingWidth: 46,
    actions: [
      if (state.onEditMode)
        IconButton(
          iconSize: 24,
          icon: SvgPicture.asset('assets/icons/ic_trash.svg'),
          onPressed: () => dispatch(
            ConditionEditorActionCreator.delete(),
          ),
        )
    ],
  );
}

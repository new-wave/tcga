import 'package:tcga_flutter/screens/recipients/conditions/conditions_page/state.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';

import '../manager.dart';
import 'state.dart';

final _manager = ConditionManager();

class Validator {
  static bool isValid(ConditionEditorState state) {
    if (state.titleEditController.text == '') return false;

    // if (state.chronicity == null) return false;
    //
    // if (state.type == null) return false;
    //
    // if (state.status == null) return false;
    //
    // if (state.diagnosedDate == null) return false;

    return true;
  }

  static Future<bool> checkCondition(ConditionEditorState state) async {
    var items = (await _manager.query(recipientId: state.recipientId)).where(
        (element) =>
            element.code == state.condition.code && element.id != state.id);

    if (items.length > 0) return false;

    return true;
  }
}

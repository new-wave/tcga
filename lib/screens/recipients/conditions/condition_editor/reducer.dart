import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/valuesets/valueset_picker_page/valueset_item_component/state.dart';

import 'action.dart';
import 'state.dart';

Reducer<ConditionEditorState> buildReducer() {
  return asReducer(
    <Object, Reducer<ConditionEditorState>>{
      ConditionEditorAction.handleConditionSelected: _handleConditionSelected,
      ConditionEditorAction.handleConditionTypeSelected:
          _handleConditionTypeSelected,
      ConditionEditorAction.handleConditionStatusSelected:
          _handleConditionStatusSelected,
      ConditionEditorAction.handleDiagnosedDateSelected:
          _handleDiagnosedDateSelected,
      ConditionEditorAction.toggleIsPrimary: _toggleIsPrimary,
      ConditionEditorAction.handleChronicityTypeSelected:
          _handleChronicityTypeSelected,
      ConditionEditorAction.checkField: _checkFieldReducer,
    },
  );
}

ConditionEditorState _checkFieldReducer(ConditionEditorState state, Action action) {
  final ConditionEditorState newState = state.clone();
  newState.isCheckField = true;
  return newState;
}

ConditionEditorState _handleConditionSelected(
  ConditionEditorState state,
  Action action,
) {
  final item = action.payload as ValuesetItemState;
  final newState = state.clone()..condition = item;
  newState.titleEditController.text = item.display;
  return newState;
}

ConditionEditorState _handleConditionTypeSelected(
  ConditionEditorState state,
  Action action,
) {
  final index = action.payload as int;
  final ConditionEditorState newState = state.clone();
  newState.type = index;
  return newState;
}

ConditionEditorState _handleChronicityTypeSelected(
  ConditionEditorState state,
  Action action,
) {
  final index = action.payload as int;
  final ConditionEditorState newState = state.clone();
  newState.chronicity = index;
  return newState;
}

ConditionEditorState _handleConditionStatusSelected(
  ConditionEditorState state,
  Action action,
) {
  final index = action.payload as int;
  final ConditionEditorState newState = state.clone();
  newState.status = index;
  return newState;
}

ConditionEditorState _handleDiagnosedDateSelected(
  ConditionEditorState state,
  Action action,
) {
  final date = action.payload as DateTime;
  final newState = state.clone()..diagnosedDate = date;
  return newState;
}

ConditionEditorState _toggleIsPrimary(
  ConditionEditorState state,
  Action action,
) {
  for(int i = 0; i < state.items.length; i++){
    state.items[i].isPrimary = false;
  }
  final newState = state.clone()..isPrimary = !state.isPrimary;

  return newState;
}

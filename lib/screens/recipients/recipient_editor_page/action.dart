import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/recipients/recipient_editor_page/state.dart';

enum RecipientEditorAction {
  initialize,
  checkField,
  save,
  delete,
}

class RecipientEditorActionCreator {
  static Action save() {
    return const Action(
      RecipientEditorAction.save,
    );
  }

  static Action checkField() {
    return const Action(
      RecipientEditorAction.checkField,
    );
  }

  static Action delete() {
    return const Action(
      RecipientEditorAction.delete,
    );
  }

  static Action initialize(RecipientEditorState result) {
    return Action(
      RecipientEditorAction.initialize,
      payload: result,
    );
  }
}

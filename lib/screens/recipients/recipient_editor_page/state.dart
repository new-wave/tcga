import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/address_item_component/state.dart';
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/state.dart';
import 'package:tcga_flutter/screens/global_widgets/emergency_contact/contact_item_component/state.dart';
import 'package:tcga_flutter/screens/global_widgets/emergency_contact/state.dart';
import 'package:tcga_flutter/screens/global_widgets/general_details_component/state.dart';
import 'package:tcga_flutter/screens/global_widgets/language/language_selection_page/language_item_component/state.dart';
import 'package:tcga_flutter/screens/global_widgets/language/languages_component/state.dart';
import 'package:tcga_flutter/screens/global_widgets/phone_number/phone_item_component/state.dart';
import 'package:tcga_flutter/screens/global_widgets/phone_number/phone_picker_component/state.dart';

import '../state.dart';

class RecipientEditorState implements Cloneable<RecipientEditorState> {
  String id;

  bool get onEditMode => id != null && id.isNotEmpty;

  String get title => onEditMode ? 'Edit Care Recipient' : 'New Care Recipient';

  List<AddressItemState> addresses;
  List<LanguageItemState> languages;
  List<EmergencyContactItemState> emergencyContacts;

  GeneralDetailsState generalDetails;

  @override
  RecipientEditorState clone() {
    return RecipientEditorState()
      ..id = id
      ..emergencyContacts = emergencyContacts
      ..generalDetails = generalDetails
      ..addresses = addresses
      ..languages = languages;
  }
}

RecipientEditorState initState(Map<String, dynamic> args) {
  final item =
      args.containsKey('item') ? args['item'] as RecipientItemState : null;

  final state = RecipientEditorState()
    ..id = item?.id
    ..generalDetails = (GeneralDetailsState()
      ..firstNameController = TextEditingController(
        text: item?.firstName,
      )
      ..lastNameController = TextEditingController(
        text: item?.lastName,
      )
      ..emailAddressController = TextEditingController(
        text: '',
      )
      ..phoneNumberController = TextEditingController(
        text: '',
      )
      ..dateOfBirth = item?.dateOfBirth)
    ..addresses = []
    ..emergencyContacts = []
    ..languages = [];

  return state;
}

class AddressesStateConnector
    extends ConnOp<RecipientEditorState, AddressesState> {
  @override
  AddressesState get(RecipientEditorState state) =>
      AddressesState()..items = state.addresses;

  @override
  void set(RecipientEditorState state, AddressesState subState) =>
      state.addresses = subState.items
          .map(
            (e) => e.clone(),
          )
          .toList();
}

class LanguagesStateConnector
    extends ConnOp<RecipientEditorState, LanguagesState> {
  @override
  LanguagesState get(RecipientEditorState state) => LanguagesState(
        items: state.languages,
      );

  @override
  void set(RecipientEditorState state, LanguagesState subState) {
    state.languages = subState.items;
  }
}

class ContactsStateConnector
    extends ConnOp<RecipientEditorState, EmergencyContactState> {
  @override
  EmergencyContactState get(RecipientEditorState state) =>
      EmergencyContactState()..items = state.emergencyContacts;

  @override
  void set(RecipientEditorState state, EmergencyContactState subState) {
    state.emergencyContacts = subState.items;
  }
}

class GeneralDetailsStateConnector
    extends ConnOp<RecipientEditorState, GeneralDetailsState> {
  @override
  GeneralDetailsState get(RecipientEditorState state) =>
      state.generalDetails.clone();

  @override
  void set(RecipientEditorState state, GeneralDetailsState subState) {
    state.generalDetails = subState.clone();
  }
}

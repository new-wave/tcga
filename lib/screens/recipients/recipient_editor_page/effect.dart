import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/global_widgets/phone_number/manager.dart';
import 'package:tcga_flutter/utils/phone_format.dart';

import '../../../utils/utils.dart';
import '../manager.dart';
import 'validator.dart';
import 'action.dart';
import 'state.dart';

final _phoneManager = PhoneManager();
final _manager = RecipientManager();

Effect<RecipientEditorState> buildEffect() {
  return combineEffects(<Object, Effect<RecipientEditorState>>{
    Lifecycle.initState: _init,
    RecipientEditorAction.save: _save,
    RecipientEditorAction.delete: _delete,
  });
}

Future _init(Action action, Context<RecipientEditorState> ctx) async {
  final state = ctx.state;

  if (state.onEditMode) {
    final result = await _manager.fill(ctx.context, state);

    ctx.dispatch(
      RecipientEditorActionCreator.initialize(result),
    );
  } else {
    String countryCode = await getCountryCodeFromDevice();
    final phone =
        await _phoneManager.getCountryByCode(ctx.context, code: countryCode);
    RecipientEditorState state = ctx.state.clone();
    state.generalDetails.phoneItemState = phone;
    state.generalDetails.phoneNumberController.text = '';
    ctx.dispatch(
      RecipientEditorActionCreator.initialize(state),
    );
  }
}

Future _save(Action action, Context<RecipientEditorState> ctx) async {
  final state = ctx.state;

  bool isValid = Validator.isValid(state);
  if (!isValid) {
    ctx.dispatch(RecipientEditorActionCreator.checkField());
    return;
  }

  final result =
      state.onEditMode ? await _manager.update(state) : _manager.create(state);

  if (result != null) {
    ctx.goBack(result);
  }
}

Future _delete(Action action, Context<RecipientEditorState> ctx) async {
  final confirmed = await ctx.confirm(
    title: 'Deletion Warning!',
    message: 'Are you sure to delete this item?',
    positiveActionTitle: 'Yes, I am sure.',
    negativeActionTitle: 'Cancel',
  );

  if (!confirmed) return;

  final state = ctx.state;

  ctx.goBack({
    'deleted-item-id': state.id,
  });
}

import 'package:fish_redux/fish_redux.dart';

import 'action.dart';
import 'state.dart';

Reducer<RecipientEditorState> buildReducer() {
  return asReducer(
    <Object, Reducer<RecipientEditorState>>{
      RecipientEditorAction.initialize: _initialize,
      RecipientEditorAction.checkField: _checkFieldReducer,
    },
  );
}

RecipientEditorState _initialize(
  RecipientEditorState state,
  Action action,
) {
  return action.payload.clone();
}

RecipientEditorState _checkFieldReducer(
    RecipientEditorState state, Action action) {
  final RecipientEditorState newState = state.clone();
  newState.generalDetails.isCheckField = true;
  return newState;
}

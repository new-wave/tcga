import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/widget/button_widget.dart';

import 'action.dart';
import 'state.dart';
import 'validator.dart';

Widget buildView(
  RecipientEditorState state,
  Dispatch dispatch,
  ViewService viewService,
) {
  dynamic _checkLenguage() {
    if (state.onEditMode) {
      return AppLocalizations.of(viewService.context)
          .text('recipients.editor.appbar.title_edit');
    }
    return AppLocalizations.of(viewService.context)
        .text('recipients.editor.appbar.title_new');
  }

  final appBar = AppBar(
    elevation: 3,
    centerTitle: false,
    title: Text(_checkLenguage()),
    leading: GestureDetector(
      onTap: () => Navigator.pop(viewService.context),
      child: Container(
        margin: EdgeInsets.only(left: 16),
        child: SvgPicture.asset('assets/icons/ic_arrow_left_circle.svg'),
      ),
    ),
    leadingWidth: 46,
    actions: [
      if (state.onEditMode)
        IconButton(
          iconSize: 24,
          icon: SvgPicture.asset('assets/icons/ic_trash.svg'),
          onPressed: () => dispatch(
            RecipientEditorActionCreator.delete(),
          ),
        )
    ],
  );
  return Builder(builder: (context) {
    return Padding(
      padding: EdgeInsets.only(
        bottom: MediaQuery.of(context).viewInsets.bottom,
      ),
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        //resizeToAvoidBottomPadding: false,
        backgroundColor: Colors.white,
        appBar: appBar,
        body: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: [
              Container(
                child: viewService.buildComponent(
                  'general-details-component',
                ),
              ),
              Container(
                child: viewService.buildComponent(
                  'contacts-component',
                ),
              ),
              Container(
                child: viewService.buildComponent(
                  'addresses-component',
                ),
              ),
              Container(
                child: viewService.buildComponent(
                  'languages-component',
                ),
              ),
            ],
          ),
        ),
        bottomNavigationBar: Builder(
          builder: (context) => Container(
            decoration: BoxDecoration(
                color: colorPrimary, border: Border.all(color: colorPrimary)),
            child: ButtonWidget(
              colorButton: colorPrimary,
              textButton:
                  AppLocalizations.of(viewService.context).text('common.save'),
              onPress: () {
                dispatch(
                  RecipientEditorActionCreator.save(),
                );
              },
            ),
          ),
        ),
      ),
    );
  });
}

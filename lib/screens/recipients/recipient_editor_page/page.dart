import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/component.dart';
import 'package:tcga_flutter/screens/global_widgets/emergency_contact/component.dart';
import 'package:tcga_flutter/screens/global_widgets/general_details_component/component.dart';
import 'package:tcga_flutter/screens/global_widgets/language/languages_component/component.dart';
import 'package:tcga_flutter/screens/global_widgets/phone_number/phone_picker_component/component.dart';

import 'effect.dart';
import 'state.dart';
import 'view.dart';
import 'reducer.dart';

class RecipientEditorPage
    extends Page<RecipientEditorState, Map<String, dynamic>> {
  RecipientEditorPage()
      : super(
          initState: initState,
          effect: buildEffect(),
          reducer: buildReducer(),
          view: buildView,
          dependencies: Dependencies<RecipientEditorState>(
              adapter: null,
              slots: <String, Dependent<RecipientEditorState>>{
                'addresses-component':
                    AddressesStateConnector() + AddressesComponent(),
                'general-details-component':
                    GeneralDetailsStateConnector() + GeneralDetailsComponent(),
                'languages-component':
                    LanguagesStateConnector() + LanguagesComponent(),
                'contacts-component':
                    ContactsStateConnector() + EmergencyContactComponent(),
              }),
          // middleware: <Middleware<RecipientEditState>>[],
        );
}

import 'package:devicelocale/devicelocale.dart';
import 'package:flutter/cupertino.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:tcga_flutter/database/models.dart';
import 'package:tcga_flutter/database/models/enums.dart';
import 'package:tcga_flutter/database/repository.dart';
import 'package:tcga_flutter/screens/auth/manager.dart';
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/address_item_component/state.dart';
import 'package:tcga_flutter/screens/global_widgets/emergency_contact/contact_item_component/state.dart';
import 'package:tcga_flutter/screens/global_widgets/language/language_selection_page/language_item_component/state.dart';
import 'package:tcga_flutter/screens/global_widgets/phone_number/manager.dart';
import 'package:tcga_flutter/screens/global_widgets/phone_number/phone_item_component/state.dart';
import 'package:tcga_flutter/screens/recipients/recipient_editor_page/state.dart';
import 'package:tcga_flutter/screens/recipients/state.dart';
import 'package:tcga_flutter/utils/phone_format.dart';

class RecipientManager {
  static final _singleton = RecipientManager._internal();

  factory RecipientManager() {
    return _singleton;
  }

  RecipientManager._internal();

  final _authManager = AuthManager();
  final _personRepository = Repository<Person>();
  final _teamRepository = Repository<Team>();
  final _phoneManager = PhoneManager();

  Future<List<RecipientItemState>> query({
    String keyword,
  }) async {
    final team = await _getCaregiverTeam();
    final recipientIds = team.members
        .where(
          (x) =>
              x.role == TeamRole.CareRecipient.index &&
              x.personId != _authManager.currentCaregiverId,
        )
        .map(
          (x) => x.personId,
        )
        .toList();

    keyword = (keyword ?? '').trim();
    var entities = await _personRepository.query(
      filter: (person) {
        return recipientIds.contains(person.id) &&
            (person.firstName.contains(keyword) ||
                person.lastName.contains(keyword));
      },
    );

    return entities
        .map(
          (entity) => RecipientItemState(
            id: entity.id,
            firstName: entity.firstName,
            lastName: entity.lastName,
            dateOfBirth: entity.birthDate,
            genderType: entity.gender != null
                ? AdministrativeGenderType.values[entity.gender]
                : null,
          ),
        )
        .toList();
  }

  Future<RecipientEditorState> fill(
      BuildContext context, RecipientEditorState state) async {
    final person = await _personRepository.get(state.id);

    if (person == null) return state;

    final newState = state.clone();
    newState.generalDetails.firstNameController.text = person.firstName;
    newState.generalDetails.lastNameController.text = person.lastName;
    newState.generalDetails.dateOfBirth = person.birthDate;
    newState.generalDetails.emailAddressController.text = person.email;

    if (person.mobilePhone != null && person.mobilePhone.isNotEmpty) {
      List<String> phones =
          person.mobilePhone.split('-').toList(growable: false) ?? [];
      PhoneItemState phone;
      if (person.countryCode != null && person.countryCode.isNotEmpty) {
        phone = await _phoneManager.getCountryByCode(
          context,
          code: person.countryCode,
        );
      } else {
        phone = await _phoneManager.getCountryByDialCode(
          context,
          dialCode: (phones != null && phones.length >0) ? phones[0] : '',
        );
      }
      newState.generalDetails.phoneItemState = phone;
      newState.generalDetails.phoneNumberController.text =
          formatMaskPhoneNumber(
              (phones != null && phones.length > 1) ? phones[1] : '',
              phone?.mask != null ? phone?.mask[0] : '' ?? '');
    } else {
      String countryCode = await getCountryCodeFromDevice();
      final phone =
          await _phoneManager.getCountryByCode(context, code: countryCode);
      newState.generalDetails.phoneItemState = phone;
      newState.generalDetails.phoneNumberController.text = '';
    }

    newState.generalDetails.gender = person.gender != null
        ? AdministrativeGenderType.values[person.gender]
        : null;
    newState.generalDetails.maritalStatus = person.maritalStatus != null
        ? MaritalStatusType.values[person.maritalStatus]
        : null;
    newState.generalDetails.prefix =
        person.prefix != null ? PrefixType.values[person.prefix] : null;
    newState.generalDetails.suffix =
        person.suffix != null ? SuffixType.values[person.suffix] : null;

    newState.addresses = (person.addresses ?? [])
        .map(
          (x) => AddressItemState(
            id: x.id,
            address: x.addressContent,
            city: x.city,
            state: x.stateProvince,
            zip: x.zipPostalCode,
          ),
        )
        .toList();
    newState.languages = (person.languages ?? [])
        .map(
          (x) => LanguageItemState(
            code: x.name,
            display: x.name,
            isPrimary: x.isPrimary,
          ),
        )
        .toList();

    var recipientContactTeam = await _getRecipientTeam(person.id);
    var contactIds = recipientContactTeam.members
        .where((x) =>
            x.personId != person.id &&
            x.role == TeamRole.EmergencyContact.index)
        .map((x) => x.personId)
        .toList();
    var people = await _personRepository.query(
      filter: (x) => contactIds.contains(x.id),
    );

    newState.emergencyContacts = people
        .map(
          (x) => EmergencyContactItemState(
            id: x.id,
            firstName: x.firstName,
            email: x.email,
            lastName: x.lastName,
            phone: x.mobilePhone,
            countryCode: x.countryCode,
          ),
        )
        .toList();

    return newState;
  }

  Future<RecipientItemState> create(RecipientEditorState state) async {
    final team = await _getCaregiverTeam();

    List<Address> addresses = state.addresses
        .where((element) => !element.isDelete)
        .map(
          (x) => Address(
            id: x.id,
            contactId: x.id,
            title: x.title,
            addressContent: x.address,
            city: x.city,
            stateProvince: x.state,
            zipPostalCode: x.zip,
          ),
        )
        .toList();

    final person = Person(
      firstName: state.generalDetails.firstNameController.text.trim(),
      lastName: state.generalDetails.lastNameController.text.trim(),
      birthDate: state.generalDetails.dateOfBirth,
      email: state.generalDetails.emailAddressController.text.trim(),
      mobilePhone: formatPhoneNumber(
        state.generalDetails?.phoneItemState,
        state.generalDetails.phoneNumberController.text.trim(),
      ),
      countryCode: state.generalDetails?.phoneItemState?.id,
      prefix: state.generalDetails.prefix?.index,
      suffix: state.generalDetails.suffix?.index,
      gender: state.generalDetails.gender?.index,
      languages: state.languages
          .map(
            (x) => Language(
              code: x.code,
              name: x.display,
              isPrimary: x.isPrimary,
            ),
          )
          .toList(),
      addresses: addresses,
    );

    person.id = await _personRepository.create(person);

    team.members.add(TeamMember(
      personId: person.id,
      role: TeamRole.CareRecipient.index,
    ));

    await _teamRepository.update(team);

    final recipientContactTeam = await _getRecipientTeam(person.id);
    for (var contact in state.emergencyContacts) {
      if (!contact.isDelete) {
        final contactId = await _personRepository.create(
          Person(
            firstName: contact.firstName,
            lastName: contact.lastName,
            mobilePhone: contact.phone,
            email: contact.email,
            countryCode: contact.countryCode,
          ),
        );

        recipientContactTeam.members.add(
          TeamMember(
            personId: contactId,
            role: TeamRole.EmergencyContact.index,
          ),
        );
      }
    }

    await _teamRepository.update(recipientContactTeam);

    return RecipientItemState(
      id: person.id,
      firstName: person.firstName,
      lastName: person.lastName,
      dateOfBirth: person.birthDate,
      genderType: AdministrativeGenderType.values[person.gender],
    );
  }

  Future<RecipientItemState> update(RecipientEditorState state) async {
    final team = await _getCaregiverTeam();

    final teamMember = team.members.firstWhere(
      (element) => element.personId == state.id,
    );

    if (teamMember == null) return null;

    List<Address> addresses = state.addresses
        .where((element) => !element.isDelete)
        .map(
          (x) => Address(
            id: x.id,
            contactId: x.id,
            title: x.title,
            addressContent: x.address,
            city: x.city,
            stateProvince: x.state,
            zipPostalCode: x.zip,
          ),
        )
        .toList();

    final person = Person(
      id: state.id,
      firstName: state.generalDetails.firstNameController.text.trim(),
      lastName: state.generalDetails.lastNameController.text.trim(),
      birthDate: state.generalDetails.dateOfBirth,
      email: state.generalDetails.emailAddressController.text.trim(),
      mobilePhone: formatPhoneNumber(
        state.generalDetails?.phoneItemState,
        state.generalDetails.phoneNumberController.text.trim(),
      ),
      countryCode: state.generalDetails?.phoneItemState?.id,
      prefix: state.generalDetails.prefix?.index,
      suffix: state.generalDetails.suffix?.index,
      gender: state.generalDetails.gender?.index,
      maritalStatus: state.generalDetails.maritalStatus?.index,
      languages: state.languages
          .map(
            (x) => Language(
              code: x.code,
              name: x.display,
              isPrimary: x.isPrimary,
            ),
          )
          .toList(),
      addresses: addresses,
    );

    await _personRepository.update(person);

    final recipientContactTeam = await _getRecipientTeam(person.id);
    for (var contact in state.emergencyContacts) {
      if (!contact.isDelete) {
        if (recipientContactTeam.members.any(
          (x) => x.personId == contact.id,
        )) {
          await _personRepository.update(
            Person(
              id: contact.id,
              firstName: contact.firstName,
              lastName: contact.lastName,
              mobilePhone: contact.phone,
              email: contact.email,
              countryCode: contact.countryCode,
            ),
          );
          continue;
        }

        final contactId = await _personRepository.create(
          Person(
            firstName: contact.firstName,
            lastName: contact.lastName,
            mobilePhone: contact.phone,
            email: contact.email,
            countryCode: contact.countryCode,
          ),
        );

        recipientContactTeam.members.add(
          TeamMember(
            personId: contactId,
            role: TeamRole.EmergencyContact.index,
          ),
        );
      } else {
        await _personRepository.delete(contact?.id);
      }
    }

    await _teamRepository.update(recipientContactTeam);

    return RecipientItemState(
      id: person.id,
      firstName: person.firstName,
      lastName: person.lastName,
      dateOfBirth: person.birthDate,
      genderType: AdministrativeGenderType.values[person.gender],
    );
  }

  Future delete(String id) async {
    final team = await _getCaregiverTeam();

    final teamMember = team.members.firstWhere(
      (element) => element.personId == id,
      orElse: () => null,
    );

    if (teamMember == null) return false;

    team.members.remove(teamMember);
    await _personRepository.delete(id);
    await _teamRepository.update(team);

    return true;
  }

  Future<Team> _getRecipientTeam(String personId) {
    return _getOrCreateTeam(
      personId,
      TeamOwnerType.Recipient,
    );
  }

  Future<Team> _getCaregiverTeam() {
    return _getOrCreateTeam(
      _authManager.currentCaregiverId,
      TeamOwnerType.Caregiver,
    );
  }

  Future<Team> _getOrCreateTeam(
    String ownerId,
    TeamOwnerType teamOwnerType,
  ) async {
    final exists = await _teamRepository.query(
      filter: (team) {
        return team.ownerId == ownerId && team.ownerType == teamOwnerType.index;
      },
    );

    if (exists.length > 0) {
      return exists[0];
    }

    final team = Team(
        ownerId: ownerId,
        name: 'Contacts',
        ownerType: teamOwnerType.index,
        type: teamOwnerType == TeamOwnerType.Caregiver
            ? TeamType.Contacts.index
            : TeamType.EmergencyContacts.index,
        members: [
          TeamMember(
            isActive: true,
            personId: ownerId,
            role: teamOwnerType == TeamOwnerType.Caregiver
                ? TeamRole.CareGiver.index
                : TeamRole.CareRecipient.index,
          ),
        ]);

    team.id = await _teamRepository.create(team);

    return team;
  }

  Future<RecipientItemState> get(String id) async {
    final person = await _personRepository.get(id);
    if (person != null) {
      return RecipientItemState(
        id: person.id,
        firstName: person.firstName,
        lastName: person.lastName,
        dateOfBirth: person.birthDate,
        genderType: AdministrativeGenderType.values[person.gender],
      );
    } else {
      return null;
    }
  }
}

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'package:fish_redux/fish_redux.dart';

import 'package:tcga_flutter/screens/valuesets/valueset_picker_page/valueset_item_component/state.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';

import '../immunizations_page/immunization_item_component/state.dart';

class ImmunizationEditorState implements Cloneable<ImmunizationEditorState> {
  List<ImmunizationItemState> items;

  String id;
  String recipientId;

  TextEditingController titleEditController;

  ValuesetItemState immunization;

  bool isCheckField;

  DateTime occuredDate;
  final scaffoldKey = GlobalKey<ScaffoldState>();
  get onEditMode => id != null;
  get title => onEditMode
      ? AppLocalizations.instance.text('immunization.editor.appbar.title_edit') : AppLocalizations.instance.text('immunization.editor.appbar.title_new');

  @override
  ImmunizationEditorState clone() {
    return ImmunizationEditorState()
      ..immunization = immunization
      ..id = id
      ..items = items
      ..recipientId = recipientId
      ..occuredDate = occuredDate
      ..isCheckField = isCheckField
      ..titleEditController = titleEditController;
  }
}

ImmunizationEditorState initState(Map<String, dynamic> arg) {
  final item =
      arg.containsKey('item') ? arg['item'] as ImmunizationItemState : null;
  final recipientId =
      arg.containsKey('recipient-id') ? arg['recipient-id'] as String : null;
  final items = arg.containsKey('items')
      ? arg['items'] as List<ImmunizationItemState>
      : null;

  final state = ImmunizationEditorState()
    ..immunization = ValuesetItemState(
      code: item?.code,
      display: item?.title,
    )
    ..id = item?.id
    ..items = items
    ..recipientId = recipientId
    ..isCheckField = false
    ..occuredDate = item?.occuredDate ?? DateTime.now()
    ..titleEditController = TextEditingController(text: item?.title);
  return state;
}

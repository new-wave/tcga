import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart' hide Action, Page;
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/database/models/enums.dart';
import 'package:tcga_flutter/screens/valuesets/valueset_picker_page/valueset_item_component/state.dart';

import '../../../../utils/utils.dart';
import '../../../../utils/app_localizations.dart';
import '../manager.dart';

import 'action.dart';
import 'state.dart';
import 'validator.dart';

final _manager = ImmunizationManager();

Effect<ImmunizationEditorState> buildEffect() {
  return combineEffects(<Object, Effect<ImmunizationEditorState>>{
    ImmunizationEditorAction.save: _save,
    ImmunizationEditorAction.delete: _delete,
    ImmunizationEditorAction.selectImmunization: _selectImmunization,
    ImmunizationEditorAction.checkValidator: _checkValidator,
  });
}

Future _save(
  Action action,
  Context<ImmunizationEditorState> ctx,
) async {
  final state = ctx.state;
  final result = state.onEditMode
      ? await _manager.update(state)
      : await _manager.create(state);

  if (result == null) return;

  ctx.goBack(result);
}

Future _checkValidator(
  Action action,
  Context<ImmunizationEditorState> ctx,
) async {
  final state = ctx.state;

  bool isValid = Validator.isValid(state);
  if (!isValid) {
    ctx.dispatch(ImmunizationEditorActionCreator.checkField());
    return;
  }

  if (!await Validator.checkCondition(state)) {
    final Widget snackBar = SnackBar(
      content: Container(
        child: Text(
          AppLocalizations.of(ctx.context)
              .text('immunization.editor.error.immunization_name_false'),
        ),
      ),
      backgroundColor: colorIndicatorWarning,
    );

    ctx.state.scaffoldKey.currentState.showSnackBar(snackBar);
    return;
  }

  return ctx.dispatch(
    ImmunizationEditorActionCreator.save(),
  );
}

Future _delete(
  Action action,
  Context<ImmunizationEditorState> ctx,
) async {
  final confirmed = await ctx.confirm(
    title: AppLocalizations.instance.text('common.warning.delete.title'),
    message: AppLocalizations.instance.text('common.warning.delete.desc'),
    negativeActionTitle: AppLocalizations.instance.text('common.cancel'),
    positiveActionTitle: AppLocalizations.instance.text('common.sure'),
  );

  if (!confirmed) return;

  final deletedItemId = ctx.state.id;
  final deleted = await _manager.delete(deletedItemId);

  if (!deleted) {
    // TODO show error message
    return;
  }

  ctx.goBack(
    {
      'deleted-item-id': deletedItemId,
    },
  );
}

Future _selectImmunization(
  Action action,
  Context<ImmunizationEditorState> ctx,
) async {
  var selectedItem = await ctx.pickValuesetItem(
    ValueSetType.immunization,
    confirmRequired: true,
  ) as ValuesetItemState;

  if (selectedItem != null) {
    ctx.dispatch(
      ImmunizationEditorActionCreator.handleImmunizationSelected(
        selectedItem,
      ),
    );
  }
}

import 'package:flutter/material.dart';

import 'package:fish_redux/fish_redux.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/widget/import.dart';

import 'action.dart';
import 'state.dart';

Widget buildView(
  ImmunizationEditorState state,
  Dispatch dispatch,
  ViewService viewService,
) {
  return Builder(builder: (context) {
    return Scaffold(
      key: state.scaffoldKey,
      backgroundColor: Colors.white,
      appBar: _appBar(state, viewService, dispatch),
      body: Padding(
        padding: edgeInsetsBigTop,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
                AppLocalizations.of(viewService.context)
                    .text('condition.editor.name'),
                style: textStylePrimary),
            Container(
              margin: EdgeInsets.symmetric(vertical: spacingSm),
              child: PickerWidget(
                tap: () => dispatch(
                  ImmunizationEditorActionCreator.selectImmunization(),
                ),
                hint: AppLocalizations.of(viewService.context).text('care_recipient_dashboard.item.immunizations.title'),
                controller: state.titleEditController,
                suffixIcon: SvgPicture.asset(
                  'assets/icons/ic_search.svg',
                  color: colorPrimary,
                  fit: BoxFit.none,
                ),
                warningText: AppLocalizations.instance.text('common.validator.immunization_name'),
                isRequired: true,
                isCheckField: state.isCheckField ?? false,
              ),
            ),
            Container(
              margin: edgeOnlyTopBotPrimaryMini,
              child: Text(
                AppLocalizations.of(viewService.context)
                    .text('immunization.editor.occurrence_date'),
                style: textStylePrimary,
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 8),
              child: DatePickerWidget(
                initial: state.occuredDate,
                max: DateTime.now(),
                min: DateTime.now().subtract(
                  Duration(days: 30),
                ),
                dateSelected: (date) => dispatch(
                  ImmunizationEditorActionCreator.handleDiagnosedDateSelected(
                    date,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: Builder(
        builder: (context) => Container(
          decoration: BoxDecoration(
              color: colorPrimary, border: Border.all(color: colorPrimary)),
          child: ButtonWidget(
            colorButton: colorPrimary,
            textButton:
            AppLocalizations.of(viewService.context).text('common.save'),
            onPress: () {
              dispatch(ImmunizationEditorActionCreator.checkValidator());
            },
          ),
        ),
      ),
    );
  });
}

AppBar _appBar(
    ImmunizationEditorState state, ViewService viewService, Dispatch dispatch) {
  return AppBar(
    elevation: 3,
    centerTitle: false,
    title: Text((state.onEditMode)
        ? AppLocalizations.of(viewService.context)
            .text('immunization.editor.appbar.title_edit')
        : AppLocalizations.of(viewService.context)
            .text('immunization.editor.appbar.title_new')),
    leading: GestureDetector(
      onTap: () => Navigator.pop(viewService.context),
      child: Container(
        margin: EdgeInsets.only(left: 16),
        child: SvgPicture.asset('assets/icons/ic_arrow_left_circle.svg'),
      ),
    ),
    leadingWidth: 46,
    actions: [
      if (state.onEditMode)
        IconButton(
          iconSize: 24,
          icon: SvgPicture.asset('assets/icons/ic_trash.svg'),
          onPressed: () => dispatch(
            ImmunizationEditorActionCreator.delete(),
          ),
        )
    ],
  );
}

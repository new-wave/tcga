import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/valuesets/valueset_picker_page/valueset_item_component/state.dart';

enum ImmunizationEditorAction {
  save,
  delete,
  selectImmunization,
  handleImmunizationSelected,
  handleImmunizationTypeSelected,
  handleOccuredDateSelected,
  toggleIsPrimary,
  handleChronicityTypeSelected,
  handleImmunizationStatusSelected,
  checkValidator,
  checkField,
}

class ImmunizationEditorActionCreator {
  static Action save() {
    return const Action(
      ImmunizationEditorAction.save,
    );
  }

  static Action checkField() {
    return const Action(
      ImmunizationEditorAction.checkField,
    );
  }

  static Action checkValidator() {
    return const Action(
      ImmunizationEditorAction.checkValidator,
    );
  }

  static Action delete() {
    return const Action(
      ImmunizationEditorAction.delete,
    );
  }

  static Action selectImmunization() {
    return const Action(
      ImmunizationEditorAction.selectImmunization,
    );
  }

  static Action handleImmunizationSelected(ValuesetItemState item) {
    return Action(
      ImmunizationEditorAction.handleImmunizationSelected,
      payload: item,
    );
  }

  static Action handleImmunizationTypeSelected(int index) {
    return Action(
      ImmunizationEditorAction.handleImmunizationTypeSelected,
      payload: index,
    );
  }

  static Action handleDiagnosedDateSelected(DateTime date) {
    return Action(
      ImmunizationEditorAction.handleOccuredDateSelected,
      payload: date,
    );
  }

  static Action toggleIsPrimary() {
    return Action(
      ImmunizationEditorAction.toggleIsPrimary,
    );
  }

  static Action handleChronicityTypeSelected(int index) {
    return Action(
      ImmunizationEditorAction.handleChronicityTypeSelected,
      payload: index,
    );
  }

  static Action handleImmunizationStatusSelected(int index) {
    return Action(
      ImmunizationEditorAction.handleImmunizationStatusSelected,
      payload: index,
    );
  }
}

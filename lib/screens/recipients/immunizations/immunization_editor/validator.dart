import 'package:tcga_flutter/utils/app_localizations.dart';

import '../manager.dart';
import 'state.dart';

final _manager = ImmunizationManager();

class Validator {
  static bool isValid(ImmunizationEditorState state) {
    if (state.titleEditController.text == '') return false;
    if (state.occuredDate == null) return false;

    return true;
  }

  static Future<bool> checkCondition(ImmunizationEditorState state) async {
    var items = (await _manager.query(recipientId: state.recipientId))
        .where((element) => element.code == state.immunization.code && element.id != state.id);

    if (items.length > 0) return false;

    return true;
  }
}

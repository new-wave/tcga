import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/valuesets/valueset_picker_page/valueset_item_component/state.dart';

import 'action.dart';
import 'state.dart';

Reducer<ImmunizationEditorState> buildReducer() {
  return asReducer(
    <Object, Reducer<ImmunizationEditorState>>{
      ImmunizationEditorAction.handleImmunizationSelected:
          _handleImmunizationSelected,
      ImmunizationEditorAction.handleOccuredDateSelected:
          _handleDiagnosedDateSelected,
      ImmunizationEditorAction.checkField: _checkFieldReducer,
    },
  );
}

ImmunizationEditorState _checkFieldReducer(ImmunizationEditorState state, Action action) {
  final ImmunizationEditorState newState = state.clone();
  newState.isCheckField = true;
  return newState;
}

ImmunizationEditorState _handleImmunizationSelected(
  ImmunizationEditorState state,
  Action action,
) {
  final item = action.payload as ValuesetItemState;
  final newState = state.clone()..immunization = item;
  newState.titleEditController.text = item.display;
  return newState;
}

ImmunizationEditorState _handleDiagnosedDateSelected(
  ImmunizationEditorState state,
  Action action,
) {
  final date = action.payload as DateTime;
  final newState = state.clone()..occuredDate = date;
  return newState;
}

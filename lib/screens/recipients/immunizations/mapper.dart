import 'package:tcga_flutter/database/models.dart';

import 'immunization_editor/state.dart';
import 'immunizations_page/immunization_item_component/state.dart';

extension EntityToStateMapper on Immunization {
  ImmunizationItemState toState() => ImmunizationItemState(
        id: this.id,
        code: this.vaccineCode,
        title: this.name,
        occuredDate: this.recordedDate,
      );
}

extension StateToEntityMapper on ImmunizationEditorState {
  Immunization toEntity() => Immunization(
        id: this.id,
        vaccineCode: this.immunization.code,
        name: this.immunization.display,
        recordedDate: this.occuredDate,
        personId: this.recipientId,
      );
}

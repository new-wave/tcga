import 'package:fish_redux/fish_redux.dart';

import 'immunization_item_component/state.dart';

enum ImmunizationsAction {
  initialize,
  create,
  handleImmunizationCreated,
  edit,
  handleImmunizationEdited,
  delete,
  handleImmunizationDeleted,
  addNote,
}

class ImmunizationsActionCreator {
  static Action initialize(List<ImmunizationItemState> items) {
    return Action(
      ImmunizationsAction.initialize,
      payload: items,
    );
  }

  static Action edit(ImmunizationItemState item) {
    return Action(
      ImmunizationsAction.edit,
      payload: item,
    );
  }

  static Action handleImmunizationEdited(ImmunizationItemState item) {
    return Action(
      ImmunizationsAction.handleImmunizationEdited,
      payload: item,
    );
  }

  static Action addNote(ImmunizationItemState item) {
    return Action(
      ImmunizationsAction.addNote,
      payload: item,
    );
  }

  static Action delete(ImmunizationItemState item) {
    return Action(
      ImmunizationsAction.delete,
      payload: item,
    );
  }

  static Action handleImmunizationDeleted(String id) {
    return Action(
      ImmunizationsAction.handleImmunizationDeleted,
      payload: id,
    );
  }

  static Action create(String recipientId) {
    return Action(
      ImmunizationsAction.create,
      payload: recipientId,
    );
  }

  static Action handleImmunizationCreated(ImmunizationItemState item) {
    return Action(
      ImmunizationsAction.handleImmunizationCreated,
      payload: item,
    );
  }
}

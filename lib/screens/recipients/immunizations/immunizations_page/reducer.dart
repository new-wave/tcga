import 'package:fish_redux/fish_redux.dart';

import 'action.dart';
import 'immunization_item_component/state.dart';
import 'state.dart';

Reducer<ImmunizationsState> buildReducer() {
  return asReducer(
    <Object, Reducer<ImmunizationsState>>{
      ImmunizationsAction.initialize: _initialize,
      ImmunizationsAction.handleImmunizationCreated: _handleImmunizationAdded,
      ImmunizationsAction.handleImmunizationEdited: _handleImmunizationEdited,
      ImmunizationsAction.handleImmunizationDeleted: _handleImmunizationDeleted,
    },
  );
}

ImmunizationsState _initialize(ImmunizationsState state, Action action) {
  final ImmunizationsState newState = state.clone();

  newState.items = action.payload as List<ImmunizationItemState> ?? [];

  return newState;
}

ImmunizationsState _handleImmunizationAdded(
    ImmunizationsState state, Action action) {
  final item = action.payload as ImmunizationItemState;
  final newState = state.clone();

  newState.items.add(item);

  return newState;
}

ImmunizationsState _handleImmunizationEdited(
    ImmunizationsState state, Action action) {
  final item = action.payload as ImmunizationItemState;
  final newState = state.clone();

  final editedIndex = newState.items.indexWhere(
    (x) => x.id == item.id,
  );

  newState.items[editedIndex] = item;

  return newState;
}

ImmunizationsState _handleImmunizationDeleted(
    ImmunizationsState state, Action action) {
  final id = action.payload;
  final newState = state.clone();

  final deletedIndex = newState.items.indexWhere(
    (x) => x.id == id,
  );

  newState.items.removeAt(deletedIndex);

  return newState;
}

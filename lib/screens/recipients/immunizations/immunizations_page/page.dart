import 'package:fish_redux/fish_redux.dart';

import '../../recipient_dashboard/recipient_header_component/component.dart';

import 'immunizations_adapter/adapter.dart';
import 'effect.dart';
import 'state.dart';
import 'view.dart';
import 'reducer.dart';

class ImmunizationsPage extends Page<ImmunizationsState, Map<String, dynamic>> {
  ImmunizationsPage()
      : super(
          initState: initState,
          effect: buildEffect(),
          reducer: buildReducer(),
          view: buildView,
          dependencies: Dependencies<ImmunizationsState>(
            adapter: NoneConn<ImmunizationsState>() + ImmunizationsAdapter(),
            slots: <String, Dependent<ImmunizationsState>>{
              'recipient-header':
                  RecipientHeaderStateConnector() + RecipientHeaderComponent(),
            },
          ),
        );
}

import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/database/models/enums.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';

import '../../../../route_name.dart';
import '../../../../utils/utils.dart';

import '../manager.dart';
import 'action.dart';
import 'immunization_item_component/state.dart';
import 'state.dart';

final _manager = ImmunizationManager();

Effect<ImmunizationsState> buildEffect() {
  return combineEffects(<Object, Effect<ImmunizationsState>>{
    Lifecycle.initState: _init,
    ImmunizationsAction.create: _create,
    ImmunizationsAction.edit: _edit,
    ImmunizationsAction.delete: _delete,
    ImmunizationsAction.addNote: _addNote,
  });
}

Future _init(
  Action action,
  Context<ImmunizationsState> ctx,
) async {
  final items = await _manager.query(
    recipientId: ctx.state.recipient.id,
  );

  ctx.dispatch(
    ImmunizationsActionCreator.initialize(
      items,
    ),
  );
}

Future _create(Action action, Context<ImmunizationsState> ctx) async {
  final result = await ctx.navigateTo(
    RouteName.recipientsImmunizationEdit,
    arguments: {
      'recipient-id': ctx.state.recipient.id,
      'items': ctx.state.items,
    },
  );

  if (result == null) return;

  ctx.dispatch(
    ImmunizationsActionCreator.handleImmunizationCreated(
      result,
    ),
  );
}

Future _edit(Action action, Context<ImmunizationsState> ctx) async {
  final result = await ctx.navigateTo(
    RouteName.recipientsImmunizationEdit,
    arguments: {
      'item': action.payload,
      'recipient-id': ctx.state.recipient.id,
      'items': ctx.state.items,
    },
  );

  if (result == null) return;

  if (result is ImmunizationItemState) {
    ctx.dispatch(
      ImmunizationsActionCreator.handleImmunizationEdited(
        result,
      ),
    );
  } else {
    ctx.dispatch(
      ImmunizationsActionCreator.handleImmunizationDeleted(
        action.payload.id,
      ),
    );
  }
}

Future _delete(Action action, Context<ImmunizationsState> ctx) async {
  final confirmed = await ctx.confirm(
    title: AppLocalizations.instance.text('common.warning.delete.title'),
    message: AppLocalizations.instance.text('common.warning.delete.desc'),
    negativeActionTitle: AppLocalizations.instance.text('common.cancel'),
    positiveActionTitle: AppLocalizations.instance.text('common.sure'),
  );

  if (!confirmed) return;

  final deletedItemId = action.payload.id;
  final deleted = await _manager.delete(deletedItemId);

  if (!deleted) {
    // TODO show error message
    return;
  }

  ctx.dispatch(
    ImmunizationsActionCreator.handleImmunizationDeleted(
      deletedItemId,
    ),
  );
}

Future _addNote(Action action, Context<ImmunizationsState> ctx) {
  return ctx.navigateTo(
    RouteName.notesEditor,
    arguments: {
      'item-id': action.payload.id,
      'type': NoteType.Immunization,
      'recipient-id': ctx.state.recipient.id,
    },
  );
}

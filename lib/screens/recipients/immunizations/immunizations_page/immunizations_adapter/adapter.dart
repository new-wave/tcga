import 'package:fish_redux/fish_redux.dart';

import '../immunization_item_component/component.dart';
import '../state.dart';

class ImmunizationsAdapter extends SourceFlowAdapter<ImmunizationsState> {
  ImmunizationsAdapter()
      : super(
          pool: <String, Component<Object>>{
            'immunization-item': ImmunizationItemComponent()
          },
        );
}

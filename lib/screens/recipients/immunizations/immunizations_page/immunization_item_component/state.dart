import 'package:fish_redux/fish_redux.dart';

class ImmunizationItemState implements Cloneable<ImmunizationItemState> {
  String id;
  String title;
  String code;
  DateTime occuredDate;

  ImmunizationItemState({
    this.id,
    this.title,
    this.code,
    this.occuredDate,
  });

  @override
  ImmunizationItemState clone() {
    return ImmunizationItemState()
      ..id = id
      ..title = title
      ..code = code
      ..occuredDate = occuredDate;
  }
}

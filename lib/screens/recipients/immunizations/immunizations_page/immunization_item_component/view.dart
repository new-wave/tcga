import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';

import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/widget/list_item_widget.dart';

import '../action.dart';
import 'state.dart';

Widget buildView(
  ImmunizationItemState state,
  Dispatch dispatch,
  ViewService viewService,
) {
  return SizedBox(
    height: 45,
    child: Column(
      children: [
        ListItemWidget(
          contextActions: [
            'recipients.popup.add_note',
            'recipients.popup.edit',
            'recipients.popup.delete',
          ],
          onTap: () => dispatch(
            ImmunizationsActionCreator.edit(
              state,
            ),
          ),
          onContextActionSelected: (String action) {
            switch (action) {
              case 'recipients.popup.add_note':
                {
                  dispatch(
                    ImmunizationsActionCreator.addNote(
                      state,
                    ),
                  );
                }
                break;
              case 'recipients.popup.edit':
                {
                  dispatch(
                    ImmunizationsActionCreator.edit(
                      state,
                    ),
                  );
                }
                break;
              case 'recipients.popup.delete':
                {
                  dispatch(
                    ImmunizationsActionCreator.delete(
                      state,
                    ),
                  );
                }
                break;
            }
          },
          child: Container(
            margin: EdgeInsets.only(left: spacingPrimary),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  child: Text(
                    state.title,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      color: colorTextPrimary,
                      fontSize: 14,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ),
                SizedBox(
                  height: 8,
                ),
                Row(
                  children: [
                    Text(
                      formatDateTime(state.occuredDate.toString()),
                      style: TextStyle(
                        color: colorTextSecondary,
                        fontSize: 12,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ],
    ),
  );
}

String formatDateTime(var value) {
  value = value.toString().split(" ");
  value = value[0].toString().split("-");
  return "Occurrence Date: ${value[2]}/${value[1]}/${value[0]}";
}

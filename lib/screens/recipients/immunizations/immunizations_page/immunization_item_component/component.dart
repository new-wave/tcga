import 'package:fish_redux/fish_redux.dart';

import 'state.dart';
import 'view.dart';

class ImmunizationItemComponent extends Component<ImmunizationItemState> {
  ImmunizationItemComponent()
      : super(
          view: buildView,
        );
}

import 'package:fish_redux/fish_redux.dart';

import '../../recipient_dashboard/recipient_header_component/state.dart';
import '../../state.dart';

import 'immunization_item_component/state.dart';

class ImmunizationsState extends MutableSource
    implements Cloneable<ImmunizationsState> {
  List<ImmunizationItemState> items;
  RecipientItemState recipient;
  ImmunizationItemState primaryImmunization;

  @override
  ImmunizationsState clone() {
    return ImmunizationsState()
      ..items = items
      ..recipient = recipient
      ..primaryImmunization = primaryImmunization;
  }

  @override
  Object getItemData(int index) => items[index];

  @override
  String getItemType(int index) => 'immunization-item';

  @override
  int get itemCount => items?.length ?? 0;

  @override
  void setItemData(int index, Object data) => items[index] = data;
}

ImmunizationsState initState(Map<String, dynamic> arg) {
  return ImmunizationsState()..recipient = arg['recipient'];
}

class RecipientHeaderStateConnector
    extends ConnOp<ImmunizationsState, RecipientHeaderState> {
  @override
  RecipientHeaderState get(ImmunizationsState state) => RecipientHeaderState(
        recipient: state.recipient,
      );

  @override
  void set(ImmunizationsState state, RecipientHeaderState subState) {
    state.recipient = subState.recipient;
  }
}

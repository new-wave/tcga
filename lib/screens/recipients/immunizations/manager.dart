import 'package:tcga_flutter/screens/recipients/immunizations/immunization_editor/state.dart';

import '../../../database/models.dart';
import '../../../database/repository.dart';

import 'immunizations_page/immunization_item_component/state.dart';
import 'mapper.dart';

class ImmunizationManager {
  static final _singleton = ImmunizationManager._internal();

  factory ImmunizationManager() {
    return _singleton;
  }

  ImmunizationManager._internal();

  final _immunizationRepository = Repository<Immunization>();

  Future<List<ImmunizationItemState>> query({
    int pageSize = 100,
    String recipientId,
  }) async {
    final items = await _immunizationRepository.query(
        filter: (x) => x.personId == recipientId);

    return items.map((x) => x.toState()).toList();
  }

  Future<bool> delete(String id) {
    return _immunizationRepository.delete(id);
  }

  Future<ImmunizationItemState> update(ImmunizationEditorState state) async {
    final entity = state.toEntity();

    final _ = await _immunizationRepository.update(entity);

    return entity.toState();
  }

  Future<ImmunizationItemState> create(ImmunizationEditorState state) async {
    final entity = state.toEntity();

    entity.id = await _immunizationRepository.create(entity);

    return entity.toState();
  }
}

import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart' hide Action, Page;
import 'package:flutter_svg/svg.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'state.dart';

Widget buildView(
  TaskState state,
  Dispatch dispatch,
  ViewService viewService,
) {
  return Column(
    children: [
      Container(
        padding: EdgeInsets.fromLTRB(3, 3, 21, 4),
        height: 60,
        margin: EdgeInsets.only(
          left: 14,
          right: 18,
        ),
        decoration: BoxDecoration(
          color: state.typeTask == "Appointment"
              ? Color(0xff27AE60)
              : Color(0xffF2994A),
          borderRadius: BorderRadius.circular(5),
        ),
        child: Row(
          children: <Widget>[
            Container(
              width: 69,
              padding: EdgeInsets.only(left: 10),
              height: 53,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(2),
                color: Colors.white,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    state.time,
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.w700,
                      color: colorTextPrimary,
                    ),
                  ),
                  Text(
                    state.cvtime,
                    style: TextStyle(
                      fontSize: 10,
                      fontWeight: FontWeight.w400,
                    ),
                  )
                ],
              ),
            ),
            SizedBox(
              width: 16,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    state.typeTask,
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w400,
                      color: Colors.white,
                    ),
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                  ),
                  SizedBox(
                    height: 12,
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      SvgPicture.asset("assets/icons/ic_user.svg"),
                      SizedBox(
                        width: 5,
                      ),
                      Text(
                        state.name,
                        style: TextStyle(
                          fontSize: 12,
                          color: Colors.white,
                          fontWeight: FontWeight.w400,
                        ),
                        overflow: TextOverflow.ellipsis,
                        maxLines: 1,
                      )
                    ],
                  ),
                ],
              ),
            ),
            SizedBox(
              width: 16,
            ),
            SvgPicture.asset(
              state.typeIcon,
              width: 20,
              height: 19,
            ),
          ],
        ),
      ),
      SizedBox(
        height: 8,
      ),
    ],
  );
}

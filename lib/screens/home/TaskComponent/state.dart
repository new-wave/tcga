import 'package:fish_redux/fish_redux.dart';

class TaskState implements Cloneable<TaskState> {
  String time;
  String cvtime;
  String typeTask;
  String name;
  String typeIcon;

  TaskState({
    this.cvtime,
    this.name,
    this.time,
    this.typeIcon,
    this.typeTask,
  });

  @override
  TaskState clone() {
    return TaskState()
      ..cvtime = cvtime
      ..name = name
      ..time = time
      ..typeIcon = typeIcon
      ..typeTask = typeTask;
  }
}

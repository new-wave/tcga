import 'package:fish_redux/fish_redux.dart';
import '../HomePage/state.dart';
import 'component.dart';

class TaskListAdapter extends SourceFlowAdapter<HomePageState> {
  TaskListAdapter()
      : super(
          pool: <String, Component<Object>>{'task_list': TaskComponent()},
        );
}

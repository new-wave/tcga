import 'package:fish_redux/fish_redux.dart';

enum TaskAction {
  action,
}

class TaskActionCreator {
  static Action action() {
    return const Action(TaskAction.action);
  }
}

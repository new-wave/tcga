import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'state.dart';

Widget buildView(
  ReportState state,
  Dispatch dispatch,
  ViewService viewService,
) {
  return Container(
    margin: EdgeInsets.symmetric(
      horizontal: 16,
      vertical: 26,
    ),
    height: 70,
    padding: EdgeInsets.symmetric(
      horizontal: 16,
    ),
    decoration: BoxDecoration(
      color: Colors.white,
      boxShadow: [
        BoxShadow(
          color: Color(0xff000000).withOpacity(0.15),
          spreadRadius: 0,
          blurRadius: 10,
          offset: Offset(0, 1),
        ),
      ],
      borderRadius: BorderRadius.circular(5),
    ),
    child: Row(
      children: <Widget>[
        Text(
          "Today, Feb 17, 2021",
          style: TextStyle(
              fontSize: 14,
              color: Color(0xff0B507D),
              fontWeight: FontWeight.w400),
        ),
        Spacer(),
        CircleAvatar(
          radius: 12,
          backgroundColor: Color(0xffF2994A),
          child: SvgPicture.asset("assets/icons/ic_drugs.svg"),
        ),
        SizedBox(
          width: 3,
        ),
        Text(
          '${state.totalDrug}',
          style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.w400,
              color: Color(
                0xff323232,
              )),
        ),
        SizedBox(
          width: 18,
        ),
        CircleAvatar(
          radius: 12,
          backgroundColor: Color(0xff27AE60),
          child: SvgPicture.asset("assets/icons/ic_schedule.svg"),
        ),
        SizedBox(
          width: 3,
        ),
        Text(
          '${state.totalAppointment}',
          style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.w400,
              color: Color(
                0xff323232,
              )),
        ),
      ],
    ),
  );
}

import 'package:fish_redux/fish_redux.dart';

class ReportState implements Cloneable<ReportState> {
  int totalDrug;
  int totalAppointment;

  ReportState({
    this.totalAppointment = 0,
    this.totalDrug,
  });

  @override
  ReportState clone() {
    return ReportState()
      ..totalDrug = totalDrug
      ..totalAppointment = totalAppointment;
  }
}

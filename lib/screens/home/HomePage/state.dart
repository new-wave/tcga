import 'package:fish_redux/fish_redux.dart';
import '../ReportComponent/state.dart';
import '../TaskComponent/state.dart';

class HomePageState extends MutableSource implements Cloneable<HomePageState> {
  List<TaskState> listTasks;

  HomePageState clone() {
    return HomePageState()..listTasks = listTasks;
  }

  @override
  Object getItemData(int index) => listTasks[index];

  @override
  String getItemType(int index) => 'task_list';

  @override
  int get itemCount => listTasks.length ?? 0;

  @override
  void setItemData(int index, Object data) => listTasks[index] = data;
}

HomePageState initState(Map<String, dynamic> args) {
  return HomePageState();
}

class ReportConnector extends ConnOp<HomePageState, ReportState>
    with ReselectMixin<HomePageState, ReportState> {
  @override
  ReportState computed(HomePageState state) {
    return ReportState()
      ..totalDrug = state.listTasks
          .where((TaskState taskState) =>
              taskState.typeTask == "Medication Schedule")
          .length
      ..totalAppointment = state.listTasks.length;
  }

  @override
  List<dynamic> factors(HomePageState state) {
    return <int>[
      state.listTasks
          .where((TaskState taskState) =>
              taskState.typeTask == "Medication Schedule")
          .length,
      state.listTasks.length
    ];
  }

  @override
  void set(HomePageState state, ReportState subState) {
    throw Exception('Unexcepted to set PageState from ReportState');
  }
}

import 'package:fish_redux/fish_redux.dart';
import 'action.dart';
import 'state.dart';
import '../TaskComponent/state.dart';

Reducer<HomePageState> buildReducer() {
  return asReducer(<Object, Reducer<HomePageState>>{
    HomeAction.action: _onAction,
    HomeAction.initTask: _initTaskReducer,
  });
}

HomePageState _onAction(HomePageState state, Action action) {
  final HomePageState newState = state.clone();
  return newState;
}

//Ham nay de lay du lieu vao initState
HomePageState _initTaskReducer(HomePageState state, Action action) {
  final List<TaskState> tasks = action.payload ?? <TaskState>[];
  final HomePageState newState = state.clone();
  newState.listTasks = tasks;

  return newState;
}

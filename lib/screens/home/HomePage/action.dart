import 'package:fish_redux/fish_redux.dart';
import '../TaskComponent/state.dart';

enum HomeAction {
  action,
  initTask,
}

class HomeActionCreator {
  static Action action() {
    return const Action(HomeAction.action);
  }

  static Action initTasksAction(List<TaskState> tasks) {
    return Action(HomeAction.initTask, payload: tasks);
  }
}

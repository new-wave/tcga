import 'package:fish_redux/fish_redux.dart';
import '../ReportComponent/component.dart';
import '../TaskComponent/adapter.dart';
import './reducer.dart';
import './state.dart';
import './effect.dart';
import './view.dart';

class HomePage extends Page<HomePageState, Map<String, dynamic>> {
  HomePage()
      : super(
          initState: initState,
          effect: buildEffect(),
          reducer: buildReducer(),
          view: buildView,
          dependencies: Dependencies<HomePageState>(
            adapter: NoneConn<HomePageState>() + TaskListAdapter(),
            slots: <String, Dependent<HomePageState>>{
              'report': ReportConnector() + ReportComponent()
            },
          ),
        );
}

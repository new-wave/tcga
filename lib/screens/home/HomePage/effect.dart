import 'package:fish_redux/fish_redux.dart';
import 'action.dart';
import '../TaskComponent/state.dart';

import './state.dart';

Effect<HomePageState> buildEffect() {
  return combineEffects(<Object, Effect<HomePageState>>{
    Lifecycle.initState: init,
    HomeAction.action: _onAction,
  });
}

void init(Action action, Context<HomePageState> ctx) {
  final List<TaskState> initTasks = [
    TaskState(
      time: "09:30",
      cvtime: "AM",
      typeTask: "Medication Schedule",
      name: "David Covver",
      typeIcon: "assets/icons/ic_drugs.svg",
    ),
    TaskState(
      time: "10:30",
      cvtime: "AM",
      typeTask: "Medication Schedule",
      name: "David Covver",
      typeIcon: "assets/icons/ic_drugs.svg",
    ),
    TaskState(
      time: "11:00",
      cvtime: "AM",
      typeTask: "Appointment",
      name: "David Covver",
      typeIcon: "assets/icons/ic_schedule.svg",
    ),
    TaskState(
      time: "12:00",
      cvtime: "AM",
      typeTask: "Appointment",
      name: "David Covver",
      typeIcon: "assets/icons/ic_schedule.svg",
    ),
    TaskState(
      time: "02:00",
      cvtime: "PM",
      typeTask: "Appointment",
      name: "David Covver",
      typeIcon: "assets/icons/ic_schedule.svg",
    ),
    TaskState(
      time: "07:30",
      cvtime: "PM",
      typeTask: "Medication Schedule",
      name: "David Covver",
      typeIcon: "assets/icons/ic_drugs.svg",
    ),
  ];
  ctx.dispatch(HomeActionCreator.initTasksAction(initTasks));
}

void _onAction(Action action, Context<HomePageState> ctx) {}

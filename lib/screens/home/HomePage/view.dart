import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:tcga_flutter/screens/Drawer/drawer_page.dart';
import 'state.dart';

Widget buildView(
    HomePageState state, Dispatch dispatch, ViewService viewService) {
  final ListAdapter adapter = viewService.buildAdapter();
  return Scaffold(
    backgroundColor: Colors.white,
    appBar: AppBar(
        elevation: 0,
        iconTheme: IconThemeData(color: Colors.black),
        backgroundColor: Color(0xFFFFFFFF),
        title: Row(
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  "Good morning",
                  textAlign: TextAlign.start,
                  style: TextStyle(
                    fontSize: 18,
                    color: Color(0xff4f4f4f),
                    fontWeight: FontWeight.w700,
                  ),
                ),
                Text(
                  "Maria",
                  style: TextStyle(
                    fontSize: 18,
                    color: Color(0xff4f4f4f),
                    fontWeight: FontWeight.w400,
                  ),
                )
              ],
            ),
          ],
        )),
    drawer: DrawerPage(),
    body: SingleChildScrollView(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          viewService.buildComponent('report'),
          ListView.builder(
            shrinkWrap: true,
            itemCount: adapter.itemCount,
            itemBuilder: adapter.itemBuilder,
          ),
        ],
      ),
    ),
  );
}

import 'package:flutter/widgets.dart';

import 'package:fish_redux/fish_redux.dart';
import 'package:intl/intl.dart';

import 'package:tcga_flutter/database/models/enums.dart';
import 'package:tcga_flutter/screens/auth/manager.dart';
import 'package:tcga_flutter/screens/recipients/recipient_picker_component/state.dart';
import 'package:tcga_flutter/screens/recipients/state.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';

import '../notes_page/note_item_component/state.dart';

final _authManager = AuthManager();

class NoteEditorState implements Cloneable<NoteEditorState> {
  static final priorities = {
    NotePriority.High: 'High',
    NotePriority.Medium: 'Medium',
    NotePriority.Low: 'Low',
  };

  String id;
  String recipientId;

  TextEditingController subjectEditController;
  TextEditingController contentEditController;

  NoteType type;
  NotePriority priority;

  DateTime diagnosedDate;
  bool isConfidential;
  bool noteClass;
  bool viewType;
  var reference;
  String createdBy;

  RecipientItemState recipient;
  bool isCheckField;

  get onEditMode => id != null;

  get title => onEditMode
      ? AppLocalizations.instance.text('notes.editor.appbar.title_edit')
      : AppLocalizations.instance.text('notes.editor.appbar.title_new');

  @override
  NoteEditorState clone() {
    return NoteEditorState()
      ..id = id
      ..recipientId = recipientId
      ..priority = priority
      ..diagnosedDate = diagnosedDate
      ..isConfidential = isConfidential
      ..noteClass = noteClass
      ..type = type
      ..recipient = recipient
      ..isCheckField = isCheckField
      ..subjectEditController = subjectEditController
      ..contentEditController = contentEditController
      ..viewType = viewType
      ..createdBy = createdBy;
  }
}

NoteEditorState initState(Map<String, dynamic> arg) {
  final item = arg.containsKey('item') ? arg['item'] as NoteItemState : null;
  final noteType = arg.containsKey('type') ? arg['type'] as NoteType : null;
  final recipientId =
      arg.containsKey('recipient-id') ? arg['recipient-id'] as String : null;

  var viewType = arg.containsKey('viewType') ? arg['viewType'] as bool : null;
  if (viewType == null) viewType = false;

  final state = NoteEditorState()
    ..id = item?.id
    ..recipientId = item?.recipientId ?? recipientId
    ..priority = item?.priority ?? NotePriority.High
    ..noteClass = item?.noteClass ?? false
    ..type = item?.type ?? noteType
    ..isConfidential = item?.isConfidential ?? false
    ..isCheckField = false
    ..subjectEditController = TextEditingController(text: item?.subject)
    ..contentEditController = TextEditingController(text: item?.content)
    ..viewType = item?.viewType ?? !viewType
    ..createdBy = _authManager.currentCaregiverId;
  return state;
}

class RecipientPickerStateConnector
    extends ConnOp<NoteEditorState, RecipientPickerState> {
  @override
  RecipientPickerState get(NoteEditorState state) => RecipientPickerState(
        item: state.recipient,
        isCheckField: state.isCheckField,
      );

  @override
  void set(NoteEditorState state, RecipientPickerState subState) {
    state.recipient = subState.item;
    state.isCheckField = subState.isCheckField;
  }
}

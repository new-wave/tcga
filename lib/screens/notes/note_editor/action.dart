import 'package:fish_redux/fish_redux.dart';

enum NoteEditorAction {
  save,
  checkField,
  handlePrioritySelected,
  toggleIsConfidential,
  toggleNoteClass,
}

class NoteEditorActionCreator {
  static Action save() {
    return const Action(
      NoteEditorAction.save,
    );
  }

  static Action checkField() {
    return const Action(
      NoteEditorAction.checkField,
    );
  }

  static Action handlePrioritySelected(int index) {
    return Action(
      NoteEditorAction.handlePrioritySelected,
      payload: index,
    );
  }

  static Action toggleNoteClass() {
    return const Action(
      NoteEditorAction.toggleNoteClass,
    );
  }

  static Action toggleIsConfidential() {
    return Action(
      NoteEditorAction.toggleIsConfidential,
    );
  }
}

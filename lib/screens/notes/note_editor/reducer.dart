import 'package:fish_redux/fish_redux.dart';

import 'action.dart';
import 'state.dart';

Reducer<NoteEditorState> buildReducer() {
  return asReducer(
    <Object, Reducer<NoteEditorState>>{
      NoteEditorAction.toggleIsConfidential: _toggleIsConfidential,
      NoteEditorAction.toggleNoteClass: _toggleNoteClass,
      NoteEditorAction.handlePrioritySelected: _handlePrioritySelected,
      NoteEditorAction.checkField: _checkFieldReducer,
    },
  );
}

NoteEditorState _checkFieldReducer(NoteEditorState state, Action action) {
  final NoteEditorState newState = state.clone();
  newState.isCheckField = true;
  return newState;
}

NoteEditorState _handlePrioritySelected(
  NoteEditorState state,
  Action action,
) {
  final newState = state.clone();
  newState.priority = NoteEditorState.priorities.keys.elementAt(
    action.payload,
  );
  return newState;
}

NoteEditorState _toggleIsConfidential(
  NoteEditorState state,
  Action action,
) {
  final newState = state.clone()..isConfidential = !state.isConfidential;
  return newState;
}

NoteEditorState _toggleNoteClass(
  NoteEditorState state,
  Action action,
) {
  final newState = state.clone()..noteClass = !state.noteClass;
  return newState;
}

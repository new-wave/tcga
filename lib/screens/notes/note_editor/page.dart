import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/recipients/recipient_picker_component/component.dart';

import 'state.dart';
import 'view.dart';
import 'effect.dart';
import 'reducer.dart';

class NoteEditorPage extends Page<NoteEditorState, Map<String, dynamic>> {
  NoteEditorPage()
      : super(
          initState: initState,
          effect: buildEffect(),
          reducer: buildReducer(),
          view: buildView,
          dependencies: Dependencies<NoteEditorState>(
              adapter: null,
              slots: <String, Dependent<NoteEditorState>>{
                'recipient-picker': RecipientPickerStateConnector() +
                    RecipientPickerComponent(),
              }),
        );
}

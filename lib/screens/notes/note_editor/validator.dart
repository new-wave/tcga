import 'package:tcga_flutter/utils/app_localizations.dart';

import 'state.dart';

class Validator {
  static bool isValid(NoteEditorState state) {
    if (state.subjectEditController.text == null ||
        state.subjectEditController.text.isEmpty) return false;

    if (state.contentEditController.text == null ||
        state.contentEditController.text.isEmpty) return false;

    if (state.recipientId == null || state.recipientId.isEmpty) {
      if (state.recipient == null) return false;
    }

    if (state.priority == null) return false;

    return true;
  }
}

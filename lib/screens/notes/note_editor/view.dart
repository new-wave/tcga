import 'package:flutter/material.dart';

import 'package:fish_redux/fish_redux.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/widget/import.dart';

import 'action.dart';
import 'state.dart';
import 'validator.dart';

Widget buildView(
  NoteEditorState state,
  Dispatch dispatch,
  ViewService viewService,
) {
  return Builder(builder: (context) {
    return Padding(
      padding: EdgeInsets.only(
        bottom: MediaQuery.of(context).viewInsets.bottom,
      ),
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        // resizeToAvoidBottomPadding: false,
        backgroundColor: Colors.white,
        appBar: _appBar(state, viewService),
        body: Padding(
          padding: EdgeInsets.only(top: 24, right: 16, left: 16),
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.symmetric(vertical: spacingSm),
                  child: Text(
                    AppLocalizations.of(viewService.context)
                        .text("notes.editor.subject_required"),
                    style: textStylePrimary,
                  ),
                ),
                Container(
                  child: EntryWidget(
                    hint: AppLocalizations.of(viewService.context)
                        .text("notes.editor.enter_a_subject"),
                    controller: state.subjectEditController,
                    keyboardType: TextInputType.text,
                    warningText: AppLocalizations.instance.text('common.validator.subject'),
                    isRequired: true,
                    isCheckField: state.isCheckField ?? false,
                  ),
                ),
                SizedBox(
                  height: 8,
                ),
                Container(
                  margin: EdgeInsets.symmetric(vertical: spacingSm),
                  child: Text(AppLocalizations.of(viewService.context)
                      .text("notes.editor.content_required")),
                ),
                Container(
                  child: EntryWidget(
                    hint: AppLocalizations.of(viewService.context)
                        .text("notes.editor.enter_a_content"),
                    controller: state.contentEditController,
                    keyboardType: TextInputType.text,
                    warningText: AppLocalizations.instance.text('common.validator.content'),
                    isRequired: true,
                    isCheckField: state.isCheckField ?? false,
                  ),
                ),
                SizedBox(
                  height: (state.recipientId != null && state.recipientId.isNotEmpty) ? 8 : 16,
                ),
                (state.recipientId != null && state.recipientId.isNotEmpty) ? Container() : viewService.buildComponent(
                  'recipient-picker',
                ),
                (state.recipientId != null && state.recipientId.isNotEmpty) ? Container() : SizedBox(
                  height: 8,
                ),
                Container(
                  margin: EdgeInsets.symmetric(vertical: spacingSm),
                  child: Text(
                    AppLocalizations.of(viewService.context)
                        .text("notes.editor.priority_required"),
                    style: textStylePrimary,
                  ),
                ),
                PickerWidget(
                  items: NoteEditorState.priorities.values.toList(),
                  initialIndex: state.priority?.index,
                  title: AppLocalizations.of(viewService.context)
                      .text("notes.editor.priority_required"),
                  indexSelected: (index) => dispatch(
                    NoteEditorActionCreator.handlePrioritySelected(
                      index,
                    ),
                  ),
                ),
                SizedBox(
                  height: 8,
                ),
                Row(
                  children: [
                    Checkbox(
                      hoverColor: Colors.pink,
                      activeColor: colorPrimary,
                      value: state.isConfidential,
                      onChanged: (value) {
                        dispatch(
                          NoteEditorActionCreator.toggleIsConfidential(),
                        );
                      },
                    ),
                    GestureDetector(
                      onTap: () => dispatch(
                        NoteEditorActionCreator.toggleIsConfidential(),
                      ),
                      child: Text(
                        AppLocalizations.of(viewService.context)
                            .text("notes.editor.confidential"),
                        style: textStylePrimary,
                      ),
                    ),
                  ],
                ),
                Row(
                  children: [
                    SizedBox(
                      child: Checkbox(
                        hoverColor: Colors.pink,
                        activeColor: colorPrimary,
                        value: state.noteClass,
                        onChanged: (value) {
                          dispatch(
                            NoteEditorActionCreator.toggleNoteClass(),
                          );
                        },
                      ),
                    ),
                    GestureDetector(
                      onTap: () => dispatch(
                        NoteEditorActionCreator.toggleNoteClass(),
                      ),
                      child: Text(
                        AppLocalizations.of(viewService.context)
                            .text("notes.editor.note_class"),
                        style: textStylePrimary,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
        bottomNavigationBar: Builder(
          builder: (context) => Container(
            decoration: BoxDecoration(
                color: colorPrimary, border: Border.all(color: colorPrimary)),
            child: ButtonWidget(
              colorButton: colorPrimary,
              textButton:
                  AppLocalizations.of(viewService.context).text("common.save"),
              onPress: () {
                dispatch(
                  NoteEditorActionCreator.save(),
                );
              },
            ),
          ),
        ),
      ),
    );
  });
}

AppBar _appBar(NoteEditorState state, ViewService viewService) {
  return AppBar(
    elevation: 3,
    centerTitle: false,
    title: Text(
      state.title,
    ),
    leading: GestureDetector(
      onTap: () => Navigator.pop(viewService.context),
      child: Container(
        margin: EdgeInsets.only(left: 16),
        child: SvgPicture.asset('assets/icons/ic_arrow_left_circle.svg'),
      ),
    ),
    leadingWidth: 46,
  );
}

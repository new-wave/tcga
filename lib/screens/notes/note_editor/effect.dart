import 'package:fish_redux/fish_redux.dart';

import '../../../utils/format_date.dart';
import '../../../utils/utils.dart';
import '../manager.dart';
import 'validator.dart';
import 'action.dart';
import 'state.dart';

final _manager = NoteManager();

Effect<NoteEditorState> buildEffect() {
  return combineEffects(<Object, Effect<NoteEditorState>>{
    NoteEditorAction.save: _save,
  });
}

Future _save(
  Action action,
  Context<NoteEditorState> ctx,
) async {
  bool isValid = Validator.isValid(ctx.state);
  if (!isValid) {
    ctx.dispatch(NoteEditorActionCreator.checkField());
    return;
  }

  final state = ctx.state.clone()..diagnosedDate = DateTime.now();
  final result = state.onEditMode
      ? await _manager.update(state)
      : await _manager.create(state);

  if (result == null) return;
  result.viewType = state.viewType;
  ctx.goBack(result);
}

import 'package:tcga_flutter/database/models/enums.dart';
import 'package:tcga_flutter/screens/auth/manager.dart';

import '../../database/models.dart';
import '../../database/repository.dart';
import '../../utils/utils.dart';
import 'note_editor/state.dart';
import 'notes_page/note_item_component/state.dart';
import 'mapper.dart';

class NoteManager {
  static final _singleton = NoteManager._internal();

  factory NoteManager() {
    return _singleton;
  }

  NoteManager._internal();

  final _noteRepository = Repository<Note>();
  final _authManager = AuthManager();
  final _personRepository = Repository<Person>();

  Future<List<NoteItemState>> query(
    String careRecipientId, {
    String keyword,
    DateTime fromDate,
    DateTime toDate,
    NoteType noteType,
  }) async {
    final filter = (Note note) {
      bool isOk = true;
      if (careRecipientId.isNotEmpty) {
        isOk = note.personId != null && note.personId == careRecipientId;
        if (!isOk) return isOk;
      }

      if (fromDate != null && toDate != null) {
        isOk = note.createdOn.isDateBetween(fromDate, toDate);
        if (!isOk) return isOk;
      }

      isOk = noteType == null || noteType.index == note.type;
      if (!isOk) return isOk;

      if (keyword != null && keyword.trim().isNotEmpty) {
        isOk = note.subject.toLowerCase().contains(keyword.trim());
        if (!isOk) return isOk;
      }

      return isOk;
    };

    final items = await _noteRepository.query(
      filter: filter,
    );

    return items.map((x) => x.toState()).toList();
  }

  Future<NoteItemState> update(NoteEditorState state) async {
    final entity = state.toEntity();

    final _ = await _noteRepository.update(entity);

    final updatedEntity = await _noteRepository.get(entity.id);

    return updatedEntity.toState();
  }

  Future<NoteItemState> create(NoteEditorState state) async {
    final entity = state.toEntity();
    entity.personId =
        (state.recipientId != null && state.recipientId.isNotEmpty)
            ? state.recipientId
            : (state.recipient != null)
                ? state.recipient.id
                : '';
    entity.createdBy = _authManager.currentCaregiverId;
    entity.id = await _noteRepository.create(entity);

    final updatedEntity = await _noteRepository.get(entity.id);

    return updatedEntity.toState();
  }

  Future<String> getName() async {
    var name =
        await _personRepository.get(_authManager.currentCaregiverId).then(
              (x) => x.firstName + ' ' + x.lastName,
            );
    return name;
  }

// String getCretorName(String name ) {
//   getName().then((String value) => name = value);
//   return name;
// }
}

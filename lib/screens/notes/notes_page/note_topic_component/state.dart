import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:tcga_flutter/screens/notes/notes_page/state.dart';

class NoteTopicComponentSate extends Cloneable<NoteTopicComponentSate> {
  bool activeList;
  bool isFilter;

  NoteTopicComponentSate({
    this.activeList,
    this.isFilter,
  });

  @override
  NoteTopicComponentSate clone() {
    return NoteTopicComponentSate()
      ..activeList = activeList
      ..isFilter = isFilter;
  }
}

class HeaderTopicNotPgaeConnector
    extends ConnOp<NotesState, NoteTopicComponentSate> {
  @override
  NoteTopicComponentSate get(NotesState state) {
    NoteTopicComponentSate mState = NoteTopicComponentSate();
    mState.isFilter = state.isFilter ?? false;
    mState.activeList = state.activeListView ?? false;
    return mState;
  }

  @override
  void set(NotesState state, NoteTopicComponentSate subState) {
    subState.isFilter = state.isFilter ?? false;
    subState.activeList = state.activeListView ?? false;
  }
}

class Topic {
  String title;
  Color color;

  Topic({
    this.title,
    this.color,
  });
}

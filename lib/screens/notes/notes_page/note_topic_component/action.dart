import 'package:fish_redux/fish_redux.dart';

enum NoteTopicAction {
  activeListView,
}

class NoteTopicCreator {
  static Action activeListView(bool isActive) {
    return Action(
      NoteTopicAction.activeListView,
      payload: isActive,
    );
  }
}

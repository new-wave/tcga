import 'package:fish_redux/fish_redux.dart';

import 'state.dart';
import 'view.dart';
import 'effect.dart';

class NoteTopicComponent extends Component<NoteTopicComponentSate>{
  NoteTopicComponent():super(
    view: buildView,
    effect: buildEffect(),
  );
}
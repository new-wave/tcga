import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';

import '../../../note/action.dart';
import '../action.dart';
import 'action.dart';
import 'state.dart';

Widget buildView(
  NoteTopicComponentSate state,
  Dispatch dispatch,
  ViewService viewService,
) {
  return Column(
    children: [
      SizedBox(
        height: 10,
      ),
      Row(
        children: [
          SizedBox(
            width: 6,
          ),
          GestureDetector(
            child: Row(
              children: [
                Container(
                  width: 25,
                  height: 25,
                  padding: EdgeInsets.all(3),
                  decoration: BoxDecoration(
                    color: (state.isFilter) ? colorPrimary : Colors.transparent,
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: SvgPicture.asset(
                    "assets/icons/ic_filter.svg",
                    color: (state.isFilter) ? Colors.white : colorPrimary,
                  ),
                ),
                SizedBox(
                  width: 8.0,
                ),
                Text(AppLocalizations.of(viewService.context)
                    .text("notes.filter.filter")),
              ],
            ),
            onTap: () => dispatch(
              NotesActionCreator.handleClickFilter(),
            ),
          ),
          Spacer(),
          GestureDetector(
            child: Container(
              width: 25,
              height: 25,
              padding: EdgeInsets.all(5),
              decoration: BoxDecoration(
                color: (state.activeList) ? Colors.transparent : colorPrimary,
                borderRadius: BorderRadius.circular(5),
              ),
              child: SvgPicture.asset(
                "assets/icons/ic_list.svg",
                color: (state.activeList) ? colorPrimary : Colors.white,
              ),
            ),
            onTap: () {
              state.activeList = false;
              dispatch(
                NoteTopicCreator.activeListView(false),
              );
            },
          ),
          SizedBox(
            width: 12,
          ),
          GestureDetector(
            child: Container(
              child: SvgPicture.asset(
                "assets/icons/ic_grid.svg",
                color: (state.activeList) ? Colors.white : colorPrimary,
              ),
              width: 25,
              height: 25,
              padding: EdgeInsets.all(5),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                color: (state.activeList) ? colorPrimary : Colors.transparent,
              ),
            ),
            onTap: () {
              state.activeList = true;
              dispatch(NoteTopicCreator.activeListView(true));
            },
          ),
          SizedBox(
            width: 6,
          )
        ],
      ),
      Container(
        width: double.infinity,
        height: 17,
        margin: EdgeInsets.only(top: 27, bottom: 7),
        child: ListView.builder(
          padding: EdgeInsets.all(0),
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, index) => Row(
            children: [
              Container(
                width: 16,
                height: 16,
                decoration: BoxDecoration(
                    color: topics[index].color,
                    borderRadius: BorderRadius.circular(8)),
              ),
              SizedBox(
                width: 8,
              ),
              Text(topics[index].title),
              SizedBox(
                width: 30,
              ),
            ],
          ),
          itemCount: topics.length,
          shrinkWrap: true,
        ),
      ),
    ],
  );
}

final List<Topic> topics = [
  Topic(
    color: Color(0xff008080),
    title: AppLocalizations.instance.text('notes.note_type.allergy_intolerance'),
  ),
  Topic(
    title: AppLocalizations.instance.text('notes.note_type.contact'),
    color: Color(0xffA569BD),
  ),
  Topic(
    color: Color(0xff05944F),
    title: AppLocalizations.instance.text('notes.note_type.condition'),
  ),
  Topic(
    title: AppLocalizations.instance.text('notes.note_type.immunization'),
    color: Color(0xff00FF00),
  ),
  Topic(
    color: Color(0xff00FFFF),
    title: AppLocalizations.instance.text('notes.note_type.medication'),
  ),
  Topic(
    title: AppLocalizations.instance.text('notes.note_type.general'),
    color: Color(0xffFFC75A),
  ),
  Topic(
    color: Color(0xff2B86DA),
    title: AppLocalizations.instance.text('notes.note_type.appointment'),
  ),
];

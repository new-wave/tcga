import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/notes/notes_page/action.dart';

import 'action.dart';
import 'state.dart';

Effect<NoteTopicComponentSate> buildEffect(){
  return combineEffects(<Object,Effect<NoteTopicComponentSate>>{
    NoteTopicAction.activeListView:_activeListView
  });
}

void _activeListView(Action action,Context<NoteTopicComponentSate> context){
  context.dispatch(NotesActionCreator.activeListView(action.payload));
}


import 'package:fish_redux/fish_redux.dart';

import 'effect.dart';
import 'reducer.dart';
import 'state.dart';
import 'view.dart';

class NoteFilterPage extends Page<NoteFilterState, Map<String, dynamic>> {
  NoteFilterPage()
      : super(
    initState: initState,
    effect: buildEffect(),
    reducer: buildReducer(),
    view: buildView,
    // dependencies: Dependencies<NoteFilterState>(
    //     adapter: null,
    //     slots: <String, Dependent<NoteFilterState>>{
    //     }),
    // middleware: <Middleware<NoteFilterState>>[
    // ],
  );

}

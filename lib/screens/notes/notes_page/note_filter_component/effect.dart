import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart' hide Action;
import 'package:tcga_flutter/database/models/enums.dart';
import 'package:tcga_flutter/screens/recipients/state.dart';
import 'package:uuid/uuid.dart';

import '../../../recipients/manager.dart';
import '../state.dart';
import 'action.dart';
import 'state.dart';

final _uuid = Uuid();
final _manager = RecipientManager();

Effect<NoteFilterState> buildEffect() {
  return combineEffects(<Object, Effect<NoteFilterState>>{
    Lifecycle.initState: _init,
    NoteFilterAction.close: _close,
    NoteFilterAction.submit: _submit,
  });
}

void _init(Action action, Context<NoteFilterState> ctx) async {
  var state = ctx.state;
  if (state.fromDate == null) {
    state.fromDate = DateTime.now().subtract(Duration(days: 7));
  }
  if (state.toDate == null) {
    state.toDate = DateTime.now();
  }
  ctx.dispatch(
    NoteFilterActionCreator.initialize(state),
  );
}

void _close(Action action, Context<NoteFilterState> ctx) {
  final state = ctx.state;
  Navigator.of(ctx.context).pop(NotesState(
    recipientId: state.recipientId ?? _uuid.v4(),
    items: state.items,
    activeListView: state.activeListView,
    isFilter: false,
  ));
}

void _submit(Action action, Context<NoteFilterState> ctx) {
  final state = ctx.state;
  Navigator.of(ctx.context).pop(NotesState(
    recipientId: state.recipientId ?? _uuid.v4(),
    items: state.items,
    activeListView: state.activeListView,
    fromDate: state.fromDate,
    toDate: state.toDate,
    noteType: state.noteType,
    recipientItemState: state.recipientItemState,
    isFilter: true,
  ));
}

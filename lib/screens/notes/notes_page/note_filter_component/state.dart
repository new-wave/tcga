import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';

import '../../../../database/models/enums.dart';
import '../../../recipients/state.dart';
import '../../../recipients/state.dart';
import '../note_item_component/state.dart';
import '../state.dart';

class NoteFilterState implements Cloneable<NoteFilterState> {
  List<NoteItemState> items;
  String recipientId;
  bool activeListView;

  Map<NoteType, String> get noteTypes => {
        NoteType.AllergyIntolerance: AppLocalizations.instance
            .text('enums.note_type.allergyIntolerance'),
        NoteType.Contact:
            AppLocalizations.instance.text('enums.note_type.contact'),
        NoteType.Condition:
            AppLocalizations.instance.text('enums.note_type.condition'),
        NoteType.Immunization:
            AppLocalizations.instance.text('enums.note_type.immunization'),
        NoteType.Medication:
            AppLocalizations.instance.text('enums.note_type.medication'),
        NoteType.General:
            AppLocalizations.instance.text('enums.note_type.general'),
        NoteType.Appointment:
            AppLocalizations.instance.text('enums.note_type.appointment'),
      };

  List<RecipientItemState> authors = [];
  List<String> authorsString = [];

  DateTime fromDate;
  DateTime toDate;
  NoteType noteType;
  RecipientItemState recipientItemState;

  @override
  NoteFilterState clone() {
    return NoteFilterState()
      ..recipientId = recipientId
      ..items = items
      ..fromDate = fromDate
      ..toDate = toDate
      ..noteType = noteType
      ..authors = authors
      ..authorsString = authorsString
      ..recipientItemState = recipientItemState
      ..activeListView;
  }
}

NoteFilterState initState(Map<String, dynamic> args) {
  final note = args['item'] as NotesState;

  return NoteFilterState()
    ..recipientId = note?.recipientId
    ..items = note?.items
    ..activeListView = note?.activeListView
    ..fromDate = note?.fromDate
    ..toDate = note?.toDate
    ..noteType = note?.noteType
    ..recipientItemState = note?.recipientItemState;
}

import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/widget/button_widget.dart';

import '../../../../widget/date_picker_widget.dart';
import '../../../../widget/picker_widget.dart';
import 'action.dart';
import 'state.dart';

Widget buildView(
    NoteFilterState state, Dispatch dispatch, ViewService viewService) {
  return Container(
    padding: EdgeInsets.only(top: 16),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          padding: edgeAllInsetsPrimary,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Spacer(
                    flex: 5,
                  ),
                  Text(
                    AppLocalizations.of(viewService.context).text("notes.filter.filter"),
                    style: textStyleThird,
                  ),
                  Spacer(
                    flex: 4,
                  ),
                  GestureDetector(
                      onTap: () async => dispatch(
                        NoteFilterActionCreator.close(
                          state: state,
                        ),
                      ),
                      child: SvgPicture.asset("assets/icons/ic_close.svg")),
                ],
              ),
              SizedBox(
                height: 8,
              ),
              Row(
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          margin: edgeOnlyTopBotPrimaryMini,
                          child: Text(
                            AppLocalizations.of(viewService.context).text("notes.filter.from"),
                            style: textStylePrimary,
                          ),
                        ),
                        Container(
                          child: DatePickerWidget(
                            initial: state.fromDate,
                            max: DateTime.now(),
                            min: DateTime.now().subtract(
                              Duration(days: 30),
                            ),
                            dateSelected: (date) => dispatch(
                              NoteFilterActionCreator.handleStartDateSelected(
                                date,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    width: 8.0,
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          margin: edgeOnlyTopBotPrimaryMini,
                          child: Text(
                            AppLocalizations.of(viewService.context).text("notes.filter.to"),
                            style: textStylePrimary,
                          ),
                        ),
                        Container(
                          child: DatePickerWidget(
                            initial: state.toDate,
                            max: DateTime.now(),
                            min: DateTime.now().subtract(
                              Duration(days: 30),
                            ),
                            dateSelected: (date) => dispatch(
                              NoteFilterActionCreator.handleEndDateSelected(
                                date,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              Container(
                margin: edgeOnlyTopBotPrimaryMini,
                child: Text(
                  AppLocalizations.of(viewService.context).text("notes.filter.author"),
                  style: textStylePrimary,
                ),
              ),
              PickerWidget(
                initialIndex: state.recipientItemState?.fullName != null
                    ? state.authorsString?.indexOf(state.recipientItemState?.fullName)
                    : null,
                items: state.authorsString,
                title: '',
                suffixIcon: SvgPicture.asset(
                  'assets/icons/ic_author.svg',
                ),
                indexSelected: (index) {
                  dispatch(
                    NoteFilterActionCreator.handleAuthorSelected(
                      state.authors[index],
                    ),
                  );
                },
              ),
              Container(
                margin: edgeOnlyTopBotPrimaryMini,
                child: Text(
                  AppLocalizations.of(viewService.context).text("notes.filter.note_type"),
                  style: textStylePrimary,
                ),
              ),
              PickerWidget(
                initialIndex: state.noteType?.index,
                items: state.noteTypes.values.toList(),
                title: '',
                suffixIcon: SvgPicture.asset(
                  'assets/icons/ic_note_type.svg',
                ),
                indexSelected: (index) {
                  dispatch(
                    NoteFilterActionCreator.handleNoteTypeSelected(
                      index,
                    ),
                  );
                },
              ),
              SizedBox(
                height: 16.0,
              ),
            ],
          ),
        ),
        ButtonWidget(
          colorButton: colorPrimary,
          textButton: AppLocalizations.of(viewService.context).text("common.confirm"),
          onPress: () {
            dispatch(NoteFilterActionCreator.submit());
          },
        ),
      ],
    ),
  );
}

import 'package:fish_redux/fish_redux.dart';

import '../../../../database/models/enums.dart';
import '../../../../database/models/enums.dart';
import '../../../recipients/state.dart';
import 'action.dart';
import 'state.dart';

Reducer<NoteFilterState> buildReducer() {
  return asReducer(
    <Object, Reducer<NoteFilterState>>{
      NoteFilterAction.initialize: _initialize,
      NoteFilterAction.handleStartDateSelected: _handleStartDateSelected,
      NoteFilterAction.handleEndDateSelected: _handleEndDateSelected,
      NoteFilterAction.handleAuthorSelected: _handleAuthorSelected,
      NoteFilterAction.handleNoteTypeSelected: _handleNoteTypeSelected,
    },
  );
}

NoteFilterState _initialize(
  NoteFilterState state,
  Action action,
) {
  return action.payload.clone();
}

NoteFilterState _handleStartDateSelected(NoteFilterState state, Action action) {
  final NoteFilterState newState = state.clone();
  newState.fromDate = action.payload as DateTime;
  return newState;
}

NoteFilterState _handleEndDateSelected(NoteFilterState state, Action action) {
  final NoteFilterState newState = state.clone();
  newState.toDate = action.payload as DateTime;
  return newState;
}

NoteFilterState _handleAuthorSelected(NoteFilterState state, Action action) {
  final NoteFilterState newState = state.clone();
  newState.recipientItemState = action.payload as RecipientItemState;
  return newState;
}

NoteFilterState _handleNoteTypeSelected(NoteFilterState state, Action action) {
  final NoteFilterState newState = state.clone();
  int index = action.payload as int;
  newState.noteType = NoteType.values[index];
  return newState;
}

import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/notes/notes_page/note_filter_component/state.dart';

import '../../../recipients/state.dart';
import '../state.dart';

//TODO replace with your own action
enum NoteFilterAction {
  initialize,
  close,
  submit,
  handleStartDateSelected,
  handleEndDateSelected,
  handleNoteTypeSelected,
  handleAuthorSelected,
}

class NoteFilterActionCreator {
  static Action initialize(NoteFilterState result) {
    return Action(
      NoteFilterAction.initialize,
      payload: result,
    );
  }

  static Action close({NoteFilterState state}) {
    return Action(
      NoteFilterAction.close,
      payload: state,
    );
  }

  static Action submit() {
    return Action(
      NoteFilterAction.submit,
    );
  }

  static Action handleStartDateSelected(DateTime date) {
    return Action(
      NoteFilterAction.handleStartDateSelected,
      payload: date,
    );
  }

  static Action handleEndDateSelected(DateTime date) {
    return Action(
      NoteFilterAction.handleEndDateSelected,
      payload: date,
    );
  }

  static Action handleNoteTypeSelected(int index) {
    return Action(
      NoteFilterAction.handleNoteTypeSelected,
      payload: index,
    );
  }

  static Action handleAuthorSelected(RecipientItemState item) {
    return Action(
      NoteFilterAction.handleAuthorSelected,
      payload: item,
    );
  }
}

import 'package:fish_redux/fish_redux.dart';

import 'note_item_component/state.dart';
import 'state.dart';

enum NotesAction {
  initialize,
  create,
  handleNoteCreated,
  edit,
  handleNoteEdited,
  delete,
  handleNoteDeleted,
  addNote,
  activeListView,
  handleClickFilter,
  filter,
  search,
  refresh,
  handleRefresh,
}

class NotesActionCreator {
  static Action search(String textSearching) {
    return Action(
      NotesAction.search,
      payload: textSearching,
    );
  }

  static Action refresh() {
    return Action(
      NotesAction.refresh,
    );
  }

  static Action initialize(List<NoteItemState> items, String creatorName) {
    return Action(NotesAction.initialize, payload: [
      items,
      creatorName,
    ]);
  }

  static Action edit(NoteItemState item) {
    return Action(
      NotesAction.edit,
      payload: item,
    );
  }

  static Action handleNoteEdited(NoteItemState item) {
    return Action(
      NotesAction.handleNoteEdited,
      payload: item,
    );
  }

  static Action addNote(NoteItemState item) {
    return Action(
      NotesAction.addNote,
      payload: item,
    );
  }

  static Action delete(NoteItemState item) {
    return Action(
      NotesAction.delete,
      payload: item,
    );
  }

  static Action handleNoteDeleted(String id) {
    return Action(
      NotesAction.handleNoteDeleted,
      payload: id,
    );
  }

  static Action create(String recipientId) {
    return Action(
      NotesAction.create,
      payload: recipientId,
    );
  }

  static Action handleNoteCreated(NoteItemState item) {
    return Action(
      NotesAction.handleNoteCreated,
      payload: item,
    );
  }

  static Action activeListView(bool value) {
    return Action(
      NotesAction.activeListView,
      payload: value,
    );
  }

  static Action handleClickFilter() {
    return Action(
      NotesAction.handleClickFilter,
    );
  }

  static Action filter({NotesState item}) {
    return Action(
      NotesAction.filter,
      payload: item,
    );
  }
}

import 'package:fish_redux/fish_redux.dart';

import '../../../route_name.dart';
import '../../../utils/show_picker.dart';
import '../../../utils/utils.dart';

import '../manager.dart';

import 'action.dart';
import 'state.dart';

final _manager = NoteManager();

Effect<NotesState> buildEffect() {
  return combineEffects(<Object, Effect<NotesState>>{
    Lifecycle.initState: _init,
    NotesAction.refresh: _init,
    NotesAction.create: _create,
    NotesAction.edit: _edit,
    NotesAction.handleClickFilter: _handleClickFilter,
    NotesAction.search: _search,
  });
}

Future _search(Action action, Context<NotesState> ctx) async {
  String keyword = action.payload as String;
  var items = await _manager.query(
    ctx.state.recipientId,
    keyword: keyword,
  );
  ctx.dispatch(NotesActionCreator.initialize(items, ''));
}

Future _init(
  Action action,
  Context<NotesState> ctx,
) async {
  final name = await _manager.getName();
  final items = await _manager.query(
    ctx.state.recipientId,
  );

  ctx.dispatch(
    NotesActionCreator.initialize(items, name),
  );
}

Future _create(Action action, Context<NotesState> ctx) async {
  final result = await ctx.navigateTo(
    RouteName.notesEditor,
    arguments: {
      'recipient-id': ctx.state.recipientId,
      'viewType': ctx.state.activeListView,
    },
  );

  if (result == null) return;

  ctx.dispatch(
    NotesActionCreator.handleNoteCreated(
      result,
    ),
  );
}

Future _edit(Action action, Context<NotesState> ctx) async {
  if (!action.payload.allowsToEdit) return;

  final result = await ctx.navigateTo(
    RouteName.notesEditor,
    arguments: {
      'item': action.payload,
      'recipient-id': ctx.state.recipientId,
    },
  );

  if (result == null) return;

  ctx.dispatch(
    NotesActionCreator.handleNoteEdited(
      result,
    ),
  );
}

Future _handleClickFilter(Action action, Context<NotesState> ctx) async {
  final parameter = ctx.state;
  var result = await showModal2(ctx.context, RouteName.noteFilter, args: {
    'item': parameter,
  }) as NotesState;

  if (result == null) return;

  NotesState newState;
  final items = await _manager.query(
    result.recipientId,
    fromDate: result.fromDate,
    toDate: result.toDate,
    noteType: result.noteType,
  );
  newState = result.clone()
    ..isFilter = result.isFilter
    ..items = items;

  ctx.dispatch(
    NotesActionCreator.filter(
      item: newState,
    ),
  );
}

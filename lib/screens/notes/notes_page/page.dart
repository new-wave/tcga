import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/notes/notes_page/note_header_component/component.dart';
import 'package:tcga_flutter/screens/notes/notes_page/note_topic_component/component.dart';
import 'package:tcga_flutter/screens/notes/notes_page/note_topic_component/state.dart';

import 'notes_adapter/adapter.dart';
import 'effect.dart';
import 'state.dart';
import 'view.dart';
import 'reducer.dart';

class NotesPage extends Page<NotesState, Map<String, dynamic>> {
  NotesPage()
      : super(
          initState: initState,
          effect: buildEffect(),
          reducer: buildReducer(),
          view: buildView,
          dependencies: Dependencies<NotesState>(
            adapter: NoneConn<NotesState>() + NotesAdapter(),
            slots: <String, Dependent<NotesState>>{
              'header-note-page':
                  HeaderNotePageConnector() + HeaderNotePageComponent(),
              'header-topic-note-page':
                  HeaderTopicNotPgaeConnector() + NoteTopicComponent()
            },
          ),
        );
}

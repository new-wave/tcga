import 'package:fish_redux/fish_redux.dart';

import '../../../utils/format_date.dart';
import 'action.dart';
import 'note_item_component/state.dart';
import 'state.dart';

Reducer<NotesState> buildReducer() {
  return asReducer(
    <Object, Reducer<NotesState>>{
      NotesAction.initialize: _initialize,
      NotesAction.filter: _handleFilter,
      NotesAction.handleNoteCreated: _handleNoteAdded,
      NotesAction.handleNoteEdited: _handleNoteEdited,
      NotesAction.handleNoteDeleted: _handleNoteDeleted,
      NotesAction.activeListView: _handleNoteView,
    },
  );
}

NotesState _initialize(NotesState state, Action action) {
  final NotesState newState = state.clone();

  newState.items = action.payload[0] as List<NoteItemState> ?? [];
  newState.creatorName = action.payload[1] as String;
  newState.items.forEach((element) {
    element.creatorName = newState.creatorName;
  });
  return newState;
}

NotesState _handleNoteAdded(NotesState state, Action action) {
  final item = action.payload as NoteItemState;
  final newState = state.clone();
  item.creatorName = newState.creatorName;
  newState.items.add(item);
  newState.activeListView=state.activeListView;
  return newState;
}

NotesState _handleNoteEdited(NotesState state, Action action) {
  final item = action.payload as NoteItemState;
  final newState = state.clone()..activeListView=state.activeListView;

  final editedIndex = newState.items.indexWhere(
    (x) => x.id == item.id,
  );
  item.creatorName = newState.creatorName;
  newState.items[editedIndex] = item;

  return newState;
}

NotesState _handleNoteDeleted(NotesState state, Action action) {
  final id = action.payload;
  final newState = state.clone();

  final deletedIndex = newState.items.indexWhere(
    (x) => x.id == id,
  );

  newState.items.removeAt(deletedIndex);

  return newState;
}

NotesState _handleNoteView(NotesState state, Action action) {
  NotesState newState = state.clone();
  newState.activeListView = action.payload;
  for (int i = 0; i < newState.items.length; i++) {
    newState.items[i].viewType = !action.payload;
  }
  return newState;
}

NotesState _handleFilter(NotesState state, Action action) {
  final noteState = action.payload as NotesState;
  return noteState;
}

import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/screens/notes/notes_page/note_header_component/state.dart';

import '../../../database/models/enums.dart';
import '../../recipients/state.dart';
import 'note_item_component/state.dart';

class NotesState extends MutableSource implements Cloneable<NotesState> {
  List<NoteItemState> items;
  String recipientId;
  bool activeListView;

  bool isFilter;
  DateTime fromDate;
  DateTime toDate;
  NoteType noteType;
  RecipientItemState recipientItemState;
  String creatorName;

  NotesState(
      {this.recipientId,
      this.items,
      this.activeListView,
      this.isFilter = false,
      this.fromDate,
      this.toDate,
      this.noteType,
      this.recipientItemState,
      this.creatorName});

  @override
  NotesState clone() {
    return NotesState()
      ..items = items
      ..recipientId = recipientId
      ..isFilter = isFilter
      ..fromDate = fromDate
      ..toDate = toDate
      ..noteType = noteType
      ..recipientItemState = recipientItemState
      ..creatorName = creatorName
      ..activeListView;
  }

  @override
  Object getItemData(int index) => items[index];

  @override
  String getItemType(int index) => 'note-item';

  @override
  int get itemCount => items?.length ?? 0;

  @override
  void setItemData(int index, Object data) => items[index] = data;
}

NotesState initState(Map<String, dynamic> arg) {
  if (arg == null) return NotesState()..recipientId = '';
  return NotesState()..recipientId = arg['recipient-id'] ?? '';
}

class HeaderNotePageConnector extends ConnOp<NotesState, HeaderNotePageSate> {
  @override
  HeaderNotePageSate get(NotesState state) {
    return HeaderNotePageSate();
  }
}

class NotesStateConnector extends ConnOp<NotesState, NoteItemState>
    with ReselectMixin<NotesState, NoteItemState> {
  @override
  NoteItemState computed(NotesState state) {
    return NoteItemState()..creatorName = state.creatorName;
  }

  @override
  List<dynamic> factors(NotesState state) {
    return [state.creatorName];
  }

  @override
  void set(NotesState state, NoteItemState subState) {
    state.creatorName = subState.creatorName;
  }
}

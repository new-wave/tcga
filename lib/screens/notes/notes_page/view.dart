import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tcga_flutter/screens/drawer/drawer_page.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';

import 'action.dart';
import 'state.dart';

Widget buildView(NotesState state, Dispatch dispatch, ViewService viewService) {
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
  GlobalKey<RefreshIndicatorState>();

  final ListAdapter adapter = viewService.buildAdapter();
  return Builder(builder: (context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 3,
        backgroundColor: Colors.white,
        leading: state.recipientId.isEmpty
            ? null
            : IconButton(
          iconSize: 24,
          icon: SvgPicture.asset('assets/icons/ic_arrow_left_circle.svg'),
          onPressed: () {
            Navigator.pop(viewService.context);
          },
        ),
        title:
        Text(AppLocalizations.of(viewService.context).text('notes.notes')),
        actions: [
          IconButton(
            iconSize: 24,
            icon: SvgPicture.asset('assets/icons/ic_button_add.svg'),
            onPressed: () {
              dispatch(
                NotesActionCreator.create(
                  state.recipientId,
                ),
              );
            },
          )
        ],
      ),
      drawer: state.recipientId.isEmpty ? DrawerPage() : null,
      body: SafeArea(
        top: false,
        bottom: true,
        child: Container(
          padding: EdgeInsets.only(
            left: 16,
            right: 16,
            top: 16.0,
          ),
          child: Column(
            children: [
              viewService.buildComponent('header-note-page'),
              viewService.buildComponent('header-topic-note-page'),
              Expanded(
                child: RefreshIndicator(
                  key: _refreshIndicatorKey,
                  child: ListView.separated(
                    physics: AlwaysScrollableScrollPhysics(),
                    shrinkWrap: true,
                    padding: EdgeInsets.only(top: 15),
                    separatorBuilder: (context, index) =>
                        Divider(
                          height: 16,
                          thickness: 0,
                          color: Colors.transparent,
                        ),
                    itemCount: adapter.itemCount,
                    itemBuilder: adapter.itemBuilder,
                  ),
                  onRefresh: (){
                    dispatch(
                      NotesActionCreator.refresh(),
                    );
                    return Future.delayed(const Duration(seconds: 0));
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  });
}
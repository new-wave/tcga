import 'package:fish_redux/fish_redux.dart';

import '../note_item_component/component.dart';
import '../state.dart';

class NotesAdapter extends SourceFlowAdapter<NotesState> {
  NotesAdapter()
      : super(
          pool: <String, Component<Object>>{'note-item': NoteItemComponent()},
        );
}

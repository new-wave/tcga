import 'package:fish_redux/fish_redux.dart';

import 'state.dart';
import 'view.dart';

class NoteItemComponent extends Component<NoteItemState> {
  NoteItemComponent()
      : super(
          view: buildView,
        );
}

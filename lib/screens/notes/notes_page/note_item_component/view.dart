import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/screens/notes/notes_page/note_topic_component/state.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/utils/format_date.dart';
import 'package:tcga_flutter/widget/list_item_widget.dart';

import '../../../../database/models/enums.dart';
import '../action.dart';
import 'state.dart';

Widget buildView(
  NoteItemState state,
  Dispatch dispatch,
  ViewService viewService,
) {
  if (state.viewType == null) {
    state.viewType = true;
  }
  return Container(
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(5),
      color: Color(0xffF4F4F4),
    ),
    child: ListItemWidget(
      onTap: () => dispatch(
        NotesActionCreator.edit(
          state,
        ),
      ),
      child: Row(
        children: [
          Container(
            width: 5,
            height: (state.viewType) ? 65 : 230,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5),
              color: checkItemColor(state.type.toString().split(".")[1]),
            ),
          ),
          SizedBox(
            width: 11,
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Row(
                  children: [
                    Container(
                      width: MediaQuery.of(viewService.context).size.width - 85,
                      child: Text(
                        state.subject,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            color: colorPrimary,
                            fontSize: 16,
                            fontWeight: FontWeight.w400),
                      ),
                    ),
                    Spacer(),
                    !state.viewType
                        ? GestureDetector(
                            child: Container(
                              child:
                                  SvgPicture.asset("assets/icons/ic_edit.svg"),
                            ),
                            onTap: () => dispatch(
                              NotesActionCreator.edit(
                                state,
                              ),
                            ),
                          )
                        : Container()
                  ],
                ),
                (state.viewType)
                    ? Container()
                    : buildRemigeGridView(state, dispatch, viewService),
                SizedBox(
                  height: 5,
                ),
                Row(
                  children: [
                    SvgPicture.asset(
                      'assets/icons/ic_user_profile.svg',
                      width: 20,
                      height: 20,
                      fit: BoxFit.cover,
                    ),
                    SizedBox(
                      width: 6.33,
                    ),
                    //state.creatorName != null ?
                    Text(
                      state.creatorName ?? "",
                      style: TextStyle(
                        color: Color(0xff000000),
                        fontSize: 12,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                    //: Text('aaa'),
                    Spacer(),
                    Text(
                      formatDateTime(state.createdOn) ?? "",
                      style: TextStyle(
                        color: Color(0xff000000),
                        fontSize: 12,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                    SizedBox(
                      width: 5.5,
                    ),
                    SvgPicture.asset(
                      'assets/icons/ic_clock.svg',
                      width: 20,
                      height: 20,
                      fit: BoxFit.cover,
                    ),
                  ],
                ),
              ],
            ),
          ),
          SizedBox(
            width: 15,
          ),
        ],
      ),
    ),
  );
}

Widget buildBorderBottom(
  NoteItemState state,
  Dispatch dispatch,
  ViewService viewService,
) {
  return Container(
    width: double.infinity,
    height: (state.viewType) ? 0 : 1,
    color: Color(0xffE0E0E0),
    margin: EdgeInsets.only(
      top: (state.viewType) ? 0 : 12.75,
      bottom: (state.viewType) ? 0 : 12.75,
    ),
  );
}

Widget buildRemigeGridView(
  NoteItemState state,
  Dispatch dispatch,
  ViewService viewService,
) {
  return Column(
    mainAxisAlignment: MainAxisAlignment.start,
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      buildBorderBottom(state, dispatch, viewService),
      Text(
        "${AppLocalizations.of(viewService.context).text("notes.filter.note_type")}: ${state.type.toString().split(".")[1]}",
        style: TextStyle(
          color: colorTextPrimary,
          fontSize: 14,
          fontWeight: FontWeight.w400,
        ),
      ),
      buildBorderBottom(state, dispatch, viewService),
      Text(
        "${AppLocalizations.of(viewService.context).text("notes.priority")}: ${state.priority.toString().split(".")[1]}",
        style: TextStyle(
          color: colorTextPrimary,
          fontSize: 14,
          fontWeight: FontWeight.w400,
        ),
      ),
      buildBorderBottom(state, dispatch, viewService),
      Text(
        "${AppLocalizations.of(viewService.context).text("notes.description")}: ${state.content}",
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
        style: TextStyle(
          color: colorTextPrimary,
          fontSize: 14,
          fontWeight: FontWeight.w400,
        ),
      ),
      buildBorderBottom(state, dispatch, viewService),
    ],
  );
}

Color checkItemColor(String value) {
  for (int i = 0; i < topics.length; i++) {
    if (topics[i].title.toLowerCase() == value.toLowerCase()) {
      return topics[i].color;
    }
  }

  return Color(0xff008080);
}

final List<Topic> topics = [
  Topic(
    color: Color(0xff008080),
    title:
        AppLocalizations.instance.text('notes.note_type.allergy_intolerance'),
  ),
  Topic(
    title: AppLocalizations.instance.text('notes.note_type.contact'),
    color: Color(0xffA569BD),
  ),
  Topic(
    color: Color(0xff05944F),
    title: AppLocalizations.instance.text('notes.note_type.condition'),
  ),
  Topic(
    title: AppLocalizations.instance.text('notes.note_type.immunization'),
    color: Color(0xff00FF00),
  ),
  Topic(
    color: Color(0xff00FFFF),
    title: AppLocalizations.instance.text('notes.note_type.medication'),
  ),
  Topic(
    title: AppLocalizations.instance.text('notes.note_type.general'),
    color: Color(0xffFFC75A),
  ),
  Topic(
    color: Color(0xff2B86DA),
    title: AppLocalizations.instance.text('notes.note_type.appointment'),
  ),
];

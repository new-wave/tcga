import 'package:fish_redux/fish_redux.dart';
import 'package:tcga_flutter/database/models/enums.dart';

import '../../manager.dart';

final _noteManager = NoteManager();

class NoteItemState implements Cloneable<NoteItemState> {
  String id;
  String subject;
  String content;
  NotePriority priority;
  bool noteClass;
  bool isConfidential;
  NoteType type;
  String recipientId;
  String reference;
  bool viewType;
  DateTime createdOn;
  String createdBy;
  String creatorName;

  //String get creatorName => _noteManager.getCretorName();

  NoteItemState({
    this.id,
    this.subject,
    this.content,
    this.priority,
    this.noteClass,
    this.isConfidential,
    this.type,
    this.recipientId,
    this.reference,
    this.viewType,
    this.createdOn,
    this.createdBy,
    this.creatorName = '',
  });

  bool get allowsToEdit =>
      this.createdOn != null &&
      DateTime.now().difference(this.createdOn).inHours < 24;

  @override
  NoteItemState clone() {
    return NoteItemState()
      ..id = id
      ..subject = subject
      ..content = content
      ..priority = priority
      ..noteClass = noteClass
      ..isConfidential = isConfidential
      ..type = type
      ..recipientId = recipientId
      ..reference = reference
      ..viewType = viewType
      ..createdOn = createdOn
      ..createdBy = createdBy
      ..creatorName = creatorName;
  }
}

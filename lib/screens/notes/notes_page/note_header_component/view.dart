import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/screens/notes/notes_page/action.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/widget/entry_widget.dart';

import 'state.dart';

Widget buildView(
  HeaderNotePageSate sate,
  Dispatch dispatch,
  ViewService viewService,
) {
  return EntryWidget(
    hint: AppLocalizations.of(viewService.context).text("notes.search_your_note"),
    titleStyle: textStylePrimary,
    textStyle: textStylePrimaryFontWeight,
    keyboardType: TextInputType.text,
    onTextChanged: (text) {
      dispatch(
        NotesActionCreator.search(text),
      );
    },
    suffixIcon: SvgPicture.asset(
      'assets/icons/ic_search.svg',
      color: colorPrimary,
      fit: BoxFit.none,
    ),
  );
}

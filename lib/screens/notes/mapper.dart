import 'package:tcga_flutter/database/models.dart';
import 'package:tcga_flutter/database/models/enums.dart';

import 'note_editor/state.dart';
import 'notes_page/note_item_component/state.dart';

extension EntityToStateMapper on Note {
  NoteItemState toState() => NoteItemState(
        id: this.id,
        content: this.content,
        subject: this.subject,
        noteClass: this.noteClass,
        isConfidential: this.isConfidential, // state: db
        priority:
            this.priority != null ? NotePriority.values[this.priority] : null,
        type: this.type != null ? NoteType.values[this.type] : null,
        recipientId: this.personId,
        reference: this.reference,
        createdOn: this.createdOn,
        createdBy: this.createdBy,
      );
}

extension StateToEntityMapper on NoteEditorState {
  Note toEntity() => Note(
        id: this.id,
        content: this.contentEditController.text,
        subject: this.subjectEditController.text,
        isConfidential: this.isConfidential, //db: chỗ gán data (editor)
        noteClass: this.noteClass,
        priority: this.priority.index,
        type: this.type?.index ?? NoteType.General.index,
        personId: this.recipientId,
        reference: this.reference,
        createdBy: this.createdBy,
      );
}

import 'package:flutter/material.dart';

class Avatar extends StatelessWidget {
  final String avatarUrl;
  final String fullName;

  const Avatar({
    Key key,
    this.avatarUrl,
    this.fullName,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return avatarUrl == null
        ? Container(
            alignment: Alignment.center,
            width: 40,
            height: 40,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: Color(0xff2B86DA),
            ),
            child: Text(
              createAvatar(fullName),
              style: TextStyle(
                color: Colors.white,
                fontSize: 14,
                fontWeight: FontWeight.w400,
              ),
            ),
          )
        : ClipRRect(
            borderRadius: BorderRadius.circular(20),
            child: Image.network(
              avatarUrl,
              fit: BoxFit.cover,
              width: 40,
              height: 40,
            ),
          );
  }
}

String createAvatar(String name) {
  if (name.length > 1) {
    List<String> arrName = name.split(" ");
    if (arrName.length == 1) {
      return arrName[0][0].toUpperCase() + arrName[0][1].toUpperCase();
    }
    return arrName[0][0].toUpperCase() + arrName[1][0].toUpperCase();
  }
  return name.toUpperCase();
}

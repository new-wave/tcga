import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mdi/mdi.dart';
import 'package:tcga_flutter/config/palette.dart';

class AppbarIcon extends StatefulWidget implements PreferredSizeWidget {
  AppbarIcon({
    Key key,
    this.title,
    this.textStyle,
    this.setIconSearch,
    this.setIconAdd,
    this.pressOn,
  })  : preferredSize = Size.fromHeight(kToolbarHeight),
        super(key: key);
  final String title;
  final TextStyle textStyle;
  @override
  final Size preferredSize; // default is 56.0
  final bool setIconSearch;
  final bool setIconAdd;
  final GestureTapCallback pressOn;

  @override
  _AppbarIconState createState() => _AppbarIconState(
      title: title,
      textStyle: textStyle,
      setIconSearch: setIconSearch,
      setIconAdd: setIconAdd,
      pressOn: pressOn);
}

class _AppbarIconState extends State<AppbarIcon> {
  _AppbarIconState(
      {this.title,
      this.textStyle,
      this.setIconSearch,
      this.setIconAdd,
      this.pressOn});
  String title;
  final TextStyle textStyle;
  final bool setIconSearch;
  final bool setIconAdd;
  final GestureTapCallback pressOn;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 33, right: 16, left: 16),
      decoration: BoxDecoration(color: Colors.white, boxShadow: [
        BoxShadow(
          color: Colors.white.withOpacity(0.07),
          offset: Offset(0.0, 0.75),
        ),
      ]),
      child: Row(
        children: [
          IconButton(
            iconSize: 24,
            icon: Icon(
              Mdi.arrowLeftCircle,
              color: colorDisabled,
            ),
            onPressed: () {
              setState(
                () {
                  Navigator.pop(context);
                },
              );
            },
          ),
          Spacer(
            flex: 2,
          ),
          Text(title, style: textStyle ?? textStyleTitleThird),
          Spacer(),
          Visibility(
            visible: setIconSearch ?? true,
            child: SvgPicture.asset(
              "assets/icons/ic_search.svg",
              height: 24,
              width: 24,
            ),
          ),
          Spacer(),
          GestureDetector(
            onTap: pressOn,
            child: Visibility(
                visible: setIconAdd ?? true,
                child: SvgPicture.asset(
                  "assets/icons/ic_button_add.svg",
                  height: 24,
                  width: 24,
                )),
          )
        ],
      ),
    );
  }
}

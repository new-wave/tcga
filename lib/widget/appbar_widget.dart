import 'package:flutter/material.dart';
import 'package:mdi/mdi.dart';
import 'package:tcga_flutter/config/palette.dart';

class CustomAppBar extends StatefulWidget implements PreferredSizeWidget {
  CustomAppBar({
    Key key,
    this.title,
    this.textStyle,
    this.canGoBack,
  })  : preferredSize = Size.fromHeight(kToolbarHeight),
        super(key: key);
  final String title;
  final TextStyle textStyle;
  @override
  final Size preferredSize; // default is 56.0
  final Function canGoBack;

  @override
  _CustomAppBarState createState() => _CustomAppBarState(
      title: title, textStyle: textStyle, canGoBack: canGoBack);
}

class _CustomAppBarState extends State<CustomAppBar> {
  _CustomAppBarState({this.title, this.textStyle, this.canGoBack});
  String title;
  final TextStyle textStyle;
  final Function canGoBack;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 33, right: 16, left: 16),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          IconButton(
            iconSize: 24,
            icon: Icon(
              Mdi.arrowLeftCircle,
              color: colorDisabled,
            ),
            onPressed: () {
              if (this.canGoBack != null && this.canGoBack() == false) {
                return;
              }

              Navigator.pop(context);
            },
          ),
          Spacer(
            flex: 9,
          ),
          Text(
            title,
            style: textStyle,
          ),
          Spacer(
            flex: 12,
          ),
          SizedBox(
            width: 24,
          )
        ],
      ),
    );
  }
}

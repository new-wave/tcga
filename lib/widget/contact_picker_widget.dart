import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/screens/contacts/state.dart';
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/address_item_component/state.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import '../route_name.dart';
import 'entry_widget.dart';

class ContactPickerWidget extends StatefulWidget {
  final Function(AddressItemState) contactSelected;

  final String title;
  final TextStyle titleStyle;
  final TextStyle hintStyle;
  final TitlePosition titlePosition;
  final TextStyle textStyle;
  final Widget suffixIcon;
  final Widget prefixIcon;
  final Color fillColor;
  final TextAlign textAlign;
  final bool readOnly;
  final AddressItemState contact;

  ContactPickerWidget({
    Key key,
    this.titlePosition = TitlePosition.hint,
    this.contactSelected,
    this.title,
    this.prefixIcon,
    this.suffixIcon,
    this.contact,
    this.fillColor = colorBackground,
    this.textAlign = TextAlign.left,
    this.titleStyle = const TextStyle(
      fontSize: 14,
    ),
    this.textStyle = const TextStyle(
      fontSize: 14,
    ),
    this.hintStyle = const TextStyle(
      fontSize: 14,
      color: colorDisabled,
    ),
    this.readOnly = false,
  }) : super(key: key);

  @override
  _ContactPickerWidgetState createState() => _ContactPickerWidgetState();
}

class _ContactPickerWidgetState extends State<ContactPickerWidget> {
  final TextEditingController _controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    _controller.text = widget.contact?.title ??
        AppLocalizations.instance.text('contact.picker');
    return SizedBox(
      height: 36,
      child: Stack(
        children: [
          EntryWidget(
            titlePosition: widget.titlePosition,
            controller: _controller,
            fillColor: widget.fillColor,
            suffixIcon: widget.suffixIcon ??
                SvgPicture.asset(
                  'assets/icons/ic_address.svg',
                ),
            textAlign: widget.textAlign,
            textStyle: widget.contact?.title != null ? widget.titleStyle : widget.hintStyle,
          ),
          InkWell(
            onTap: () async {
              if (widget.readOnly) return;

              final item = await Navigator.of(this.context).pushNamed(
                RouteName.contactsPicker,
                arguments: {
                      'selected-item': widget.contact,
                    } ??
                    Map<String, dynamic>(),
              );

              if (item == null) return;

              AddressItemState addressItemState;
              if (item is ContactItemState) {
                addressItemState = AddressItemState(
                  id: item.id,
                  title: item.fullName,
                );
              }

              if (widget.contactSelected != null) {
                widget.contactSelected(addressItemState);
              }
            },
            child: SizedBox.expand(
              child: Container(
                color: Colors.transparent,
              ),
            ),
          )
        ],
      ),
    );
  }
}

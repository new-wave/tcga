import 'package:flutter/material.dart';
import 'package:tcga_flutter/config/palette.dart';

class SelectableOptionsWidget extends StatefulWidget {
  final Axis scrollDirection;
  final List<String> options;
  final int initialIndex;
  final Function(int) indexSelected;

  const SelectableOptionsWidget({
    Key key,
    this.scrollDirection = Axis.horizontal,
    this.options,
    this.initialIndex,
    this.indexSelected,
  }) : super(
          key: key,
        );

  @override
  _SelectableOptionsWidgetState createState() =>
      _SelectableOptionsWidgetState();
}

class _SelectableOptionsWidgetState extends State<SelectableOptionsWidget> {
  int _selectedIndex;

  @override
  void initState() {
    super.initState();
    _selectedIndex = widget.initialIndex;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 36,
      child: SingleChildScrollView(
        scrollDirection: widget.scrollDirection,
        child: widget.scrollDirection == Axis.horizontal
            ? Row(
                children: _generateOptions(),
              )
            : Column(
                children: _generateOptions(),
              ),
      ),
    );
  }

  List<Widget> _generateOptions() {
    return List.generate(
      widget.options.length * 2,
      (index) {
        if (index.isOdd) {
          return widget.scrollDirection == Axis.horizontal
              ? SizedBox(
                  width: 16,
                )
              : SizedBox(
                  height: 16,
                );
        }

        final actualIndex = index ~/ 2;

        return InkWell(
          onTap: () {
            setState(() {
              _selectedIndex = actualIndex;
            });

            if (widget.indexSelected != null) {
              widget.indexSelected(actualIndex);
            }
          },
          child: Container(
            height: 36,
            padding: const EdgeInsets.symmetric(
              horizontal: 16,
            ),
            decoration: BoxDecoration(
              color:
                  _selectedIndex == actualIndex ? colorPrimary : Colors.white,
              border: Border.all(
                color: _selectedIndex == actualIndex
                    ? colorPrimary
                    : colorTextPrimary,
              ),
              borderRadius: BorderRadius.circular(18),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  widget.options[actualIndex],
                  style: _selectedIndex == actualIndex
                      ? textStylePrimary.copyWith(
                          color: Colors.white,
                        )
                      : textStylePrimary,
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}

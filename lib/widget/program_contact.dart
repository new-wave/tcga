import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/models/program_contact.dart';

class ProgramContactComponent extends StatelessWidget {
  final Program program;

  const ProgramContactComponent({
    Key key,
    this.program,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      alignment: Alignment.center,
      padding: edgeHorizonThird,
      margin: EdgeInsets.only(bottom: 8.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        color: colorBackground,
      ),
      child: Row(
        children: [
          Container(
            child: Text(program.nameProgram),
          ),
          Spacer(),
          Container(
            height: 24,
            width: 24,
            child: SvgPicture.asset('assets/icons/ic_edit.svg'),
          ),
        ],
      ),
    );
  }
}

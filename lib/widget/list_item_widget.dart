import 'package:flutter/material.dart';
import 'package:mdi/mdi.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';

enum ContextPositioned { start, center, end }

class ListItemWidget extends StatelessWidget {
  final Widget child;
  final Function(String) onContextActionSelected;
  final GestureTapCallback onTap;
  final List<String> contextActions;
  final EdgeInsetsGeometry padding;
  final ContextPositioned contextPositioned;
  final bool contextActionsVisible;
  const ListItemWidget({
    Key key,
    this.child,
    this.onContextActionSelected,
    this.onTap,
    this.contextActions,
    this.padding = const EdgeInsets.symmetric(horizontal: 0.0),
    this.contextPositioned = ContextPositioned.center,
    this.contextActionsVisible = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    final button = contextActions != null && contextActions.length > 0
        ? PopupMenuButton(
            child: Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 8,
                vertical: 8,
              ),
              child: Icon(Mdi.dotsVertical),
            ),
            itemBuilder: (_) => List.generate(
              contextActions.length,
              (index) => new PopupMenuItem(
                child: Text(
                  AppLocalizations.instance.text(contextActions[index]),
                ),
                value: contextActions[index],
              ),
            ),
            onSelected: (value) async {
              if (onContextActionSelected != null) {
                onContextActionSelected(value);
              }
            },
          )
        : SizedBox.shrink();
    return Container(
      padding: padding,
      alignment: Alignment.center,
      width: size.width,
      child: InkWell(
        onTap: () {
          if (onTap != null) {
            onTap();
          }
        },
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: child,
            ),
            if (contextActionsVisible) button,
          ],
        ),
      ),
    );
  }
}

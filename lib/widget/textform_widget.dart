import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:tcga_flutter/config/palette.dart';

import '../config/palette.dart';

class BigTextFormWidget extends StatelessWidget {
  BigTextFormWidget({
    this.labeltext,
    this.controller,
    this.hinText,
    this.textStyle,
    this.validate,
    this.iconForm,
    this.showLabel,
    this.onChange,
    this.focusNode,
    this.setEnable,
    this.textInputAction = TextInputAction.next,
  });
  final Function onChange;
  final String labeltext;
  final TextEditingController controller;
  final String hinText;
  final TextStyle textStyle;
  final bool validate;
  final Widget iconForm;
  final bool showLabel;
  //final Padding padding;
  final FocusNode focusNode;
  final TextInputAction textInputAction;
  final bool setEnable;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 40,
      alignment: Alignment.center,
      padding: edgeHorizonThird,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        color: colorBackground,
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Center(
            child: Visibility(
              visible: showLabel ?? false,
              child: Text(
                "$labeltext",
                style: textFieldStyle,
              ),
            ),
          ),
          Visibility(
            visible: showLabel ?? false,
            child: SizedBox(
              width: 10,
            ),
          ),
          Expanded(
            child: Center(
              child: TextFormField(
                enabled: setEnable ?? true,
                textAlignVertical: TextAlignVertical.top,
                controller: controller,
                style: textStyle,
                autofocus: false,
                textInputAction: textInputAction,
                focusNode: focusNode,
                decoration: InputDecoration(
                  contentPadding: iconForm != null
                      ? EdgeInsets.zero
                      : EdgeInsets.only(bottom: 12),
                  border: InputBorder.none,
                  focusedBorder: InputBorder.none,
                  enabledBorder: InputBorder.none,
                  errorBorder: InputBorder.none,
                  disabledBorder: InputBorder.none,
                  hintStyle: textField,
                  suffixIcon: iconForm,
                  suffixIconConstraints:
                      BoxConstraints(minWidth: 24, minHeight: 24),
                  hintText: hinText,
                ),
                onChanged: onChange,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

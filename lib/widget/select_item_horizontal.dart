import 'package:flutter/material.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/widget/select_button_widget.dart';

class SelectItemHorizontal extends StatefulWidget {
  final GestureTapCallback press;
  final String text;
  final bool choose;

  SelectItemHorizontal({
    this.press,
    this.text,
    this.choose,
  });

  @override
  _SelectItemHorizontalState createState() => _SelectItemHorizontalState();
}

class _SelectItemHorizontalState extends State<SelectItemHorizontal> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(right: spacingPrimary),
      child: SelectButtonWidget(
        onTap: widget.press,
        buttonColor: widget.choose == true ? buttonBackgroundColor : colorWhite,
        buttonTextColor:
            widget.choose == true ? Colors.white : colorTextPrimary,
        buttonBorderColor:
            widget.choose == true ? buttonBackgroundColor : colorTextPrimary,
        buttonText: widget.text,
      ),
    );
  }
}

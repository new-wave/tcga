// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_svg/flutter_svg.dart';
// import 'package:intl/intl.dart';
// import 'package:mdi/mdi.dart';

// import 'package:tcga_flutter/config/palette.dart';
// import 'package:tcga_flutter/config/style.dart';
// import 'package:tcga_flutter/screens/calendar/model/select_around_time.dart';
// import 'package:tcga_flutter/screens/calendar/model/time_zone.dart';
// import 'package:tcga_flutter/screens/calendar/widget/timezone.dart';

// import 'button_widget.dart';
// import 'datetime_select_controller.dart';
// import 'textform_widget.dart';

// final f = new DateFormat('hh:mm a');
// final f1 = new DateFormat('d/MM/y');

// Future<SelectAroundTime> showBottomSheetSelection(
//   BuildContext context,
//   bool isAllDay,
//   DateTime startTime,
//   DateTime endTime,
//   TimeZone timeZone,
//   Function selectTimeZone,
//   List<TimeZone> listTimeZonze,
// ) async {
//   SelectAroundTime _selectAroundTime = await showModalBottomSheet(
//     context: context,
//     backgroundColor: Colors.transparent,
//     builder: (BuildContext build) {
//       return SelectTime(
//         isAllDay: isAllDay,
//         startTime: startTime,
//         endTime: endTime,
//         timeZone: timeZone,
//         listTimeZonze: listTimeZonze,
//         selectTimeZone: selectTimeZone,
//       );
//     },
//   );
//   return _selectAroundTime;
// }

// class SelectTime extends StatefulWidget {
//   bool isAllDay;
//   DateTime startTime;
//   DateTime endTime;
//   TimeZone timeZone;
//   Function selectTimeZone;
//   List<TimeZone> listTimeZonze;
//   SelectTime({
//     Key key,
//     this.isAllDay,
//     this.endTime,
//     this.startTime,
//     this.timeZone,
//     this.listTimeZonze,
//     this.selectTimeZone,
//   });
//   @override
//   _SelectTimeState createState() => _SelectTimeState(
//         isAllDay: isAllDay,
//         startTime: startTime,
//         endTime: endTime,
//         timeZone: timeZone,
//         listTimeZonze: listTimeZonze,
//         selectTimeZone: selectTimeZone,
//       );
// }

// class _SelectTimeState extends State<SelectTime> {
//   bool isAllDay;
//   DateTime startTime;
//   DateTime endTime;
//   TextEditingController controller;
//   TimeZone timeZone;
//   Function selectTimeZone;
//   List<TimeZone> listTimeZonze;
//   _SelectTimeState({
//     Key key,
//     this.isAllDay,
//     this.controller,
//     this.endTime,
//     this.startTime,
//     this.timeZone,
//     this.listTimeZonze,
//     this.selectTimeZone,
//   });
//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       padding: edgeAllInsetsPrimary,
//       decoration: BoxDecoration(
//         borderRadius: BorderRadius.vertical(top: Radius.circular(15.0)),
//         color: Colors.white,
//       ),
//       height: 380,
//       child: Column(
//         crossAxisAlignment: CrossAxisAlignment.stretch,
//         children: [
//           Row(
//             children: [
//               Spacer(
//                 flex: 5,
//               ),
//               Text(
//                 'Select Time',
//                 style: Style.labelTitleItem,
//               ),
//               Spacer(
//                 flex: 4,
//               ),
//               InkWell(
//                 onTap: () async => Navigator.pop(context),
//                 child: SvgPicture.asset(
//                   "assets/icons/ic_close.svg",
//                 ),
//               ),
//             ],
//           ),
//           Column(
//             crossAxisAlignment: CrossAxisAlignment.stretch,
//             children: [
//               Text(
//                 'Start Time',
//                 textAlign: TextAlign.start,
//                 style: Style.labelNormal,
//               ),
//               Row(
//                 children: [
//                   Expanded(
//                     flex: 6,
//                     child: Container(
//                       margin: EdgeInsets.only(
//                         top: spacingSm,
//                       ),
//                       child: GestureDetector(
//                         onTap: () async {
//                           var date =
//                               await dateTimeController(context, startTime);
//                           if (date != null) {
//                             setState(() {
//                               date = date
//                                   .subtract(new Duration(hours: date.hour))
//                                   .add(new Duration(
//                                       hours: startTime.hour,
//                                       minutes: startTime.minute));
//                               startTime = date;
//                             });
//                           }
//                         },
//                         child: BigTextFormWidget(
//                           showLabel: false,
//                           hinText: '${f1.format(startTime)}',
//                           textStyle: textField,
//                           setEnable: false,
//                           iconForm:
//                               SvgPicture.asset('assets/icons/ic_date.svg'),
//                         ),
//                       ),
//                     ),
//                   ),
//                   SizedBox(
//                     width: 16,
//                   ),
//                   Expanded(
//                     flex: 4,
//                     child: Container(
//                       margin: EdgeInsets.only(
//                         top: spacingSm,
//                       ),
//                       child: GestureDetector(
//                         onTap: () async {
//                           var time = await showTimePicker(
//                             cancelText: '',
//                             context: context,
//                             initialTime: TimeOfDay.now(),
//                           );
//                           if (time != null) {
//                             setState(() {
//                               startTime = startTime
//                                   .subtract(new Duration(
//                                       hours: startTime.hour,
//                                       minutes: startTime.minute))
//                                   .add(new Duration(
//                                       hours: time.hour, minutes: time.minute));
//                             });
//                           }
//                         },
//                         child: BigTextFormWidget(
//                           showLabel: false,
//                           hinText: '${f.format(startTime)}',
//                           textStyle: textField,
//                           setEnable: false,
//                           iconForm: SvgPicture.asset(
//                             'assets/icons/ic_alarm_clock.svg',
//                             color: colorPrimary,
//                           ),
//                         ),
//                       ),
//                     ),
//                   ),
//                 ],
//               ),
//             ],
//           ),
//           Spacer(),
//           Column(
//             crossAxisAlignment: CrossAxisAlignment.stretch,
//             children: [
//               Text(
//                 'End Time',
//                 textAlign: TextAlign.start,
//                 style: Style.labelNormal,
//               ),
//               Row(
//                 children: [
//                   Expanded(
//                     flex: 6,
//                     child: Container(
//                       margin: EdgeInsets.only(
//                         top: spacingSm,
//                       ),
//                       child: GestureDetector(
//                         onTap: () async {
//                           var date = await dateTimeController(context, endTime);
//                           if (date != null) {
//                             setState(() {
//                               date = date
//                                   .subtract(new Duration(hours: date.hour))
//                                   .add(new Duration(
//                                       hours: endTime.hour,
//                                       minutes: endTime.minute));
//                               endTime = date;
//                             });
//                           }
//                         },
//                         child: BigTextFormWidget(
//                           showLabel: false,
//                           hinText: '${f1.format(endTime)}',
//                           textStyle: textField,
//                           setEnable: false,
//                           iconForm:
//                               SvgPicture.asset('assets/icons/ic_date.svg'),
//                         ),
//                       ),
//                     ),
//                   ),
//                   SizedBox(
//                     width: 16,
//                   ),
//                   Expanded(
//                     flex: 4,
//                     child: Container(
//                       margin: EdgeInsets.only(
//                         top: spacingSm,
//                       ),
//                       child: GestureDetector(
//                         onTap: () async {
//                           var time = await showTimePicker(
//                             cancelText: '',
//                             context: context,
//                             initialTime: TimeOfDay.now(),
//                           );
//                           if (time != null) {
//                             setState(() {
//                               endTime = endTime
//                                   .subtract(new Duration(
//                                       hours: endTime.hour,
//                                       minutes: endTime.minute))
//                                   .add(new Duration(
//                                       hours: time.hour, minutes: time.minute));
//                             });
//                           }
//                         },
//                         child: BigTextFormWidget(
//                           showLabel: false,
//                           hinText: '${f.format(endTime)}',
//                           textStyle: textField,
//                           setEnable: false,
//                           iconForm: SvgPicture.asset(
//                             'assets/icons/ic_alarm_clock.svg',
//                             color: colorPrimary,
//                           ),
//                         ),
//                       ),
//                     ),
//                   ),
//                 ],
//               ),
//             ],
//           ),
//           Spacer(),
//           InkWell(
//             onTap: () async {
//               var _timeZone =
//                   await showTimeZone(context, listTimeZonze, selectTimeZone);
//               if (_timeZone != null) {
//                 setState(() {
//                   widget.timeZone = _timeZone;
//                 });
//               }
//             },
//             child: BigTextFormWidget(
//               showLabel: false,
//               hinText:
//                   '${widget.timeZone.timeZone}, ${widget.timeZone.utcOffset}',
//               textStyle: textField,
//               setEnable: false,
//               iconForm: Container(
//                 child: Icon(
//                   Mdi.earth,
//                   color: colorPrimary,
//                 ),
//               ),
//             ),
//           ),
//           Divider(
//             color: Colors.white,
//             height: spacingSm,
//           ),
//           Container(
//             child: Row(
//               children: [
//                 Checkbox(
//                   value: isAllDay,
//                   onChanged: (value) {
//                     setState(() {
//                       isAllDay = value;
//                     });
//                   },
//                 ),
//                 Text(
//                   'All day event',
//                   style: Style.labelNormal,
//                 ),
//               ],
//             ),
//           ),
//           Spacer(),
//           Container(
//             margin: EdgeInsets.only(bottom: spacingMd),
//             height: 45,
//             width: double.infinity,
//             child: ButtonWidget(
//               onPress: () {
//                 SelectAroundTime selectAroundTime = SelectAroundTime(
//                   startTime: startTime,
//                   endTime: endTime,
//                   isAllDay: isAllDay,
//                   timeZone: timeZone,
//                 );
//                 Navigator.of(context).pop(selectAroundTime);
//               },
//               colorButton: colorPrimary,
//               textButton: "Confirm",
//             ),
//           ),
//         ],
//       ),
//     );
//   }
// }

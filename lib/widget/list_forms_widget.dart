import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mdi/mdi.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/config/style.dart';
import 'package:tcga_flutter/screens/recipients/state.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';

class ListForms extends StatefulWidget {
  final String label;
  final List<RecipientItemState> careRecipientState;
  final Function onPressed;
  final Function onClickUser;

  ListForms({
    Key key,
    this.label,
    this.careRecipientState,
    this.onPressed,
    this.onClickUser,
  });

  @override
  _ListFormsState createState() => _ListFormsState();
}

class _ListFormsState extends State<ListForms> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        color: colorBackground,
      ),
      margin: EdgeInsets.symmetric(vertical: spacingSm),
      padding: EdgeInsets.only(
        left: spacingMd,
        right: spacingSm,
        bottom: spacingMd,
      ),
      child: Column(
        children: <Widget>[
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  '${widget.label}',
                  style: textStylePrimary,
                ),
                IconButton(
                  icon: SvgPicture.asset(
                    "assets/icons/ic_user_group.svg",
                    color: colorPrimary,
                    height: 24,
                    width: 24,
                    fit: BoxFit.none,
                  ),
                  onPressed: widget.onClickUser,
                )
              ],
            ),
          ),
          if (widget.careRecipientState == null ||
              widget.careRecipientState.length < 1)
            Row(
              mainAxisSize: MainAxisSize.max,
              children: [
                Expanded(
                  child: Text(
                    AppLocalizations.of(context)
                        .text('calendar.editor.placeholder_attendee_picker'),
                    style: Style.labelNormalOpacity,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                  ),
                ),
              ],
            )
          else
            ...widget.careRecipientState
                .map(
                  (e) => Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.all(spacingXs),
                          color: Colors.white,
                          child: Text(
                            '${e.fullName}',
                            textAlign: TextAlign.start,
                            style: Style.labelNormal,
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                          ),
                        ),
                      ),
                      IconButton(
                          icon: Icon(Mdi.close),
                          onPressed: () {
                            widget.onPressed(e);
                          })
                    ],
                  ),
                )
                .toList(),
        ],
      ),
    );
  }
}

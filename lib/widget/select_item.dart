import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class SelectItem extends StatelessWidget {
  final String text;

  const SelectItem({
    Key key,
    this.text,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        color: Color(0xffF4F4F4),
      ),
      padding: EdgeInsets.only(left: 16.78, right: 16.78, top: 11, bottom: 11),
      height: 40,
      child: Row(
        children: [
          Text(
            text,
            style: TextStyle(
              color: Color(0xff323232),
              fontSize: 14,
              fontWeight: FontWeight.w400,
            ),
          ),
          Spacer(),
          SvgPicture.asset(
            "assets/icons/ic_down.svg",
            width: 11,
            height: 6,
          ),
        ],
      ),
    );
  }
}

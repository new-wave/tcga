import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';

class ErrorWidget extends StatelessWidget {
  final String error;
  final Future<void> onTap;
  ErrorWidget({
    this.error,
    this.onTap,
  });
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.zero,
      height: 120,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            alignment: Alignment.topLeft,
            child: Text(
              AppLocalizations.instance.text('common.error'),
              style: TextStyle(
                  color: colorTextPrimary,
                  fontSize: 18,
                  fontWeight: FontWeight.w600),
            ),
          ),
          Container(
            alignment: Alignment.center,
            child: Text(
              error,
              style: TextStyle(
                color: colorTextPrimary,
                fontSize: fontSizeLarge,
              ),
            ),
          ),
          Container(
            alignment: Alignment.bottomRight,
            child: GestureDetector(
              onTap: () {
                EasyLoading.dismiss();
              },
              child: Text(
                AppLocalizations.instance.text('common.ok'),
                style: TextStyle(
                  fontSize: 16.0,
                  color: colorTextPrimary,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/utils/show_picker.dart';
import '../utils/string_extensions.dart';
import 'entry_widget.dart';

class PickerWidget extends StatefulWidget {
  final GestureTapCallback tap;
  final List<String> items;
  final Function(int) indexSelected;
  final int initialIndex;
  final double height;

  final String title;
  final TextStyle titleStyle;
  final String hint;
  final TextStyle hintStyle;
  final TextStyle textStyle;
  final Widget suffixIcon;
  final Widget prefixIcon;
  final Color fillColor;
  final TextAlign textAlign;
  final bool readOnly;
  final TextEditingController controller;
  final String warningText;

  final bool isCheckField;
  final bool isRequired;

  PickerWidget({
    Key key,
    this.tap,
    this.items,
    this.indexSelected,
    this.initialIndex,
    this.title,
    this.prefixIcon,
    this.suffixIcon,
    this.fillColor = colorBackground,
    this.textAlign = TextAlign.left,
    this.titleStyle = const TextStyle(
      fontSize: 14,
    ),
    this.textStyle = const TextStyle(
      fontSize: 14,
    ),
    this.readOnly = false,
    this.controller,
    this.hint,
    this.hintStyle,
    this.height = 36,
    this.warningText,
    this.isCheckField = false,
    this.isRequired = false,
  }) : super(key: key);

  @override
  _PickerWidgetState createState() => _PickerWidgetState();
}

class _PickerWidgetState extends State<PickerWidget> {
  TextEditingController controller;
  int selectedIndex;
  bool showWarningText;

  @override
  void initState() {
    super.initState();
    showWarningText = false;
    controller = widget.controller ?? TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    showWarningText = controller.text.isEmptyOrWhitespace;
    final isValidInitialIndex = widget.items != null &&
        widget.initialIndex != null &&
        widget.initialIndex > -1 &&
        widget.items.length > widget.initialIndex;

    if (isValidInitialIndex) {
      selectedIndex = widget.initialIndex;
      controller.text = widget.items[selectedIndex];
    }

    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          height: widget.height,
          child: Stack(
            children: [
              EntryWidget(
                height: widget.height,
                controller: controller,
                fillColor: widget.fillColor,
                prefixIcon: widget.prefixIcon,
                readOnly: widget.readOnly,
                suffixIcon: widget.suffixIcon ??
                    SvgPicture.asset(
                      'assets/icons/ic_caret.svg',
                    ),
                textAlign: widget.textAlign,
                textStyle: widget.textStyle,
                title: widget.title,
                titleStyle: widget.titleStyle,
                hint: widget.hint,
                hintStyle: widget.hintStyle,
              ),
              InkWell(
                onTap: () async {
                  if (widget.readOnly) return;

                  if (widget.items != null && widget.items.length > 0) {
                    final selectedIndex = await showPicker(
                      context,
                      widget.items,
                      title: widget.hint ?? widget.title,
                      initValue: this.selectedIndex,
                      isRequired: widget.isRequired,
                    );

                    if (selectedIndex == null) return;

                    this.selectedIndex = selectedIndex;
                    if(selectedIndex >= 0) {
                      controller.text = AppLocalizations.instance
                          .text(widget.items[selectedIndex]);
                    } else {
                      controller.text = '';
                    }
                    if (widget.indexSelected != null) {
                      setState(() {});
                      widget.indexSelected(selectedIndex);
                    }
                    return;
                  }
                  if (widget.tap != null) {
                    widget.tap();
                  }
                },
                child: SizedBox.expand(
                  child: Container(
                    color: Colors.transparent,
                  ),
                ),
              )
            ],
          ),
        ),
        Visibility(
          visible: (widget.isRequired && widget.isCheckField && showWarningText),
          child: Container(
            alignment: Alignment.centerLeft,
            margin: const EdgeInsets.only(top: 5),
            child: Text(
              widget.warningText ?? '',
              style: textStylePrimaryRed,
              maxLines: 2,
            ),
          ),
        ),
      ],
    );
  }
}

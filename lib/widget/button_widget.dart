import 'dart:io';

import 'package:flutter/material.dart';

class ButtonWidget extends StatelessWidget {
  ButtonWidget({
    Key key,
    this.onPress,
    this.colorButton,
    this.textButton,
    this.borderRadius,
    this.height = 45,
  });

  final GestureTapCallback onPress;
  final Color colorButton;
  final String textButton;
  final BorderRadius borderRadius;
  double height;

  @override
  Widget build(BuildContext context) {
    if (Platform.isIOS) {
      height += MediaQuery.of(context).padding.bottom / 2;
    }
    return GestureDetector(
      onTap: onPress,
      child: Container(
        alignment: Alignment.center,
        height: height,
        width: double.infinity,
        decoration: BoxDecoration(
          borderRadius: borderRadius,
          color: colorButton,
        ),
        child: Text(
          textButton,
          style: TextStyle(
            fontSize: 14,
            fontWeight: FontWeight.w500,
            color: Colors.white,
          ),
        ),
      ),
    );
  }
}

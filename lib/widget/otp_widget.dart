import 'package:flutter/material.dart';
import 'package:tcga_flutter/config/palette.dart';

class OtpWidget extends StatefulWidget {
  final bool isSecured;
  final int maxLength;
  final bool isCheckField;
  final String textWarning;
  final TextEditingController controller;

  const OtpWidget({
    Key key,
    this.isSecured = false,
    this.maxLength = 6,
    this.controller,
    this.isCheckField,
    this.textWarning,
  }) : super(key: key);

  @override
  _OtpWidgetState createState() => _OtpWidgetState(
        this.isSecured,
        this.maxLength,
        this.controller,
      );
}

class _OtpWidgetState extends State<OtpWidget> {
  final bool isSecured;
  final int maxLength;
  final TextEditingController controller;

  FocusNode focusNode;

  String _otp;

  _OtpWidgetState(
    this.isSecured,
    this.maxLength,
    this.controller,
  );

  @override
  void initState() {
    super.initState();

    focusNode = FocusNode();

    _otp = '';
  }

  @override
  void dispose() {
    // Clean up the focus node when the Form is disposed.
    focusNode.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (!focusNode.hasPrimaryFocus) {
      focusNode.requestFocus();
    }
    return Container(
      child: Stack(
        children: [
          Opacity(
            opacity: 0.0,
            child: TextFormField(
              focusNode: focusNode,
              controller: controller,
              decoration: InputDecoration(
                border: InputBorder.none,
                contentPadding: const EdgeInsets.all(0),
              ),
              keyboardType: TextInputType.number,
              maxLength: this.maxLength,
              autofocus: true,
              onChanged: (text) {
                setState(() {
                  _otp = text;
                });
              },
            ),
          ),
          GestureDetector(
            onTap: () {
              if (!focusNode.hasPrimaryFocus) {
                focusNode.requestFocus();
              }
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: List.generate(
                this.maxLength,
                (index) {
                  final letter = index < _otp.length
                      ? this.isSecured
                          ? '*'
                          : _otp[index]
                      : '';
                  return Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 4.0),
                    child: _SingleLetterWidget(
                      width: 30,
                      letter: letter,
                      indicatorColor: colorSecondary,
                      textStyle: TextStyle(
                        fontSize: 18,
                      ),
                    ),
                  );
                },
              ),
            ),
          ),
          Visibility(
            visible: widget.isCheckField ?? false,
            child: Container(
              margin: EdgeInsets.only(top: 50),
              alignment: Alignment.center,
              child: Text(
                widget.textWarning ?? '',
                style: textStylePrimaryRed,
                maxLines: 2,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class _SingleLetterWidget extends StatelessWidget {
  final String letter;
  final double width;
  final double indicatorHeight;
  final Color indicatorColor;
  final TextStyle textStyle;

  const _SingleLetterWidget({
    Key key,
    this.letter,
    this.width,
    this.indicatorHeight,
    this.indicatorColor,
    this.textStyle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: this.width == null ? 48 : this.width,
      child: Column(
        children: [
          Text(
            this.letter,
            style: textStyle,
          ),
          SizedBox(
            height: 6,
          ),
          SizedBox(
            height: this.indicatorHeight ?? 3,
            child: Container(
              color: this.indicatorColor,
            ),
          ),
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mdi/mdi.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/config/style.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/utils/format_date.dart';
import '../screens/calendar/timeslot_picker_page/timeslot_item_component/state.dart';

class TimeForms extends StatefulWidget {
  final String label;
  final List<TimeslotItemState> list;
  final Function removeSelectTime;

  final bool isCheckField;
  final bool isRequired;
  final String warningText;

  TimeForms({
    Key key,
    this.label,
    this.list,
    this.removeSelectTime,
    this.isCheckField = false,
    this.isRequired = false,
    this.warningText = '',
  });

  @override
  _TimeFormsState createState() => _TimeFormsState();
}

class _TimeFormsState extends State<TimeForms> {
  bool showWarningText;

  @override
  void initState() {
    showWarningText = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    showWarningText = (widget.list.isEmpty && widget.isCheckField);

    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        color: colorBackground,
      ),
      margin: EdgeInsets.symmetric(vertical: spacingSm),
      padding: EdgeInsets.only(
        left: spacingMd,
        right: spacingSm,
        bottom: spacingMd,
      ),
      child: Column(
        children: <Widget>[
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  '${widget.label}',
                  style: textStylePrimary,
                ),
                SvgPicture.asset(
                  "assets/icons/ic_date.svg",
                  color: colorPrimary,
                  height: 24,
                  width: 24,
                  fit: BoxFit.fill,
                ),
              ],
            ),
          ),
          if (widget.list == null || widget.list.length < 1)
            Row(
              children: [
                Text(
                  AppLocalizations.of(context)
                      .text('calendar.editor.placeholder_time_slot'),
                  style: Style.labelNormalOpacity,
                ),
              ],
            )
          else
            ...widget.list
                .map((item) => Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          child: Container(
                            padding: EdgeInsets.only(
                                left: 8, top: 4, bottom: 4, right: 4),
                            color: Colors.white,
                            height: 30,
                            alignment: Alignment.centerLeft,
                            child: Text(
                              '${dateFormatHM(item.startAt)}, duration ${item.durationInMinute * 5 + 5} minute(s)',
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                color: colorPrimary,
                                fontSize: fontSizeNormal,
                              ),
                              overflow: TextOverflow.ellipsis,
                              maxLines: 1,
                            ),
                          ),
                        ),
                        IconButton(
                          icon: Icon(Mdi.close),
                          onPressed: () {
                            widget.removeSelectTime(item);
                          },
                        )
                      ],
                    ))
                .toList(),
          Visibility(
            visible:
                (widget.isCheckField && widget.isRequired && showWarningText),
            child: Container(
              alignment: Alignment.centerLeft,
              margin: const EdgeInsets.only(top: 5),
              child: Text(
                widget.warningText ?? '',
                style: textStylePrimaryRed,
                maxLines: 2,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl/intl.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/utils/date_formatter.dart';
import 'package:tcga_flutter/utils/format_date.dart';
import 'package:tcga_flutter/utils/phone_format.dart';
import '../utils/string_extensions.dart';
import 'entry_widget.dart';

class DatePickerWidget extends StatefulWidget {
  static final DateFormat dateFormat = DateFormat('MMM dd, yyyy');

  final DateTime initial;
  final DateTime max;
  final DateTime min;

  final Function(DateTime) dateSelected;

  final String title;
  final TextStyle titleStyle;
  final TitlePosition titlePosition;
  final TextStyle textStyle;
  final Widget suffixIcon;
  final Widget prefixIcon;
  final Color fillColor;
  final TextAlign textAlign;
  final bool readOnly;
  final String warningText;
  final bool isCheckField;
  final bool isRequired;

  DatePickerWidget({
    Key key,
    @required this.initial,
    @required this.max,
    @required this.min,
    this.titlePosition = TitlePosition.hint,
    this.dateSelected,
    this.title,
    this.prefixIcon,
    this.suffixIcon,
    this.fillColor = colorBackground,
    this.textAlign = TextAlign.left,
    this.titleStyle = const TextStyle(
      fontSize: 14,
    ),
    this.textStyle = const TextStyle(
      fontSize: 14,
    ),
    this.readOnly = false,
    this.isCheckField = false,
    this.isRequired = false,
    this.warningText,
  }) : super(key: key);

  @override
  _DatePickerWidgetState createState() => _DatePickerWidgetState();
}

class _DatePickerWidgetState extends State<DatePickerWidget> {
  final TextEditingController _controller = TextEditingController();
  bool showWarningText;
  final _focusNode = FocusNode();

  @override
  void initState() {
    super.initState();
    showWarningText = false;

    _focusNode.addListener(() {
      bool hasFocus = _focusNode.hasFocus;
      if (!hasFocus) {
        DateTime dateTime = formatStringToDateTimeWithFormat(
            getHintDateFormat(),
            _controller.text.length == 10 ? _controller.text : '');
        if (dateTime == null) return;
        widget.dateSelected(dateTime);
      }
    });
  }

  @override
  void dispose() {
    _focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _controller.text =
        widget.initial != null ? dateFormatYMMMDLocale(widget.initial) : '';
    showWarningText = _controller.text.isEmptyOrWhitespace;
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          height: 36,
          child: EntryWidget(
            focusNode: _focusNode,
            titlePosition: widget.titlePosition,
            controller: _controller,
            fillColor: widget.fillColor,
            prefixIcon: widget.prefixIcon,
            readOnly: widget.readOnly,
            hint: getHintDateFormat(),
            onTextChanged: (text) {
              if (text.length == 10) {
                DateTime dateTime =
                    formatStringToDateTimeWithFormat(getHintDateFormat(), text);
                if (dateTime == null) return;
                widget.dateSelected(dateTime);
              }
            },
            formatters: [
              WhitelistingTextInputFormatter(RegExp("[0-9/]")),
              LengthLimitingTextInputFormatter(10),
              DateFormatter(),
            ],
            suffixIcon: InkWell(
              onTap: () async {
                if (widget.readOnly) return;

                final result = await showDatePicker(
                  context: context,
                  initialDate: widget.initial ?? widget.max ?? DateTime.now(),
                  firstDate: widget.min,
                  lastDate: widget.max,
                );

                if (result == null) return;

                _controller.text = dateFormatYMMMDLocale(result);

                DateTime selectedDate = DateTime(
                  result.year,
                  result.month,
                  result.day,
                  DateTime.now().hour,
                  DateTime.now().minute,
                  DateTime.now().second,
                  DateTime.now().millisecond,
                  DateTime.now().microsecond,
                );

                if (widget.dateSelected != null) {
                  widget.dateSelected(selectedDate);
                }
              },
              child: widget.suffixIcon ??
                  SvgPicture.asset(
                    'assets/icons/ic_date.svg',
                  ),
            ),
            textAlign: widget.textAlign,
            textStyle: widget.textStyle,
            title: widget.title,
            titleStyle: widget.titleStyle,
          ),
        ),
        Visibility(
          visible:
              (widget.isRequired && widget.isCheckField && showWarningText),
          child: Container(
            alignment: Alignment.centerLeft,
            margin: const EdgeInsets.only(top: 5),
            child: Text(
              widget.warningText ?? '',
              style: textStylePrimaryRed,
              maxLines: 2,
            ),
          ),
        ),
      ],
    );
  }
}

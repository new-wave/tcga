// import 'dart:async';
// import 'package:flutter/material.dart';
// import 'package:flutter_svg/svg.dart';
// import 'package:intl/intl.dart';
// import 'package:tcga_flutter/config/palette.dart';
// import 'package:tcga_flutter/config/style.dart';
// import 'package:tcga_flutter/database/models/appointment.dart';
// import 'package:tcga_flutter/widget/button_widget.dart';
// import 'package:timetable/timetable.dart';
// import 'package:time_machine/time_machine.dart';

// import 'time_slot_forms_widget.dart';

// final f = new DateFormat('HH:mm');

// class SelectTimeSlot extends StatefulWidget {
//   @override
//   _SelectTimeSlotState createState() => _SelectTimeSlotState();
// }

// class _SelectTimeSlotState extends State<SelectTimeSlot> {
//   @override
//   Widget build(BuildContext context) {
//     return Container();
//   }
// }

// Future<TimeSlot> selectTimeSlot(
//     BuildContext context, Function onPressed, int duration) async {
//   final TimeSlot item = await showGeneralDialog(
//     context: context,
//     barrierDismissible: true,
//     barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
//     barrierColor: Colors.black87.withOpacity(0.3),
//     transitionDuration: const Duration(milliseconds: 200),
//     pageBuilder: (context, _, __) {
//       return TimeSelect(
//         onPressed: onPressed,
//         duration: duration,
//       );
//     },
//   );
//   print(duration);
//   return item;
// }

// class TimeSelect extends StatefulWidget {
//   final Function(TimeSlot) onPressed;
//   int duration;
//   TimeSelect({
//     Key key,
//     this.onPressed,
//     this.duration,
//   });
//   @override
//   _TimeSelectState createState() => _TimeSelectState(
//         onPressed: onPressed,
//         duration: duration,
//       );
// }

// class _TimeSelectState extends State<TimeSelect> {
//   final Function(TimeSlot) onPressed;
//   int duration;
//   _TimeSelectState({
//     Key key,
//     this.onPressed,
//     this.duration,
//   });
//   TimetableController<Appointment> _controller;
//   var eventprovider = EventProvider<Appointment>.list([]);

//   DateTime _date;
//   @override
//   void initState() {
//     _date = DateTime.now();
//     _controller = TimetableController<Appointment>(
//       eventProvider: eventprovider,
//       initialDate: LocalDate.today(),
//       visibleRange: VisibleRange.days(1),
//       initialTimeRange: InitialTimeRange.range(
//         startTime: LocalTime(8, 0, 0),
//         endTime: LocalTime(18, 0, 0),
//       ),
//       firstDayOfWeek: DayOfWeek.monday,
//     );
//     super.initState();
//   }

//   @override
//   void dispose() {
//     _controller.dispose();
//     super.dispose();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       margin: EdgeInsets.only(top: 100),
//       padding: EdgeInsets.only(top: spacingMd),
//       decoration: BoxDecoration(
//         borderRadius: BorderRadius.circular(spacingMd),
//         color: Colors.white,
//       ),
//       child: Material(
//         child: Column(
//           children: [
//             Expanded(
//               child: Timetable<Appointment>(
//                 leadingHeaderBuilder: (context, date) {
//                   return Center(
//                     child: Text(
//                       '',
//                     ),
//                   );
//                 },
//                 dateHeaderBuilder: (context, date) {
//                   return Column(
//                     children: [
//                       Row(
//                         children: [
//                           Spacer(),
//                           Container(
//                             alignment: Alignment.center,
//                             child: Text(
//                               'Select a Time Slot',
//                               style: Style.labelTitleDialog,
//                             ),
//                           ),
//                           Spacer(),
//                           InkWell(
//                             onTap: () {
//                               Navigator.pop(context);
//                             },
//                             child: Container(
//                                 margin: EdgeInsets.only(right: spacingMd),
//                                 alignment: Alignment.centerRight,
//                                 child: Icon(
//                                   Icons.close_outlined,
//                                 )),
//                           )
//                         ],
//                       ),
//                       Container(
//                         margin: EdgeInsets.only(top: spacingMd),
//                         child: Text(
//                           '$date',
//                           textAlign: TextAlign.center,
//                           style: TextStyle(
//                             color: colorPrimary,
//                             fontSize: spacingMd,
//                           ),
//                         ),
//                       ),
//                     ],
//                   );
//                 },
//                 onEventBackgroundTap: (start, isAllDay) {
//                   setState(() {
//                     _date = start
//                         .toDateTimeLocal()
//                         .subtract(new Duration(minutes: start.minuteOfHour));
//                     _controller = TimetableController<Appointment>(
//                       eventProvider: EventProvider.list(
//                         [
//                           Appointment(
//                             id: '1',
//                             subject: "dasdas",
//                             color: colorPrimary,
//                             capacity: 1,
//                             startTime: _date,
//                             endTime: _date.add(
//                               Duration(hours: 1),
//                             ),
//                             description: "dasdsa",
//                             isAllDay: false,
//                           ),
//                         ],
//                       ),
//                       initialDate: LocalDate.today(),
//                       visibleRange: VisibleRange.days(1),
//                       initialTimeRange: InitialTimeRange.range(
//                         startTime: LocalTime(8, 0, 0),
//                         endTime: LocalTime(18, 0, 0),
//                       ),
//                       firstDayOfWeek: DayOfWeek.monday,
//                     );
//                   });
//                 },
//                 controller: _controller,
//                 eventBuilder: (event) {
//                   List<String> listDurations = listDuration();
//                   Appointment _event = event;
//                   DateTime dateTime = event.startTime;
//                   return Material(
//                     shape: RoundedRectangleBorder(
//                       side: BorderSide(
//                         color: _event.color,
//                         width: 0.75,
//                       ),
//                       borderRadius: BorderRadius.circular(4),
//                     ),
//                     color: _event.color,
//                     child: Container(
//                       padding: EdgeInsets.fromLTRB(4, 2, 4, 0),
//                       child: Row(
//                         mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                         crossAxisAlignment: CrossAxisAlignment.stretch,
//                         children: [
//                           Expanded(
//                             child: InkWell(
//                               onTap: () async {
//                                 var list = listStart(_event.startTime);
//                                 await showDialog(
//                                   context: context,
//                                   builder: (BuildContext context) {
//                                     return TimeAroundTime(
//                                       event: event,
//                                       dateTime: dateTime,
//                                       list: list,
//                                     );
//                                   },
//                                 );
//                                 setState(() {
//                                   _date = event.startTime;
//                                   _controller =
//                                       TimetableController<Appointment>(
//                                     eventProvider: EventProvider.list(
//                                       [
//                                         Appointment(
//                                           id: '1',
//                                           subject: "dasdas",
//                                           color: colorPrimary,
//                                           capacity: 1,
//                                           startTime: _date,
//                                           endTime: _date.add(
//                                             Duration(hours: 1),
//                                           ),
//                                           description: "dasdsa",
//                                           isAllDay: false,
//                                         ),
//                                       ],
//                                     ),
//                                     initialDate: LocalDate.today(),
//                                     visibleRange: VisibleRange.days(1),
//                                     initialTimeRange: InitialTimeRange.range(
//                                       startTime: LocalTime(8, 0, 0),
//                                       endTime: LocalTime(18, 0, 0),
//                                     ),
//                                     firstDayOfWeek: DayOfWeek.monday,
//                                   );
//                                 });
//                               },
//                               child: Container(
//                                 height: 36,
//                                 decoration: BoxDecoration(
//                                   color: Colors.white,
//                                   borderRadius: BorderRadius.circular(5),
//                                 ),
//                                 margin: EdgeInsets.symmetric(
//                                     horizontal: spacingXs,
//                                     vertical: spacingXxs),
//                                 padding: EdgeInsets.symmetric(
//                                     horizontal: spacingSm, vertical: spacingXs),
//                                 child: Row(
//                                   mainAxisAlignment:
//                                       MainAxisAlignment.spaceBetween,
//                                   children: [
//                                     Column(
//                                       mainAxisAlignment:
//                                           MainAxisAlignment.spaceAround,
//                                       children: [
//                                         Text(
//                                           'Start at',
//                                           style: TextStyle(
//                                               fontSize: 10,
//                                               color: colorTextSecondary),
//                                         ),
//                                         Text(
//                                           '${f.format(dateTime)}',
//                                           style: textStyleSecondaryTabBar,
//                                         ),
//                                       ],
//                                     ),
//                                     SvgPicture.asset(
//                                       'assets/icons/ic_alarm_clock.svg',
//                                       color: colorPrimary,
//                                       fit: BoxFit.none,
//                                     ),
//                                   ],
//                                 ),
//                               ),
//                             ),
//                           ),
//                           Expanded(
//                             child: InkWell(
//                               onTap: () {
//                                 setState(() {
//                                   showDialog(
//                                     context: context,
//                                     builder: (BuildContext context) {
//                                       return AlertDialog(
//                                         title: Text('Duration'),
//                                         content: Container(
//                                           width: double.maxFinite,
//                                           child: ListView.builder(
//                                             shrinkWrap: true,
//                                             itemCount: listDurations.length,
//                                             itemBuilder: (context, index) {
//                                               return ListTile(
//                                                 onTap: () {
//                                                   setState(
//                                                     () {
//                                                       Navigator.pop(context);
//                                                       duration = index;
//                                                       print(duration);
//                                                     },
//                                                   );
//                                                 },
//                                                 title: Text(
//                                                     '${listDurations[index]}'),
//                                               );
//                                             },
//                                           ),
//                                         ),
//                                       );
//                                     },
//                                   );
//                                 });
//                               },
//                               child: Container(
//                                 decoration: BoxDecoration(
//                                   color: Colors.white,
//                                   borderRadius: BorderRadius.circular(5),
//                                 ),
//                                 margin: EdgeInsets.symmetric(
//                                     horizontal: spacingXs,
//                                     vertical: spacingXxs),
//                                 padding: EdgeInsets.symmetric(
//                                     horizontal: spacingSm, vertical: spacingXs),
//                                 child: Row(
//                                   mainAxisAlignment:
//                                       MainAxisAlignment.spaceBetween,
//                                   children: [
//                                     Column(
//                                       mainAxisAlignment:
//                                           MainAxisAlignment.spaceAround,
//                                       children: [
//                                         Text(
//                                           'Duration',
//                                           style: TextStyle(
//                                               fontSize: 10,
//                                               color: colorTextSecondary),
//                                         ),
//                                         Text(
//                                           '${listDurations[duration]}',
//                                           style: textStyleSecondaryTabBar,
//                                         ),
//                                       ],
//                                     ),
//                                     SvgPicture.asset(
//                                       'assets/icons/ic_duration.svg',
//                                       fit: BoxFit.none,
//                                     ),
//                                   ],
//                                 ),
//                               ),
//                             ),
//                           ),
//                         ],
//                       ),
//                     ),
//                   );
//                 },
//               ),
//             ),
//             Container(
//               margin: EdgeInsets.only(
//                 top: spacingMd,
//                 bottom: spacingLarge,
//                 left: spacingMd,
//                 right: spacingMd,
//               ),
//               child: ButtonWidget(
//                 onPress: () {
//                   TimeSlot timeSlot = new TimeSlot();
//                   timeSlot.startAt = _date;
//                   timeSlot.durationInMinute = duration;
//                   onPressed(timeSlot);
//                 },
//                 textButton: 'Confirm',
//                 colorButton: colorPrimary,
//               ),
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }

// List<String> listStart(DateTime dateTime) {
//   List<String> list = [];
//   dateTime = dateTime.subtract(new Duration(minutes: dateTime.minute));
//   for (int i = 0; i < 4; ++i) {
//     var date = dateTime.add(new Duration(minutes: i * 15));
//     list.insert(i, '${f.format(date)}');
//   }
//   return list;
// }

// List<String> listDuration() {
//   List<String> list = [];
//   for (int i = 1; i < 25; ++i) {
//     list.add('${i * 5} mins');
//   }
//   return list;
// }

// class TimeAroundTime extends StatefulWidget {
//   final List<String> list;
//   Appointment event;
//   DateTime dateTime;
//   TimetableController<Appointment> controller;
//   TimeAroundTime({
//     Key key,
//     this.event,
//     this.list,
//     this.dateTime,
//   });
//   @override
//   _TimeAroundTimeState createState() => _TimeAroundTimeState(
//         event: event,
//         list: list,
//         dateTime: dateTime,
//       );
// }

// class _TimeAroundTimeState extends State<TimeAroundTime> {
//   final List<String> list;
//   Appointment event;
//   DateTime dateTime;
//   TimetableController<Appointment> controller;
//   _TimeAroundTimeState({
//     Key key,
//     this.event,
//     this.list,
//     this.dateTime,
//   });
//   @override
//   Widget build(BuildContext context) {
//     return AlertDialog(
//       title: Text('Start Time'),
//       content: Container(
//         width: double.maxFinite,
//         child: ListView.builder(
//           shrinkWrap: true,
//           itemCount: list.length,
//           itemBuilder: (context, index) {
//             return ListTile(
//               onTap: () {
//                 setState(
//                   () {
//                     Navigator.pop(context);
//                     event.startTime = dateTime = dateTime
//                         .subtract(new Duration(minutes: event.startTime.minute))
//                         .add(new Duration(minutes: index * 15));
//                   },
//                 );
//               },
//               title: Text('${list[index]}'),
//             );
//           },
//         ),
//       ),
//     );
//   }
// }

import 'package:flutter/material.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/widget/text_widget.dart';

class TextFormWidget extends StatelessWidget {
  TextFormWidget(
    this.label,
    this.controller,
    this.hinText,
    this.textStyle, [
    this.obscureText = false,
    this.textInputAction = TextInputAction.next,
    this.keyboardType,
    this.warningText,
    this.showWarningText,
  ]);

  final bool showWarningText;
  final String warningText;
  final TextInputType keyboardType;
  final String label;
  final TextEditingController controller;
  final String hinText;
  final TextStyle textStyle;
  final bool obscureText;
  final TextInputAction textInputAction;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: edgeHorizontalInsetsPrimary,
      child: Column(
        children: [
          Row(
            children: [
              Text(
                label,
                style: textStylePrimary,
              ),
            ],
          ),
          Container(
            margin: edgeTopInsetsSecondary,
            child: TextWidget(
              controller: controller,
              hinText: hinText,
              textStyle: textStyle,
              obscureText: obscureText,
              keyboardType: keyboardType,
              textInputAction: textInputAction,
            ),
          ),
          Container(
            alignment: Alignment.centerLeft,
            margin: const EdgeInsets.only(top: 5),
            child: Visibility(
              visible: showWarningText ?? false,
              child: Flexible(
                child: Text(
                  warningText ?? '',
                  style: textStylePrimaryRed,
                  maxLines: 2,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

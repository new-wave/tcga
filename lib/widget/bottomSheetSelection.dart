import 'package:flutter/material.dart';

class BottomSheetSelection extends StatelessWidget {
  final GestureTapCallback pressEdit;
  final GestureTapCallback pressAddNote;
  final GestureTapCallback pressDelete;

  const BottomSheetSelection({
    Key key,
    this.pressEdit,
    this.pressAddNote,
    this.pressDelete,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 180,
      padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
      child: Column(
        children: <Widget>[
          GestureDetector(
            onTap: pressEdit,
            child: Container(
              height: 30,
              margin: EdgeInsets.symmetric(vertical: 8),
              child: Row(
                children: <Widget>[
                  Icon(
                    Icons.edit,
                    size: 20,
                  ),
                  SizedBox(
                    width: 16,
                  ),
                  Text("Edit"),
                ],
              ),
            ),
          ),
          GestureDetector(
            onTap: pressAddNote,
            child: Container(
              margin: EdgeInsets.symmetric(vertical: 8),
              height: 30,
              child: Row(
                children: <Widget>[
                  Icon(
                    Icons.note,
                    size: 20,
                  ),
                  SizedBox(
                    width: 16,
                  ),
                  Text("Add note"),
                ],
              ),
            ),
          ),
          GestureDetector(
            onTap: pressDelete,
            child: Container(
              margin: EdgeInsets.symmetric(vertical: 8),
              height: 30,
              child: Row(
                children: <Widget>[
                  Icon(
                    Icons.delete,
                    size: 20,
                  ),
                  SizedBox(
                    width: 16,
                  ),
                  Text("Delete"),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/database/models/enums.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import '../utils/string_extensions.dart';

enum TitlePosition {
  hint,
  leading,
}

class EntryWidget extends StatefulWidget {
  final double height;
  final TextEditingController controller;
  final String title;
  final TextStyle titleStyle;
  final String hint;
  final TextStyle hintStyle;
  final TextStyle textStyle;
  final Widget suffixIcon;
  final Widget prefixIcon;
  final Color fillColor;
  final TextAlign textAlign;
  final bool readOnly;
  final Function(String) onTextChanged;
  final TitlePosition titlePosition;
  final TextInputType keyboardType;
  final int maxLength;
  final bool hideText;
  final TextInputAction textInputAction;
  final Function() onEditingComplete;
  final bool isCheckField;
  final bool isRequired;
  String warningText;
  final String label;
  final bool hideLabel;
  final Function(String) onSave;
  final Function() onTap;
  final EntryWidgetType entryWidgetType;
  List<TextInputFormatter> formatters;
  final FocusNode focusNode;

  bool get titleAsHint => titlePosition != TitlePosition.leading;

  EntryWidget({
    Key key,
    this.focusNode,
    this.height = 36,
    this.hideText = false,
    this.controller,
    this.title,
    this.onTap,
    this.prefixIcon,
    this.suffixIcon,
    this.fillColor = colorBackground,
    this.textAlign = TextAlign.left,
    this.titleStyle = const TextStyle(
      fontSize: 14,
    ),
    this.textStyle = const TextStyle(
      fontSize: 14,
    ),
    this.readOnly = false,
    this.onTextChanged,
    this.titlePosition = TitlePosition.hint,
    this.hint,
    this.hintStyle,
    this.keyboardType = TextInputType.text,
    this.maxLength,
    this.textInputAction = TextInputAction.next,
    this.onEditingComplete,
    this.isCheckField = false,
    this.isRequired = false,
    this.entryWidgetType = EntryWidgetType.Normal,
    this.warningText = '',
    this.label,
    this.hideLabel,
    this.onSave,
    this.formatters,
  }) : super(key: key);

  @override
  _EntryWidgetState createState() => _EntryWidgetState();
}

class _EntryWidgetState extends State<EntryWidget> {
  bool isValid;
  String warningText = '';
  bool isShowWarningText = false;

  @override
  void initState() {
    isValid = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.entryWidgetType == EntryWidgetType.Normal) {
      isValid = widget.controller?.text?.isEmptyOrWhitespace ?? false;
      isShowWarningText = (widget.isCheckField && widget.isRequired && isValid);
    } else if (widget.entryWidgetType == EntryWidgetType.Email) {
      if (widget.isCheckField) {
        isValid = false;
        bool isEmpty = widget.controller?.text?.isEmptyOrWhitespace ?? true;

        /// if Email is not empty
        if (isEmpty == false) {
          isValid = EmailValidator.validate(widget.controller.text);

          /// if Email is not valid
          if (!isValid) {
            widget.warningText = AppLocalizations.instance
                .text('common.validator.email_address_is_not_valid');
            isShowWarningText = true;
          }

          /// if Email is valid
          else {
            isShowWarningText = widget.warningText ==
                AppLocalizations.instance
                    .text('common.warning.email_has_already');
          }
        }

        /// if Email is Empty
        else {
          if (widget.isRequired) {
            isShowWarningText = true;
            widget.warningText = AppLocalizations.instance
                .text('common.validator.email_address');
          } else {
            isShowWarningText = false;
            widget.warningText = '';
          }
        }
      } else {
        isShowWarningText = false;
        widget.warningText = '';
      }
    } else if (widget.entryWidgetType == EntryWidgetType.Password) {
      if (widget.isCheckField && widget.isRequired) {
        bool isEmpty = widget.controller?.text?.isEmptyOrWhitespace ?? true;

        /// if Password is not empty
        if (isEmpty == false) {
          /// if Password is not Strong
          if (!widget.controller.text.isStrongPassword) {
            widget.warningText = AppLocalizations.instance
                .text('sign_up.your_password_not_strong');
            isShowWarningText = true;
          }

          /// if Password is Strong
          else {
            isShowWarningText = widget.warningText ==
                AppLocalizations.instance.text('sign_up.warning_cfpassword');
          }
        }

        /// if Password is empty
        else {
          isShowWarningText = true;
          widget.warningText =
              AppLocalizations.instance.text('sign_up.fill_your_password');
        }
      } else {
        isShowWarningText = false;
        widget.warningText = '';
      }
    }
    return _buildTextField(widget.readOnly);
  }

  @protected
  Column _buildTextField([
    bool readOnly = false,
  ]) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        Visibility(
          visible: widget.hideLabel ?? false,
          child: Container(
            margin: EdgeInsets.only(bottom: 5),
            alignment: Alignment.centerLeft,
            child: Text(
              widget.label ?? '',
              style: textStylePrimary,
            ),
          ),
        ),
        SizedBox(
          height: widget.height,
          child: Center(
            child: TextFormField(
              focusNode: widget.focusNode,
              readOnly: readOnly,
              obscureText: widget.hideText ?? false,
              controller: widget.controller,
              textAlign: widget.textAlign,
              style: widget.textStyle,
              inputFormatters: widget.formatters,
              onTap: () {
                if (widget.onTap != null) {
                  widget.onTap();
                }
              },
              onFieldSubmitted: (text) {
                if (widget.onSave != null) widget.onSave(text);
              },
              onChanged: (text) {
                setState(() {});
                if (widget.onTextChanged != null) widget.onTextChanged(text);
              },
              textInputAction: widget.textInputAction,
              onEditingComplete: widget.onEditingComplete,
              keyboardType: widget.keyboardType,
              maxLength: widget.maxLength,
              decoration: InputDecoration(
                counterText: '',
                hintText: widget.titleAsHint ? widget.title : widget.hint,
                hintStyle:
                    widget.titleAsHint ? widget.titleStyle : widget.hintStyle,
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5.0),
                  gapPadding: 0,
                  borderSide: BorderSide.none,
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5.0),
                  gapPadding: 0,
                  borderSide: BorderSide.none,
                ),
                contentPadding: const EdgeInsets.only(
                  left: 16,
                  right: 8,
                  top: 8,
                  bottom: 8,
                ),
                filled: true,
                fillColor: widget.fillColor,
                focusColor: widget.fillColor,
                suffixIcon: widget.suffixIcon != null
                    ? Padding(
                        padding: EdgeInsets.all(5),
                        child: SizedBox(
                          height: 36,
                          width: 36,
                          child: Container(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [widget.suffixIcon],
                            ),
                          ),
                        ),
                      )
                    : null,
                prefixIcon: widget.prefixIcon != null
                    ? SizedBox(
                        height: 36,
                        width: 36,
                        child: Container(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [widget.prefixIcon],
                          ),
                        ),
                      )
                    : widget.titleAsHint
                        ? null
                        : SizedBox(
                            height: 36,
                            child: Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 16),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    widget.title,
                                    textAlign: TextAlign.left,
                                    style: widget.titleStyle,
                                  ),
                                ],
                              ),
                            ),
                          ),
              ),
            ),
          ),
        ),
        Visibility(
          visible: isShowWarningText,
          child: Container(
            alignment: Alignment.centerLeft,
            margin: const EdgeInsets.only(top: 5),
            child: Text(
              widget.warningText ?? '',
              style: textStylePrimaryRed,
              maxLines: 2,
            ),
          ),
        ),
      ],
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl_phone_number_input/intl_phone_number_input.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:tcga_flutter/utils/show_picker.dart';
import '../utils/string_extensions.dart';
import 'entry_widget.dart';

class PhonePickerWidget extends StatefulWidget {
  final GestureTapCallback tap;
  final TextEditingController controller;

  PhoneNumber phoneNumberInitial;
  final Function(PhoneNumber) onChangePhoneNumber;
  final Function(bool) onInputValidated;
  final String warningText;

  final bool isCheckField;
  final bool isRequired;

  PhonePickerWidget({
    Key key,
    this.tap,
    this.phoneNumberInitial,
    this.onInputValidated,
    this.onChangePhoneNumber,
    this.controller,
    this.warningText,
    this.isCheckField = false,
    this.isRequired = false,
  }) : super(key: key);

  @override
  _PhonePickerWidgetState createState() => _PhonePickerWidgetState();
}

class _PhonePickerWidgetState extends State<PhonePickerWidget> {
  TextEditingController controller;
  int selectedIndex;
  bool isShowWarningText;
  PhoneNumber phoneNumber;
  bool isPhoneValid = false;

  @override
  void initState() {
    super.initState();
    isShowWarningText = false;
    if (widget.phoneNumberInitial == null) {
      widget.phoneNumberInitial = PhoneNumber(isoCode: 'US');
    }
    phoneNumber = widget.phoneNumberInitial;
    controller = widget.controller ?? TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    isShowWarningText = controller.text.isEmptyOrWhitespace;

    return Column(
      children: [
        Container(
          height: 48,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5),
            color: colorBackground,
          ),
          padding: EdgeInsets.symmetric(
            horizontal: 16.0,
          ),
          child: InternationalPhoneNumberInput(
            onInputChanged: (PhoneNumber number) {
              phoneNumber = number;
              // if (widget.onChangePhoneNumber != null) {
              //   widget.onChangePhoneNumber(number);
              // }
              print(number.phoneNumber);
            },
            onInputValidated: (bool isValid) {
              isPhoneValid = isValid;
              setState(() {
              });
              // if (widget.onInputValidated != null) {
              //   widget.onInputValidated(isValid);
              // }
              print(isValid);
            },
            selectorConfig: SelectorConfig(
              selectorType: PhoneInputSelectorType.DIALOG,
            ),
            ignoreBlank: false,
            autoValidateMode: AutovalidateMode.disabled,
            selectorTextStyle: TextStyle(color: Colors.black),
            initialValue: phoneNumber,
            textFieldController: widget.controller,
            formatInput: false,
            keyboardType:
                TextInputType.numberWithOptions(signed: true, decimal: true),
            //     inputBorder: OutlineInputBorder(),
            inputDecoration: InputDecoration(
              counterText: '',
              hintText: widget.isRequired
                  ? AppLocalizations.of(context).text('sign_up.phone_number')
                  : AppLocalizations.of(context)
                      .text('general_information.placeholder_phone_number'),
              hintStyle: TextStyle(
                fontSize: 14,
              ),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(5.0),
                gapPadding: 0,
                borderSide: BorderSide.none,
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(5.0),
                gapPadding: 0,
                borderSide: BorderSide.none,
              ),
              contentPadding: const EdgeInsets.symmetric(
                horizontal: 16,
                vertical: 8,
              ),
            ),
            onSaved: (PhoneNumber number) {
              print('On Saved: $number');
            },
          ),
        ),
        Visibility(
          visible: (widget.isCheckField && !isPhoneValid),
          child: Container(
            alignment: Alignment.centerLeft,
            margin: const EdgeInsets.only(top: 5),
            child: Text(
              AppLocalizations.instance.text('common.validator.phone_number'),
              style: textStylePrimaryRed,
              maxLines: 2,
            ),
          ),
        ),
      ],
    );
  }

  void getPhoneNumber(String phone) async {
    if (phone != null && phone.isNotEmpty) {
      PhoneNumber number = await PhoneNumber.getRegionInfoFromPhoneNumber(
          phone, phoneNumber.isoCode);
      setState(() {
        this.phoneNumber = number;
      });
    }
  }
}

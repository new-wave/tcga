import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/address_item_component/state.dart';

class AddressPickerWidget extends StatelessWidget {
  AddressPickerWidget({
    Key key,
    this.onPress,
    this.isActive = false,
    this.address,
    this.hint = '',
    this.isPrefixIconVisible = false,
  });

  final GestureTapCallback onPress;
  final bool isActive;
  final AddressItemState address;
  final String hint;
  final bool isPrefixIconVisible;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPress,
      child: Container(
        alignment: Alignment.centerLeft,
        padding: EdgeInsets.only(left: 16.0, top: 8.0, bottom: 8.0, right: 8.0),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(2.0),
          color: colorBackground,
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Expanded(
              child: Text(
                address?.fullAddress ?? hint,
                style: textStylePrimary,
                maxLines: 3,
                overflow: TextOverflow.ellipsis,
              ),
            ),
            Visibility(
              visible: isPrefixIconVisible,
              child: Container(
                child: SvgPicture.asset(
                  isActive
                      ? "assets/icons/ic_address.svg"
                      : "assets/icons/ic_address_inactive.svg",
                  width: 20,
                  height: 20,
                  color: colorPrimary,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

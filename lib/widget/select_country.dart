import 'package:flutter/material.dart';
import 'package:search_choices/search_choices.dart';

enum Department {
  treasury,
  state,
}

Future<void> askedToLead(BuildContext context) async {
  return await showDialog<Department>(
    context: context,
    builder: (BuildContext context) {
      return SimpleDialog(
        children: <Widget>[
          SearchChoices.single(
            items: [
              DropdownMenuItem<String>(
                value: '0',
                child: Text('0'),
              )
            ],
            hint: "Select one",
            searchHint: "Select one",
            onChanged: (value) {
              // setState(() {
              //   selectedValueSingleDialog = value;
              // });
            },
            isExpanded: true,
          ),
        ],
      );
    },
  );
}

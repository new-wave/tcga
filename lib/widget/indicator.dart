
import 'package:flutter/material.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/utils/currency_format.dart';

class Indicator extends StatelessWidget {
  final Color color;
  final String text;
  final bool isSquare;
  final double size;
  final Color textColor;
  final double subValue;

  const Indicator({
    Key key,
    this.color,
    this.text,
    this.isSquare = false,
    this.size = 16,
    this.textColor = const Color(0xff505050),
    this.subValue,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Container(
                width: size,
                height: size,
                decoration: BoxDecoration(
                  shape: isSquare ? BoxShape.rectangle : BoxShape.circle,
                  color: color,
                ),
              ),
              const SizedBox(
                width: 4,
              ),
              Expanded(
                child: Text(
                  text,
                  style: TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.w300,
                      color: textColor),
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
              )
            ],
          ),
        ),
        subValue != null
            ? Text(
          convertMoney(subValue),
          style: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.w400,
            color: Palette.subIndicator,
          ),
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
        )
            : SizedBox()
      ],
    );
  }
}

import 'package:flutter/material.dart';
import 'package:tcga_flutter/config/palette.dart';

class SelectButtonWidget extends StatelessWidget {
  SelectButtonWidget(
      {this.buttonText,
      this.buttonColor = Colors.white,
      this.buttonBorderColor,
      this.buttonTextColor,
      this.buttonMargin,
      this.onTap});
  final String buttonText;
  final Color buttonColor;
  final Color buttonBorderColor;
  final Color buttonTextColor;
  final EdgeInsets buttonMargin;
  final Function onTap;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        margin: buttonMargin,
        padding: edgeAllInsetsThirdBigLeftRight,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            border: Border.all(color: buttonBorderColor),
            color: buttonColor ?? colorWhite),
        child: Center(
          child: Text(
            "$buttonText",
            style: TextStyle(
              color: buttonTextColor,
              fontSize: 14,
            ),
          ),
        ),
        height: 36,
      ),
    );
  }
}

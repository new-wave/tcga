import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:tcga_flutter/config/palette.dart';

import 'entry_widget.dart';

class TimePickerWidget extends StatefulWidget {
  static final DateFormat timeFormat = DateFormat('hh:mm a');

  final DateTime initial;

  final Function(TimeOfDay) timeSelected;

  final String title;
  final TextStyle titleStyle;
  final TextStyle textStyle;
  final Widget suffixIcon;
  final Widget prefixIcon;
  final Color fillColor;
  final TextAlign textAlign;
  final bool readOnly;

  TimePickerWidget({
    Key key,
    @required this.initial,
    this.timeSelected,
    this.title,
    this.prefixIcon,
    this.suffixIcon,
    this.fillColor = colorBackground,
    this.textAlign = TextAlign.left,
    this.titleStyle = const TextStyle(
      fontSize: 14,
    ),
    this.textStyle = const TextStyle(
      fontSize: 14,
    ),
    this.readOnly = false,
  }) : super(key: key);

  @override
  _TimePickerWidgetState createState() => _TimePickerWidgetState();
}

class _TimePickerWidgetState extends State<TimePickerWidget> {
  final TextEditingController controller = TextEditingController();

  @override
  void initState() {
    super.initState();

    controller.text = TimePickerWidget.timeFormat.format(
      widget.initial,
    );
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 36,
      child: Stack(
        children: [
          EntryWidget(
            controller: controller,
            fillColor: widget.fillColor,
            prefixIcon: widget.prefixIcon,
            readOnly: widget.readOnly,
            suffixIcon: widget.suffixIcon,
            textAlign: widget.textAlign,
            textStyle: widget.textStyle,
            title: widget.title,
            titleStyle: widget.titleStyle,
          ),
          InkWell(
            onTap: () async {
              if (widget.readOnly) return;

              final selectedTime = await showTimePicker(
                context: context,
                initialTime: TimeOfDay(
                  hour: widget.initial.hour,
                  minute: widget.initial.minute,
                ),
                initialEntryMode: TimePickerEntryMode.input,
              );

              if (selectedTime == null) return;

              controller.text = selectedTime.format(context);

              if (widget.timeSelected != null) {
                widget.timeSelected(selectedTime);
              }
            },
            child: SizedBox.expand(
              child: Container(
                color: Colors.transparent,
              ),
            ),
          )
        ],
      ),
    );
  }
}

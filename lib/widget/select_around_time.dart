// import 'package:flutter/material.dart';
// import 'package:flutter_svg/flutter_svg.dart';
// import 'package:mdi/mdi.dart';
// import 'package:tcga_flutter/config/palette.dart';
// import 'package:tcga_flutter/config/style.dart';
// import 'package:intl/intl.dart';
// import 'package:tcga_flutter/screens/calendar/model/time_zone.dart';
// import 'package:tcga_flutter/widget/select_bottom_sheet.dart';

// class SelectEventTime extends StatefulWidget {
//   bool isAllDay;
//   DateTime startTime;
//   DateTime endTime;
//   TimeZone timeZone;
//   final Function selectAroundTime;
//   final Function selectTimeZone;
//   final Function searchTimeZone;
//   List<TimeZone> listTimeZone;
//   SelectEventTime({
//     Key key,
//     this.isAllDay,
//     this.endTime,
//     this.startTime,
//     this.timeZone,
//     this.selectAroundTime,
//     this.listTimeZone,
//     this.searchTimeZone,
//     this.selectTimeZone,
//   });
//   @override
//   _SelectEventTimeState createState() => _SelectEventTimeState(
//         isAllDay: isAllDay,
//         startTime: startTime,
//         endTime: endTime,
//         timeZone: timeZone,
//         selectAroundTime: selectAroundTime,
//         searchTimeZone: searchTimeZone,
//         listTimeZone: listTimeZone,
//         selectTimeZone: searchTimeZone,
//       );
// }

// class _SelectEventTimeState extends State<SelectEventTime> {
//   bool isAllDay;
//   DateTime startTime;
//   DateTime endTime;
//   TimeZone timeZone;
//   final Function selectAroundTime;
//   final Function searchTimeZone;
//   final Function selectTimeZone;
//   List<TimeZone> listTimeZone;
//   _SelectEventTimeState({
//     Key key,
//     this.isAllDay,
//     this.endTime,
//     this.startTime,
//     this.timeZone,
//     this.selectAroundTime,
//     this.listTimeZone,
//     this.searchTimeZone,
//     this.selectTimeZone,
//   });

//   final DateFormat formatter = DateFormat('MMM dd, yyyy');
//   final DateFormat formatterHours = DateFormat('jm');
//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       decoration: BoxDecoration(
//         color: colorBackground,
//         borderRadius: BorderRadius.circular(5),
//       ),
//       child: Column(
//         children: [
//           Row(
//             mainAxisAlignment: MainAxisAlignment.spaceBetween,
//             crossAxisAlignment: CrossAxisAlignment.start,
//             children: [
//               Container(
//                 padding: EdgeInsets.symmetric(horizontal: spacingMd),
//                 margin: EdgeInsets.only(top: spacingMd),
//                 child: Text(
//                   'All Day Event',
//                   style: Style.labelNormal,
//                 ),
//               ),
//               Container(
//                 margin: EdgeInsets.only(right: spacingSm),
//                 child: Checkbox(
//                   value: widget.isAllDay,
//                   onChanged: (value) {
//                     setState(() {
//                       widget.isAllDay = !widget.isAllDay;
//                     });
//                   },
//                 ),
//               ),
//             ],
//           ),
//           Divider(
//             height: 2,
//             color: colorDisabled,
//           ),
//           InkWell(
//             onTap: () async {
//               var aroundTime = await showBottomSheetSelection(
//                 context,
//                 widget.isAllDay,
//                 widget.startTime,
//                 widget.endTime,
//                 widget.timeZone,
//                 widget.selectTimeZone,
//                 widget.listTimeZone,
//               );
//               selectAroundTime(aroundTime);
//             },
//             child: Column(
//               children: [
//                 Container(
//                   padding: EdgeInsets.symmetric(horizontal: spacingMd),
//                   margin: EdgeInsets.only(top: spacingMd),
//                   child: Row(
//                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                     crossAxisAlignment: CrossAxisAlignment.start,
//                     children: [
//                       Container(
//                         child: Text(
//                           '${formatter.format(widget.startTime)} at ${formatterHours.format(widget.startTime)}',
//                           style: Style.labelNormal,
//                         ),
//                       ),
//                       SvgPicture.asset('assets/icons/ic_date.svg'),
//                     ],
//                   ),
//                 ),
//                 Container(
//                   margin: EdgeInsets.only(top: spacingSm, bottom: spacingSm),
//                   padding: EdgeInsets.only(right: spacingMd),
//                   child: Row(
//                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                     crossAxisAlignment: CrossAxisAlignment.start,
//                     children: [
//                       Container(
//                         padding: EdgeInsets.symmetric(horizontal: spacingMd),
//                         child: Text(
//                           '${formatter.format(widget.endTime)} at ${formatterHours.format(widget.endTime)}',
//                           style: Style.labelNormal,
//                         ),
//                       ),
//                       SvgPicture.asset('assets/icons/ic_date.svg'),
//                     ],
//                   ),
//                 ),
//               ],
//             ),
//           ),
//           Divider(
//             height: 2,
//             color: colorDisabled,
//           ),
//           InkWell(
//             onTap: () {
//               // int timeZone = await showTimeZone(
//               //   context,
//               //   widget.listTimeZone,
//               //   widget.searchTimeZone,
//               //   widget.selectTimeZone,
//               // );
//               searchTimeZone();
//             },
//             child: Container(
//               margin: EdgeInsets.all(spacingMd),
//               child: Row(
//                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                 children: [
//                   Text(
//                     '${widget.timeZone.timeZone}, ${widget.timeZone.utcOffset}',
//                     style: Style.labelNormal,
//                   ),
//                   Icon(
//                     Mdi.earth,
//                     color: colorPrimary,
//                   ),
//                 ],
//               ),
//             ),
//           ),
//         ],
//       ),
//     );
//   }
// }

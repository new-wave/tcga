import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tcga_flutter/config/palette.dart';

class TextWidget extends StatelessWidget {
  TextWidget({
    this.controller,
    this.hinText,
    this.textStyle,
    this.obscureText = false,
    this.keyboardType,
    this.height = 36,
    this.textInputAction = TextInputAction.next,
    this.isAlignCenter = true,
  });
  final TextInputType keyboardType;
  final TextEditingController controller;
  final String hinText;
  final TextStyle textStyle;
  final bool obscureText;
  final TextInputAction textInputAction;
  final double height;
  final bool isAlignCenter;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      alignment: isAlignCenter ? Alignment.center : Alignment.topCenter,
      padding: edgeLeftInsetsPrimary,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        color: colorBackground,
      ),
      child: TextFormField(
        controller: controller,
        textAlignVertical: TextAlignVertical.center,
        style: textStyle,
        textInputAction: textInputAction,
        decoration: InputDecoration(
          border: InputBorder.none,
          focusedBorder: InputBorder.none,
          enabledBorder: InputBorder.none,
          errorBorder: InputBorder.none,
          disabledBorder: InputBorder.none,
          hintText: hinText,
          hintStyle: textStyleHinttext,
        ),
        keyboardType: keyboardType,
        obscureText: obscureText,
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';
import 'package:tcga_flutter/config/palette.dart';

Future<DateTime> dateTimeController(
    BuildContext context, DateTime dateTime) async {
  DateTime time = await showModalBottomSheet(
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
    context: context,
    builder: (BuildContext build) {
      //CalendarController _controller = CalendarController();
      return Container(
        margin: EdgeInsets.symmetric(horizontal: spacingMd),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(vertical: spacingPrimary),
              child: Center(
                child: Text(
                  "Select date",
                  style: textStyleTitleThird,
                ),
              ),
            ),
            SfCalendar(
              showDatePickerButton: true,
              view: CalendarView.month,
              onTap: (calendarTapDetails) {
                // Navigator.of(context).pop(
                //   calendarTapDetails.date,
                // );
              },
            ),
            // TableCalendar(
            //   availableCalendarFormats: const {
            //     CalendarFormat.month: 'Month',
            //   },
            //   weekendDays: [],
            //   startingDayOfWeek: StartingDayOfWeek.monday,
            //   headerStyle: HeaderStyle(
            //     formatButtonVisible: false,
            //     centerHeaderTitle: true,
            //   ),
            //   initialCalendarFormat: CalendarFormat.month,
            //   calendarController: _controller,
            //   onDaySelected: (day, events, holidays) {
            //     Navigator.of(context).pop(day);
            //   },
            // ),
          ],
        ),
      );
    },
  );
  return time;
}

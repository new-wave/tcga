import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/models/chart_model.dart';
import 'package:tcga_flutter/utils/enum_to_string.dart';

class HorizontalBarChart extends StatelessWidget {
  final List<ChartModel> data;

  final bool animate;

  HorizontalBarChart(
    this.data, {
    this.animate,
  });

  @override
  Widget build(BuildContext context) {
    // For horizontal bar charts, set the [vertical] flag to false.
    return new charts.BarChart(
      convertChartData(data),
      animate: animate,
      vertical: false,
      domainAxis: new charts.OrdinalAxisSpec(
          // Make sure that we draw the domain axis line.
          showAxisLine: true,
          // But don't draw anything else.
          renderSpec: new charts.NoneRenderSpec()),
    );
  }
}

List<charts.Series<ChartModel, String>> convertChartData(
    List<ChartModel> _data) {
  var a = [
    new charts.Series<ChartModel, String>(
      id: 'Sales',
      domainFn: (ChartModel c, _) => getEnumValue(c.category),
      measureFn: (ChartModel c, _) => c.value,
      colorFn: (ChartModel c, index) =>
          charts.Color.fromHex(code: chartPalette[index].toHex()),
      data: _data,
    ),
  ];

  return a;
}

extension HexColor on Color {
  /// Prefixes a hash sign if [leadingHashSign] is set to `true` (default is `true`).
  String toHex({bool leadingHashSign = true}) {
    String value = '${leadingHashSign ? '#' : ''}'
        '${alpha.toRadixString(16).padLeft(2, '0')}'
        '${red.toRadixString(16).padLeft(2, '0')}'
        '${green.toRadixString(16).padLeft(2, '0')}'
        '${blue.toRadixString(16).padLeft(2, '0')}';
    return value.replaceFirst('ff', '');
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/screens/global_widgets/phone_number/phone_item_component/state.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';

import '../route_name.dart';

class PhoneInputWidget extends StatefulWidget {
  final GestureTapCallback tap;
  final double height;

  final TextEditingController controller;
  final String warningText;

  final bool isCheckField;
  final bool isRequired;
  final bool isShowWarningText;

  PhoneItemState countryCode;

  PhoneInputWidget({
    Key key,
    this.tap,
    this.controller,
    this.height = 36,
    this.warningText,
    this.isCheckField = false,
    this.isRequired = false,
    this.isShowWarningText,
    this.countryCode,
  }) : super(key: key);

  @override
  _PhoneInputWidgetState createState() => _PhoneInputWidgetState();
}

class _PhoneInputWidgetState extends State<PhoneInputWidget> {
  bool showWarningText;
  PhoneItemState countryCode;

  @override
  void initState() {
    super.initState();
    showWarningText = false;
    countryCode = widget.countryCode;
  }

  @override
  Widget build(BuildContext context) {
    // showWarningText =
    //     widget.controller.text.length != countryCode?.mask?.length;
    return Column(
      children: [
        Container(
          height: 36,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5),
            color: colorBackground,
          ),
          padding: EdgeInsets.symmetric(
            horizontal: 16.0,
          ),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              InkWell(
                onTap: () async {
                  final item = await Navigator.of(this.context).pushNamed(
                    RouteName.phonePicker,
                    arguments: {
                      'selected-item': countryCode,
                    },
                  );
                  if (item == null) return;
                  countryCode = item;
                  setState(() {});
                },
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(
                      countryCode?.flag ?? '    ',
                      style: textStyleTitleSecondary,
                    ),
                    SizedBox(
                      width: 8.0,
                    ),
                    SvgPicture.asset(
                      'assets/icons/ic_down.svg',
                    ),
                    SizedBox(
                      width: 8.0,
                    ),
                    Text(
                      countryCode?.dialCode ?? '  ',
                      style: textStylePrimary,
                    ),
                  ],
                ),
              ),
              Expanded(
                child: TextField(
                  inputFormatters: [
                    MaskTextInputFormatter(
                      mask: widget.countryCode?.mask != null
                          ? widget.countryCode?.mask[0]
                          : '' ?? '',
                      filter: {"#": RegExp(r'[0-9]')},
                    ),
                  ],
                  onChanged: (text) {
                  },
                  decoration: InputDecoration(
                    counterText: '',
                    hintText: widget.isRequired
                        ? AppLocalizations.of(context).text('sign_up.phone_number')
                        : AppLocalizations.of(context)
                        .text('general_information.placeholder_phone_number'),
                    hintStyle: TextStyle(
                      fontSize: 14,
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5.0),
                      gapPadding: 0,
                      borderSide: BorderSide.none,
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5.0),
                      gapPadding: 0,
                      borderSide: BorderSide.none,
                    ),
                    contentPadding: const EdgeInsets.symmetric(
                      horizontal: 16,
                      vertical: 8,
                    ),
                  ),
                  keyboardType: TextInputType.number,
                  controller: widget.controller,
                ),
              ),
            ],
          ),
        ),
        Visibility(
          visible:
              (widget.isRequired && widget.isCheckField && widget.isShowWarningText),
          child: Container(
            alignment: Alignment.centerLeft,
            margin: const EdgeInsets.only(top: 5),
            child: Text(
              AppLocalizations.instance
                  .text('common.validator.phone_number_is_not_valid'),
              style: textStylePrimaryRed,
              maxLines: 2,
            ),
          ),
        ),
      ],
    );
  }
}

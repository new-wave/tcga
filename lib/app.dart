import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:tcga_flutter/routes.dart';
import 'package:tcga_flutter/utils/app_localizations.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'application.dart';
import 'config/palette.dart';
import 'route_name.dart';

String apiKey = "AIzaSyDiiId6f7f-6VbTN6hezEinKoWivtcni-g";
int pageSize = 100;

class TCGAApp extends StatefulWidget {
  TCGAApp({Key key}) : super(key: key);

  @override
  _TCGAAppState createState() => _TCGAAppState();
}

class _TCGAAppState extends State<TCGAApp> {
  AppLocalizationsDelegate _newLocaleDelegate;

  @override
  void initState() {
    super.initState();
    _newLocaleDelegate = AppLocalizationsDelegate(newLocale: null);
  }

  @override
  Widget build(BuildContext context) {
    EasyLoading.instance..loadingStyle = EasyLoadingStyle.light;

    return MaterialApp(
      title: 'TCGA',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        brightness: Brightness.light,
        primaryColorBrightness: Brightness.light,
        primarySwatch: Colors.blue,
        appBarTheme: AppBarTheme(
          elevation: 0.0,
          color: Colors.white,
          brightness: Brightness.light,
          textTheme: TextTheme(headline6: textStyleTitleThird),
        ),
      ),
      localizationsDelegates: [
        _newLocaleDelegate,
        const AppLocalizationsDelegate(),
        //provides localised strings
        GlobalMaterialLocalizations.delegate,
        //provides RTL support
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
        DefaultCupertinoLocalizations.delegate
      ],
      home: routes.buildPage(
            RouteName.signIn,
            null,
          ),
      onGenerateRoute: (RouteSettings settings) {
        return MaterialPageRoute<Object>(builder: (BuildContext context) {
          return routes.buildPage(
            settings.name,
            settings.arguments,
          );
        });
      },
      supportedLocales: Application().supportedLocales(),
      builder: EasyLoading.init(),
    );
  }

  void onLocaleChange(Locale locale) {
    setState(() {
      _newLocaleDelegate = AppLocalizationsDelegate(newLocale: locale);
    });
  }

  @override
  void dispose() {
    super.dispose();
  }
}

import 'package:flutter/material.dart';
import 'package:tcga_flutter/config/palette.dart';

class Style {
  static const TextStyle labelNormal = TextStyle(
    color: colorTextPrimary,
    fontSize: fontSizeNormal,
  );

  static TextStyle labelNormalOpacity = TextStyle(
    color: colorTextPrimary.withOpacity(0.5),
    fontSize: fontSizeNormal,
  );

  static const TextStyle labelNormalBold = TextStyle(
    color: colorTextPrimary,
    fontSize: fontSizeNormal,
    fontWeight: FontWeight.w600,
  );

  static const TextStyle labelSecondary = TextStyle(
    color: colorTextPrimary,
    fontSize: fontSizeSmall,
  );

  static const TextStyle labelHighlight = TextStyle(
    color: colorAccent,
    fontSize: fontSizeNormal,
  );

  static const TextStyle labelTitleItem = TextStyle(
    color: colorTextPrimary,
    fontSize: fontSizeLarge,
  );

  static const TextStyle labelTitleMessage = TextStyle(
    color: colorIndicatorPositive,
    fontSize: fontSizeXLarge,
  );

  static const TextStyle labelTitlePage = TextStyle(
    color: colorSecondary,
    fontSize: fontSizeXXL,
  );

  static const TextStyle labelClear = TextStyle(
    color: colorSecondary,
    fontSize: fontSizeNormal,
  );

  static const TextStyle labelTitleDialog = TextStyle(
    color: colorTextPrimary,
    fontSize: fontSizeXLarge,
  );

  static TextStyle labelTitleDialogBold = TextStyle(
      color: colorTextPrimary,
      fontSize: fontSizeXLarge,
      fontWeight: FontWeight.w700);

  static const TextStyle labelTimeSmall = TextStyle(
    color: colorDisabled,
    fontSize: fontSizeXSmall,
  );

  static const TextStyle labelIconBottomBar = TextStyle(
    fontSize: fontSizeSmall,
    color: Colors.white,
    fontWeight: FontWeight.w700,
  );
}

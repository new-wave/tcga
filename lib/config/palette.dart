import 'package:flutter/material.dart';

const Color colorPrimary = Color(0xFF0B507D);
const Color colorSecondary = Color(0xFF2B86DA);
const Color colorDisabled = Color(0xFFB1B1B1);

const Color colorAccent = Color(0xFFE13B3B);

const Color colorTextPrimary = Color(0xFF323232);
const Color colorTextSecondary = Color(0xFF545454);
const Color colorTextTertiary = Color(0xFF757575);
final Color colorTextOpacityGrey = Color(0xFF757575).withOpacity(.2);

const Color buttonBackgroundColor = Color.fromRGBO(11, 80, 125, 1);
const Color colorIndicatorPositive = Color(0xFF05944F);
const Color colorIndicatorWarning = Color(0xFFF2994A);

const Color colorWhite = Colors.white;
const Color colorBackground = Color(0xFFF4F4F4);
const Color colorDivider = Color(0xFFBDBDBD);

const Color colorTitleSecondary = Color(0xFF4F4F4F);

//define global color on app
class Palette {
  static const Color colorWhite = Color(0xFFFFFFFF);
  static const Color warning = Color(0xFFFFC043);
  static const Color subIndicator = Color(0xFF506883);
}

List<Color> chartPalette = [
  Color(0xFF50E3C2),
  Color(0xFFF5A623),
  Color(0xFF03BD5B),
  Color(0xFFE350B1),
];

const EdgeInsets edgeAllInsetsPrimary = EdgeInsets.all(16);
const EdgeInsets edgeAllInsetsLarge = EdgeInsets.all(24);
const EdgeInsets edgeAllInsetsPrimaryLarge = EdgeInsets.all(20);
const EdgeInsets edgeAllInsetsSecondary = EdgeInsets.all(8);

const EdgeInsets edgeHorizontalInsetsPrimary =
    EdgeInsets.symmetric(horizontal: 16);
const EdgeInsets edgeOnlyBottom = EdgeInsets.only(bottom: 20);
const EdgeInsets edgeOnlySuperBottom = EdgeInsets.only(bottom: 35);

const EdgeInsets edgeVerticalInsetsPrimary = EdgeInsets.symmetric(vertical: 16);
const EdgeInsets edgeTopInsetsPrimary = EdgeInsets.only(top: 16);
const EdgeInsets edgeBottomInsetsPrimary = EdgeInsets.only(bottom: 16);
const EdgeInsets edgeLeftInsetsPrimary = EdgeInsets.only(left: 16);
const EdgeInsets edgeRightInsetsPrimary = EdgeInsets.only(right: 16);

const EdgeInsets edgeHorizontalInsetsSecondary =
    EdgeInsets.symmetric(horizontal: 8);
const EdgeInsets edgeVerticalInsetsSecondary =
    EdgeInsets.symmetric(vertical: 8);
const EdgeInsets edgeTopInsetsSecondary = EdgeInsets.only(top: 8);
const EdgeInsets edgeBottomInsetsSecondary = EdgeInsets.only(bottom: 8);
const EdgeInsets edgeLeftInsetsSecondary = EdgeInsets.only(left: 8);
const EdgeInsets edgeRightInsetsSecondary = EdgeInsets.only(right: 8);

const EdgeInsets edgeTopInsetsThird = EdgeInsets.only(top: 14);
const EdgeInsets edgeAllInsetsThirdBigLeftRight =
    EdgeInsets.only(bottom: 8, top: 8, left: 16, right: 16);
const EdgeInsets edgeAllInsetsThird =
    EdgeInsets.only(top: 3, right: 16, bottom: 16, left: 16);

const EdgeInsets edgeHorizonThird = EdgeInsets.only(
  left: 16,
  right: 8,
);
const EdgeInsets edgeExceptTop =
    EdgeInsets.only(left: 16, bottom: 16, right: 16);
const EdgeInsets edgeExceptTopLarge =
    EdgeInsets.only(left: 20, bottom: 20, right: 20);
const EdgeInsets edgeExceptTopBotLarge = EdgeInsets.only(left: 20, right: 20);
const EdgeInsets edgeExceptRightPrimary =
    EdgeInsets.only(left: 16, top: 16, bottom: 16);

const EdgeInsets edegMiniTopBot = EdgeInsets.only(top: 5, bottom: 5);
const EdgeInsets edgeMiniTop = EdgeInsets.only(top: spacingSSm);
const EdgeInsets edgeMiniBot = EdgeInsets.only(bottom: spacingSSm);

const EdgeInsets edgeOnlyTopBotPrimary = EdgeInsets.only(top: 16, bottom: 16);
const EdgeInsets edgeOnlyTopBotPrimaryLarge =
    EdgeInsets.only(top: 20, bottom: 16);
const EdgeInsets edgeOnlyTopBotPrimaryMini =
    EdgeInsets.only(top: 20, bottom: spacingSSm);
const EdgeInsets edgeOnlyTopBotSecondLarge =
    EdgeInsets.only(top: 16, bottom: 20);

const EdgeInsets edgeInsetsFourth = EdgeInsets.only(top: 30, bottom: 16);
const EdgeInsets edgeInsetsBigTop =
    EdgeInsets.only(top: 24, bottom: 16, right: 16, left: 16);
const EdgeInsets edgeInsetsTopBotSmaller =
    EdgeInsets.only(top: 16, bottom: 16, right: 20, left: 20);
const EdgeInsets edgeInsetsTopBotMini =
    EdgeInsets.only(top: 5, bottom: 5, right: 20, left: 20);

const double spacingPrimary = 16;
const double spacingLarge = 24;
const double spacingSecondary = 8;

const double spacingXl = 32;
const double spacingLg = 24;
const double spacingMd = 16;
const double spacingSm = 8;
const double spacingSSm = 5;
const double spacingXs = 4;
const double spacingXxs = 2;
const double spacingNegative = -10;

const double fontSizeXXL = 24;
const double fontSizeXLarge = 20;
const double fontSizeLarge = 16;
const double fontSizeNormal = 14;
const double fontSizeSmall = 12;
const double fontSizeXSmall = 10;

const double heightButton = 45;

final TextStyle textStyleHinttext =
    TextStyle(color: colorTextPrimary.withOpacity(0.5), fontSize: 14);

final TextStyle textStyleHintTextSmall =
    TextStyle(color: colorTextPrimary.withOpacity(0.5), fontSize: 12);

const TextStyle textStylePrimary =
    TextStyle(color: colorTextPrimary, fontSize: 14);

const TextStyle textStylePrimaryNormal =
    TextStyle(color: colorPrimary, fontSize: 14);

const TextStyle textStylePrimarySmall =
    TextStyle(color: colorPrimary, fontSize: 12);

const TextStyle textStylePrimaryFontWeight = TextStyle(
    color: colorTextPrimary, fontSize: 14, fontWeight: FontWeight.w600);

const TextStyle textStylePrimaryGray =
    TextStyle(color: colorTextTertiary, fontSize: 14);

const TextStyle textStylePrimaryRed =
    TextStyle(color: colorAccent, fontSize: 14);

const TextStyle textStyleSecondary =
    TextStyle(color: colorTextPrimary, fontSize: 12);

const TextStyle textStyleSmall =
    TextStyle(color: colorTextPrimary, fontSize: 13);
const TextStyle textStyleMiniWeight = TextStyle(
    color: colorTextPrimary, fontSize: 12, fontWeight: FontWeight.w600);

const TextStyle textStyleSecondaryWhite =
    TextStyle(color: Colors.white, fontSize: 12);

const TextStyle textStylePrimaryWhite =
    TextStyle(color: Colors.white, fontSize: 14);

const TextStyle textStyleTitlePrimary =
    TextStyle(color: colorSecondary, fontSize: 24);

const TextStyle textStyleTitleSecondary =
    TextStyle(color: colorTitleSecondary, fontSize: 24);

const TextStyle textStyleTitle = TextStyle(
  color: colorTitleSecondary,
  fontWeight: FontWeight.w400,
  fontSize: 20,
);

const TextStyle textStyleSecondaryTabBar = TextStyle(
  color: colorTextSecondary,
  fontSize: 14,
);

const TextStyle textStylePrimaryTabBar = TextStyle(
  color: colorPrimary,
  fontWeight: FontWeight.bold,
);
const TextStyle textStyleTitleThird = TextStyle(
  color: colorTitleSecondary,
  fontSize: 18,
);

const TextStyle textStyleThird = TextStyle(
  color: colorTitleSecondary,
  fontSize: 16,
);

const TextStyle textFieldStyle = TextStyle(
  color: Color.fromRGBO(50, 50, 50, 0.5),
  fontSize: 14,
);

const TextStyle textField = TextStyle(
  color: Color.fromRGBO(50, 50, 50, 1),
  fontSize: 14,
);

import 'package:uuid/uuid.dart';

class EmergencyContact {
  String id;
  final String firstName;
  final String lastName;
  final String address;
  final String phoneNumber;
  final String emailAddress;

  EmergencyContact({
    this.id,
    this.firstName,
    this.lastName,
    this.address,
    this.phoneNumber,
    this.emailAddress,
  }) {
    id ??= Uuid().v4();
  }

  @override
  String toString() {
    // TODO: implement toString
    return 'EmergencyContact: $firstName $lastName $emailAddress $phoneNumber';
  }
}

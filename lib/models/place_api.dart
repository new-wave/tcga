import 'dart:convert';

import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart';
import 'package:tcga_flutter/database/models/maps/direction.dart';
import 'package:tcga_flutter/database/models/maps/distance_matrix.dart';
import 'package:tcga_flutter/database/models/maps/element_matrix.dart';
import 'package:tcga_flutter/database/models/maps/geocode.dart';
import 'package:tcga_flutter/database/models/maps/geocode_result.dart';
import 'package:tcga_flutter/database/models/maps/location.dart';
import 'package:tcga_flutter/database/models/maps/overview_polyline.dart';
import 'package:tcga_flutter/database/models/maps/routes.dart';
import 'package:tcga_flutter/database/models/maps/row_matrix.dart';
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/address_item_component/state.dart';

import '../app.dart';

class Place {
  String streetNumber;
  String street;
  String city;
  String zipCode;

  Place({
    this.streetNumber,
    this.street,
    this.city,
    this.zipCode,
  });

  @override
  String toString() {
    return 'Place(streetNumber: $streetNumber, street: $street, city: $city, zipCode: $zipCode)';
  }
}

class Suggestion {
  final String placeId;
  final String description;

  Suggestion(this.placeId, this.description);

  @override
  String toString() {
    return 'Suggestion(description: $description, placeId: $placeId)';
  }
}

class PlaceApiProvider {
  final client = Client();

  Future<String> getRouteCoordinates(LatLng l1, LatLng l2) async {
    String request =
        "https://maps.googleapis.com/maps/api/directions/json?origin=${l1.latitude},${l1.longitude}&destination=${l2.latitude},${l2.longitude}&key=$apiKey";
    final response = await client.get(request);
    if (response.statusCode == 200) {
      final result = json.decode(response.body);
      Direction direction = Direction.fromJson(result);
      if (direction.status == 'OK') {
        List<Routes> routes = direction.routes;
        if (routes != null && routes.isNotEmpty) {
          OverviewPolyline overviewPolyline = routes.first.overviewPolyline;
          return overviewPolyline?.points ?? '';
        }
      }
    }
    return '';
  }

  Future<ElementMatrix> getDistanceMatrix(
      LatLng fromAddress, LatLng toAddress) async {
    if (fromAddress != null && toAddress != null) {
      final request =
          "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&language=en&origins=${fromAddress.latitude},${fromAddress.longitude}&destinations=${toAddress.latitude},${toAddress.longitude}&key=$apiKey";

      final response = await client.get(request);
      if (response.statusCode == 200) {
        final result = json.decode(response.body);
        DistanceMatrix distanceMatrix = DistanceMatrix.fromJson(result);
        if (distanceMatrix.status == 'OK') {
          List<RowMatrix> rows = distanceMatrix.rows;
          if (rows != null && rows.isNotEmpty) {
            List<ElementMatrix> elements = rows.first.elements;
            if (elements != null && elements.isNotEmpty) {
              return elements.first;
            }
          }
        }
      }
    }
    return null;
  }

  Future<LatLng> getLocationFromAddress(String input) async {
    final request =
        'https://maps.googleapis.com/maps/api/geocode/json?address=$input&language=en&key=$apiKey';
    final response = await client.get(request);

    if (response.statusCode == 200) {
      final result = json.decode(response.body);
      Geocode geocode = Geocode.fromJson(result);
      if (geocode.status == 'OK') {
        List<GeocodeResult> geocodeResults = geocode.results;
        if (geocodeResults != null && geocodeResults.isNotEmpty) {
          GeocodeResult geocodeResult = geocodeResults.first;
          Location location = geocodeResult?.geometry?.location;
          return LatLng(location.lat, location.lng);
        }
      }
      return null;
    }
    return null;
  }

  Future<List<Suggestion>> fetchSuggestions(String input) async {
    List<Suggestion> suggestions = [];
    final request =
        'https://maps.googleapis.com/maps/api/place/autocomplete/json?input=$input&types=address&language=en&key=$apiKey';
    final response = await client.get(request);

    if (response.statusCode == 200) {
      final result = json.decode(response.body);
      if (result['status'] == 'OK') {
        suggestions = result['predictions']
            .map<Suggestion>((p) => Suggestion(p['place_id'], p['description']))
            .toList();
        return suggestions;
      }
      // if (result['status'] == 'ZERO_RESULTS') {
      //   return suggestions;
      // }
      //   throw Exception(result['error_message']);
      return suggestions;
    }
    return suggestions;
    throw Exception('Failed to fetch suggestion');
  }

  Future<AddressItemState> getPlaceDetailFromId(String placeId) async {
    final request =
        'https://maps.googleapis.com/maps/api/place/details/json?place_id=$placeId&language=en&fields=address_component&key=$apiKey';
    final url = Uri.decodeComponent(request);
    final response = await client.get(url);
    if (response.statusCode == 200) {
      final result = json.decode(response.body);
      if (result['status'] == 'OK') {
        final components =
            result['result']['address_components'] as List<dynamic>;
        // build result
        final address = AddressItemState();
        components.forEach((c) {
          final List type = c['types'];
          if (type.contains('route')) {
            address.address = c['long_name'];
          }
          if (type.contains('locality')) {
            address.city = c['long_name'];
          }
          if (type.contains('country')) {
            address.country = c['long_name'];
          }
          if (type.contains('postal_code')) {
            address.zip = c['long_name'];
          }
        });
        return address;
      }
      throw Exception(result['error_message']);
    } else {
      throw Exception('Failed to fetch suggestion');
    }
  }
}

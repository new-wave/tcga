import 'package:charts_flutter/flutter.dart';
import 'package:tcga_flutter/database/models/enums.dart';

class ChartModel {
  double value;
  double percent;
  Color color;
  ExpenseCategory category;
  String currencyCode;

  ChartModel({
    this.value,
    this.percent,
    this.color,
    this.category,
    this.currencyCode,
  });
}

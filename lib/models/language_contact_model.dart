class Language {
  bool selected;
  String language;
  bool primary;

  Language({
    this.selected,
    this.language,
    this.primary,
  });
}
List<Language> listLanguage = [
  Language(selected: false, language: "American English", primary: false),
  Language(selected: false, language: "British English", primary: false),
  Language(selected: false, language: "French", primary: false),
  Language(selected: false, language: "Portugal", primary: false),
];
class RouteName {
  static final String signIn = 'sign-in';
  static final String signUp = 'sign-up';
  static final String editAddress = 'edit-address';
  static final String checkFingerprint = 'check-finger';

  static final String home = 'home';

  static final String calendar = 'calendar';
  static final String appointmentEditor = 'appointment-editor';
  static final String appointmentTimePicker = 'appointment-time-picker';
  static final String appointmentTimeSlotPicker = 'appointment-timeslot-picker';
  static final String makeRecurrencePicker = 'make-recurrence-picker';
  static final String appointmentAlertPicker = 'appointment-alert-picker';
  static final String appointmentSelectionSave = 'appointment-selection-save';

  static final String note = 'note';
  static final String noteFilter = 'note-filter';

  static final String alert = 'alert';

  static final String expenses = 'expenses';
  static final String expensesEditor = 'expenses-editor';

  static final String recipients = 'recipients';
  static final String recipientsEditor = 'recipients/editor';
  static final String recipientsDashboard = 'recipients/dashboard';
  static final String recipientsCondition = 'recipients/condition';
  static final String recipientsMedication = 'recipients/medication';
  static final String recipientsImmunization = 'recipients/immunization';
  static final String recipientsAllergy = 'recipients/allergy';

  static final String notes = 'notes';
  static final String notesEditor = 'notes-editor';

  static final String recipientsConditionEdit = 'recipients/condition/edit';
  static final String recipientsMedicationEdit = 'recipients/medication/edit';
  static final String recipientsImmunizationEdit =
      'recipients/immunization/edit';
  static final String recipientsImmunizationSearch =
      'recipients/immunization/search';
  static final String recipientsAllergyEdit = 'recipients/allergy/edit';

  static final String programDialog = 'program';
  static final String recipientsPicker = 'recipients-picker';
  static final String phonePicker = 'phone-picker';
  static final String currencyPicker = 'currency-picker';

  static final String addressEditor = 'address-editor';
  static final String contactEditor = 'contact-editor';
  static final String programEditor = 'program-editor';

  static final String contacts = 'contacts';
  static final String contactsEditor = 'contacts-editor';
  static final String contactsPicker = 'contacts-picker';

  static final String teams = 'teams';
  static final String teamEditor = 'team-editor';
  static final String teamMemberPicker = 'team-member-picker';

  static final String languagesSelection = 'languages-selection';
  static final String valuesetPicker = 'valueset-picker';

  static final String mileage = 'mileage';
  static final String resultRoute = 'result-route';
  static final String addressPicker = 'address-picker';
  static final String addressContactPicker = 'address-contact-picker';
  static final String addressPickerMaps = 'address-picker-maps';

  static final String tripFilter = 'trip-filter';
  static final String tripDetail = 'trip-detail';
  static final String tripMapDetail = 'trip-map-detail';
  static final String tripCalculate = 'trip-calculate';
  static final String consentForms = 'consent-forms';
}

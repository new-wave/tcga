import 'package:devicelocale/devicelocale.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:tcga_flutter/screens/global_widgets/phone_number/phone_item_component/state.dart';

String formatPhoneNumber(PhoneItemState phone, String number) {
  if (number == null) return null;
  if (number.isEmpty) return null;
  number = number.replaceAll('(', '');
  number = number.replaceAll(')', '');
  number = number.replaceAll('-', '');

  return "${phone?.dialCode ?? ''}-$number";
}

String formatFullPhoneNumber(String number) {
  if (number == null) return null;
  if (number.isEmpty) return null;

  String displayNumber = number.replaceAll('-', '');
  return displayNumber;
}

String formatMaskPhoneNumber(String number, String mask) {
  var maskFormatter = MaskTextInputFormatter(
    mask: mask,
    filter: {"#": RegExp(r'[0-9]')},
  );
  return maskFormatter.maskText(number);
}

Future<String> getCountryCodeFromDevice() async {
  String countryCode = await Devicelocale.currentLocale;
  List<String> items = countryCode.split('-').toList(growable: false);
  return items.length > 1 ? items[1] : 'US';
}

Future<String> getLocaleFromDevice() async {
  String code = await Devicelocale.currentLocale;
  return code;
}
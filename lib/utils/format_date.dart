import 'dart:io';

import 'package:intl/intl.dart';

String dateFormatYMD(DateTime date) {
  return new DateFormat('yMd').format(date);
}

String dateFormatYMMMD(DateTime date) {
  return new DateFormat.yMMMMd('en_US').format(date);
}

String dateFormatYMMMDLocale(DateTime date) {
  String locale = Platform.localeName;
  String dateTime = DateFormat.yMd(locale).format(date);
  List<String> dates = dateTime.split('/').toList();
  if (dates.length == 3) {
    String date1 = dates[0];
    String month = dates[1];
    String year = dates[2];
    int dd = int.parse(date1);
    int mm = int.parse(month);
    if (dd < 10) {
      date1 = "0$dd";
    }
    if (mm < 10) {
      month = "0$mm";
    }
    return "$date1/$month/$year";
  } else {
    return dateTime;
  }
}

String getHintDateFormat() {
  try {
    String pattern = DateFormat.yMd(Platform.localeName).pattern;
    List<String> patterns =
        pattern.split('/').map((e) => e.toLowerCase()).toList();
    if (patterns.length > 1) {
      if (patterns[0].contains('m')) {
        return 'MM/dd/yyyy';
      } else {
        return 'dd/MM/yyyy';
      }
    } else {
      return 'dd/MM/yyyy';
    }
  } catch (error) {
    return 'dd/MM/yyyy';
  }
}

DateFormat dateFormatYMDLocale() {
  String locale = Platform.localeName;
  return new DateFormat.yMd(locale);
}

String dateFormatHM(DateTime date) {
  return new DateFormat('HH:mm a').format(date);
}

String formatDateTimeToString(DateTime date) {
  try {
    if (date != null) {
      return DateFormat('yyyy-MM-dd').format(date);
    } else {
      return "";
    }
  } catch (error) {
    return "";
  }
}

DateTime formatStringToDateTime(String date) {
  try {
    return DateTime.parse(date);
  } catch (error) {
    return null;
  }
}

DateTime formatStringToDateTimeWithFormat(String format, String date) {
  try {
    return DateFormat(format).parse(date);
  } catch (error) {
    return null;
  }
}

String formatToDisplayTime(String date) {
  return formatDateTime(formatStringToDateTime(date));
}

String formatDateTime(DateTime date) {
  if (date != null) {
    List<String> months = [
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "Jun",
      "Jul",
      "Aug",
      "Sep",
      "Oct",
      "Nov",
      "Dec"
    ];
    String day;
    if (date.day == 1) {
      day = "1st";
    } else if (date.day == 1) {
      day = "2nd";
    } else {
      day = "${date.day}th";
    }
    return "$day ${months[date.month - 1]} ${date.year}";
  } else {
    return "";
  }
}

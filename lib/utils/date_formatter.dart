import 'dart:io';

import 'package:flutter/services.dart';
import 'package:tcga_flutter/utils/format_date.dart';

class DateFormatter extends TextInputFormatter {
  String typeDateFormat = getHintDateFormat();

  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue prevText, TextEditingValue currText) {
    int selectionIndex;

    // Get the previous and current input strings
    String pText = prevText.text;
    String cText = currText.text;
    // Abbreviate lengths
    int cLen = cText.length;
    int pLen = pText.length;

    if (cLen == 2 && pLen == 1) {
      // Days cannot be greater than 31
      if (typeDateFormat == 'MM/dd/yyyy') {
        int mm = int.parse(cText.substring(0, 2));
        if (mm > 12) {
          String currentText = cText.substring(1, 2);
          cText = "0";
          cText += pText;
          cText += '/';
          cText += currentText;
        } else if(mm == 0) {
          cText = cText.substring(0, 1);
        } else {
          cText = cText.substring(0, 2);
          cText += '/';
        }
      } else {
        int dd = int.parse(cText.substring(0, 2));
        if (dd > 31) {
          String currentText = cText.substring(1, 2);
          cText = "0";
          cText += pText;
          cText += '/';
          cText += currentText;
        } else if(dd == 0) {
          cText = cText.substring(0, 1);
        } else {
          cText = cText.substring(0, 2);
          cText += '/';
        }
      }
    } else if (cLen == 5 && pLen == 4) {
      if (typeDateFormat == 'MM/dd/yyyy') {
        int dd = int.parse(cText.substring(3, 5));
        int mm = int.parse(cText.substring(0, 2));
        String previousText = cText.substring(3, 4);
        int currentInt = int.parse(cText.substring(4, 5));
        if (mm == 4 || mm == 6 || mm == 9 || mm == 11) {
          if (dd > 30) {
            cText = cText.substring(0, 3);
            cText += '0';
            cText += previousText;
            cText += '/';
            if (currentInt == 1 || currentInt == 2) {
              cText += '$currentInt';
            }
          } else if(dd == 0) {
            cText = cText.substring(0, 4);
          } else {
            cText = cText.substring(0, 5);
          }
        } else if (mm == 2) {
          if (dd > 29) {
            cText = cText.substring(0, 3);
            cText += '0';
            cText += previousText;
            cText += '/';
            if (currentInt == 1 || currentInt == 2) {
              cText += '$currentInt';
            }
          } else if(dd == 0) {
            cText = cText.substring(0, 4);
          } else {
            cText = cText.substring(0, 5);
          }
        } else {
          if (dd > 31) {
            cText = cText.substring(0, 3);
            cText += '0';
            cText += previousText;
            cText += '/';
            if (currentInt == 1 || currentInt == 2) {
              cText += '$currentInt';
            }
          } else if(dd == 0) {
            cText = cText.substring(0, 4);
          }  else {
            cText = cText.substring(0, 5);
          }
        }
      } else {
        int dd = int.parse(cText.substring(0, 2));
        int mm = int.parse(cText.substring(3, 5));
        String previousText = cText.substring(3, 4);
        int currentInt = int.parse(cText.substring(4, 5));
        if (mm > 12) {
          int m = int.parse(cText.substring(3, 4));
          if (dd == 31) {
            if (m == 2 || m == 4 || m == 6 || m == 9) {
              cText = cText.substring(0, 4);
            } else {
              cText = cText.substring(0, 3);
              cText += '0';
              cText += previousText;
              cText += '/';
              if (currentInt == 1 || currentInt == 2) {
                cText += '$currentInt';
              }
            }
          } else if (dd == 30) {
            if (m == 2) {
              cText = cText.substring(0, 4);
            } else {
              cText = cText.substring(0, 3);
              cText += '0';
              cText += previousText;
              cText += '/';
              if (currentInt == 1 || currentInt == 2) {
                cText += '$currentInt';
              }
            }
          } else {
            cText = cText.substring(0, 3);
            cText += '0';
            cText += previousText;
            cText += '/';
            if (currentInt == 1 || currentInt == 2) {
              cText += '$currentInt';
            }
          }
        } else if(mm == 0) {
          cText = cText.substring(0, 4);
        } else {
          if (dd == 31) {
            if (mm == 2 || mm == 4 || mm == 6 || mm == 9 || mm == 11) {
              cText = cText.substring(0, 4);
            } else {
              cText = cText.substring(0, 5);
            }
          } else if (dd == 30) {
            if (mm == 2) {
              cText = cText.substring(0, 4);
            } else {
              cText = cText.substring(0, 5);
            }
          } else {
            cText = cText.substring(0, 5);
          }
        }
      }
    } else if ((cLen == 3 && pLen == 4) || (cLen == 6 && pLen == 7)) {
      // Remove / char
      cText = cText.substring(0, cText.length - 1);
    } else if (cLen == 3 && pLen == 2) {
      if (int.parse(cText.substring(2, 3)) > 1) {
        // Replace char
        cText = cText.substring(0, 2) + '/';
      } else {
        // Insert / char
        cText =
            cText.substring(0, pLen) + '/' + cText.substring(pLen, pLen + 1);
      }
    } else if (cLen == 6 && pLen == 5) {
      // Can only be 1 or 2 - if so insert a / char
      int y1 = int.parse(cText.substring(5, 6));
      if (y1 < 1 || y1 > 2) {
        // Replace char
        cText = cText.substring(0, 5) + '/';
      } else {
        // Insert / char
        cText = cText.substring(0, 5) + '/' + cText.substring(5, 6);
      }
    } else if (cLen == 7) {
      // Can only be 1 or 2
      int y1 = int.parse(cText.substring(6, 7));
      if (y1 < 1 || y1 > 2) {
        // Remove char
        cText = cText.substring(0, 6);
      }
    } else if (cLen == 8) {
      // Can only be 19 or 20
      int y2 = int.parse(cText.substring(6, 8));
      if (y2 < 19 || y2 > 20) {
        // Remove char
        cText = cText.substring(0, 7);
      }
    }

    selectionIndex = cText.length;
    return TextEditingValue(
      text: cText,
      selection: TextSelection.collapsed(offset: selectionIndex),
    );
  }
}

import 'package:flutter/material.dart';

import 'app_localizations.dart';

enum ConfirmTypes { approve, cancel }

Future<dynamic> showErrorDialog(
  BuildContext context, {
  String title,
  String description,
}) async {
  return showDialog<dynamic>(
    context: context,
    builder: (BuildContext buildContext) {
      return AlertDialog(
        title: Text(
          title,
        ),
        content: Text(
          description,
          style: TextStyle(
            fontSize: 14,
          ),
        ),
        actions: <Widget>[
          GestureDetector(
            child: Padding(
              padding: const EdgeInsets.symmetric(
                vertical: 8.0,
                horizontal: 16,
              ),
              child: Text(AppLocalizations.instance.text('common.ok'),
                  style: TextStyle(fontSize: 16.0)),
            ),
            onTap: () => Navigator.of(buildContext).pop(),
          ),
        ],
      );
    },
  );
}

Future<dynamic> showConfirmDialog(
  BuildContext context, {
  bool isPositiveButtonVisible,
  String title,
  String body,
  String positiveAction,
  String negativeAction,
}) async {
  return showDialog<dynamic>(
    context: context,
    barrierDismissible: false, // user must tap button!
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text(
            title ?? AppLocalizations.of(context).text('common.dialog.header')),
        content: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              Text(body ??
                  AppLocalizations.of(context)
                      .text('common.dialog.question_one')),
            ],
          ),
        ),
        actions: <Widget>[
          Visibility(
            visible: isPositiveButtonVisible ?? true,
            child: TextButton(
              child: Text(
                positiveAction ??
                    AppLocalizations.of(context).text('common.dialog.yes'),
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              onPressed: () {
                Navigator.of(context).pop(ConfirmTypes.approve);
              },
            ),
          ),
          TextButton(
            child: Text(
              negativeAction ??
                  AppLocalizations.of(context).text('common.dialog.cancel'),
            ),
            onPressed: () {
              Navigator.of(context).pop(ConfirmTypes.cancel);
            },
          ),
        ],
      );
    },
  );
}

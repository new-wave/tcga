import 'package:email_validator/email_validator.dart';

final _passcodeRegExp = RegExp(r'^[0-9]{6,6}$');
final _whitespaceRegExp = RegExp(r'^\s+$');
final _containingDigitRegExp = RegExp(r'\d+');
final _containingLowercaseRegExp = RegExp(r'[a-z]+');
final _containingUppercaseRegExp = RegExp(r'[A-Z]+');
final _containingNonAlphabetRegExp = RegExp(
  r'[^A-Z0-9 ]+',
  caseSensitive: false,
);

extension StringExtensions on String {
  bool get isEmptyOrWhitespace {
    if (this.isEmpty) return true;

    return _whitespaceRegExp.hasMatch(this);
  }

  bool get isValidEmailAddress {
    return EmailValidator.validate(this);
  }

  bool get isStrongPassword {
    if (this.isEmptyOrWhitespace) return false;

    if (this.length < 6) return false;

    if (!_containingDigitRegExp.hasMatch(this)) return false;

    if (!_containingLowercaseRegExp.hasMatch(this)) return false;

    if (!_containingUppercaseRegExp.hasMatch(this)) return false;

    if (!_containingNonAlphabetRegExp.hasMatch(this)) return false;

    return true;
  }

  bool get isValidPasscode {
    return _passcodeRegExp.hasMatch(this);
  }
}

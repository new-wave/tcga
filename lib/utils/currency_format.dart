import 'package:intl/intl.dart';
import 'package:mdi/mdi.dart';

String convertMoney(double money,
    {String symbol = '\$', int decimalDigits = 2, String locale = 'en_US'}) {
  String moneyFormat =  NumberFormat.currency(
    locale: locale,
    symbol: symbol,
    decimalDigits: decimalDigits,
  ).format(money);
  moneyFormat = moneyFormat.replaceAll('.00', '');
  return moneyFormat;
}

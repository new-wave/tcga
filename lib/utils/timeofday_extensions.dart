import 'package:flutter/material.dart';

extension TimeOfDayExtension on TimeOfDay {
  String getTimeZone() {
    String minute = this.minute < 30 ? '00' : this.minute.toString();
    if (this.hour < 0) {
      String number = this.hour.toString().replaceFirst('-', '');
      return 'UTC -${number.padLeft(2, '0')}:$minute';
    } else if (this.hour > 0) {
      return 'UTC +${this.hour.toString().padLeft(2, '0')}:$minute';
    }
    return 'UTC ${this.hour.toString().padLeft(2, '0')}:$minute';
  }
}

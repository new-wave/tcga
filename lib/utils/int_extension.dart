import 'package:flutter/material.dart';

extension IntExtension on int {
  TimeOfDay toTimeOfDay() {
    return TimeOfDay(hour: this ~/ 60, minute: this % 60);
  }
}

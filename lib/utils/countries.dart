import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:tcga_flutter/database/models/country.dart';
import 'package:tcga_flutter/database/models/service_type.dart';

class CountryUtil {
  Future<List<Country>> convertFromJSON(BuildContext context) async {
    try {
      var countriesJson = await DefaultAssetBundle.of(context)
          .loadString('assets/json/countries.json');
      var counties = json.decode(countriesJson) as List;
      List<Country> data = counties.map((e) => Country.fromJson(e)).toList();
      return data ?? [];
    } catch (e) {
      return [];
    }
  }

  Future<List<Country>> convertServiceTypeFromJSON(BuildContext context) async {
    try {
      var serviceTypesJson = await DefaultAssetBundle.of(context)
          .loadString('assets/json/service_type.json');
      var services = json.decode(serviceTypesJson) as List;
      List<ServiceType> data =
          services.map((e) => ServiceType.fromJson(e)).toList();
      return data ?? [];
    } catch (e) {
      return [];
    }
  }
}

// class TimeZoneUtil {
//   Future<List<TimeZone>> convertFromJSON(BuildContext context) async {
//     var timeZoneJson = await DefaultAssetBundle.of(context)
//         .loadString('assets/json/timezones.json');
//     final data = json.decode(timeZoneJson) as List;
//     final lst = data
//         .map((timeZone) => TimeZone.fromJson(
//               timeZone,
//             ))
//         .toList();

//     return lst ?? [];
//   }
// }

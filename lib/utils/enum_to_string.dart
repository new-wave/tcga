List<String> convertEnumToList(List<dynamic> enumData) {
  return enumData.map((e) => e.toString().split('.').last).toList();
}

String getEnumValue(dynamic enumValue) {
  return enumValue != null ? enumValue.toString().split('.').last : 'Unknown';
}

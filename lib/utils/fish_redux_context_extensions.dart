import 'package:fish_redux/fish_redux.dart';

import 'package:flutter/material.dart';
import 'package:tcga_flutter/database/models/enums.dart';
import 'package:tcga_flutter/screens/valuesets/valueset_picker_page/valueset_item_component/state.dart';
import 'package:tcga_flutter/utils/confirm_dialog.dart';

import '../route_name.dart';

extension ContextExtensions<T> on Context<T> {
  Future<TResult> navigateTo<TResult extends Object>(
    String namedRoute, {
    Map<String, dynamic> arguments,
  }) {
    return Navigator.of(this.context).pushNamed<TResult>(
      namedRoute,
      arguments: arguments ?? Map<String, dynamic>(),
    );
  }

  goBack<TResult extends Object>([
    Object result,
  ]) {
    Navigator.of(this.context).pop(
      result,
    );
  }

  Future<dynamic> pickValuesetItem(
    String type, {
    bool multiSelection = false,
    bool confirmRequired = false,
    List<ValuesetItemState> selectedItems,
  }) {
    final args = <String, dynamic>{
      'type': type,
    };

    if (multiSelection) {
      args['multi-selection'] = multiSelection;
    }
    if (confirmRequired) {
      args['confirm-required'] = confirmRequired;
    }
    if (selectedItems != null) {
      args['selected-items'] = selectedItems;
    }

    return Navigator.of(this.context).pushNamed(
      RouteName.valuesetPicker,
      arguments: args,
    );
  }

  Future<bool> confirm({
    String title,
    String message,
    bool isPositiveButtonVisible,
    String positiveActionTitle,
    String negativeActionTitle,
  }) async {
    var confirmedType = await showConfirmDialog(
      this.context,
      title: title,
      body: message,
      isPositiveButtonVisible: isPositiveButtonVisible,
      positiveAction: positiveActionTitle,
      negativeAction: negativeActionTitle,
    );

    return confirmedType == ConfirmTypes.approve;
  }

  Future<TimeOfDay> pickTime(TimeOfDay timeOfDay) {
    return showTimePicker(
      context: context,
      initialTime: timeOfDay,
    );
  }
}

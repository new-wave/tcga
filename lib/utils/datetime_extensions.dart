import 'package:intl/intl.dart';
import 'package:timezone/timezone.dart';
import 'package:tuple/tuple.dart';

var fullDateTimeFormat = DateFormat('EEE, MMM dd, yyyy hh:mm a');
var longDateTimeFormat = DateFormat('EEEE, d MMMM, yyyy');
var shortDateTimeFormat = DateFormat('MMM dd, yyyy');
var shortTimeFormat = DateFormat('hh:mm a');
var dateTimeString = DateFormat('yyyyMMdd');

extension DateTimeExtensions on DateTime {
  bool between(DateTime fromDate, DateTime toDate) {
    DateTime now = new DateTime.now();
    DateTime dateTime = new DateTime(now.year, now.month, now.day);
    return dateTime.millisecondsSinceEpoch > fromDate.millisecondsSinceEpoch &&
        dateTime.millisecondsSinceEpoch < toDate.millisecondsSinceEpoch;
  }

  bool isDateBetween(DateTime fromDate, DateTime toDate) {
    return this.millisecondsSinceEpoch >= fromDate.millisecondsSinceEpoch &&
        this.millisecondsSinceEpoch <= toDate.millisecondsSinceEpoch;
  }

  int countDateBetween(DateTime fromDate) {
    return this.difference(fromDate).inDays;
  }

  Tuple2<DateTime, DateTime> getWeekRangeOfDay(DateTime dt) {
    int currentDayOfWeek = dt.weekday;
    var lastSunday = dt.add(new Duration(days: -currentDayOfWeek));
    var monday = lastSunday.add(new Duration(days: 1));
    var sunday = monday.add(new Duration(days: 7));
    return new Tuple2<DateTime, DateTime>(monday, sunday);
  }

  bool isInThisWeek() {
    DateTime now = DateTime.now();
    DateTime date = new DateTime(now.year, now.month, now.day);
    var weekRange = getWeekRangeOfDay(date);
    return date.between(weekRange.item1, weekRange.item2);
  }

  bool isInLastWeek() {
    DateTime now = new DateTime.now();
    DateTime date = new DateTime(now.year, now.month, now.day);
    var weekRange = getWeekRangeOfDay(date.subtract(new Duration(days: 7)));
    return date.between(weekRange.item1, weekRange.item2);
  }

  bool isSameDay(DateTime selectedDay) {
    if (this.year == selectedDay.year &&
        this.month == selectedDay.month &&
        this.day == selectedDay.day) {
      return true;
    } else {
      return false;
    }
  }

  DateTime changeDay(DateTime selectedDay) {
    return DateTime(
      selectedDay?.year ?? this.year,
      selectedDay?.month ?? this.month,
      selectedDay?.day ?? this.day,
      this.hour,
      this.minute,
      this.second,
      this.millisecond,
      this.microsecond,
    );
  }

  DateTime addMonth(int month) {
    return DateTime(
      this.year,
      this.month + month,
      this.day,
      this.hour,
      this.minute,
      this.second,
      this.millisecond,
      this.microsecond,
    );
  }

  DateTime addYear(int year) {
    return DateTime(
      this.year + year,
      this.month,
      this.day,
      this.hour,
      this.minute,
      this.second,
      this.millisecond,
      this.microsecond,
    );
  }

  String toFullDateTimeString() {
    return fullDateTimeFormat.format(this);
  }

  String toLongDateTimeString() {
    return longDateTimeFormat.format(this);
  }

  String toShortDateString() {
    return shortDateTimeFormat.format(this);
  }

  String dateTimeToString() {
    return dateTimeString.format(this);
  }

  String toShortTimeString() {
    return shortTimeFormat.format(this);
  }

  DateTime today() {
    return DateTime(this.year, this.month, this.day);
  }

  DateTime getCurrentTimeZone(String timeZone) {
    var dateUTC = this.toUtc(); //co chu Z
    var date = dateUTC.toString().split(new RegExp(r"Z"));

    DateTime datefromUTC = DateTime.parse(date[0]);

    var currentTimeZone = getLocation(timeZone);
    int offsert = currentTimeZone.currentTimeZone.offset ~/ 60000;
    Duration getTimeZoneOffset =
        Duration(hours: offsert ~/ 60, minutes: (offsert % 60).toInt());
    print(datefromUTC.add(getTimeZoneOffset));
    return datefromUTC.add(getTimeZoneOffset);
  }

  DateTime getTimeZone(String timeZone) {
    DateTime date;
    var timeZoneInfo = this.toLocal().timeZoneOffset;
    var currentTimeZone = getLocation(timeZone);
    int offsert = currentTimeZone.currentTimeZone.offset ~/ 60000;
    Duration getTimeZoneOffset =
        Duration(hours: offsert ~/ 60, minutes: (offsert % 60).toInt());

    date = this.subtract(getTimeZoneOffset);

    return date.add(Duration(hours: timeZoneInfo.inHours));
  }

  String timeZoneCode() {
    DateTime now = DateTime.now();
    var duration = now.timeZoneOffset;
    if (duration.isNegative) {
      return "UTC - ${duration.inHours.toString().padLeft(2, '0')}:${(duration.inMinutes - (duration.inHours * 60)).toString().padLeft(2, '0')}";
    }
    return "UTC + ${duration.inHours.toString().padLeft(2, '0')}:${(duration.inMinutes - (duration.inHours * 60)).toString().padLeft(2, '0')}";
  }
}

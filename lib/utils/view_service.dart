import 'package:fish_redux/fish_redux.dart';

import 'app_localizations.dart';

extension ViewServiceExtensions on ViewService {
  static const String placeholderPattern = '(\{\{([a-zA-Z0-9]+)\}\})';

  String localize(String placeholderText, Map<String, dynamic> value) {
    String title = AppLocalizations.of(this.context).text('$placeholderText');
    if (value == null) {
      return title;
    } else {
      var template = "$title: ${{{value.keys.elementAt(0).toString()}}}";
      return sprintf(template, [value.values.elementAt(0)]);
    }
  }

  String sprintf(String template, List replacements) {
    var regExp = RegExp(placeholderPattern);
    assert(regExp.allMatches(template).length == replacements.length,
        "Template and Replacements length are incompatible");

    for (var replacement in replacements) {

      template = template.replaceFirst(regExp, replacement.toString());
    }

    return template;
  }
}

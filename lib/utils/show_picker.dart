import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tcga_flutter/config/palette.dart';
import 'package:tcga_flutter/routes.dart';
import 'package:tcga_flutter/screens/global_widgets/address/addresses_component/address_item_component/state.dart';

import 'app_localizations.dart';

typedef void OnMarkerPressed(AddressItemState address);

showInfoWindowMarker(BuildContext context, Offset position,
    AddressItemState address, OnMarkerPressed onPress) async {
  final RenderBox overlay = Overlay.of(context).context.findRenderObject();
  final selectedId = await showMenu(
    context: context,
    position: RelativeRect.fromRect(
        position & Size(0, 0), // smaller rect, the touch area
        Offset.zero & overlay.size // Bigger rect, the entire screen
        ),
    items: [
      PopupMenuItem(
        child: InkWell(
          onTap: () {
            if (onPress != null) {
              Navigator.of(context).pop();
              onPress(address);
            }
          },
          child: Container(
            width: 120.0,
            child: Row(
              children: [
                Expanded(
                  child: Text(
                    address?.fullAddress ?? '',
                    style: textStyleHinttext,
                    maxLines: 3,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    ],
    elevation: 4.0,
  );
}

Future<int> showPicker(
  BuildContext context,
  List<String> data, {
  int initValue,
  String title,
  bool isRequired,
}) async {
  int some = initValue ?? 0;
  String titlePicker = title?.replaceAll('*', '') ?? '';
  final selectedId = await showCupertinoModalPopup(
    context: context,
    builder: (context) {
      return Material(
        type: MaterialType.transparency,
        child: Container(
          height: 300.0,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                  color: Palette.colorWhite,
                  border: Border(
                    bottom: BorderSide(
                      color: Color(0xff999999),
                      width: 0.0,
                    ),
                  ),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    CupertinoButton(
                      child: Text(AppLocalizations.instance.text(isRequired
                          ? 'common.cancel'
                          : 'common.clear')),
                      onPressed: () {
                        Navigator.of(context).pop(isRequired ? null : -1);
                      },
                      padding: const EdgeInsets.symmetric(
                        horizontal: 16.0,
                        vertical: 5.0,
                      ),
                    ),
                    if (title != null && title.trim().length > 0)
                      Expanded(
                        child: Center(
                          child: Text(
                            titlePicker,
                            style: textStylePrimary,
                            overflow: TextOverflow.ellipsis,
                            maxLines: 1,
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                    CupertinoButton(
                      child: Text(
                          AppLocalizations.of(context).text('common.confirm')),
                      onPressed: () {
                        Navigator.of(context).pop(some);
                      },
                      padding: const EdgeInsets.symmetric(
                        horizontal: 16.0,
                        vertical: 5.0,
                      ),
                    )
                  ],
                ),
              ),
              Container(
                height: 180.0,
                color: Palette.colorWhite,
                child: CupertinoPicker(
                  scrollController: FixedExtentScrollController(
                    initialItem: initValue ?? 0,
                  ),
                  backgroundColor: Palette.colorWhite,
                  onSelectedItemChanged: (value) {
                    some = value;
                  },
                  itemExtent: 32.0,
                  children: data.map((e) => Text(e)).toList(),
                ),
              )
            ],
          ),
        ),
      );
    },
  );
  return selectedId;
}

Future<T> showCustomDialog<T>(
  BuildContext context,
  String pageName, {
  Map<String, dynamic> args,
}) {
  return showGeneralDialog(
    context: context,
    pageBuilder: (context, anim1, anim2) {
      return routes.buildPage(pageName, args);
    },
    barrierDismissible: true,
    barrierLabel: 'dismiss',
    barrierColor: Colors.black54,
  );
}

Future<T> showModal<T>(
  BuildContext context,
  String pageName, {
  Map<String, dynamic> args,
}) {
  return showGeneralDialog(
    context: context,
    pageBuilder: (context, anim1, anim2) {
      return Container(
        margin: const EdgeInsets.only(top: 72),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15.0),
        ),
        clipBehavior: Clip.hardEdge,
        child: routes.buildPage(pageName, args),
      );
    },
    barrierDismissible: true,
    barrierLabel: 'dismiss',
    barrierColor: Colors.black54,
  );
}

Future<T> showModal2<T>(
  BuildContext context,
  String pageName, {
  Map<String, dynamic> args,
}) {
  return showModalBottomSheet(
    isScrollControlled: true,
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
    context: context,
    builder: (context) {
      return SingleChildScrollView(
          padding: EdgeInsets.only(
            bottom: MediaQuery.of(context).viewInsets.bottom,
          ),
          child: routes.buildPage(pageName, args));
    },
  );
}

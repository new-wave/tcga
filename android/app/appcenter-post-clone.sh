#!/usr/bin/env bash
#Place this script in project/android/app/
cd ..
# fail if any command fails
set -e
# debug log
set -x
cd ..

rm -fR flutter
git clone -b master https://github.com/flutter/flutter.git
cd flutter
git checkout 9b2d32b605630f28625709ebd9d78ab3016b2bf6
cd ..

export PATH=`pwd`/flutter/bin:$PATH

flutter doctor
echo "Installed flutter to `pwd`/flutter"

flutter build apk --release
#copy the APK where AppCenter will find it
mkdir -p android/app/build/outputs/apk/; mv build/app/outputs/apk/release/app-release.apk $_
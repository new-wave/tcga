=======
# TheCaregiverApp
A version of TheCaregiverApp on Flutter following the design defined [here](https://www.figma.com/file/4leNnSXoa8jrLzj60Jwlj2/CareGiverApp?node-id=868%3A3570)

# Use Multi Language
- Thêm text vào 3 file json ở đường dẫn: 
    [English](assets/i18n/en.json)
    [Spanish](assets/i18n/es.json)
    
- Dùng text đã khai báo ở file json: AppLocalizations.instance.text('key') hoặc AppLocalizations.of(context).text("key")

# Flutter Hive
```
flutter packages pub run build_runner build --delete-conflicting-outputs
```